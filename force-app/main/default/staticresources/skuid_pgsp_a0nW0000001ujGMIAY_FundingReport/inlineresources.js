(function(skuid){
skuid.snippet.register('HideOtherFacilitiesST',function(args) {var field = arguments[0];
var amount = arguments[1];
var cellElem = field.element;
var row = field.row;
var stFacility = row.Opportunity__r.Short_Term_Facility__c;
var acctFacility = 'CRB';

if(stFacility == acctFacility){ 
    cellElem.text( amount );
    
}
else{
    cellElem.text( );
    
}
});
}(window.skuid));