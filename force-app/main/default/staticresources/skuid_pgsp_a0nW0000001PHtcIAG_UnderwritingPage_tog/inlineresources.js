(function(skuid){
skuid.snippet.register('PageReload',function(args) {location.reload();
});
skuid.snippet.register('ResultsTable',function(args) {// stageRenderer.js
var $ = skuid.$;
var field = arguments[0];
var value = arguments[1];
var row = field.row;
switch (field.mode) {
case 'edit':
    skuid.ui.fieldRenderers[field.metadata.displaytype].edit(field, value);
    break;
case 'read':
case 'readonly':
    var cellElem = field.element;
    cellElem.css('color', function () {
        var bgValue = null;
        if (value) {
            if (row.Result__c == 'Pass') {
                bgValue = '#1dcd84';           
            } else {
                bgValue = '#CDA61D';
            }
        }
        return bgValue;
    });
    cellElem.css('background-color', function () {
        var bgValue = null;
        if (value) {
            if (row.Result__c == 'Pass') {
                bgValue = '#D3FAEA';           
            } else {
                bgValue = '#FFF8BC';
            }
        }
        return bgValue;
    });

    skuid.ui.fieldRenderers[field.metadata.displaytype].read(field, skuid.utils.decodeHTML(value));
    break;
}
});
skuid.snippet.register('FundingPackage',function(args) {var params = arguments[0],
$ = skuid.$;

$.blockUI({
   message: 'Please wait,Review In progress..',
   onBlock:function(){
       var UnderwritingModel = skuid.model.getModel('Underwriting');
var OppId = UnderwritingModel.data[0].Opportunity__c;
var result = sforce.apex.execute(
"FundingPackageService","getFundingPackageData",{
opportunityId:OppId,
ignoreRequiredFolder:false
}

);
var respObj = JSON.parse(result);
if(respObj.isRequiredDocMising){
    
    var txt=respObj.missingFolders;
  if (confirm("Warning!\n\nThe following required folders are missing. Do you still want to continue?\n"+txt)) {
      var result1 = sforce.apex.execute(
"FundingPackageService","getFundingPackageData",{
opportunityId:OppId,
ignoreRequiredFolder:true
});

  } else {
      location.reload();
      return false;
      
      
  }

}  {
    $.unblockUI();
}     
        
   }
});
});
skuid.snippet.register('EquipmentValidation',function(args) {var params = arguments[0],
$ = skuid.$;

var UnderwritingModel = skuid.model.getModel('Underwriting');
var OppModel = skuid.model.getModel('RelatedOpportunity');
var FundingDataModel = skuid.model.getModel('FundingData');
var combinedLoanAmount = OppModel.data[0].Combined_Loan_Amount__c;
var recId = UnderwritingModel.data[0].Id;
var equipOrderAmount = FundingDataModel.data[0].Equipment_Order_Amount__c;
var invoiceAmount = FundingDataModel.data[0].Equipment_Invoice_Amount__c;
var stipName = 'EQP';
if(equipOrderAmount/combinedLoanAmount >0.6){
    
  if (confirm("ALERT: DO NOT forward the order. The equipment cost exceeds 60% of the loan amount. A stip will be created to inform the installer that the equipment cost must not be greater than 60% of loan amount. Please confirm equipment cost was entered accurately and click \"OK\" if the Equipment Order Amount was entered accurately. (DO NOT forward the order) Click \"Cancel\" to close this message without opening a Stip so that you can modify the equipment order amount.")) {
     var result = sforce.apex.execute(
"DistributionChannelPartnershipServ","createStipData",{
underWritingId:recId,
stipName:stipName
});
      FundingDataModel.save();
  } else {
      

  }

}
});
skuid.snippet.register('InvoiceValidation',function(args) {var params = arguments[0],
$ = skuid.$;

var UnderwritingModel = skuid.model.getModel('Underwriting');
var OppModel = skuid.model.getModel('RelatedOpportunity');
var FundingDataModel = skuid.model.getModel('FundingData');
var combinedLoanAmount = OppModel.data[0].Combined_Loan_Amount__c;
var recId = UnderwritingModel.data[0].Id;
var equipOrderAmount = FundingDataModel.data[0].Equipment_Order_Amount__c;
var invoiceAmount = FundingDataModel.data[0].Equipment_Invoice_Amount__c;
var stipName = 'INV';
if(equipOrderAmount != invoiceAmount){    
  if (confirm("Invoice amount does not match the amount received in equipment order. An INV stip will be created to notify Sonepar that the discrepancy needs to be resolved. Please confirm the invoice amount was entered accurately and click \"OK\" to create the INV stip. If the invoice amount needs to be corrected, click \"Cancel\" to close this message without opening a Stip so that you can modify the invoice amount")) {
     var result = sforce.apex.execute(
"DistributionChannelPartnershipServ","createStipData",{
underWritingId:recId,
stipName:stipName
});
      FundingDataModel.save();
  } else {
      

  }

}
});
skuid.snippet.register('accountStatus',function(args) {var field = arguments[0], 
value = arguments[1]; 
skuid.ui.fieldRenderers[field.metadata.displaytype][field.mode](field,value);
if (value == 'Inactive' || value == 'Suspended') { 
field.element.css({'color':'red'}); 
}
});
(function(skuid){
    
    function toggle(id){
        alert('got id: ' + id);
    }
	//var $ = skuid.$;
	//$(document.body).one('pageload',function(){
	//	var myModel = skuid.model.getModel('MyModelId');
//	var myComponent = skuid.component.getById('MyComponentUniqueId');
//	});

//once alert works, then
//	var myModel = skuid.model.getModel('EvaluationResultCO');
// var row = myModel.getrowid(id);
// updateRow(row,"Passed");  //depedent on input...
// myModel.updateData();



})(skuid);;
}(window.skuid));