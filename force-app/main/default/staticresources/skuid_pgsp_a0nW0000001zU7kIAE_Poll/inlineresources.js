(function(skuid){
(function(skuid){
	var $ = skuid.$;
	$(function(){
	    // THe names of the Models that should be checked every so often for updates
	    // These should NOT be the Models associated with Charts / Tables, etc.
	    var RECENT_UPDATES_MODELS = [
	        'RecentUpdates_Opportunity'
	    ];
	    // The number of seconds to wait before checking for updates
		var INTERVAL_IN_SECONDS = 10;
		// Each of our Models should have a Condition named "LastModifiedDate"
		var COMMON_CONDITION_NAME = "LastModifiedDate";
		
		var milliseconds = INTERVAL_IN_SECONDS * 1000;
		var RecentUpdates = $.map(RECENT_UPDATES_MODELS,function(modelId){ return skuid.$M(modelId); });
		setInterval(function(){
		    var now = new Date();
		    var previous = new Date(now.getTime() - milliseconds);
		    $.each(RecentUpdates,function(i,model){
		        var condition = model.getConditionByName(COMMON_CONDITION_NAME,true);
    		    var sfDateTime = skuid.time.getSFDateTime(previous);
    		    model.setCondition(condition,previous);
		    });
		    $.when(skuid.model.updateData(RecentUpdates))
		        .done(function(){
		           // If there are records in any of our Models, show our message
		           var foundModelWithUpdates = false;
		           $.each(RecentUpdates,function(i,model){
		               if (model.getRows().length) {
		                    foundModelWithUpdates = true;
		               }
		           });
		           // If we found any Model(s) with recent updates,
		           // show our updated records alert
		           if (foundModelWithUpdates) {
		                $('#UpdatedRecordsAlert').show('fast');    
		           }
		        });
		},milliseconds);
	});
})(skuid);;
skuid.componentType.register('newRecordsAlert',function(elem) {var element = arguments[0],
	$ = skuid.$;

// The names of the Models that we want to REFRESH
// if there are any updates
var MODELS_TO_REFRESH = {
    'RecentUpdates_Opportunity' : 'Opportunity' 
};
var ALERT_MESSAGE = 'There are new / updated Opportunities.';
var MESSAGE_WHILE_LOADING = 'Loading new / updated records...';

var dismissMessage = function(){
  element.hide('fast');
};

var refreshList = $('<a>Click to refresh list</a>')
    .css('text-decoration','underline')
    .css('color','white')
    .on('click',function(){
        $.blockUI({
            message: MESSAGE_WHILE_LOADING
        });
        // Determine the Models we need to update
        // Only update the ones whose corresponding RecentUpdate model has data rows
        var modelsToUpdate = [];
        $.each(MODELS_TO_REFRESH,function(checkModelId,updateModelId){
            var checkModel = skuid.$M(checkModelId);
            var updateModel = skuid.$M(updateModelId);
           if (checkModel && checkModel.getRows().length && updateModel) {
               modelsToUpdate.push(updateModel); 
           }
        });
        $.when(skuid.model.updateData(modelsToUpdate))
            .done(function(){
                dismissMessage();    
                $.unblockUI(); 
            });
        
    });

element
    .on('click',dismissMessage)
    .append(ALERT_MESSAGE)
    .append(refreshList)
    // Hide our element initially
    .hide();
});
}(window.skuid));