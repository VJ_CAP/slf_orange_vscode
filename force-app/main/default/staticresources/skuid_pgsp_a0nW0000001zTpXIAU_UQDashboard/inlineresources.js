(function(skuid){
skuid.snippet.register('stageRenderer',function(args) {// stageRenderer.js
var $ = skuid.$;
var field = arguments[0];
var value = arguments[1];
var row = field.row;
switch (field.mode) {
case 'edit':
    skuid.ui.fieldRenderers[field.metadata.displaytype].edit(field, value);
    break;
case 'read':
case 'readonly':
    var cellElem = field.element;
    cellElem.css('color', function () {
        var bgValue = null;
        if (value) {
            if (row.Opportunity__r.EDW_Originated__c) {
                bgValue = '#0000ff';           
            } else {
                bgValue = '#FFA500';
            }
        }
        return bgValue;
    });

    skuid.ui.fieldRenderers[field.metadata.displaytype].read(field, skuid.utils.decodeHTML(value));
    break;
}
});
skuid.snippet.register('Sort_ByUIOnlyDate',function(args) {//Begin contents of snippet
//Declare the field I want to sort upon, as well as the sorting function
var field = 'Age';
//Sort by the specified field with values ascending from least to greatest
function sort__ByField_ASC(a,b){
  if ( a[field] < b[field] ) return 1;
  if ( a[field] > b[field] ) return -1;
  return 0;
}

//Perform the sorting
var model = skuid.$M('M0Stip');
var rows = model.getRows(); //Get list of unsorted rows
model.abandonAllRows(); //Remove unsorted rows from the original model
var sortedRows = rows.sort(sort__ByField_ASC); //perform the sorting function defined above on the unsorted rows
model.adoptRows(sortedRows); //Adopt the newly sorted rows back into the model//End contents of snippet
});
skuid.snippet.register('M1StipAgeOrder',function(args) {skuid.$M('MyModel').orderByClause = 'Age1 DESC';
});
}(window.skuid));