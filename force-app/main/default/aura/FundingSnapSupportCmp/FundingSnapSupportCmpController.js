({
    doInit : function(component, event, helper) {
        //helper.getFundingLinteItems(component);
        var width = document.documentElement.clientWidth;
        component.set("v.width",width);
    },
    next: function (component, event, helper) {
        helper.next(component, event);
    },
    previous: function (component, event, helper) {
        helper.previous1(component, event);
    },
    next1: function (component, event, helper) {
        helper.next1(component, event);
    },
    previous1: function (component, event, helper) {
        helper.previous1(component, event);
    },
    getFundingLineItemsSolar : function(component, event, helper) {
        component.set("v.data1",null);
        helper.getColumnAndAction(component);
        helper.getFundingLineItemsSolar(component,component.get("v.accrecordId"),false);
    },
    getFundingLineItemsHI : function(component, event, helper) {
        helper.getColumnAndActionForHI(component);
        helper.getFundingLineItemsSolar(component,component.get("v.accrecordId"),true);
    },
    getHistoricalSnapshot : function(component, event, helper) {
        helper.getColumnAndActionForProcessed(component);
        helper.getViewHistoricalData(component,component.get("v.accrecordId"));
    },
    handleSelect : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows'); 
        var setRows = [];
        var setRowsForCsv = [];
        if(selectedRows.length == 0){
            component.set("v.enableDeleteButton",false);
        }
        for ( var i = 0; i < selectedRows.length; i++ ) {
            setRows.push(selectedRows[i].Id);
            setRowsForCsv.push(selectedRows[i]);
            component.set("v.enableDeleteButton",true);
        }
        component.set("v.selectedRows", setRows);
        component.set("v.selectedRowsForCsv", setRowsForCsv);
    },
    handleRowAction : function(component, event, helper) {
    	helper.ViewHistoricalRecordLines(component, event, helper);
    },
    markSelectedRows:function(component, event, helper) {
        helper.showSpinner(component,Event);
        var action = component.get("c.markSnapshotAsProcessed");
        action.setParams({fundingLines:JSON.stringify(component.get("v.data"))});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.blnmarkSnapShotProcessed",false);
                component.set("v.enableDeleteButton",false);
                component.set("v.noRecordsDisplayMessage","THE SNAPSHOT HAS BEEN MARKED AS PROCESSED, PLEASE EXPORT IT NOW AND SEND TO FACILITY ");
                helper.hideSpinner(component,Event);
                component.set("v.data", null);	
            }
        });
        $A.enqueueAction(action);
    },
    markDeletedRows:function(component, event, helper) {
        helper.showSpinner(component,Event);
        var action = component.get("c.deleteFundingLineItems");
        action.setParams({fundingLineIds:component.get("v.selectedRows")});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.hideSpinner(component,Event);
                helper.parseData(component,response.getReturnValue());
                //component.set("v.data", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    downloadCsv : function(component,event,helper){
        var stockData;
        if(component.get("v.selectedRowsForCsv").length != 0)
        	stockData = component.get("v.selectedRowsForCsv");
        else
            stockData = component.get("v.dataDownload");
        var colNames;
        colNames = component.get("v.columns");
        
        var csv = helper.convertArrayOfObjectsToCSV(component,stockData,colNames);   
         if (csv == null){return;} 
	     var hiddenElement = document.createElement('a');
          hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
          hiddenElement.target = '_self'; // 
          hiddenElement.download = 'Funding_Snapshot_Lineitems-export.csv';  // CSV file Name* you can change it.[only name not .csv] 
          document.body.appendChild(hiddenElement); // Required for FireFox browser
    	  hiddenElement.click(); // using click() js function to download csv file
    },
    onOpsPrioritized: function(cmp, evt) {
         var checkCmp = cmp.find("isPrioritized");
        cmp.set("v.opsPrioritize", checkCmp.get("v.value"));
        console.log('resultCmp: '+checkCmp.get("v.value"));

    }
})