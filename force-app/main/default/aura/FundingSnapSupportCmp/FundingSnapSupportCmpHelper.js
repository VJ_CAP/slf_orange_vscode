({
    getColumnAndAction : function(component) {
        component.set('v.columns', [
            {label: 'Funding Type', fieldName: 'Funding_Type__c', type: 'text,initialWidth: 34'},
            {label: 'Underwriting File', fieldName: 'uwFile', type: 'url',typeAttributes: {label: { fieldName: 'uwName' }, target: '_blank'}},
            {label: 'Sighten UUID', fieldName: 'Sighten_UUID__c', type: 'text'},
            {label: 'Opportunity Id', fieldName: 'OpportunityId', type: 'text'},
            {label: 'Account ID', fieldName: 'AccountId', type: 'text'},
            {label: 'Funding ID', fieldName: 'FundingDataId', type: 'text'},
            {label: 'Installer Name', fieldName: 'Installer_Name__c', type: 'text'},
            {label: 'Snapshot Processed', fieldName: 'Snapshot_Processed__c', type: 'text'},
            {label: 'Loan Type', fieldName: 'Loan_Type__c', type: 'text'},
            {label: 'Loan Identifier', fieldName: 'Loan_Identifier__c', type: 'number'},
            {label: 'Account Name', fieldName: 'Account_Name__c', type: 'text'},
            {label: 'M1 Approval Date', fieldName: 'M1_Approval_Date__c', type: 'text'},
            {label: 'Credit Decision Date', fieldName: 'Credit_Decision_Date__c', type: 'text'},
            {label: 'ST Loan Term', fieldName: 'ST_Loan_Term__c', type: 'number'},
            {label: 'LT Loan Term', fieldName: 'LT_Loan_Term__c', type: 'number'},
            {label: 'APR', fieldName: 'APR__c', type: 'number'},
            {label: 'Install State', fieldName: 'Install_State__c', type: 'text'},
            {label: 'Install Contract System Cost', fieldName: 'Install_Contract_System_Cost__c', type: 'currency'},
            {label: 'Gross Total Loan Amount', fieldName: 'Gross_Total_Loan_Amount__c', type: 'currency'},
            {label: 'ST Amount Financed', fieldName: 'ST_Amount_Financed__c', type: 'currency'},
            {label: 'LT Amount Financed', fieldName: 'LT_Amount_Financed__c', type: 'currency'},
            {label: 'LT Loan Monthly Payment', fieldName: 'LT_Loan_Monthly_Payment__c', type: 'currency'},
            {label: 'Effective ST OID', fieldName: 'Effective_ST_OID__c', type: 'number'},
            {label: 'Effective LT OID', fieldName: 'Effective_LT_OID__c', type: 'number'},
            {label: 'Gross OID', fieldName: 'Gross_OID__c', type: 'currency'},
            {label: 'Short Term OID', fieldName: 'Short_Term_OID__c', type: 'currency'},
            {label: 'Long Term OID', fieldName: 'Long_Term_OID__c', type: 'currency'},
            {label: 'Total Facility Fee', fieldName: 'Total_Facility_Fee__c', type: 'currency'},
            {label: 'ST Facility Fee', fieldName: 'ST_Facility_Fee__c', type: 'currency'},
            {label: 'LT Facility Fee', fieldName: 'LT_Facility_Fee__c', type: 'currency'},
            {label: 'Facility Membership Fee', fieldName: 'Facility_Membership_Fee__c', type: 'currency'},
            {label: 'Net Funding Required', fieldName: 'Net_Funding_Required__c', type: 'currency'},
            {label: 'Facility ST Funding Balance', fieldName: 'Facility_ST_Funding_Balance__c', type: 'currency'},
            {label: 'Facility LT Funding Balance', fieldName: 'Facility_LT_Funding_Balance__c', type: 'currency'},
            {label: 'Facility Total Funding Balance Due*', fieldName: 'Facility_Total_Funding_Balance_Due__c', type: 'currency'},
            {label: 'ST Reapproval', fieldName: 'ST_Reapproval__c', type: 'boolean'},
            {label: 'LT Reapproval', fieldName: 'LT_Reapproval__c', type: 'boolean'},
            {label: 'ST Funding Change Order', fieldName: 'ST_Funding_Change_Order__c', type: 'boolean'},
            {label: 'LT Funding Change Order', fieldName: 'LT_Funding_Change_Order__c', type: 'boolean'},
            {label: 'BorrowerLastName', fieldName: 'BorrowerLastName__c', type: 'text'},
            {label: 'BorrowerFirstName', fieldName: 'BorrowerFirstName__c', type: 'text'},
            {label: 'BorrowerDOB', fieldName: 'BorrowerDOB__c', type: 'text'},
            {label: 'SSN', fieldName: 'ssnEncrypted', type: 'text'},
            {label: 'BorrowerStreet', fieldName: 'BorrowerStreet__c', type: 'text'},
            {label: 'BorrowerCity', fieldName: 'BorrowerCity__c', type: 'text'},
            {label: 'BorrowerState', fieldName: 'BorrowerState__c', type: 'text'},
            {label: 'BorrowerZip', fieldName: 'BorrowerZip__c', type: 'text'},
            {label: 'FICO', fieldName: 'FICO__c', type: 'number'},
            {label: 'FICODate', fieldName: 'FICODate__c', type: 'text'},
            {label: 'DebtUtilization', fieldName: 'DebtUtilization__c', type: 'number'},
            {label: 'TotalRevolvingDebt', fieldName: 'TotalRevolvingDebt__c', type: 'currency'},
            {label: 'CreditInquiries12Months', fieldName: 'CreditInquiries12months__c', type: 'number'},
            {label: 'DQPast24Months', fieldName: 'DQPast24months__c', type: 'number'},
            {label: 'AccountsOpenedPast24months', fieldName: 'AccountsOpenedPast24months__c', type: 'number'},
            {label: 'OpenCreditLines', fieldName: 'OpenCreditLines__c', type: 'number'},
            {label: 'CollectionsExcludingMedical', fieldName: 'CollectionsExcludingMedical__c', type: 'number'},
            {label: 'PublicRecordsOnFile', fieldName: 'PublicRecordsOnFile__c', type: 'number'},
            {label: 'MonthsSinceLastRecord', fieldName: 'MonthsSinceLastRecord__c', type: 'number'},
            {label: 'EmploymentLength', fieldName: 'EmploymentLength__c', type: 'number'},
            {label: 'Employer', fieldName: 'Employer__c', type: 'text'},
            {label: 'AnnualIncome', fieldName: 'AnnualIncome__c', type: 'currency'},
            {label: 'DTI', fieldName: 'DTI__c', type: 'number'},
            {label: 'CoBorrowerFirstName', fieldName: 'CoBorrowerFirstName__c', type: 'text'},
            {label: 'CoBorrowerLastName', fieldName: 'CoBorrowerLastName__c', type: 'text'},
            {label: 'CoBorrowerDOB', fieldName: 'CoBorrowerDOB__c', type: 'text'},
            {label: 'CoBorrowerSSN', fieldName: 'CoBorrowerSSN__c', type: 'text'},
            {label: 'CoBorrowerStreet', fieldName: 'CoBorrowerStreet__c', type: 'text'},
            {label: 'CoBorrowerCity', fieldName: 'CoBorrowerCity__c', type: 'text'},
            {label: 'CoBorrowerState', fieldName: 'CoBorrowerState__c', type: 'text'},
            {label: 'CoBorrowerZip', fieldName: 'CoBorrowerZip__c', type: 'text'},
            {label: 'True Up', fieldName: 'True_Up__c', type: 'currency'},
            {label: 'CoApp FICO', fieldName: 'CoApp_Fico__c', type: 'text'},
            {label: 'ST Net Funding Required', fieldName: 'ST_Net_Funding__c', type: 'currency'},
            {label: 'LT Net Funding Required', fieldName: 'LT_Net_Funding_Required__c', type: 'currency'},
            {label: 'Borrower Citizenship', fieldName: 'Borrower_Citizenship__c', type: 'text'},
            {label: 'Borrower Driver License Expire Date', fieldName: 'Borrower_Driver_License_Expire_Date__c', type: 'text'},
            {label: 'Borrower Driver License Issue Date', fieldName: 'Borrower_Driver_License_Issue_Date__c', type: 'text'},
            {label: 'Borrower Driver License Number', fieldName: 'Borrower_Driver_License_Number__c', type: 'text'},
            {label: 'Borrower Driver License State', fieldName: 'Borrower_Driver_License_State__c', type: 'text'},
            {label: 'Borrower Maiden Name', fieldName: 'Borrower_Maiden_Name__c', type: 'text'},
            {label: 'Borrower Mortgage Balance', fieldName: 'Borrower_Mortgage_Balance__c', type: 'currency'},
            {label: 'CoBorrower Citizenship', fieldName: 'CoBorrower_Citizenship__c', type: 'text'},
            {label: 'CoBorrower Driver License Expire Date', fieldName: 'CoBorrower_Driver_License_Expire_Date__c', type: 'text'},
            {label: 'CoBorrower Driver License Issue Date', fieldName: 'CoBorrower_Driver_License_Issue_Date__c', type: 'text'},
            {label: 'CoBorrower Driver License Number', fieldName: 'CoBorrower_Driver_License_Number__c', type: 'text'},
            {label: 'CoBorrower Driver License State', fieldName: 'CoBorrower_Driver_License_State__c', type: 'text'},
            {label: 'CoBorrower Maiden Name', fieldName: 'CoBorrower_Maiden_Name__c', type: 'text'},
            {label: 'UF Created Date Time', fieldName: 'UF_Created_Date_Time__c', type: 'text'},
            {label: 'FNI Application ID', fieldName: 'FNIApplicationId', type: 'text'},
            {label: 'Project Type', fieldName: 'Project_Type__c', type: 'text'},
            {label: 'Product Loan Type', fieldName: 'ProductLoanType', type: 'text'},
            {label: 'Inverter', fieldName: 'Inverter__c', type: 'text'},
            {label: 'Battery', fieldName: 'Battery__c', type: 'text'},
            {label: 'Module', fieldName: 'Module__c', type: 'text'},
            {label: 'System Size KW', fieldName: 'System_Size_kW__c', type: 'number'},
            {label: 'Credit exception (if applicable)', fieldName: 'Credit_exception_if_applicable__c', type: 'text'},
            {label: 'Go to Payment', fieldName: 'Go_to_Payment__c', type: 'currency'},
            {label: 'Final Payment Amount', fieldName: 'Final_Payment_Amount__c', type: 'currency'},
            {label: 'ECOA Code', fieldName: 'ECOA_Code__c', type: 'text'},
            {label: 'Trust Indicator', fieldName: 'TrustIndicator', type: 'text'},
            {label: 'APN', fieldName: 'apn', type: 'text'},
            {label: 'County', fieldName: 'county', type: 'text'},
            {label: 'Legal Description', fieldName: 'LegalDescription', type: 'text'},
            {label: 'CL Owner of Record', fieldName: 'CLOwnerOfRecord', type: 'text'},
            {label: 'Cl Street Address', fieldName: 'CLStreetAddress', type: 'text'},
            {label: 'CL City', fieldName: 'CLCity', type: 'text'},
            {label: 'CL State Code', fieldName: 'CLStateCode', type: 'text'},
            {label: 'CL ZipCode', fieldName: 'CLZipCode', type: 'text'},
            {label: 'Bureau Primary Applicant First Name', fieldName: 'BPAFname', type: 'text'},
            {label: 'Bureau Primary Applicant Middle Name', fieldName: 'BPAMname', type: 'text'},
            {label: 'Bureau Primary Applicant Last Name', fieldName: 'BPALname', type: 'text'},
            {label: 'Bureau Primary DOB', fieldName: 'BPDob', type: 'text'},
            {label: 'Bureau Primary SSN', fieldName: 'BPssn', type: 'text'},
            {label: 'Bureau Co-Applicant First Name', fieldName: 'BCAFname', type: 'text'},
            {label: 'Bureau Co-Applicant Middle Name', fieldName: 'BCAMname', type: 'text'},
            {label: 'Bureau Co-Applicant Full Last', fieldName: 'BCALname', type: 'text'},
            {label: 'Bureau Co-Applicant DOB', fieldName: 'BCADob', type: 'text'},
            {label: 'Bureau Co-Applicant SSN', fieldName: 'BCAssn', type: 'text'},
            {label: 'Hot Water Heater', fieldName: 'hwheater', type: 'text'},
            {label: 'ACH', fieldName: 'ach', type: 'boolean'},
            {label: 'O.ACH', fieldName: 'oAch', type: 'boolean'},
            {label: 'Routing Number', fieldName: 'routingNo', type: 'text'},
            {label: 'Bank Name', fieldName: 'bankName', type: 'text'},
            {label: 'Account Number', fieldName: 'accountNo', type: 'text'},
            {label: 'LexisNexis IDLookupResult', fieldName: 'lnIdLookupResult', type: 'text'},
            {label: 'LexisNexis IDQuestionsResult', fieldName: 'lnIdQuestionsResult', type: 'text'},
            {label: 'ACH APR Process Required', fieldName: 'achAprProcessReq', type: 'boolean'},
            {label: 'Total $/Watt', fieldName: 'totalWatt', type: 'number'},
            {label: 'Escalated Monthly Payment Interest Only', fieldName: 'escalatedPayment', type: 'currency'}            
        ]);
    },
    getColumnAndActionForHI : function(component) {
        component.set('v.columns', [
            {label: 'Underwriting ID', fieldName: 'uwID', type: 'text,initialWidth: 34'},
            {label: 'Opportunity Id', fieldName: 'OpportunityId', type: 'text'},
            {label: 'Account ID', fieldName: 'accountLink', type: 'url',typeAttributes: {label: { fieldName: 'Account_Name__c' }, target: '_blank'}},
            {label: 'Installer Account Name', fieldName: 'InstallerName', type: 'text'},
            {label: 'Product Loan Type', fieldName: 'ProductLoanType1', type: 'text'},            
            {label: 'Account Name', fieldName: 'Account_Name__c', type: 'text'},
            {label: 'Payment 1 Approval Date', fieldName: 'Payment_1_Approval_Date__c', type: 'text'},
            {label: 'Payment 1 Funding Amount', fieldName: 'Payment_1_Funding_Amount__c', type: 'currency'},
            {label: 'Payment 2 Approval Date', fieldName: 'Payment_2_Approval_Date__c', type: 'text'},
            {label: 'Payment 2 Funding Amount', fieldName: 'Payment_2_Funding_Amount__c', type: 'currency'},
            {label: 'Payment 3 Approval Date', fieldName: 'Payment_3_Approval_Date__c', type: 'text'},
            {label: 'Payment 3 Funding Amount', fieldName: 'Payment_3_Funding_Amount__c', type: 'currency'},
            {label: 'Credit Decision Date', fieldName: 'Credit_Decision_Date__c', type: 'text'},
            {label: 'LT Loan Term', fieldName: 'LT_Loan_Term__c', type: 'number'},
            {label: 'Long Term APR', fieldName: 'longTermApr', type: 'number'},
            {label: 'Promo Term', fieldName: 'promoTerm', type: 'number'},
            {label: 'Promo APR', fieldName: 'promoApr', type: 'number'},
            {label: 'Interest Type', fieldName: 'interestType', type: 'text'},
            {label: 'Product Name', fieldName: 'productName', type: 'text'},
            {label: 'Install State', fieldName: 'Install_State__c', type: 'text'},
            {label: 'Gross Total Loan Amount', fieldName: 'Gross_Total_Loan_Amount__c', type: 'currency'},
            {label: 'LT Amount Financed', fieldName: 'LT_Amount_Financed__c', type: 'currency'},
            {label: 'Monthly Payment', fieldName: 'monthlyPayment', type: 'currency'},
            {label: 'Monthly Payment Escalated (Single Loan)', fieldName: 'monthlyPaymentEsc', type: 'currency'},
            {label: 'Finance Charge', fieldName: 'finCharge', type: 'currency'},
            {label: 'Effective LT OID', fieldName: 'Effective_LT_OID__c', type: 'number'},
            {label: 'Gross OID', fieldName: 'Gross_OID__c', type: 'currency'},
            {label: 'Long Term OID', fieldName: 'Long_Term_OID__c', type: 'currency'},
            {label: 'Total Facility Fee', fieldName: 'Total_Facility_Fee__c', type: 'currency'},
            {label: 'LT Facility Fee', fieldName: 'LT_Facility_Fee__c', type: 'currency'},
            {label: 'LT Reapproved', fieldName: 'LT_Reapproved__c', type: 'text'},
            {label: 'Facility LT Funding Balance', fieldName: 'Facility_LT_Funding_Balance__c', type: 'currency'},
            {label: 'Net Funding Required', fieldName: 'Net_Funding_Required__c', type: 'currency'},
            {label: 'MLA Flag', fieldName: 'mlaFlag', type: 'boolean'},
            {label: 'Loan Agreement Signed Date', fieldName: 'laSignedDate', type: 'text'},
            {label: 'FL Tax', fieldName: 'flTax', type: 'currency'},
            {label: 'Project Complete', fieldName: 'projectComplete', type: 'boolean'},
            {label: 'Last Name', fieldName: 'lastName', type: 'text'},
            {label: 'First Name', fieldName: 'firstName', type: 'text'},
            {label: 'BorrowerDOB', fieldName: 'BorrowerDOB__c', type: 'text'},
            {label: 'Applicant SSN', fieldName: 'SSN__c', type: 'text'},
            {label: 'BorrowerStreet', fieldName: 'BorrowerStreet__c', type: 'text'},
            {label: 'BorrowerCity', fieldName: 'BorrowerCity__c', type: 'text'},
            {label: 'BorrowerState', fieldName: 'BorrowerState__c', type: 'text'},
            {label: 'BorrowerZip', fieldName: 'BorrowerZip__c', type: 'text'},
            {label: 'FICODate', fieldName: 'FICODate__c', type: 'text'},
            {label: 'FICO Score', fieldName: 'ficoScore', type: 'number'},
            {label: 'DebtUtilization', fieldName: 'DebtUtilization__c', type: 'number'},
            {label: 'TotalRevolvingDebt', fieldName: 'TotalRevolvingDebt__c', type: 'currency'},
            {label: 'CreditInquiries12Months', fieldName: 'CreditInquiries12months__c', type: 'number'},
            {label: 'AccountsOpenedPast24months', fieldName: 'AccountsOpenedPast24months__c', type: 'number'},
            {label: 'DQPast24Months', fieldName: 'DQPast24months__c', type: 'number'},
            {label: 'OpenCreditLines', fieldName: 'OpenCreditLines__c', type: 'number'},
            {label: 'CollectionsExcludingMedical', fieldName: 'CollectionsExcludingMedical__c', type: 'number'},
            {label: 'PublicRecordsOnFile', fieldName: 'PublicRecordsOnFile__c', type: 'number'},
            {label: 'MonthsSinceLastRecord', fieldName: 'MonthsSinceLastRecord__c', type: 'number'},
            {label: 'EmploymentLength', fieldName: 'EmploymentLength__c', type: 'number'},
            {label: 'Employer', fieldName: 'Employer__c', type: 'text'},
            {label: 'AnnualIncome', fieldName: 'AnnualIncome__c', type: 'currency'},
            {label: 'DTI', fieldName: 'DTI__c', type: 'number'},
            {label: 'CoBorrowerFirstName', fieldName: 'CoBorrowerFirstName__c', type: 'text'},
            {label: 'CoBorrowerLastName', fieldName: 'CoBorrowerLastName__c', type: 'text'},
            {label: 'CoBorrowerDOB', fieldName: 'CoBorrowerDOB__c', type: 'text'},
            {label: 'CoBorrowerSSN', fieldName: 'CoBorrowerSSN__c', type: 'text'},
            {label: 'CoBorrowerStreet', fieldName: 'CoBorrowerStreet__c', type: 'text'},
            {label: 'CoBorrowerCity', fieldName: 'CoBorrowerCity__c', type: 'text'},
            {label: 'CoBorrowerState', fieldName: 'CoBorrowerState__c', type: 'text'},
            {label: 'CoBorrowerZip', fieldName: 'CoBorrowerZip__c', type: 'text'},
            {label: 'CoApp FICO', fieldName: 'coAppFico', type: 'number'},
            {label: 'Borrower Citizenship', fieldName: 'Borrower_Citizenship__c', type: 'text'},
            {label: 'Borrower Driver License Expire Date', fieldName: 'Borrower_Driver_License_Expire_Date__c', type: 'text'},
            {label: 'Borrower Driver License Issue Date', fieldName: 'Borrower_Driver_License_Issue_Date__c', type: 'text'},
            {label: 'Borrower Driver License Number', fieldName: 'Borrower_Driver_License_Number__c', type: 'text'},
            {label: 'Borrower Driver License State', fieldName: 'Borrower_Driver_License_State__c', type: 'text'},
            {label: 'Borrower Maiden Name', fieldName: 'Borrower_Maiden_Name__c', type: 'text'},
            {label: 'Borrower Mortgage Balance', fieldName: 'Borrower_Mortgage_Balance__c', type: 'currency'},
            {label: 'CoBorrower Citizenship', fieldName: 'CoBorrower_Citizenship__c', type: 'text'},
            {label: 'CoBorrower Driver License Expire Date', fieldName: 'CoBorrower_Driver_License_Expire_Date__c', type: 'text'},
            {label: 'CoBorrower Driver License Issue Date', fieldName: 'CoBorrower_Driver_License_Issue_Date__c', type: 'text'},
            {label: 'CoBorrower Driver License Number', fieldName: 'CoBorrower_Driver_License_Number__c', type: 'text'},
            {label: 'CoBorrower Driver License State', fieldName: 'CoBorrower_Driver_License_State__c', type: 'text'},
            {label: 'CoBorrower Maiden Name', fieldName: 'CoBorrower_Maiden_Name__c', type: 'text'},
            {label: 'FNI Application ID', fieldName: 'fniAppId', type: 'text'},
            {label: 'Project Category', fieldName: 'projCategory', type: 'text'},
            {label: 'Project Type', fieldName: 'projType', type: 'text'},
            {label: 'Credit exception (if applicable)', fieldName: 'Credit_exception_if_applicable__c', type: 'text'},
            {label: 'Bureau Primary Applicant First Name', fieldName: 'BPAFname', type: 'text'},
            {label: 'Bureau Primary Applicant Last Name', fieldName: 'BPALname', type: 'text'},
            {label: 'Bureau Primary DOB', fieldName: 'BPDob', type: 'text'},
            {label: 'Bureau Primary SSN', fieldName: 'BPssn', type: 'text'},
            {label: 'Bureau Co-Applicant First Name', fieldName: 'BCAFname', type: 'text'},
            {label: 'Bureau Co-Applicant Full Last', fieldName: 'BCALname', type: 'text'},
            {label: 'Bureau Co-Applicant DOB', fieldName: 'BCADob', type: 'text'},
            {label: 'Bureau Co-Applicant SSN', fieldName: 'BCAssn', type: 'text'},
            {label: 'Funding ID', fieldName: 'FundingDataId', type: 'text'},			
            {label: 'ACH APR Process Required', fieldName: 'achAprProcessReq', type: 'boolean'},
            {label: 'Loan Type Code', fieldName: 'loanTypeCode', type: 'text'},
            {label: 'Lender Code', fieldName: 'lenderCode', type: 'text'}
        ]);
    },
    getColumnAndActionForProcessed : function(component) {
        component.set('v.columns1', [{type: "button",initialWidth: 8, typeAttributes: {
            iconName: 'utility:forward',
            name: 'View',
            title: 'View',
            disabled: false,
            value: 'view',
            iconPosition: 'left'
        }},
                                     {label: 'Funding Snapshot Ref', fieldName: 'fundingSnapshotRec', type: 'url',typeAttributes: {label: {fieldName: 'Name'} , target: '_blank'}},
                                     {label: 'Type', fieldName: 'type', type: 'text'},
                                     {label: 'Processed', fieldName: 'Processed__c', type: 'text'},
                                     {label: 'Membership Report Link', fieldName: 'membershipLink', type: 'url',typeAttributes: {label:'Membership Report Link' , target: '_blank'}}
                                    ]);
    }, 
    ViewHistoricalRecordLines : function(component, event, helper) {
        component.set("v.blnmarkSnapShotProcessed",false);
        var row = event.getParam('row');
        var recordId = row.Id;
        var type = row.type;
        var action = component.get("c.getHistoricalLineItems");
        action.setParams({fundingSnapshotId:recordId});
        action.setCallback(this,function(response) {  
            var state = response.getState();
            if (state === "SUCCESS") {
                this.getColumnAndActionForProcessed(component); 
                this.getViewHistoricalData(component,component.get("v.accrecordId"));
                if(type == 'Solar'){
                    this.getColumnAndAction(component);
                }else{
                    this.getColumnAndActionForHI(component);
                }  
                this.parseData(component,response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getFundingLineItemsSolar: function(component,accountRecordId,homeImprovement) {
        this.showSpinner(component,Event);
        component.set("v.data1",null);
        var action = component.get("c.getFundingSnapShotLineItems");
        var counts = component.get("v.currentCount");
        var startDate = component.find("sDate").get("v.value");
        var endDate = component.find("eDate").get("v.value");
        var pageSize = component.get("v.pageSize").toString();
        var pageNumber = component.get("v.pageNumber").toString();
        action.setParams({accountId:accountRecordId,pageSize : pageSize,pageNumber : pageNumber,startDate:startDate,endDate:endDate,prioritizedOnly:component.get("v.opsPrioritize"),homeImprovement:homeImprovement});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue().length > 0){
                     component.set("v.blnmarkSnapShotProcessed",true);
                }else{
                    component.set("v.blnmarkSnapShotProcessed",false);
                }
                this.parseData(component,response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    parseData: function(component,resultData){
        component.set("v.showPagination",true);
        var sofGrossTotalLoanAmount = 0;
        var sofSTAmountFinanced = 0;
        var sofLTAmountFinanced = 0;
        var sofTotalFacilityFee= 0;
        var sofFacilityMembershipFee= 0;
        var sofFacilitySTFundingBalance= 0;
        var sofFacilityLTFundingBalance= 0;
        var sofFacilityTotalFundingBalanceDue= 0;
        var hostname = window.location.hostname;
        var arr = hostname.split(".");
        var instance = arr[1];
        this.hideSpinner(component,Event);
        //component.set("v.dataSize", resultData.length); 
        if(resultData.length == 0){
            component.set("v.noRecordsDisplayMessage","There are no underwriting files ready to be processed for "+component.get("v.accountName")+" at this time");
        }else{
            component.set("v.noRecordsDisplayMessage","");
        }
        for (var i = 0; i < resultData.length; i++) {
            var row = resultData[i];
            component.set("v.fundingSnapShotId",row.Funding_Snapshot__c);
            component.set("v.fundingType",row.Funding_Snapshot__r.isHome__c);
            console.log('notifiy==='+JSON.stringify(row));
            if (row.Gross_Total_Loan_Amount__c != null)
                sofGrossTotalLoanAmount += row.Gross_Total_Loan_Amount__c;
            if(row.ST_Amount_Financed__c)
                sofSTAmountFinanced += row.ST_Amount_Financed__c;
            if(row.LT_Amount_Financed__c)
                sofLTAmountFinanced += row.LT_Amount_Financed__c;
            if(row.Total_Facility_Fee__c)
                sofTotalFacilityFee += row.Total_Facility_Fee__c;
            if(row.Facility_Membership_Fee__c)
                sofFacilityMembershipFee += row.Facility_Membership_Fee__c;
            if(row.Facility_ST_Funding_Balance__c)
                sofFacilitySTFundingBalance += row.Facility_ST_Funding_Balance__c;
            if(row.Facility_LT_Funding_Balance__c)
                sofFacilityLTFundingBalance += row.Facility_LT_Funding_Balance__c;
            if(row.Facility_Total_Funding_Balance_Due__c)
                sofFacilityTotalFundingBalanceDue += row.Facility_Total_Funding_Balance_Due__c;
            
            if(row.SSN__c)
                row.ssnEncrypted = row.SSN__c;
            if(row.Underwriting_File__r.Opportunity__r.Account !=null ){
                row.accountLink = 'https://'+instance+'.salesforce.com/'+row.Underwriting_File__r.Opportunity__r.AccountId;
            }
            if (row.Underwriting_File__r) { 
                row.uwFile = 'https://c.'+instance+'.visual.force.com/'+row.Underwriting_File__r.Id+'';
                row.uwID = row.Underwriting_File__r.Id;
                row.uwName = row.Underwriting_File__r.Name;
                // alert(row.uwName);
                row.OpportunityId = row.Underwriting_File__r.OpportunityID18Char__c;
                row.monthlyPayment = row.Underwriting_File__r.Monthly_Payment__c;
            }
            if (row.Box_Fields__r) { 
                row.laSignedDate = row.Box_Fields__r.Latest_LT_Loan_Agreement_Date_Time__c;
            }
            if(row.Underwriting_File__r.Opportunity__r){
                row.AccountId = row.Underwriting_File__r.Opportunity__r.Account.Id;
                row.FNIApplicationId = row.Underwriting_File__r.Opportunity__r.FNI_Application_ID__c;
                row.TrustIndicator = row.Underwriting_File__r.Opportunity__r.Trust_Indicator__c;
                row.apn = row.Underwriting_File__r.Opportunity__r.APN__c;
                row.county = row.Underwriting_File__r.Opportunity__r.County__c;
                row.LegalDescription = row.Underwriting_File__r.Opportunity__r.Legal_Description__c;
                row.CLOwnerOfRecord = row.Underwriting_File__r.Opportunity__r.CL_Owner_of_Record__c;
                row.CLStreetAddress = row.Underwriting_File__r.Opportunity__r.CL_Street_Address__c;
                row.CLCity = row.Underwriting_File__r.Opportunity__r.CL_City__c;
                row.CLStateCode = row.Underwriting_File__r.Opportunity__r.CL_State_Code__c;
                row.CLZipCode = row.Underwriting_File__r.Opportunity__r.CL_ZipCode__c;
                row.BPAFname = row.Underwriting_File__r.Opportunity__r.Bureau_Primary_Applicant_First_Name__c;
                row.BPAMname = row.Underwriting_File__r.Opportunity__r.Bureau_Primary_Applicant_Middle_Name__c;
                row.BPALname = row.Underwriting_File__r.Opportunity__r.Bureau_Primary_Applicant_Last_Name__c;
                row.BPDob = row.Underwriting_File__r.Opportunity__r.Bureau_Primary_DoB__c;
                row.BPssn = row.Underwriting_File__r.Opportunity__r.Bureau_Primary_SSN__c;
                row.BCAFname = row.Underwriting_File__r.Opportunity__r.Bureau_Co_Applicant_First_Name__c;
                row.BCAMname = row.Underwriting_File__r.Opportunity__r.Bureau_Co_Applicant_Middle_Name__c;
                row.BCALname = row.Underwriting_File__r.Opportunity__r.Bureau_Co_Applicant_Full_Last__c;
                row.BCADob = row.Underwriting_File__r.Opportunity__r.Bureau_Co_Applicant_DoB__c;
                row.BCAssn = row.Underwriting_File__r.Opportunity__r.Bureau_Co_Applicant_SSN__c;
                row.hwheater = row.Underwriting_File__r.Opportunity__r.Hot_Water_Heater__c;
                row.oAch = row.Underwriting_File__r.Opportunity__r.isACH__c;
                row.totalWatt = row.Underwriting_File__r.Opportunity__r.Total_Watt__c;
                row.escalatedPayment = row.Underwriting_File__r.Opportunity__r.Escalated_Monthly_Payment_Interest_Only__c;
                row.achAprProcessReq = row.Underwriting_File__r.Opportunity__r.ACH_APR_Process_Required__c;
                row.InstallerName = row.Underwriting_File__r.Opportunity__r.Installer_Account_Name__c;
                row.ProductLoanType1 = row.Underwriting_File__r.Opportunity__r.Product_Loan_Type__c;
                row.longTermApr = row.Underwriting_File__r.Opportunity__r.Long_Term_APR__c;
                row.monthlyPaymentEsc = row.Underwriting_File__r.Opportunity__r.Monthly_Payment_Escalated_Single_Loan__c;
                row.finCharge = row.Underwriting_File__r.Opportunity__r.Home_Finance_Charge_Loan_Amount__c;
                row.mlaFlag = row.Underwriting_File__r.Opportunity__r.MLA_Flag__c;
                row.flTax = row.Underwriting_File__r.Opportunity__r.FL_Tax__c;
                row.ficoScore = row.Underwriting_File__r.Opportunity__r.Primary_Applicant_FICO__c;
                row.coAppFico = row.Underwriting_File__r.Opportunity__r.Co_Applicant_FICO__c;
                row.fniAppId = row.Underwriting_File__r.Opportunity__r.FNI_Reference_Number__c;
                row.projCategory = row.Underwriting_File__r.Opportunity__r.Project_Category__c;
                row.projType = row.Underwriting_File__r.Opportunity__r.Project_Type__c;
            }
            if(row.Funding_Data__r){
                row.FundingDataId = row.Funding_Data__r.Id;
                row.loanTypeCode = row.Funding_Data__r.Loan_Type_Code__c;
                row.lenderCode = row.Funding_Data__r.Lender_Code__c;
                if(row.Funding_Data__r.Draw_1_ID__r!= null)
                    if(row.Funding_Data__r.Draw_1_ID__r.Project_Complete_Certification__c)
                        row.projectComplete = true;
                if(row.Funding_Data__r.Draw_2_ID__r!= null)
                    if(row.Funding_Data__r.Draw_2_ID__r.Project_Complete_Certification__c)
                        row.projectComplete = true;
                if(row.Funding_Data__r.Draw_3_ID__r!= null)
                    if(row.Funding_Data__r.Draw_3_ID__r.Project_Complete_Certification__c)
                        row.projectComplete = true;
            }
            if(row.Underwriting_File__r.Opportunity__r.SLF_Product__r){
                row.ProductLoanType = row.Underwriting_File__r.Opportunity__r.SLF_Product__r.Product_Loan_Type__c;
                row.ach = row.Underwriting_File__r.Opportunity__r.SLF_Product__r.ACH__c;
                row.promoTerm = row.Underwriting_File__r.Opportunity__r.SLF_Product__r.Promo_Term__c;
                row.promoApr = row.Underwriting_File__r.Opportunity__r.SLF_Product__r.Promo_APR__c;
                row.interestType = row.Underwriting_File__r.Opportunity__r.SLF_Product__r.Interest_Type__c;
                row.productName = row.Underwriting_File__r.Opportunity__r.SLF_Product__r.Name;
            }
            if(row.Underwriting_File__r.Opportunity__r.Account){     
                row.accountName = row.Underwriting_File__r.Opportunity__r.Account.Name;
                row.bankName = row.Underwriting_File__r.Opportunity__r.Account.Bank_Name__c;
                row.lastName = row.Underwriting_File__r.Opportunity__r.Account.LastName;
                row.firstName = row.Underwriting_File__r.Opportunity__r.Account.FirstName;
                row.lnIdLookupResult = row.Underwriting_File__r.Opportunity__r.Account.LexisNexis_IDLookupResult__c;
                row.lnIdQuestionsResult = row.Underwriting_File__r.Opportunity__r.Account.LexisNexis_IDQuestionsResult__c;                        
                row.accountNo = row.Underwriting_File__r.Opportunity__r.Account.Account_Number__c;
                row.routingNo = row.Underwriting_File__r.Opportunity__r.Account.Routing_Number__c;
            }
        }
        component.set("v.SumofGrossTotalLoanAmount",sofGrossTotalLoanAmount);
        component.set("v.SumofSTAmountFinanced",sofSTAmountFinanced);
        component.set("v.SumofLTAmountFinanced",sofLTAmountFinanced);
        component.set("v.SumofTotalFacilityFee",sofTotalFacilityFee);
        component.set("v.SumofFacilityMembershipFee",sofFacilityMembershipFee);
        component.set("v.SumofFacilitySTFundingBalance",sofFacilitySTFundingBalance);
        component.set("v.SumofFacilityLTFundingBalance",sofFacilityLTFundingBalance);
        component.set("v.SumofFacilityTotalFundingBalanceDue",sofFacilityTotalFundingBalanceDue);
        var pageSize = component.get("v.pageSize");
        component.set("v.dataDownload", resultData);
        component.set("v.totalRecords", component.get("v.dataDownload").length);
        component.set("v.startPage",0);
        
        component.set("v.endPage",pageSize-1);
        var data = [];
        for(var i=0; i< pageSize; i++){
            if(component.get("v.dataDownload").length> i)
                data.push(resultData[i]);    
        }
        component.set('v.data', data);
    },
    
    getViewHistoricalData: function(component,accountRecordId) {
        component.set("v.data", null);
        component.set("v.blnmarkSnapShotProcessed", false);
        this.showSpinner(component,Event);
        var action = component.get("c.getViewHistoricalProcessedSnapshot");
        action.setParams({accountId:accountRecordId});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var hostname = window.location.hostname;
                var arr = hostname.split(".");
                var instance = arr[1];
                this.hideSpinner(component,Event);
                var resultData = response.getReturnValue();
                if(resultData.length == 0 ){
                    component.set("v.noRecordsDisplayMessage","No Historical Records to Display");
                }
                for (var i = 0; i < resultData.length; i++) {
                    var row = resultData[i];
                    //row.Processed__c = row.Processed__c.toLocaleString("en-US", {timeZone: "America/New_York"}) //  $A.localizationService.formatDate(row.Processed__c, "MMMM dd yyyy, hh:mm:ss a")
                    if(row.isHome__c)
                        row.type = 'Home';
                    else
                        row.type = 'Solar';
                    row.fundingSnapshotRec = 'https://'+instance+'.salesforce.com/'+row.Id;
                    row.membershipLink = 'https://c.'+instance+'.visual.force.com/apex/TCUMemberShipReportVF?fundingrecordId='+row.Id;
                }
                var pageSize = component.get("v.pageSize1");
        		component.set("v.dataHistorical", resultData);
        		component.set("v.totalRecords1", component.get("v.dataHistorical").length);
        		component.set("v.startPage1",0);
        
        		component.set("v.endPage1",pageSize-1);
        		var data = [];
        		for(var i=0; i< pageSize; i++){
            		if(component.get("v.dataHistorical").length> i)
                		data.push(resultData[i]);    
        		}
        		component.set('v.data1', data);
            }
        });
        $A.enqueueAction(action);
    },
    
    /*
     * Method will be called when use clicks on next button and performs the 
     * calculation to show the next set of records
     */
    next : function(component, event){
        var sObjectList = component.get("v.dataDownload");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var data = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(sObjectList.length > i){
                data.push(sObjectList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.data', data);
    },
    previous : function(component, event){
        var sObjectList = component.get("v.dataDownload");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var data = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                data.push(sObjectList[i]);
                counter ++;
            }else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.data', data);
    },
    /*
     * Method will be called when use clicks on next button and performs the 
     * calculation to show the next set of records
     */
    next1 : function(component, event){
        var sObjectList = component.get("v.dataHistorical");
        var end = component.get("v.endPage1");
        var start = component.get("v.startPage1");
        var pageSize = component.get("v.pageSize1");
        var data = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(sObjectList.length > i){
                data.push(sObjectList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage1",start);
        component.set("v.endPage1",end);
        component.set('v.data1', data);
    },
    previous1 : function(component, event){
        var sObjectList = component.get("v.dataHistorical");
        var end = component.get("v.endPage1");
        var start = component.get("v.startPage1");
        var pageSize = component.get("v.pageSize1");
        var data = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                data.push(sObjectList[i]);
                counter ++;
            }else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage1",start);
        component.set("v.endPage1",end);
        component.set('v.data1', data);
    },
    showSpinner: function(component, event) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event){
        component.set("v.spinner", false);
    },
    convertArrayOfObjectsToCSV : function(component,objectRecords,colNames){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys variable store fields API Names as a key 
        // this labels use in CSV file header  
        
        var keys = new Set();
        var labels = new Set();
        
        colNames.forEach(function (col) {
            keys.add(col.fieldName);
            labels.add(col.label);
        });        
        keys = Array.from(keys);
        labels = Array.from(labels);
        
        //keys = ['Id','firstName','middleName'];
        csvStringResult = '';
        csvStringResult += labels.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            //if(objectRecords[i].isSelected)	{
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                let value = objectRecords[i][skey] === undefined ? '' : objectRecords[i][skey];
                csvStringResult += '"'+ value+'"'; 
                
                counter++;
                
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        //}
        // return the CSV formate String 
        return csvStringResult;        
    }
})