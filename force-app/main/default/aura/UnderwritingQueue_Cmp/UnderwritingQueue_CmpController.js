({
    /*
     * This finction defined column header
     * and calls getAccounts helper method for column data
     * editable:'true' will make the column editable
     * */
	doInit : function(component, event, helper){
        console.log('I am here');
        component.set("v.selectedItem", "slfops");
        component.set("v.showSlfOps", true);
        component.set('v.activeItem', event.getParam('name')); 
        component.set("v.spinner1", true);
        //helper.getQueueCounts(component, helper);
        helper.getInitData(component, helper);
        
        //executes getInitData() every 30 seconds
        /*var interval = window.setInterval(
            $A.getCallback(function(){
                console.log('interval Id - '+interval);
                component.set("v.spinner1", false);
                helper.getInitData(component,helper);
            }), 30000
        );
        component.set("v.setIntervalId", interval);*/
    },
    
    getQueueResults : function (component, event, helper){
		console.log('********* in getQueueResults controller');
		component.set("v.spinner1", true);
		//alert("1 slsI - "+component.get('v.slsI'));
		//alert("1 slsII - "+component.get('v.slsII'));
		helper.getQueueData(component, helper);
    },
	
	getSearchResults : function (component, event, helper){
		console.log('********* in getSearchResults controller');
		component.set("v.spinner1", true);
		//alert("1 slsI - "+component.get('v.slsI'));
		//alert("1 slsII - "+component.get('v.slsII'));
		helper.getQueueData(component, helper);
    },
	
    /*getQueueResults : function (component, event, helper){
        console.log("Old active tab: " + event.getParam("oldValue"));
        console.log("New active tab: " + event.getParam("value"));
        var oldValue = event.getParam("oldValue");
        var newValue = event.getParam("value");
        if(oldValue !== newValue && oldValue !== undefined){
            console.log('clear interval due to navigation');
            window.clearInterval(component.get("v.setIntervalId"));
            
            component.set("v.spinner1", true);
            helper.getQueueData(component, helper);
            
            //executes getQueueCounts() every 30 seconds
            var interval = window.setInterval(
                $A.getCallback(function() { 
                    component.set("v.spinner1", false);
                    helper.getQueueData(component,helper);
                }), 30000
            );
            component.set("v.setIntervalId", interval);
        }else{
            console.log('******************* getQueueResults: condition not satisfied');
        }
    },*/
    
    navigateToUW : function(component, event, helper){
		
		var ctarget = event.currentTarget;
		var id_str = ctarget.dataset.value;
		console.log(id_str);
        var indexStr = ctarget.dataset.index;
		console.log('Index - '+indexStr);
        var selectedItem = component.get("v.selectedItem");
        
        //window.open('/'+id_str);
		
		var action = component.get("c.createUWTask");
        action.setParams({ uwId : id_str, activeTab : selectedItem});
		
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('response: '+res);
                
                if(res.includes("created") || !res){
                    console.log('in if '+id_str);
					/*var queueRecs = component.get("v.data");
					queueRecs.splice(indexStr, 1);
					var selectedItem = component.get("v.selectedItem");
					console.log('selectedItem '+selectedItem);
					if(selectedItem === "slfops"){
						var count = component.get("v.slfOpsCount");
						component.set("v.slfOpsCount", count-1);
					}
					component.set("v.data", queueRecs);*/
					
                    window.open('/'+id_str, "_self");
                    component.set("v.spinner1", false);
					helper.getQueueData(component, helper);
                }else{
                    console.log('in else');
                    component.set("v.alertMessage",res);
                    component.set("v.showReviewAlert",true);
					var queueRecs = component.get("v.data");
					queueRecs.splice(indexStr, 1);
					component.set("v.data", queueRecs);
                }
            }else if (state === "ERROR") {
                console.log('error'+ $A.log("Errors", a.getError()));
            }
        });
		$A.enqueueAction(action);
    },
	
	handleClick : function(component, helper){
		console.log('********* in search controller');
		helper.getQueueData(component, helper);
	},
    
    showSpinner: function(component, event, helper){
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    onNext : function(component, event, helper){
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        helper.buildData(component, helper);
    },
    
    onPrev : function(component, event, helper) {
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        helper.buildData(component, helper);
    },
    
    processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));
        helper.buildData(component, helper);
    },
    
    onFirst : function(component, event, helper) {        
        component.set("v.currentPageNumber", 1);
        helper.buildData(component, helper);
    },
    
    onLast : function(component, event, helper) {        
        component.set("v.currentPageNumber", component.get("v.totalPages"));
        helper.buildData(component, helper);
    },
    
    closeAlert : function(component, event, helper) {        
        component.set("v.showReviewAlert",false)
		helper.getQueueData(component, helper);
    },
	
    handleSLSIClick : function(component, event, helper){
        var slsI = component.get('v.slsI');
		//alert("slsI - "+!slsI);
        component.set('v.slsI', !slsI);
		//alert("slsII - "+component.get('v.slsII'));
		helper.getQueueData(component, helper);
    },
	
    handleSLSIIClick : function(component, event, helper){
        var slsII = component.get('v.slsII');
		//alert("2 slsI - "+component.get('v.slsI'));
		//alert("2 slsII - "+!slsII);
        component.set('v.slsII', !slsII);
		helper.getQueueData(component, helper);
    },
  
})