({
    /*
     * This function will execute on page load 
	 * to get SLFOpsQueue data and Queue counts
     * */
	getInitData : function(component, helper, event){
        console.log("in getInitData helper function");
        var selectedItem = component.get("v.selectedItem");
		var isProfileExcluded = component.get("v.isProfileExcluded");
		//To get SLF Ops Queue data
		console.log("isProfileExcluded"+isProfileExcluded);
		if(isProfileExcluded){
            component.set("v.selectedItem", "m1");
            var m1QueueAction = component.get("c.getM1QueueData");
            m1QueueAction.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var queueDataList = response.getReturnValue();
                    console.log('Response: '+queueDataList);
                    console.log('Page size: '+component.get("v.pageSize"));
                    console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    //component.set("v.m1QueueData", queueDataList);
                    component.set("v.m1Count", queueDataList.length);
                    component.set("v.allData", queueDataList);
                    component.set("v.currentPageNumber",1);
                    helper.buildData(component, helper);
                }else{
					console.log('m1 Action Failed... ');
				}
            });
            var requestInitiatedTime = new Date().getTime();
            $A.enqueueAction(m1QueueAction);
		}else{
			var getOpsDataAction = component.get("c.getSLFOpsQueueData");
			getOpsDataAction.setCallback(this,function(response){
				var state = response.getState();
				if (state === "SUCCESS"){
					var queueData = response.getReturnValue();
					console.log('Response: '+queueData);
					console.log('Page size: '+component.get("v.pageSize"));
					console.log('Total Pages: '+Math.ceil(queueData.length/component.get("v.pageSize")));
					component.set("v.totalPages", Math.ceil(queueData.length/component.get("v.pageSize")));
					//component.set("v.slfOpsQueueData", queueData);
					component.set("v.slfOpsCount", queueData.length);
					component.set("v.allData", queueData);
					component.set("v.currentPageNumber",1);
					helper.buildData(component, helper);
				}else{
					console.log('getOpsDataAction Action Failed... ');
				}
			});
			$A.enqueueAction(getOpsDataAction);
		}
        
		//To fetch queue counts
        var queueCountAction = component.get("c.getQueueCounts");
        queueCountAction.setParams({ isExcluded : isProfileExcluded});
        queueCountAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var queueDataMap = response.getReturnValue();
                console.log("m1Count = " + queueDataMap['m1QueueCount']);
                component.set("v.m1Count",queueDataMap['m1QueueCount']);

                console.log("m2Count = " + queueDataMap['m2QueueCount']);
                component.set("v.m2Count",queueDataMap['m2QueueCount']);
				
                console.log("m0StipCount = " + queueDataMap['m0StipQueueCount']);
                component.set("v.m0StipCount",queueDataMap['m0StipQueueCount']);
				
                console.log("postNtpStipCount = " + queueDataMap['postNtpStipQueueCount']);
                component.set("v.postNtpStipCount",queueDataMap['postNtpStipQueueCount']);
				
                console.log("hiFundingCount = " + queueDataMap['hiFundingQueueCount']);
                component.set("v.hiFundingCount",queueDataMap['hiFundingQueueCount']);
            }else{
				console.log('queueCountAction Action Failed... ');
			}
        });
		$A.enqueueAction(queueCountAction);
        
    },
    
	
	// Temporary function - currently not use it
    getQueueCounts : function(component, helper){
        console.log("in getQueueCounts helper function");
        var action = component.get("c.getQueueCounts");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var queueDataMap = response.getReturnValue();
				
                console.log("m1Count = " + queueDataMap['m1QueueCount']);
                component.set("v.m1Count",queueDataMap['m1QueueCount']);
				
                console.log("m2Count = " + queueDataMap['m2QueueCount']);
                component.set("v.m2Count",queueDataMap['m2QueueCount']);
				
                console.log("m0StipCount = " + queueDataMap['m0StipQueueCount']);
                component.set("v.m0StipCount",queueDataMap['m0StipQueueCount']);
				
                console.log("postNtpStipCount = " + queueDataMap['postNtpStipQueueCount']);
                component.set("v.postNtpStipCount",queueDataMap['postNtpStipQueueCount']);
				
                console.log("hiFundingCount = " + queueDataMap['hiFundingQueueCount']);
                component.set("v.hiFundingCount",queueDataMap['hiFundingQueueCount']);
            }
        });
		$A.enqueueAction(action);
    },
    
    /*
     * This function will get the queue data by making a
	 * server call based on the current page selection 
     * */
    getQueueData : function(component, helper){
        //event.preventDefault();
        console.log("in getQueueData helper function");
        var selectedItem = component.get("v.selectedItem");
        if(selectedItem === "slfops"){
            console.log('********* in slfops');
			component.set("v.allData", "");
			component.set("v.data", "");
			component.set("v.pageList", "");
			component.set("v.searchText", "");
			component.set("v.searchCount", 0);
            var action = component.get("c.getSLFOpsQueueData");
            action.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var queueDataList = response.getReturnValue();
                    console.log('Response: '+queueDataList);
                    console.log('Page size: '+component.get("v.pageSize"));
                    console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.slfOpsQueueData", queueDataList);
					component.set("v.slfOpsCount", queueDataList.length);
					component.set("v.allData", queueDataList);
                    component.set("v.currentPageNumber",1);
                    helper.buildData(component, helper);
                }else{
					console.log('slfops Action Failed... ');
				}
            });
            var requestInitiatedTime = new Date().getTime();
            $A.enqueueAction(action);
        }
        
        if(selectedItem === "m1"){
            console.log('********* in M1');
			component.set("v.allData", "");
			component.set("v.data", "");
			component.set("v.pageList", "");
			component.set("v.searchText", "");
			component.set("v.searchCount", 0);
            var action = component.get("c.getM1QueueData");
            action.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var queueDataList = response.getReturnValue();
                    console.log('Response: '+queueDataList);
                    console.log('Response Time: '+((new Date().getTime())-requestInitiatedTime));
                    console.log('Page size: '+component.get("v.pageSize"));
                    console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.m1QueueData", queueDataList);
                    component.set("v.m1Count", queueDataList.length);
                    component.set("v.allData", queueDataList);
                    component.set("v.currentPageNumber",1);
                    helper.buildData(component, helper);
                }else{
					console.log('m1 Action Failed... ');
				}
            });
            var requestInitiatedTime = new Date().getTime();
            $A.enqueueAction(action);
        }
        
        if(selectedItem === "m2"){
            console.log('********* in m2');
			component.set("v.allData", "");
			component.set("v.data", "");
			component.set("v.pageList", "");
			component.set("v.searchText", "");
			component.set("v.searchCount", 0);
            var action = component.get("c.getM2QueueData");
            action.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var queueDataList = response.getReturnValue();
                    console.log('Response: '+queueDataList);
                    console.log('Response Time: '+((new Date().getTime())-requestInitiatedTime));
                    console.log('Page size: '+component.get("v.pageSize"));
                    console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.m2QueueData", queueDataList);
                    component.set("v.m2Count", queueDataList.length);
                    component.set("v.allData", queueDataList);
                    component.set("v.currentPageNumber",1);
                    helper.buildData(component, helper);
                }else{
					console.log('m2 Action Failed... ');
				}
            });
            var requestInitiatedTime = new Date().getTime();
            $A.enqueueAction(action);
        }
        
        if(selectedItem === "m0stip"){
            console.log('********* in m0stip');
			component.set("v.allData", "");
			component.set("v.data", "");
			component.set("v.pageList", "");
			component.set("v.searchText", "");
			component.set("v.searchCount", 0);
			var includeSLSI = component.get("v.slsI");
			var includeSLSII = component.get("v.slsII");
			//alert("1 slsI - "+component.get('v.slsI'));
			//alert("1 slsII - "+component.get('v.slsII'));
            var action = component.get("c.getM0StipQueueData");
			action.setParams({ includeSLSI : includeSLSI, includeSLSII : includeSLSII});
            action.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var queueDataList = response.getReturnValue();
                    console.log('Response: '+queueDataList);
                    console.log('Response Time: '+((new Date().getTime())-requestInitiatedTime));
                    console.log('Page size: '+component.get("v.pageSize"));
                    console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.m0StipQueueData", queueDataList);
                    component.set("v.m0StipCount", queueDataList.length);
                    component.set("v.allData", queueDataList);
                    component.set("v.currentPageNumber",1);
                    helper.buildData(component, helper);
                }else{
					console.log('m0stip Action Failed... ');
				}
            });
            var requestInitiatedTime = new Date().getTime();
            $A.enqueueAction(action);
        }
        
        if(selectedItem === "postntpstip"){
            console.log('********* in postntpstip');
			component.set("v.allData", "");
			component.set("v.data", "");
			component.set("v.pageList", "");
			component.set("v.searchText", "");
			component.set("v.searchCount", 0);
            var action = component.get("c.getPostNtpStipQueueData");
            action.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var queueDataList = response.getReturnValue();
                    console.log('Response: '+queueDataList);
                    console.log('Response Time: '+((new Date().getTime())-requestInitiatedTime));
                    console.log('Page size: '+component.get("v.pageSize"));
                    console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.postNtpStipQueueData", queueDataList);
                    component.set("v.postNtpStipCount", queueDataList.length);
                    component.set("v.allData", queueDataList);
                    component.set("v.currentPageNumber",1);
                    helper.buildData(component, helper);
                }else{
					console.log('postntpstip Action Failed... ');
				}
            });
            var requestInitiatedTime = new Date().getTime();
            $A.enqueueAction(action);
        }
        
        if(selectedItem === "hifunding"){
            console.log('********* in hifunding');
			component.set("v.allData", "");
			component.set("v.data", "");
			component.set("v.searchCount", 0);
			//component.set("v.pageList", 0);
			component.set("v.searchText", "");
            var action = component.get("c.getHIFundingQueueData");
            action.setCallback(this,function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var queueDataList = response.getReturnValue();
                    console.log('Response: '+queueDataList);
                    console.log('Response Time: '+((new Date().getTime())-requestInitiatedTime));
                    console.log('Page size: '+component.get("v.pageSize"));
                    console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
                    component.set("v.hiFundingQueueData", queueDataList);
                    component.set("v.hiFundingCount", queueDataList.length);
                    component.set("v.allData", queueDataList);
                    component.set("v.currentPageNumber",1);
                    helper.buildData(component, helper);
                }else{
					console.log('hifunding Action Failed... ');
				}
            });
            var requestInitiatedTime = new Date().getTime();
            $A.enqueueAction(action);
        }
		
        if(selectedItem === "search"){
            console.log('********* in search');
			component.set("v.allData", "");
			component.set("v.data", ""); 
			component.set("v.pageList", 0);
			var searchText = component.get("v.searchText");
			console.log('searchText - '+searchText);
			if(searchText !== ""){
				var action = component.get("c.getSearchData");
				action.setParams({ searchText : component.get("v.searchText")});
				action.setCallback(this,function(response) {
					var state = response.getState();
					if (state === "SUCCESS") {
						var queueDataList = response.getReturnValue();
						console.log('Response: '+queueDataList);
						console.log('Response Time: '+((new Date().getTime())-requestInitiatedTime));
						console.log('Page size: '+component.get("v.pageSize"));
						console.log('Total Pages: '+Math.ceil(queueDataList.length/component.get("v.pageSize")));
						component.set("v.totalPages", Math.ceil(queueDataList.length/component.get("v.pageSize")));
						component.set("v.searchCount", queueDataList.length);
						//component.set("v.hiFundingQueueData", queueDataList);
						//component.set("v.hiFundingCount", queueDataList.length);
						component.set("v.allData", queueDataList);
						component.set("v.currentPageNumber",1);
						helper.buildData(component, helper);
					}else{
						console.log('Search Action Failed... ');
					}
				});
				var requestInitiatedTime = new Date().getTime();
				$A.enqueueAction(action);
			}else{
				component.set("v.searchCount", 0);
			}
			
        }
    },
    
    /*
     * This function will build table data
     * based on current page selection
     * */
    buildData : function(component, helper){
        var data = [];
        var pageNumber = component.get("v.currentPageNumber");
        var pageSize = component.get("v.pageSize");
        var allData = component.get("v.allData");
        var x = (pageNumber-1)*pageSize;
        
        //creating data-table data
        for(; x<=(pageNumber)*pageSize; x++){
            if(allData[x]){
            	data.push(allData[x]);
            }
        }
        component.set("v.data", data);
        
        helper.generatePageList(component, pageNumber);
    },
    
    /*
     * This function is to generate page list
     * */
    generatePageList : function(component, pageNumber){
        pageNumber = parseInt(pageNumber);
        var pageList = [];
        var totalPages = component.get("v.totalPages");
        console.log('totalPages: '+totalPages);
        if(totalPages > 1){
            if(totalPages <= 10){
                var counter = 2;
                for(; counter < (totalPages); counter++){
                    pageList.push(counter);
                } 
            } else{
                if(pageNumber < 5){
                    pageList.push(2, 3, 4, 5, 6);
                } else{
                    if(pageNumber>(totalPages-5)){
                        pageList.push(totalPages-5, totalPages-4, totalPages-3, totalPages-2, totalPages-1);
                    } else{
                        pageList.push(pageNumber-2, pageNumber-1, pageNumber, pageNumber+1, pageNumber+2);
                    }
                }
            }
        }
		//alert("pageList - "+pageList);
        component.set("v.pageList", pageList);
    },
   
 })