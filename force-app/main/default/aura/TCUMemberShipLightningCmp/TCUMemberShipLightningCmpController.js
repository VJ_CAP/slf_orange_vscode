({
	doInit : function(component, event, helper) {
		helper.getColumnAndAction(component);
        helper.getMemberShips(component,component.get("v.fundingrecordId"));
        var width = document.documentElement.clientWidth;
        component.set("v.width",width);
        //console.log(component.get("v.width"));
	},
    next: function (component, event, helper) {
        helper.next(component, event);
    },
    previous: function (component, event, helper) {
        helper.previous(component, event);
    },
    handleSelect : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows'); 
        var setRows = [];
        for ( var i = 0; i < selectedRows.length; i++ ) {
            setRows.push(selectedRows[i]);
        }
        component.set("v.selectedRows", setRows);
    },
    downloadCsv : function(component,event,helper){
        // get the Records [contact] list from 'ListOfContact' attribute 
        var stockData;
        if(component.get("v.selectedRows").length != 0)
        	stockData = component.get("v.selectedRows");
        else
            stockData = component.get("v.dataDownload");
        var colNames;
        colNames = component.get("v.columns");
        
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.convertArrayOfObjectsToCSV(component,stockData,colNames);   
         if (csv == null){return;} 
        
        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
	     var hiddenElement = document.createElement('a');
          hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
          hiddenElement.target = '_self'; // 
          hiddenElement.download = 'UnderwritingFile-export.csv';  // CSV file Name 
          document.body.appendChild(hiddenElement); 
    	  hiddenElement.click(); // using click() js function to download csv file
        }
 	
})