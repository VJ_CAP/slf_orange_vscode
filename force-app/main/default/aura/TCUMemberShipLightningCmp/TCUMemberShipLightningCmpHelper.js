({
	getColumnAndAction : function(component) {
        component.set('v.columns', [
            {label: 'Record ID', fieldName: 'Id', type: 'text'},
            {label: 'First Name', fieldName: 'firstName', type: 'text'},
            {label: 'Middle Initial', fieldName: 'middleInitial', type: 'text'},
            {label: 'Last Name', fieldName: 'lastName', type: 'text'},
            {label: 'Home Phone', fieldName: 'homePhone', type: 'text'},
            {label: 'Work Phone', fieldName: 'workPhone', type: 'text'},
            {label: 'Cell Phone', fieldName: 'cellPhone', type: 'text'},
            {label: 'Email', fieldName: 'email', type: 'text'},
            {label: 'Street', fieldName: 'BorrowerStreet__c', type: 'text'},
            {label: 'City', fieldName: 'BorrowerCity__c', type: 'text'},
            {label: 'State', fieldName: 'BorrowerState__c', type: 'text'},
            {label: 'Zip', fieldName: 'BorrowerZip__c', type: 'text'},
            {label: 'ID Type', fieldName: 'idType', type: 'text'},
            {label: 'ID Number', fieldName: 'idNumber', type: 'text'},
            {label: 'ID State', fieldName: 'idState', type: 'text'},
            {label: 'ID Issue Date', fieldName: 'idIssueDate', type: 'text'},
            {label: 'ID Expiration Date', fieldName: 'idExpDate', type: 'text'},
            {label: 'BirthDate', fieldName: 'bDate', type: 'text'},
            {label: 'Maiden Name', fieldName: 'maidenName', type: 'text'},
            {label: 'SSN', fieldName: 'ssn', type: 'text'},
            {label: 'Citizenship', fieldName: 'cship', type: 'text'},
            {label: 'Employer', fieldName: 'employer', type: 'text'},
            {label: 'Occupation', fieldName: 'occupation', type: 'text'},
            {label: 'Joint First Name', fieldName: 'coFName', type: 'text'},
            {label: 'Joint Middle Name', fieldName: 'coMName', type: 'text'},
            {label: 'Joint Last Name', fieldName: 'coLName', type: 'text'},
            {label: 'Joint Home Phone', fieldName: 'coHomePhone', type: 'text'},
            {label: 'Joint Work Phone', fieldName: 'coWorkPhone', type: 'text'},
            {label: 'Joint Cell Phone', fieldName: 'coCellPhone', type: 'text'},
            {label: 'Joint Email', fieldName: 'coEmail', type: 'text'},
            {label: 'Joint Extra Address', fieldName: 'coExtraAddr', type: 'text'},
            {label: 'Joint Street', fieldName: 'CoBorrowerStreet__c', type: 'text'},
            {label: 'Joint City', fieldName: 'CoBorrowerCity__c', type: 'text'},
            {label: 'Joint State', fieldName: 'CoBorrowerState__c', type: 'text'},
            {label: 'Joint Zip', fieldName: 'CoBorrowerZip__c', type: 'text'},
            {label: 'Joint ID Type', fieldName: 'coIdType', type: 'text'},
            {label: 'Joint ID Number', fieldName: 'coIdNumber', type: 'text'},
            {label: 'Joint ID State', fieldName: 'ciIdState', type: 'text'},
            {label: 'Joint ID Issue Date', fieldName: 'coIdIssueDate', type: 'text'},
            {label: 'Joint ID Expiration Date', fieldName: 'ciIdExpDate', type: 'text'},
            {label: 'Joint BirthDate', fieldName: 'coBdate', type: 'text'},
            {label: 'Joint Maiden Name', fieldName: 'coMaidenName', type: 'text'},
            {label: 'Joint SSN', fieldName: 'coSsn', type: 'text'},
            {label: 'Joint Citizenship', fieldName: 'CoBorrower_Citizenship__c', type: 'text'},
            {label: 'Joint Employer', fieldName: 'coEmployer', type: 'text'},
            {label: 'Joint Occupation', fieldName: 'coOccupation', type: 'text'},
            {label: 'Mailing Street', fieldName: 'mailStreet', type: 'text'},
            {label: 'Mailing City', fieldName: 'mailCity', type: 'text'},
            {label: 'Mailing State', fieldName: 'mailState', type: 'text'},
            {label: 'Mailing Zip', fieldName: 'mailZip', type: 'text'},
            {label: 'Loan Identifier', fieldName: 'loanIdentifier', type: 'text'},
            {label: 'Opportunity Id', fieldName: 'OpportunityID18Char__c', type: 'text'},
        ]);
    },
    getMemberShips: function(component,fundingSnapShotId) {
        var action = component.get("c.getMemberShipData");
        action.setParams({fundingSnapShotId:fundingSnapShotId});
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
            	//console.log('=============='+JSON.stringify(resultData));
                if(resultData.length == 0){
                    component.set("v.noRecordsDisplayMessage","There are no records to Display");
                }
                for (var i = 0; i < resultData.length; i++) {
                    var row = resultData[i];
                    if(row.Opportunity__r.Account)	{
                    	row.firstName = row.Opportunity__r.Account.FirstName;
                        row.middleInitial = row.Opportunity__r.Account.MiddleName;
                        row.lastName = row.Opportunity__r.Account.LastName;
                        row.homePhone = row.Opportunity__r.Account.PersonHomePhone;
                        row.workPhone = row.Opportunity__r.Account.PersonOtherPhone;
                        row.cellPhone = row.Opportunity__r.Account.PersonMobilePhone;
                        row.email = row.Opportunity__r.Account.PersonEmail;
                        row.idType = row.Opportunity__r.Account.ID_Type__c;
            			row.idNumber = row.Opportunity__r.Account.ID_Number__c;
            			row.idState = row.Opportunity__r.Account.ID_Issuer__c;
            			row.idIssueDate = row.Opportunity__r.Account.ID_Issue_Date__c;
            			row.idExpDate = row.Opportunity__r.Account.ID_Expiration_Date__c;
						row.bDate = row.Opportunity__r.Account.PersonBirthdate;
            			row.maidenName = row.Opportunity__r.Account.Maiden_Name__c;
            			row.ssn = row.Opportunity__r.Account.SSN__c;
            			row.cship = row.Opportunity__r.Account.Citizenship__c;
            			row.employer = row.Opportunity__r.Account.Employer_Name__c;
            			row.occupation = row.Opportunity__r.Account.Job_Title__c;   
            			row.mailStreet = row.Opportunity__r.Account.PersonMailingStreet;
            			row.mailCity = row.Opportunity__r.Account.PersonMailingCity;
            			row.mailState = row.Opportunity__r.Account.PersonMailingState;
            			row.mailZip = row.Opportunity__r.Account.PersonMailingPostalCode;
                    }
                    if(row.Opportunity__r.Co_Applicant__r) {
                        row.coFName = row.Opportunity__r.Co_Applicant__r.FirstName;
                        row.coMName = row.Opportunity__r.Co_Applicant__r.MiddleName;
                        row.coLName = row.Opportunity__r.Co_Applicant__r.LastName;
                        row.coHomePhone = row.Opportunity__r.Co_Applicant__r.PersonHomePhone;
                        row.coWorkPhone = row.Opportunity__r.Co_Applicant__r.PersonOtherPhone;
                        row.coCellPhone = row.Opportunity__r.Co_Applicant__r.PersonMobilePhone;
                        row.coEmail = row.Opportunity__r.Co_Applicant__r.PersonEmail;
                        row.coIdType = row.Opportunity__r.Co_Applicant__r.ID_Type__c;
                        row.coIdNumber = row.Opportunity__r.Co_Applicant__r.ID_Number__c;
                        row.coIdState = row.Opportunity__r.Co_Applicant__r.ID_Issuer__c;
                        row.coIdIssueDate = row.Opportunity__r.Co_Applicant__r.ID_Issue_Date__c;
                        row.coIdExpDate = row.Opportunity__r.Co_Applicant__r.ID_Expiration_Date__c;
                        row.coBdate = row.Opportunity__r.Co_Applicant__r.PersonBirthdate;
                        row.coMaidenName = row.Opportunity__r.Co_Applicant__r.Maiden_Name__c;
                        row.coSsn = row.Opportunity__r.Co_Applicant__r.SSN__c;
                        row.coEmployer = row.Opportunity__r.Co_Applicant__r.Employer_Name__c;
                        row.coOccupation = row.Opportunity__r.Co_Applicant__r.Job_Title__c;
                    }
                    if(row.Opportunity__r)	{
                        row.loanIdentifier = row.Opportunity__r.Sighten_UUID__c;
                    }
                }
                component.set("v.dataSize", resultData.length);
        		component.set("v.dataDownload", resultData);
    			var pageSize = component.get("v.pageSize");
    			component.set("v.totalRecords", component.get("v.dataDownload").length);
        		component.set("v.startPage",0);
        
        		component.set("v.endPage",pageSize-1);
        		var data = [];
        		for(var i=0; i< pageSize; i++){
            		if(component.get("v.dataDownload").length> i)
                		data.push(resultData[i]);    
        		}
 				component.set('v.data', data);
            }
        });
        $A.enqueueAction(action);
    },
    convertArrayOfObjectsToCSV : function(component,objectRecords,colNames){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
       
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
         }
         
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
 
        // in the keys variable store fields API Names as a key 
        // this labels use in CSV file header  
        
        var keys = new Set();
        var labels = new Set();
        
    	colNames.forEach(function (col) {
            keys.add(col.fieldName);
            labels.add(col.label);
    	});        
		keys = Array.from(keys);
        labels = Array.from(labels);
        
        //keys = ['Id','firstName','middleName'];
        csvStringResult = '';
        csvStringResult += labels.join(columnDivider);
        csvStringResult += lineDivider;
 		
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            //if(objectRecords[i].isSelected)	{
                 for(var sTempkey in keys) {
                    var skey = keys[sTempkey] ;  
     
                  // add , [comma] after every String value,. [except first]
                      if(counter > 0){ 
                          csvStringResult += columnDivider; 
                       }   
                   let value = objectRecords[i][skey] === undefined ? '' : objectRecords[i][skey];
                       
                   csvStringResult += '"'+ value+'"'; 
                   
                   counter++;
     
                } // inner for loop close 
                 csvStringResult += lineDivider;
              }// outer main for loop close 
        //}
       // return the CSV formate String
        return csvStringResult;        
    },
        /*
     * Method will be called when use clicks on next button and performs the 
     * calculation to show the next set of records
     */
    next : function(component, event){
        var sObjectList = component.get("v.dataDownload");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var data = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(sObjectList.length > i){
                data.push(sObjectList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        console.log('end1: '+end);
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        console.log('end2: '+end);
        component.set('v.data', data);
    },
    previous : function(component, event){
        var sObjectList = component.get("v.dataDownload");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var data = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                data.push(sObjectList[i]);
                counter ++;
            }else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.data', data);
    }
})