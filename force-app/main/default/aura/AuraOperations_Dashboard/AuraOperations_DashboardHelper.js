({
	createGraph : function(cmp, temp) {
        var dataMap = {"chartLabels": Object.keys(temp),
                       "chartData": Object.values(temp)
                       };
        var el = cmp.find('barChart').getElement();
        var ctx = el.getContext('2d');
        new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: dataMap.chartLabels,
                datasets: [{
                        barPercentage: 0.0,
                        barThickness: 0.2,
                        maxBarThickness: 0.2,
                        minBarLength: 2,
                        label: "Credit Approvals",
                        backgroundColor: "black",
                    	borderColor:"red",
                        data: dataMap.chartData
                    }
                ],             
            },
            
        });
	},
})