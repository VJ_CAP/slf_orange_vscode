({	
	ctr : function(cmp, event, helper) {
        var temp = [];
        var temp2 = [];
        var action = cmp.get("c.getChartMap");
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS' && response.getReturnValue()){
                temp = response.getReturnValue();
                helper.createGraph(cmp, temp);
            }
        });    
       $A.enqueueAction(action);
	}
})