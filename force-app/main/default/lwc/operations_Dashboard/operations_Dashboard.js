import { LightningElement, track, wire } from 'lwc';
// importing apex class and method to retrive accounts
import retriveAccounts  from '@salesforce/apex/LWCExampleController.fetchAccounts';
import retriveContacts  from '@salesforce/apex/LWCExampleController.fetchContacts';

export default class Operations_Dashboard extends LightningElement {

    // to track object name from vf page
    @track objName = 'Account';
    @track accData;
    @track error;
    @track objName1 = 'Contact';
    @track conData;
    @track error1;

    // getting accounts using wire service
    @wire(retriveAccounts, {strObjectName : '$objName'})
    accounts({data, error}) {
        if(data) {
            this.accData = data;
            this.error = undefined;
        }
        else if(error) {
            this.accData = undefined;
            this.error = error;
            window.console.log(error);
        }
    }
    @wire(retriveContacts, {strObjectName : '$objName1'})
    contacts({data, error}) {
        if(data) {
            this.conData = data;
            this.error1 = undefined;
        }
        else if(error) {
            this.conData = undefined;
            this.error1 = error;
            window.console.log(error);
        }
    }
}