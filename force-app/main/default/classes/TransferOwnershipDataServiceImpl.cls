/**
* Description: User and Contact Creation logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Brahmeswar            21/05/2018              Created
******************************************************************************/

public without sharing class TransferOwnershipDataServiceImpl extends RestDataServiceBase{
    /*
* Description: Convert out response from salesforce sobject to wrapper (JSON)
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+TransferOwnershipDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        List<UnifiedBean.UsersWrapper> userRecLst =  reqData.users; 
        UnifiedBean unifiedBean = new UnifiedBean(); 
        unifiedBean.users = new List<UnifiedBean.UsersWrapper>();
        unifiedBean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            List<user> userobj = new List<User>();
        Savepoint sp = Database.setSavepoint();
        List<String> Oppids = new List<String>();
        List<Opportunity> lstOpp = new List<Opportunity>(); 
        try
        {   
            if(String.isBlank(reqData.projectIds) && String.isBlank(reqData.externalIds) && String.isBlank(reqData.hashIds) && 
               String.isBlank(reqData.users[0].newOwnerId) && String.isBlank(reqData.users[0].newOwnerHashId) && String.isBlank(reqData.users[0].hashId) &&
               String.isBlank(reqData.users[0].Id)){ 
                   errMsg = ErrorLogUtility.getErrorCodes('214',null);
                   unifiedBean.returnCode = '214';
               }else if(String.isBlank(reqData.projectIds) && String.isBlank(reqData.externalIds) && String.isBlank(reqData.hashIds)){
                   
                   if(String.isBlank(reqData.users[0].ID) && String.isBlank(reqData.users[0].hashId) && String.isBlank(reqData.users[0].email)){
                       errMsg = ErrorLogUtility.getErrorCodes('203',null);
                       unifiedBean.returnCode = '203';
                   }else if(!String.isBlank(reqData.users[0].ID) && !String.isBlank(reqData.users[0].hashId) && !String.isBlank(reqData.users[0].email) ){
                       errMsg = ErrorLogUtility.getErrorCodes('373',null);
                       unifiedBean.returnCode = '373';
                   }else if(reqData.users != null && !String.isBlank(reqData.users[0].Id)){
                       For(opportunity objopp :[select id,Installer_Account__c,Installer_Account__r.parentid from Opportunity where ownerid=:reqData.users[0].Id]){
                           Oppids.add(objopp.id);
                       }
                   }else if(reqData.users != null && !String.isBlank(reqData.users[0].hashId)){
                       For(opportunity objopp :[select id,Installer_Account__c,Installer_Account__r.parentid from Opportunity where Hash_Id__c=:reqData.users[0].hashId]){
                           Oppids.add(objopp.id);
                       }
                   }else if(reqData.users != null && !String.isBlank(reqData.users[0].email)){
                       For(opportunity objopp :[select id,Installer_Account__c,Installer_Account__r.parentid from Opportunity where owner.email=:reqData.users[0].email]){
                           Oppids.add(objopp.id);
                       }
                   }
                   
               }else if((!String.isBlank(reqData.projectIds) || !String.isBlank(reqData.externalIds) || !String.isBlank(reqData.hashIds)) && (!String.isBlank(reqData.users[0].Id) ||!String.isBlank(reqData.users[0].hashId) || !String.isBlank(reqData.users[0].email))){
                   errMsg = ErrorLogUtility.getErrorCodes('214',null);
                   unifiedBean.returnCode = '214';
                   
               }else if(!String.isBlank(reqData.projectIds) && !String.isBlank(reqData.externalIds) && !String.isBlank(reqData.hashIds)){
                   errMsg = ErrorLogUtility.getErrorCodes('201',null);
                   unifiedBean.returnCode = '201';
                   
               }else if(!String.isBlank(reqData.projectIds) && !String.isBlank(reqData.externalIds)){
                   errMsg = ErrorLogUtility.getErrorCodes('201',null);
                   unifiedBean.returnCode = '201';
                   
               }else if(!String.isBlank(reqData.externalIds) && !String.isBlank(reqData.hashIds)){
                   errMsg = ErrorLogUtility.getErrorCodes('201',null);
                   unifiedBean.returnCode = '201';
               }else if(!String.isBlank(reqData.projectIds) && !String.isBlank(reqData.hashIds)){
                   errMsg = ErrorLogUtility.getErrorCodes('201',null);
                   unifiedBean.returnCode = '201';
               }else{
                   
                   
                   System.debug('***errMsg2222--'+errMsg);
                   if(string.isBlank(errMsg) && reqData.users != null)
                   {
                       if(String.isNotBlank(reqData.users[0].newOwnerId) && String.isNotBlank(reqData.users[0].newOwnerHashId) && String.isNotBlank(reqData.users[0].newOwnerEmail)){
                           errMsg = ErrorLogUtility.getErrorCodes('373',null);
                           unifiedBean.returnCode = '373';
                       }else if(String.isBlank(reqData.users[0].newOwnerId) && String.isBlank(reqData.users[0].newOwnerHashId)&& String.isBlank(reqData.users[0].newOwnerEmail)){
                           errMsg = ErrorLogUtility.getErrorCodes('373',null);
                           unifiedBean.returnCode = '373';
                       }else if(String.isNotBlank(reqData.users[0].newOwnerId) || String.isNotBlank(reqData.users[0].newOwnerHashId) || String.isNotBlank(reqData.users[0].newOwnerEmail)){
                           if(String.isNotBlank(reqData.users[0].newOwnerId)){
                               userobj = [SELECT Id, Username, LastName,IsActive,Profile.Name, FirstName,UserRole.Name,UserRole.DeveloperName,Hash_Id__c,contact.Accountid,contact.Account.parentid FROM User where id=:reqData.users[0].newOwnerId];
                               if(userobj.isEmpty()) 
                               {
                                   errMsg = ErrorLogUtility.getErrorCodes('371',null);
                                   unifiedBean.returnCode = '371';
                               }
                               
                           }else if(String.isNotBlank(reqData.users[0].newOwnerHashId)){
                               userobj = [SELECT Id, Username, LastName,IsActive,Profile.Name, FirstName,UserRole.Name,UserRole.DeveloperName,Hash_Id__c,contact.Accountid,contact.Account.parentid FROM User where Hash_Id__c=:reqData.users[0].hashId];
                               if(userobj.isEmpty()) 
                               {
                                   errMsg = ErrorLogUtility.getErrorCodes('371',null);
                                   unifiedBean.returnCode = '371';
                               }
                               
                           }else if(String.isNotBlank(reqData.users[0].newOwnerEmail)){
                               userobj = [SELECT Id, Username, LastName,IsActive,Profile.Name, FirstName,UserRole.Name,UserRole.DeveloperName,Hash_Id__c,contact.Accountid,contact.Account.parentid FROM User where email=:reqData.users[0].newOwnerEmail];
                               if(userobj.isEmpty()) 
                               {
                                   errMsg = ErrorLogUtility.getErrorCodes('371',null);
                                   unifiedBean.returnCode = '371';
                               }
                               
                           }
                           if(!userobj.isEmpty() && !userobj[0].IsActive){
                               errMsg = ErrorLogUtility.getErrorCodes('371',null);
                               unifiedBean.returnCode = '371';
                           }
                           
                       }
                       
                   }
                   if(string.isBlank(errMsg)){
                       List<String> externalIds = new List<String>();
                       List<String> hashIds = new List<String>();
                       if(!Oppids.isEmpty() || String.isNotBlank(reqData.projectIds)){
                           if(String.isNotBlank(reqData.projectIds))
                               Oppids = String.valueOf(reqData.projectIds).split(';'); 
                           if(Oppids!= null && Oppids.size()>25){
                               errMsg = ErrorLogUtility.getErrorCodes('376',null);
                               unifiedBean.returnCode = '376';
                           }else{
                               system.debug('### Oppids--'+Oppids);
                               lstOpp = [Select id,Accountid,Co_Applicant__c,Installer_Account__c,Installer_Account__r.parentid,(select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c from Underwriting_Files__r) from Opportunity where id IN : Oppids OR (StageName='Archive' AND Opportunity__c IN:Oppids)];
                               if(lstOpp.isEmpty()){
                                   errMsg = ErrorLogUtility.getErrorCodes('202',null);
                                   unifiedBean.returnCode = '202';
                               }else{
                                   for(Opportunity oppRec :lstOpp){
                                       if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                                           unifiedBean.returnCode = '366';
                                           errMsg = ErrorLogUtility.getErrorCodes('366',null);
                                           iolist = new list<DataValidator.InputObject>{};
                                               gerErrorMsg(unifiedBean,errMsg,unifiedBean.returnCode,iolist);
                                           iRestRes = (IRestResponse)unifiedBean;
                                           return iRestRes;
                                       }
                                   }
                               }
                           }
                       }else if(String.isNotBlank(reqData.externalIds)){
                           externalIds = String.valueOf(reqData.externalIds).split(';');
                           system.debug('### externalIds--'+externalIds);
                           if(externalIds!= null && externalIds.size()>25){
                               errMsg = ErrorLogUtility.getErrorCodes('376',null);
                               unifiedBean.returnCode = '376';
                           }else{
                               lstOpp = [Select id,Accountid,Co_Applicant__c,Installer_Account__c,Installer_Account__r.parentid,(select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c from Underwriting_Files__r) from Opportunity where Partner_Foreign_Key__c IN : externalIds];
                               if(lstOpp.isEmpty()){
                                   errMsg = ErrorLogUtility.getErrorCodes('371',null);
                                   unifiedBean.returnCode = '371';
                               }else{
                                   for(Opportunity oppRec :lstOpp){
                                       if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                                           unifiedBean.returnCode = '366';
                                           errMsg = ErrorLogUtility.getErrorCodes('366',null);
                                           iolist = new list<DataValidator.InputObject>{};
                                               gerErrorMsg(unifiedBean,errMsg,unifiedBean.returnCode,iolist);
                                           iRestRes = (IRestResponse)unifiedBean;
                                           return iRestRes;
                                       }
                                   }
                               }
                           }
                       }else if(String.isNotBlank(reqData.hashIds)){
                           hashIds = String.valueOf(reqData.hashIds).split(';');
                           system.debug('### hashIds--'+hashIds);
                           if(hashIds!= null && hashIds.size()>25){
                               errMsg = ErrorLogUtility.getErrorCodes('376',null);
                               unifiedBean.returnCode = '376';
                           }else{
                               lstOpp = [Select id,Accountid,Co_Applicant__c,Installer_Account__c,Installer_Account__r.parentid,(select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c from Underwriting_Files__r) from Opportunity where Hash_Id__c IN : hashIds];
                               if(lstOpp.isEmpty()){
                                   errMsg = ErrorLogUtility.getErrorCodes('371',null);
                                   unifiedBean.returnCode = '371';
                               }else{
                                   for(Opportunity oppRec :lstOpp){
                                       if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                                           unifiedBean.returnCode = '366';
                                           errMsg = ErrorLogUtility.getErrorCodes('366',null);
                                           iolist = new list<DataValidator.InputObject>{};
                                               gerErrorMsg(unifiedBean,errMsg,unifiedBean.returnCode,iolist);
                                           iRestRes = (IRestResponse)unifiedBean;
                                           return iRestRes;
                                       }
                                   }
                               }
                           }
                       }
                       
                   }
                   System.debug('****errMsg--'+errMsg);
                   if(string.isNotBlank(errMsg))
                   {   
                       //errMsg = ErrorLogUtility.getErrorCodes('400',null);
                       //unifiedBean.returnCode = '400';
                       iolist = new list<DataValidator.InputObject>{};
                           gerErrorMsg(unifiedBean,errMsg,unifiedBean.returnCode,iolist);
                       iRestRes = (IRestResponse)unifiedBean;
                       return iRestRes;
                   }else{
                       Set<Account> lstAcc = new Set<Account>();
                       List<Opportunity> lstOppUpdate = new List<Opportunity>();
                       List<SLF_Credit__c> lstcredit = new List<SLF_Credit__c>();
                       List<Quote> lstQuote = new List<Quote>();
                       List<Underwriting_File__c> lstUnerwriting = new List<Underwriting_File__c>();
                       List<Stipulation__c> lstStipulation = new List<Stipulation__c>();
                       List<System_Design__c> lstSystemdDesign = new List<System_Design__c>();
                       set<ID> lstoppids = new set<ID>(); 
                       Account Accobj;
                       if(lstOpp != null && lstOpp.size()>0 && userobj != null && userobj.size()>0){
                           for(Opportunity oppobj : lstOpp){
                               Accobj = new Account(id=oppobj.Accountid,ownerid=userobj[0].id);
                               lstAcc.add(Accobj);
                               if(oppobj.Co_Applicant__c != null){
                                   Accobj = new Account(id=oppobj.Co_Applicant__c,ownerid=userobj[0].id);
                                   lstAcc.add(Accobj);
                               }
                               if(oppobj.Installer_Account__r.parentid == null){
                                   if(oppobj.Installer_Account__c == userobj[0].contact.Accountid){
                                       lstOppUpdate.add(new Opportunity(id=oppobj.id,ownerid=userobj[0].id));
                                   }else{
                                       System.debug('###11111--');
                                       errMsg = ErrorLogUtility.getErrorCodes('377',null);
                                       unifiedBean.returnCode = '377';
                                   }
                               }else if(oppobj.Installer_Account__r.parentid != null){
                                   if( userobj[0].contact.Accountid != null && userobj[0].contact.Account.parentid == null && !String.valueOf(userobj[0].UserRole.DeveloperName).contains('PartnerExecutive')){
                                       System.debug('###2222--');
                                       errMsg = ErrorLogUtility.getErrorCodes('377',null);
                                       unifiedBean.returnCode = '377';
                                   }else if((oppobj.Installer_Account__c == userobj[0].contact.Accountid || oppobj.Installer_Account__c == userobj[0].contact.Account.parentid || 
                                             oppobj.Installer_Account__r.Parentid == userobj[0].contact.Accountid || oppobj.Installer_Account__r.Parentid ==userobj[0].contact.Account.parentid) && ( userobj[0].contact.Account.parentid==null || oppobj.Installer_Account__c == userobj[0].contact.Accountid)){
                                                 lstOppUpdate.add(new Opportunity(id=oppobj.id,ownerid=userobj[0].id));
                                             }else{
                                                 System.debug('###333--');    
                                                 errMsg = ErrorLogUtility.getErrorCodes('377',null);
                                                 unifiedBean.returnCode = '377';
                                             } 
                                   
                                   
                                   
                                   
                               }
                               if(string.isNotBlank(errMsg))
                                   break;
                               lstoppids.add(oppobj.id);
                           }
                           if(string.isNotBlank(errMsg))
                           {   
                               iolist = new list<DataValidator.InputObject>{};
                                   gerErrorMsg(unifiedBean,errMsg,unifiedBean.returnCode,iolist);
                               iRestRes = (IRestResponse)unifiedBean;
                               return iRestRes;
                           }
                           for(SLF_Credit__c objcredit : [select id,ownerid from SLF_Credit__c where opportunity__c IN :lstoppids]){
                               lstcredit.add(new SLF_Credit__c(id=objcredit.id,ownerid=userobj[0].id));
                           }
                           for(Quote  objquote : [select id,ownerid from Quote where opportunityid IN :lstoppids]){
                               lstQuote.add(new Quote(id=objquote.id,ownerid=userobj[0].id));
                           }
                           for(Underwriting_File__c  objUw : [select id,owner__c from Underwriting_File__c where opportunity__c IN :lstoppids]){
                               lstUnerwriting.add(new Underwriting_File__c(id=objUw.id,owner__c=userobj[0].id));
                           }
                           for(Stipulation__c objstip : [Select id,ownerid,Underwriting__c from Stipulation__c where Underwriting__r.opportunity__c IN :lstoppids]){
                               lstStipulation.add(new Stipulation__c(id=objstip.id,ownerid=userobj[0].id));
                               
                           }
                           for(System_Design__c objSD : [select id,ownerid,Opportunity__c from System_Design__c where Opportunity__c IN:lstoppids]){
                               lstSystemdDesign.add(new System_Design__c(id=objSD.id,ownerid=userobj[0].id));
                           }
                       }
                       
                       if(!lstAcc.isEmpty()){
                           List<Account> lstAccUpdate = new List<Account>();
                           lstAccUpdate.addAll(lstAcc);
                           update lstAccUpdate;
                       }
                       if(!lstOppUpdate.isEmpty())
                           update lstOppUpdate;
                       if(!lstcredit.isEmpty())
                           update lstcredit;
                       //if(!lstQuote.isEmpty())
                       //update lstQuote;
                       if(!lstUnerwriting.isEmpty())
                           update lstUnerwriting;
                       //if(!lstStipulation.isEmpty())
                       //update lstStipulation; 
                       if(!lstSystemdDesign.isEmpty())
                           update lstSystemdDesign;
                       
                       unifiedBean = SLFUtility.getUnifiedBean(lstoppids,'Orange',false);
                       unifiedBean.returnCode = '200';
                   }
                   iRestRes = (IRestResponse)unifiedBean;
                   return iRestRes;
               }
            if(string.isNotBlank(errMsg))
            {   
                iolist = new list<DataValidator.InputObject>{};
                    gerErrorMsg(unifiedBean,errMsg,unifiedBean.returnCode,iolist);
                iRestRes = (IRestResponse)unifiedBean;
                return iRestRes;
            }
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400',null);
            gerErrorMsg(unifiedBean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)unifiedBean;
            system.debug('### TransferOwnershipDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('TransferOwnershipDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        
        system.debug('### Exit from transformOutput() of '+TransferOwnershipDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        iRestRes = (IRestResponse)unifiedBean;
        return iRestRes;
    }
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
    
    // added as part of ORANGE-1109 by venkatesh  - Start
    public static User assignOwnerInfo(UnifiedBean projectWrP){
        Map<String,Id> mapOfUserInstallerInfo = new Map<String,Id>();  
       User newOwner = new User();
        try{
            String  STR_OWNER_ID='OWNER_ID';
            String  STR_INSTALLER_ID='INSTALLER_ID';
           
            List<User> lstNewOwner = new List<User>();
            
            if(String.isNotBlank(projectWrP.projects[0].ownerEmail)){

                 lstNewOwner =  [SELECT id, email, contactid, contact.FirstName, contact.LastName, contact.AccountId,contact.Account.Risk_Based_Pricing__c,contact.Account.Push_Endpoint__c,contact.Account.Subscribe_to_Push_Updates__c,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.FNI_Domain_Code__c,contact.Account.Account_Status__c,contact.Account.Eligible_for_Last_Four_SSN__c,contact.Account.Credit_Waterfall__c,Hash_Id__c,Username, LastName,IsActive,Profile.Name, FirstName,UserRole.Name,UserRole.DeveloperName,Account.Requires_ACH_APR_Validation__c,contact.Account.parentid 
                                FROM USER 
                                WHERE Email =:projectWrP.projects[0].ownerEmail AND IsActive=True];
                 system.debug('### lstNewOwner -->:'+lstNewOwner.size());
                
                List<User> lstOldOwner= new List<User>(); 
                
                lstOldOwner = [SELECT id, email, contactid, contact.FirstName, contact.LastName, contact.AccountId,Hash_Id__c,Username, LastName,IsActive,Profile.Name, FirstName,UserRole.Name,UserRole.DeveloperName,Account.Requires_ACH_APR_Validation__c,contact.Account.parentid,contact.Account.Account_Status__c FROM USER WHERE id=:UserInfo.getUserId()];

                    // chekc for user record
                    if(!lstNewOwner.isEmpty() && lstNewOwner.size() == 1 && lstNewOwner[0].contactid !=null && lstOldOwner[0].contact!=null && lstOldOwner[0].UserRole!=null && lstNewOwner[0].UserRole!=null){
                        
                        // old owner has no parent
                        if(lstOldOwner[0].contact.Account.parentid ==null ){
                            system.debug('### Owner Doesnt Have Parent');
                            //newOwner=lstNewOwner[0];// added by suma to check 11355 defect
                            // user belongs to same installer 
                            if(lstOldOwner[0].contact.AccountId == lstNewOwner[0].contact.AccountId ){
                                system.debug('###  user belongs to same installer ');
                                // if old user is partner executive,new user is not partner executive, assign owner with new value
                                if(String.valueOf(lstOldOwner[0].UserRole.DeveloperName).contains('PartnerExecutive')  ){
                                   newOwner=lstNewOwner[0];
                                    
                                }
                                
                            }else if(lstNewOwner[0].contact.Account.parentid!=null && lstOldOwner[0].contact.AccountId == lstNewOwner[0].contact.Account.parentid ){
                                system.debug('###  Old user is parent and new user is child and parent user role is executive ');
                                // Old user is parent and new user is child and parent user role is executive
                                
                               // if old user is partner executive, assign owner with new value
                                if(String.valueOf(lstOldOwner[0].UserRole.DeveloperName).contains('PartnerExecutive') ){
                                     newOwner=lstNewOwner[0];
                                    
                                }
                            }
                        }else{
                            system.debug('### Owner Has Parent');
                            if(lstOldOwner[0].contact.AccountId == lstNewOwner[0].contact.AccountId){
                                //  old owner and new owner belongs to same installer 
                                system.debug('### old owner and new owner belongs to same installer ');
                                system.debug('### lstOldOwner UserRole -->: '+lstOldOwner[0].UserRole.DeveloperName+'### lstNewOwner UserRole -->:'+lstNewOwner[0].UserRole.DeveloperName);
                                // if old user is partner executive, assign owner with new value
                                if(String.valueOf(lstOldOwner[0].UserRole.DeveloperName).contains('PartnerExecutive') ){
                                    newOwner=lstNewOwner[0];
                                    
                                }
                                
                            }else if(lstNewOwner[0].contact.Account.parentid!=null && lstOldOwner[0].contact.AccountId == lstNewOwner[0].contact.Account.parentid  ){
                                // old owner is parent and new owner is child
                                // if old user is partner executive, assign owner with new value
                                if(String.valueOf(lstOldOwner[0].UserRole.DeveloperName).contains('PartnerExecutive') ){
                                    newOwner=lstNewOwner[0];
                                    
                                }
                                
                            }else if((lstOldOwner[0].contact.Account.parentid == lstNewOwner[0].contact.Account.parentid)&& (lstOldOwner[0].contact.AccountId == lstNewOwner[0].contact.AccountId)){
                                // old owner and new owner belongs to same installer parent i.e. and child installer is same for this two users 
                                // if old user is partner executive, assign owner with new value
                                if(String.valueOf(lstOldOwner[0].UserRole.DeveloperName).contains('PartnerExecutive') ){
                                   newOwner=lstNewOwner[0];
                                   
                                }
                            }else if(lstOldOwner[0].contact.Account.parentid == lstNewOwner[0].contact.AccountId){
                                //  old owner is child and new owner is parent hense cannot assign new owner 
                                
                            }
                            
                        }
   
                    }  
            }
        }catch(exception e){
            system.debug('### exception in assignOwnerInfo -->:'+e.getMessage() + '### line -->:'+e.getLineNumber());

            ErrorLogUtility.writeLog('TransferOwnershipDataServiceImpl.assignOwnerInfo()',e.getLineNumber(),'assignOwnerInfo()',e.getMessage() + '###' +e.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(e,null,null,null,null));   
        }
        system.debug('newOwner@@'+newOwner);
        
        return  newOwner;
    } 
    
    public static void updateChildRecords(id oppRecId,User ownerRec){
        
        List<Account> lstAcc = new List<Account>();
        List<SLF_Credit__c> lstcredit = new List<SLF_Credit__c>();
        List<Underwriting_File__c> lstUnerwriting = new List<Underwriting_File__c>();
        List<Stipulation__c> lstStipulation = new List<Stipulation__c>();
        List<System_Design__c> lstSystemdDesign = new List<System_Design__c>();
        Account Accobj;
        try{
            List<Opportunity> lstOpp = [SELECT id,ownerId,Co_Applicant__c,Accountid From Opportunity WHERE id=:oppRecId];
            if(lstOpp.size()>0){
                
                string projectId=lstOpp[0].id;
                string userRecId=(ownerRec!=null)?ownerRec.Id:userInfo.getUserId();
                lstOpp[0].ownerId=userRecId;
                Accobj = new Account(id=lstOpp[0].Accountid,ownerid=userRecId);
                lstAcc.add(Accobj);
                system.debug('Owner Id-->:'+ownerRec);
                if(lstOpp[0].Co_Applicant__c != null){
                    Accobj = new Account(id=lstOpp[0].Co_Applicant__c,ownerid=userRecId);
                    lstAcc.add(Accobj);
                }
                for(SLF_Credit__c objcredit : [select id,ownerid from SLF_Credit__c where opportunity__c =:projectId]){
                    lstcredit.add(new SLF_Credit__c(id=objcredit.id,ownerid=userRecId));
                }
                
                for(Underwriting_File__c  objUw : [select id,owner__c from Underwriting_File__c where opportunity__c =:projectId]){
                    lstUnerwriting.add(new Underwriting_File__c(id=objUw.id,owner__c=userRecId));
                }
                for(Stipulation__c objstip : [Select id,ownerid,Underwriting__c from Stipulation__c where Underwriting__r.opportunity__c =:projectId]){
                    lstStipulation.add(new Stipulation__c(id=objstip.id,ownerid=userRecId));
                    
                }
                for(System_Design__c objSD : [select id,ownerid,Opportunity__c from System_Design__c where Opportunity__c =:projectId]){
                    lstSystemdDesign.add(new System_Design__c(id=objSD.id,ownerid=userRecId));
                }
                
                if(!lstAcc.isEmpty()){
                    update lstAcc;
                }
                if(!lstOpp.isEmpty()){
                    update lstOpp;
                }
                if(!lstcredit.isEmpty())
                    update lstcredit;
               
                if(!lstUnerwriting.isEmpty())
                    update lstUnerwriting;
                //if(!lstStipulation.isEmpty())
                //update lstStipulation; 
                if(!lstSystemdDesign.isEmpty())
                    update lstSystemdDesign;  
            }
        }catch(exception e){
            system.debug('### exception in updateChildRecords -->:'+e.getMessage() + '### line -->:'+e.getLineNumber());
             ErrorLogUtility.writeLog('TransferOwnershipDataServiceImpl.updateChildRecords()',e.getLineNumber(),'updateChildRecords()',e.getMessage() + '###' +e.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(e,null,null,null,null));   
        }
        
        
    }
    
    // added as part of ORANGE-1109 by venkatesh  - End
}