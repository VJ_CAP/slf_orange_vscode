/**
* Description: Approve Milestone Document Submission related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/
public with sharing class ApproveMilestoneServiceImpl extends RESTServiceBase{  
   
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ApproveMilestoneServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean apprvMilStnRespBean = new UnifiedBean();
        apprvMilStnRespBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        system.debug('### reqData '+reqData);
        apprvMilStnRespBean.error = new list<UnifiedBean.errorWrapper>();
        
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {
            Set<String> missingList = new Set<String>();
           
            String opportunityId = '';
            String externalId;
            if(reqData.projects == NULL ||reqData.projects[0].milestoneRequest == NULL || reqData.projects[0].milestoneRequest == '')
            {
                apprvMilStnRespBean.returnCode = '266';
                errMsg = ErrorLogUtility.getErrorCodes('266',null);
            }
            if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
            {                 
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                apprvMilStnRespBean.returnCode = '201';
            } 
            else if(
                (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
            {
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                apprvMilStnRespBean.returnCode = '201';
            }else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
            {
                List<Opportunity> opprLst = [SELECT Id FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                
                if (opprLst==null || opprLst.size() == 0)
                {
                    apprvMilStnRespBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
                else{
                    opportunityId = opprLst.get(0).Id;
                }
            }
            else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
            {
                List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                        System.debug('### externalId '+externalId);
                    }else{
                        apprvMilStnRespBean.returnCode = '400';
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                    }
                    
                }
                if(String.isBlank(errMsg))
                {
                    List<opportunity> opprLst = [select id,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        apprvMilStnRespBean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                    else{
                        opportunityId = opprLst.get(0).id;
                    }
                }
            }else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
            {
                List<opportunity> opprLst = [select id,Hash_Id__c from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                if (opprLst!=null && opprLst.size() > 0)
                {
                    opportunityId = opprLst.get(0).id;
                }else {
                    apprvMilStnRespBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            } 
             opportunity oppRec = new opportunity();
             Underwriting_File__c underwritingRec;
             Box_Fields__c boxFieldRec;//Added By Adithya as per 1930
             if(String.isBlank(errMsg)  && String.isNotBlank(opportunityId))
             {
                String fieldnames='';
                for (SF_Category_Map__c categs : SF_Category_Map__c.getAll().values())
                {
                    if(categs.Folder_Id_Field_on_Underwriting__c != null){
                        if(!fieldnames.contains(categs.Folder_Id_Field_on_Underwriting__c)){
                            if (fieldnames == '') {
                                fieldnames = categs.Folder_Id_Field_on_Underwriting__c;
                            } else {
                                fieldnames += ',' + categs.Folder_Id_Field_on_Underwriting__c;
                            }
                        }
                    }
                    if(categs.TS_Field_On_Underwriting__c != null){
                        if(!fieldnames.contains(categs.TS_Field_On_Underwriting__c)){
                            if (fieldnames == '') {
                                fieldnames = categs.TS_Field_On_Underwriting__c;
                            } else {
                                fieldnames += ',' + categs.TS_Field_On_Underwriting__c;
                            }
                        }
                    }
                    if(categs.Last_Run_TS_Field_on_Underwriting_File__c != null){
                        if(!fieldnames.contains(categs.Last_Run_TS_Field_on_Underwriting_File__c)){
                            if (fieldnames == '') {
                                fieldnames = categs.Last_Run_TS_Field_on_Underwriting_File__c;
                            } else {
                                fieldnames += ',' + categs.Last_Run_TS_Field_on_Underwriting_File__c;
                            }
                        }
                    }
                }
                System.debug('@@@@ fieldnames'+fieldnames);
                
                String docuFields='';
                List<Document_Map_Setting__mdt> documentMap =[Select id,DeveloperName,label,M0_Field_Name_s__c,M1_Field_Name_s__c,M2_Field_Name_s__c,Inspection_Field_Name_s__c,Kitting_Field_Name_s__c,Permit_Field_Name_s__c from Document_Map_Setting__mdt];// Added fields by Deepika on 5/2/2019 as part of 1930
                Map<String,Document_Map_Setting__mdt> docNameMap = new Map<String,Document_Map_Setting__mdt>();
                for (Document_Map_Setting__mdt docuMap : documentMap){
                    docNameMap.put(docuMap.label,docuMap);
                    if(docuMap.M0_Field_Name_s__c != null){
                        for(String st: docuMap.M0_Field_Name_s__c.split(';')){
                            if(!docuFields.contains(st)){
                                if (docuFields == '') {
                                    docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                } else {
                                    docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                }
                            }
                        }
                    }
                    if(docuMap.M1_Field_Name_s__c != null){
                        for(String st: docuMap.M1_Field_Name_s__c.split(';')){
                            if(!docuFields.contains(st)){
                                if (docuFields == '') {
                                    docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                } else {
                                    docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                }
                            }
                        }
                    }
                    if(docuMap.M2_Field_Name_s__c != null){
                        for(String st: docuMap.M2_Field_Name_s__c.split(';')){
                            if(!docuFields.contains(st)){
                                if (docuFields == '') {
                                    docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                } else {
                                    docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                }
                            }
                        }
                    }
                    //Added By Deepika on 5/2/2019 as part of 1930 - Start
                    if(docuMap.Inspection_Field_Name_s__c != null){
                        for(String st: docuMap.Inspection_Field_Name_s__c.split(';')){
                            if(!docuFields.contains(st)){
                                if (docuFields == '') {
                                    docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                } else {
                                    docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                }
                            }
                        }
                    }
                    if(docuMap.Kitting_Field_Name_s__c != null){
                        for(String st: docuMap.Kitting_Field_Name_s__c.split(';')){
                            if(!docuFields.contains(st)){
                                if (docuFields == '') {
                                    docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                } else {
                                    docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                }
                            }
                        }
                    }
                    if(docuMap.Permit_Field_Name_s__c != null){
                        for(String st: docuMap.Permit_Field_Name_s__c.split(';')){
                            if(!docuFields.contains(st)){
                                if (docuFields == '') {
                                    docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                } else {
                                    docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                }
                            }
                        }
                    }
                    //Added By Deepika on 5/2/2019 as part of 1930 - End
                }
                System.debug('@@@@ docuFields'+docuFields);
                //updated below SOQl by Adithya as per 1930
                String queryString = 'Select id,StageName,EDW_Originated__c,SyncedQuoteId,Synced_Quote_Id__c,Synced_Credit_Id__c,M0_Split__c,M1_Split__c,M2_Split__c,Permit_Split__c,Kitting_Split__c,Inspection_Split__c,(Select id,Name,isSyncing from Quotes),(select Id,Name,isSyncing__c,Status__c from Credits__r),(select Id,Opportunity__c,Opportunity__r.Change_Order_Status__c,Opportunity__r.isChangeOrderSubmitted__c,M0_Approval_Requested_Date__c,CO_Approval_Requested_Date__c,M1_Approval_Requested_Date__c,M2_Approval_Requested_Date__c,Opportunity__r.SyncedQuoteId,Project_Status__c,CO_Requested_Date__c,Opportunity__r.Product_Loan_Type__c,'+docuFields+' from Underwriting_Files__r) from Opportunity WHERE Id=\'' + opportunityId + '\'';
                
                System.debug('### queryString==> '+queryString);
                List<Opportunity> oppLst = database.query(queryString);
                
                if(!oppLst.isEmpty())
                {
                    oppRec = oppLst[0];
                    system.debug('### oppRec '+oppRec);
                    if(oppRec.EDW_Originated__c == true){
                        errMsg = ErrorLogUtility.getErrorCodes('218', new List<String>{String.valueOf(oppRec.id)});
                        apprvMilStnRespBean.returnCode = '218';
                        iolist = new list<DataValidator.InputObject>{};
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        apprvMilStnRespBean.error.add(errorMsg);
                        iRestRes = (IRestResponse)apprvMilStnRespBean;
                        res.response = iRestRes;
                        return apprvMilStnRespBean;
                    }else if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                        apprvMilStnRespBean.returnCode = '366';
                        errMsg = ErrorLogUtility.getErrorCodes('366',null);
                    }
                    
                    if(oppRec.Underwriting_Files__r != null)
                    {
                        underwritingRec = oppRec.Underwriting_Files__r[0];
                        
                        //Added By Adithya as per 1930
                        //Start
                        if(null != underwritingRec)
                        {
                             string BoxFieldsqueryString = 'select id,Name,Box_Approval_Letters_TS__c,Box_Final_Designs_TS__c,Box_Installation_Photos_TS__c,Box_Utility_Bills_TS__c,Box_Government_IDs_TS__c,Box_Install_Contracts_TS__c,Box_Loan_Agreements_TS__c,Box_Payment_Election_Form_TS__c,'+fieldnames+' from Box_Fields__c where Underwriting__c =\'' + underwritingRec.Id+ '\'';
                             
                             System.debug('### BoxFieldsqueryString==> '+BoxFieldsqueryString);
                            List<Box_Fields__c> boxFieldLst = database.query(BoxFieldsqueryString);
                            if(!boxFieldLst.isEmpty())
                                boxFieldRec = boxFieldLst[0];
                        }
                        //Stop
                    }
                    
                    Boolean syncedsppcredit = false;
                    if( (null != oppRec.M0_Split__c && reqData.projects[0].milestoneRequest == 'M0') || (null != oppRec.M1_Split__c && reqData.projects[0].milestoneRequest == 'M1') || (null != oppRec.M2_Split__c && reqData.projects[0].milestoneRequest == 'M2') || (null != oppRec.Permit_Split__c && reqData.projects[0].milestoneRequest == 'Permit') || (null != oppRec.Kitting_Split__c && reqData.projects[0].milestoneRequest == 'Kitting') || (null != oppRec.Inspection_Split__c && reqData.projects[0].milestoneRequest == 'Inspection') || reqData.projects[0].milestoneRequest == 'CO')
                    {
                        if(null != oppRec.Credits__r) 
                        {
                            for(SLF_Credit__c creditIterate : oppRec.Credits__r)
                            {
                                if(creditIterate.IsSyncing__c &&(creditIterate.Status__c == 'Auto Approved' || creditIterate.Status__c == 'Manual Approved')){
                                    syncedsppcredit = true;
                                }
                            }
                        }
                        
                        if(!syncedsppcredit || oppRec.SyncedQuoteId == null){
                            errMsg = ErrorLogUtility.getErrorCodes('267',null);
                            apprvMilStnRespBean.returnCode = '267'; 
                        }
                    }else{
                        apprvMilStnRespBean.returnCode = '265';
                        errMsg = ErrorLogUtility.getErrorCodes('265',null);
                    }
                }
               
                if(String.isNotBlank(errMsg)){
                    iolist = new list<DataValidator.InputObject>{};
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    apprvMilStnRespBean.error.add(errorMsg);
                    iRestRes = (IRestResponse)apprvMilStnRespBean;
                    res.response = iRestRes;
                    return apprvMilStnRespBean;
                }
               
                apprvMilStnRespBean.projects = new  List<UnifiedBean.OpportunityWrapper>();
                UnifiedBean.OpportunityWrapper apprvMilStnBean = new UnifiedBean.OpportunityWrapper();
                
                Boolean documentNullCheck = false;
                
                if(null != underwritingRec)
                {
                    Document_Map_Setting__mdt reqDocFields = new Document_Map_Setting__mdt();
                    if(underwritingRec.Opportunity__r.Product_Loan_Type__c != null){
                            reqDocFields =docNameMap.get(underwritingRec.Opportunity__r.Product_Loan_Type__c);
                    }
                    
                    Map<String,String> milestoneFieldMap = new Map<String,String>();
                    milestoneFieldMap.put('M0','M0_Field_Name_s__c');
                    milestoneFieldMap.put('M1','M1_Field_Name_s__c');
                    milestoneFieldMap.put('M2','M2_Field_Name_s__c');
                    milestoneFieldMap.put('CO','M0_Field_Name_s__c');
                    //Added by Deepika as part of 1930 - 5/2/2019 - Start
                    milestoneFieldMap.put('Permit','Permit_Field_Name_s__c');
                    milestoneFieldMap.put('Kitting','Kitting_Field_Name_s__c');
                    milestoneFieldMap.put('Inspection','Inspection_Field_Name_s__c');
                    //Added by Deepika as part of 1930 - 5/2/2019 - End
                    
                    //Srikanth - 03/29 Added below condition as per new requirements Orange-14
                    
                    for(String sn : milestoneFieldMap.keyset())
                    {
                        if(milestoneFieldMap.containsKey(sn) && reqDocFields.get(milestoneFieldMap.get(sn)) != null){//Added by Deepika to avoid exceptions
                            String docTypeCheck= (String)reqDocFields.get(milestoneFieldMap.get(sn));                        
                            if(docTypeCheck != null){//Added by Deepika to handle exception for product type HII and HIS
                                for(String st: docTypeCheck.split(';')){
                                    String fieldName=(String)underwritingRec.getSobject('Opportunity__r').getSobject('Installer_Account__r').get(st);
                                    if(String.isNotBlank(fieldName)){                               
                                        documentNullCheck = true;                                    
                                    }
                                }
                            }
                        }
                    }
                    
                    if(reqData.projects[0].milestoneRequest == 'M0' || reqData.projects[0].milestoneRequest == 'M1' || reqData.projects[0].milestoneRequest == 'M2' || reqData.projects[0].milestoneRequest == 'Permit' || reqData.projects[0].milestoneRequest == 'Kitting' || reqData.projects[0].milestoneRequest == 'Inspection' ||  reqData.projects[0].milestoneRequest == 'CO')
                    {
                        if(reqDocFields.get(milestoneFieldMap.get(reqData.projects[0].milestoneRequest)) != null){
                            System.debug('### docuMap'+reqDocFields.get(milestoneFieldMap.get(reqData.projects[0].milestoneRequest)));
                            String docType= (String)reqDocFields.get(milestoneFieldMap.get(reqData.projects[0].milestoneRequest));
                            if(docType != null){//Added by Deepika to avoid exception
                                for(String st: docType.split(';')){
                                    String fieldName=(String)underwritingRec.getSobject('Opportunity__r').getSobject('Installer_Account__r').get(st);
                                    System.debug('### fieldName'+fieldName);
                                    if(String.isBlank(fieldName)){                              

                                    }else{
                                        if(null != boxFieldRec)//Added By Adithya for 1930
                                        {
                                            for(String doc: fieldName.split(';'))
                                            {
                                                for (SF_Category_Map__c categs : SF_Category_Map__c.getAll().values())
                                                {
                                                    System.debug('### categs.Folder_Name__c==>'+categs.Folder_Name__c);
                                                    System.debug('### doc==>'+doc);
                                                    if(categs.Folder_Name__c == doc){
                                                        System.debug('### boxFieldRec.get(categs.TS_Field_On_Underwriting__c) '+boxFieldRec.get(categs.TS_Field_On_Underwriting__c));
                                                        if(boxFieldRec.get(categs.TS_Field_On_Underwriting__c)!= null)
                                                        {
                                                            break;
                                                        } 
                                                        else
                                                        {
                                                            missingList.add(doc);
                                                        }
                                                    }
                                                    
                                                }
                                                if(doc == 'Loan Agreements' && underwritingRec.Opportunity__r.Change_Order_Status__c =='Pending'){
                                                    missingList.add(doc);
                                                }
                    
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }                   
                    else{
                        apprvMilStnRespBean.returnCode = '265';
                        errMsg = ErrorLogUtility.getErrorCodes('265',null);
                        System.debug('### returnCode==>'+apprvMilStnRespBean.returnCode);
                    }
                    if(!documentNullCheck)
                    {
                        apprvMilStnRespBean.returnCode = '260';
                        errMsg = ErrorLogUtility.getErrorCodes('260',null);
                    }
                    
                     if(String.isNotBlank(errMsg)){
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        apprvMilStnRespBean.error.add(errorMsg);
                        iRestRes = (IRestResponse)apprvMilStnRespBean;
                        res.response = iRestRes;
                        return apprvMilStnRespBean;
                    }
                    
                    if(missingList.size() > 0){
                        system.debug('### missingList '+missingList);
                        apprvMilStnRespBean.returnCode = '298';
                        apprvMilStnBean.message = ErrorLogUtility.getErrorCodes('298', new List<String>{String.valueOf(missingList)});
                        apprvMilStnRespBean.projects.add(apprvMilStnBean);
                    }else if(reqData.projects[0].milestoneRequest == 'M0'){
                        underwritingRec.M0_Approval_Requested_Date__c = System.now();
                        apprvMilStnRespBean.returnCode = '200';
                    }else if(reqData.projects[0].milestoneRequest == 'CO' && underwritingRec.CO_Requested_Date__c != null){
                        underwritingRec.CO_Approval_Requested_Date__c = System.now();
                        apprvMilStnRespBean.returnCode = '200';
                    }else if(reqData.projects[0].milestoneRequest == 'Permit'){ //Added By Adithya as per 1930
                        underwritingRec.Permit_Approval_Requested_Date__c = System.now();
                        apprvMilStnRespBean.returnCode = '200';
                    }else if(reqData.projects[0].milestoneRequest == 'Kitting'){ //Added By Adithya as per 1930
                        underwritingRec.Kitting_Approval_Requested_Date__c = System.now();
                        apprvMilStnRespBean.returnCode = '200';
                    }else if(reqData.projects[0].milestoneRequest == 'Inspection'){ //Added By Adithya as per 1930
                        underwritingRec.Inspection_Approval_Requested_Date__c = System.now();
                        apprvMilStnRespBean.returnCode = '200';
                    }else if(reqData.projects[0].milestoneRequest == 'M1'){
                        if(underwritingRec.Opportunity__r.SyncedQuoteId != null)
                        {
                            Quote quoteRec = [Select id,System_Design__c from Quote where id=: underwritingRec.Opportunity__r.SyncedQuoteId];
                            if(quoteRec.System_Design__c!=null){
                                List<System_Design__c> sysRec =  [Select id,Name,Battery_Capacity__c,Battery_Count__c,M1_Request__c,Project_Type__c,
                                                                    BatteryMake__c,Battery_Model__c,Est_Annual_Production_kWh__c,Ground_Mount_Type__c,
                                                                    Inverter_Count__c,InverterMake__c,Inverter_Model__c,Module_Count__c,ModuleMake__c,
                                                                    Module_Model__c,Tilt__c,Opportunity__c,System__c
                                                                    from System_Design__c where id= :quoteRec.System_Design__c ];
                                system.debug('### sysRec '+sysRec[0]);
                                if(!sysRec.isEmpty())
                                {
                                    if(underwritingRec.Opportunity__r.Product_Loan_Type__c != 'Battery Only' && underwritingRec.Opportunity__r.Product_Loan_Type__c != 'Non-PV Stand Alone Battery')
                                    {
                                        iolist.add (new DataValidator.InputObject (sysRec[0],'FNI Credit'));
                                    }
                                    else{
                                        iolist.add (new DataValidator.InputObject (sysRec[0],'Battery'));
                                    }
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,'');
                                    if(string.isNotBlank(errorMsg.errorMessage))
                                    {
                                        errMsg = errorMsg.errorMessage;
                                        apprvMilStnRespBean.error.add(errorMsg);
                                        apprvMilStnRespBean.returnCode = '207';
                                        iRestRes = (IRestResponse)apprvMilStnRespBean;
                                        res.response = iRestRes;
                                        return apprvMilStnRespBean;
                                    }
                                    if(sysRec[0].Project_Type__c != null){  
                                        if(sysRec[0].Project_Type__c == 'Ground Mount PV' && sysRec[0].Ground_Mount_Type__c == null)
                                        {
                                            apprvMilStnRespBean.returnCode = '314';
                                            errMsg = ErrorLogUtility.getErrorCodes('314', null);
                                        }else if(sysRec[0].Project_Type__c == 'Rooftop PV' && sysRec[0].Ground_Mount_Type__c != null)
                                        {
                                            apprvMilStnRespBean.returnCode = '315';
                                            errMsg = ErrorLogUtility.getErrorCodes('315', null);
                                        }
                                        if(String.isNotBlank(errMsg)){
                                            iolist = new list<DataValidator.InputObject>{};
                                            UnifiedBean.errorWrapper errorMsg1 = SLFUtility.parseDataValidator(iolist,errMsg);
                                            apprvMilStnRespBean.error.add(errorMsg1);
                                            iRestRes = (IRestResponse)apprvMilStnRespBean;
                                            res.response = iRestRes;
                                            return apprvMilStnRespBean;
                                        }
                                    }
                                }
                            }           
                        }
                        //underwritingRec.M1_Approval_Requested_Date__c = Date.TODAY();
                        underwritingRec.M1_Approval_Requested_Date__c = System.now();
                        apprvMilStnRespBean.returnCode = '200';
                    }else if(reqData.projects[0].milestoneRequest == 'M2'){
                        if(underwritingRec.Opportunity__r.SyncedQuoteId != null)
                        {
                            Quote quoteRec = [Select id,System_Design__c from Quote where id=: underwritingRec.Opportunity__r.SyncedQuoteId];
                            if(quoteRec.System_Design__c!=null){
                                List<System_Design__c> sysRec =  [Select id,Name,Battery_Capacity__c,Battery_Count__c,M1_Request__c,Project_Type__c,
                                                                    BatteryMake__c,Battery_Model__c,Est_Annual_Production_kWh__c,Ground_Mount_Type__c,
                                                                    Inverter_Count__c,InverterMake__c,Inverter_Model__c,Module_Count__c,ModuleMake__c,
                                                                    Module_Model__c,Tilt__c,Opportunity__c,System__c
                                                                    from System_Design__c where id= :quoteRec.System_Design__c ];
                                system.debug('### sysRec '+sysRec[0]);
                                if(!sysRec.isEmpty())
                                {
                                    if(underwritingRec.Opportunity__r.Product_Loan_Type__c != 'Battery Only'&& underwritingRec.Opportunity__r.Product_Loan_Type__c != 'Non-PV Stand Alone Battery')
                                    {
                                        iolist.add (new DataValidator.InputObject (sysRec[0],'FNI Credit'));
                                    }else{
                                        //iolist.add (new DataValidator.InputObject (sysRec[0],'Battery'));
                                        
                                    }
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,'');
                                    if(string.isNotBlank(errorMsg.errorMessage))
                                    {
                                        errMsg = errorMsg.errorMessage;
                                        apprvMilStnRespBean.error.add(errorMsg);
                                        apprvMilStnRespBean.returnCode = '207';
                                        iRestRes = (IRestResponse)apprvMilStnRespBean;
                                        res.response = iRestRes;
                                        return apprvMilStnRespBean;
                                    }
                                    if(sysRec[0].Project_Type__c != null){  
                                        if(sysRec[0].Project_Type__c == 'Ground Mount PV' && sysRec[0].Ground_Mount_Type__c == null)
                                        {
                                            apprvMilStnRespBean.returnCode = '314';
                                            errMsg = ErrorLogUtility.getErrorCodes('314', null);
                                        }else if(sysRec[0].Project_Type__c == 'Rooftop PV' && sysRec[0].Ground_Mount_Type__c != null)
                                        {
                                            apprvMilStnRespBean.returnCode = '315';
                                            errMsg = ErrorLogUtility.getErrorCodes('315', null);
                                        }
                                        if(String.isNotBlank(errMsg)){
                                            iolist = new list<DataValidator.InputObject>{};
                                            UnifiedBean.errorWrapper errorMsg1 = SLFUtility.parseDataValidator(iolist,errMsg);
                                            apprvMilStnRespBean.error.add(errorMsg1);
                                            iRestRes = (IRestResponse)apprvMilStnRespBean;
                                            res.response = iRestRes;
                                            return apprvMilStnRespBean;
                                        }
                                    }
                                }
                            }           
                        }
                        //underwritingRec.M2_Approval_Requested_Date__c = Date.TODAY();
                        underwritingRec.M2_Approval_Requested_Date__c = System.now();
                        apprvMilStnRespBean.returnCode = '200';
                    }else{ 
                       apprvMilStnRespBean.returnCode = '265';
                       errMsg = ErrorLogUtility.getErrorCodes('265', null);
                       System.debug('### returnCode==>'+apprvMilStnRespBean.returnCode);
                    }
                    update underwritingRec;
                } else{
                    apprvMilStnRespBean.returnCode = '281';
                    errMsg = ErrorLogUtility.getErrorCodes('281', null);
                }
                if(apprvMilStnRespBean.returnCode == '200')
                {
                     system.debug('### apprvMilStnRespBean.returnCode ' +apprvMilStnRespBean.returnCode);
                    
                    apprvMilStnRespBean = SLFUtility.getUnifiedBean(new set<Id>{underwritingRec.Opportunity__c},'Orange',false);
                    if(!apprvMilStnRespBean.projects.isEmpty())
                    {
                        
                        for(UnifiedBean.OpportunityWrapper oppIterate : apprvMilStnRespBean.projects)
                        {
                            if(reqData.projects[0].milestoneRequest == 'CO') //Srikanth- 06/10/2019 - Orange-1877
                            {
                                oppIterate.message = 'You have successfully submitted for Change Order approval.';
                            }
                            else
                            {
                                oppIterate.message = 'Success! You have submitted for milestone approval.'; //'Your request has been submitted' 
                            }
                        }
                    }
                    apprvMilStnRespBean.returnCode = '200';
                    iRestRes = (IRestResponse)apprvMilStnRespBean;
                    res.response = iRestRes;
                   
                }
                system.debug('###..response'+res.response);              
                iRestRes = (IRestResponse)apprvMilStnRespBean;
                res.response = iRestRes;
                system.debug('###..response'+res.response);   
            } 
        }catch(Exception err){
            system.debug('### ApproveMilestoneServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
             ErrorLogUtility.writeLog(' ApproveMilestoneServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
           }
        if(String.isNotBlank(errMsg)){
            //apprvMilStnRespBean.returnCode = '400';
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            apprvMilStnRespBean.error.add(errorMsg);
            iRestRes = (IRestResponse)apprvMilStnRespBean;
            res.response = iRestRes;
        }
       
        system.debug('### Exit from fetchData() of ' + ApproveMilestoneServiceImpl.class);
        
        return apprvMilStnRespBean;
        
    }
}