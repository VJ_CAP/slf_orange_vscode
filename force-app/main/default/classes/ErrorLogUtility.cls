/**
* Description: Utility class contains all CRM relation funtionality which can be used globally.
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/27/2017          Created 
    Anurag Srivastava       06/21/2017          Updated
******************************************************************************************/
public without sharing class ErrorLogUtility {
    
    /**
    * Description: Writes the ErrorLog
    *
    * @param funcName    - Functionality Name
    * @param methodName  - Method Name
    * @param lineNumber  - Error Line Number
    * @param description - Error Description
    * @param errType     - Type of Exception/Error
    */
    public static void writeLog(String funcName, Integer lineNumber, String methodName, String description, String errType, String recIdStr){
        try
        {             
            system.debug('### Entered into writeLog() of '+ ErrorLogUtility.class);
            Error_Log__c errorLog = new Error_Log__c();
            errorLog.Functionality__c= funcName;
            errorLog.Method_Name__c= methodName;  
            errorLog.Line_Number__c = lineNumber;
            errorLog.RecordIds__c = recIdStr;
            if(description.length() > 32767)
            {
                errorLog.Message__c = description.left(32760) + '...';    
            }
            else
            {
                errorLog.Message__c = description;
            }
            errorLog.Type__c = errtype;
            Database.upsert(errorLog); //Writing the error log in Error_Log__c object
        }
        catch(Exception ex)
        {
            //ErrorLogUtility.writeLog('ErrorLogUtility.writeLog()',ex.getLineNumber(),'writeLog()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error');
            system.debug('### Exception in ErrorLogUtility.writeLog():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
        }
        system.debug('### Exit from ErrorLogUtility.writeLog() of '+ ErrorLogUtility.class);
    }
   
    /*
    *   Description: This Method used to write Error Log on Insert Operation.
    *       -It captures List of SaveResults 
    *       -Iterate them to find the failed records
    *       -insert Error Log details on Error_Log__c objects
    *   @param objectsToInsert type Database.SaveResult
    */
    
    public static void writeInsertLog(List<Database.SaveResult> objectsToInsert)
    {
        try
        {
            system.debug('### Entered into writeInsertLog() of '+ ErrorLogUtility.class);
            String errMsg='';    
            Integer rowCount = 0;
    
            //Iterate through each SaveResult's
            for (Database.SaveResult sr : objectsToInsert)
            {
                //Getting failed records
                if (!sr.isSuccess())
                {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) 
                    {
                        errMsg += 'Row:'+ rowCount +' Fields:'+ err.getFields() + ' Error:'+err.getMessage()+'\n';
                    }
                }
                rowCount++;
            }
            
            //Send error log details to writeLog method to insert on Error_Log__c object
            if(string.isNotBlank(errMsg))
                writeLog('',0,'writeInsertLog()',errMsg,'Error',getErrorRecIds(null,objectsToInsert,null,null,null));  
           
        }
        catch(Exception ex)
        {
            ErrorLogUtility.writeLog('ErrorLogUtility.writeInsertLog()',ex.getLineNumber(),'writeInsertLog()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',getErrorRecIds(ex,null,null,null,null));
            system.debug('### Exception in ErrorLogUtility.writeInsertLog():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
        }
        system.debug('### Exit from ErrorLogUtility.writeInsertLog() of '+ ErrorLogUtility.class);
    }
    
    
   /*
    *   Description: This Method used to write Error Log on Update Operation.
    *       -It captures List of SaveResults 
    *       -Iterate them to find the failed records
    *       -insert Error Log details on Error_Log__c objects
    *   @param objectsToUpdate type Database.SaveResult
    */
    
    public static void writeUpdateLog(List<Database.SaveResult> objectsToUpdate)
    {
        try
        {
            system.debug('### Entered into writeUpdateLog() of '+ ErrorLogUtility.class);
            String errMsg='';                
            Integer rowCount = 0;
    
            //For loop to iterate through each result
            for (Database.SaveResult sr : objectsToUpdate)
            {
                //To check record failed or not
                if (!sr.isSuccess())
                {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) 
                    {
                        errMsg += 'Row:'+ rowCount +' Fields:'+ err.getFields() + ' Error:'+err.getMessage()+'\n';
                    }
                }
                rowCount++;
            }
            
            //Send error log details to writeLog method to update on Error_Log__c object
            if(string.isNotBlank(errMsg))
                writeLog('',0,'writeUpdateLog',errMsg,'error',getErrorRecIds(null,objectsToUpdate,null,null,null)); 
            
        }
        catch(Exception ex)
        {
            ErrorLogUtility.writeLog('ErrorLogUtility.writeUpdateLog()',ex.getLineNumber(),'writeUpdateLog()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',getErrorRecIds(ex,null,null,null,null));            system.debug('### Exception in ErrorLogUtility.writeUpdateLog():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
        }
        system.debug('### Exit from ErrorLogUtility.writeUpdateLog() of '+ ErrorLogUtility.class);
    }
    
   
   /*
    *   Description: This Method used to write Error Log on Upsert Operation.
    *       -It captures List of UpsertResult 
    *       -Iterate them to find the failed records
    *       -insert Error Log details on Error_Log__c objects
    *   @param objectsToUpsert type Database.UpsertResult
    */

    
    public static void writeUpsertLog(List<Database.UpsertResult> objectsToUpsert)
    {
        try
        {
            system.debug('### Entered into writeUpsertLog() of '+ ErrorLogUtility.class);
            String errMsg='';               
            Integer rowCount = 0;
    
            //For loop to iterate through each result
            for (Database.UpsertResult sr : objectsToUpsert)
            {
                //To check record failed or not
                if (!sr.isSuccess())
                {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) 
                    {
                        errMsg += 'Row:'+ rowCount +' Fields:'+ err.getFields() + ' Error:'+err.getMessage()+'\n';
                    }
                }
                rowCount++;
            }
            //Send error log details to writeLog method to upsert on Error_Log__c object
            if(string.isNotBlank(errMsg))
                writeLog('',0,'writeUpsertLog',errMsg,'Error',getErrorRecIds(null,null,objectsToUpsert,null,null)); 
            
        }
        catch(Exception ex)
        {
            ErrorLogUtility.writeLog('ErrorLogUtility.writeUpsertLog()',ex.getLineNumber(),'writeUpsertLog()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',getErrorRecIds(ex,null,null,null,null));
            system.debug('### Exception in ErrorLogUtility.writeUpsertLog():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
        }
        system.debug('### Exit from ErrorLogUtility.writeUpsertLog() of '+ ErrorLogUtility.class);
    }
    
    
    /*
    *   Description: This Method used to write Error Log on Delete Operation.
    *       -It captures List of DeleteResult 
    *       -Iterate them to find the failed records
    *       -insert Error Log details on Error_Log__c objects
    *   @param objectsToDelete type Database.Delete
    */
    
    public static void writeDeleteLog(List<Database.DeleteResult> objectsToDelete)
    {
        try
        {
            system.debug('### Entered into writeDeleteLog() of '+ ErrorLogUtility.class);
            String errMsg='';
            Integer rowCount = 0;
    
            //For loop to iterate through each result
            for (Database.DeleteResult sr : objectsToDelete)
            {
                //To check record failed or not
                if (!sr.isSuccess())
                {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) 
                    {
                        errMsg += 'Row:'+ rowCount +' Fields:'+ err.getFields() + ' Error:'+err.getMessage()+'\n';
                    }
                }
                rowCount++;
            }
            
            //Send error log details to writeLog method to delete on Error_Log__c object
            if(string.isNotBlank(errMsg))
                writeLog('',0,'writeDeleteLog',errMsg,'error',getErrorRecIds(null,null,null,objectsToDelete,null));     
            
        }
        catch(Exception ex)
        {
            ErrorLogUtility.writeLog('ErrorLogUtility.writeDeleteLog()',ex.getLineNumber(),'writeDeleteLog()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',getErrorRecIds(ex,null,null,null,null));
            system.debug('### Exception in ErrorLogUtility.writeDeleteLog():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
        }
        system.debug('### Exit from ErrorLogUtility.writeDeleteLog() of '+ ErrorLogUtility.class);
    }
    
    
    /*
    *   Description: This Method used to write Error Log on Undelete Operation.
    *       -It captures List of UndeleteResult 
    *       -Iterate them to find the failed records
    *       -insert Error Log details on Error_Log__c objects
    *   @param objectsToUnDelete type Database.UndeleteResult
    */
    
    public static void writeUnDeleteLog(List<Database.UndeleteResult> objectsToUnDelete)
    {
        try
        {
            system.debug('### Entered into writeUnDeleteLog() of '+ ErrorLogUtility.class);
            String errMsg='';
            Integer rowCount = 0;
    
            //For loop to iterate through each result
            for (Database.UndeleteResult sr : objectsToUnDelete)
            {
                //To check record failed or not
                if (!sr.isSuccess())
                {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) 
                    {
                        errMsg += 'Row:'+ rowCount +' Fields:'+ err.getFields() + ' Error:'+err.getMessage()+'\n';
                    }
                }
                rowCount++;
            }
            
            //Send error log details to writeLog method to UnDelete on Error_Log__c object
            if(string.isNotBlank(errMsg))
                writeLog('',0,'writeUnDeleteLog',errMsg,'error',getErrorRecIds(null,null,null,null,objectsToUnDelete));     
            
        }
        catch(Exception ex)
        {
            ErrorLogUtility.writeLog('ErrorLogUtility.writeUnDeleteLog()',ex.getLineNumber(),'writeUnDeleteLog()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',getErrorRecIds(ex,null,null,null,null));
            system.debug('### Exception in ErrorLogUtility.writeUnDeleteLog():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
        }
        system.debug('### Exit from ErrorLogUtility.writeUnDeleteLog() of '+ ErrorLogUtility.class);
    }
    
    
    /*
    *   WriteFutureLog() 
    * @param funcName    - Functionality Name
    * @param lineNumber  - Line Number
    * @param methodName  - Method Name
    * @param description - Error Description
    * @param errType     - Type of Exception/Error
    */
    @future
    public static void writeFutureLog(String funcName, Integer lineNumber, String methodName,String description, String errType){
        try
        {
            Error_Log__c errorLog = new Error_Log__c();
            errorLog.Functionality__c = funcName;  
            errorLog.Method_Name__c = methodName;  
            errorLog.Line_Number__c = lineNumber;
            if(description.length() > 32767)
            {
                errorLog.Message__c = description.left(32760) + '...';    
            }
            else
            {
                errorLog.Message__c = description;
            }
            errorLog.Type__c = errtype;
            Database.upsert(errorLog); //Writing the error log in Error_Log__c object
        }
        catch(Exception ex)
        {
            ErrorLogUtility.writeLog('ErrorLogUtility.writeFutureLog()',ex.getLineNumber(),'writeFutureLog()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',getErrorRecIds(ex,null,null,null,null));
            system.debug('### Exception in ErrorLogUtility.writeFutureLog():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
        }
    }  
    public static String getErrorRecIds(Exception e, 
                                        List<Database.SaveResult> srList, 
                                        List<Database.UpsertResult> urList, 
                                        List<Database.DeleteResult> drList, 
                                        List<Database.UndeleteResult> udrList){
       system.debug('### Entered into getErrorRecIds() of '+ ErrorLogUtility.class);
       try{                                                                                           
            String recIds = '';
            if(e != null){
                if(e.getNumDml() != null){
                    for(Integer i=0;i<e.getNumDml();i++){
                        if(e.getDmlId(i)!=null){
                            if(recIds == '')
                                recIds+=e.getDmlId(i);
                            else
                                recIds+=', '+e.getDmlId(i);
                        }
                    }
                }
            }
            else if(srList!=null && !srList.isEmpty()){
                for(Database.SaveResult sr : srList){
                    if(sr.getId()!=null){
                        if(recIds == '')
                            recIds+=sr.getId();
                        else
                            recIds+=', '+sr.getId();
                    }
                }
            }
            else if(urList!=null && !urList.isEmpty()){
                for(Database.UpsertResult ur : urList){
                    if(ur.getId()!=null){
                        if(recIds == '')
                            recIds+=ur.getId();
                        else
                            recIds+=', '+ur.getId();
                    }
                }
            }
           else if(drList!=null && !drList.isEmpty()){
                for(Database.DeleteResult dr : drList){
                    if(dr.getId()!=null){
                        if(recIds == '')
                            recIds+=dr.getId();
                        else
                            recIds+=', '+dr.getId();
                    }
                }
            }
            else if(udrList!=null && !udrList.isEmpty()){
                for(Database.UndeleteResult udr : udrList){
                    if(udr.getId()!=null){
                        if(recIds == '')
                            recIds+=udr.getId();
                        else
                            recIds+=', '+udr.getId();
                    }
                }
            }
            system.debug('### Exit from getErrorRecIds() of '+ ErrorLogUtility.class);
            return (recIds!=null?recIds:'');
       }catch(Exception ex){
           system.debug('### Exit from getErrorRecIds() of '+ ErrorLogUtility.class);
           return '';
       }
       system.debug('### Exit from getErrorRecIds() of '+ ErrorLogUtility.class);
       return '';
    }
      
    public static String getErrorCodes(String errorCode, List<String> msgStrList)
    {
        String errorMsg = '';
        if(SLF_Error_Codes__c.getValues(errorCode) != null){
            String errMsg1 = SLF_Error_Codes__c.getValues(errorCode).Error_Message__c;
            if(String.isNotBlank(errMsg1))
                errorMsg += errMsg1.trim();
            String errMsg2 = SLF_Error_Codes__c.getValues(errorCode).Error_Message_2__c;
            if(String.isNotBlank(errMsg2))
                errorMsg += ' ' + errMsg2.trim();
            
            if(msgStrList != null && !msgStrList.isEmpty()){
                for(Integer i=0; i < msgStrList.size(); i++){
                    String placeHldr = '##'+i+'##';
                    System.debug('placeHldr::'+placeHldr);
                    if(errorMsg.contains(placeHldr)){
                        System.debug('2');
                        errorMsg = errorMsg.replace(placeHldr, msgStrList[i]);
                        System.debug('errorMsg::'+errorMsg);
                    }
                }              
            }
        }
        
        return errorMsg;
    }
    
}