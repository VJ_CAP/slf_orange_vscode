/**
* Description: Prequalv2 API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Deepika                06/26/2018             Created
******************************************************************************************/
public with sharing class PrequalServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + PrequalServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        PrequalDataServiceImpl prequalDataServiceImplObj = new PrequalDataServiceImpl(); 
        IRestResponse iRestRes;
        
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            
            res.response = prequalDataServiceImplObj.transformOutput(reqData);

        }catch(Exception err){
           
            system.debug('### PrequalServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' PrequalServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            
            UnifiedBean unifbean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214',null);
            errorWrapperLst.add(errorWrapper);
            unifbean.error  = errorWrapperLst ;
            unifbean.returnCode = '214';
            iRestRes = (IRestResponse)unifbean;
            res.response= iRestRes;
        }
       
        system.debug('### Exit from fetchData() of ' + PrequalServiceImpl.class);
        
        return res.response ;
    }
    
    
}