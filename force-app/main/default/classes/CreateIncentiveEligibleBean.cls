/**
* Description: CreateIncentiveCouldBeEligibleBean class is used to display response in JSON format.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public class CreateIncentiveEligibleBean implements IRestRequest,IRestResponse {

    public String returnCode {get;set;}
    public String quoteName {get;set;}
    public String product {get;set;}
    public List<String> amortizationTable {get;set;}
    public Double finalMonthlyPayment {get;set;}
    public Double finalEscalatedMonthlyPayment {get;set;}
    public String message {get;set;}
    public Double monthlyPayment {get;set;}
    public List<String> prepaymentAmortizationTable {get;set;}
    public Double escalatedMonthlyPayment {get;set;}
    public List<ErrorWrapper> error {get; set;}
    
    public class ErrorWrapper{
        public String errorMessage{get; set;} 
    }
    
}