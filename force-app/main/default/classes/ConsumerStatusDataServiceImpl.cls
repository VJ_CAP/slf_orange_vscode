/**
* Description: Consumer Status API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           11/19/2019          Created
******************************************************************************************/

public with sharing class ConsumerStatusDataServiceImpl extends RestDataServiceBase{
    
   /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        UnifiedBean unifBean = new UnifiedBean(); 
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj; 
        system.debug('### reqData '+reqData);
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        UnifiedBean respData = new UnifiedBean();
        respData.error = new List<UnifiedBean.ErrorWrapper>();
        IRestResponse iRestRes;
        try{
            system.debug('### Entered into transformOutput() of '+ConsumerStatusDataServiceImpl.class);
            
            string errMsg = '';
            if(String.isNotBlank(reqData.id) && String.isNotBlank(reqData.hashId))
            {
                errMsg = ErrorLogUtility.getErrorCodes('612',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respData.error.add(errorMsg);
                            respData.returnCode = '612';
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
            }
            String opportunityId;
            
            if(!reqData.portalType.equalsIgnoreCase('Consumer')){
            errMsg = ErrorLogUtility.getErrorCodes('214',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respData.error.add(errorMsg);
                            respData.returnCode = '214';
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
            
            }
            if(String.isNotBlank(reqData.id))
            {
                List<Opportunity> oppLst1 = [select id,Name,Hash_Id__c from opportunity where id=: reqData.id];
                if(!oppLst1.isEmpty()){
                
                   opportunityId = oppLst1[0].id;
                }
               else{
                 errMsg = ErrorLogUtility.getErrorCodes('613',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respData.error.add(errorMsg);
                            respData.returnCode = '613';
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
                }   
                
            }else if(String.isNotBlank(reqData.hashId))
            {
                List<Opportunity> oppLst = [select id,Name,Hash_Id__c from opportunity where Hash_Id__c =: reqData.hashId];
                if(!oppLst.isEmpty()){
                
                   opportunityId = oppLst[0].id;
                }
               else{
                 errMsg = ErrorLogUtility.getErrorCodes('613',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respData.error.add(errorMsg);
                            respData.returnCode = '613';
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
                }   
            }
            if(string.isNotBlank(opportunityId))
            {
                List<SLF_Credit__c>  creditList = [select Id,Name,Status__c from SLF_Credit__c where Opportunity__c =: opportunityId AND status__c =: 'Auto Approved' AND IsSyncing__c = true];
                
                if(creditList.isEmpty())
                {
                    List<Quote> quoteList = [select Id,Name,NonACH_APR__c, SLF_Product__r.Expiration_Date__c,OpportunityId,Opportunity.Installer_Account__r.Risk_Based_Pricing__c,Opportunity.Installer_Account__r.Credit_Waterfall__c from Quote where OpportunityId =: opportunityId AND IsSyncing = true];
                    
                    if(!quoteList.isEmpty())
                    {   
                        if(quoteList[0].SLF_Product__r.Expiration_Date__c < system.today() && quoteList[0].Opportunity.Installer_Account__r.Risk_Based_Pricing__c == false && quoteList[0].Opportunity.Installer_Account__r.Credit_Waterfall__c == false)
                        {
                           errMsg = ErrorLogUtility.getErrorCodes('362',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respData.error.add(errorMsg);
                            respData.returnCode = '362';
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
                        }
                    }
                }
                //Set<id> oppIds = new Set<id>();
                //oppIds.add(opportunityId);
                
                unifBean = SLFUtility.getUnifiedBean(new set<id>{opportunityId},'Consumer',false);
                unifBean.returnCode = '200';
                
            }
           
        }catch(Exception err){
            system.debug('### CustomerStatusDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('CustomerStatusDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null)); 
        }    
         system.debug('### Exit from transformOutput() of '+ ConsumerStatusDataServiceImpl.class);
         return unifBean;
    }
}