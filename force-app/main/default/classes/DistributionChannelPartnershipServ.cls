/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkat               05/07/2019              Created
******************************************************************************************/
global class DistributionChannelPartnershipServ {

     webservice static String ClearFundingDataVlaue(Id fundingId){
        List<Funding_Data__c> lstFD = [select Id from Funding_Data__c where Id=:fundingId];
        for(Funding_Data__c objF:lstFD ){
            objF.Equipment_Invoice_Amount__c = null;
        }
        if(!lstFD.isEmpty()){
            update lstFD;
        }
        return '';
    }
    // webservice method
    webservice static String createStipData(Id underWritingId,String stipName){
        String response=''; 
        try{
            if(String.isNotBlank(underWritingId) && String.isNotBlank(stipName)){
                List<Underwriting_File__c> UnderwritingLst = [SELECT Id,Opportunity__c,Opportunity__r.Combined_Loan_Amount__c,Opportunity__r.CL_Address_Chk__c,Opportunity__r.Account.Name,Opportunity__r.Installer_Account__r.Installer_Email__c,Name,(SELECT id,Name,Status__c FROM Stipulations__r WHERE Stipulation_Data__r.Name=:stipName And Status__c='In Progress'),(SELECT id,Equipment_Order_Number__c,Equipment_Order_Date_Received__c,Equipment_Order_Amount__c FROM Funding_Data__r) FROM 
                                                              Underwriting_File__c WHERE Id =:underWritingId ]; 
                
                // Check for exisitng stip records
                if(UnderwritingLst.size()>0){
                    
                    String query='';
                    List<Stipulation__c> StipList = new List<Stipulation__c>();
                    List<Stipulation_Data__c> stipData =  new List<Stipulation_Data__c>();
                    query += 'Select id,Name,CL_Code__c,POS_Message__c,Email_Template_Name__c,Email_Text__c,Insert_New_Stip__c from Stipulation_Data__c where Name=:stipName';
                    if(!String.isBlank(query)){        
                        stipData = database.query(query);
                    } 
                    
                    if(!stipData.isEmpty()){
                        Stipulation__c newStip = new Stipulation__c();
                        newStip.Underwriting__c = underWritingId;
                        newStip.Status__c = 'New';
                        newStip.Stipulation_Data__c = stipData[0].Id;
                        StipList.add(newStip);
                        system.debug('### StipList'+StipList);
                        if(!StipList.isEmpty()){
                            insert StipList;
                            if(null!=stipData[0].Email_Text__c && null!=stipData[0].Email_Template_Name__c)
                                sendPaymentEmial(stipData[0],UnderwritingLst[0]);
                            response='Stip Created successfully and email has been sent to installer';
                        }
                    }else{
                        response='Stip data not found for the selected stip';
                    }  
                }
                
            }else{
                response='Required input is missing';
            }
            
        }catch(exception err){
            System.debug('Exception ==>:'+err.getMessage()+'### at Line==>:'+err.getLineNumber());
            response=err.getMessage();
            ErrorLogUtility.writeLog(' DistributionChannelPartnershipServ.createStipData()',err.getLineNumber(),'createStipData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
        }
        
        return response;
    }
    public static void sendPaymentEmial(Stipulation_Data__c stipData,Underwriting_File__c underWrtingRec){
        try{
            
            List<EmailTemplate> template = [SELECT Id,Subject, DeveloperName,HtmlValue,Body from EmailTemplate where name =:stipData.Email_Template_Name__c LIMIT 1];
            List<OrgWideEmailAddress> senderEmail = [select id,DisplayName, Address from OrgWideEmailAddress where DisplayName ='Sunlight Financial Support' LIMIT 1];
            //  Create a master list to hold the emails
            List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
            if(template.size()>0 && null!=underWrtingRec.Opportunity__r.Installer_Account__r.Installer_Email__c){
                String stipEmailData=stipData.Email_Text__c;
                stipEmailData=stipEmailData.replace('[opportunity.CL_Address_Chk__c]',''+underWrtingRec.Opportunity__r.CL_Address_Chk__c);
                stipEmailData=stipEmailData.replace('[opportunity.Account.Name]',underWrtingRec.Opportunity__r.Account.Name);
                if(null!=underWrtingRec.Opportunity__r.Combined_Loan_Amount__c){
                    stipEmailData=stipEmailData.replace('[opportunity.Combined_Loan_Amount__c * 0.6]',String.valueOf(underWrtingRec.Opportunity__r.Combined_Loan_Amount__c*0.6));
                }else{
                    stipEmailData=stipEmailData.replace('[opportunity.Combined_Loan_Amount__c * 0.6]','');   
                }
                
                if(!underWrtingRec.Funding_Data__r.isEmpty()){
                    stipEmailData=stipEmailData.replace('[fd.Equipment_Order_Number]',''+underWrtingRec.Funding_Data__r[0].Equipment_Order_Number__c);
                    stipEmailData=stipEmailData.replace('[fd.Equipment_Order_Amount]',''+underWrtingRec.Funding_Data__r[0].Equipment_Order_Amount__c);
                    stipEmailData=stipEmailData.replace('[fd.Equipment_Order_Date_Received]',''+underWrtingRec.Funding_Data__r[0].Equipment_Order_Date_Received__c);
                }else{
                    stipEmailData=stipEmailData.replace('[fd.Equipment_Order_Number]','');
                    stipEmailData=stipEmailData.replace('[fd.Equipment_Order_Amount]','');
                    stipEmailData=stipEmailData.replace('[fd.Equipment_Order_Date_Received]','');
                }
                
                String htmlBody='';
                htmlBody = template[0].HtmlValue;
                htmlBody = htmlBody.replace('<Image_URL>',System.Label.Sunlight_Logo);
                htmlBody = htmlBody.replace('{!Stipulation_Data__c.Email_Text__c}',stipEmailData);
                Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                //  list of people who should get the email
                List<String> sendTo = new List<String>();
                if(stipData.name != 'INV'){
                    sendTo.add(underWrtingRec.Opportunity__r.Installer_Account__r.Installer_Email__c);
                }
                else{
                    if(System.Label.INV_stip_Email != null){
                        String emails = System.Label.INV_stip_Email;
                        for(String str :emails.split(';')){
                            sendTo.add(str);
                        }
                    }
                }
                if(template[0].Subject != null)
                    mail.setSubject (template[0].Subject);
                if(senderEmail.size()>0){
                    mail.setOrgWideEmailAddressId(senderEmail[0].id);
                } 
                mail.setHtmlBody(htmlBody);  
                mail.setToAddresses(sendTo);
                mail.setSaveAsActivity(false);
                
                // Add your email to the master list
                mails.add(mail);
                
                system.debug('### mail-->:'+mail);
                
            }
            
            // Send all emails in the master list
            if(mails.size()>0)  
                Messaging.sendEmail(mails);
        }catch(exception e){
            system.debug('#### exception in sendEmail-->:'+e.getMessage()+'## at Line-->:'+e.getLineNumber());
        }
        
    }
}