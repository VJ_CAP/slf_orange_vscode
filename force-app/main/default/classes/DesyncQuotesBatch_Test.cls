@isTest
public with sharing class DesyncQuotesBatch_Test {
    public static testMethod void myUnitTest(){
        SLFUtilityTest.createCustomSettings();   
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true,Risk_Based_Pricing__c = true,Credit_Waterfall__c = true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a2;

        Contact c1 = new Contact (AccountId=a1.Id, Lastname = 'Test 1');
        insert c1;

        Contact c2 = new Contact (AccountId=a1.Id, Lastname = 'Test 2');
        insert c2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Term_mo__c = 123,
                                            APR__c = 5.55,      
                                            ACH__c = true,                                      
                                            Is_Active__c = false,
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Expiration_Date__c = system.today().addDays(-1),
                                            Product_Display_Name__c = 'Test',
                                            Product_Tier__c = '0',
                                            Product_Loan_Type__c = 'HII');
        insert p1;
        
         Product__c p2 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Term_mo__c = 123,
                                            APR__c = 5.55,
                                            ACH__c = true,                                          
                                            Is_Active__c = true,
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Product_Tier__c = '0',
                                            Product_Loan_Type__c = 'HII'
                                        );
        insert p2;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            Primary_Contact__c = c1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;
        
         //Insert Quote Record
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = o1.id;
        quoteRec.SLF_Product__c = p1.Id ;
        
        quoteRec.Monthly_Payment__c = 1000;
        quoteRec.Combined_Loan_Amount__c = 9999;
        //quoteRec.Accountid =acc.id;
        insert quoteRec;
        o1.SyncedQuoteId = quoteRec.Id;
        update o1;     
        
        //Insert Admin Email Record
        Admin_Emails__c adminEmail = new Admin_Emails__c();
        adminEmail.name = '1';
        adminEmail.Admin_Email__c = 'test@gmail.com';
        adminEmail.Functionality__c = 'Product Expiration Batch';
        insert adminEmail;
        
        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
       
        uf1.ST_Amount_Financed__c = 1001;
        update uf1;
         
        o1.Primary_Contact__c = c2.Id;
        update o1;
        DesyncQuotesBatch batch = new DesyncQuotesBatch();
        database.executeBatch(batch,1); 
    }
    public static testMethod void myUnitTest2(){
        SLFUtilityTest.createCustomSettings();   
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true,Risk_Based_Pricing__c = false,Credit_Waterfall__c = true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a2;

        Contact c1 = new Contact (AccountId=a1.Id, Lastname = 'Test 1');
        insert c1;

        Contact c2 = new Contact (AccountId=a1.Id, Lastname = 'Test 2');
        insert c2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Term_mo__c = 123,
                                            APR__c = 5.55,
                                            ACH__c = true, 
                                            Is_Active__c = false,
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Expiration_Date__c = system.today().addDays(-1),
                                            Product_Display_Name__c = 'Test',
                                            Product_Tier__c = '0',
                                            Product_Loan_Type__c = 'HII');
        insert p1;
        
         Product__c p2 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Term_mo__c = 123,
                                            APR__c = 5.55,
                                            ACH__c = true,                                
                                            Is_Active__c = true,
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Product_Tier__c = '0',
                                            Product_Loan_Type__c = 'HII'
                                        );
        insert p2;
         
        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            Primary_Contact__c = c1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;
        
        
        
         //Insert Quote Record
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = o1.id;
        quoteRec.SLF_Product__c = p1.Id ;
        
        quoteRec.Monthly_Payment__c = 1000;
        quoteRec.Combined_Loan_Amount__c = 9999;
        //quoteRec.Accountid =acc.id;
        insert quoteRec;
        o1.SyncedQuoteId = quoteRec.Id;
        update o1;     
        
        //Insert Admin Email Record
        Admin_Emails__c adminEmail = new Admin_Emails__c();
        adminEmail.name = '1';
        adminEmail.Admin_Email__c = 'test@gmail.com';
        adminEmail.Functionality__c = 'Product Expiration Batch';
        insert adminEmail;
        
        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
       
        uf1.ST_Amount_Financed__c = 1001;
        update uf1;
         
        o1.Primary_Contact__c = c2.Id;
        update o1;
        DesyncQuotesBatch batch = new DesyncQuotesBatch();
        database.executeBatch(batch,1); 
    }
}