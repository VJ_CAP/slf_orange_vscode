/**
* Description: SP-152 Status API: Do bulk Rest call's to boomi from Status_Audit__c object.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/28/2017          Created
******************************************************************************************/

global class StatusAuditBatch implements Database.Batchable<sobject>,Database.AllowsCallouts,Database.Stateful{
    private String query; //Query in the start method
    private DateTime prevRunDt; //Previous datetime when the job ran
    private DateTime currRunDt; //Current datetime job started running
    global Map<Id,Status_Audit__c> statusAuditMap = new Map<Id,Status_Audit__c>();
    
    
    /**
    * Description: Constructor to construct Query based on the user criteria.
    */
    global StatusAuditBatch(DateTime prevRunDt, DateTime currRunDt)
    {
        system.debug('### Entered into StatusAuditBatch() of '+ StatusAuditBatch.class);
        this.query = getQuery();
        this.prevRunDt = prevRunDt;
        this.currRunDt = currRunDt;
        system.debug('### Leaving from StatusAuditBatch() of '+ StatusAuditBatch.class);
    }
    
    /**
    * Description: Entry of batch and execute a SOQL.
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('### Entered & Leaving into start() of '+ StatusAuditBatch.class);
        system.debug('### query String '+ query);
        
        
        return Database.getQueryLocator(query);
    }
    
    /**
    * Description: Do Rest call to boomi and update isProcessed__c = true into Status_Audit__c object.
    */
    global void execute(Database.BatchableContext BC, List<Status_Audit__c> scopeList)
    {
        system.debug('### Entered into execute() of '+ StatusAuditBatch.class);
        system.debug('### scopeList '+ scopeList);
        set<Id> oppIdSet = new set<Id>();
        List<Status_Audit__c> stsAudUpdateLst = new List<Status_Audit__c>();
        Map<Id,String> exceptionMap = new Map<Id,String>();//To Store All exception records
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        try
        {  
            for(Status_Audit__c stsAuditIterate : scopeList){
                if(null != stsAuditIterate.Opportunity__r.Installer_Account__c){                                    
                    /*integer daysDifference  =  currentDateTime.Date().daysBetween(stsAuditIterate.CreatedDate.Date());
                    daysDifference = daysDifference * -1;
                    system.debug('### daysDifference '+daysDifference);*/                    
                     
                       //added the condition  Subscribe_to_Push_Updates__c  orange -1072  
                    if(null != stsAuditIterate.JSON_Body__c && null != stsAuditIterate.Opportunity__r.Installer_Account__r.Push_Endpoint__c && stsAuditIterate.Opportunity__r.Installer_Account__r.Subscribe_to_Push_Updates__c && stsAuditIterate.isProcessed__c == false )
                    {
                         system.debug('### Inside the loop');
                        try
                        {
                            //send http post call to external system.
                            Http ObjHttp = new Http();
                            HttpRequest httpReq = new HttpRequest();
                            httpReq.setTimeout(120000);
                            httpReq.setEndpoint(stsAuditIterate.Opportunity__r.Installer_Account__r.Push_Endpoint__c);
                            httpReq.setHeader('content-type', 'application/json');
                            
                            // Start - Adding header to the request for authenticated status push e.g. Vivint Solar integration
                            if(String.isNotBlank(stsAuditIterate.Opportunity__r.Installer_Account__r.Endpoint_Header__c))
                            {                            
                                String strHeader = stsAuditIterate.Opportunity__r.Installer_Account__r.Endpoint_Header__c;
                                List<String> hdrList = strHeader.split(',');
                                for(String s : hdrList)
                                {
                                    if(String.isNotBlank(s) && s.split(':').size() == 2)
                                    {
                                        List<String> keyvalue = s.split(':');
                                        String key = keyvalue.get(0).trim();
                                        String value = keyvalue.get(1).trim();
                                        httpReq.setHeader(key, value);
                                    }
                                }   
                            }
                            
                            // End- Adding header to the request for authenticated status push e.g. Vivint Solar integration                            
                                                         
                            httpReq.setMethod('POST');
                            httpReq.setBody(stsAuditIterate.JSON_Body__c);
                            HttpResponse httpRes = new HttpResponse();
                            Integer stsCode;
                            if(!Test.isRunningTest())
                            {
                                httpRes = ObjHttp.send(httpReq);
                                stsCode = httpRes.getStatusCode();
                            }else{
                                //daysDifference = 2;
                                stsCode = 201;
                            }
                            
                            //If response is 200 status code update Status Audit isProcessed__c record to true.
                            
                            if(stsCode == 200)
                            {
                                Status_Audit__c stsAuditRec = new Status_Audit__c(id=stsAuditIterate.Id);
                                stsAuditRec.isProcessed__c = true;
                                stsAudUpdateLst.add(stsAuditRec);
                            }else{
                                // Added as part of ORANGE - 2925
                                    Error_Log__c errorLog = new Error_Log__c();
                                    errorLog.Functionality__c= 'StatusAuditBatch.execute()';
                                    errorLog.Method_Name__c= 'execute()';  
                                    errorLog.Line_Number__c = stsCode;
                                    errorLog.RecordIds__c = stsAuditIterate.Id;
                                    String errSTR=httpRes.getBody() + '###' +httpReq.toString();
                                    if(errSTR.length() > 32767)
                                    {
                                    errorLog.Message__c = errSTR.left(32760) + '...';    
                                    }
                                    else
                                    {
                                    errorLog.Message__c = errSTR;
                                    }
                                    errorLog.Type__c = 'Webservice';
                                    lstErrorLog.add(errorLog);
                                //ErrorLogUtility.writeLog('StatusAuditBatch.execute()',stsCode,'execute()',httpRes.getBody() + '###' +httpReq.toString(),'Error',stsAuditIterate.Id);   
                                //Assigning installer account Id as key and status Audit record as values to remove duplicate mails
                                statusAuditMap.put(stsAuditIterate.Opportunity__r.Installer_Account__c,stsAuditIterate);
                            }
                            
                            
                        }catch(Exception ex)
                        {
                            string strConcate = 'StatusAuditBatch.execute()@'+ex.getLineNumber()+'@execute()@'+ex.getMessage() + '###' +ex.getStackTraceString()+'@Error@'+stsAuditIterate.Id;
                            exceptionMap.put(stsAuditIterate.Id,strConcate);
                        }   
                    }
                    
                }
            }
            
           
            
            //Update Status Audit records with isProcessed = true.
            if(!stsAudUpdateLst.isEmpty())
                SLFUtility.upsertData(stsAudUpdateLst); 
            if(!lstErrorLog.isEmpty())
                DataBase.Insert(lstErrorLog);
            //Inserting Exception records in Error Log Object
            if(!exceptionMap.isEmpty())
            {
                for(Id recId : exceptionMap.keyset())
                {
                    string strConcate = exceptionMap.get(recId);
                    string[] splitStr = strConcate.split('@');
                    
                    ErrorLogUtility.writeLog(splitStr[0],Integer.valueOf(splitStr[1]),splitStr[2],splitStr[3],splitStr[4],splitStr[5]);
                }
                
            }
        }
        catch(Exception ex)
        {
            system.debug('### StatusAuditBatch.execute():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('StatusAuditBatch.execute()',ex.getLineNumber(),'execute()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }   
        system.debug('### Exit from execute() of '+ StatusAuditBatch.class);
    }
    
    /**
    * Description:
    */
    global void finish(Database.BatchableContext bc){
        system.debug('### statusAuditMap.size '+statusAuditMap.size());
        system.debug('### statusAuditMap '+statusAuditMap);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        if(!statusAuditMap.isEmpty())
        {
            for(id accIdIterate : statusAuditMap.keyset())
            {
                Status_Audit__c stsAuditIterate = statusAuditMap.get(accIdIterate);
                
                DateTime currentDateTime = system.now();
                Double createdDateHours = Math.Floor((Double.valueOf(currentDateTime.getTime()) - Double.valueOf(stsAuditIterate.CreatedDate.getTime())) / (1000.0*60.0*60.0));
                system.debug('### createdDateHours '+createdDateHours);
                
                //Send email alert to admin if end point URL is down
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> toAddresses = new List<String>();
                for (Admin_Emails__c emails : Admin_Emails__c.getAll().values()){
                    if(emails.Functionality__c == null && emails.Admin_Email__c != null){
                        toAddresses.add(emails.Admin_Email__c);
                    }
                }
                if(!toAddresses.isEmpty())
                {
                    //String[] toAddresses = new String[] {Label.Admin_Email}; 
                    mail.setToAddresses(toAddresses);
                    mail.setSenderDisplayName('Salesforce Support Team');

                    //List<String> ccTo = new List<String>();
                    //ccTo.add('test@test.com');
                    //mail.setCcAddresses(ccTo);
                    mail.setSubject(stsAuditIterate.Opportunity__r.Installer_Account__r.Name+' Account PUSH end point URL is down.');
                    String body = '<html><body>';
                    body += 'Hello Admin,</br></br>';
                    body += 'You currently subscribe to Sunlight Financial’s Push Status updates using your endpoint of '+stsAuditIterate.Opportunity__r.Installer_Account__r.Push_Endpoint__c+'.</br>';
                    body += 'We found that messages are not successfully received starting ' +createdDateHours+ ' hours ago.<br/></br>';
                    body += 'We maintain status messages for 24 hours and after that we purge them.<br/></br>';
                    body += 'Please let us know if you have any questions or updates for us.<br/></br>';
                    body += 'Installer Account : '+stsAuditIterate.Opportunity__r.Installer_Account__r.Name+'</br>';
                    body += 'Detail Link : <a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+stsAuditIterate.Opportunity__r.Installer_Account__c+'">view details</a></br></br></br>';
                    body += 'Sincerely,</br>';
                    body += 'Sunlight Financial Support Team';
                    body = body +'</body></html>';
                    mail.setHtmlBody(body);
                    mails.add(mail);
                }
            }
            
        }
        if(!mails.isEmpty())
            Messaging.sendEmail(mails);
           
    }
    
    /**
    * Description: Contructing query for just getting into start method.
    */
    public String getQuery(){
        system.debug('### Entered into getQuery() of '+ StatusAuditBatch.class);
        this.query = 'select isProcessed__c,JSON_Body__c,Opportunity__c,Opportunity__r.Installer_Account__c,Opportunity__r.Installer_Account__r.Name,Opportunity__r.Installer_Account__r.Push_Endpoint__c,Opportunity__r.Installer_Account__r.Subscribe_to_Push_Updates__c,Opportunity__r.Installer_Account__r.Endpoint_Header__c,Name,createdDate from Status_Audit__c where isProcessed__c = false AND Push_Type__c = \'Status Push\'';
        system.debug('### Leaving from getQuery() of '+ StatusAuditBatch.class);
        return this.query;
    }
}