global with sharing class BatchImportPayments 
							implements Database.Batchable<SObject>,
										Database.Stateful, 
										Database.AllowsCallouts,
										Schedulable 
{
	Boolean testMode = Test.isRunningTest();
	public Job__c job; 

	class Parameters
	{
		String statusToProcess = 'New';
		Boolean updateDatabase = true;
	}

	Parameters params = new Parameters ();

	class Row
	{
		Integer num;
		Date transactionDate;
		Decimal total;
		Decimal m0;
		Decimal m1;
		Decimal m2;
		Decimal incentive;
		Decimal m0Payback;
		Decimal m0Netout;
		Decimal cancellation;
		String id;
	}

//******************************************************************

	global BatchImportPayments() 
	{
		//  nothing to do
		//  JobSupport.submitJob('BatchImportPayments', '{"statusToProcess":"New"}');
		//  JobSupport.submitJob('BatchImportPayments', '{"statusToProcess":"Corrected"}');
	}

//******************************************************************

	global void execute(SchedulableContext sc)  
	{
		JobSupport.submitJob('BatchImportPayments', sc.getTriggerId());
	}

//******************************************************************

	global database.querylocator start (Database.BatchableContext bc)
	{		
        this.job = JobSupport.getJobForId (bc.getJobId());
		JobSupport.addLineToJobLog (this.job, 'Started', true);
        update this.job;
																		
        if (this.job.Parameters__c != null)
        {
            this.params = (Parameters) JSON.deserialize (this.job.Parameters__c, Parameters.class);
        }

        System.debug(this.params);

        JobSupport.addLineToJobLog (this.job, 'Parameters: ' + this.params, false);

		String query = 'SELECT Id, Name, Transaction_Date__c, M0__c, M1__c, M2__c, M0_Net_Out__c, M0_Payback__c, ' + 
						' Total__c, Status__c, Opportunity_Identifier__c, Cancellation__c, Incentive__c ' +
						' FROM Funding_Roster_Row__c WHERE Status__c = \'' + this.params.statusToProcess + '\'' +
							(this.testMode ? ' LIMIT 10' : '');
							
		Database.Querylocator ql = Database.getqueryLocator(query); 
		
		return ql;   
	}

//******************************************************************

	global void execute (Database.BatchableContext bc, list<Funding_Roster_Row__c> frrs) 
	{						 

    	Batch__c batch = new Batch__c (Job__c = this.job.Id);
    	
    	JobSupport.addLineToBatchLog (batch, 'Started', true);
		insert batch;

		list<Underwriting_Transaction__c> uts = new list<Underwriting_Transaction__c>{};
		map<String,Underwriting_File__c> ufsMap = new map<String,Underwriting_File__c>{};

		//. first, get all UF referenced by the Ids input and put in a map

		set<String> ids = new set<String>{};

		for (Funding_Roster_Row__c frr : frrs)
		{
			System.debug('ALANDEBUG: ' + frr.Opportunity_Identifier__c);
			
			if (frr.Opportunity_Identifier__c != null && 
				frr.Opportunity_Identifier__c.startswith('006') && 
				frr.Opportunity_Identifier__c.length() == 15)  // normalize to 18-character form of Id
			{
				try
				{
					Id x = frr.Opportunity_Identifier__c;
					frr.Opportunity_Identifier__c = x;
				}
				catch (Exception e) {}
			}
			if (frr.Opportunity_Identifier__c != null) ids.add (frr.Opportunity_Identifier__c);
		}

		for (Underwriting_File__c uf : [SELECT Id, Opportunity__r.Sighten_UUID__c
										FROM Underwriting_File__c
										WHERE Opportunity__r.Sighten_UUID__c IN :ids])
		{
			ufsMap.put (uf.Opportunity__r.Sighten_UUID__c, uf);
		}

		for (Underwriting_File__c uf : [SELECT Id, Opportunity__c
										FROM Underwriting_File__c
										WHERE Opportunity__c IN :ids])
		{
			ufsMap.put (uf.Opportunity__c, uf);
		}

		//. now process each record

		for (Funding_Roster_Row__c frr : frrs)
		{
			uts.addAll (generateTransactions (frr, ufsMap));
			JobSupport.addLineToBatchLog (batch, 'FRR result : ' + frr, false);
		}

		Savepoint sp = Database.setSavepoint();

		for (String s : DebugSupport.insertandReturnErrors(uts))
		{
			// report errors here
		}

		for (String s : DebugSupport.updateandReturnErrors(frrs))
		{
			// report errors here
		}

        if (this.params.updateDatabase == false)  
        {
        	Database.rollback(sp);
        	JobSupport.addLineToBatchLog (batch, 'Database updates rolled back', false);
        }

    	JobSupport.addLineToBatchLog (batch, 'Finished', true);
		update batch;
	}

//******************************************************************

	private list<Underwriting_Transaction__c> generateTransactions (Funding_Roster_Row__c frr, map<String,Underwriting_File__c> ufsMap)
	{
		list<Underwriting_Transaction__c> uts = new list<Underwriting_Transaction__c>{};

		//. processing here
		//. validate, if errors, set the status and errors field on the frr
		//  if ok, add UTs to the return list

		Row r = new Row ();

		r.transactionDate = getDate (frr.Transaction_Date__c);
		r.total = getDecimal (frr.Total__c);
		r.m0 = getDecimal (frr.M0__c);
		r.m1 = getDecimal (frr.M1__c);
		r.m2 = getDecimal (frr.M2__c);
		r.incentive = getDecimal (frr.Incentive__c);  //. etc
		r.m0Payback = getDecimal (frr.M0_Payback__c);
		r.m0Netout = getDecimal (frr.M0_Net_Out__c);
		r.cancellation = getDecimal (frr.Cancellation__c);
		r.id = frr.Opportunity_Identifier__c;

		frr.Status__c = 'Failed';

		frr.Error_Details__c = '';

		if (r.transactionDate == null)
		{
			frr.Error_Details__c += 'Invalid or missing date\n';
		}

		if (r.m0 == null && frr.M0__c != null)
		{
			frr.Error_Details__c += 'M0 invalid amount\n';
		}

		if (r.m1 == null && frr.M1__c != null)
		{
			frr.Error_Details__c += 'M1 invalid amount\n';
		}

		if (r.m2 == null && frr.M2__c != null)
		{
			frr.Error_Details__c += 'M2 invalid amount\n';
		}

		if (r.incentive == null && frr.Incentive__c != null)
		{
			frr.Error_Details__c += 'Incentive invalid amount\n';
		}

		if (r.m0Payback == null && frr.M0_Payback__c != null)
		{
			frr.Error_Details__c += 'M0 Payback invalid amount\n';
		}

		if (r.m0Netout == null && frr.M0_Net_Out__c != null)
		{
			frr.Error_Details__c += 'M0 Net Out invalid amount\n';
		}

		Underwriting_File__c uf = ufsMap.get (r.id);

		if (uf == null)
		{
			frr.Error_Details__c += 'Underwriting File not found for Id\n';
		}

		if (frr.Error_Details__c.length() > 0) return uts;

		frr.Status__c = 'Processed OK';

		if (r.m0 != null)
		{
			uts.add ( buildTransaction (r, uf, 'Installer M0 Payment Paid', r.m0));
		}

		if (r.m1 != null)
		{
			uts.add ( buildTransaction (r, uf, 'Installer M1 Payment Paid', r.m1));
		}

		if (r.m2 != null)
		{
			uts.add ( buildTransaction (r, uf, 'Installer M2 Payment Paid', r.m2));
		}

		if (r.incentive != null)
		{
			uts.add ( buildTransaction (r, uf, 'Incentive Payment Paid', r.incentive));
		}

		if (r.m0Payback != null)
		{
			uts.add ( buildTransaction (r, uf, 'Installer M0 Payback Paid', r.m0Payback));
		}

		if (r.m0Netout != null)
		{
			uts.add ( buildTransaction (r, uf, 'Installer M0 Net Out Paid (Taken)', r.m0Netout));
		}

		return uts;
	}

//  ***************************

	private Underwriting_Transaction__c buildTransaction (Row r, Underwriting_File__c uf, String transactionType, Decimal amount)

	// helper method builds an Underwriting_Transaction__c

	{
		Underwriting_Transaction__c t = new Underwriting_Transaction__c ();
		t.Underwriting_File__c = uf.Id;
		t.Type__c = transactionType;
		t.Amount__c = amount;
		t.Transaction_Date__c = r.transactionDate;
		t.Test_Only__c = true;

		return t;
	}

//  ***************************

	private Date getDate (String value)

	//  helper method validates a text date in the format mm/dd/yy or mm/dd/yyyy and if OK returns a Date object instance
	//  if not OK returns null

	{
		if (value == null || value.length() == 0) return null;
		list<String> parts = value.split('/');
		if (parts.size() != 3) return null;
		if (parts[2].length() == 2) parts[2] = '20' + parts[2];

		Date d = null;

		try
		{
			d = Date.newInstance (Integer.valueOf(parts[2]),Integer.valueOf(parts[0]),Integer.valueOf(parts[1]));
		}
		catch (Exception e) {}

		return d;
	}

//  ***************************

	private Decimal getDecimal (String value)

	//  helper method validates a text currency in the format "$nn,nnn.mm" and if OK returns a Decimal object instance
	//  if not OK returns null

	{
		if (value == null || value.length() == 0) return null;
		Decimal d = null;
		try
		{
			String s = value.remove('$').remove('"').remove(',').trim();
			if (s.contains('('))
			{
				s = '-' + s.remove('(').remove(')');
			}
			d = Decimal.valueOf (s);
		}
		catch (Exception e) {}

		return d;
	}

//******************************************************************
	
	global void finish (Database.BatchableContext bc)
	{
		JobSupport.addLineToJobLog (this.job, 'Finished', true);	
        update this.job;        
    }
}