/**
* Description: RevertChangeOrder related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma           10/17/2017          Created
******************************************************************************************/
public class RevertChangeOrderDataServiceImpl extends RestDataServiceBase{
    
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse transformOutput(IDataObject changOrDtObj)
    {
        system.debug('### Entered into transformOutput() of '+RevertChangeOrderDataServiceImpl.class);
        
        List<Opportunity> oppLst = new list<Opportunity>();
        
        UnifiedBean reqData = (UnifiedBean)changOrDtObj;
        system.debug('### reqData '+reqData);
        
        UnifiedBean revertChangeOrderbean = new UnifiedBean(); 
        revertChangeOrderbean.projects = new List<UnifiedBean.OpportunityWrapper>();
        revertChangeOrderbean.error = new list<UnifiedBean.errorWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        projectWrap.applicants = new List<UnifiedBean.ApplicantWrapper>();
        projectWrap.stips = new List<UnifiedBean.StipsAndStatusWrapper>();
        projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
        projectWrap.systemDesigns = new List<UnifiedBean.SystemDesignWrapper>();
        projectWrap.quotes = new List<UnifiedBean.QuotesWrapper>();
        
        Savepoint sp = Database.setSavepoint();
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {
            opportunity oppRec = new opportunity();
            String opportunityId = '';
            String externalId;
            List<user> loggedInUsr = [select Id,contactid,contact.AccountId,contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,UserRole.Name from User where Id =: userinfo.getUserId()];
            List<UnifiedBean.OpportunityWrapper> reqProjectsLst = reqData.projects;
            if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
            {                 
                errMsg = ErrorLogUtility.getErrorCodes('203',null);
                revertChangeOrderbean.returnCode = '203';
                //  return reqData;
            } 
            else if(
                (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
            {
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                revertChangeOrderbean.returnCode = '201';
            }else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
            {
                List<Opportunity> opprLst = [SELECT Id FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                
                if (opprLst==null || opprLst.size() == 0)
                {
                    revertChangeOrderbean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
                else{
                    opportunityId = opprLst.get(0).Id;
                }
            }
            else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
            {
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                        System.debug('### externalId '+externalId);
                    }else{
                        revertChangeOrderbean.returnCode = '400';
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                    }
                    
                }
                if(String.isBlank(errMsg))
                {
                    List<opportunity> opprLst = [select id,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        revertChangeOrderbean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                    else{
                        opportunityId = opprLst.get(0).id;
                    }
                }
            }else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
            {
                List<opportunity> opprLst = [select id,Hash_Id__c from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                if (opprLst!=null && opprLst.size() > 0)
                {
                    opportunityId = opprLst.get(0).id;
                }else {
                    revertChangeOrderbean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            } 
            if(String.isBlank(errMsg)  && String.isNotBlank(opportunityId))
            {
                oppLst =  [Select id,StageName,Change_Order_Status__c,EDW_Originated__c, SyncedQuoteId,Long_Term_Amount_Approved__c,Synced_Quote_Id__c,Installer_Account__c, Synced_Credit_Id__c,                   
                                             (Select id,Name,isSyncing from Quotes),                    
                                             (select Id,Name, isSyncing__c from Credits__r),                    
                                             (select id,LT_OID__c,LT_Facility_Compensation_Rate__c,Long_Term_Amount_Approved__c,Facility_Membership_Fee__c from Opportunities__r order by CreatedDate Desc limit 1),
                                             (select id,dsfs__DocuSign_Envelope_ID__c from R00N80000002fD9vEAE__r where dsfs__Envelope_Status__c =: 'Sent' OR dsfs__Envelope_Status__c = 'Delivered' limit 100 ),
                                             (select Id,Name,Project_Status__c ,Opportunity__c,Funding_status__c,INSTALL_CONTRACT_SYSTEM_COST__C, ST_AMOUNT_FINANCED__C,LT_AMOUNT_FINANCED__C,NAME_OF_UTILITY__C,Project_Status_Detail__c from Underwriting_Files__r)
                                             from Opportunity where id =: opportunityId];
                if(!oppLst.isEmpty())
                {
                    
                    oppRec = oppLst[0];
                    system.debug('### oppRec '+oppRec);
                    if(oppRec.EDW_Originated__c == true){
                        errMsg = ErrorLogUtility.getErrorCodes('218',new List<String>{String.valueOf(oppRec.id)}); 
                        revertChangeOrderbean.returnCode = '218';
                    }
                    
                    //else if(oppRec.Change_Order_Status__c==null || oppRec.Change_Order_Status__c != 'Active'){
                    else if(oppRec.Change_Order_Status__c==null || oppRec.Change_Order_Status__c != 'Pending'){
                        revertChangeOrderbean.returnCode = '311';
                        errMsg = ErrorLogUtility.getErrorCodes('311',null);
                    }else if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                        revertChangeOrderbean.returnCode = '366';
                        errMsg = ErrorLogUtility.getErrorCodes('366',null);
                    }
                    else{
                        Map<String,String> OwnercheckMap = SLFUtility.checkOpportunityOwner(oppRec,loggedInUsr[0]);
                        if(OwnercheckMap != null){
                            errMsg = OwnercheckMap.values().get(0);
                            List<String> errList = new List<String>();
                            errList.addAll(OwnercheckMap.keyset());
                            revertChangeOrderbean.returnCode = errList[0];
                        }
                        else
                        {
                           
                            String fieldnames = '';
                            Opportunity obj;
                            String objName = 'Opportunity'; 
                            system.debug('### oppRec related list '+oppRec.Opportunities__r);
                            Id oppId = oppRec.Opportunities__r[0].Id;
                            Boolean exist = false;
                            
                            Map < String, Schema.SObjectType > m = Schema.getGlobalDescribe();
                            Schema.SObjectType s = m.get(objName);
                            Schema.DescribeSObjectResult r = s.getDescribe();
                            //get fields
                            Map < String, Schema.SObjectField > fields = r.fields.getMap();
                            for (string field: fields.keySet()) 
                            {
                                system.debug('**field*'+field);
                                Schema.DescribeFieldResult fieldDescribe = fields.get(field).getDescribe();
                                system.debug('**isUpdateable*'+fieldDescribe.isUpdateable());
                                
                                if(!fieldDescribe.isCalculated()&&fieldDescribe.isUpdateable() && fieldDescribe.isAutoNumber() == false)
                                {
                                    list<String> updateFields = new list<String>();
                                    exist = false;
                                    for (ExceptionList__c el : ExceptionList__c.getAll().values())
                                    {
                                        system.debug('**el*'+el);
                                        if(el.API_Name__c == field && el.Object_Name__c == 'Opportunity')
                                        {   
                                            exist = true;
                                            break;
                                        }
                                    }
                                    if(!exist)
                                    {
                                        if (fieldnames == '') {
                                            fieldnames = field;
                                        } else {
                                            fieldnames += ',' + field;
                                        }
                                    }
                                }
                            }
                            String sql = 'SELECT ' + fieldnames + ' FROM ' + objName + ' WHERE Id=\'' + oppId + '\'';
                            system.debug('**sql*'+sql);
                            obj = database.query(sql);
                            if(null != oppRec.Synced_Credit_Id__c)
                            {
                                SLF_Credit__c newSLFCredit = new SLF_Credit__c(id = oppRec.Synced_Credit_Id__c);
                                newSLFCredit.isSyncing__c = true;
                                update newSLFCredit;
                            }
                            if(null != oppRec.Opportunities__r)
                            {
                                List<Opportunity> lstOpprToUpdate = new List<Opportunity>(); //Added by Suresh on 31/07/2018 (Orange 791)
                                Opportunity opt = new Opportunity();
                                for(String st: fieldnames.split(','))
                                {
                                    system.debug('**st*'+st);
                                    system.debug('**value*'+obj.get(st));
                                    opt.put(st,obj.get(st));
                                }
                                //Updated by Adithya as part of ORANGE-4789
                                //start
                                if(null != oppRec.Synced_Quote_Id__c)
                                {
                                    Quote syncedQuote = new Quote(id=oppRec.Synced_Quote_Id__c);
                                    syncedQuote.Max_Amount_Approved__c = opt.Long_Term_Amount_Approved__c;
                                    update syncedQuote;
                                }
                                //End
                                opt.id = oppRec.id;
                                opt.StageName = 'Closed Won';
                                opt.ForecastCategoryName = 'Closed';
                                opt.Probability = 100;
                                
                                //Srikanth - 06/11 - Orange-1877 -> commented
                            //  opt.Change_Order_Status__c = NULL;  //Srikanth - 03/15 - added this to reset change order status so that cancel change order can't be invoked twice
                                
                                opt.isChangeOrderCancelled__c = true;
                                opt.isChangeOrderSubmitted__c = false;
                                opt.SyncedQuoteId = oppRec.Synced_Quote_Id__c;
                                                                
                                //update opt; //Commented by Suresh on 31/07/2018 (Orange 791)
                                
                                System.debug('### Tracking Opportunitied to update: '+opt);
                                
                                //Added by Suresh on 31/07/2018 (Orange 791)
                                List<Opportunity> lstArchivedOppt = oppRec.Opportunities__r;
                                Opportunity archivedOpp = lstArchivedOppt.get(0);
                                System.debug('### archivedOpp: '+archivedOpp);
                                archivedOpp.Change_Order_Status__c = 'Cancelled';
                                //Added by Adithya
                                //start
                                opt.LT_OID__c = archivedOpp.LT_OID__c;
                                opt.LT_Facility_Compensation_Rate__c = archivedOpp.LT_Facility_Compensation_Rate__c;
                                opt.Facility_Membership_Fee__c = archivedOpp.Facility_Membership_Fee__c;
                                //End
                                lstOpprToUpdate.add(opt); //Added by Suresh on 31/07/2018 (Orange 791)
                                lstOpprToUpdate.add(archivedOpp);
                                
                                update lstOpprToUpdate;
                                //End
                            }
                            String uwFields = '';
                            Set<id> uwIds = new Set<id>();
                            List<Underwriting_File__c> uwObj;
                            
                            for (ExceptionList__c el : ExceptionList__c.getAll().values())
                            {
                                if(el.Object_Name__c == 'Underwriting_File__c'){
                                    if (uwFields == '') {
                                        uwFields = el.API_Name__c;
                                    } else {
                                        uwFields += ',' + el.API_Name__c;
                                    }
                                }
                            }
                            Underwriting_File__c updateduwRec = new Underwriting_File__c();
                            String uwSql = 'SELECT ' + uwFields + ' FROM Underwriting_File__c WHERE Opportunity__c=\'' + oppId + '\'';
                            uwObj = database.query(uwSql);
                            system.debug('### Values from archived UW record..###'+uwObj[0]);
                            Underwriting_File__c underwritingRec = uwObj[0];
                            if(oppRec.Underwriting_Files__r != null)
                            {
                                for(String st: uwFields.split(','))
                                {
                                    updateduwRec.put(st,underwritingRec.get(st));
                                }
                                updateduwRec.id = oppRec.Underwriting_Files__r[0].id;
                                update updateduwRec;
                            }
                            
                            revertChangeOrderbean = SLFUtility.getUnifiedBean(new set<Id>{oppRec.Id},'Orange',false);
                            revertChangeOrderbean.returnCode = '200';
                        }
                    }
                    
                }
            }
            System.debug('### Tracking - Error Message: '+errMsg);
            if(string.isNotBlank(errMsg))
            {
                Database.rollback(sp);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                revertChangeOrderbean.error.add(errorMsg);
            }
        }catch(Exception err)
        {
            Database.rollback(sp);
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            revertChangeOrderbean.returnCode = '400';   
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            revertChangeOrderbean.error.add(errorMsg); 
            system.debug('### RevertChangeOrderDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('RevertChangeOrderDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }  
        
        iRestRes = (IRestResponse)revertChangeOrderbean;
        system.debug('### Exit from transformOutput() of '+RevertChangeOrderDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        if(!oppLst.isEmpty()){
            if(oppLst[0].R00N80000002fD9vEAE__r != NULL && oppLst[0].R00N80000002fD9vEAE__r.size() > 0)
            {
                list<Id> ilist = new list<id>();
                for(dsfs__DocuSign_Status__c ds : oppLst[0].R00N80000002fD9vEAE__r)
                    ilist.add(ds.Id);
                VoidLoanDocs.voiddocs(ilist);
            }
        }

        return iRestRes;
    }
    
}