@isTest
public class ResendAutomatedLoanConfirmationEmailTest {

    @testSetup static void createTestData(){
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgFundingDataflag = new TriggerFlags__c();
        trgFundingDataflag.Name ='Funding_Data__c';
        trgFundingDataflag.isActive__c =true;
        trgLst.add(trgFundingDataflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgattachflag = new TriggerFlags__c();
        trgattachflag.Name ='Attachment';
        trgattachflag.isActive__c =true;
        trgLst.add(trgattachflag);
        
        insert trgLst;
        
        List<Underwriting_Confirmation_Emails__c> UWEmailList =  new List<Underwriting_Confirmation_Emails__c>();
        
        Underwriting_Confirmation_Emails__c ut = new Underwriting_Confirmation_Emails__c();
        ut.Loan_Type__c ='Single';
        ut.Name = 'AAC_Single_Loan_1';
        ut.Long_Term_Facility__c ='AAC';
        ut.Short_Term_Facility__c ='';
        ut.Email_Template_Name__c ='AAC_Single_Loan_1';
        UWEmailList.add(ut);
        
        ut = new Underwriting_Confirmation_Emails__c();
        ut.Loan_Type__c ='Single';
        ut.Name = 'CCU_Single_Loan_1';
        ut.Long_Term_Facility__c ='CCU';
        ut.Short_Term_Facility__c ='';
        ut.Email_Template_Name__c ='CCU_Single_Loan_1';
        UWEmailList.add(ut);
        
        ut = new Underwriting_Confirmation_Emails__c();
        ut.Loan_Type__c ='Combo';
        ut.Name = 'CRB_CRB_v2_1';
        ut.Long_Term_Facility__c ='CRB';
        ut.Short_Term_Facility__c ='CRB';
        ut.Email_Template_Name__c ='CRB_CRB_v2_1';
        UWEmailList.add(ut);
        
        ut = new Underwriting_Confirmation_Emails__c();
        ut.Loan_Type__c = 'Same as Cash';
        ut.Name = 'CRB_Short_term_Only_v2_1';
        ut.Long_Term_Facility__c ='';
        ut.Short_Term_Facility__c ='CRB';
        ut.Email_Template_Name__c ='CRB_Short_term_Only_v2_1';
        UWEmailList.add(ut);
        
        insert UWEmailList;
        
        insert new SFSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), Root_Folder_ID__c='0');

        Account acct = new Account(
            Name = 'Installer Test Account',
            Installer_Legal_Name__c='Bright planet Solar',
            Solar_Enabled__c=true
        );
        insert acct;

        Contact c = new Contact(
            FirstName = 'Sunlight',
            LastName = 'Financial',
            Email = 'test@email.com'
        );
        insert c;

        Product__c slf = new Product__c(
            //Short_Term_Facility__c = 'CRB',
            //Long_Term_Facility__c = 'CRB',
            Installer_Account__c = acct.Id,
            State_Code__c = 'TX',
            FNI_Min_Response_Code__c = 5,
            Product_Display_Name__c = 'Test Product',
            Qualification_Message__c = 'Test Qualification Message',
            Is_Active__c = true,
            Long_Term_Facility__c ='CRB',
            Short_Term_Facility__c ='CRB',  
            Name = 'Test',
            Product_Tier__c = '0'
        );
        insert slf;

        Opportunity opp = new Opportunity(
            SLF_Product__c = slf.Id,
            Name = 'Test Opportunity',
            StageName = 'Qualified',
            CloseDate = Date.today().addDays(-3),
            Install_State_Code__c = 'TX',
            Installer_Account__c = acct.Id,
            Accountid = acct.Id,
            isACH__c = true,
            ACH_APR_Process_Required__c = true
        );
        insert opp;

        Underwriting_File__c uwf = new Underwriting_File__c(
            Send_Confirmation_Email__c = true,
            Opportunity__c = opp.Id,
            Confirm_ACH_Information__c = 'Not ACH Enrolled'
        );
        insert uwf;

        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = opp.Id;
        quoteRec.SLF_Product__c = slf.Id ;
        insert quoteRec;
        
        opp.SyncedQuoteId = quoteRec.Id;
        update opp;
        
        List<Attachment> attchList = new List<Attachment>();
        Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob; 
        attach.parentId=opp.Id; 
        attach.ContentType = 'application/msword'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test'; 
        attchList.add(attach);
        insert attchList; 
        
        box__FRUP__c boxObj = new box__FRUP__c(box__Object_Name__c = 'Opportunity',box__Record_ID__c = opp.id, box__Folder_ID__c = 'TestFolder');
        insert boxObj;
    }

      public static testMethod void runTest() {
        
        Underwriting_File__c uwf = [SELECT Id FROM Underwriting_File__c LIMIT 1];
        String authResponse = BoxTestJsonResponseFactory.AUTH_USER_TOKEN;
        Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(authResponse, 'OK', 200));
        Test.startTest();
        ResendAutomatedLoanConfirmationEmail.sendEmailConfirmation(uwf.id);
        ResendAutomatedLoanConfirmationEmail.SendEmailConfirmationInput  i = new ResendAutomatedLoanConfirmationEmail.SendEmailConfirmationInput(uwf.Id);
        try{
            ResendAutomatedLoanConfirmationEmail.sendEmailConfirmation (new list<ResendAutomatedLoanConfirmationEmail.SendEmailConfirmationInput>{i});
            ResendAutomatedLoanConfirmationEmail.sendEmailConfirmation (new list<ResendAutomatedLoanConfirmationEmail.SendEmailConfirmationInput>{});
        }catch(Exception ex){}
          
        Test.stopTest();
    }
}