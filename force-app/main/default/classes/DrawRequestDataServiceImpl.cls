/**
* Description: User validation logic
*
*   Modification Log :
--------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Suresh Kumar            10/26/2018          Created
******************************************************************************************/
public class DrawRequestDataServiceImpl extends RestDataServiceBase{
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse transformOutput(IDataObject stsDtObj){ 
        system.debug('### Entered into transformOutput() of '+DrawRequestDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        
        UnifiedBean unifiedRespBean = new UnifiedBean(); 
        unifiedRespBean.error = new list<UnifiedBean.errorWrapper>();
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            string errMsg = '';
        IRestResponse iRestRes;
        
        String sInstallerId = null;
        String sLoggedInUser = null;
        String sloggedInUserName = null;
        String strMessage = '';
        String strAmount='';
        Savepoint sp = Database.setSavepoint();
        try
        {
            

            List<user> loggedInUsr = [select Id,name,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
            sInstallerId = loggedInUsr.get(0).contact.AccountId;
            sLoggedInUser = loggedInUsr.get(0).Id;
            sloggedInUserName = loggedInUsr.get(0).name;
            
            if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
            {                 
                errMsg = ErrorLogUtility.getErrorCodes('203',null);
                unifiedRespBean.returnCode = '203';                
            } 
            else if(
                (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
            {
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                unifiedRespBean.returnCode = '201';
            }
            else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
            {
                List<Opportunity> opprLst = [SELECT Id,Installer_Account__c FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                
                if (opprLst==null || opprLst.size() == 0)
                {
                    unifiedRespBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            }
            else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
            {
                String externalId;
                //List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                    }else{
                        unifiedRespBean.returnCode = '400';
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                    }
                }
                if(String.isBlank(errMsg)){
                    List<opportunity> opprLst = [select id,StageName,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        unifiedRespBean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                }
            }
            else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
            {
                List<opportunity> opprLst = [select id,StageName,Hash_Id__c from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                if (opprLst==null || opprLst.size() == 0) {
                    unifiedRespBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            }
            
            if(string.isNotBlank(errMsg))
            {
                //Database.rollback(sp);
                iolist = new list<DataValidator.InputObject>{};
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                errMsg = errorMsg.errorMessage;
                unifiedRespBean.error.add(errorMsg);
                iRestRes = (IRestResponse)unifiedRespBean;
                return iRestRes;
            }
            else
            {
                //Orange-7033 Udaya Kiran start
                if(reqData.projects[0].draws[0].progressPay == true)
                    {
                        List<Underwriting_file__c> lstUnderWriting = [select id,opportunity__c,Approved_for_Payments__c, Amount_Remaining__c,Total_Amount_Drawn__c,Opportunity__r.Progress_Pay_Request_Amount__c,opportunity__r.id,opportunity__r.Has_Open_Stip__c,opportunity__r.Block_Draws__c, opportunity__r.Draw_Window_Expiration__c, opportunity__r.Combined_Loan_Amount__c, opportunity__r.Max_Draw_Available__c,opportunity__r.Draw_Carry_Over__c, opportunity__r.Primary_Applicant_Email__c,Project_Status_Detail__c, opportunity__r.Co_Applicant__r.PersonEmail,opportunity__r.Max_Draw_Count__c,opportunity__r.All_HI_Required_Documents__c,opportunity__r.Installer_Account__r.Name,Opportunity__r.Owner.Name,opportunity__r.Account.Name,opportunity__r.Installer_Account__r.Installer_Email__c,opportunity__r.Installer_Account__r.Operations_Email__c,opportunity__r.Hash_Id__c,(select id,Level__c,Amount__c,Project_Complete_Certification__c,Status__c from Draw_Requests__r) from underwriting_file__c where opportunity__r.id=:reqData.projects[0].id];
                        if(lstUnderWriting[0].Draw_Requests__r.size() > 0)
                        {
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('363',null),'363');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }else{
                            Opportunity ppayOpp = new Opportunity();
                            ppayOpp.Id = reqData.projects[0].id;
                            ppayOpp.Progress_Pay_Requested_Date__c = System.now();
                            ppayOpp.Progress_Pay_Requested_by__c = sloggedInUserName;
                            ppayOpp.Progress_Pay_Approval_Date__c = System.now();
                            Update ppayOpp;

                            //Need to update Funding_Data__c.Progress_Pay_Approval_Date__c when Oppty.Progress_Pay_Approval_Date_c is populated
                            List<Funding_Data__c> lstFundingData = [Select Id, Underwriting_ID__c, Progress_Pay_Request_Amount__c, Progress_Pay_Approval_Date__c from Funding_Data__c where Underwriting_ID__c =: lstUnderWriting[0].Id];
                            if(!lstFundingData.isEmpty())
                            {
                                Funding_Data__c fundingDataRec = new Funding_Data__c(id=lstFundingData[0].id);
                                fundingDataRec.Progress_Pay_Approval_Date__c = System.today();
                                fundingDataRec.Progress_Pay_Request_Amount__c =  lstUnderWriting[0].Opportunity__r.Progress_Pay_Request_Amount__c;
                                update fundingDataRec;
                            }
                                                        
                            set<Id> oppIdSet = new set<Id>();
                            oppIdSet.add(reqData.projects[0].id);
                            unifiedRespBean = SLFUtility.getUnifiedBean(oppIdSet,'Orange',false);
                            unifiedRespBean.returnCode = '200';
                            unifiedRespBean.message ='Thank you for your payment request. We will proceed with your progress payment request now.';
                        }
                    }else{//Orange-7033 Udaya Kiran end
                System.debug('### processing draw request');
                if(reqData.projects[0].draws!= null){
                     Id oppId;
                      // resend payment request
                    if(reqData.projects[0].draws[0].id!= null && reqData.projects[0].draws[0].resendEmail!=null && reqData.projects[0].draws[0].resendEmail){
                        List<Draw_Requests__c> lstDrawRequest=[SELECT id,Approval_Date__c,Draw_Request_Resend_Date__c,Customer_Name__c,Draw_Methodology__c,Installer_Account_Name__c,Installer_Email__c,Primary_Applicant_Email__c,Request_Date__c,Status__c,Sales_Rep_Name__c,Underwriting__c,Underwriting__r.opportunity__c  FROM Draw_Requests__c WHERE id=:reqData.projects[0].draws[0].id];
                        
                        if(!lstDrawRequest.isEmpty()){
                            if(lstDrawRequest[0].Status__c=='Requested'){
                                lstDrawRequest[0].Draw_Request_Resend_Date__c = System.now();
                                oppId = lstDrawRequest[0].Underwriting__r.opportunity__c;
                                update lstDrawRequest[0];   
                            }else{
                                // invalid input
                                getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                                iRestRes = (IRestResponse)unifiedRespBean;
                                return iRestRes;
                            }
                        }else{
                            // invalid input
                                getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                                iRestRes = (IRestResponse)unifiedRespBean;
                                return iRestRes;
                        }
                        
                    }else{
                    List<Underwriting_file__c> lstUnderWriting = [select id,opportunity__c,Approved_for_Payments__c, Amount_Remaining__c,Total_Amount_Drawn__c,opportunity__r.id,opportunity__r.Has_Open_Stip__c,opportunity__r.Block_Draws__c, opportunity__r.Draw_Window_Expiration__c, opportunity__r.Combined_Loan_Amount__c, opportunity__r.Max_Draw_Available__c,opportunity__r.Draw_Carry_Over__c, opportunity__r.Primary_Applicant_Email__c,Project_Status_Detail__c, opportunity__r.Co_Applicant__r.PersonEmail,opportunity__r.Max_Draw_Count__c,opportunity__r.All_HI_Required_Documents__c,opportunity__r.Installer_Account__r.Name,Opportunity__r.Owner.Name,opportunity__r.Account.Name,opportunity__r.Installer_Account__r.Installer_Email__c,opportunity__r.Installer_Account__r.Operations_Email__c,opportunity__r.Hash_Id__c,(select id,Level__c,Amount__c,Project_Complete_Certification__c,Status__c from Draw_Requests__r) from underwriting_file__c where opportunity__r.id=:reqData.projects[0].id];
                    
                    if(lstUnderWriting!=null && !lstUnderWriting.isEmpty()) 
                    {
                        List<String> lstDrawnLevels = new List<String>();
                       
                        Underwriting_file__c objUnderWriting = lstUnderWriting.get(0);
                        double totalAmountDrawn = objUnderWriting.Total_Amount_Drawn__c!=null?objUnderWriting.Total_Amount_Drawn__c : 0;
                        double approvedLoanAmount = objUnderWriting.opportunity__r.Combined_Loan_Amount__c!=null ? objUnderWriting.opportunity__r.Combined_Loan_Amount__c : 0;
                        Decimal dAmount = objUnderWriting.opportunity__r.Combined_Loan_Amount__c.setScale(2);
                        strAmount = '$'+ String.valueOf(dAmount.format().contains('.')?dAmount.format():(dAmount.format()+'.00')) +'.';  
                        boolean hasAnyOpenStips = objUnderWriting.opportunity__r.Has_Open_Stip__c=='False' ? false : true;
                        Double drawCarriedOver = objUnderWriting.opportunity__r.Draw_Carry_Over__c!=null ? objUnderWriting.opportunity__r.Draw_Carry_Over__c : 0;
                        Decimal maxDrawCount = objUnderWriting.opportunity__r.Max_Draw_Count__c!=null?objUnderWriting.opportunity__r.Max_Draw_Count__c:3;
                        boolean isAllRequiredDocsUploaded = objUnderWriting.opportunity__r.All_HI_Required_Documents__c;
                        boolean isProjectCompleted = false;
                        boolean bAnyPendingDraws = false;
                        
                        List<Draw_Requests__c> lstDrawRequests = objUnderWriting.Draw_Requests__r;
                        system.debug('### lstDrawRequests size==>:'+lstDrawRequests.size());
                        //Check what all Drawn levels completed
                        if(lstDrawRequests!=null && !lstDrawRequests.isEmpty()){
                            for(Draw_Requests__c objDrawReqs : lstDrawRequests){
                                
                                if(objDrawReqs.Status__c=='Approved')
                                {
                                    lstDrawnLevels.add(objDrawReqs.level__c);
                                }
                                if(objDrawReqs.Project_Complete_Certification__c == false && objDrawReqs.level__c == '1'){
                                    strMessage = 'You must confirmed completion of this project.';
                                }else{
                                    strMessage = 'Thank you for your payment request, we will request authorization from the customer.';
                                    //unifiedRespBean.message = 'Thank you for your payment request, we will request authorization from the customer.';
                                }
                                //if(objDrawReqs.Project_Complete_Certification__c)
                                if(objDrawReqs.Project_Complete_Certification__c && objDrawReqs.Status__c=='Approved'){
                                    isProjectCompleted = true;
                                }
                                if(objDrawReqs.Status__c=='Requested'){
                                    bAnyPendingDraws = bAnyPendingDraws!=true ? true : false;
                                }
                            }
                        }
                        system.debug('### bAnyPendingDraws==>:'+bAnyPendingDraws);
                        if(bAnyPendingDraws){
                            //System.debug('### There are any pending draw requests on underwriting.'+bAnyPendingDraws);
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('363',null),'363');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        
                        if(isProjectCompleted){
                            //System.debug('### Project has been completed. You canot make any more draw requests.');
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('358',null),'358');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        if(hasAnyOpenStips){
                            //throw openstips error
                            //System.debug('### Has open stips. please close them.');
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('343',null),'343');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        else if(objUnderWriting.opportunity__r.Block_Draws__c){
                            //throw block draws error
                            //System.debug('### Blocked draws. Please contact sunlight financial');
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('344',null),'344');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        else if(!objUnderWriting.Approved_for_Payments__c){
                            //throw payment not aproved error
                            //System.debug('### Payment canot be approved.');
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('345',null),'345');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        else if(System.Date.today() > objUnderWriting.opportunity__r.Draw_Window_Expiration__c ){
                            //throw payment not aproved error
                            //System.debug('### Draw Window Expired Payment canot be approved.');
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('346',null),'346');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        else if(!isAllRequiredDocsUploaded){
                            //throw payment not aproved error
                            //System.debug('### Required documents are not uploaded.');
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('347',null),'347');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        else{
                            //Query Draw Allocations for the installer and prepare a map(DrawLevel,DrawAllocation in %)
                            Map<String,Decimal> allocations = new Map<String,Decimal>();
                            List<Draw_Allocation__c> lstDrawAllocation = [select id,Draw_Allocation__c,Level__c,Account__c from Draw_Allocation__c where Account__c=:sInstallerId];
                            if(lstDrawAllocation!=null && !lstDrawAllocation.isEmpty()){
                                for(Draw_Allocation__c objDrawAllocations : lstDrawAllocation){
                                    allocations.put(objDrawAllocations.Level__c,objDrawAllocations.Draw_Allocation__c);
                                }
                            }
                            else{
                                System.debug('### Draw Allocations not configured.');
                                getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('400',null),'400');
                                iRestRes = (IRestResponse)unifiedRespBean;
                                return iRestRes;
                            }
                            System.debug('### Draw Allocations.'+allocations);
                            
                            List<Draw_Requests__c> insrtDrawRequestList = new List<Draw_Requests__c>();
                            List<Opportunity> updOpprList = new List<Opportunity>();
                            List<Underwriting_file__c> updateUnderwritingList = new List<Underwriting_file__c>();
                            
                            for(UnifiedBean.DrawRequestWrapper drawRequestIterate : reqData.projects[0].draws)
                            {
                                Double maxDrawAmount = 0;
                                Double requestedDrawAmount = drawRequestIterate.amount;
                                Double totalAllowedDrawPercent = 0;
                                Decimal allocationPercentForNext = 0;
                                
                                Opportunity oppUpdate = new Opportunity();
                                oppUpdate.id=objUnderWriting.opportunity__r.id;
                                
                                if(drawRequestIterate.drawLevel==null){
                                    //System.debug('### Requested Amount is more than remaining balance.');
                                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('361',null),'361');
                                    iRestRes = (IRestResponse)unifiedRespBean;
                                    return iRestRes;
                                }
                                
                                if(objUnderWriting.Amount_Remaining__c < drawRequestIterate.amount){
                                    //System.debug('### Requested Amount is more than remaining balance.');
                                    List<String> lstReplacements = new List<String>();
                                    lstReplacements.add(String.valueOf(objUnderWriting.Amount_Remaining__c));
                                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('356',lstReplacements),'356');
                                    iRestRes = (IRestResponse)unifiedRespBean;
                                    return iRestRes;
                                }
                                
                                if(drawRequestIterate.amount > approvedLoanAmount){
                                    //System.debug('### Requested draw amount should not exceed than maximum amount.');
                                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('355',null),'355');
                                    iRestRes = (IRestResponse)unifiedRespBean;
                                    return iRestRes;
                                }
                                
                                if(lstDrawnLevels.contains(String.valueOf(drawRequestIterate.drawLevel))){
                                    //System.debug('### Requested level already completed.');
                                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('348',null),'348');
                                    iRestRes = (IRestResponse)unifiedRespBean;
                                    return iRestRes;
                                }
                                
                                if(drawRequestIterate.drawLevel > maxDrawCount){
                                    //System.debug('### Exceeded max draw count.');
                                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('349',null),'349');
                                    iRestRes = (IRestResponse)unifiedRespBean;
                                    return iRestRes;
                                }
                                
                                Decimal allocationPercent = allocations.get(String.ValueOf(drawRequestIterate.drawLevel));
                                if(drawRequestIterate.drawLevel >= maxDrawCount || !allocations.containsKey(String.valueOf(drawRequestIterate.drawLevel))){
                                    allocationPercentForNext = 0;
                                }
                                else if(null != allocations && allocations.containsKey(String.ValueOf(drawRequestIterate.drawLevel+1))){
                                    system.debug('drawLevel - '+drawRequestIterate.drawLevel);
                                    system.debug('allocation - '+allocations.get(String.ValueOf(drawRequestIterate.drawLevel+1)));
                                    
                                    allocationPercentForNext = allocations.get(String.ValueOf(drawRequestIterate.drawLevel+1));
                                    
                                }
                                system.debug('allocationPercent - '+allocationPercent); 
                                system.debug('allocationPercentForNext - '+allocationPercentForNext);
                                system.debug('drawCarriedOver - '+drawCarriedOver);
                                system.debug('approvedLoanAmount - '+approvedLoanAmount);
                                Double nextMaxAmount = (((allocationPercent+allocationPercentForNext+drawCarriedOver)/100) * approvedLoanAmount);
                                System.debug('### Tracking - nextMaxAmount: '+nextMaxAmount);
                                
                                totalAllowedDrawPercent = allocationPercent+drawCarriedOver;
                                System.debug('### Tracking - totalAllowedDrawPercent: '+totalAllowedDrawPercent);
                                
                                if(!drawRequestIterate.isProjectComplete){
                                    
                                    
                                    maxDrawAmount = totalAllowedDrawPercent/100 * approvedLoanAmount;
                                    Double nextMaxDrawAmount = nextMaxAmount-requestedDrawAmount;
                                    oppUpdate.Max_Draw_Available__c = nextMaxDrawAmount;
                                    oppUpdate.Draw_Carry_Over__c = (totalAllowedDrawPercent - (requestedDrawAmount/approvedLoanAmount * 100));
                                } else if(drawRequestIterate.isProjectComplete){
                                    maxDrawAmount = approvedLoanAmount-totalAmountDrawn;
                                    Double nextMaxDrawAmount = maxDrawAmount-requestedDrawAmount;
                                    oppUpdate.Max_Draw_Available__c = nextMaxDrawAmount;
                                    
                                    //oppUpdate.Draw_Carry_Over__c = (allocationPercent - (requestedDrawAmount/approvedLoanAmount * 100));
                                    oppUpdate.Draw_Carry_Over__c = ((nextMaxDrawAmount/approvedLoanAmount) * 100);
                                }
                                
                                if(maxDrawAmount < drawRequestIterate.amount){
                                    //System.debug('### Requested draw amount should not exceed than maximum amount.');
                                    List<String> lstReplacements = new List<String>();
                                    lstReplacements.add(String.valueOf(maxDrawAmount));
                                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('356',lstReplacements),'356');
                                    iRestRes = (IRestResponse)unifiedRespBean;
                                    return iRestRes;
                                }
                                Draw_Requests__c objDraw = new Draw_Requests__c();
                                objDraw.Level__c = String.valueOf(drawRequestIterate.drawLevel);
                                objDraw.Amount__c = drawRequestIterate.amount;
                                objDraw.Request_Date__c = System.now();
                                objDraw.Status__c = 'Requested';
                                objDraw.Project_Complete_Certification__c = drawRequestIterate.isProjectComplete;
                                objDraw.Requester__c = sLoggedInUser;
                                objDraw.Maximum_Draw_Available__c = maxDrawAmount;
                                objDraw.Underwriting__c = objUnderWriting.id;
                                objDraw.Primary_Applicant_Email__c = objUnderWriting.opportunity__r.Primary_Applicant_Email__c;
                                objDraw.Co_Applicant_Email__c = objUnderWriting.opportunity__r.Co_Applicant__r.PersonEmail;
                                
                                
                                objDraw.Total_Amount_Drawn__c = objUnderWriting.Total_Amount_Drawn__c;
                                objDraw.Maximum_loan_amount__c = objUnderWriting.opportunity__r.Combined_Loan_Amount__c;
                                objDraw.Remaining_amount_for_additional_payment__c =  objUnderWriting.Amount_Remaining__c - drawRequestIterate.amount;
                                objDraw.Installer_Account_Name__c = objUnderWriting.opportunity__r.Installer_Account__r.Name;
                                objDraw.Sales_Rep_Name__c = objUnderWriting.Opportunity__r.Owner.Name;
                                objDraw.Customer_Name__c = objUnderWriting.opportunity__r.Account.Name;
                                objDraw.Installer_Email__c = objUnderWriting.opportunity__r.Installer_Account__r.Installer_Email__c;
                                objDraw.Operations_Email__c = objUnderWriting.opportunity__r.Installer_Account__r.Operations_Email__c;
                                objDraw.Opp_Hash_Id__c = objUnderWriting.opportunity__r.Hash_Id__c;
                                
                                
                                insrtDrawRequestList.add(objDraw);
                                updOpprList.add(oppUpdate);
                                Underwriting_file__c underwritingRec = new Underwriting_file__c(id = objUnderWriting.id);
                                underwritingRec.Project_Status_Detail__c = 'Payment Requested';
                                updateUnderwritingList.add(underwritingRec);
                            }
                            if(insrtDrawRequestList!=null && !insrtDrawRequestList.isEmpty()){
                                insert insrtDrawRequestList;
                                
                                update updateUnderwritingList;
                                
                                update updOpprList;
                                System.debug('### Draw request has been placed.');
                                
                                
                                 oppId=objUnderWriting.opportunity__r.id;
                                
                            }
                        }
                    }
                    
                    }  
                    
                    set<Id> oppIdSet = new set<Id>();
                    oppIdSet.add(oppId);
                    unifiedRespBean = SLFUtility.getUnifiedBean(oppIdSet,'Orange',false);
                    unifiedRespBean.returnCode = '200';
                    //unifiedRespBean.message ='Thank you for your payment request. We will now request authorization from your customer via text (if applicable) and e-mail for the full requested loan amount of ' + strAmount;
                    unifiedRespBean.message ='Thank you for your payment request. We will now request authorization from your customer via text (if applicable) and e-mail for the full requested loan amount.';
                    system.debug('#### unifiedRespBean ==>:'+unifiedRespBean);
                    
                }
                else{
                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                    iRestRes = (IRestResponse)unifiedRespBean;
                    return iRestRes;
                }
            }
        }}
        catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedRespBean.error.add(errorMsg);
            unifiedRespBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedRespBean;
            system.debug('### DrawRequestDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('DrawRequestDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### iRestRes '+iRestRes);
        system.debug('### Exit from transformOutput() of '+DrawRequestDataServiceImpl.class);
        iRestRes = (IRestResponse)unifiedRespBean;
        return iRestRes;
    }
    
    public void getErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
}