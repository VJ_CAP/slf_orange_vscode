@isTest
private class UFTriggerSupport_Test 
{
    
    @isTest static void test1 () 
    {
       //get Custom setting data.
        SLFUtilityTest.createCustomSettings();   
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a2;

        Contact c1 = new Contact (AccountId=a1.Id, Lastname = 'Test 1');
        insert c1;

        Contact c2 = new Contact (AccountId=a1.Id, Lastname = 'Test 2');
        insert c2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Product_Tier__c = '0');
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            Primary_Contact__c = c1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;
        
        
        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
       
        uf1.ST_Amount_Financed__c = 1001;
        update uf1;
         
        o1.Primary_Contact__c = c2.Id;
        update o1;
    }   
}