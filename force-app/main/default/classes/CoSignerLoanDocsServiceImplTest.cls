@isTest
public class CoSignerLoanDocsServiceImplTest{
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
        
    }
  
    /**
    *
    * Description: This method is used to cover DisclosureServiceImpl and DisclosureDataServiceImpl 
    *
    */   
    
    private testMethod static void CosignerLoanDocsTest(){
        SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
        endpoint1.Name = 'DocusignAPI';
        endpoint1.Endpoint_URL__c =  'https://demo.docusign.net/restapi/v2/accounts/3664863/envelopes/';
        endpoint1.username__c = '649e38c4-139b-4c0b-b1ed-8b1bbe6a28ef';
        endpoint1.password__c = 'Sunlight123!';
        endpoint1.APIKey__c = '6760cb64-1f57-4d76-a3b4-ad32a005c19e';
        insert endpoint1;
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account instlrAccObj = [select id, Credit_Waterfall__c, Risk_Based_Pricing__c from Account 
                                                where id=:loggedInUsr[0].contact.AccountId];
        instlrAccObj.Credit_Waterfall__c = true;
        instlrAccObj.Risk_Based_Pricing__c = true;
        update instlrAccObj;
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1]; 
        
        dsfs__DocuSign_Status__c docStatus = new dsfs__DocuSign_Status__c();
        docStatus.dsfs__Opportunity__c = opp.id;
        docStatus.dsfs__Company__c = acc.id; 
        docStatus.dsfs__Envelope_Status__c = 'Sent';
        insert docStatus;  
             
        Map<String, String> loanDocsMap= new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            String getcoSign;
            Test.starttest();
            List<dsfs__DocuSign_Recipient_Status__c> docList = new List<dsfs__DocuSign_Recipient_Status__c>();
            
            dsfs__DocuSign_Recipient_Status__c objDocStatus = new dsfs__DocuSign_Recipient_Status__c();
            objDocStatus.Name = 'Test';
            objDocStatus.dsfs__DocuSign_Recipient_Email__c = 'Test@gmail.com';
            objDocStatus.dsfs__DocuSign_Routing_Order__c = 2;
            objDocStatus.dsfs__Recipient_Status__c = 'Sent';
            objDocStatus.dsfs__Parent_Status_Record__c = docStatus.id;
            objDocStatus.dsfs__DocuSign_Recipient_Id__c = '68105689-3AF6-4554-911A-30B49A3D671D';
            objDocStatus.dsfs__Envelope_Id__c = '50A07ECD-C930-4042-8C0A-2BF592287C02';
            docList.add(objDocStatus);
            insert docList;
            system.debug('********Doc Recipient Status List******'+docList);
            getcoSign =  '{"projects":[{"envelopeId":"'+objDocStatus.dsfs__Envelope_Id__c+'"}]}';
            MapWebServiceURI(loanDocsMap,getcoSign,'getcosignerloandocs');
            Test.stoptest();
        }
        
    }
        
       /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}