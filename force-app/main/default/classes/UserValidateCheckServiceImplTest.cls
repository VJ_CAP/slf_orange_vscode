/******************************************************************************************
* Description: Test class to cover UserValidateCheckServiceImpl class 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri              07/29/2019            Created
******************************************************************************************/

@isTest
public class UserValidateCheckServiceImplTest {
    @testsetup static void createtestdata(){
        // SLFUtilityDataSetup.initData();
        insertUserData();
    }
    private static void insertUserData(){
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
                        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        insert trgLst;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
                
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id,Reset_Password_Flag__c = true);
        insert con; 
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', 
        ProfileId = p.Id, 
        Default_User__c = true,                            
        //UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id,Question_Counter__c = 0);
        
        insert portalUser;
    }
    /**
*
* Description: This method is used to cover UserValidateCheckServiceImpl
*
*/
    private testMethod static void userprecheckTest(){
        
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c from User where Email =: 'testc@mail.com'];
       
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            string userObjValidJsonreq;   
            Map<String, String> userObjValidMap = new Map<String, String>();
            
            userObjValidJsonreq = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"'+loggedInUsr[0].Hash_Id__c+'"}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"erter","reportsTo":"123"}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"id":"1233414","firstName":"Test","lastName":"User","email":"testMMc@mail.com","phone":"9263693291","isActive":false,"title":"terrte","role":"42434334","reportsTo":"33453","HashId":"2343"}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            //To cover exception block
            UserValidateCheckServiceImpl ValidateCheckSrvImpl = new UserValidateCheckServiceImpl();
            ValidateCheckSrvImpl.fetchData(null);
            
            Test.stopTest();
            
        }    
    }
     /**
*
* Description: This method is used to cover UserValidateCheckServiceImpl
*
*/
    private testMethod static void userQnAprecheckTest(){
        
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c from User where Email =: 'testc@mail.com'];
        
        loggedInUsr[0].Question1__c='Test';
        loggedInUsr[0].Question2__c='Test';
        loggedInUsr[0].Question3__c='Test';
        loggedInUsr[0].Answer1__c='Test';
        loggedInUsr[0].Answer2__c='Test';
        loggedInUsr[0].Answer3__c='Test';
        
        Update loggedInUsr[0];
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            string userObjValidJsonreq;   
            Map<String, String> userObjValidMap = new Map<String, String>();
            
            userObjValidJsonreq = '{"users":[{"HashId":"'+loggedInUsr[0].Hash_Id__c+'"}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"questionNumber":1,"HashId":"'+loggedInUsr[0].Hash_Id__c+'","selectedQuestionsAndAnswers": [{ "question": "What is your grandmother maiden name?", "answer": "test111"}]}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"questionNumber":2,"HashId":"'+loggedInUsr[0].Hash_Id__c+'","selectedQuestionsAndAnswers": [{ "question": "What is your grandmother maiden name?", "answer": "test111"}]}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"questionNumber":3,"HashId":"'+loggedInUsr[0].Hash_Id__c+'","selectedQuestionsAndAnswers": [{ "question": "What is your grandmother maiden name?", "answer": "test111"}]}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            //To cover exception block
            UserValidateCheckServiceImpl ValidateCheckSrvImpl = new UserValidateCheckServiceImpl();
            ValidateCheckSrvImpl.fetchData(null);
            
            Test.stopTest();
            
        }    
    }
     private testMethod static void userQnAprecheckTest2(){
        
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c from User where Email =: 'testc@mail.com'];
        
        loggedInUsr[0].Question1__c='Test';
        loggedInUsr[0].Question2__c='Test';
        loggedInUsr[0].Question3__c='Test';
        loggedInUsr[0].Answer1__c='Test';
        loggedInUsr[0].Answer2__c='Test';
        loggedInUsr[0].Answer3__c='Test';
        
        Update loggedInUsr[0];
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            string userObjValidJsonreq;   
            Map<String, String> userObjValidMap = new Map<String, String>();
            
            userObjValidJsonreq = '{"users":[{"HashId":"'+loggedInUsr[0].Hash_Id__c+'"}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"questionNumber":1,"HashId":"'+loggedInUsr[0].Hash_Id__c+'","selectedQuestionsAndAnswers": [{ "question": "test", "answer": "test"}]}]}';
            MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"questionNumber":2,"HashId":"'+loggedInUsr[0].Hash_Id__c+'","selectedQuestionsAndAnswers": [{ "question": "test", "answer": "test"}]}]}';
            //MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            userObjValidJsonreq = '{"users":[{"questionNumber":3,"HashId":"'+loggedInUsr[0].Hash_Id__c+'","selectedQuestionsAndAnswers": [{ "question": "test", "answer": "test"}]}]}';
           // MapWebServiceURI(userObjValidMap,userObjValidJsonreq,'userprecheck');
            
            //To cover exception block
            UserValidateCheckServiceImpl ValidateCheckSrvImpl = new UserValidateCheckServiceImpl();
            ValidateCheckSrvImpl.fetchData(null);
            
            Test.stopTest();
            
        }    
    }
    
     /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}