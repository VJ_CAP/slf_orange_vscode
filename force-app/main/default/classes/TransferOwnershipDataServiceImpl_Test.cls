/******************************************************************************************
* Description: Test class to cover TransferOwnershipDataServiceImpl
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri            09/16/2019              Created
******************************************************************************************/
@isTest
public class TransferOwnershipDataServiceImpl_Test{    
    
    @testsetup static void createtestdata(){
        //SLFUtilityDataSetup.initData();
       testDataInsert();
        
    }
    private static void testDataInsert(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
                
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
                        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.Credit_Run__c = true;
        acc.Default_FNI_Loan_Amount__c = 25000;
        acc.Risk_Based_Pricing__c = true;
        acc.Credit_Waterfall__c = true;
        acc.Eligible_for_Last_Four_SSN__c = true;
        acc.Phone = '9874563210';
        acc.Push_Endpoint__c = 'www.google.com';
        acc.Subscribe_to_Push_Updates__c = true;
        insert acc;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
 
        //Insert SLF Product Record
            List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            //insert prod;
            prodList.add(prod);
            
            insert prodList;
            
            //Insert opportunity record
            List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            //insert personOpp;
            oppList.add(personOpp);
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = personAcc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            opp.ProductTier__c = '0';         
            opp.Type_of_Residence__c  = 'Own';
            opp.language__c = 'Spanish';
            opp.Project_Category__c = 'Solar';
            //insert opp;
            oppList.add(opp);
            
            insert oppList;
            
            /*Underwriting_File__c undStpbt = new Underwriting_File__c();
            undStpbt.Opportunity__c = opp.id;
            //undStpbt.Opportunity__c = opp.id;           
            insert undStpbt;*/
            
            list<Underwriting_File__c> uwList = new list<Underwriting_File__c >();
        
            Underwriting_File__c underWrFilRec = new Underwriting_File__c();
            underWrFilRec.name = 'Underwriting test';
            underWrFilRec.ACT_Review__c ='Yes';
            underWrFilRec.Opportunity__c = opp.Id;
            underWrFilRec.Project_Status__c = 'Project Withdrawn';
            underWrFilRec.Withdraw_Reason__c = 'Credit Expired';
            underWrFilRec.Declined_Withdrawn_Date__c = system.today();
            uwList.add(underWrFilRec);
            
            Underwriting_File__c underWrFilRec1 = new Underwriting_File__c();
            underWrFilRec1.name = 'Underwriting test';
            underWrFilRec1.ACT_Review__c ='Yes';
            underWrFilRec1.Opportunity__c = opp.Id;
            underWrFilRec1.Project_Status__c = 'Pending Loan Docs';
            uwList.add(underWrFilRec1);
            insert uwList;
            
            //Insert Credit Record
            List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.id;
            //credit.Primary_Applicant__c = personAcc.id;
            //credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = prod.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'New';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            //insert credit;
            creditList.add(credit);
             insert creditList;
          
    }
 
   /**
*
* Description: This method is used to cover TransferOwnershipServiceImpl and TransferOwnershipDataServiceImpl 
*
*/   
    private testMethod static void TransferOwnershipDataServiceImplTest(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        /*List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);*/
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,SLF_Product__c,Installer_Account__c,Installer_Account__r.parentid from opportunity where name = 'opName' LIMIT 1]; 
        system.debug('*****opp List****'+opp);
        Product__c prod = [select Id,Name from Product__c where Name =: 'testprod' LIMIT 1];
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1]; 
        
         List<user> userobj = new List<User>();
        
        userobj = [SELECT Id, Username, LastName, IsActive, Hash_Id__c FROM User where Hash_Id__c = '123'];
        system.debug('&&&userObj' +userobj);
        
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        update acc;
        
         //Opportunity opp = new Opportunity();
         opp.Co_Applicant__c = acc.id;
          opp.Installer_Account__c = acc.id;
           update opp;

        Account acc1 = new Account();
        acc1.name = 'Test Account1';
        acc1.Installer_Email__c = 'testemail@gmail.com';
        acc1.Solar_Enabled__c=true;
        insert acc1;
        
        Contact con1 = new Contact();
        con1.lastname = 'Test Contact1';
        con1.accountid = acc1.id;
        insert con1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        User portalUser1 = new User(alias = 'testcc', Email='testcc@mail.com', IsActive = true,Hash_Id__c='1234',
        EmailEncodingKey='UTF-8', LastName='testt', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        timezonesidkey='America/Los_Angeles', 
        username='testclasss@mail.com',ContactId = con1.Id);
        insert portalUser1;
        
        
 
        
            Test.starttest();
            Map<String, String> transferOwnershipmap = new Map<String, String>();
            
            String transferOwnership ;
            
            transferOwnership= '{"projectIds":"","externalIds":"","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"","hashIds":"","users":[{"newOwnerId":"12323","newOwnerHashId":"12333","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"","hashIds":"","users":[{"newOwnerId":"12323","newOwnerHashId":"12333","hashId":"w22","Id":"12321344"}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":"'+loggedInUsr[0].id+'"}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"'+loggedInUsr[0].Hash_Id__c+'","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":"'+loggedInUsr[0].id+'"}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"12323","hashIds":"'+opp.Hash_Id__c+'","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"12323","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"12323","hashIds":"'+opp.Hash_Id__c+'","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"'+opp.Hash_Id__c+'","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"","users":[{"newOwnerId":"1232133","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"","users":[{"newOwnerId":"1232133","newOwnerHashId":"tee11","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');            
            
            transferOwnership= '{"projectIds":"","externalIds":"'+opp.id+'","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"tee11","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"","hashIds":"'+opp.Hash_Id__c+'","users":[{"newOwnerId":"","newOwnerHashId":"tee11","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"tee11","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"","users":[{"newOwnerId":"'+loggedInUsr[0].id+'","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+opp.id+'","externalIds":"","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":"'+loggedInUsr[0].id+'","ownerEmail":"'+loggedInUsr[0].email+'"}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"'+userobj[0].id+'","externalIds":"","hashIds":"123","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"123","Id":"'+userobj[0].id+'"}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"'+opp.Partner_Foreign_Key__c+'","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds": "wrwr"}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            transferOwnership= '{"projectIds":"","externalIds":"'+opp.Partner_Foreign_Key__c+'","hashIds":"448","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":""}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
             transferOwnership= '{"projectIds":"","externalIds":"94698","hashIds":"","users":[{"newOwnerId":"","newOwnerHashId":"","hashId":"","Id":"","ownerEmail":"'+loggedInUsr[0].email+'"}]}';
            MapWebServiceURI(transferOwnershipmap,transferOwnership,'transferownership');
            
            TransferOwnershipServiceImpl transferOwn = new TransferOwnershipServiceImpl();
            transferOwn.fetchData(null);
            TransferOwnershipDataServiceImpl.updateChildRecords(opp.id,loggedInUsr[0]);
            /*UnifiedBean  objbean = new UnifiedBean ();
            system.debug('****Portal User Email*****'+portalUser1.email);
            objbean.projects[0].ownerEmail = portalUser1.email;  */
            TransferOwnershipDataServiceImpl.assignOwnerInfo(null);
            Test.stoptest();
        
    }
    
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}