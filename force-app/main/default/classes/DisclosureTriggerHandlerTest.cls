@isTest(seeAllData=false)
public class DisclosureTriggerHandlerTest {
    private testMethod static void DisclosureTriggerHandlerTest(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();       
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Disclosures__c';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        insert trgLst;
        
        Disclosures__c disObj1 = new Disclosures__c(Disclosure_Text__c='Test Text',
                                                  Active__c = true,
                                                  Type__c = 'RBP');
        insert disObj1;
        Disclosures__c disObj3;
        try{
        Disclosures__c disObj2 = new Disclosures__c(Disclosure_Text__c='Test Text',
                                                  Active__c = true,
                                                  Type__c = 'RBP');
        insert disObj2;
        }
        catch(Exception ex){
                disObj3 = new Disclosures__c(Disclosure_Text__c='Test Text',
                                                  Active__c = false,
                                                  Type__c = 'RBP');
        		insert disObj3;
        }
        
        try{
        disObj3.Active__c = true;
        update disObj3;
        }catch(Exception ex){}
    }
}