/**
* Description: REST Data service interface for converting input/out request/response data to application specific
*
*   Modification Log:
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public interface IRestDataService{
    //Convert input request wrapper (JSON) to salesforce sobject
    IRestRequest transformInput(IDataObject req);
    
    //Convert out response from salesforce sobject to wrapper (JSON)
    IRestResponse transformOutput(IDataObject res);
}