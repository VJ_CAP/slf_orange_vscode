/**
* Description: Test class for ContactTrg trigger and ContactTriggerHandler class. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         12/01/2017          Created
******************************************************************************************/
@isTest
public class ContactTriggerHandlerTest {
    
    public testMethod static void testContactTrgHandler(){
    
         //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        
         //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        //create new Account
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'Test FNI';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c=true;
        insert acc;   
        
        
        //create new Contact
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        con.Is_Default_Owner__c = true;
        insert con;
        
        con.Is_Default_Owner__c = false;
        update con;
        
        con.Is_Default_Owner__c = true;
        update con;
        
        System.debug('test:'+[select id,ConnectionReceived.ConnectionName from Contact where id =:con.Id].ConnectionReceived.ConnectionName);
        
         //create new Contact con1
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acc.Id);
        con1.Is_Default_Owner__c = true;
        insert con1;
        System.debug('test1:'+[select id,ConnectionReceived.ConnectionName from Contact where id =:con1.Id].ConnectionReceived.ConnectionName);
        
        
    }
}