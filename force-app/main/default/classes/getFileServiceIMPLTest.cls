@isTest(seeAllData=false)
public class getFileServiceIMPLTest {
    @testsetup static void createtestdata(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        insert trgLst;
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.Box_Folder_ID_Net_Out__c='85779699960';
        acc.Box_Folder_ID_Remittance__c='66828620442';
        acc.Box_Folder_ID_HI_Remittance__c='70700686068';
        insert acc;
        
        Opportunity oppObj = new Opportunity();
        oppObj.name = 'OppNew';
        oppObj.CloseDate = system.today();
        oppObj.StageName = 'New';
        oppObj.AccountId = acc.id;
        oppObj.Installer_Account__c = acc.id;
        oppObj.Install_State_Code__c = 'CA';
        oppObj.Install_Street__c ='Street';
        oppObj.Install_City__c ='InCity';
        oppObj.Combined_Loan_Amount__c = 10000;
        oppObj.Partner_Foreign_Key__c = 'TCU||1234';
        oppObj.Co_Applicant__c = acc.id;
        insert oppObj;
        
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
    }
    
    private testMethod static void getFileServiceIMPLTest1(){
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.Box_Folder_ID_Net_Out__c,contact.Account.Box_Folder_ID_Remittance__c,contact.Account.Box_Folder_ID_HI_Remittance__c from User where Email =: 'testc@mail.com'];
        System.debug('loggedinuser details'+loggedInUsr);
        System.debug('loggedinuser details'+loggedInUsr[0].contact.Account.Box_Folder_ID_Net_Out__c);
        Opportunity opp=[Select Id,Name from Opportunity WHERE AccountID=:acc.id];
        
        System.runAs(loggedInUsr[0]){
            Map<String, String> getfileserviceimplMap = new Map<String, String>();
           
            String req;
            req='{"id" : "85779699960"}';
            SLFRestDispatchTest.MapWebServiceURI(getfileserviceimplMap,req,'finopsgetfileV2');
            
            req='{"id" : ""}';
            SLFRestDispatchTest.MapWebServiceURI(getfileserviceimplMap,req,'finopsgetfileV2');
        } 
    }
    
    private testMethod static void getFileServiceIMPLTest2(){
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.Box_Folder_ID_Net_Out__c,contact.Account.Box_Folder_ID_Remittance__c,contact.Account.Box_Folder_ID_HI_Remittance__c from User where Email =: 'testc@mail.com'];
        
        
        System.runAs(loggedInUsr[0]){
              Map<String, String> getfileserviceimplMap = new Map<String, String>();
            
            String req;
            req=null;
            SLFRestDispatchTest.MapWebServiceURI(getfileserviceimplMap,'ghj','finopsgetfileV2');
            
        } 
    }
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        
    }
}