@isTest(seeAllData=false)
public class FNIFacilityDecisionServiceImplTest{    
    
    private testMethod static void CreditRefactorTest(){
       
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);
        TriggerFlags__c trgflag3 = new TriggerFlags__c();
        trgflag3.Name ='SLF_Credit__c';
        trgflag3.isActive__c =true;
        trgrList.add(trgflag3);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='Contact';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        TriggerFlags__c trgflag5 = new TriggerFlags__c();
        trgflag5.Name ='User';
        trgflag5.isActive__c =true;
        trgrList.add(trgflag5);
        TriggerFlags__c trgflag6 = new TriggerFlags__c();
        trgflag6.Name ='Quote';
        trgflag6.isActive__c =true;
        trgrList.add(trgflag6);
        TriggerFlags__c trgflag7 = new TriggerFlags__c();
        trgflag7.Name ='State_Allocation__c';
        trgflag7.isActive__c =false;
        trgrList.add(trgflag7);
        TriggerFlags__c trgflag8 = new TriggerFlags__c();
        trgflag8.Name ='Underwriting_File__c';
        trgflag8.isActive__c =false;
        trgrList.add(trgflag8);
        TriggerFlags__c trgflag9 = new TriggerFlags__c();
        trgflag9.Name ='Funding_Data__c';
        trgflag9.isActive__c =false;
        trgrList.add(trgflag9);
        insert trgrList;
        
        //Insert Custom Endpoints Custom Setting Record
        List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
        SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
        endpoint1.Name = 'Pricing';
        endpoint1.Endpoint_URL__c =  'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
        endpoint.add(endpoint1);
        SLF_Boomi_API_Details__c endpoint2 = new SLF_Boomi_API_Details__c();
        endpoint2.Name = 'BoomiToAWSPricing';
        endpoint2.Endpoint_URL__c =  'https://test.connect.boomi.com/ws/rest/v1/dev/pricing/callaws/';
        endpoint.add(endpoint2);
        
        
        SLF_Boomi_API_Details__c apidetails = new SLF_Boomi_API_Details__c();
        apidetails.Name ='Boomi QA';
        apidetails.Username__c ='sunlightfinancial-WSHA3Z.N29SCE';
        apidetails.Password__c ='2458a5de-d689-4708-a4fb-d3594c067610';
        apidetails.Endpoint_URL__c ='https://test.connect.boomi.com/ws/soap12/prequal';
        apidetails.viaBoomi__c =true;
        endpoint.add(apidetails);
        
        SLF_Boomi_API_Details__c endpoint3 = new SLF_Boomi_API_Details__c();
        endpoint3.Name = 'FNI QA';
        endpoint3.Username__c =  'salesforceuser';
        endpoint3.Password__c = 'RC8N1CreKnz4rm2kMTCj';
        endpoint.add(endpoint3);
        
        SLF_Boomi_API_Details__c endpoint4 = new SLF_Boomi_API_Details__c();
        endpoint4.Name = 'FNI Prod';
        endpoint4.Username__c =  'salesforceuser';
        endpoint4.Password__c = 'AmtPHqSlK9sB6NXjSyq4';
        endpoint.add(endpoint4);
        insert endpoint;  
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';
        accObj.Solar_Enabled__c = true;        
        accObj.Type = 'Facility';
        insert accObj;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = accObj.Id);
        insert con;  
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        System.runAs(loggedInUsr[0])
        {
            Product__c prdObj = new Product__c(Name='Test',
                                               Product_Loan_Type__c='Solar',
                                               Installer_Account__c = accObj.Id,
                                               State_Code__c='CA',
                                               FNI_Min_Response_Code__c=2,
                                               Product_Display_Name__c = 'Display Name',
                                               Qualification_Message__c = 'Message',
                                               Term_mo__c = 60,
                                               APR__c = 20,
                                               ACH__c = true,
                                               Internal_Use_Only__c = FALSE,
                                               Is_Active__c = TRUE,
                                               Long_Term_Facility__c = 'CVX',
                                               Product_Tier__c = '0');
            insert prdObj;
            
            Opportunity oppObj = new Opportunity(Name = 'OppName',
                                                 AccountId=accObj.Id,
                                                 StageName='Qualified',
                                                 CloseDate=system.today(),
                                                 SLF_Product__c = prdObj.ID,
                                                 Install_State_Code__c = 'CA',
                                                 Synced_Quote_Id__c = 'qId',
                                                 Desync_Bypass__c = true,isACH__c = true);
            insert oppObj;        
                    
            SLF_Credit__c slfCreditObj = new SLF_Credit__c();
            slfCreditObj.Opportunity__c = oppObj.id;
            slfCreditObj.Primary_Applicant__c = accObj.id;
            slfCreditObj.Co_Applicant__c = accObj.id;
            slfCreditObj.SLF_Product__c = prdObj.Id ;
            slfCreditObj.Total_Loan_Amount__c = 100;
            slfCreditObj.Status__c = 'New';
            slfCreditObj.LT_FNI_Reference_Number__c = '123';
            slfCreditObj.Application_ID__c = '123';
            insert slfCreditObj;
            
            Prequal__c prqlObj = new Prequal__c(Opportunity__c = oppObj.Id,
                                                Installer_Account__c =accObj.Id,
                                                Middle_Name__c = 'K',
                                                SSN__c = '123456789',
                                                Term_mo__c = 60,
                                                DOB__c = system.today(),
                                                Mailing_State__c='CA',
                                                Mailing_Postal_Code__c = '25846',
                                                Annual_Income__c = 12300,
                                                Lending_Facility__c = 'CVX,TCU',
                                                Last_Name__c = 'Test',
                                                Employment_Years__c=5,
                                                Employment_Months__c = 1,
                                                Co_LastName__c = 'Test',
                                                Co_FirstName__c = 'Test',
                                                Co_Middle_Initial__c = 'w',
                                                Co_SSN__c = '123456758',
                                                Co_Date_of_Birth__c = System.today(),
                                                Co_Mailing_Street__c = 'Street',
                                                Co_Mailing_City__c = 'City',
                                                Co_Mailing_State__c = 'CA',
                                                Co_Mailing_Postal_Code__c = '45678',
                                                Co_Employer_Name__c = 'Employee',
                                                Co_Annual_Income__c = 200000,
                                                Co_Employment_Years__c = 1,
                                                Co_Employment_Months__c = 2,
                                                APR__c = 20
                                                );
            
            insert prqlObj;     
            
            Quote qObj = new Quote(name = 'quote',
            OpportunityId = oppObj.Id);
            insert qObj;
          
            Map<String, String> createincenMap = new Map<String, String>();
            
            String creditRefactor ;
            Test.startTest();
            //NY exclusion Zipcode
            creditRefactor = '{"prequal" : {"id" : "'+prqlObj.Id+'"},"projects" : [{"credits" : [{"id" : "'+slfCreditObj.Id+'","fniSoftResp" : "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Header/><soap:Body><ns3:RESPONSE xmlns=\"http://tko.fni.com/application/transaction.xsd\" xmlns:ns2=\"http://tko.fni.com/application/request.xsd\" xmlns:ns3=\"http://tko.fni.com/application/response.xsd\"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage/><ns3:FNIReferenceNumber>20000005363214</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-06-11T10:06:27-05:00</ns3:DecisionDateTime><ns3:FirstName>BARBARA</ns3:FirstName><ns3:LastName>WILLEMSEN</ns3:LastName><ns3:TransactionID>13134AB9-E9DD-462D-B3A5-1D607DAB1AB4</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2/><ns3:StipRsn3/><ns3:StipRsn4/><ns3:LineAssignmentAmt>0</ns3:LineAssignmentAmt><ns3:DTI>0</ns3:DTI><ns3:LowFico>812</ns3:LowFico><ns3:DebtForMax>20</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>32479</ns3:MaxPayment><ns3:MaxDTI>750</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,creditRefactor,'fnisoftpulldecision');         
            
            
            creditRefactor = '{"prequal" : {"id" : "'+prqlObj.Id+'"},"projects" : [{"credits" : [{"id" : "'+slfCreditObj.Id+'","fniSoftResp" : "<?xml version=\\"1.0\\" encoding=\\"UTF-8\\" standalone=\\"no\\"?><soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"><soap:Header/><soap:Body><ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage/><ns3:FNIReferenceNumber>20000005363214</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-06-11T10:06:27-05:00</ns3:DecisionDateTime><ns3:FirstName>BARBARA</ns3:FirstName><ns3:LastName>WILLEMSEN</ns3:LastName><ns3:TransactionID>13134AB9-E9DD-462D-B3A5-1D607DAB1AB4</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2/><ns3:StipRsn3/><ns3:StipRsn4/><ns3:LineAssignmentAmt>0</ns3:LineAssignmentAmt><ns3:DTI>0</ns3:DTI><ns3:LowFico>812</ns3:LowFico><ns3:DebtForMax>20</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>32479</ns3:MaxPayment><ns3:MaxDTI>750</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,creditRefactor,'fnisoftpulldecision');
             
                        
            State_Allocation__c saObj = new State_Allocation__c(Installer_Account__c=accObj.Id, 
                                                            State_Code__c='CA',
                                                            Facility_1__c=accObj.Id,
                                                            Facility_1_Allocation__c = 10,
                                                            Facility_2_Allocation__c = 10,
                                                            Facility_3_Allocation__c = 10,
                                                            Facility_4_Allocation__c = 10,
                                                            Facility_5_Allocation__c = 10,
                                                            Facility_6_Allocation__c = 10,
                                                            Facility_7_Allocation__c = 10,
                                                            Facility_8_Allocation__c = 10,
                                                            Facility_9_Allocation__c = 20);
            insert saObj;
        
            creditRefactor = '{"prequal" : {"id" : "'+prqlObj.Id+'"},"projects" : [{"credits" : [{"id" : "'+slfCreditObj.Id+'","fniSoftResp" : "<?xml version=\\"1.0\\" encoding=\\"UTF-8\\" standalone=\\"no\\"?><soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"><soap:Header/><soap:Body><ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage/><ns3:FNIReferenceNumber>20000005363214</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-06-11T10:06:27-05:00</ns3:DecisionDateTime><ns3:FirstName>BARBARA</ns3:FirstName><ns3:LastName>WILLEMSEN</ns3:LastName><ns3:TransactionID>13134AB9-E9DD-462D-B3A5-1D607DAB1AB4</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2/><ns3:StipRsn3/><ns3:StipRsn4/><ns3:LineAssignmentAmt>0</ns3:LineAssignmentAmt><ns3:DTI>0</ns3:DTI><ns3:LowFico>812</ns3:LowFico><ns3:DebtForMax>20</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>32479</ns3:MaxPayment><ns3:MaxDTI>750</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,creditRefactor,'fnisoftpulldecision');            
            
            creditRefactor = null;
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,creditRefactor,'fnisoftpulldecision');
           
            oppObj.Synced_Quote_Id__c = qObj.Id;
            update oppObj;
            
            creditRefactor = '{"prequal" : {"id" : "'+prqlObj.Id+'"},"projects" : [{"credits" : [{"id" : "'+slfCreditObj.Id+'","fniSoftResp" : "<?xml version=\\"1.0\\" encoding=\\"UTF-8\\" standalone=\\"no\\"?><soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"><soap:Header/><soap:Body><ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage/><ns3:FNIReferenceNumber>20000005363214</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-06-11T10:06:27-05:00</ns3:DecisionDateTime><ns3:FirstName>BARBARA</ns3:FirstName><ns3:LastName>WILLEMSEN</ns3:LastName><ns3:TransactionID>13134AB9-E9DD-462D-B3A5-1D607DAB1AB4</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2/><ns3:StipRsn3/><ns3:StipRsn4/><ns3:LineAssignmentAmt>0</ns3:LineAssignmentAmt><ns3:DTI>0</ns3:DTI><ns3:LowFico>812</ns3:LowFico><ns3:DebtForMax>20</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>23813</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>32479</ns3:MaxPayment><ns3:MaxDTI>750</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,creditRefactor,'fnisoftpulldecision'); 
            Test.stopTest(); 
           
        }
        
    }
}