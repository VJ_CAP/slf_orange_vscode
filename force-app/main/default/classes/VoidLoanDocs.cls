/**
* Description: RevertChangeOrder related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Srikanth             10/17/2017                 Created
******************************************************************************************/
global class VoidLoanDocs {
  @future(callout = true)
  public static void voiddocs(list<Id> recordIds) {
        system.debug('### Entered into voiddocs() of ' + VoidLoanDocs.class);
        list<SLF_Boomi_API_Details__c> clist = SLF_Boomi_API_Details__c.getAll().values();
        String resLst = '';
      	String IdList = '';
        for(SLF_Boomi_API_Details__c c : SLF_Boomi_API_Details__c.getAll().values())
        {
        String apiName = c.Name;
            if(apiName.equalsIgnoreCase('DocusignAPI'))
            {
                for(dsfs__Docusign_Status__c d : [select Id, dsfs__DocuSign_Envelope_ID__c from dsfs__Docusign_Status__c where id IN: recordIds])
                {
                    Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    request.setEndpoint(c.Endpoint_URL__c+d.dsfs__DocuSign_Envelope_ID__c);
                    request.setMethod('PUT');
                    request.setHeader('Content-Type', 'application/json');
                    request.setHeader('X-DocuSign-Authentication','<DocuSignCredentials><Username>'+c.username__c+'</Username><Password>'+c.password__c+'</Password><IntegratorKey>'+c.APIKey__c+'</IntegratorKey></DocuSignCredentials>');
                    String body = '{ "status": "voided", "voidedReason": "Change Order was canceled." }';
                    request.setBody(body);
                    if(!Test.isRunningTest()){
                    HttpResponse response = http.send(request);
                    if (response.getStatusCode() != 200) {
                    // insert error log
                        resLst+= d.id+'_'+response.getStatus()+'@@@';
                        IdList+=(IdList==''?d.id:','+d.id);
                    }
                    system.debug('### void envelopes - '+response);
                }   }            
            }   
        }
        if(string.isNotBlank(resLst)){
            ErrorLogUtility.writeLog('VoidLoanDocs.voiddocs()',26,'voiddocs()',resLst,'Error',IdList);
            
        }
         system.debug('### Exit from voiddocs() of ' + VoidLoanDocs.class);
    }
}