/**
* Description: Applicant Update related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/

public class ApplicantObjDataServiceImpl extends RestDataServiceBase{
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
            system.debug('### Entered into transformOutput() of '+ApplicantObjDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        
        UnifiedBean unifbean = new UnifiedBean(); 
        unifbean.projects = new List<UnifiedBean.OpportunityWrapper>();
        unifbean.error = new list<UnifiedBean.errorWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        projectWrap.applicants = new List<UnifiedBean.ApplicantWrapper>();
        projectWrap.stips = new List<UnifiedBean.StipsAndStatusWrapper>();
        projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
        projectWrap.systemDesigns = new List<UnifiedBean.SystemDesignWrapper>();
        projectWrap.applicants = new List<UnifiedBean.ApplicantWrapper>();
        List<Account> accLst = new List<Account>();
        List<Underwriting_File__c> uwfile=new List<Underwriting_File__c>();

        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        Savepoint sp = Database.setSavepoint();
        try
        {   
            boolean creditcheckflag;
            if(null != reqData.projects)
            {
                if(null != reqData.projects[0].applicants)
                {
                    
                    //Updated by Ravi as part of ORANGE-3354 Start
                    List<user> loggedInUsr = [select Id, email, contactid, contact.FirstName, contact.LastName from User where Id =: userinfo.getUserId()];
                    Map<String, Email_Validation_Keywords__c> keyWrds = Email_Validation_Keywords__c.getAll();
                    //Updated by Ravi as part of ORANGE-3354 End
                    
                    List<UnifiedBean.ApplicantWrapper> AccountRecLst = reqData.projects[0].applicants;
                    Map<string,Account> AccountMap = new Map<string,Account>();
                    List<Account> Accountlst = new List<Account>();
                    set<string> applicantset = new set<string>();
                    for(UnifiedBean.ApplicantWrapper AccountIterate : AccountRecLst)
                    {
                        if(string.isNotBlank(AccountIterate.id))
                            applicantset.add(AccountIterate.Id);
                    }
                    if(applicantset.Size()>1)
                    {
                       errMsg = ErrorLogUtility.getErrorCodes('239',null);
                       iolist = new list<DataValidator.InputObject>{};
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        unifBean.error.add(errorMsg);
                        unifBean.returnCode = '239';
                        iRestRes = (IRestResponse)unifbean;
                        return iRestRes;
                    }
                    else if(!applicantset.isEmpty())
                    {
                        Accountlst = [Select id,firstName,lastName,SSN__c,MiddleName,PersonEmail,PersonBirthdate,Phone,
                                        personOtherPhone,BillingCountry,BillingStreet,BillingCity,BillingStateCode,
                                        ShippingStateCode,BillingState,BillingPostalCode,ShippingCountry,isCustomerEmail__c,
                                        ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,Annual_Income__c,
                                        Employer_Name__c,Length_of_Employment_Months__c,Length_of_Employment_Years__c,
                                        Job_Title__c,Credit_Run__c,RecordTypeId from Account where id IN: applicantset];
                        
                        List<Opportunity> oppLst = [Select id,Name,EDW_Originated__c,Co_Applicant__c,AccountId,Account.PersonEmail,(select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c,Contact_Email__c,CoApplicant_Email__c from Underwriting_Files__r) from Opportunity where Accountid IN: applicantset or Co_Applicant__c IN: applicantset];
                        for(Opportunity oppRec :oppLst){
                            //Added by kalyani as part of 2040 sprint 12 -start
                            if(oppRec.Underwriting_Files__r[0].Contact_Email__c!=reqData.projects[0].applicants[0].email && reqData.projects[0].applicants[0].id == oppRec.AccountId){
                                oppRec.Underwriting_Files__r[0].Contact_Email__c=reqData.projects[0].applicants[0].email;
                                uwfile.add(oppRec.Underwriting_Files__r[0]);
                            }
                            //Added by Sejal as part of 15940 - Start
                            if(oppRec.Underwriting_Files__r[0].CoApplicant_Email__c!=reqData.projects[0].applicants[0].email && reqData.projects[0].applicants[0].id == oppRec.Co_Applicant__c){
                                oppRec.Underwriting_Files__r[0].CoApplicant_Email__c = reqData.projects[0].applicants[0].email;
                                uwfile.add(oppRec.Underwriting_Files__r[0]);
                            }
                            //Added by Sejal as part of 15940 - End
                            if(!uwfile.isEmpty())
                                update uwfile;
                            //Added by kalyani as part of 2040 sprint 12 -end
                             if(oppRec.EDW_Originated__c == true){
                                Database.rollback(sp);
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = ErrorLogUtility.getErrorCodes('218', new List<String>{String.valueOf(oppRec.id)});
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                unifbean.error.add(errorMsg);
                                unifbean.returnCode = '218';
                                iRestRes = (IRestResponse)unifbean;
                                return iRestRes;
                            }else if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                                unifbean.returnCode = '366';
                                errMsg = ErrorLogUtility.getErrorCodes('366',null);
                                 Database.rollback(sp);
                                iolist = new list<DataValidator.InputObject>{};
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                unifbean.error.add(errorMsg);
                                iRestRes = (IRestResponse)unifbean;
                                return iRestRes;
                            }
                            
                        }
                        system.debug('### Accountlst '+Accountlst);
                        
                        if(!Accountlst.isEmpty()&& string.isBlank(errMsg))
                        {
                            for(Account AccountIterate : Accountlst)
                            {
                                
                                string AccountId = string.ValueOf(AccountIterate.Id);
                                if(AccountId.length()>15)
                                    AccountId = AccountId.subString(0,15);
                                    
                                AccountMap.put(AccountId,AccountIterate);
                                
                                if(AccountIterate.Credit_Run__c)
                                {
                                    creditcheckflag=true;
                                }else{
                                    creditcheckflag=false;
                                }
                                
                            }
                        }else{
                            errMsg = ErrorLogUtility.getErrorCodes('285',null);
                            iolist = new list<DataValidator.InputObject>{};
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            unifBean.error.add(errorMsg);
                            unifBean.returnCode = '285';
                            iRestRes = (IRestResponse)unifbean;
                            return iRestRes;
                        }
                    }else{
                        errMsg = ErrorLogUtility.getErrorCodes('285',null);
                        iolist = new list<DataValidator.InputObject>{};
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        unifBean.error.add(errorMsg);
                        unifBean.returnCode = '285';
                        iRestRes = (IRestResponse)unifbean;
                        return iRestRes;
                    }
                    system.debug('### AccountMap '+AccountMap);
                    if(!AccountMap.isEmpty()&& string.isBlank(errMsg))
                    {
                        for(UnifiedBean.ApplicantWrapper AccountIterate : AccountRecLst)
                        {
                            if(string.isNotBlank(AccountIterate.id))
                            {
                               
                                if(AccountIterate.id.length()>15)
                                    AccountIterate.id = AccountIterate.id.subString(0,15);
                                
                                if(AccountMap.containsKey(AccountIterate.Id) && creditcheckflag && !checkApplicantUpdate(AccountIterate))
                                {
                                    Account accRec = AccountMap.get(AccountIterate.Id);
                                    
                                    //Updated by Ravi as part of ORANGE-3354 Start
                                    if(string.isNotBlank(AccountIterate.email)){
                                        accRec.PersonEmail = AccountIterate.email;
                                        
                                        system.debug('### checking if the email is suspecious');
                                        Email_Domain_Validation_List__c dom = Email_Domain_Validation_List__c.getInstance(AccountIterate.email.toLowercase().split('@').get(1));
                                        system.debug('### Email domain list - '+dom);
                                        Boolean islikeKeywrd = false;
                                        
                                        system.debug('keyset - '+keyWrds.keyset());
                                        for(string strIterate : keyWrds.keyset()){
                                            if(!islikeKeywrd)
                                                islikeKeywrd = AccountIterate.email.toLowercase().contains(strIterate.toLowercase());
                                        }
                                        
                                        system.debug('### islikeKeywrd - '+islikeKeywrd);
                                        system.debug('### Email domain list - '+dom);
                                        system.debug('### loggedInUsr FirstName - '+loggedInUsr[0].contact.FirstName.toLowercase());
                                        system.debug('### loggedInUsr LastName - '+loggedInUsr[0].contact.LastName.toLowercase());
                                        system.debug('### applicant email - '+AccountIterate.email);
                                        system.debug('### isCustomerEmail - '+AccountIterate.isCustomerEmail);
                                        system.debug('### loggedInUsr Email - '+loggedInUsr[0].email);
                                        
                                        if((dom == null && AccountIterate.email.toLowercase().split('@').get(1) == loggedInUsr[0].email.toLowercase().split('@').get(1)) || AccountIterate.email.toLowercase().contains(loggedInUsr[0].contact.FirstName.toLowercase()) || AccountIterate.email.toLowercase().contains(loggedInUsr[0].contact.LastName.toLowercase()) || islikeKeywrd){
                                            accRec.isEmailSuspicious__c = 'Yes';
                                        }else{
                                            accRec.isEmailSuspicious__c = null;
                                        }
                                        
                                        if(AccountIterate.isCustomerEmail != null && AccountIterate.isCustomerEmail)
                                            accRec.isCustomerEmail__c = true;
                                        else if(accRec.isEmailSuspicious__c == null || AccountIterate.isCustomerEmail == false)        //Added as a part of Orange 14510
                                            accRec.isCustomerEmail__c = false;  
                                    }
                                    //Updated by Ravi as part of ORANGE-3354 End
                                    
                                    if(string.isNotBlank(AccountIterate.phone))
                                    accRec.Phone = AccountIterate.phone;
                                    if(string.isNotBlank(AccountIterate.otherPhone))
                                    accRec.personOtherPhone = AccountIterate.otherPhone;
                                   
                                    accLst.add(accRec);
                                }else if(AccountMap.containsKey(AccountIterate.Id) && !creditcheckflag)
                                {
                                    Account accRec = AccountMap.get(AccountIterate.Id);
                                    if(string.isNotBlank(AccountIterate.firstName))
                                        accRec.firstName = AccountIterate.firstName;
                                    if(string.isNotBlank(AccountIterate.lastName))
                                        accRec.lastName = AccountIterate.lastName;
                                    if(string.isNotBlank(AccountIterate.middleInitial))
                                        accRec.middleName = AccountIterate.middleInitial;
                                    if(string.isNotBlank(AccountIterate.ssn))
                                        accRec.SSN__c = AccountIterate.ssn;
                                    
                                    //Updated by Ravi as part of ORANGE-3354 Start
                                    if(string.isNotBlank(AccountIterate.email)){
                                        accRec.PersonEmail = AccountIterate.email;
                                        
                                        system.debug('### checking if the email is suspecious');
                                        Email_Domain_Validation_List__c dom = Email_Domain_Validation_List__c.getInstance(AccountIterate.email.toLowercase().split('@').get(1));
                                        system.debug('### Email domain list - '+dom);
                                        Boolean islikeKeywrd = false;
                                        
                                        system.debug('keyset - '+keyWrds.keyset());
                                        for(string strIterate : keyWrds.keyset()){
                                            if(!islikeKeywrd)
                                                islikeKeywrd = AccountIterate.email.toLowercase().contains(strIterate.toLowercase());
                                        }
                                        
                                        system.debug('### islikeKeywrd - '+islikeKeywrd);
                                        system.debug('### Email domain list - '+dom);
                                        system.debug('### loggedInUsr FirstName - '+loggedInUsr[0].contact.FirstName.toLowercase());
                                        system.debug('### loggedInUsr LastName - '+loggedInUsr[0].contact.LastName.toLowercase());
                                        system.debug('### applicant email - '+AccountIterate.email);
                                        system.debug('### isCustomerEmail - '+AccountIterate.isCustomerEmail);
                                        system.debug('### loggedInUsr Email - '+loggedInUsr[0].email);
                                        
                                        if((dom == null && AccountIterate.email.toLowercase().split('@').get(1) == loggedInUsr[0].email.toLowercase().split('@').get(1)) || AccountIterate.email.toLowercase().contains(loggedInUsr[0].contact.FirstName.toLowercase()) || AccountIterate.email.toLowercase().contains(loggedInUsr[0].contact.LastName.toLowercase()) || islikeKeywrd){
                                            accRec.isEmailSuspicious__c = 'Yes';
                                        }else{
                                            accRec.isEmailSuspicious__c = null;
                                        }
                                        
                                        if(AccountIterate.isCustomerEmail != null && AccountIterate.isCustomerEmail)
                                            accRec.isCustomerEmail__c = true;
                                        else if(accRec.isEmailSuspicious__c == null || AccountIterate.isCustomerEmail == false)        //Added as a part of Orange 14510
                                            accRec.isCustomerEmail__c = false;      
                                    }
                                    //Updated by Ravi as part of ORANGE-3354 End
                                    
                                    if(string.isNotBlank(AccountIterate.dateOfBirth))
                                    {
                                        Date currentdate = date.today();
                                        system.debug('### currentdate '+currentdate);
                                        Date birthdate = Date.ValueOf(AccountIterate.dateOfBirth);
                                        Integer temp = birthdate.daysBetween(currentdate);
                                        integer age = Integer.valueOf(temp/365.2425);
                                        system.debug('### age '+age);
                                        if(age< 18){
                                            Database.rollback(sp);
                                            iolist = new list<DataValidator.InputObject>{};
                                            errMsg = ErrorLogUtility.getErrorCodes('213',null);
                                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                            unifbean.error.add(errorMsg);
                                            unifbean.returnCode = '213';
                                            iRestRes = (IRestResponse)unifbean;
                                            return iRestRes;
                                        }
                                        accRec.PersonBirthdate = Date.ValueOf(AccountIterate.dateOfBirth);
                                    }
                                    if(string.isNotBlank(AccountIterate.phone))
                                        accRec.Phone = AccountIterate.phone;
                                    if(string.isNotBlank(AccountIterate.otherPhone))
                                        accRec.personOtherPhone = AccountIterate.otherPhone;
                                        accRec.BillingCountry = 'United States';
                                    if(string.isNotBlank(AccountIterate.mailingStreet))  
                                        accRec.BillingStreet = AccountIterate.mailingStreet;
                                    if(string.isNotBlank(AccountIterate.mailingCity))
                                        accRec.BillingCity = AccountIterate.mailingCity;
                                    if(string.isNotBlank(AccountIterate.mailingStateName))
                                        accRec.BillingState = AccountIterate.mailingStateName;
                                    if(string.isNotBlank(AccountIterate.mailingZipCode))
                                        accRec.BillingPostalCode = AccountIterate.mailingZipCode;
                                    accRec.ShippingCountry = 'United States';
                                  //  accRec.ShippingCountry = AccountIterate.residenceCountry;
                                    if(string.isNotBlank(AccountIterate.residenceStreet))
                                        accRec.ShippingStreet = AccountIterate.residenceStreet;
                                    if(string.isNotBlank(AccountIterate.residenceCity))
                                        accRec.ShippingCity = AccountIterate.residenceCity;
                                    if(string.isNotBlank(AccountIterate.residenceStateName))
                                        accRec.ShippingState = AccountIterate.residenceStateName;
                                    if(string.isNotBlank(AccountIterate.residenceZipCode))
                                        accRec.ShippingPostalCode = AccountIterate.residenceZipCode;
                                    if(AccountIterate.annualIncome != null)
                                        accRec.Annual_Income__c = AccountIterate.annualIncome;
                                    if(string.isNotBlank(AccountIterate.employerName))
                                        accRec.Employer_Name__c = AccountIterate.employerName;
                                    if(AccountIterate.employmentMonths != null)
                                        accRec.Length_of_Employment_Months__c = AccountIterate.employmentMonths;
                                    if(AccountIterate.employmentYears != null)
                                        accRec.Length_of_Employment_Years__c = AccountIterate.employmentYears;
                                    if(string.isNotBlank(AccountIterate.jobTitle))
                                        accRec.Job_Title__c = AccountIterate.jobTitle;
                                     //iolist.add (new DataValidator.InputObject (accRec,'FNI Credit'));   
                                      

                                    accLst.add(accRec);
                                }else if(checkApplicantUpdate(AccountIterate) && creditcheckflag){
                                    Database.rollback(sp);
                                    iolist = new list<DataValidator.InputObject>{};
                                    errMsg = ErrorLogUtility.getErrorCodes('277',null);
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    unifbean.error.add(errorMsg);
                                    unifbean.returnCode = '277';
                                    iRestRes = (IRestResponse)unifbean;
                                    return iRestRes;
                                }else{
                                    errMsg = ErrorLogUtility.getErrorCodes('285',null);
                                    iolist = new list<DataValidator.InputObject>{};
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    unifBean.error.add(errorMsg);
                                    unifBean.returnCode = '285';
                                    iRestRes = (IRestResponse)unifbean;
                                    return iRestRes;
                                }
                                
                            }else{
                                errMsg = ErrorLogUtility.getErrorCodes('285',null);
                                iolist = new list<DataValidator.InputObject>{};
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                unifBean.error.add(errorMsg);
                                unifBean.returnCode = '285';
                                iRestRes = (IRestResponse)unifbean;
                                return iRestRes;
                            }
                        }
                        
                        if(!accLst.isEmpty()&& string.isBlank(errMsg))
                        {
                            update accLst;
                            system.debug('### accLst '+accLst);
                            List<Account> AccReclst = [Select id,firstName,MiddleName,lastName,SSN__c,PersonEmail,PersonBirthdate,Phone,
                                                       personOtherPhone,BillingCountry,BillingStreet,BillingCity,BillingStateCode,
                                                       ShippingStateCode,BillingState,BillingPostalCode,ShippingCountry,
                                                       ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,Annual_Income__c,
                                                       Employer_Name__c,Length_of_Employment_Months__c,Length_of_Employment_Years__c,
                                                       Job_Title__c,RecordTypeId from Account where id =: accLst[0].id];

                            if(!AccReclst.isEmpty())
                            {
                                for(Account acc :AccReclst)
                                {
                                    
                                    //Applicant Details
                                    UnifiedBean.ApplicantWrapper applicWrapr = new UnifiedBean.ApplicantWrapper();
                                    applicWrapr.id = acc.id;
                                    applicWrapr.firstName = acc.firstName;
                                    applicWrapr.middleInitial = acc.MiddleName;
                                    applicWrapr.lastName  = acc.lastName;
                                    applicWrapr.ssn = acc.SSN__c;
                                    applicWrapr.dateOfBirth  = string.valueOf(acc.PersonBirthdate);
                                    applicWrapr.phone = acc.Phone;
                                    applicWrapr.otherPhone = acc.personOtherPhone;
                                    applicWrapr.email = acc.PersonEmail;
                                    applicWrapr.mailingStreet  = acc.BillingStreet;
                                    applicWrapr.mailingCity  = acc.BillingCity;
                                    applicWrapr.mailingStateName   = acc.BillingState;
                                    applicWrapr.mailingZipCode  = acc.BillingPostalCode;
                                    applicWrapr.residenceStreet  = acc.ShippingStreet;
                                    applicWrapr.residenceCity  = acc.ShippingCity;
                                    applicWrapr.residenceStateName   = acc.ShippingState;
                                    applicWrapr.residenceZipCode  = acc.ShippingPostalCode;
                                    applicWrapr.annualIncome  = acc.Annual_Income__c;
                                    applicWrapr.employerName  = acc.Employer_Name__c;
                                    applicWrapr.employmentMonths  = acc.Length_of_Employment_Months__c;
                                    applicWrapr.employmentYears   = acc.Length_of_Employment_Years__c;
                                    applicWrapr.jobTitle  = acc.Job_Title__c;
                                    projectWrap.applicants.add(applicWrapr);
                                    
                                    unifbean.projects.add(projectWrap);
                                }
                            }
                            unifbean.returnCode = '200';
                            iRestRes = (IRestResponse)unifbean;
                        }  
                    }
                    
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('286',null);
                    iolist = new list<DataValidator.InputObject>{};
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    unifBean.error.add(errorMsg);
                    unifBean.returnCode = '286';
                    iRestRes = (IRestResponse)unifbean;
                    return iRestRes;
                }   
            }
          
            if(string.isNotBlank(errMsg))
            {
                Database.rollback(sp);
                iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                errMsg = errorMsg.errorMessage;
                unifbean.error.add(errorMsg);
                unifbean.returnCode = '400';
            }
            iRestRes = (IRestResponse)unifbean;
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifbean.error.add(errorMsg);
            unifbean.returnCode = '400';
            iRestRes = (IRestResponse)unifbean;
            
            
            system.debug('### ApplicantObjDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
           ErrorLogUtility.writeLog('ApplicantObjDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+ApplicantObjDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
    public Boolean checkApplicantUpdate(UnifiedBean.ApplicantWrapper app)
    {
        if(String.isNotBlank(app.firstName) || String.isNotBlank(app.lastName) || String.isNotBlank(app.middleInitial) ||
            String.isNotBlank(app.ssn) || String.isNotBlank(app.dateOfBirth) || String.isNotBlank(app.mailingStreet) || null != app.employmentMonths || null != app.employmentYears ||
            String.isNotBlank(app.mailingCity ) || String.isNotBlank(app.mailingStateName ) || String.isNotBlank(app.mailingZipCode) ||
            String.isNotBlank(app.residenceStreet) || String.isNotBlank(app.residenceCity) || String.isNotBlank(app.residenceStateName ) ||
            String.isNotBlank(app.residenceZipCode) || null != app.annualIncome || String.isNotBlank(app.employerName) || String.isNotBlank(app.jobTitle ) )
            return true;
        else
            return false;
    }
}