@isTest
private class FundingSnapshotSupportctrl_Test 
{
    
    @isTest static void test1 () 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Facility_Membership_Fee__c=1000,Solar_Enabled__c=true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            Long_Term_Facility_Lookup__c = a3.id, Short_Term_Facility_Lookup__c = a3.id,
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30),
                                            Credit_Decision_Date__c = System.today());
        insert o1;
        
        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M1_Approval_Date__c = System.today()-3,
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
        

        list<Funding_Snapshot_Line_Item__c> lineItems = new list<Funding_Snapshot_Line_Item__c>{};

        lineItems = FundingSnapshotSupportctrl.getFundingSnapShotLineItems(a3.Id,'10','1',String.valueof(System.today()-10),String.valueof(System.today()),false,false);
        
        o1.Project_Category__c = 'Home';
        update o1;
        TriggerFlags__c stipTrgFlag = new TriggerFlags__c(Name='Draw_Requests__c',isActive__c=true);
        insert stipTrgFlag;
        List<Draw_Requests__c> DrawList = new List<Draw_Requests__c>();
        for(integer i=1;i<=3;i++){
            Draw_Requests__c objDraw = new Draw_Requests__c();
            objDraw.Level__c = String.valueOf(i);
            objDraw.Amount__c = 1000;
            objDraw.Request_Date__c = System.now();
            objDraw.Status__c = 'Requested';
            objDraw.Primary_Applicant_Email__c = 'test@gma.com';
            objDraw.Underwriting__c = uf1.id;
            DrawList.add(objDraw);
        }
            insert DrawList;
            for(Draw_Requests__c objDraw :DrawList){
            objDraw.Status__c = 'Approved';
            objDraw.SLF_Approved__c = system.now()-3;
            }
            update DrawList;
        lineItems = FundingSnapshotSupportctrl.getFundingSnapShotLineItems(a3.Id,'10','1',String.valueof(System.today()-10),String.valueof(System.today()),false,true);
                
    }   
    
}