@isTest
public class ApplicationIntegrationCalloutMock implements HttpCalloutMock {

    protected Integer statusCode;

    public ApplicationIntegrationCalloutMock(Integer code){
        this.statusCode = code;
    }

    public HttpResponse respond(HttpRequest req){
        String validjson = '{"GetSFApplicationDataResult":"{\"AnnualIncome\":150000,\"ApplicationId\":\"10000008642862\",\"CoAppFICOScore\":null,\"CoAppPersonBirthdate\":\"1979-04-01\",\"CoAppSSN\":\"502087042\",\"DTI\":28.40,\"EmployerName\":\"AMERIPRISE FINCANCIAL\",\"FICODate\":\"2017-04-06\",\"FICOScore\":738,\"LengthOfEmployment\":\"10\",\"MortgageBalance\":278177,\"PersonBirthdate\":\"1978-07-28\",\"PersonEmail\":\"joeaadland@gmail.com\",\"Phone\":\"7632296004\",\"SSN\":\"503080428\",\"LastName\":\"Applicant\"}"}';
        String invalidjson = '{"GetSFApplicationDataResult":"{\"Error\":101,\"ApplicationId\":\"10000000007276\",\"Message\":\"Error getting data\"}"}';

        HttpResponse response = new HttpResponse();

        response.setStatusCode(this.statusCode);

        if(this.statusCode == 200){
            response.setBody(validjson);
        } else {
            response.setBody(invalidjson);
        }

        return response;
    }
}