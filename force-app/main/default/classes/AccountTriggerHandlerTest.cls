/******************************************************************************************
* Description: Test class to cover AccountTriggerHandler and AccountTrg 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Ashish Rai            10/27/2017            Created
******************************************************************************************/
    
    
    @isTest
    public class AccountTriggerHandlerTest { 

        @testSetup static void methodName() {
            List<TriggerFlags__c> trgFlg = new List<TriggerFlags__c>();
            TriggerFlags__c trgflag1 = new TriggerFlags__c();
            trgflag1.Name ='Account';
            trgflag1.isActive__c =true; 
            trgFlg.add(trgflag1);
            TriggerFlags__c trgflag2 = new TriggerFlags__c();
            trgflag2.Name ='Opportunity';
            trgflag2.isActive__c =true; 
            trgFlg.add(trgflag2);
            TriggerFlags__c trgflag3 = new TriggerFlags__c();
            trgflag3.Name ='Contact';
            trgflag3.isActive__c =true; 
            trgFlg.add(trgflag3);
            insert trgFlg;
            
            
            Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
            List<Routing_Number__c> routeList = new List<Routing_Number__c>();
            Routing_Number__c rNum = new Routing_Number__c();
            rNum.ACH_Routing_Numbers__c = '9874563210';
            routeList.add(rNum);
            Routing_Number__c rNum1 = new Routing_Number__c();
            rNum1.ACH_Routing_Numbers__c = '1478523690';
            routeList.add(rNum1);
            insert routeList;
            
            Account personAcc = new Account();
            personAcc.name = 'Test Account123';
            personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.BillingStateCode = 'CA';
            personAcc.FNI_Domain_Code__c = 'TCU';
            personAcc.Push_Endpoint__c ='www.test1.com';
            personAcc.Installer_Legal_Name__c='Bright planet Solar1';
            personAcc.Solar_Enabled__c=true;
            personAcc.Permit_Enabled__c = true;
            personAcc.Kitting_Enabled__c = true;
            personAcc.Inspection_Enabled__c = true;
            personAcc.M0_Split__c = 20; 
            personAcc.M1_Split__c = 20;
            personAcc.M2_Split__c = 20;
            personAcc.Permit_Split__c = 20;
            personAcc.Kitting_Split__c = 10;
            personAcc.Inspection_Split__c = 10;
            personAcc.Permit_Ever_Enabled__c = true;
            personAcc.Kitting_Ever_Enabled__c = true;
            personAcc.Inspection_Ever_Enabled__c = true;
            personAcc.Routing_Number_Matches__c = false;
            personAcc.Routing_Number__c = routeList[0].ACH_Routing_Numbers__c;
            
            Contact personcon = new Contact(FirstName ='Sunlight',LastName = 'Financial', AccountId = personAcc.Id, Email = 'test@mail.com');
            insert personcon;
            
            insert personAcc;
            
             List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =personAcc.id;
            prod.Long_Term_Facility_Lookup__c =personAcc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            //insert prod;
            prodList.add(prod);
            insert prodList;
            
            //Opportunity Creation
            Opportunity opp = new Opportunity();
            opp.name = 'OpName1';
            opp.CloseDate = system.today();
            opp.StageName = 'New';
            opp.AccountId = personAcc.id;
            opp.Installer_Account__c = personAcc.id;
            opp.Install_State_Code__c = 'CA'; 
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            opp.Co_Applicant__c = personAcc.Id;
            opp.SLF_Product__c = prod.id;
            insert opp;
        }
        private testMethod static void accountTriggerHandlerTest(){  
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true; 
        insert trgflag1;
            Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
               
            //insert custom setting TriggerFlag records.
            //start
            SLFUtilityTest.createCustomSettings();
            //stop
            
            List<Routing_Number__c> routeList = new List<Routing_Number__c>();
            Routing_Number__c rNum = new Routing_Number__c();
            rNum.ACH_Routing_Numbers__c = '012345678';
            routeList.add(rNum);
            Routing_Number__c rNum1 = new Routing_Number__c();
            rNum.ACH_Routing_Numbers__c = '001210210';
            routeList.add(rNum1);
            insert routeList;
            
            // Inserting Account records        
            Account personAcc = new Account();
            personAcc.name = 'Test Account';
            personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.BillingStateCode = 'CA';
            personAcc.FNI_Domain_Code__c = 'Solar';
            personAcc.Push_Endpoint__c ='www.test.com';
            personAcc.Installer_Legal_Name__c='Bright planet Solar';
            personAcc.Solar_Enabled__c=true;
            personAcc.Permit_Enabled__c = true;
            personAcc.Kitting_Enabled__c = true;
            personAcc.Inspection_Enabled__c = true;
            personAcc.M0_Split__c = null; 
            personAcc.M1_Split__c = null;
            personAcc.M2_Split__c = null;
            personAcc.Permit_Split__c = null;
            personAcc.Kitting_Split__c = null;
            personAcc.Inspection_Split__c = null;
            personAcc.Permit_Ever_Enabled__c = true;
            personAcc.Kitting_Ever_Enabled__c = true;
            personAcc.Inspection_Ever_Enabled__c = true;
            personAcc.Routing_Number_Matches__c = false;
            personAcc.Routing_Number__c = routeList[0].ACH_Routing_Numbers__c;
            //personAcc.isPartner = true; 
            insert personAcc;
            
            personAcc.Routing_Number_Matches__c = true;
            personAcc.Routing_Number__c = routeList[1].ACH_Routing_Numbers__c;
            update personAcc;
            
            Account accObj = new Account();
            accObj = personAcc;
            
            Contact personcon = new Contact(LastName ='testCon',AccountId = personAcc.Id);
            insert personcon;
            
            personAcc = [select Id from Account where Id =: personAcc.Id];
            
            personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
            update personAcc;
            
            //Inserting opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = personAcc.id;
            opp.Installer_Account__c = personAcc.id;
            opp.Install_State_Code__c = 'CA'; 
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'Solar||123';
            opp.ACH_APR_Process_Required__c = true;
            insert opp;
            
            AccountTriggerHandler acctrg = new AccountTriggerHandler(true);
            acctrg.OnAfterUpdate(null,null);
            AccountTriggerHandler.isTrgExecuting = true;
            acctrg.OnAfterUpdate(new List<Account>{personAcc},new Map<Id,Account>{accObj.Id=>accObj});
            AccountTriggerHandler.isTrgExecuting = false;
            AccountTriggerHandler.runOnce();
        }
        private testMethod static void accountTriggerHandlerTest1(){
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
               
            //insert custom setting TriggerFlag records.
            //start
            SLFUtilityTest.createCustomSettings();
            //stop
            List<Routing_Number__c> routeList = new List<Routing_Number__c>();
            Routing_Number__c rNum = new Routing_Number__c();
            rNum.ACH_Routing_Numbers__c = '012345678';
            routeList.add(rNum);
            Routing_Number__c rNum1 = new Routing_Number__c();
            rNum.ACH_Routing_Numbers__c = '001210210';
            routeList.add(rNum1);
            insert routeList;
            
            Account personAcc = new Account();
            personAcc.name = 'Test Account';
            personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.BillingStateCode = 'CA';
            personAcc.FNI_Domain_Code__c = 'Solar';
            personAcc.Push_Endpoint__c ='www.test.com';
            personAcc.Installer_Legal_Name__c='Bright planet Solar';
            personAcc.Solar_Enabled__c = false;
            personAcc.Permit_Enabled__c = true;
            personAcc.Kitting_Enabled__c = true;
            personAcc.Inspection_Enabled__c = true;
            personAcc.M0_Split__c = 20; 
            personAcc.M1_Split__c = 20;
            personAcc.M2_Split__c = 20;
            personAcc.Permit_Split__c = 20;
            personAcc.Kitting_Split__c = 10;
            personAcc.Inspection_Split__c = 10;
            personAcc.Permit_Ever_Enabled__c = false;
            personAcc.Kitting_Ever_Enabled__c = false;
            personAcc.Inspection_Ever_Enabled__c = false;
            personAcc.Routing_Number_Matches__c = false;
            personAcc.Routing_Number__c = routeList[0].ACH_Routing_Numbers__c;
            personAcc.Subscribe_to_Push_Updates__c = true;
            personAcc.Push_Endpoint__c = 'www.google.com';
            //personAcc.isPartner = true; 
            insert personAcc;
            
            personAcc.Routing_Number_Matches__c = true;
            personAcc.Routing_Number__c = routeList[1].ACH_Routing_Numbers__c;
            update personAcc;
            
            Account accObj = new Account();
            accObj = personAcc;
            
            Contact personcon = new Contact(LastName ='testCon',AccountId = personAcc.Id);
            insert personcon;
            
            personAcc = [select Id from Account where Id =: personAcc.Id];
            
            personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
            update personAcc;
            
            //Inserting opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = personAcc.id;
            opp.Installer_Account__c = personAcc.id;
            opp.Install_State_Code__c = 'CA'; 
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'Solar||123';
            opp.EDW_Originated__c = false;
            insert opp;
            
                        
            AccountTriggerHandler acctrg = new AccountTriggerHandler(true);
            acctrg.OnAfterUpdate(null,null);
            AccountTriggerHandler.isTrgExecuting = true;
            acctrg.OnAfterUpdate(new List<Account>{personAcc},new Map<Id,Account>{accObj.Id=>accObj});
            AccountTriggerHandler.isTrgExecuting = false;
            AccountTriggerHandler.runOnce();
        }
        private testMethod static void accountTriggerHandlerTest2(){
            List<Routing_Number__c> route = [select id, ACH_Routing_Numbers__c from Routing_Number__c];
            Account acc = [select id,M0_Split__c,M1_Split__c,M2_Split__c,Permit_Split__c,Kitting_Split__c,Inspection_Split__c from Account where Name ='Test Account123' limit 1];
            Opportunity opp = [select id, StageName from Opportunity where Installer_Account__c =: acc.Id limit 1];
            opp.StageName = 'Closed Won';
            opp.ACH_APR_Process_Required__c = true;
            update opp;
            
            acc.M0_Split__c = null;
            acc.M1_Split__c = null;
            acc.M2_Split__c = null;
            acc.Permit_Split__c = null;
            acc.Kitting_Split__c = null;
            acc.Inspection_Split__c = null;
            acc.Routing_Number_Matches__c = true;
            acc.Account_Number__c = '123456789';
            acc.ACH_true__c = null;

            Update acc;
        }

    }