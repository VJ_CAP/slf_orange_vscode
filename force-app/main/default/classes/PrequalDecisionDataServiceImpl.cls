/**
* Description: User and Contact Creation logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Brahmeswar             06/04/2018           Created
******************************************************************************/

public without sharing class PrequalDecisionDataServiceImpl extends RestDataServiceBase{
   /*
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+PrequalDecisionDataServiceImpl.class);
        System.debug('### stsDtObj:'+stsDtObj);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean respBean = new UnifiedBean(); 
        respBean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        Map<string,string> mapConFieldInfo = new Map<string,string>();
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        List<String> FacilityNames = new List<String>();
        SET<String> PrequalIds = new SET<String>();
        String ProjectCategory = '';
        Savepoint sp = Database.setSavepoint();
        try
        { 
            System.debug('### reqData.projects:::'+reqData.projects);
            System.debug('### reqData.projects.size():::'+reqData.projects.size());
            System.debug('### reqData.projects[0]:::'+reqData.projects[0]);
            System.debug('### reqData.projects[0].prequals:::'+reqData.projects[0].prequals);
            if(reqData.projects != null && reqData.projects[0].prequals != null)
            {
                if(String.isNotBlank(reqData.projects[0].prequals[0].fniLTRes)){
                    String PrequalId = reqData.projects[0].prequals[0].id;
                    Map<string,string> LenderStatus = new map<string,string>();
                    String Installeridval = '';
                    Map<String,Product__c> LenderNameAndProductid = new Map<string,Product__c>();
                    Prequal__c prequalObj = [SELECT  DTI__c, FICO__c, Pre_Qual_Status__c,Term_mo__c, APR__c, 
                                                     Lending_Facility__c,FNI_Status_Message__c, Decision__c, 
                                                     Opportunity__c,Opportunity__r.Install_State_Code__c,Opportunity__r.Project_Category__c, 
                                                     Installer_Account__c, SSN__c, DOB__c,Mailing_State__c,Id,HashID__c, 
                                                     Name,FNI_SOAP_Request__c,FNI_SOAP_Response__c,Pre_Qual_Authorized__c,
                                                     Decision_Date_Time__c,Stip_Code_1__c,Decision_Stips__c, CreatedDate, Send_Email_Flag__c 
                                                     FROM Prequal__c where id=:PrequalId];
                    Installeridval = prequalObj.Installer_Account__c;
                    System.debug('### prequalObj: '+prequalObj);
                    ProjectCategory = prequalObj.Opportunity__r.Project_Category__c;
                    // Removed ACH__C as part of 3622
                    for(Product__c prdobj: [select id,Long_Term_Facility__c,Long_Term_Facility_Lookup__c,Installer_Account__c,Term_mo__c,APR__c 
                                            from Product__c where Installer_Account__c=:Installeridval 
                                            AND State_Code__c=:prequalObj.Opportunity__r.Install_State_Code__c
                                            AND Internal_Use_Only__c = FALSE 
                                            AND Is_Active__c = TRUE 
                                            ] ){
                        LenderNameAndProductid.put(prdobj.Long_Term_Facility__c,prdobj);
                    }
                    Set<String> facilities = new Set<String>();
                    /*DOM.XmlNode rootNode = FNIFacilityDecisionDataServiceImpl.getFNIResponseNode(reqData.projects[0].prequals[0].fniLTRes);
                    List<FNIFacilityDecisionDataServiceImpl.LenderWrapper> lstLenderWrap = FNIFacilityDecisionDataServiceImpl.parseFNIResponseXML(rootNode);*/
                    //Orange-1309
                    List<DOM.XmlNode> rootNodes = RBPFNIFacilityDecisionDataServiceImpl.getFNIResponseNode(reqData.projects[0].prequals[0].fniLTRes);
                    List<RBPFNIFacilityDecisionDataServiceImpl.LenderWrapper> lstLenderWrap = RBPFNIFacilityDecisionDataServiceImpl.parseFNIResponseXML(rootNodes);
                    
                    system.debug('#####RBPFNIFacilityDecisionDataServiceImpl########'+lstLenderWrap);
                    system.debug('#####LenderNameAndProductid########'+LenderNameAndProductid);
                    List<Prequal_Decision__c> lstprequalDesion = new List<Prequal_Decision__c>();
                    String hasPending = '';
                    String hasDecline = '';
                    Prequal_Decision__c prequalDesion;
                    if(!lstLenderWrap.isEmpty()){
                        for(RBPFNIFacilityDecisionDataServiceImpl.LenderWrapper objlender: lstLenderWrap){
                            system.debug('#####objlender Iterate########'+objlender);
                            prequalDesion = new Prequal_Decision__c();
                            system.debug('#####LenderNameAndProductid###value#####'+LenderNameAndProductid.get(objlender.FacilityName));
                            if(!LenderNameAndProductid.isEmpty() && LenderNameAndProductid.get(objlender.FacilityName) != null)
                                prequalDesion.Facility_Name__c = LenderNameAndProductid.get(objlender.FacilityName).Long_Term_Facility_Lookup__c;
                            
                            prequalDesion.Max_DTI__c = objlender.MaxDTI!=null ? Decimal.valueOf(objlender.MaxDTI) : 0;
                            prequalDesion.MaxPayment__c = objlender.MaxPayment!=null ? Decimal.valueOf(objlender.MaxPayment) : 0;
                            prequalDesion.Prequal__c = PrequalId;
                            prequalDesion.Max_Line__c = objlender.Line!=null && objlender.Line!='' ? Decimal.valueOf(objlender.Line) : 0;
                            prequalDesion.Prequal_Descision__c = objlender.decision;
                            if(objlender.decision =='P')
                                    hasPending = 'Pending Review';
                            if(objlender.decision =='D')
                                    hasDecline = 'Auto Declined';
                            /*
                            if(!LenderNameAndProductid.isEmpty() && LenderNameAndProductid.get(objlender.FacilityName) != null)
                                prequalDesion.SLF_Product__c = LenderNameAndProductid.get(objlender.FacilityName).id;
                            */
                            prequalDesion.Product_Tier__c = objlender.tierId;//Orange-1309
                            prequalDesion.StipCode__c = objlender.stipRsn;
                            prequalDesion.Product_Type__c = objlender.ProductType;
                            LenderStatus.put(objlender.FacilityName,objlender.decision);
                            lstprequalDesion.add(prequalDesion);
                            
                        }
                    }
                    System.debug('### lstprequalDesion: '+lstprequalDesion);
                    if(!lstprequalDesion.isEmpty())
                        insert lstprequalDesion;
                    
                    UnifiedBean.PrequalWrapper prequalWrap = new UnifiedBean.PrequalWrapper();
                    /*//Note: Do we require below SOQL because we can get all details from lstprequalDesion
                    UnifiedBean.PrequalWrapper prequalWrap = new UnifiedBean.PrequalWrapper();
                    List<Prequal_Decision__c> DecisionRecs = [Select id,Name,Facility_Name__c,Max_DTI__c,MaxPayment__c,Prequal_Descision__c,StipCode__c,SLF_Max_Line__c,Max_Line__c,Product_Tier__c,Prequal__c from Prequal_Decision__c where Prequal__c =:PrequalId and Prequal_Descision__c ='A' order by Product_Tier__c asc];
                    //Orange-1309*/
                    
                    Integer tempTierval;
                    List<String> lstHIIFacilities  = new List<String>();
                    List<String> lstHISFacilities  = new List<String>();
                    Map<integer,list<String>> mapHIITierAndFacilities = new Map<integer,list<String>>();
                    Map<integer,list<String>> mapHISTierAndFacilities = new Map<integer,list<String>>();
                    Map<integer,list<String>> mapHINTierAndFacilities = new Map<integer,list<String>>();
                    
                    
                    for(Prequal_Decision__c objprequalD : [Select id,Name,Facility_Name__c,Max_DTI__c,MaxPayment__c,Prequal_Descision__c,StipCode__c,SLF_Max_Line__c,Max_Line__c,Product_Tier__c,Product_Type__c,Prequal__c from Prequal_Decision__c where Prequal__c =:PrequalId and Prequal_Descision__c ='A' order by Product_Tier__c asc]){
                        System.debug('###objprequal1:'+prequalObj.Pre_Qual_Status__c);
                        System.debug('###objprequalD:'+objprequalD);
                            integer tierval = Integer.ValueOf(objprequalD.Product_Tier__c);
                            System.debug('###ProjectCategory:'+ProjectCategory);
                            System.debug('###tierval:'+tierval);
                            if(ProjectCategory != '' && ProjectCategory == 'Home'){
                                if(objprequalD.Product_Type__c != null && objprequalD.Product_Type__c == 'HII'){ 
                                    if(mapHIITierAndFacilities.get(tierval) != null){
                                        List<String> lstFacilitytemp = mapHIITierAndFacilities.get(tierval);
                                        lstFacilitytemp.add(objprequalD.Facility_Name__c);
                                        mapHIITierAndFacilities.put(tierval,lstFacilitytemp);
                                        
                                    }else{
                                        List<String> lstFacilitytemp = new List<String>();
                                        lstFacilitytemp.add(objprequalD.Facility_Name__c);
                                        mapHIITierAndFacilities.put(tierval,lstFacilitytemp);
                                    }
                                }
                                if(objprequalD.Product_Type__c != null && objprequalD.Product_Type__c == 'HIS'){ 
                                    if(mapHISTierAndFacilities.get(tierval) != null){
                                        List<String> lstFacilitytemp = mapHISTierAndFacilities.get(tierval);
                                        lstFacilitytemp.add(objprequalD.Facility_Name__c);
                                        mapHISTierAndFacilities.put(tierval,lstFacilitytemp);
                                    }else{
                                        List<String> lstFacilitytemp = new List<String>();
                                        lstFacilitytemp.add(objprequalD.Facility_Name__c);
                                        mapHISTierAndFacilities.put(tierval,lstFacilitytemp);
                                    }
                                }
                                if(objprequalD.Product_Type__c != null && objprequalD.Product_Type__c == 'HIN'){ 
                                    if(mapHINTierAndFacilities.get(tierval) != null){
                                        List<String> lstFacilitytemp = mapHINTierAndFacilities.get(tierval);
                                        lstFacilitytemp.add(objprequalD.Facility_Name__c);
                                        mapHINTierAndFacilities.put(tierval,lstFacilitytemp);
                                    }else{
                                        List<String> lstFacilitytemp = new List<String>();
                                        lstFacilitytemp.add(objprequalD.Facility_Name__c);
                                        mapHINTierAndFacilities.put(tierval,lstFacilitytemp);
                                    }
                                }
                            }else{  
                                if(objprequalD.Facility_Name__c != null){
                                    facilities.add(objprequalD.Facility_Name__c);
                                }
                                if(tempTierval == null)
                                    tempTierval = Integer.valueOf(objprequalD.Product_Tier__c);
                            }
                            prequalWrap.decisionDetails =  objprequalD.Prequal_Descision__c;//added By Adithya
                            prequalWrap.status = 'Auto Approved';//added By Adithya
                    }
                    if(prequalWrap.decisionDetails == null || prequalWrap.decisionDetails != 'A'){
                        if(hasPending != '' && hasPending == 'Pending Review'){
                            prequalWrap.decisionDetails =  'P';
                            prequalWrap.status = hasPending;
                        }else{
                            if(hasDecline != '' && hasDecline == 'Auto Declined'){
                                prequalWrap.decisionDetails =  'D';
                                prequalWrap.status = hasDecline;
                            }
                        }
                        
                    }
                    System.debug('###prequalWrap:'+prequalWrap);
                    System.debug('###mapHIITierAndFacilities:'+mapHIITierAndFacilities);
                    System.debug('###mapHISTierAndFacilities:'+mapHISTierAndFacilities);
                    System.debug('###mapHINTierAndFacilities:'+mapHINTierAndFacilities);
                    Integer HIITier;
                    Integer HISTier;
                    Integer HINTier;
                    
                    for(Integer tier : mapHIITierAndFacilities.keyset()){
                        System.debug('###HII Facilities:'+mapHIITierAndFacilities.get(tier));
                        facilities.addAll(mapHIITierAndFacilities.get(tier));
                        HIITier =tier;
                        break;
                    }
                    for(Integer tier : mapHISTierAndFacilities.keyset()){
                        System.debug('###HIS Facilities:'+mapHISTierAndFacilities.get(tier));
                        facilities.addAll(mapHISTierAndFacilities.get(tier));
                        HISTier = tier;
                        break;
                    }
                    for(Integer tier : mapHINTierAndFacilities.keyset()){
                        System.debug('###HIS Facilities:'+mapHINTierAndFacilities.get(tier));
                        facilities.addAll(mapHINTierAndFacilities.get(tier));
                        HINTier = tier;
                        break;
                    }
                    System.debug('###HIITier:'+HIITier);
                    System.debug('###HISTier:'+HISTier);
                    System.debug('###HINTier:'+HINTier);
                    
                    prequalWrap.id = prequalObj.id;
                    prequalWrap.hashId = prequalObj.HashID__c;//Added by Deepika as part of Orange - 3391
                    prequalWrap.status = prequalObj.Pre_Qual_Status__c;
                    /*if(!DecisionRecs.isEmpty()){
                        prequalWrap.lineAmount = DecisionRecs[0].SLF_Max_Line__c;
                    }*/
                    System.debug('### line amount:::'+prequalWrap.lineAmount);
                    prequalWrap.isPrequalAuthorized = prequalObj.Pre_Qual_Authorized__c;
                    //prequalWrap.createdDateTime = String.valueOf(system.now());
                    prequalWrap.createdDateTime = String.valueOf(prequalObj.CreatedDate);
                    prequalWrap.decisionDateTime = String.valueOf(prequalObj.Decision_Date_Time__c);                    
                    //prequalWrap.stipReason = prequalObj.Stip_Code_1__c; //Commented by Suresh
                    prequalWrap.stipReason = prequalObj.Decision_Stips__c; //Added by Suresh
                    System.debug('### stipResn:::'+prequalWrap.stipReason);
                    prequalWrap.products = new List<UnifiedBean.ProductWrapper>();
                    
                    Map<String,Product__c> prodMap = new Map<String,Product__c>();
                    //Orange-1309
                    Set<String> tierval = new Set<String>();
                    
                    if(ProjectCategory != '' && ProjectCategory == 'Home'){
                        tierval.add(string.valueOf(HIITier));
                        tierval.add(string.valueOf(HISTier));
                        tierval.add(string.valueOf(HINTier));
                    }else{
                        if(tempTierval != null)
                        tierval.add(string.valueOf(tempTierval));
                    }
                    
                    System.debug('###tierval:'+tierval);
                    
                    
                    if(tierval != null && tierval.size()>0){
                        //Added as part of ORANGE-1595 START
                        String productTypes = Label.Home_Type_Products;
                        List<string> lstProducttypes = new List<String>();
                        for(string typeval : productTypes.split('&&')){
                            lstProducttypes.add(typeval);
                        }
                        system.debug('###lstProducttypes:'+lstProducttypes);
                        List<Product__c> lstProducts = new List<Product__c>();
                        
                        if(ProjectCategory != '' && ProjectCategory == 'Home'){
                            // removed ACH__C as part of 3622
                            lstProducts = [select id,Long_Term_Facility__c,Long_Term_Facility_Lookup__c,State_Code__c,Product_Tier__c,Installer_Account__c,NonACH_Prod_Display__c,NonACH_UI_Display_Name__c,Product_Display_Name__c,Prequal_Display_Name__c,UI_Display_Name__c,Term_mo__c,APR__c,Product_Loan_Type__c,NonACH_APR__c,Promo_APR__c,Promo_Term__c,NonACH_Promo_APR__c from Product__c where Installer_Account__c=:Installeridval 
                                                AND State_Code__c=:prequalObj.Opportunity__r.Install_State_Code__c
                                                AND Internal_Use_Only__c = FALSE 
                                                AND Is_Active__c = TRUE and Long_Term_Facility_Lookup__c IN :facilities
                                                AND APR__c != null AND Term_mo__c != null AND Product_Tier__c IN: tierval AND Product_Loan_Type__c IN:lstProducttypes ];
                        }else{
                             // removed ACH__C as part of 3622
                            lstProducts = [select id,Long_Term_Facility__c,Long_Term_Facility_Lookup__c,State_Code__c,Product_Tier__c,Installer_Account__c,Term_mo__c,NonACH_Prod_Display__c,NonACH_UI_Display_Name__c,Product_Display_Name__c,Prequal_Display_Name__c,UI_Display_Name__c,APR__c,Product_Loan_Type__c ,NonACH_Promo_APR__c,NonACH_APR__c,Promo_APR__c,Promo_Term__c from Product__c where Installer_Account__c=:Installeridval 
                                                AND State_Code__c=:prequalObj.Opportunity__r.Install_State_Code__c
                                                AND Internal_Use_Only__c = FALSE 
                                                AND Is_Active__c = TRUE and Long_Term_Facility_Lookup__c IN :facilities
                                                AND APR__c != null AND Term_mo__c != null AND Product_Tier__c IN: tierval AND Product_Loan_Type__c NOT IN:lstProducttypes Order BY Product_Loan_Type__c,Term_mo__c,APR__c ASC];
                            
                        }
                        
                        
                        system.debug('###lstProducts:'+lstProducts);                        
                        if(lstProducts != null && lstProducts.size()>0){    
                            for(Product__c prodIterate: lstProducts){
                                String productName = '';
                                //productName = prodIterate.APR__c+'-'+prodIterate.Term_mo__c;
                                productName = prodIterate.Product_Display_Name__c; 
                                
                                String nonACHProductName = '';
                                if(null != prodIterate.NonACH_APR__c)
                                    nonACHProductName = prodIterate.NonACH_Prod_Display__c; 
                                
                                if(ProjectCategory != '' && ProjectCategory == 'Home')
                                {
                                    productName = prodIterate.UI_Display_Name__c; 
                                    nonACHProductName = prodIterate.NonACH_UI_Display_Name__c; 
                                    
                                    productName = productName+'-'+prodIterate.Product_Loan_Type__c;
                                    if(string.isNotBlank(nonACHProductName))
                                        nonACHProductName = nonACHProductName+'-'+prodIterate.Product_Loan_Type__c;
                                    
                                }
                                if(ProjectCategory != '' && ProjectCategory == 'Home' && prodIterate.NonACH_UI_Display_Name__c == ''){
                                    nonACHProductName = '';
                                }
                                if(ProjectCategory != '' && ProjectCategory != 'Home' && prodIterate.NonACH_Prod_Display__c == '')
                                {
                                    nonACHProductName = '';
                                }
                                
                                
                                if(productName != '' && !prodMap.ContainsKey(productName))
                                {
                                    prodMap.put(productName,prodIterate);
                                    UnifiedBean.ProductWrapper prodWrap = new UnifiedBean.ProductWrapper();
                                    System.debug('###prodIterate.Product_Tier__c:'+prodIterate.Product_Tier__c);
                                    if(prodIterate.Product_Tier__c != null && prodIterate.Product_Tier__c !='0')//Orange-1309
                                    {
                                        prodWrap.tier = prodIterate.Product_Tier__c;
                                        
                                    }
                                    if(null != prodIterate.Promo_APR__c)
                                        prodWrap.promoAPR = prodIterate.Promo_APR__c;
                                   
                                    if(null != prodIterate.Promo_Term__c)
                                        prodWrap.promoTerm = Integer.ValueOf(prodIterate.Promo_Term__c);
                                    
                                    prodWrap.apr= prodIterate.APR__c;
                                    prodWrap.term = prodIterate.Term_mo__c;
                                    prodWrap.displayName = prodIterate.Product_Display_Name__c;
                                    prodWrap.prequalDisplayName = prodIterate.Prequal_Display_Name__c;
                                    
                                    if(ProjectCategory != '' && ProjectCategory == 'Home')
                                    {
                                        prodWrap.displayName = prodIterate.UI_Display_Name__c;
                                    }
                                    prodWrap.productType = prodIterate.Product_Loan_Type__c;
                                    System.debug('###prodWrap:'+prodWrap);
                                    prequalWrap.products.add(prodWrap);
                                    
                                    //Added By Adithya as part of ORANGE-7210
                                    //Start
                                    if(nonACHProductName != '' && !prodMap.ContainsKey(nonACHProductName))
                                    {
                                        prodMap.put(nonACHProductName,prodIterate);
                                        UnifiedBean.ProductWrapper prodWrapCln = prodWrap.clone();
                                        prodWrapCln.displayName = prodIterate.NonACH_Prod_Display__c;
                                        prodWrapCln.apr = prodIterate.NonACH_APR__c;
                                        if(ProjectCategory != '' && ProjectCategory == 'Home')
                                        {
                                            prodWrapCln.displayName = prodIterate.NonACH_UI_Display_Name__c;
                                            if(null != prodIterate.NonACH_Promo_APR__c)
                                            {
                                                prodWrapCln.promoAPR = prodIterate.NonACH_Promo_APR__c;
                                            }
                                        }
                                        prequalWrap.products.add(prodWrapCln);
                                    }
                                    //End
                                    
                                }   
                            }
                        }
                        System.debug('###products:'+prequalWrap.products);
                        if(!prequalWrap.products.isEmpty() && prequalWrap.products != null){
                            system.debug('### check point before updating prequalObj');
                            prequalObj.Pre_Qual_Status__c = prequalWrap.status;//added By Adithya
                            prequalObj.Decision__c = prequalWrap.decisionDetails;//added By Adithya
                            prequalObj.Decision_Message__c = JSON.serialize(prequalWrap.products, true);
                            prequalObj.Send_Email_Flag__c = true;
                            system.debug('### prequalObj'+prequalObj);
                            update prequalObj;                                                      
                        }
                    }
                    
                    if(!prequalObj.Send_Email_Flag__c)
                    {
                            prequalObj.Send_Email_Flag__c = true;
                            update prequalObj;
                    }
                    System.debug('###prequalObj:'+prequalObj);
                    respBean = SLFUtility.getUnifiedBean(new set<Id>{prequalObj.Opportunity__c},'Orange',false);
                    UnifiedBean.OpportunityWrapper projectIterate = respBean.projects[0];
                    projectIterate.prequals =  new List<UnifiedBean.PrequalWrapper>();
                    projectIterate.prequals.add(prequalWrap);
                    if(projectIterate.prequals != null){
                        for(UnifiedBean.PrequalWrapper preqalobjt : projectIterate.prequals){
                        if(prequalObj.Decision__c == 'A')
                            preqalobjt.status = 'Auto Approved';
                        }
                    }
                    
                    respBean.returnCode = '200';
                    iRestRes = (IRestResponse)respBean;
                    System.debug('### Response Bean: '+iRestRes);
                    return iRestRes;
                }
            }   
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            FNIFacilityDecisionDataServiceImpl facltyDcsnObj = new FNIFacilityDecisionDataServiceImpl();
            facltyDcsnObj.gerErrorMsg(respBean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)respBean;
            system.debug('### PrequalDecisionDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('PrequalDecisionDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### iRestRes '+iRestRes);
        system.debug('### Exit from transformOutput() of '+CreateUserDataServiceImpl.class);

        return iRestRes;
    }

}