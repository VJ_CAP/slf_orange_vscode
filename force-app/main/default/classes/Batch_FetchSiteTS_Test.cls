@isTest
public class Batch_FetchSiteTS_Test {
    @testSetup static void setUpData()
    {
        //  SLFUtilityDataSetup.initData();
        //  SLFUtilityTest.createCustomSettings();   
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgBoxflag = new TriggerFlags__c();
        trgBoxflag.Name ='Box_Fields__c';
        trgBoxflag.isActive__c =true;
        trgLst.add(trgBoxflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
 
        insert trgLst;
        
        SF_Category_Map__c sfCatMap = new SF_Category_Map__c(Name='AHJ Approval',
                                                             Last_Run_TS_Field_on_Underwriting_File__c = 'Latest_Communications_Date_Time__c',
                                                             TS_Field_On_Underwriting__c = 'Box_Communications_TS__c' );
        insert sfCatMap;
        
        SFSettings__c slfencrypt = new SFSettings__c();
        slfencrypt.Encrypt_Private_Key__c = 'SLFOrange';
        slfencrypt.Name = 'PrivateKey';
        slfencrypt.Funding_Package_End_Point__c='www.test.com';
        slfencrypt.Batch_Number__c=1;
        
        
        insert slfencrypt;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        system.debug('@@ I am here' +portalUser.Contact.AccountId);
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        //update acc;
        
        
        AccountShare accShare = new AccountShare(); //a new empty AccountShare object
        accShare.userorgroupid = portalUser.Id;
        accShare.accountid = con.accountid;
        accShare.accountaccesslevel = 'Edit';
        accShare.OpportunityAccessLevel = 'None';
        accShare.CaseAccessLevel = 'None';
        insert accShare;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact personcon = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = personAcc.Id);
        insert personcon; 
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        personAcc.PersonBirthdate = Date.ValueOf('1929-03-10');
        update personAcc;
        
        AccountShare personAccShare = new AccountShare(); //a new empty AccountShare object
        personAccShare.userorgroupid = portalUser.Id;
        personAccShare.accountid = personAcc.Id;
        personAccShare.accountaccesslevel = 'Edit';
        personAccShare.OpportunityAccessLevel = 'None';
        personAccShare.CaseAccessLevel = 'None';
        insert personAccShare;
        
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        insert prod;
        
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Street__c ='Street';
        opp.Install_City__c ='InCity';
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'TCU||123';
        //opp.Approved_LT_Facility__c = 'TCU';
        opp.SLF_Product__c = prod.id;
        opp.Block_Draws__c = false;
        opp.Has_Open_Stip__c = 'False';
        opp.All_HI_Required_Documents__c=true;
        opp.Max_Draw_Count__c=3;
        insert opp;
        
        
        
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = opp.id;
        undStpbt.Approved_for_Payments__c = true;
        insert undStpbt;
        Site_Download__c siteDwnld = new Site_Download__c(Success__c = true,Site_UUID__c='2585',Opportunity__c=opp.id,Batch_Number__c=1);
        insert siteDwnld;
        
        
        List<Attachment> attchList = new List<Attachment>();
        Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob; 
        attach.parentId=siteDwnld.Id; 
        attach.ContentType = 'application/msword'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test'; 
        attchList.add(attach);
        insert attchList; 
        
        Box_Fields__c boxObj  = new Box_Fields__c(Underwriting__c = undStpbt.ID,Latest_LT_Loan_Agreement_Date_Time__c=system.now(),Latest_Install_Contract_Date_Time__c = system.now());
        insert boxObj;   
        
        
    }
    public static testMethod void test_FetchSiteTS()
    {
        
        Test.startTest();
        Database.executeBatch(new Batch_FetchSiteTS(), 50);
        Test.stopTest();
    }
    public static testMethod void testscheduleBatch_FetchSiteTS() 
    {
        Test.StartTest();
        Batch_FetchSiteTS sh1 = new Batch_FetchSiteTS();
        String sch = '0 0 23 * * ?'; system.schedule('Test Schedule Batch_FetchSiteTS', sch, sh1);
        Test.stopTest();
    }
}