global class Batch_FetchSiteTS implements Database.Batchable<Opportunity>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {

    DateTime lastExecutionTime;
    DateTime curBatchStartTime;
    Double  batchNumber;
    
    public Batch_FetchSiteTS()
    {
        BoxUtil.checkLimits();
        BoxUtil.abortBatchCheck();
        
        // **** load last execution batch time and set current batch start time
		SFSettings__c settings = SFSettings__c.getOrgDefaults();
        lastExecutionTime = settings.Site_Batch_Execution_TS__c;
        
        if (settings.Batch_Overlay_Offset__c != null && settings.Batch_Overlay_Offset__c > 0) {
            lastExecutionTime = lastExecutionTime - (settings.Batch_Overlay_Offset__c/24);
        }
        batchNumber = settings.Batch_Number__c;
        if (lastExecutionTime == null) {
            Date dt = date.parse('01/01/2000');
            lastExecutionTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
        }
        
        curBatchStartTime = System.Now();
    }
    
    global Iterable<Opportunity> start(Database.BatchableContext BC)
	{
		List<Opportunity> ol = new List<Opportunity>();
        
        List<Site_Download__c> sl = new List<Site_Download__c>([SELECT Opportunity__c,Site_UUID__c,Sighten_Last_Modified_TS__c FROM Site_Download__c  where Opportunity__c != null AND (Sighten_Last_Modified_TS__c > :lastExecutionTime OR Created_Time__c > :lastExecutionTime)]);
        for (Site_Download__c s : sl) 
        {
            Opportunity o = new Opportunity();
            o.Id = s.Opportunity__c;
            o.Sighten_UUID__c = s.Site_UUID__c;
            ol.add(o);
        }
        
        
        SFSettings__c settings = SFSettings__c.getOrgDefaults(); 
        if (settings.Process_Archived_Sites__c) {
            List<Site_Download__c> sl2 = new List<Site_Download__c>([SELECT Opportunity__c,Site_UUID__c,Sighten_Last_Modified_TS__c FROM Site_Download__c  where Opportunity__c != null AND Sighten_Last_Modified_TS__c = null ]);
            for (Site_Download__c s : sl2) 
            {
                Opportunity o = new Opportunity();
                o.Id = s.Opportunity__c;
                o.Sighten_UUID__c = s.Site_UUID__c;
                ol.add(o);
            }
        }
        
        return ol;
    }
    

    global void execute(SchedulableContext sc)
    {
        Batch_FetchSiteTS bf = new Batch_FetchSiteTS();
        Database.executeBatch(bf,1);
 	}
    
 	global void execute(Database.BatchableContext BC,List<Opportunity> ol) 
    {
		
       
        List<Site_Download__c> logList = new List<Site_Download__c>();
        List<Attachment> attList = new List<Attachment>();
        
    	Site_Download__c l = null;
        for (Opportunity o : ol) {
            BoxUtil.abortBatchCheck();
            try {
			 BoxUtil.checkLimits();
             // ** SS CHANGE
             BoxUtil.SiteDownloadWrapper w = sightenCallout.downloadSiteFromSighten(o, lastExecutionTime);
             l = w.site_download;
             if (w.attachment != null)
                 attList.add(w.attachment);
                
             l.Batch_Number__c = batchNumber;
             batchNumber = batchNumber + 1;
             //l = sightenCallOut.getDocsBySiteFromSighten(o, lastExecutionTime);
             l.Notes__c = '';
             l.Processed__c = false;
             l.Processing_Error_Details__c = null;
             l.Success__c = true;
             logList.add(l);
            } catch (Exception e) {
                l = new Site_Download__c();
               	l.Batch_Number__c = batchNumber;
                batchNumber = batchNumber + 1;
                l.Opportunity__c  = o.Id;
             	l.Site_UUID__c = o.Sighten_UUID__c;
                l.Success__c = false;
                l.Processed__c = false;
				l.Notes__c = e.getMessage().Left(255);
                l.Processing_Error_Details__c  = e.getLineNumber() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + e.getStackTraceString();
                logList.add(l);
            }
            
            SFSettings__c settings = SFSettings__c.getOrgDefaults();
            if (settings.Delay_Between_Sighten_Calls__c > 0)
            	sightenCallout.sleep(Integer.valueOf(settings.Delay_Between_Sighten_Calls__c));
        }
        System.Debug('******* UPSERTING logList ' + logList);
        upsert logList Site_UUID__c;
        
        // *** now to process the attachments
        Map<String, Id> uuidSiteMap = new Map<String, Id>();
        for (Site_Download__c s : logList)
            uuidSiteMap.put(s.Site_UUID__C,s.Id);
        
        // *** delete all existing attachments
        List<Attachment> delList = new List<Attachment>([Select Id from Attachment where parentId in :uuidSiteMap.values()]);
        delete delList;
        
        // *** the parentId was set in the name field
        for (Attachment a : attList) {
            a.parentId = uuidSiteMap.get(a.name);
        	a.name = a.name + ' - ' + System.Now();
        }
        insert attList;
        
        
        
        update ol;
        
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        
        if (!Test.IsRunningTest()) {
            if (settings.CHAIN_BATCHES__c) {
       			 Database.executeBatch(new Batch_ProcessSite(), 1);
            }
        }
    }
}