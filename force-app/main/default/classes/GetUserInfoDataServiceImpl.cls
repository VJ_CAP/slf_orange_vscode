/**
* Description: Fetch User information logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Brahmeswar            03/13/2018           Created
*****************************************************************************************/
public without sharing class GetUserInfoDataServiceImpl extends RestDataServiceBase{ 
    
    
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+GetUserInfoDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        List<UnifiedBean.UsersWrapper> userRecLst =  reqData.users;
        UnifiedBean createuserbean = new UnifiedBean(); 
        createuserbean.users = new List<UnifiedBean.UsersWrapper>();
        createuserbean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            List<user> userobj = new List<User>();
            List<Opportunity> opprLst;
             String opportunityId = '';
        String externalId;
        Savepoint sp = Database.setSavepoint();
        UnifiedBean.UsersWrapper userRec = userRecLst!=null && !userRecLst.isEmpty() ? userRecLst[0] : null;
        //UnifiedBean.UsersWrapper userRec = userRecLst[0];
        
        try
        { 
            if(null != reqData.projects)
            {
                if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
                {                 
                    errMsg = ErrorLogUtility.getErrorCodes('203', null);
                    createuserbean.returnCode = '203';
                    //  return reqData;
                } 
                else if((String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
                {
                    errMsg = ErrorLogUtility.getErrorCodes('201',null);
                    createuserbean.returnCode = '201';
                } 
                else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id)){
                    opprLst = new List<Opportunity>();
                    opprLst = [SELECT Id, EDW_Originated__c,Installer_Account__c,Installer_Account__r.Risk_Based_Pricing__c,ownerid FROM Opportunity WHERE Id = :reqData.projects[0].id 
                                      LIMIT 1];
                    
                    if(opprLst == null || opprLst.size() == 0) 
                    {
                        createuserbean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                    else{
                        opportunityId = opprLst.get(0).Id;
                    }
                }
                else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
                {
                    List<user> loggedInUsr = [select Id,contactid,contact.AccountId,isInstallerReportEnabled__c,isNetReportEnabled__c,isRemittanceReportEnabled__c,
                                                        contact.Account.Default_FNI_Loan_Amount__c,
                                                        contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,
                                                        UserRole.Name,contact.Account.Eligible_for_Last_Four_SSN__c,
                                                        contact.Account.Credit_Waterfall__c,
                                                        contact.Account.Relationship_Manager__r.Name, 
                                                        contact.Account.Relationship_Manager__r.Email, 
                                                        contact.Account.Relationship_Manager__r.Phone,
                                                        contact.Account.Relationship_Manager__r.FullPhotoUrl,
                                                        contact.Account.Solar_Enabled__c,
                                                        contact.Account.Home_Improvement_Enabled__c,
                                                        contact.Account.Inspection_Ever_Enabled__c,
                                                        contact.Account.Kitting_Ever_Enabled__c,
                                                        contact.Account.Permit_Ever_Enabled__c,
                                                        contact.Account.Enable_Reason_for_CO__c
                                                        from User 
                                                        where Id =: userinfo.getUserId()];  
                    if(reqData.projects[0].externalId.contains('||')){
                        createuserbean.returnCode = '219';
                        errMsg =ErrorLogUtility.getErrorCodes('219',null);
                    }
                       
                    if(!loggedInUsr.isEmpty())
                    {
                        if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                            externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                        }else{
                            iolist = new list<DataValidator.InputObject>{};
                            errMsg = ErrorLogUtility.getErrorCodes('400',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            createuserbean.error.add(errorMsg);
                            createuserbean.returnCode = '400';
                            iRestRes = (IRestResponse)createuserbean;
                            return iRestRes;
                        }
                    }
                    if(String.isBlank(errMsg)){
                        opprLst = new List<Opportunity>();
                        opprLst = [SELECT Id,Partner_Foreign_Key__c, EDW_Originated__c,Installer_Account__c,Installer_Account__r.Risk_Based_Pricing__c,ownerid FROM Opportunity where Partner_Foreign_Key__c=:externalId 
                                          limit 1];
                        if (opprLst!=null && opprLst.size() > 0)
                        {                            
                            opportunityId = opprLst.get(0).id;
                        }
                        else {
                            createuserbean.returnCode = '202';
                            errMsg = ErrorLogUtility.getErrorCodes('202',null);
                        }
                    }
                }
                else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
                {
                    opprLst = new List<Opportunity>();
                    opprLst = [SELECT Id, Hash_Id__c,EDW_Originated__c,Installer_Account__c,Installer_Account__r.Risk_Based_Pricing__c,ownerid FROM Opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                    if (opprLst!=null && opprLst.size() > 0)
                    {
                        opportunityId = opprLst.get(0).id;
                    }else {
                        createuserbean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                }
                if(string.isNotBlank(errMsg))
                {
                    Database.rollback(sp);
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    createuserbean.error.add(errorMsg);
                    iRestRes = (IRestResponse)createuserbean;
                    return iRestRes;
                }
                if(!opprLst.isEmpty()){
                    userobj = [SELECT Id, X2020_ITC_Step_down_Acknowledged__c, Username,LoginCount__c, LastName,Email, Profile.Name, FirstName,isInstallerReportEnabled__c,isNetReportEnabled__c,isRemittanceReportEnabled__c,Is_Home_Remittance_Report_Enabled__c,contact.Account.Installer_Dedicated_Phone_Support__c,ACH_Acknowledged__c,
                               UserRole.Name,Hash_Id__c,contact.Account.Credit_Waterfall__c,
                               contact.Account.Prequal_Enabled__c,contact.Account.Risk_Based_Pricing__c,
                               contact.Account.Requires_ACH_APR_Validation__c,
                               contact.Account.Name,contact.Account.isLoanAgreementSuppressed__c,
                               contact.Account.Eligible_for_Last_Four_SSN__c,
                               contact.Account.Home_Improvement_Enabled__c,
                               contact.Account.Solar_Enabled__c,Training_Certification_Completed__c,
                               contact.Account.Relationship_Manager__r.Name, 
                               contact.Account.Relationship_Manager__r.Email, 
                               contact.Account.Relationship_Manager__r.Phone,
                               contact.Account.Relationship_Manager__r.FullPhotoUrl,
                               contact.Account.Inspection_Ever_Enabled__c,
                               contact.Account.Kitting_Ever_Enabled__c,
                               contact.Account.Permit_Ever_Enabled__c,contact.Account.Enable_Reason_for_CO__c,
                               contact.Account.Account_Status__c, // Added as part of 4542
                               Last_View_State__c,
                               contact.AccountId // Added as part of 3700
                               FROM User where id= :opprLst[0].ownerid];
                }
            }
            else if(reqData.users == null || (String.isBlank(userRec.id) && String.isBlank(userRec.hashId))){
                //errMsg = 'Either id or hashId must be present';
                //createuserbean.returnCode = '203';
                userobj = [SELECT Id, Username,X2020_ITC_Step_down_Acknowledged__c, LoginCount__c, LastName,Email, Profile.Name, FirstName,isInstallerReportEnabled__c,isNetReportEnabled__c,isRemittanceReportEnabled__c,Is_Home_Remittance_Report_Enabled__c,ACH_Acknowledged__c,
                           UserRole.Name,Hash_Id__c,contact.Account.Credit_Waterfall__c,
                           contact.Account.Prequal_Enabled__c,contact.Account.Risk_Based_Pricing__c,
                           contact.Account.Requires_ACH_APR_Validation__c,
                           contact.Account.Name,contact.Account.isLoanAgreementSuppressed__c,
                           contact.Account.Eligible_for_Last_Four_SSN__c,
                           contact.Account.Installer_Dedicated_Phone_Support__c,
                           contact.Account.Home_Improvement_Enabled__c,
                           contact.Account.Solar_Enabled__c,Training_Certification_Completed__c,
                           contact.Account.Relationship_Manager__r.Name, 
                           contact.Account.Relationship_Manager__r.Email, 
                           contact.Account.Relationship_Manager__r.Phone,
                           contact.Account.Relationship_Manager__r.FullPhotoUrl,
                           contact.Account.Inspection_Ever_Enabled__c,
                           contact.Account.Kitting_Ever_Enabled__c,
                           contact.Account.Permit_Ever_Enabled__c,contact.Account.Enable_Reason_for_CO__c,
                           contact.Account.Account_Status__c, // Added as part of 4542
                            Last_View_State__c,
                            contact.AccountId // Added as part of 3700                        
                           FROM User where id=:userinfo.getuserId()];
            }else if(String.isNotBlank(userRec.id) && String.isNotBlank(userRec.hashId)){
                errMsg = ErrorLogUtility.getErrorCodes('373', null);
                createuserbean.returnCode = '373';
                
            }else{
                
                if(reqData.users != null)
                {
                    
                    system.debug('***userRec--'+userRec);
                    if(string.isNotBlank(userRec.id))
                    {
                        userobj = [SELECT Id, Username, X2020_ITC_Step_down_Acknowledged__c, LoginCount__c, LastName,Email, Profile.Name, FirstName,isInstallerReportEnabled__c,isNetReportEnabled__c,isRemittanceReportEnabled__c,Is_Home_Remittance_Report_Enabled__c,ACH_Acknowledged__c,
                                   UserRole.Name,Hash_Id__c,contact.Account.Credit_Waterfall__c,
                                   contact.Account.Prequal_Enabled__c,contact.Account.Name,
                                   contact.Account.Requires_ACH_APR_Validation__c,
                                   contact.Account.Risk_Based_Pricing__c,contact.Account.isLoanAgreementSuppressed__c,
                                   contact.Account.Eligible_for_Last_Four_SSN__c,
                                   contact.Account.Home_Improvement_Enabled__c,
                                   contact.Account.Solar_Enabled__c,Training_Certification_Completed__c ,
                                   contact.Account.Relationship_Manager__r.Name, contact.Account.Relationship_Manager__r.Email,contact.Account.Relationship_Manager__r.Phone, contact.Account.Relationship_Manager__r.FullPhotoUrl,
                                   contact.Account.Inspection_Ever_Enabled__c,contact.Account.Installer_Dedicated_Phone_Support__c,
                                   contact.Account.Kitting_Ever_Enabled__c,
                                   contact.Account.Permit_Ever_Enabled__c,contact.Account.Enable_Reason_for_CO__c,
                                   contact.Account.Account_Status__c, // Added as part of 4542
                                   Last_View_State__c,
                                   contact.AccountId // Added as part of 3700
                                   FROM User where id=:userRec.id];
                        if(userobj.isEmpty()) 
                        {
                            iolist = new list<DataValidator.InputObject>{};
                            errMsg = ErrorLogUtility.getErrorCodes('371', null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            createuserbean.error.add(errorMsg);
                            createuserbean.returnCode = '371';
                            iRestRes = (IRestResponse)createuserbean;
                            return iRestRes;
                        }
                        
                    }else if(string.isNotBlank(userRec.hashId)){
                        
                        userobj = [SELECT Id, Username,LoginCount__c, X2020_ITC_Step_down_Acknowledged__c, LastName,Email, Profile.Name, FirstName,isInstallerReportEnabled__c,isNetReportEnabled__c,isRemittanceReportEnabled__c,Is_Home_Remittance_Report_Enabled__c,contact.Account.Installer_Dedicated_Phone_Support__c,ACH_Acknowledged__c,
                                   UserRole.Name,Hash_Id__c,contact.Account.Credit_Waterfall__c,
                                   contact.Account.Prequal_Enabled__c,contact.Account.Name,
                                   contact.Account.Requires_ACH_APR_Validation__c,
                                   contact.Account.Risk_Based_Pricing__c, contact.Account.isLoanAgreementSuppressed__c,
                                   contact.Account.Eligible_for_Last_Four_SSN__c,
                                   contact.Account.Home_Improvement_Enabled__c,
                                   contact.Account.Solar_Enabled__c,Training_Certification_Completed__c,
                                   contact.Account.Relationship_Manager__r.Name, contact.Account.Relationship_Manager__r.Email,contact.Account.Relationship_Manager__r.Phone, contact.Account.Relationship_Manager__r.FullPhotoUrl,                                  
                                   contact.Account.Inspection_Ever_Enabled__c,
                                   contact.Account.Kitting_Ever_Enabled__c,
                                   contact.Account.Permit_Ever_Enabled__c,
                                   contact.Account.Enable_Reason_for_CO__c,//Added as part of 1877 Udaya Kiran
                                   contact.Account.Account_Status__c, // Added as part of 4542
                                   Last_View_State__c,
                                   contact.AccountId // Added as part of 3700
                                   FROM User where Hash_Id__c=:userRec.hashId];
                        if(userobj.isEmpty()) 
                        {
                            iolist = new list<DataValidator.InputObject>{};
                            errMsg = ErrorLogUtility.getErrorCodes('371', null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            createuserbean.error.add(errorMsg);
                            createuserbean.returnCode = '371';
                            iRestRes = (IRestResponse)createuserbean;
                            return iRestRes;
                        }
                        
                    }
                    
                }
            }
            if(string.isNotBlank(errMsg)){
                Database.rollback(sp);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                createuserbean.error.add(errorMsg);
                iRestRes = (IRestResponse)createuserbean;
                return iRestRes;
            }            
            
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400', null);
            gerErrorMsg(createuserbean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)createuserbean;
            system.debug('### GetUserInfoDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('GetUserInfoDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('***userobj--'+userobj);
        if(userobj[0] != null){
            UnifiedBean.UsersWrapper Userinfo = new UnifiedBean.UsersWrapper();
            //Userinfo.userName = UserObj.Username;
            Userinfo.hashId = UserObj[0].Hash_Id__c;
            Userinfo.id = UserObj[0].id;
            //Added by Dax starts 3377
            Userinfo.firstName = UserObj[0].FirstName;
            Userinfo.lastName = UserObj[0].LastName;
            Userinfo.email = UserObj[0].Email;
            //Added by Dax ends 3377
            Userinfo.role = UserObj[0].UserRole.Name;
            Userinfo.profile = UserObj[0].Profile.Name;
            Userinfo.loginCount = Integer.valueOf(UserObj[0].LoginCount__c);
            //Added by Sejal and Srinivas for Orange - 3471 on 26/04/2019
            Userinfo.dashboardView = (UserObj[0].Last_View_State__c != null?UserObj[0].Last_View_State__c:'');
            
            Userinfo.itcStepDownAcknowledged = UserObj[0].X2020_ITC_Step_down_Acknowledged__c;
            
            system.debug('***Userinfo--'+Userinfo);
            createuserbean.returnCode = '200';
            createuserbean.users.add(Userinfo); 
            UnifiedBean.InstallerInfoWrapper respInstallerInfoWrapper = new UnifiedBean.InstallerInfoWrapper();
            respInstallerInfoWrapper.name = UserObj[0].contact.Account.Name;
            /*
            Dax Commented this out as per Paul's request
            if(null != UserObj[0].contact.Account.Relationship_Manager__c)
            {
                respInstallerInfoWrapper.rmFullName = UserObj[0].contact.Account.Relationship_Manager__r.Name;
                respInstallerInfoWrapper.rmEmail = UserObj[0].contact.Account.Relationship_Manager__r.Email;
                respInstallerInfoWrapper.rmWorkNumber = UserObj[0].contact.Account.Relationship_Manager__r.Phone;
                respInstallerInfoWrapper.rmFullPhotoUrl = ConnectApi.UserProfiles.getPhoto(null, UserObj[0].contact.Account.Relationship_Manager__c).fullEmailPhotoUrl;
            }
            */
            respInstallerInfoWrapper.isWaterfallEnabled = UserObj[0].contact.Account.Credit_Waterfall__c;
            respInstallerInfoWrapper.isPrequalEnabled = UserObj[0].contact.Account.Prequal_Enabled__c;
            respInstallerInfoWrapper.isRBPEnabled = UserObj[0].contact.Account.Risk_Based_Pricing__c;
            respInstallerInfoWrapper.isLast4SSNEnabled = UserObj[0].contact.Account.Eligible_for_Last_Four_SSN__c;
            respInstallerInfoWrapper.isLoanAgreementSuppressed = UserObj[0].contact.Account.isLoanAgreementSuppressed__c;
            respInstallerInfoWrapper.isSolarEnabled = UserObj[0].contact.Account.Solar_Enabled__c; // Added by Deepika on 22/11/2018 Orange-1594(added field in related queries)
            respInstallerInfoWrapper.isHomeEnabled = UserObj[0].contact.Account.Home_Improvement_Enabled__c; // Added by Deepika on 22/11/2018 Orange-1594(added field in related queries)
            respInstallerInfoWrapper.requiresAchAprValidation= UserObj[0].contact.Account.Requires_ACH_APR_Validation__c; // Added As part of 3483
           
            if(UserObj[0].contact.Account.Account_Status__c == 'Suspended') 
                respInstallerInfoWrapper.isSuspended = true;
            else
                respInstallerInfoWrapper.isSuspended = false;
            
            //Added by Sejal as part of Orange - 3700 - Start
            List<UnifiedBean.ChildInstallersWrapper> childAccountsLst = new List<UnifiedBean.ChildInstallersWrapper>(); 
            //Updated by Ravi as part of ORANGE-8453 Start
            if(null != UserObj[0].contact){
                for(Account acc:[select id,Name from Account where ParentId = :UserObj[0].contact.Account.Id order by Name Limit 40000]){
                    UnifiedBean.ChildInstallersWrapper childAcc = new UnifiedBean.ChildInstallersWrapper();
                    childAcc.name = acc.Name;
                    childAcc.id = acc.Id;
                    childAccountsLst.add(childAcc);
                }       
                respInstallerInfoWrapper.childAccounts = childAccountsLst;
            }
            //Updated by Ravi as part of ORANGE-8453 End
            //Added by Sejal as part of Orange - 3700 - End
            respInstallerInfoWrapper.installerID = UserObj[0].contact.AccountId; // Added for Orange-3700
            
            //Added by Deepika on 04/02/2019 as part of Orange-1930- Start
            if(UserObj[0].contact.Account.Solar_Enabled__c){
                respInstallerInfoWrapper.isInspectionEnabled = UserObj[0].contact.Account.Inspection_Ever_Enabled__c;
                respInstallerInfoWrapper.isKittingEnabled = UserObj[0].contact.Account.Kitting_Ever_Enabled__c;
                respInstallerInfoWrapper.isPermitEnabled = UserObj[0].contact.Account.Permit_Ever_Enabled__c;
            }
            respInstallerInfoWrapper.enableReasonForCO = UserObj[0].contact.Account.Enable_Reason_for_CO__c; // Added as part of 1877 Udaya Kiran
            respInstallerInfoWrapper.supportNumber = UserObj[0].contact.Account.Installer_Dedicated_Phone_Support__c;
            //Added by Deepika on 04/02/2019 as part of Orange-1930- End            
            Userinfo.isTrainingCompleted = UserObj[0].Training_Certification_Completed__c;//Added By Adithya on 12/07/2018 ORANGE-1521
            
            Userinfo.trainingDocLink = label.Training_Document_Link;
            Userinfo.isAchAcknowledged= UserObj[0].ACH_Acknowledged__c; //Added as part of 3483

            if(UserObj[0].contact.Account.Home_Improvement_Enabled__c){
                Userinfo.homeDocLink1 = label.Home_Document_Link_1;
                Userinfo.homeDocLink2 = label.Home_Document_Link_2;
            }
            //Added by Suresh as per Orange-1662
            Userinfo.isInstallerReportEnabled = UserObj[0].isInstallerReportEnabled__c;
            Userinfo.isNetReportEnabled = UserObj[0].isNetReportEnabled__c;
            Userinfo.isRemittanceReportEnabled = UserObj[0].isRemittanceReportEnabled__c;
            //End-Added by Suresh as per Orange-1662
            Userinfo.isHomeRemittanceReportEnabled  = UserObj[0].Is_Home_Remittance_Report_Enabled__c; //added By Adithya as per 3443
            //added By Adithya as per 1790
            //start

            SFSettings__c sfSettings = SFSettings__c.getOrgDefaults();
            
            UnifiedBean.ApplicationInfoWrapper applicationInfoRec = new UnifiedBean.ApplicationInfoWrapper();
            applicationInfoRec.mobileAppVersion = sfSettings.Mobile_App_Version__c;
            applicationInfoRec.androidMobileAppVersion = sfSettings.Android_Mobile_App_Version__c; //added By Ravi as per 6935 
            createuserbean.applicationInfo = applicationInfoRec;

            //End
            createuserbean.installerInfo = respInstallerInfoWrapper;
        }
        system.debug('##createuserbean##'+createuserbean);
        iRestRes = (IRestResponse)createuserbean;
        system.debug('### Exit from transformOutput() of '+GetUserInfoDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        
        return iRestRes;
    }
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
    
}