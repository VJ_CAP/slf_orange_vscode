@isTest(seeAllData=false)
public class Test_BatchClasses {

     @testSetup static void setUpData()
     {
         List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        insert trgLst;
        
        SF_Category_Map__c sfCatMap = new SF_Category_Map__c(Name='AHJ Approval',
                                                            Last_Run_TS_Field_on_Underwriting_File__c = 'Latest_Communications_Date_Time__c',
                                                            TS_Field_On_Underwriting__c = 'Box_Communications_TS__c' );
        insert sfCatMap;
              
        Account a = new Account();

        a.FirstName = 'Solar';
        a.LastName = 'City';
        a.SSN__c = '666-66-6666';
        a.PersonBirthdate = Date.today();
        a.Marital_Status__c = 'Unmarried';
        a.Phone = '4344435555';
        a.PersonEmail = 'test1@test.com';
        a.ShippingPostalCode = '12456';
        a.BillingPostalCode = '12456';
        a.PersonBirthdate = date.valueOf('1968-09-21');
        a.Installer_Legal_Name__c='Bright planet Solar';
        insert a;
        System.debug('act value'+a);
        
        List<opportunity> oppList = new List<Opportunity>();
        Opportunity o = new Opportunity();
        o.Name = 'O1';
        o.StageName = 'Qualified';
        o.CloseDate = System.Today();
        o.Install_City__c = 'San Francisco';
        o.Install_State_Code__c = 'CA';
        o.Upload_Comment__c = true ;
        //o.Installer_Account__r.Name=a.Name;
        o.Install_Postal_Code__c = '94123';
        o.Install_Street__c = '2263 Vallejo Street';
        o.AccountId = a.Id;
        oppList.add(o);
        insert oppList;
        System.debug('opty value'+oppList);
        
        TestSightenCallout.setUpData();
        Account acc = [SELECT id,name from Account where lastname = 'City' limit 1];
        system.debug('****Solar City Account Record*****'+acc);
        List<Opportunity> ol = [Select Id,Upload_Comment__c,Account.name from Opportunity where Upload_Comment__c = true AND Account.name =: acc.name];
        System.debug('****ol list******'+ol);
        SFSettings__c settings = [SELECT id FROM SFSettings__c where Process_Archived_Sites__c = true limit 1];
        settings.Comments_Alert_Emails__c = 'test@gmail.com';
        settings.Comments_Alert_Threshold__c = 0;
        update settings;
     }
    
    public static testMethod void test_ImageUpload1()
    {
        Test.startTest();
        Database.executeBatch(new Batch_ImageUpload(107), 50);
        Test.stopTest();
        
    }
    
    public static testMethod void test_ImageUpload2()
    {
        Test.startTest();
        Database.executeBatch(new Batch_ImageUpload(System.Now(),System.Now()), 50);
        Test.stopTest();
    }
    
    public static testMethod void test_FetchSite()
    {
        Test.startTest();
        Database.executeBatch(new Batch_FetchSite(), 50);
        Test.stopTest();
    }
    
  /*  public static testMethod void test_FetchSiteTS()
    {
        Test.startTest();
            Database.executeBatch(new Batch_FetchSiteTS(), 50);
        Test.stopTest();
    }
    public static testMethod void testscheduleBatch_FetchSiteTS() 
    {
        Test.StartTest();
            Batch_FetchSiteTS sh1 = new Batch_FetchSiteTS();
            String sch = '0 0 23 * * ?'; system.schedule('Test Schedule Batch_FetchSiteTS', sch, sh1);
        Test.stopTest();
    }*/
    public static testMethod void testscheduleBatch_FetchSite() 
    {
        Test.StartTest();
            Batch_FetchSite sh1 = new Batch_FetchSite();
            String sch = '0 0 23 * * ?'; system.schedule('Test Schedule Batch_FetchSite', sch, sh1);
        Test.stopTest();
    }
    
   /* public static testMethod void test_ProcessSite()
    {
        Test.startTest();
        Database.executeBatch(new Batch_ProcessSite(), 50);
        Test.stopTest();
    }*/
    
     public static testMethod void test_UpdateSiteTS()
    {
        Test.startTest();
     Database.executeBatch(new Batch_UpdateSiteTS(), 50);
       Test.stopTest();
    }
   
    public static testMethod void Batch_FetchQuotes()
    {
        Test.startTest();
        Database.executeBatch(new Batch_FetchQuotes(), 50);
        Test.stopTest();
    }
    
    public static testMethod void testscheduleBatch_FetchQuotes() 
    {
        Test.StartTest();
        Batch_FetchQuotes sh1 = new Batch_FetchQuotes();
        String sch = '0 0 23 * * ?'; system.schedule('Test Schedule Batch_FetchQuotes', sch, sh1);
        Test.stopTest();
    }
    
    public static testMethod void Batch_UpdateMilestones()
    {
        Test.startTest();
        Database.executeBatch(new Batch_UpdateMilestones(), 50);
        Test.stopTest();
    }
    
     public static testMethod void Batch_UpdateMilestones1()
    {
        Test.startTest();
        Database.executeBatch(new Batch_UpdateMilestones(), 10);
        Test.stopTest();
    }
    
   /* public static testMethod void Batch_FetchMilestones()
    {
        Test.startTest();
        Database.executeBatch(new Batch_FetchMilestones(), 50);
        Test.stopTest();
    }*/
    public static testMethod void RiskStatusNotificationSchedule(){
        Test.StartTest();
            RiskStatusNotificationSchedule sh1 = new RiskStatusNotificationSchedule();
            String sch = '0 0 23 * * ?'; system.schedule('Test Schedule RiskStatusNotificationSchedule', sch, sh1);
        Test.stopTest();
    }  
    public static testMethod void DrawRequestExpirationSchedule (){
        Test.StartTest();
            DrawRequestExpirationSchedule sh1 = new DrawRequestExpirationSchedule();
            String sch = '0 0 23 * * ?'; system.schedule('Test Schedule DrawRequestExpirationSchedule', sch, sh1);
        Test.stopTest();
    } 
}