/******************************************************************************************
* Description: Test class to cover CreateUnifiedJSON class
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma           05/18/2018            Created
******************************************************************************************/
@isTest
public class CreateUnifiedJSONTest { 
    
    private testMethod static void createUnifiedJSONTest(){            
    
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        
        //insert custom setting TriggerFlag records.
        //start
        SLFUtilityTest.createCustomSettings();
        //stop
        
        List<Build_Unified_JSON__c> buildUnifiedJSONLst = new List<Build_Unified_JSON__c>();
        
        Build_Unified_JSON__c buildUnifiedJsonStatu1 = new Build_Unified_JSON__c();
        buildUnifiedJsonStatu1.Name = '01';
        buildUnifiedJsonStatu1.Attribute_Name__c = 'totalLoanAmount';
        buildUnifiedJsonStatu1.Field_API_Name__c = 'Combined_Loan_Amount__c';
        buildUnifiedJsonStatu1.Object_API_Name__c = 'Opportunity';
        buildUnifiedJSONLst.add(buildUnifiedJsonStatu1);
        
        Build_Unified_JSON__c buildUnifiedJsonStatu2 = new Build_Unified_JSON__c();
        buildUnifiedJsonStatu2.Name = '02';
        buildUnifiedJsonStatu2.Attribute_Name__c = 'hashId';
        buildUnifiedJsonStatu2.Field_API_Name__c = 'Hash_Id__c';
        buildUnifiedJsonStatu2.Object_API_Name__c = 'Opportunity';
        buildUnifiedJSONLst.add(buildUnifiedJsonStatu2);
        
        Build_Unified_JSON__c buildUnifiedJsonStatus3 = new Build_Unified_JSON__c();
        buildUnifiedJsonStatus3.Name = '03';
        buildUnifiedJsonStatus3.Attribute_Name__c = 'isCreditAuthorized';
        buildUnifiedJsonStatus3.Field_API_Name__c = 'Customer_has_authorized_credit_hard_pull__c';
        buildUnifiedJsonStatus3.Object_API_Name__c = 'Opportunity';
        buildUnifiedJSONLst.add(buildUnifiedJsonStatus3);
        
        Build_Unified_JSON__c buildUnifiedJsonStatus4 = new Build_Unified_JSON__c();
        buildUnifiedJsonStatus4.Name = '04';
        buildUnifiedJsonStatus4.Attribute_Name__c = 'externalId';
        buildUnifiedJsonStatus4.Field_API_Name__c = 'Reference_Key__c';
        buildUnifiedJsonStatus4.Object_API_Name__c = 'Opportunity';
        buildUnifiedJSONLst.add(buildUnifiedJsonStatus4);
        
        Build_Unified_JSON__c buildUnifiedJsonId = new Build_Unified_JSON__c();
        buildUnifiedJsonId.Name = '05';
        buildUnifiedJsonId.Attribute_Name__c = 'employerName';
        buildUnifiedJsonId.Field_API_Name__c = 'Employer_Name__c';
        buildUnifiedJsonId.Object_API_Name__c = 'Account';
        buildUnifiedJSONLst.add(buildUnifiedJsonId);

        Build_Unified_JSON__c buildUnifiedJsonStatus = new Build_Unified_JSON__c();
        buildUnifiedJsonStatus.Name = '06';
        buildUnifiedJsonStatus.Attribute_Name__c = 'employmentMonths';
        buildUnifiedJsonStatus.Field_API_Name__c = 'Length_of_Employment_Months__c';
        buildUnifiedJsonStatus.Object_API_Name__c = 'Account';
        buildUnifiedJSONLst.add(buildUnifiedJsonStatus);

        insert buildUnifiedJSONLst;
        List<Account> accList = new List<Account>(); 
        // Inserting Account records        
        Account personAcc = new Account();
        personAcc.name = 'Test Account';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.FNI_Domain_Code__c = 'Solar';
        personAcc.Push_Endpoint__c ='www.test.com';
        //personAcc.Subscribe_to_Event_Updates__c = true;
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c=true;        
        
        insert personAcc;
        
        Contact personcon = new Contact(LastName ='testCon',AccountId = personAcc.Id);
        insert personcon;
        
        //Commented by Suresh
        /*Account personAccN = [select Id,Employer_Name__c,recordTypeId from Account where Id =: personAcc.Id]; 
        
        //personAcc.Length_of_Employment_Months__c = 3;
        personAccN.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAccN;*/
        
         //Insert Account record
        Account personAcc1= new Account();
        personAcc1.Name = 'SLFTest';
        personAcc1.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc1.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc1.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc1.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc1.BillingStateCode = 'CA';
        personAcc1.Installer_Legal_Name__c='Bright planet Solar';
        personAcc1.Subscribe_to_Event_Updates__c = true;
        personAcc1.Solar_Enabled__c=true;
        personAcc1.Push_Endpoint__c ='www.test.com';
        personAcc1.Endpoint_Header__c = 'header1:value1,header2:value2 with spaces,awesomeperson:dax!';
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc1;
        
       // Account personAccN = [select Id,name from Account where name ='SLFTest12']; 
        
        //Inserting opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = personAcc.id;
        opp.Installer_Account__c = personAcc1.id;
        opp.Install_State_Code__c = 'CA'; 
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'Solar||1234';
        //opp.Hash_Id__c = 'shdlahbjkla1254';
        insert opp;
        
        Opportunity oppUpdate = [Select id from Opportunity where name = 'OpName'];
        system.debug('****Updated Opp Record******'+oppUpdate );
        oppUpdate.Description = 'Test';
        update oppUpdate ; 
        
        set<id> oppIds = new set<id>();
        oppIds.add(oppUpdate.Id);
        
        //Generate JSON Structure and create Status Audit Record
        String jsonNewOppList = JSON.serialize(new List<Opportunity> {opp});
        
        //deserialize New records to List of SObjects
        List<sObject> newSObjList = (List<sObject>)JSON.deserialize(jsonNewOppList,List<sObject>.class);
        CreateUnifiedJSON.buildUnifiedJSON('Opportunity','',oppIds,newSObjList,null,new Map<Id,Id>{opp.Id=>personAcc.id});
        
        //Generate JSON Structure and create Status Audit Record
        String jsonNewAccList = JSON.serialize(new List<Opportunity> {opp});
        
        //deserialize New records to List of SObjects
        List<sObject> newAccSObjList = (List<sObject>)JSON.deserialize(jsonNewAccList,List<sObject>.class);
        CreateUnifiedJSON.buildUnifiedJSON('Opportunity','Projects',oppIds,newAccSObjList,null,null);               
        CreateUnifiedJSON.buildUnifiedJSON('List','Projects',oppIds,newAccSObjList,null,null);
        CreateUnifiedJSON.buildUnifiedJSON('List','applicants',oppIds,new List<Account>{personAcc},new Map<Id,Account>{personAcc.id=>personAcc},null);
        CreateUnifiedJSON.buildUnifiedJSON('Account','applicants',oppIds,new List<Account>{personAcc},new Map<Id,Account>{personAcc.id=>personAcc},null);  
        
        Map<Id,opportunity> mapOppRecs = new Map<Id,opportunity>();
        mapOppRecs.put(oppUpdate.id,oppUpdate);
        String jsonOldMap = JSON.serialize(mapOppRecs); 
        map<id, sObject> oldMap = (Map<id, sObject>) JSON.deserialize(jsonOldMap,Map<id, sObject>.class); 
        CreateUnifiedJSON.buildUnifiedJSON('Opportunity','',oppIds,newSObjList,oldMap,null );  
        //test.starttest();
        //CreateUnifiedJSON .sendEventUpdates();
        //test.stoptest();
        
        CreateUnifiedJSON.sendEventUpdates(new Map<String,String>{personAcc1.id=>'xxx'});
    }

}