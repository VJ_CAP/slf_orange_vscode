/******************************************************************************************
* Description: Test class to cover SLFEncryption
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Raviteja Sajja          12/07/2017          Created
******************************************************************************************/
@isTest
public class SLFEncryption_Test {

    @TestSetup
    static void initData()
    {
        test.startTest();
        insert new SFSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), AES_Encryption_Key__c = 'iasdgsagdasdksakdh72379217adoasl');
        test.stopTest();
    }
    
    private testMethod static void encryptionTest(){
        test.startTest();
        String testData = 'This is a test class';
        String encryptedData = SLFEncryption.encryptData(testData);
        system.debug('encryptedData - '+encryptedData);
        String decryptedData = SLFEncryption.decryptData(encryptedData);
        system.debug('decryptedData - '+decryptedData);
        system.assertEquals(decryptedData, testData);
        test.stopTest();
    }
    private testMethod static void encryptionTest01(){
        test.startTest();
        String testData = 'This is a test class';
        String encryptedData = SLFEncryption.encryptDataAES128(testData);
        system.debug('encryptedData - '+encryptedData);
        String decryptedData = SLFEncryption.decryptData(encryptedData);
        system.debug('decryptedData - '+decryptedData);
        //system.assertEquals(decryptedData, testData);
        test.stopTest();
    }
}