@isTest
private class deleteAccountsShareTest {

    private static testMethod void test() {
            SLFUtilityTest.createCustomSettings();
                
        //get the person account record type
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account(); 
        acc.name = 'Test Account';
        acc.RecordTypeId = accRecTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id,Is_Default_Owner__c = true);
        insert con;  
        
        
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        
        
        Account acc1 = new Account();
        acc1.name = 'Test Account';
        acc1.RecordTypeId = accRecTypeInfoMap.get('Business Account').getRecordTypeId();
        acc1.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.BillingStateCode = 'CA';
        acc1.Installer_Legal_Name__c='Bright Solar Planet';
        acc1.Solar_Enabled__c = true;
        insert acc1;
        
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acc1.Id,Is_Default_Owner__c = true,ReportsToid=con.id);
        insert con1;  
        
        
        User portalUser1 = new User(alias = 'testc1', Email='test1c@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test1', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass1@mail.com',ContactId = con1.Id);
        insert portalUser1;
        
        
        
        
            Account acc2 = new Account();
            acc2.LastName = 'Test Account';
            acc2.Push_Endpoint__c ='www.test.com';
            acc2.PersonEmail = 'slfaccount@slf.com';
            acc2.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
            acc2.Installer_Email__c = 'slfaccount@slf.com';
            acc2.Installer_Legal_Name__c='Bright Solar Planet';
            insert acc2;
            
            Account businessAcc = new Account();
            businessAcc.Name = 'Test Account';
            businessAcc.Push_Endpoint__c ='www.test.com';
            businessAcc.Installer_Legal_Name__c='Bright Solar Planet';
            businessAcc.Solar_Enabled__c = true;
            insert businessAcc;
        
            Test.startTest();
            deleteAccountsShare batchobj = new deleteAccountsShare(null);
            list<AccountShare> lstaccShare = [select id from AccountShare];
            batchobj.start(null);
            batchobj.execute(null,lstaccShare);
            batchobj.finish(null);
             Test.stopTest();
    }

}