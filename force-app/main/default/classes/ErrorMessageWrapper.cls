/**
* Description: Wrapper class to display error information on JSON responce.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public with sharing class ErrorMessageWrapper implements IRestRequest, IRestResponse {
    public String error{get; set;}
  
}