/* callFNITriggerStaticHelper
 * 
 * This helper file helps determine if FNI should be called,
 * it works both for the Credit object and the lead.
 * 
 * test or todo:
 * //don't recall if there is an error (E), sunlight must clear the E to get a recall.  This prevents the recursion of a E being set.
 * Developed by Paul Mackinaw, LeanCog 4/7/16
 *  
 */
public class callFNITriggerStaticHelper {
    public static boolean callFNI(String newFirstName, 
                                  String newLastName, 
                                  String newStreet, 
                                  String newCity, 
                                  String newStateCode, 
                                  String newPostalCode, 
                                  boolean newCustAuth, 
                                  String newFNIDec,
                                  Date newDOB,
                                  Decimal newYearlyIncome,
                                  String newSSN,
                                  String oldFirstName,
                                  String oldLastName,
                                  String oldStreet,
                                  String oldCity,
                                  String oldStateCode,
                                  String oldPostalCode,
                                  Date oldDOB,
                                  Decimal oldYearlyIncome,
                                  String oldSSN,
                                  String stipCode
                                 ){
        //check if min required fields are completed, only call fni if so
            if(newFirstName != null && 
               newLastName != null && 
               newStreet != null &&
               newCity != null &&
               newStateCode != null &&
               newPostalCode != null &&
               newYearlyIncome != null &&
               newCustAuth == true &&
               newFNIDec != '0'
                //newFNIDec != ''
                ){//don't call on 0 because it should already be in the queue.
                    //check if FNI decision is set (if not, make first call) 
                   if(newFNIDec == null/* || oldCreditMap == null*/){
                       
                       return true;
                   }else{
                       //fni has already been called, check if min fields have changed
                       if(newFirstName != oldFirstName || 
                          newLastName != oldLastName || 
                          newStreet != oldStreet ||
                          newCity != oldCity ||
                          newStateCode != oldStateCode ||
                          newPostalCode != oldPostalCode){
                              //some of the min fields have changed, recall fni
                              return true;
                        } else{
                           //min fields haven't changed, but if fni decision was 3, check if optional fields have changed - if so, call fni
                            //if(newFNIDec == '4'){ Commented by suresh on 20/04/2018
                            if((newFNIDec == 'P' && stipCode=='MI') || (newFNIDec == 'D' && stipCode=='D27') ){ //Added by suresh on 20/04/2018
                                   if(newDOB != oldDOB || newYearlyIncome != oldYearlyIncome){
                                          return true;
                                      }else if(newSSN != oldSSN){
                                          if(newSSN != null){
                                              if(newSSN.length() == 11)
                                              {                                                 
                                                  return true;
                                              }
                                          }
                                      }
                              }      
                        }
                   }
               }
        return false;
    }
    public static String getState(String StateCode){
        String state;
        if(StateCode == 'AL') state = 'Alabama';
        if(StateCode == 'AK') state = 'Alaska';
        if(StateCode == 'AZ') state = 'Arizona';
        if(StateCode == 'AR') state = 'Arkansas';
        if(StateCode == 'CA') state = 'California';
        if(StateCode == 'CO') state = 'Colorado';
        if(StateCode == 'CT') state = 'Connecticut';
        if(StateCode == 'DC') state = 'District of Columbia'; // Added by Srikanth
        if(StateCode == 'DE') state = 'Delaware';
        if(StateCode == 'FL') state = 'Florida';
        if(StateCode == 'GA') state = 'Georgia';
        if(StateCode == 'HI') state = 'Hawaii';
        if(StateCode == 'ID') state = 'Idaho';
        if(StateCode == 'IL') state = 'Illinois';
        if(StateCode == 'IN') state = 'Indiana';
        if(StateCode == 'IA') state = 'Iowa';
        if(StateCode == 'KS') state = 'Kansas';
        if(StateCode == 'KY') state = 'Kentucky';
        if(StateCode == 'LA') state = 'Louisiana';
        if(StateCode == 'ME') state = 'Maine';
        if(StateCode == 'MD') state = 'Maryland';
        if(StateCode == 'MA') state = 'Massachusetts';
        if(StateCode == 'MI') state = 'Michigan';
        if(StateCode == 'MN') state = 'Minnesota';
        if(StateCode == 'MS') state = 'Mississippi';
        if(StateCode == 'MO') state = 'Missouri';
        if(StateCode == 'MT') state = 'Montana';
        if(StateCode == 'NE') state = 'Nebraska';
        if(StateCode == 'NV') state = 'Nevada';
        if(StateCode == 'NH') state = 'New Hampshire';
        if(StateCode == 'NJ') state = 'New Jersey';
        if(StateCode == 'NM') state = 'New Mexico';
        if(StateCode == 'NY') state = 'New York';
        if(StateCode == 'NC') state = 'North Carolina';
        if(StateCode == 'ND') state = 'North Dakota';
        if(StateCode == 'OH') state = 'Ohio';
        if(StateCode == 'OK') state = 'Oklahoma';
        if(StateCode == 'OR') state = 'Oregon';
        if(StateCode == 'PA') state = 'Pennsylvania';
        if(StateCode == 'RI') state = 'Rhode Island';
        if(StateCode == 'SC') state = 'South Carolina';
        if(StateCode == 'SD') state = 'South Dakota';
        if(StateCode == 'TN') state = 'Tennessee';
        if(StateCode == 'TX') state = 'Texas';
        if(StateCode == 'UT') state = 'Utah';
        if(StateCode == 'VT') state = 'Vermont';
        if(StateCode == 'VA') state = 'Virginia';
        if(StateCode == 'WA') state = 'Washington';
        if(StateCode == 'WV') state = 'West Virginia';
        if(StateCode == 'WI') state = 'Wisconsin';
        if(StateCode == 'WY') state = 'Wyoming';
        
        return state;
    }
}