/**
* Description: Download completed loandoc with certificate from DocuSign and upload to Salesforce attachment.
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Srikanth             12/29/2017                 Created
******************************************************************************************/
global class DownloadLoanDocs {
  @future(callout = true)
    public static void downloadDocs(list<Id> recordIds) {
        system.debug('### Entered into downloadDocs() of ' + DownloadLoanDocs.class);
        list<SLF_Boomi_API_Details__c> clist = SLF_Boomi_API_Details__c.getAll().values();
        list<Attachment> aList = new list<Attachment>();
        String resLst = '';
        String idStr = '';
        
        SLF_Boomi_API_Details__c c = SLF_Boomi_API_Details__c.getValues('DocusignAPI');      
        
        try{
            if(c != NULL)
            {
                String apiName = c.Name;
                if(apiName.equalsIgnoreCase('DocusignAPI'))
                {
                    //list<Underwriting_File__c> uwList = new list<Underwriting_File__c>(); // comented as part of 1930 by Adithya
                    list<Box_Fields__c> boxFieldList = new list<Box_Fields__c>(); // Added as part of 1930 by Adithya
                    
                    for(dsfs__Docusign_Status__c d : [select Id, dsfs__DocuSign_Envelope_ID__c, dsfs__Opportunity__r.Id, dsfs__Opportunity__r.Change_Order_Status__c, dsfs__Opportunity__r.Applicant_First_Name__c, dsfs__Opportunity__r.Applicant_Last_Name__c, dsfs__Opportunity__r.Long_Term_Term__c, dsfs__Opportunity__r.Long_Term_APR__c from dsfs__Docusign_Status__c where id IN: recordIds])
                    {
                        string responseValue = '';
                        
                        Http http = new Http();
                        HttpResponse response;
                        HttpRequest request = new HttpRequest();
                        request.setEndpoint(c.Endpoint_URL__c+d.dsfs__DocuSign_Envelope_ID__c+'/documents/combined');
                        request.setMethod('GET');
                        request.setHeader('Content-Type', 'application/json');
                        request.setCompressed(true);
                        request.setTimeout(60000);
                        request.setHeader('X-DocuSign-Authentication','<DocuSignCredentials><Username>'+c.username__c+'</Username><Password>'+c.password__c+'</Password><IntegratorKey>'+c.APIKey__c+'</IntegratorKey></DocuSignCredentials>');
                        if(Test.isRunningTest()){
                            response = new HttpResponse();
                            response.setStatusCode(200);
                            Blob bodyBlob = Blob.valueOf('Test Attachment Body');
                            response.setBodyAsBlob(bodyBlob);
                        }
                        //if(!Test.isRunningTest()){
                        else{
                            response = http.send(request);
                        }
                            if (response.getStatusCode() != 200) {
                                // insert error log
                                resLst+= d.id+'_'+response.getStatus()+'@@@';
                                idStr+=(idStr==''?d.id:', '+d.id);
                            }
                            else
                            {
                                //string responseValue = '';
                                responseValue = response.getBody();
                                System.debug('### o/p response ');

                                ValidateDocumentUploadDataObject validateDocUploadObj = new ValidateDocumentUploadDataObject();
                                validateDocUploadObj.documentFolderType  = 'Loan Agreements';
                                validateDocUploadObj.opportunityId = d.dsfs__Opportunity__r.Id;
                                validateDocUploadObj.externalId  = '';
                                validateDocUploadObj.fileName = 'Sunlight Financial '+d.dsfs__Opportunity__r.Applicant_First_Name__c+' '+d.dsfs__Opportunity__r.Applicant_Last_Name__c+' '+d.dsfs__Opportunity__r.Long_Term_Term__c/12+' '+d.dsfs__Opportunity__r.Long_Term_APR__c+' Loan Documents.pdf';
                                ValidateDocumentUploadDataServiceImpl validdateDocUpload = new ValidateDocumentUploadDataServiceImpl();
                                
                                ValidateDocumentUploadDataObject parseValidateDocUpload = (ValidateDocumentUploadDataObject)validdateDocUpload.transformOutput(validateDocUploadObj);
                                ValidateDocumentUploadDataObject.OutputWrapper  op = parseValidateDocUpload.output; 

                                String fileId;
                                
                                if(op.isFileNameAlreadyExist)
                                    fileId = uploadFileToBox(op.documentFolderId, response.getBodyAsBlob(), op.accessToken, 'Sunlight Financial '+d.dsfs__Opportunity__r.Applicant_First_Name__c+' '+d.dsfs__Opportunity__r.Applicant_Last_Name__c+' '+d.dsfs__Opportunity__r.Long_Term_Term__c/12+' '+d.dsfs__Opportunity__r.Long_Term_APR__c+' Loan Documents_'+Datetime.now()+'.pdf');
                                else
                                    fileId = uploadFileToBox(op.documentFolderId, response.getBodyAsBlob(), op.accessToken, 'Sunlight Financial '+d.dsfs__Opportunity__r.Applicant_First_Name__c+' '+d.dsfs__Opportunity__r.Applicant_Last_Name__c+' '+d.dsfs__Opportunity__r.Long_Term_Term__c/12+' '+d.dsfs__Opportunity__r.Long_Term_APR__c+' Loan Documents.pdf');                            
                                
                                //Create metadata
                                box.Toolkit boxToolkit = new box.Toolkit();
                                setFileMetadata.Output oput = setFileMetadata.createFileMetaData('SF Admin User',fileId,'create');
                                boxToolkit.commitChanges();
                                system.debug('### Updated metadata ...'+oput.success);
                                                                
                                //Update Underwriting record TS fields from custom settings when the folder is created for first time.                         
                                
                                if(op.uwId != NULL && !String.isBlank(op.uwId))
                                {
                                    // comented as part of 1930 by Adithya
                                    //Underwriting_File__c uf = [SELECT Id FROM Underwriting_File__c WHERE Id =: op.uwId LIMIT 1];
                                    
                                    // Added as part of 1930 by Adithya
                                    Box_Fields__c boxFields = [SELECT Id FROM Box_Fields__c WHERE Underwriting__c =: op.uwId LIMIT 1];
                                    map<String,SF_Category_Map__c> categoryFolderMap = new map<String,SF_Category_Map__c>{};
                                        
                                        for (SF_Category_Map__c cm : SF_Category_Map__c.getAll().values())
                                            categoryFolderMap.put (cm.Folder_Name__c, cm);
                                    
                                    SF_Category_Map__c cat = categoryFolderMap.get ('Loan Agreements');
                                    
                                    // updated as part of 1930 by Adithya
                                    if(!op.isFolderAlreadyExist){
                                        boxFields.put(cat.Folder_Id_Field_on_Underwriting__c, op.sharedLink);
                                        
                                        if(d.dsfs__Opportunity__r.Change_Order_Status__c == '' || d.dsfs__Opportunity__r.Change_Order_Status__c == NULL)
                                            boxFields.put (cat.TS_Field_On_Underwriting__c, System.now());
                                        
                                        boxFields.put (cat.Last_Run_TS_Field_on_Underwriting_File__c, System.now());
                                    }else{
                                        boxFields.put (cat.Last_Run_TS_Field_on_Underwriting_File__c, System.now());
                                    }
                                    boxFieldList.add(boxFields);
                                }
                            }
                        //}
                    }   
                    if(!boxFieldList.isEmpty() && boxFieldList != NULL)
                        update boxFieldList;
                }   
            }
            if(string.isNotBlank(resLst)){
                ErrorLogUtility.writeLog('DownloadLoanDocs.downloadDocs()',26,'downloadDocs()',resLst,'Error',idStr);
            }
        }
        catch(Exception err){
            system.debug('### DownloadLoanDocs.downloadDocs():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('DownloadLoanDocs.downloadDocs()',err.getLineNumber(),'',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));           
        }
        system.debug('### Exit from downloadDocs() of ' + DownloadLoanDocs.class);
    }
    
    public static String uploadFileToBox(String strFolderId,Blob file,String token, String fileName)
    {
        system.debug('### Entered into uploadFileToBox() of ' + DownloadLoanDocs.class);
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+fileName+'";\nContent-Type: multipart/form-data;'+'\nnon-svg='+True;
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        HttpResponse res;
        String strFileId;
        
        while(headerEncoded.endsWith('='))
        {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        
        String bodyEncoded = EncodingUtil.base64Encode(file);
        
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
        
        if(last4Bytes.endsWith('==')) 
        {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        }
         
         else if(last4Bytes.endsWith('=')) 
         {
             last4Bytes = last4Bytes.substring(0,3) + 'N';
             bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
             footer = '\n' + footer;
             String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
             bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
         } 
         
         else 
         {
             footer = '\r\n' + footer;
             String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
             bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
         }
    
         String sUrl = 'https://upload.box.com/api/2.0/files/content?parent_id='+strFolderId;
         HttpRequest req = new HttpRequest();
         
         req.setHeader('Content-Type','multipart/form-data;non_svg='+True+';boundary='+boundary);
         
         req.setMethod('POST');
         req.setEndpoint(sUrl);
         req.setBodyAsBlob(bodyBlob);
         req.setTimeout(60000);
         req.setHeader('Authorization', 'Bearer '+token);
         req.setHeader('Content-Length',String.valueof(req.getBodyAsBlob().size()));
         
         Http http = new Http();
         
         system.debug('### size ###'+req.getBodyAsBlob().size());
         
         if(Test.isRunningTest()) {
             
             // Create a fake response
             res = new HttpResponse();
             res.setStatusCode(201);
             //dummy body
             String body = '{"entries":[{"id":"1231452"}]}';
             res.setBody(body);
         }
         else {           
             res = http.send(req);               
         }
        
        system.debug('### Upload status...'+res.getBody()+'###');        
        DownloadLoanDocs.fileUploadJsonResponse ew = parseUploadFileResponse(res.getBody());
        system.debug('### File id...'+ew.entries[0].id);
        
        system.debug('### Exit from uploadFileToBox() of ' + DownloadLoanDocs.class);
        
        return ew.entries[0].id;        
    }
    
    /*
     * Parses the response comming from upload file API from box.com
     * */
    public static DownloadLoanDocs.fileUploadJsonResponse parseUploadFileResponse(String json){
        return (DownloadLoanDocs.fileUploadJsonResponse) System.JSON.deserialize(json, DownloadLoanDocs.fileUploadJsonResponse.class);
    }
    
    public class fileUploadJsonResponse{
        public list<Entries> entries;
    }
    
    class Entries {
        public String id;   //261163587532
    }
}