/******************************************************************************************
* Description: Test class to cover UserProfileServiceImpl Services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
XXXXXXX              06/10/2019             Created
******************************************************************************************/
@isTest
public class UserProfileServiceImple_Test {
     
    private testMethod static void DualStepVerificationTest(){
        
        List<TriggerFlags__c> trgrLsit = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccountflag = new TriggerFlags__c();
        trgAccountflag.Name ='Account';
        trgAccountflag.isActive__c = true;
        trgrLsit.add(trgAccountflag);
        
        TriggerFlags__c trgconflag = new TriggerFlags__c();
        trgconflag.Name ='Contact';
        trgconflag.isActive__c = true;
        trgrLsit.add(trgconflag);
        
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c = true;
        trgrLsit.add(trgUserflag);
        insert trgrLsit;
        
        Account acc = new Account();
        acc.name = 'Test Account';
        insert acc;
        
        Contact conExec = new Contact(LastName ='conExec',AccountId = acc.Id);
        insert conExec;  
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
     
        User portalexecUser = new User(alias = 'testc', 
                                   Email='testexecutive@mail.com', 
                                   IsActive = true,
                                   Hash_Id__c='111',
                                   EmailEncodingKey='UTF-8', 
                                   LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', 
                                   ProfileId = p.Id, 
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testexecutive@mail.com',
                                   ContactId = conExec.Id);
                                   
        insert portalexecUser;
        
        String dualStepVerificationstr;
        Map<String, String> createincenMap = new Map<String, String>();
        
        System.runAs(portalexecUser){
            dualStepVerificationstr = '{"users": [{"email": "testexecutive@mail.com","password": "Test123!"}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'getsecurityquestions');
            
            dualStepVerificationstr = '{"users": [{"email": "testexecutive@mail.com","password": "Test123!","ignoreCount": 1}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'getsecurityquestions');
            
            dualStepVerificationstr = '{"users": [{"email": "testexecutive@mail.com","password": "Test123!","selectedQuestionsAndAnswers": [{"question": "What is your grandmother\'s (on your mother\'s side) maiden name?","answer": "test"},{"question": "What was the name of the hospital where you were born?","answer": "test"},{"question": "What was the name of the company where you had your first job?","answer": "test"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'getsecurityquestions');
            
            dualStepVerificationstr = '{"users": [{"email": "testexecutive@mail.com","password": "Test123!","ignoreCount": 1,"selectedQuestionsAndAnswers": [{"question": "What is your grandmother\'s (on your mother\'s side) maiden name?","answer": "test"},{"question": "What was the name of the hospital where you were born?","answer": "test"},{"question": "What was the name of the company where you had your first job?","answer": "test"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'getsecurityquestions');
            
            dualStepVerificationstr = null;
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'getsecurityquestions');
            //214
            dualStepVerificationstr = '{}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'userprofile');
            //369
            dualStepVerificationstr = '{"users": [{"hashId": "","Id": ""}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'userprofile');
            //373
            dualStepVerificationstr = '{"users": [{"hashId": "123","Id": "123"}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'userprofile');
            //295
            dualStepVerificationstr = '{"users": [{"hashId": "123","Id": ""}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'userprofile');
            //success
            dualStepVerificationstr = '{"users": [{"hashId": "","Id": "'+portalexecUser.Id+'"}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'userprofile');
            //success2
            dualStepVerificationstr = '{"users": [{"hashId": "111","Id": ""}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'userprofile');
            //exception
            dualStepVerificationstr = '{"users": {"hashId": "","Id": "'+portalexecUser.Id+'"}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,dualStepVerificationstr,'userprofile');
        }               
    }
}