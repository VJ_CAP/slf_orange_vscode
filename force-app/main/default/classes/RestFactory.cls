/**
* Description: Factory class to create instance of each object specific service implementation.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description 
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public with sharing class RestFactory{
    
    /**
    * Description: Process the request by creating an instance of the Object specific ServiceImpl class.
    *
    * @param rw             RequestWrapper class contains all post body messages like request action parameters, request POST data.
    * return res            ResponseWrapper class with response status message, status code and response collection.
    */
    public static ResponseWrapper invokeService(RequestWrapper rw){
        system.debug('### Entered into invokeService() ' + RestFactory.class);
        ResponseWrapper res = null; //Return response wrapper after processing response
        try
        {
            RequestActionParam reqAP = rw.reqAction; //Request action parameters like AppId, sObject, Action (like GET, POST, PUT), operation like CREATE, UPDATE, READ, DELETE etc
           
            Type srvImplType;
            
            List<SLF_Webservices_Urimapping__mdt> uridata = new List<SLF_Webservices_Urimapping__mdt>();
            uridata =[Select DeveloperName,Service_Impl_Type__c from SLF_Webservices_Urimapping__mdt where DeveloperName=:rw.reqAction.serviceName Limit 1];
            if(!uridata.isEmpty())
                srvImplType = Type.forName(uridata[0].Service_Impl_Type__c);
            
            IRestService restSrv = (IRestService)srvImplType.newInstance();
            //Invoke the appropriate action in the service implementation.
            res = executeService(restSrv, rw);
        }
        catch(Exception ex)
        {
            system.debug('### RestFactory.invokeService():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('RestFactory.invokeService()',ex.getLineNumber(),'invokeService()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from invokeService() of '+ RestFactory.class);
        return res;
    }
    
    
      
    /**
    * Description: Process the requested data in its associated object's ServiceImpl class.
    *
    * @param restSrv        IRestService is an instance of the ServiceImpl class.
    * @param rw             RequestWrapper class contains all post body messages like request action parameters, request POST data.
    * return res            ResponseWrapper class with response status message, status code and response collection.
    */
    private static ResponseWrapper executeService(IRestService restSrv, RequestWrapper rw){
        system.debug('### Entered into executeService() ' + RestFactory.class);
        ResponseWrapper res = null; //Return response wrapper after processing response
        try
        {
            if(String.isNotBlank(rw.reqAction.action))
            {
                if('POST' == rw.reqAction.action)
                {
                    res = new ResponseWrapper();
                    res.response = restSrv.fetchData(rw); //fetch data from salesforce
                } 
                // adding GET method - Srikanth - 08/23
                /*
                if('GET' == rw.reqAction.action)
                {
                    res = new ResponseWrapper();
                    res.response = restSrv.fetchData(rw); //fetch data from salesforce
                } 
                */
            }
        }
        catch(Exception ex)
        {
            system.debug('### RestFactory.executeService():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('RestFactory.executeService()',ex.getLineNumber(),'executeService()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from executeService() ' + RestFactory.class);
        return res;
    }
}