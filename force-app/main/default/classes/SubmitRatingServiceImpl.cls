/**
* Description: User validation logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suresh Kumar          	10/26/2018          Created
******************************************************************************************/
public with sharing class SubmitRatingServiceImpl extends RESTServiceBase{  
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back with data and response status code.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + SubmitRatingServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        SubmitRatingDataServiceImpl submitRatingDataSrvImpl = new SubmitRatingDataServiceImpl(); 

        try{
            system.debug('### rw.reqDataStr***'+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            res.response = submitRatingDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
           system.debug('### SubmitRatingServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' SubmitRatingServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + SubmitRatingServiceImpl.class);
        return res.response ;
    }
}