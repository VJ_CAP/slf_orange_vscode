/**
    * Description: Utility class contains all CRM relation funtionality which can be used globally.
    *
    *   Modification Log : 
    ---------------------------------------------------------------------------
    Developer                   Date            Description
    ---------------------------------------------------------------------------
    Suresh Kumar                26/07/2018      Created
    *****************************************************************************************
*/

public class SyncDesyncUtilities {
    
    /**
    * Description: 1. DeSync all Credit Records
    *              2. sync credit records 
    * @param  set of Opportunity in opptyIdset
    * @param  List of SLF Credit records in SLFCreditIds.
    */
    public static void deSyncCreditRecord(set<Id> opptyIdset, set<Id> SLFCreditIds){
        system.debug('### Entered into deSyncCreditRecord() of '+ SyncDesyncUtilities.class);
        try{
            //Desync all credit records
            if(!opptyIdset.isEmpty())
            {
                List<SLF_Credit__c> updateCredit = new List<SLF_Credit__c>();
               // List<SLF_Credit__c> creditList = [SELECT Id,IsSyncing__c,Name,Opportunity__c,Status__c FROM SLF_Credit__c where IsSyncing__c =: true AND Opportunity__r.StageName != 'Closed Won' AND Opportunity__c IN: opptyIdset ];
                List<SLF_Credit__c> creditList = [SELECT Id,IsSyncing__c,Name,Opportunity__c,Status__c FROM SLF_Credit__c where IsSyncing__c =: true AND Opportunity__c IN: opptyIdset ];

                if(!creditList.isEmpty()){
                    for(SLF_Credit__c crdRec :creditList){
                        if(!SLFCreditIds.contains(crdRec.id)){
                            crdRec.IsSyncing__c = false;
                            updateCredit.add(crdRec);
                        }
                        
                    }
                }
                if(!updateCredit.isEmpty())
                    update updateCredit;
            }
            
            //Added by Rajesh on 08/24/2018 for Orange-1166
            /*list<SLF_Credit__c> newCreditLst = [select Id,IsSyncing__c,Name,Total_Loan_Amount__c,Customer_has_authorized_credit_hard_pull__c,Credit_Submission_Date__c,FNI_Credit_Expiration__c,Status__c from SLF_Credit__c where Opportunity__r.StageName != 'Closed Won' AND Total_Loan_Amount__c>0 AND Id IN: SLFCreditIds];
            syncCreditToOpportunity(newCreditLst);*/
            syncCreditToOpportunity(SLFCreditIds);
            //Commented by Rajesh on 08/24/2018 for Orange-1166
            /*
            //sync credit records
            if(!SLFCreditIds.isEmpty())
            {
                List<SLF_Credit__c> updateSLFCredit = new List<SLF_Credit__c>();
                List<SLF_Credit__c> creditList = [SELECT Id,IsSyncing__c,Name,Opportunity__c,Long_Term_Amount_Approved__c, Short_Term_Amount_Approved__c, Customer_has_authorized_credit_hard_pull__c,Status__c,Credit_Submission_Date__c,FNI_Credit_Expiration__c FROM SLF_Credit__c where Opportunity__r.StageName != 'Closed Won' AND IsSyncing__c =: false AND Id IN: SLFCreditIds AND Status__c IN ('Auto Approved','Manual Approved') ];
                if(creditList.size() > 0)
                {
                    integer lstsize = creditList.size();
                    creditList[lstsize-1].IsSyncing__c = true;
                    updateSLFCredit.add(creditList[lstsize-1]);
                    
                }else{
                    list<SLF_Credit__c> newCreditLst = [select Id,Name,Total_Loan_Amount__c,Customer_has_authorized_credit_hard_pull__c,Credit_Submission_Date__c,FNI_Credit_Expiration__c,Status__c from SLF_Credit__c where Opportunity__r.StageName != 'Closed Won' AND Id IN: SLFCreditIds];
                    //boolean syncing = false;
                    Map<Id,Decimal> SLFCreditMap = new Map<Id,Decimal>();
                    Decimal TotalLoanAmount = 0;
                    for(SLF_Credit__c creditIterate : newCreditLst)
                    {
                        if(null != creditIterate.Total_Loan_Amount__c && creditIterate.Total_Loan_Amount__c != 0 && creditIterate.Total_Loan_Amount__c > TotalLoanAmount )
                        {
                            SLFCreditMap = new Map<Id,Decimal>();
                            TotalLoanAmount = creditIterate.Total_Loan_Amount__c;
                            SLFCreditMap.put(creditIterate.Id,TotalLoanAmount);
                        }
                    }
                    if(!SLFCreditMap.isEmpty())
                    {
                        for(SLF_Credit__c creditIterate : newCreditLst)
                        {
                            if(SLFCreditMap.containsKey(creditIterate.Id))
                            {
                                creditIterate.IsSyncing__c = true;
                                updateSLFCredit.add(creditIterate);
                            }
                        }
                    }
                }
                if(!updateSLFCredit.isEmpty())
                    syncCredit(updateSLFCredit);
            }
            */
        }catch(Exception ex)
        {
            system.debug('### SyncDesyncUtilities.deSyncCreditRecord():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SyncDesyncUtilities.deSyncCreditRecord()',ex.getLineNumber(),'deSyncCreditRecord()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));            
        }
        system.debug('### Exit from deSyncCreditRecord() of '+ SyncDesyncUtilities.class);
    }
    
    //Commented by Rajesh on 08/24/2018 for Orange-1166
    /**
    * Description: 1. sync credit records 
    *              
    * @param  List of SLF Credit records in updateCredit.
    */
    /*
    public static void syncCredit(List<SLF_Credit__c> updateCredit){
        if(!updateCredit.isEmpty())
        {
            system.debug('### updateCredit in syncCredit() before updateData '+updateCredit);
            //Update credit records
            SLFUtility.updateData(updateCredit); 
            system.debug('### updateCredit in syncCredit() after updateData '+updateCredit);
            //Sync Credit Record with Opportunity
            syncCreditToOpportunity(updateCredit);
        }
    }
    */
    /**
    * Description: Sync Credit Record with Opportunity.
    * 
    * @param  Credit records.
    */
    public static void syncCreditToOpportunity(set<Id> SLFCreditIds){
        system.debug('### Entered into syncCreditToOpportunity() of '+ SyncDesyncUtilities.class);
        try{
            String fieldnames='';
            
            List<Credit_Syncing__mdt> credLst = [SELECT Id,MasterLabel,DeveloperName,Credit_Field__c,Opportunity_Field__c
                                                 FROM Credit_Syncing__mdt where MasterLabel = 'Credit'];
            Map<string,string>  creditSyncingMap = new  Map<string,string>();       
            for(Credit_Syncing__mdt creditSyncingIterate : credLst)         
            {
                creditSyncingMap.put(creditSyncingIterate.Credit_Field__c,creditSyncingIterate.Opportunity_Field__c);
                if (creditSyncingIterate.Credit_Field__c != null) {
                    fieldnames += ','+creditSyncingIterate.Credit_Field__c;
                }
            }   
            system.debug('### fieldnames  '+ fieldnames);
            List<SLF_Credit__c> newCredit = new List<SLF_Credit__c>();
            if(!String.isBlank(fieldnames)){
                //String queryString = 'Select id,Name,IsSyncing__c,Opportunity__c'+fieldnames+' from SLF_Credit__c where Opportunity__r.StageName != \'Closed Won\' AND Total_Loan_Amount__c>0 AND Id IN: SLFCreditIds';
                String queryString = 'Select id,Name,IsSyncing__c,Opportunity__c'+fieldnames+' from SLF_Credit__c where Total_Loan_Amount__c>0 AND Id IN: SLFCreditIds';

                newCredit = database.query(queryString);
            }
            Map<Id,sObject> oppUpdateMap = new Map<Id,sObject>();
            system.debug('### newCredit - '+newCredit);
            for(SLF_Credit__c creditIterate : newCredit){
                if(creditIterate.IsSyncing__c)
                {
                    for(string strIterate : creditSyncingMap.keyset()) //Application_ID_c
                    {
                        sObject sObjOpp = Schema.getGlobalDescribe().get('Opportunity').newSObject() ;
                        
                        if(oppUpdateMap.containskey(creditIterate.Opportunity__c))
                        {
                            sObjOpp = oppUpdateMap.get(creditIterate.Opportunity__c);
                        }
                        string oppFld = creditSyncingMap.get(strIterate);
                        sObjOpp.put('Id' ,(String)creditIterate.get('Opportunity__c')); 
                        sObjOpp.put(oppFld ,(object)creditIterate.get(strIterate)); 
                        oppUpdateMap.put(creditIterate.Opportunity__c,sObjOpp);
                    }
                }   
            }
            system.debug('### oppUpdateMap - '+oppUpdateMap.Values());
            if(!oppUpdateMap.isEmpty()){
                oppUpdateMap = calcSunAdvanceAmount(oppUpdateMap);//Added by Ravi as part of ORANGE-2956
                system.debug('### oppUpdateMap - '+oppUpdateMap.Values());
                update oppUpdateMap.Values();
            }
        }catch(Exception err){
            
            system.debug('### SyncDesyncUtilities.syncCreditToOpportunity():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('SyncDesyncUtilities.syncCreditToOpportunity()',err.getLineNumber(),'OnAfterUpdate()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            
        }
        system.debug('### Exit from syncCreditToOpportunity() of '+ SyncDesyncUtilities.class);
    }
    
    
    /**
    * Description:  This method is used to re-evaluate SunAdvance Amount on Opportunity records
    * Added by Ravi as part of ORANGE-2956
    */
    public static Map<Id,sObject> calcSunAdvanceAmount(Map<Id,sObject> oppIdMap){
        
        system.debug('### Entered into calcSunAdvanceAmount() of '+ SyncDesyncUtilities.class);
        system.debug(oppIdMap);
        try{
            if(!oppIdMap.isEmpty()){
                
                List<Underwriting_File__c> uwLst = [Select id, Net_Funding_Required__c, Opportunity__c, Opportunity__r.SunAdvance__c, Opportunity__r.Permit_Split__c, Opportunity__r.SunAdvance_Max_Advance_Amount__c from Underwriting_File__c where Opportunity__r.Project_Category__c = 'Solar' and Opportunity__r.SunAdvance__c = true and Opportunity__r.Id IN : oppIdMap.keySet()];
                
                system.debug('uwLst - '+uwLst);
                
                if(!uwLst.isEmpty()){
                                        
                    for(Underwriting_File__c uwIterate : uwLst){
                        
                        Opportunity opp = (Opportunity)oppIdMap.get(uwIterate.Opportunity__c);
                        
                        system.debug('### Opty SunAdvance__c - '+uwIterate.Opportunity__r.SunAdvance__c);
                        system.debug('### Opty Permit_Split__c - '+opp.Permit_Split__c);
                        system.debug('### Opty SunAdvance_Max_Advance_Amount__c - '+uwIterate.Opportunity__r.SunAdvance_Max_Advance_Amount__c);
                        system.debug('### UW Net_Funding_Required__c - '+uwIterate.Net_Funding_Required__c);
                        
                        if(null != opp.Permit_Split__c && null != uwIterate.Opportunity__r.SunAdvance_Max_Advance_Amount__c && (uwIterate.Net_Funding_Required__c/100 * opp.Permit_Split__c < uwIterate.Opportunity__r.SunAdvance_Max_Advance_Amount__c)){
                            opp.SunAdvance_Advance_Amount__c = (uwIterate.Net_Funding_Required__c/100) * opp.Permit_Split__c;
                            oppIdMap.put(opp.Id,opp);
                        }
                        else if(null != opp.Permit_Split__c && null != uwIterate.Opportunity__r.SunAdvance_Max_Advance_Amount__c && (uwIterate.Net_Funding_Required__c/100 * opp.Permit_Split__c > uwIterate.Opportunity__r.SunAdvance_Max_Advance_Amount__c)){
                            opp.SunAdvance_Advance_Amount__c = uwIterate.Opportunity__r.SunAdvance_Max_Advance_Amount__c;
                            oppIdMap.put(opp.Id,opp);
                        }
                    }
                    system.debug('### oppIdMap - '+oppIdMap);
                }
            }
        }catch(Exception err){
            
            system.debug('### SyncDesyncUtilities.calcSunAdvanceAmount():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('SyncDesyncUtilities.calcSunAdvanceAmount()',err.getLineNumber(),'OnAfterUpdate()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
        }
        system.debug('### Exit from calcSunAdvanceAmount() of '+ SyncDesyncUtilities.class);
        return oppIdMap;
    }
    
    /**
    * Description:  This method is used to Check Opportunity's installer Account and logged in users installer Account for Orange - 350.
    */
    public static Opportunity desyncQuoteFields(Opportunity oppRec)
    {
        system.debug('### Entered into desyncQuote() of '+ SyncDesyncUtilities.class);
        Map<String,String> errorMap = new Map<String,String>();
        try{
            if(oppRec != null){
                List<Opportunity> oppList = updateDesyncFields(new List<Opportunity>{oppRec},true);
                return oppList[0];
                /*
                oppRec.SyncedQuoteId = null;
                oppRec.ach__c = false;
                //oppRec.combined_loan_amount__c = null;
                oppRec.Final_Monthly_Payment__c  = null;
                oppRec.initial_payment__c = null;
                oppRec.install_cost_cash__c = null;
                oppRec.Monthly_Payment_Escalated_Single_Loan__c = null;
                oppRec.monthly_payment__c = null;
                //oppRec.Non_Solar_Amount__c = null;
                oppRec.sighten_product_uuid__c = null;
                return oppRec;
                */
                
            }
        }catch(Exception ex)
        {
            system.debug('### SyncDesyncUtilities.desyncQuote():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SyncDesyncUtilities.desyncQuote()',ex.getLineNumber(),'desyncQuote()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from desyncQuote() of '+ SyncDesyncUtilities.class);
        return null;
    }
    
    Public static void DesyncCreditFields(Set<Id> oppIds)
    {
        system.debug('### Entered into DesyncCreditFields() of '+ SyncDesyncUtilities.class);
        try
        {
            List<Opportunity> OppLst = new List<Opportunity>();
            /*List<Opportunity> OppLst = [Select id,Name,SyncedQuoteid,Customer_has_authorized_credit_hard_pull__c,Short_Term_Amount_Approved__c,Long_Term_Amount_Approved__c,Application_Status__c From Opportunity where id IN :oppIds];
            List<SLF_Credit__c> creditsUpdate =new List<SLF_Credit__c>();*/
            List<Opportunity> oppstoUpdate =new List<Opportunity>();
            
            //if(!OppLst.isEmpty()){
                for(id opty :oppIds){
                    Opportunity opps = new Opportunity(id = opty);
                    OppLst.add(opps);
                    /*
                    opps.Application_Status__c = 'New';  //Srikanth - Revision - when Credit Opinion is N/A upon running a Quote
                    opps.Long_Term_Amount_Approved__c = null;
                    opps.Short_Term_Amount_Approved__c = null;
                    opps.Credit_Expiration_Date__c = null;
                    opps.Customer_has_authorized_credit_hard_pull__c = false;
                    opps.CL_City__c = null;
                    opps.CL_Owner_of_Record__c = null;
                    opps.CL_State_Code__c = null;
                    opps.CL_Street_Address__c = null;
                    opps.CL_ZipCode__c = null; 
                    // Orange 1291 Mapping
                    opps.Bureau_Primary_Applicant_First_Name__c = null;
                    opps.Bureau_Primary_Applicant_Middle_Name__c = null;
                    opps.Bureau_Primary_Applicant_Last_Name__c=null;
                    opps.Bureau_Primary_SSN__c = null;
                    opps.Bureau_Primary_DoB__c = null;
                    opps.Bureau_Co_Applicant_First_Name__c=null;
                    opps.Bureau_Co_Applicant_Middle_Name__c = null;
                    opps.Bureau_Co_Applicant_Full_Last__c = null;
                    opps.Bureau_Co_Applicant_SSN__c=null;
                    opps.Bureau_Co_Applicant_DoB__c = null;
                    opps.APN__c = null;
                    opps.Legal_Description__c=null; 
                    opps.Trust_Indicator__c = null;
                    opps.County__c=null;
                    
                    oppstoUpdate.add(opps);
                    */
                }
            //}
            if(!OppLst.isEmpty())
                oppstoUpdate = updateDesyncFields(OppLst,false);
            
            if(!oppstoUpdate.isEmpty()){
                system.debug('### oppstoUpdate '+ oppstoUpdate);
                update oppstoUpdate;
            }
                
            system.debug('### Exit from DesyncCreditFields() of '+ SyncDesyncUtilities.class);
        }catch(Exception err){
                
            system.debug('### SyncDesyncUtilities.DesyncCreditFields():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
           ErrorLogUtility.writeLog('SyncDesyncUtilities.DesyncCreditFields()',err.getLineNumber(),'DesyncCreditFields()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));         
        }
    } 
    
    public static List<Opportunity> updateDesyncFields(List<Opportunity> optys,Boolean isQuoteDesync)
    {
        system.debug('### Entered into updateDesyncFields() of '+ SyncDesyncUtilities.class);
        try
        {
            string STR_BOOLEAN = 'BOOLEAN';
            string STR_PICKLIST = 'PICKLIST';
            string STR_STRING ='STRING';
            string STR_INTEGER ='INTEGER';
            string STR_DECIMAL ='DECIMAL';
            string STR_DATE ='DATE';

            Map<String, Schema.SObjectField> fieldMap = new Map<String, Schema.SObjectField>();
            fieldMap = Schema.SObjectType.Opportunity.fields.getMap();
            List<Desync_Mapping_Field__mdt> desyncList = new List<Desync_Mapping_Field__mdt>();
            List<Opportunity> oppList = new List<Opportunity>();
            
            if(isQuoteDesync)
                desyncList = [SELECT Field_API_Name__c,Field_Value__c FROM Desync_Mapping_Field__mdt WHERE Is_Quote_Desync__c=true];
            else
                desyncList = [SELECT Field_API_Name__c,Field_Value__c FROM Desync_Mapping_Field__mdt WHERE Is_Credit_Desync__c=true];
            
            for(Opportunity opps :optys){
                for(Desync_Mapping_Field__mdt objDesyncFld :desyncList){
                    if(objDesyncFld.Field_Value__c == 'null' || objDesyncFld.Field_Value__c == ''){
                        opps.put(objDesyncFld.Field_API_Name__c,null );
                    }else {
                        Schema.SObjectField fieldApiName = fieldMap.get(objDesyncFld.Field_API_Name__c);
                        Schema.DisplayType fldDataType = fieldApiName.getDescribe().getType();
                        system.debug('#### fieldApiName-->:'+fieldApiName+'--###fldDataType-->:'+fldDataType);
                        string fldType = string.valueOf(fldDataType); 

                        if(STR_PICKLIST.equalsIgnoreCase(fldType)){
                            opps.put(objDesyncFld.Field_API_Name__c,objDesyncFld.Field_Value__c); 
                        }else if(STR_BOOLEAN.equalsIgnoreCase(fldType)){
                             opps.put(objDesyncFld.Field_API_Name__c,BOOLEAN.valueOf(objDesyncFld.Field_Value__c));
                        }else if(STR_STRING.equalsIgnoreCase(fldType)){
                              opps.put(objDesyncFld.Field_API_Name__c,STRING.valueOf(objDesyncFld.Field_Value__c));
                        }else if(STR_INTEGER.equalsIgnoreCase(fldType)){
                              opps.put(objDesyncFld.Field_API_Name__c,Integer.valueOf(objDesyncFld.Field_Value__c));
                        }else if(STR_DECIMAL.equalsIgnoreCase(fldType)){
                              opps.put(objDesyncFld.Field_API_Name__c,Decimal.valueOf(objDesyncFld.Field_Value__c));
                        }else if(STR_DATE.equalsIgnoreCase(fldType)){
                              opps.put(objDesyncFld.Field_API_Name__c,Date.valueOf(objDesyncFld.Field_Value__c));
                        }
                        
                    }
                }
                system.debug('--###opps-->:'+opps);
                oppList.add(opps);
                
            }
            
            if(!oppList.isEmpty())
                return oppList;
            else
                return null;

            
        }catch(Exception ex)
        {
            system.debug('### SyncDesyncUtilities.updateDesyncFields():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SyncDesyncUtilities.updateDesyncFields()',ex.getLineNumber(),'updateDesyncFields()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from updateDesyncFields() of '+ SyncDesyncUtilities.class);
        return null;
    }
}