@isTest
private class DataValidator_Test 
{
    
    @isTest static void test_1 () 
    {
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};

        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999), 
            'Test'));

        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1!',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999), 
            'Test'));

        iolist.add (new DataValidator.InputObject (
            new Account(Name='12345678901234567890123456789012345678901234567890',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999), 
            'Test'));

        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = NULL,
                        Type='Partner',
                        Industry='Education',
                        Facility_Membership_Fee__c = 999), 
            'Test'));

        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='PartnerX', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999), 
            'Test'));

        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 1999), 
            'Test'));

        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999), 
            'Test-X'));

        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education-X',
                        Facility_Membership_Fee__c = 999), 
            'Test'));
        //InputObject 
        iolist.add (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education-X',
                        Facility_Membership_Fee__c = 999)));
        
        DataValidator.Input inp = new DataValidator.Input (iolist);

        DataValidator.validateObjects(inp);

        DataValidator.validateObject (new Account(Name='Test Account 1!',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999));

        DataValidator.validateObject (new DataValidator.InputObject (
            new Account(Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999), 
            'Test'));

        DataValidator.validateObjects (new list<Account>{new Account(Name='Test Account 1!',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Facility_Membership_Fee__c = 999)});

        DataValidator.Output op1 = 
                        DataValidator.validateObjects (iolist);

        Account a1 = new Account (Name='Test Account 1',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education-X',
                        Facility_Membership_Fee__c = 999);

        Opportunity o1 = new Opportunity (Name = 'Test Opportunity 1');

        Opportunity o2 = new Opportunity (Name = 'Test Opportunity 1!!!');
        //Object_Validation__mdt objValidation = new Object_Validation__mdt(MasterLabel = 'Account_FNI_Credit',Object_Type__c = 'Account',Variant__c = 'FNI Credit');
        //Field_Validation__mdt fieldValidate = new Field_Validation__mdt(MasterLabel = 'Account_FNI_Credit_BillingCity',Type__c = 'Picklist');
        System.debug( DataValidator.validateObjects (new list<SObject>{a1, o1, o2}));
    }   

//  ************************************************************

    @isTest static void test_2 () 
    {
        // this tests a set of invalid fields on the Account object

        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};

        iolist.add (new DataValidator.InputObject (
            new Account(Name = 'invalid account name!',
                        LastName = 'invalid account lastname!',
                        MiddleName = '9',
                        FirstName = 'Liz-Marie',
                        BillingStateCode = 'XX',
                        BillingStreet = 'invalid @ street',
                        BillingPostalCode = 'XXXXX',
                        PersonEmail = 'invalid email',
                        Length_of_Employment_Months__c = 123,
                        Phone = 'invalid phone',
                        SSN__c = 'invalid ssn',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Employer_Name__c = 'invalid # employer',
                        Job_Title__c = 'Invalid Job Title "',
                        Annual_Income__c = 1111111111), 
            'Test'));

        DataValidator.Input inp = new DataValidator.Input (iolist);

        DataValidator.Output out1 = DataValidator.validateObjects(inp);
        
        system.debug('out1 - '+out1);

        map<String,String> errorsMap = out1.outputObjects[0].fieldErrorsMap;
        
        system.debug('errorsMap - '+errorsMap.keyset().contains ('Name'));
        
        System.assertEquals (false, errorsMap.keyset().contains ('Name'));
        System.assertEquals (false, errorsMap.keyset().contains ('LastName'));
        System.assertEquals (false, errorsMap.keyset().contains ('MiddleName'));
        System.assertEquals (false, errorsMap.keyset().contains ('FirstName'));
        System.assertEquals (false, errorsMap.keyset().contains ('BillingStateCode'));
        System.assertEquals (false, errorsMap.keyset().contains ('BillingStreet'));
        System.assertEquals (false, errorsMap.keyset().contains ('BillingPostalCode'));
        System.assertEquals (false, errorsMap.keyset().contains ('PersonEmail'));
        System.assertEquals (false, errorsMap.keyset().contains ('Length_of_Employment_Months__c'));
        System.assertEquals (false, errorsMap.keyset().contains ('Phone'));
        System.assertEquals (false, errorsMap.keyset().contains ('SSN__c'));
        System.assertEquals (false, errorsMap.keyset().contains ('Annual_Income__c'));
        System.assertEquals (false, errorsMap.keyset().contains ('Employer_Name__c'));
        System.assertEquals (false, errorsMap.keyset().contains ('Job_Title__c'));

        System.debug('ALANDEBUGFINAL: ' + out1);
    }

//  ************************************************************

    @isTest static void test_3 () 
    {
        // this tests a set of valid fields on the Account object

        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};

        iolist.add (new DataValidator.InputObject (
            new Account(Name = 'good account namé',
                        LastName = 'good account lastname',
                        MiddleName = 'X',
                        FirstName = 'José',
                        BillingStateCode = 'DC',
                        BillingStreet = 'good street',
                        BillingPostalCode = '12345',
                        PersonEmail = 'goodemail@good.com',
                        Length_of_Employment_Months__c = 12,
                        Phone = '1231231234',
                        SSN__c = '111223333',
                        Current_Loan_Offering__c = 'Other',
                        Type='Partner', 
                        Industry='Education',
                        Employer_Name__c = 'Good émployer',
                        Job_Title__c = 'Good Job Titlé @$',
                        Annual_Income__c = 111111111), 
            'Test'));

        DataValidator.Input inp = new DataValidator.Input (iolist);

        DataValidator.Output out1 = DataValidator.validateObjects(inp);

        System.debug('ALANDEBUGFINAL: ' + out1);

        //System.assertEquals (false, out1.errors);
    }

//  ************************************************************

    @isTest static void test_4 () 
    {
        // this tests Date validation

        Date goodDate = Date.newInstance (2001,01,01);

        System.assertEquals(goodDate, DataValidator.validateDate('2001-01-01'));
        System.assertEquals(null, DataValidator.validateDate('2001-13-01'));
        System.assertEquals(null, DataValidator.validateDate('2001-01-32'));
        System.assertEquals(null, DataValidator.validateDate('2001-00-01'));
        System.assertEquals(null, DataValidator.validateDate('2001-00-01'));
        System.assertEquals(null, DataValidator.validateDate('01-01-01'));
        System.assertEquals(null, DataValidator.validateDate('01-01-01  '));
        System.assertEquals(null, DataValidator.validateDate('200x-01-01'));
        System.assertEquals(null, DataValidator.validateDate('2001-0x-01'));
        System.assertEquals(null, DataValidator.validateDate('2001-01-0x'));
        System.assertEquals(null, DataValidator.validateDate('xx'));
        System.assertEquals(null, DataValidator.validateDate('01/01/2001'));
    }
    
}