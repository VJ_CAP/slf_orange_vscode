/**
* Description: This is a wrapper class for Underwriting Queue Lightning page. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Raviteja Sajja          11/26/2019          Created 
******************************************************************************************/
public class UnderwritingQueueDataWrapper implements Comparable{

    //SLFOps Queue attributes Start
    @AuraEnabled public String age;
    @AuraEnabled public Decimal agenum;
    @AuraEnabled public String reviewType;
    @AuraEnabled public String underwritingID;
    @AuraEnabled public String facility;
    @AuraEnabled public String accountName;
    @AuraEnabled public String installerName;
    @AuraEnabled public String internalComments;
    @AuraEnabled public Datetime approvalReqDate;
    @AuraEnabled public Boolean isEdwOriginated;
    //SLFOps Queue attributes End
    
    //M0Stip Queue attributes Start
    //@AuraEnabled public Decimal age;
    //@AuraEnabled public String underwritingID;
    //@AuraEnabled public String accountName;
    @AuraEnabled public String stipDataNames;
    @AuraEnabled public String createdBy;
    @AuraEnabled public String projectType;
    //@AuraEnabled public Boolean isEdwOriginated;
    @AuraEnabled public Datetime m0ApprovalReqDate;
    //M0Stip Queue attributes End
    
    //PostNTPStip Queue attributes Start
    //@AuraEnabled public Decimal age;
    //@AuraEnabled public String underwritingID;
    //@AuraEnabled public String accountName;
    @AuraEnabled public String stipDataName;
    @AuraEnabled public String issueCode;
    @AuraEnabled public String category;
    //@AuraEnabled public String createdBy;
    //@AuraEnabled public String projectType;
    //@AuraEnabled public Datetime m0approvalReqDate;
    //@AuraEnabled public Boolean isEdwOriginated;
    //PostNTPStip Queue attributes End
    
    //HIFunding Queue attributes Start
    //@AuraEnabled public Decimal age;
    //@AuraEnabled public String facility;
    //@AuraEnabled public String accountName;
    @AuraEnabled public String coAppAccountName;
    @AuraEnabled public String installer;
    @AuraEnabled public String recordId;
    @AuraEnabled public String optyId;
    //HIFunding Queue attributes End
    
    public enum SORT_BY {
        Byagenum,ByReqDate,Byage
    }
    
    //Constructor for SLFOps Queue Start
    public UnderwritingQueueDataWrapper(String age1,Decimal agenum1, String reviewType1, String underwritingID1, String facility1,String accountName1,String installerName1,String internalComments1,Datetime approvalReqDate1, Boolean isEdwOriginated1){
        age = age1;
        agenum = agenum1;
        reviewType = reviewType1;
        underwritingID = underwritingID1;
        facility = facility1;
        accountName = accountName1;
        installerName = installerName1;            
        internalComments = internalComments1;  
        approvalReqDate = approvalReqDate1;  
        isEdwOriginated = isEdwOriginated1;
    }
    //Constructor for SLFOps Queue End
    
    //Constructor for M0Stip Queue Start
    public UnderwritingQueueDataWrapper(Decimal age1, String underwritingID1, String accountName1, String stipDataNames1, String createdBy1, String projectType1, Boolean isEdwOriginated1, Datetime m0ApprovalReqDate1)
    {
        agenum = age1;
        underwritingID = underwritingID1;
        accountName = accountName1;
        stipDataNames = stipDataNames1;
        createdBy = createdBy1;
        projectType = projectType1;
        isEdwOriginated = isEdwOriginated1;
        m0ApprovalReqDate = m0ApprovalReqDate1;
    }
    //Constructor for M0Stip Queue End
    
    //Constructor for PostNTPStip Queue Start
    public UnderwritingQueueDataWrapper(Decimal age1, String underwritingID1, String accountName1, String stipDataName1, String issueCode1, String category1, String createdBy1, String projectType1, Datetime m0approvalReqDate1, Boolean isEdwOriginated1)
    {
        agenum = age1;
        underwritingID = underwritingID1;
        accountName = accountName1;
        stipDataName = stipDataName1;            
        issueCode = issueCode1;  
        category = category1;  
        createdBy = createdBy1;
        projectType = projectType1;
        m0approvalReqDate = m0approvalReqDate1;
        isEdwOriginated = isEdwOriginated1;
    }
    //Constructor for PostNTPStip Queue End
    
    //Constructor for HIFunding Queue Start
    public UnderwritingQueueDataWrapper(Decimal age1, String facility1, String accountName1, String coAppAccountName1, String installer1, String recordId1, String optyId1){
        agenum = age1;
        facility = facility1;
        accountName = accountName1;
        coAppAccountName = coAppAccountName1;
        installer = installer1;
        recordId = recordId1;
        optyId = optyId1;
    }
    //Constructor for HIFunding Queue End

    public static SORT_BY sortBy = SORT_BY.Byagenum,ByReqDate,Byage;
    
    public Integer compareTo(Object objToCompare) {
        UnderwritingQueueDataWrapper uwQueueData = (UnderwritingQueueDataWrapper)(objToCompare);
        if (this.agenum > uwQueueData.agenum){
            return 1;
        }

        if (this.agenum == uwQueueData.agenum){
            return 0;
        }

        return -1;
    }

    //Search Queue attributes Start
    @AuraEnabled public List<SearchQueueData> SearchQueueDataList{get; set;}
    
    public class SearchQueueData{
        @AuraEnabled public String accName;
        @AuraEnabled public String uwId;
        @AuraEnabled public String optyId;
        @AuraEnabled public String installerName;
        @AuraEnabled public String instalStreet;
        @AuraEnabled public String stage;
        @AuraEnabled public Boolean isEDWOriginated;
        @AuraEnabled public String projectCategory;
    }
    //Search Queue attributes End

}