/******************************************************************************************
* Description: Test class to cover all REST services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma K         06/27/2017          Created
******************************************************************************************/
@isTest
public class SLFUtilityDataSetup {    
    
public static void initData()
    {
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgDrawflag = new TriggerFlags__c();
        trgDrawflag.Name ='Draw_Requests__c';
        trgDrawflag.isActive__c =true;
        trgLst.add(trgDrawflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgAccflagDis = new TriggerFlags__c();
        trgAccflagDis.Name ='Disclosures__c';
        trgAccflagDis.isActive__c =true;
        trgLst.add(trgAccflagDis);
        
        TriggerFlags__c trgAccflagTsk = new TriggerFlags__c();
        trgAccflagTsk.Name ='Task';
        trgAccflagTsk.isActive__c =true;
        trgLst.add(trgAccflagTsk);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
         TriggerFlags__c trgAccflagBox = new TriggerFlags__c();
        trgAccflagBox.Name ='Box_Fields__c';
        trgAccflagBox.isActive__c =true;
        trgLst.add(trgAccflagBox);
        
        insert trgLst;
        
      /* List<SF_Category_Map__c> catList = new List<SF_Category_Map__c>();
        SF_Category_Map__c sfmap = new SF_Category_Map__c();
        sfmap.Name = 'Loan Agreement';
        sfmap.Folder_Name__c = 'Loan Agreements';
        sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
        sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
        sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'LT_Loan_Agreement_Received__c';
        catList.add(sfmap);
        
       SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                         Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                         TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
        catList.add(cm1);
        insert sfmap;
        */
          
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
       
               
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'Sunlight',Email='testc@mail.com',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        system.debug('@@ I am here' +portalUser.Contact.AccountId);
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        //update acc;
        
        
        AccountShare accShare = new AccountShare(); //a new empty AccountShare object
        accShare.userorgroupid = portalUser.Id;
        accShare.accountid = con.accountid;
        accShare.accountaccesslevel = 'Edit';
        accShare.OpportunityAccessLevel = 'None';
        accShare.CaseAccessLevel = 'None';
        insert accShare;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact personcon = new Contact(FirstName = 'Sunlight',Email='testc@mail.com',LastName ='Financial',AccountId = personAcc.Id);
        insert personcon; 
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        personAcc.PersonBirthdate = Date.ValueOf('1929-03-10');
        update personAcc;
        
        AccountShare personAccShare = new AccountShare(); //a new empty AccountShare object
        personAccShare.userorgroupid = portalUser.Id;
        personAccShare.accountid = personAcc.Id;
        personAccShare.accountaccesslevel = 'Edit';
        personAccShare.OpportunityAccessLevel = 'None';
        personAccShare.CaseAccessLevel = 'None';
        insert personAccShare;
        
        //Added by Suresh Kumar to cover Orange-1606 
        List<Draw_Allocation__c> lstDrawAllocInst = new List<Draw_Allocation__c>();
        Draw_Allocation__c drawAlloc = new Draw_Allocation__c();
        drawAlloc.Account__c = acc.id;
        drawAlloc.Level__c = '1';
        drawAlloc.Draw_Allocation__c = 50;
        lstDrawAllocInst.add(drawAlloc);
        
        Draw_Allocation__c drawAlloc1 = new Draw_Allocation__c();
        drawAlloc1.Account__c = acc.id;
        drawAlloc1.Level__c = '2';
        drawAlloc1.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc1);
        
        Draw_Allocation__c drawAlloc2 = new Draw_Allocation__c();
        drawAlloc2.Account__c = acc.id;
        drawAlloc2.Level__c = '3';
        drawAlloc2.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc2);
        
        insert lstDrawAllocInst;
        //End
        
        System.runAs (portalUser)
        { 
            Document doc1 = new Document();
            doc1.Body = Blob.valueOf('Some Text');
            doc1.ContentType = 'application/pdf';
            doc1.DeveloperName = 'my_document';
            doc1.IsPublic = true;
            doc1.Name = 'Sunlight Logo';
            doc1.FolderId = [select id from folder where name = 'Shared Documents'].id;
            insert doc1;
             
            //Insert SLF Product Record
            List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            //insert prod;
            prodList.add(prod);
            
            //Inserting duplicate SLF Product Records
            Product__c prod1 = new Product__c();
            prod1.Installer_Account__c =acc.id;
            prod1.Long_Term_Facility_Lookup__c =acc.id;
            prod1.Name='testprod';
            prod1.FNI_Min_Response_Code__c=9;
            prod1.Product_Display_Name__c='test';
            prod1.Qualification_Message__c ='testmsg';
            prod1.Product_Loan_Type__c='Solar';
            prod1.State_Code__c ='CA';
            prod1.ST_APR__c = 2.99;
            prod1.Long_Term_Facility__c = 'TCU';
            prod1.APR__c = 2.99;
            prod1.ACH__c = true;
            prod1.Internal_Use_Only__c = true;
            prod1.LT_Max_Loan_Amount__c =1000;
            prod1.Term_mo__c = 144;
            prod1.Product_Tier__c = '0';
            //insert prod1;
            prodList.add(prod1);
            
            //Inserting duplicate SLF Product Records
            Product__c prod2 = new Product__c();
            prod2.Installer_Account__c =acc.id;
            prod2.Long_Term_Facility_Lookup__c =acc.id;
            prod2.Name='testprod';
            prod2.FNI_Min_Response_Code__c=9;
            prod2.Product_Display_Name__c='test';
            prod2.Product_Loan_Type__c='Battery Only';
            prod2.Qualification_Message__c ='testmsg';
            prod2.State_Code__c ='CA';
            prod2.ST_APR__c = 2.99;
            prod2.Long_Term_Facility__c = 'TCU';
            prod2.APR__c = 2.99;
            prod2.ACH__c = true;
            prod2.Internal_Use_Only__c = true;
            prod2.LT_Max_Loan_Amount__c =1000;
            prod2.Term_mo__c = 144;
            prod2.Product_Tier__c = '0';
            //insert prod2;
            prodList.add(prod2);
            
            insert prodList;
            
            //Insert SLF Product Record
            List<Product_Proxy__c> prdctPrxyLst = new List<Product_Proxy__c>();
            Product_Proxy__c prodProxy = new Product_Proxy__c();
            prodProxy.ACH__c = true;
            prodProxy.APR__c = 2.99;
            prodProxy.Installer__c =acc.id;
            prodProxy.Internal_Use_Only__c = false;
            prodProxy.Is_Active__c = true;
            prodProxy.SLF_ProductID__c = prod.Id;
            prodProxy.Name = 'testprod';
            prodProxy.State_Code__c ='CA';
            prodProxy.Term_mo__c = 120;
            //insert prodProxy;
            prdctPrxyLst.add(prodProxy);
            
            Product_Proxy__c prodProxy1 = new Product_Proxy__c();
            prodProxy1.ACH__c = true;
            prodProxy1.APR__c = 2.99;
            prodProxy1.Installer__c =acc.id;
            prodProxy1.Internal_Use_Only__c = false;
            prodProxy1.Is_Active__c = true;
            prodProxy1.SLF_ProductID__c = prod1.Id;
            prodProxy1.Name = 'testprod';
            prodProxy1.State_Code__c ='CA';
            prodProxy1.Term_mo__c = 144;
            //insert prodProxy1;
            prdctPrxyLst.add(prodProxy1);
            
            Product_Proxy__c prodProxy2 = new Product_Proxy__c();
            prodProxy2.ACH__c = true;
            prodProxy2.APR__c = 2.99;
            prodProxy2.Installer__c =acc.id;
            prodProxy2.Internal_Use_Only__c = false;
            prodProxy2.Is_Active__c = true;
            prodProxy2.SLF_ProductID__c = prod2.Id;
            prodProxy2.Name = 'testprod';
            prodProxy2.State_Code__c ='CA';
            prodProxy2.Term_mo__c = 144;
            //insert prodProxy2;
            prdctPrxyLst.add(prodProxy2);
            
            Product_Proxy__c prodProxy3 = new Product_Proxy__c();
            prodProxy3.ACH__c = true;
            prodProxy3.APR__c = 5.99;
            prodProxy3.Installer__c =acc.id;
            prodProxy3.Internal_Use_Only__c = false;
            prodProxy3.Is_Active__c = true;
            prodProxy3.SLF_ProductID__c = prod2.Id;
            prodProxy3.Name = 'testprod';
            prodProxy3.State_Code__c ='NY';
            prodProxy3.Term_mo__c = 120;
            //insert prodProxy3;
            prdctPrxyLst.add(prodProxy3);
            
            insert prdctPrxyLst;
            
            test.starttest();
            
            //Insert opportunity record
            List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            //insert personOpp;
            oppList.add(personOpp);
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            //insert opp;
            oppList.add(opp);
            
            insert oppList;
            
            //data for StipulationStatusBatch class test.
            
            Underwriting_File__c undStpbt = new Underwriting_File__c();           
            undStpbt.Opportunity__c = opp.id;
            undStpbt.Approved_for_Payments__c = true;
            insert undStpbt;
            
            
            Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', Email_Text__c = 'Test data', POS_Message__c = 'test', Review_Classification__c = 'SLS II');
            insert sd;
            
            Stipulation__c stp = new Stipulation__c();
            stp.Status__c ='New';
            stp.Stipulation_Data__c = sd.id;
            
            stp.Underwriting__c =undStpbt.id;
            insert stp;
            
            //data end
            
            personOpp.Opportunity__c = opp.Id;
            update personOpp;
            
            //Insert opportunity record
            Opportunity oppSales = new Opportunity();
            oppsales.name = 'OppNew';
            oppsales.CloseDate = system.today();
            oppsales.StageName = 'New';
            oppsales.AccountId = acc.id;
            oppsales.Installer_Account__c = acc.id;
            oppsales.Install_State_Code__c = 'CA';
            oppsales.Install_Street__c ='Street';
            oppsales.Install_City__c ='InCity';
            oppsales.Combined_Loan_Amount__c = 10000;
            oppsales.Partner_Foreign_Key__c = 'TCU||1234';
            //opp.Approved_LT_Facility__c = 'TCU';
            oppsales.SLF_Product__c = prod.id;
            oppsales.Opportunity__c = personOpp.Id;
            oppsales.Co_Applicant__c = acc.id;
            insert oppsales;
            
            List<Equipment_Manufacturer__c> eqList = new List<Equipment_Manufacturer__c>();
            Equipment_Manufacturer__c equipRec1 = new Equipment_Manufacturer__c();
            equipRec1.End_Date__c = system.today()-1;
            equipRec1.Activation_Date__c =system.today()-6;
            equipRec1.Manufacturer_Type__c ='Mounting';
            equipRec1.Manufacturer__c ='Unirac';
            //insert equipRec1;
            eqList.add(equipRec1);
            
            Equipment_Manufacturer__c equipRec2 = new Equipment_Manufacturer__c();
            equipRec2.Activation_Date__c =system.today();
            equipRec2.End_Date__c =system.today()+6;
            equipRec2.Manufacturer_Type__c ='Mounting';
            equipRec2.Manufacturer__c ='Unirac';
            //insert equipRec2;
            eqList.add(equipRec2);
            
            insert eqList;
        
            //Equipment_Manufacturer__c equipRec =[select id,Is_Active__c from Equipment_Manufacturer__c where id =:equipRec2.id];
            //system.debug('****equiplst***'+equipRec.Is_Active__c);
            
            
            //system.debug('****equiplst***'+equipRec.Is_Active__c);
            system.debug('****equiplst1***'+equipRec1.Is_Active__c);
        
              
            //Insert System Design Record
            System_Design__c sysDesignRec = new System_Design__c();
            //sysDesignRec.Name = 'Test Sys';
            sysDesignRec.System_Cost__c = 11;
            sysDesignRec.Opportunity__c = personOpp.Id;
            sysDesignRec.Est_Annual_Production_kWh__c = 10;
            sysDesignRec.Inverter_Count__c = 11;
            //  sysDesignRec.Inverter_Make__c = '11';
            sysDesignRec.Inverter_Model__c = '12';
            //sysDesignRec.Inverter__c = 'TBD';
            sysDesignRec.Module_Count__c = 10;
            sysDesignRec.Module_Model__c = '11';
            //sysDesignRec.System_Size_STC_kW__c = 12;
            insert sysDesignRec;
            
            //Insert Quote Record
            List<Quote> quoteList = new List<Quote>();
            Quote quoteRec = new Quote();
            quoteRec.Name ='Test quote';
            quoteRec.Opportunityid = oppsales.id;//opp.Id
            quoteRec.SLF_Product__c = prod.Id ;
            quoteRec.System_Design__c= sysDesignRec.id;
            //quoteRec.Accountid =acc.id;
            //insert quoteRec; 
            quoteList.add(quoteRec); 
            
            Quote quoteRec1 = new Quote();
            quoteRec1.Name ='Test quote';
            quoteRec1.Opportunityid = opp.Id;
            quoteRec1.SLF_Product__c = prod.Id ;
            quoteRec1.System_Design__c= sysDesignRec.id;
            //quoteRec.Accountid =acc.id;
            //insert quoteRec; 
            quoteList.add(quoteRec1);            
                       
            
            Quote quoteRec2 = new Quote();
            quoteRec2.Name ='Test quote';
            quoteRec2.Opportunityid = personOpp.id;
            quoteRec2.SLF_Product__c = prod.Id ;
            quoteRec2.System_Design__c= sysDesignRec.id;
            //insert quoteRec2; 
            quoteList.add(quoteRec2);
            insert quoteList;
            
            //Insert Credit Record
            List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.id;
            credit.Primary_Applicant__c = acc.id;
            credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = prod.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'New';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            //insert credit;
            creditList.add(credit);
            
            //Insert Credit Record
            SLF_Credit__c credit2 = new SLF_Credit__c();
            credit2.Opportunity__c = personOpp.id;
            credit2.Primary_Applicant__c = acc.id;
            credit2.Co_Applicant__c = acc.id;
            credit2.SLF_Product__c = prod.Id ;
            credit2.Total_Loan_Amount__c = 100;
            credit2.Status__c = 'New';
            credit2.LT_FNI_Reference_Number__c = '123';
            credit2.Application_ID__c = '123';
            //insert credit2;
            creditList.add(credit2);
            
            //Insert Credit Record
            SLF_Credit__c salescredit = new SLF_Credit__c();
            salescredit.Opportunity__c = oppsales.id;
            salescredit.Primary_Applicant__c = acc.id;
            salescredit.Co_Applicant__c = acc.id;
            salescredit.SLF_Product__c = prod.Id ;
            salescredit.Total_Loan_Amount__c = 100;
            salescredit.Status__c = 'New';
            salescredit.LT_FNI_Reference_Number__c = '123';
            salescredit.Application_ID__c = '123';
            //insert salescredit;
            creditList.add(salescredit);
            
            insert creditList;

            opp.Synced_Credit_Id__c = credit.Id;
            update opp;
            system.debug('*****opp in test setup*******'+opp.Synced_Credit_Id__c);
            
            
            //Insert Default Product Record
            Default_Product__c defautProd = new Default_Product__c();
            defautProd.Account__c = acc.id;
            defautProd.SLF_Product__c = prod.id;
            insert defautProd;
            
            //Insert Proxy Product Record
            Product_Proxy__c proxprod = new Product_Proxy__c();
            proxprod.Installer__c = acc.id;
            proxprod.State_Code__c = 'CA';
            proxprod.Term_mo__c = 120;
            proxprod.Is_Active__c = true;               
            proxprod.ACH__c = true;
            proxprod.APR__c = 2.99 ;
            insert proxprod;
            
            SFSettings__c slfencrypt = new SFSettings__c();
            slfencrypt.Encrypt_Private_Key__c = 'SLFOrange';
            slfencrypt.Name = 'PrivateKey';
            insert slfencrypt;
            
            New_York_City_Zip_Codes__c newyorkzcode = new New_York_City_Zip_Codes__c();
            newyorkzcode.County__c = 'New York';
            newyorkzcode.Name = '10000';
            insert newyorkzcode;
            
            List<ExceptionList__c> expLst = new List<ExceptionList__c>();
            ExceptionList__c excobj = new ExceptionList__c();
            excobj.API_Name__c = 'Opportunity__c';
            excobj.Object_Name__c = 'Opportunity';
            excobj.Name = 'Opportunity__c';
            expLst.add(excobj);
            
            ExceptionList__c excobj1 = new ExceptionList__c();
            excobj1.API_Name__c = 'Partner_Foreign_Key__c';
            excobj1.Object_Name__c = 'Opportunity';
            excobj1.Name = 'Partner_Foreign_Key__c';
            expLst.add(excobj1);
            
            ExceptionList__c excobj2 = new ExceptionList__c();
            excobj2.API_Name__c = 'Install_Contract_System_Cost__c';
            excobj2.Object_Name__c = 'Underwriting_File__c';
            excobj2.Name = 'Install Contract System Cost';
            expLst.add(excobj2);
            
            ExceptionList__c excobj3 = new ExceptionList__c();
            excobj3.API_Name__c = 'LT_Amount_Financed__c';
            excobj3.Object_Name__c = 'Underwriting_File__c';
            excobj3.Name = 'LT Amount Financed';
            expLst.add(excobj3);
            insert expLst;
            
            //insert crdLst;
            //Insert Custom Endpoints Custom Setting Record
            List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
            SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
            endpoint1.Name = 'Pricing';
            endpoint1.Endpoint_URL__c =  'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
            endpoint.add(endpoint1);
            SLF_Boomi_API_Details__c endpoint2 = new SLF_Boomi_API_Details__c();
            endpoint2.Name = 'BoomiToAWSPricing';
            endpoint2.Endpoint_URL__c =  'https://test.connect.boomi.com/ws/rest/v1/dev/pricing/callaws/';
            endpoint.add(endpoint2);
            
            SLF_Boomi_API_Details__c endpoint3 = new SLF_Boomi_API_Details__c();
            endpoint3.Name = 'FNI QA';
            endpoint3.Username__c =  'salesforceuser';
            endpoint3.Password__c = 'RC8N1CreKnz4rm2kMTCj';
            endpoint.add(endpoint3);
            
            SLF_Boomi_API_Details__c endpoint4 = new SLF_Boomi_API_Details__c();
            endpoint4.Name = 'FNI Prod';
            endpoint4.Username__c =  'salesforceuser';
            endpoint4.Password__c = 'AmtPHqSlK9sB6NXjSyq4';
            endpoint.add(endpoint4);
            
            
            SLF_Boomi_API_Details__c apidetails = new SLF_Boomi_API_Details__c();
            apidetails.Name ='Boomi QA';
            apidetails.Username__c ='sunlightfinancial-WSHA3Z.N29SCE';
            apidetails.Password__c ='2458a5de-d689-4708-a4fb-d3594c067610';
            apidetails.Endpoint_URL__c ='https://test.connect.boomi.com/ws/soap12/prequal';
            apidetails.viaBoomi__c =true;
            endpoint.add(apidetails);
            insert endpoint;
                    
            system.debug('***********'+prod.ST_APR__c);
            system.debug('****Long_Term_Facility__c*******'+prod.Long_Term_Facility__c);
            List<Underwriting_File__c> underWrFilInsertLst = new List<Underwriting_File__c>();
            Map<String, String> apprmilMap = new Map<String, String>();
            String apprmilString;
        
            
            
            /*for(Integer i=0; i<3; i++) 
            {
                Underwriting_File__c underWrFilRec = new Underwriting_File__c();
                underWrFilRec.name = 'Underwriting File'+i;
                underWrFilRec.ACT_Review__c ='Yes';
                underWrFilRec.Opportunity__c = personOpp.Id;
                underWrFilRec.Approved_for_Payments__c = true;
                if(i == 1)
                    underWrFilRec.Opportunity__c = oppsales.Id;

                underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
                underWrFilRec.Title_Review_Complete_Date__c = system.today();
                underWrFilRec.Install_Contract_Review_Complete__c = true;
                underWrFilRec.Utility_Bill_Review_Complete__c = true;
                underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
                underWrFilRec.Installation_Photos_Review_Complete__c = true;
                underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
                underWrFilRec.Final_Design_Document_Received__c = system.today();
                underWrFilRec.Final_Design_Review_Complete__c = true;   
                //underWrFilRec.PTO_Received__c = system.today();
                underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
                //added for testing
               // underWrFilRec.Box_Payment_Election_Form_TS__c = system.today();
              //  underWrFilRec.Box_Utility_Bills_TS__c = system.today();
              //  underWrFilRec.Box_Approval_Letters_TS__c = system.today();
                
                underWrFilRec.Box_Loan_Agreements_TS__c =  system.Today();
                underWrFilRec.Box_Communications_TS__c =  system.Today();
                underWrFilRec.Box_Government_IDs_TS__c =  system.Today();
                underWrFilRec.Box_Final_Designs_TS__c =  system.Today();
                underWrFilRec.Box_Install_Contracts_TS__c  =  system.Today();
                underWrFilRec.Box_Installation_Photos_TS__c  = system.Today();
                underWrFilRec.Box_Approval_Letters_TS__c  =  system.Today();
                underWrFilRec.Box_Utility_Bills_TS__c  =  system.Today();
                underWrFilRec.Box_Payment_Election_Form_TS__c  =  system.Today();   
                underWrFilInsertLst.add(underWrFilRec);
            }
            //insert underWrFilInsertLst;*/
            
        
           /*
            Map<Id,Underwriting_File__c> underwritingfileMap = new Map<Id,Underwriting_File__c>();  
            for(Integer i=0; i<6; i++) 
            {
                Underwriting_File__c underWrFilRec = underWrFilInsertLst[i];
                underWrFilRec.Box_Loan_Agreements_TS__c = null;
                underWrFilRec.Box_Communications_TS__c =  null;
                underWrFilRec.Box_Government_IDs_TS__c =  null;
                underWrFilRec.Box_Final_Designs_TS__c =  null;
                underWrFilRec.Box_Install_Contracts_TS__c  =  null;
                underWrFilRec.Box_Installation_Photos_TS__c  = null;
                underWrFilRec.Box_Approval_Letters_TS__c  = null;
                underWrFilRec.Box_Utility_Bills_TS__c  = null;
                underWrFilRec.Box_Payment_Election_Form_TS__c  =  null;
                underwritingfileMap.put(underWrFilRec.Id,underWrFilRec);
            }
            if(!underwritingfileMap.isEmpty())
                update underwritingfileMap.Values();
            */
            system.debug('====>underWrFilInsertLst'+underWrFilInsertLst);
            /*
            Stipulation_Data__c sData  = new Stipulation_Data__c();
            sData.Name = 'SD Test';
            insert sData;
            
            List<Stipulation__c> stipInsertLst = new List<Stipulation__c>();
            for(Integer i=0; i<5; i++) 
            {
                Stipulation__c stipTest  = new Stipulation__c();
                stipTest.Status__c = 'New';
                stipTest.Completed_Date__c = system.today();
                stipTest.Stipulation_Data__c  = sData.id;
                stipTest.Underwriting__c =underWrFilInsertLst[i].id;
                stipTest.ETC_Notes__c = 'test';
                stipInsertLst.add(stipTest);
            }
        
            insert stipInsertLst;
            */
            
        }   
    }
    
    /**
    * Description: used for Map Webservice URI for any Object
    */
    Public Static void MapWebServiceURI(Map<String, String> reqMap,string jsonString,string uriPeram){
        system.debug('*******reqMapstart****'+reqMap);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        system.debug('*******reqMap****'+reqMap);
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
            system.debug('#########req.requestBody###############'+jsonString);
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();

            for(String str : reqMap.Keyset()){
                gen.writeStringField(str,reqMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
            system.debug('#########req.requestBody###############'+gen.getAsString());
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
      //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}