@IsTest
public class jsonSystemResponse_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'  \"status_code\": 200,'+
		'  \"count\": 21,'+
		'  \"data\": {'+
		'    \"date_updated\": \"2016-06-22T21:59:18.243928+00:00\",'+
		'    \"consumption_offset\": 98.47437626432907,'+
		'    \"capacity\": 6.16,'+
		'    \"primary_module_mfr\": \"Silfab Solar\",'+
		'    \"owned_by_organization\": {'+
		'      \"link\": \"\",'+
		'      \"uuid\": \"e3043913-0b57-4f2c-8f7e-b8b367c0e925\",'+
		'      \"natural_id\": \"Vision Solar\"'+
		'    },'+
		'    \"modified_by\": {'+
		'      \"link\": \"\",'+
		'      \"uuid\": \"1b96a6cb-2c40-4fcd-9c6a-9cb3cf1f7e4d\",'+
		'      \"natural_id\": \"cseegmiller@visionsolar.com\"'+
		'    },'+
		'    \"uuid\": \"eceba58c-e3d3-4c26-989c-33d2e1c4f96a\",'+
		'    \"primary_inverter_mfr\": \"Enphase Energy\",'+
		'    \"n_modules\": 22,'+
		'    \"site\": {'+
		'      \"link\": \"/api/solar/quotegen/site/5d7ecb2c-5e95-4388-abf1-d74477efe02a\",'+
		'      \"uuid\": \"5d7ecb2c-5e95-4388-abf1-d74477efe02a\",'+
		'      \"natural_id\": \"1477 W Stone Brook Ln, Layton, UT\"'+
		'    },'+
		'    \"generation\": 11683,'+
		'    \"created_by\": {'+
		'      \"link\": \"\",'+
		'      \"uuid\": \"1b96a6cb-2c40-4fcd-9c6a-9cb3cf1f7e4d\",'+
		'      \"natural_id\": \"cseegmiller@visionsolar.com\"'+
		'    },'+
		'    \"date_created\": \"2016-06-22T21:57:13.926630+00:00\",'+
		'    \"productivity\": 1896.590909090909,'+
		'    \"name\": \"System 1\",'+
		'    \"monthly_generation\": {'+
		'      \"1\": 492,'+
		'      \"2\": 714,'+
		'      \"3\": 1021,'+
		'      \"4\": 1026,'+
		'      \"5\": 1356,'+
		'      \"6\": 1362,'+
		'      \"7\": 1383,'+
		'      \"8\": 1281,'+
		'      \"9\": 1153,'+
		'      \"10\": 790,'+
		'      \"11\": 759,'+
		'      \"12\": 346'+
		'    },'+
		'    \"owned_by_user\": {'+
		'      \"link\": \"\",'+
		'      \"uuid\": \"33267c1a-7b41-43f4-ad46-895232c2e57f\",'+
		'      \"natural_id\": \"amassey@visionsolar.com\"'+
		'    },'+
		'    \"arrays\": {'+
		'      \"link\": \"/api/solar/quotegen/array/?system_id=9332\"'+
		'    },'+
		'    \"utility_tier_offset\": {'+
		'      \"1\": 4809.783841572087,'+
		'      \"2\": 6196.428016746057,'+
		'      \"3\": 0'+
		'    },'+
		'    \"natural_id\": \"System 1\",'+
		'    \"utility_tier_usage\": {'+
		'      \"1\": -16.25944895152035,'+
		'      \"2\": 196.84983819183586'+
		'    }'+
		'  },'+
		'  \"db_table\": \"system_system\",'+
		'  \"messages\": {'+
		'    \"info\": ['+
		'      {'+
		'        \"msg_code\": \"GO3NKC0CBB\",'+
		'        \"timestamp\": \"2016-06-23 20:46:39.169309\",'+
		'        \"message\": \"Successfully fetched System instance(s)\"'+
		'      }'+
		'    ],'+
		'    \"critical\": [],'+
		'    \"error\": [],'+
		'    \"warning\": []'+
		'  }'+
		'}';
		jsonSystemResponse obj = jsonSystemResponse.parse(json);
		System.assert(obj != null);
	}
}