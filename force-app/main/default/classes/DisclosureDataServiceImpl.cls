/**
* Description: Disclosure logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
      Deepika             08/28/2018              Created
******************************************************************************/

public without sharing class DisclosureDataServiceImpl extends RestDataServiceBase{
   /*
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+DisclosureDataServiceImpl.class);
        System.debug('### stsDtObj:'+stsDtObj);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean respBean = new UnifiedBean(); 
        respBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        respBean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        Map<string,string> mapConFieldInfo = new Map<string,string>();
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        List<Opportunity> opprLst;
        String opportunityId = '';
        String externalId;
        String installerId;
 
        Savepoint sp = Database.setSavepoint();
        try
        { 
            system.debug('### reqData.projects[0] '+reqData.projects[0]);
            if(null != reqData.projects[0])
            {
                List<user> loggedInUsr = [select Id,contactid,contact.AccountId,
                                                         contact.Account.Default_FNI_Loan_Amount__c,
                                                         contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,
                                                         UserRole.Name,contact.Account.Eligible_for_Last_Four_SSN__c,
                                                         contact.Account.Credit_Waterfall__c,
                                                         contact.Account.Risk_Based_Pricing__c from User 
                                                         where Id =: userinfo.getUserId()];  
                if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
                {                 
                    errMsg = ErrorLogUtility.getErrorCodes('203', null);
                    respBean.returnCode = '203';
                    //return respBean;
                } 
                else if((String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
                {
                    errMsg = ErrorLogUtility.getErrorCodes('201',null);
                    respBean.returnCode = '201';
                } 
                
                if(string.isNotBlank(errMsg))
                {
                    Database.rollback(sp);
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    respBean.error.add(errorMsg);
                    iRestRes = (IRestResponse)respBean;
                    return iRestRes;
                }
                else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id)){
                    opprLst = new List<Opportunity>();
                    //Removed SLF_Product__r.ACH__c from query By Deepika as part of Orange - 3622
                    opprLst = [SELECT Id,Project_Category__c,EDW_Originated__c, ProductTier__c, StageName, ACH__c, Product_Loan_Type__c,Long_Term_APR__c,
                                      Install_State_Code__c,Installer_Account__c, Installer_Account__r.Risk_Based_Pricing__c,
                                      SLF_Product__c,SLF_Product__r.Term_mo__c,Co_Applicant__c,
                                      SLF_Product__r.APR__c, SLF_Product__r.NonACH_APR__c,SLF_Product__r.NonACH_OID__c,SLF_Product__r.NonACH_Promo_APR__c,
                                      SLF_Product__r.NonACH_Facility_Fee__c, SLF_Product__r.LT_OID__c,SLF_Product__r.Promo_APR__c,SLF_Product__r.LT_Facility_Compensation_Rate__c,
                                      Combined_Loan_Amount__c,AccountId,isACH__c, Install_Street__c,Install_City__c,Install_Postal_Code__c,
                                      Installer_Account__r.Prequal_Enabled__c,(select Id,Opportunity__c,
                                      Project_Status__c,Project_Status_Detail__c from Underwriting_Files__r) 
                                      FROM Opportunity WHERE Id = :reqData.projects[0].id 
                                      LIMIT 1];                    
                }
                else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
                {
                    if(reqData.projects[0].externalId.contains('||')){
                        respBean.returnCode = '219';
                        errMsg =ErrorLogUtility.getErrorCodes('219',null);
                    }
                       
                    if(!loggedInUsr.isEmpty())
                    {
                        if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                            externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                        }else{
                            iolist = new list<DataValidator.InputObject>{};
                            errMsg = ErrorLogUtility.getErrorCodes('400',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respBean.error.add(errorMsg);
                            respBean.returnCode = '400';
                            iRestRes = (IRestResponse)respBean;
                            return iRestRes;
                        }
                    }
                    if(String.isBlank(errMsg)){
                        opprLst = new List<Opportunity>();
                        //Removed SLF_Product__r.ACH__c from query By Deepika as part of Orange - 3622
                        opprLst = [select id, Project_Category__c,EDW_Originated__c, ProductTier__c, Partner_Foreign_Key__c, ACH__c, 
                                  Product_Loan_Type__c, Install_State_Code__c,StageName, Installer_Account__c, 
                                  Installer_Account__r.Risk_Based_Pricing__c,isACH__c,Long_Term_APR__c,
                                  SLF_Product__c,SLF_Product__r.Term_mo__c,SLF_Product__r.APR__c, SLF_Product__r.NonACH_APR__c,SLF_Product__r.NonACH_OID__c,SLF_Product__r.NonACH_Promo_APR__c,
                                  SLF_Product__r.NonACH_Facility_Fee__c, SLF_Product__r.LT_OID__c,SLF_Product__r.Promo_APR__c,SLF_Product__r.LT_Facility_Compensation_Rate__c,
                                  Combined_Loan_Amount__c,AccountId,Co_Applicant__c,
                                  Install_Street__c,Install_City__c,Install_Postal_Code__c,
                                  Installer_Account__r.Prequal_Enabled__c,
                                  (select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c from Underwriting_Files__r) 
                                  from opportunity where Partner_Foreign_Key__c=:externalId 
                                  limit 1];
                        
                    }
                }
                else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
                {
                    opprLst = new List<Opportunity>();
                    //Removed SLF_Product__r.ACH__c from query By Deepika as part of Orange - 3622
                    opprLst = [select id, Project_Category__c, EDW_Originated__c, ProductTier__c, Hash_Id__c,StageName, ACH__c, Product_Loan_Type__c,
                                      Install_State_Code__c, Installer_Account__c, Installer_Account__r.Risk_Based_Pricing__c,
                                      SLF_Product__r.APR__c, SLF_Product__r.NonACH_APR__c,SLF_Product__r.NonACH_OID__c,SLF_Product__r.NonACH_Promo_APR__c,
                                      SLF_Product__r.NonACH_Facility_Fee__c, SLF_Product__r.LT_OID__c,SLF_Product__r.Promo_APR__c,SLF_Product__r.LT_Facility_Compensation_Rate__c,
                                      SLF_Product__c,SLF_Product__r.Term_mo__c,Long_Term_APR__c,
                                      Combined_Loan_Amount__c,AccountId,Co_Applicant__c,isACH__c, 
                                      Install_Street__c,Install_City__c,Install_Postal_Code__c,
                                      Installer_Account__r.Prequal_Enabled__c,
                                      (select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c from Underwriting_Files__r) 
                                      from opportunity where Hash_Id__c=:reqData.projects[0].hashId 
                                      limit 1];
                }
                if (opprLst!=null && opprLst.size() > 0)
                {                            
                    opportunityId = opprLst.get(0).id;
                    installerId = opprLst.get(0).Installer_Account__c;
                }
                else {
                    respBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
                if(string.isNotBlank(errMsg))
                {
                    Database.rollback(sp);
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    respBean.error.add(errorMsg);
                    iRestRes = (IRestResponse)respBean;
                    return iRestRes;
                }
                if(!opprLst.isEmpty()){  
                    String disclsrStr = ' ';                    
                    if(opprLst[0].Installer_Account__r.Risk_Based_Pricing__c){                    
                        if(reqData.projects[0].disclosures != null && !reqData.projects[0].disclosures.isEmpty() && reqData.projects[0].disclosures[0] != null){
                            if(reqData.projects[0].disclosures[0].type == null || reqData.projects[0].disclosures[0].type == ''){
                                errMsg = ErrorLogUtility.getErrorCodes('207',null)+ ': type';
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                respBean.error.add(errorMsg);
                                respBean.returnCode = '207';
                                iRestRes = (IRestResponse)respBean;
                                return iRestRes;
                            }
                        
                            List<Disclosures__c> disclsrObj = new List<Disclosures__c>();
                            disclsrObj = [Select id, Type__c, Active__c,Disclosure_Text__c 
                                                 from Disclosures__c 
                                                 where Active__c = true and 
                                                 Type__c = :reqData.projects[0].disclosures[0].type];
                            System.debug('###disclsrObj:::'+disclsrObj);
                            
                            if(String.isBlank(installerId))
                            {
                                installerId = loggedInUsr[0].contact.AccountId;
                            }
                            
                            if(disclsrObj != null && disclsrObj.size()>0&& disclsrObj[0].Disclosure_Text__c != null && opprLst[0].SLF_Product__c != null)
                            {                           
                                List<Product__c> prdctList = new List<Product__c>();
                                if(reqData.projects[0].disclosures[0].type != 'RBP')//Added As part of ORANGE-1595
                                {   
                                    disclsrStr = disclsrObj[0].Disclosure_Text__c;
                                }
                                else
                                {
                                    // process only for RBP
                                    if(opprLst[0].Project_Category__c == 'Solar')
                                    {
                                        
                                        //for Solar deals only
                                        /*prdctList = [Select id, APR__c, Term_mo__c, Product_Tier__c 
                                                            from Product__c 
                                                            where Installer_Account__c =:installerId and 
                                                            State_Code__c = :opprLst[0].Install_State_Code__c and 
                                                            Is_Active__c = true and
                                                            ACH__c = :opprLst[0].SLF_Product__r.ACH__c and 
                                                            Product_Loan_Type__c = :opprLst[0].Product_Loan_Type__c and 
                                                            Internal_Use_Only__c = false 
                                                            //and Product_Tier__c =: opprLst[0].ProductTier__c
                                                            ORDER BY Product_Tier__c DESC, APR__c ASC];*/
                                                            
                                        //Modified above query By Deepika as part of Orange - 3622 - Start
                                        String productQuery = 'Select id, APR__c, NonACH_APR__c, Term_mo__c, Product_Tier__c from Product__c where Installer_Account__c =:installerId and State_Code__c = \''+opprLst[0].Install_State_Code__c+'\' and Is_Active__c = true and Product_Loan_Type__c = \''+opprLst[0].Product_Loan_Type__c+'\' and Internal_Use_Only__c = false';                                                          
                                        if(opprLst[0].isACH__c){
                                            productQuery += ' ORDER BY Product_Tier__c DESC, APR__c ASC';
                                        }else{
                                            productQuery += ' ORDER BY Product_Tier__c DESC, NonACH_APR__c ASC';                                
                                        }
                                        prdctList = Database.query(productQuery);
                                        //Added by Deepika as part of 3622 - End                                                                                                    
                                                            
                                        system.debug('Solar prdctList - '+prdctList);
                                        
                                        if(!prdctList.isEmpty())
                                        {
                                            Boolean foundGreaterthantierzero = false;
                                            for(Product__c p : prdctList)
                                            {
                                                system.debug('### p.Product_Tier__c :'+p.Product_Tier__c);    
                                                if(String.isNotBlank(p.Product_Tier__c) && Integer.valueOf(p.Product_Tier__c) > 0)
                                                {
                                                    foundGreaterthantierzero = true;
                                                }
                                            }
                                            system.debug('### Product_Tier__c '+prdctList[0].Product_Tier__c);
                                            if(!foundGreaterthantierzero)
                                            {
                                                system.debug('### no Tier 1 products found in Solar');
                                                disclsrStr = ' ';
                                            }
                                            else if(prdctList[0].Product_Tier__c != null){   
                                                disclsrStr = disclsrObj[0].Disclosure_Text__c;  
                                                system.debug('disclsrStr 1 - '+disclsrStr);
                                                if(disclsrStr.contains('X.XX')){
                                                    //Added by Deepika as part of 3622 - Start
                                                    if(opprLst[0].Long_Term_APR__c != 0)
                                                    {
                                                        disclsrStr = disclsrStr.replace('X.XX',String.valueOf(opprLst[0].Long_Term_APR__c));//Added as part of 3622 Udaya Kiran 14-05                                                   
                                                    }
                                                    else
                                                    {
                                                        if(opprLst[0].isACH__c)
                                                        {
                                                            disclsrStr = disclsrStr.replace('X.XX',String.valueOf(opprLst[0].SLF_Product__r.APR__c.setScale(2)));//Added as part of 3622 Dax 
                                                        }
                                                        else
                                                        {
                                                            disclsrStr = disclsrStr.replace('X.XX',String.valueOf(opprLst[0].SLF_Product__r.NonACH_APR__c.setScale(2)));//Added as part of 3622 Dax 
                                                        }
                                                    }
                                                    //Added by Deepika as part of 3622 - End
                                                    system.debug('disclsrStr 2 - '+disclsrStr);
                                                }
                                                if(disclsrStr.contains('AA'))
                                                    disclsrStr = disclsrStr.replace('AA',String.valueOf(opprLst[0].SLF_Product__r.Term_mo__c/12));
                                                    system.debug('disclsrStr 3 - '+disclsrStr);
                                                if(disclsrStr.contains('Y.YY'))
                                                    //Added by Deepika as part of 3622 - Start
                                                    if(opprLst[0].isACH__c){
                                                        disclsrStr = disclsrStr.replace('Y.YY',String.valueOf(prdctList[0].APR__c));
                                                    }else{
                                                        disclsrStr = disclsrStr.replace('Y.YY',String.valueOf(prdctList[0].NonACH_APR__c.setScale(2)));
                                                    }
                                                    //Added by Deepika as part of 3622 - End
                                                    system.debug('disclsrStr 4 - '+disclsrStr);
                                                if(disclsrStr.contains('BB'))
                                                    disclsrStr = disclsrStr.replace('BB',String.valueOf(prdctList[0].Term_mo__c/12));
                                                    system.debug('disclsrStr 5 - '+disclsrStr);
                                            }
                                        }
                                        else
                                        {
                                            system.debug('### no products found in Solar');
                                            disclsrStr = ' ';
                                            
                                        }
                                        
                                    }else if(opprLst[0].Project_Category__c == 'Home'){
                                        // for Home deals
                                        /*prdctList = [Select id, APR__c, Term_mo__c, Product_Tier__c 
                                                        from Product__c 
                                                        where Installer_Account__c =:installerId and 
                                                        State_Code__c = :opprLst[0].Install_State_Code__c and 
                                                        Is_Active__c = true and
                                                        ACH__c = :opprLst[0].SLF_Product__r.ACH__c and 
                                                        Product_Loan_Type__c = :opprLst[0].Product_Loan_Type__c and 
                                                        Internal_Use_Only__c = false 
                                                        ORDER BY Product_Tier__c ASC, APR__c ASC];*/
                                        //Modified above query By Deepika as part of Orange - 3622 - Start
                                        
                                        //Commented by Ravi as part of ORANGE-7723 START
                                        /*
                                        String productQuery = 'Select id, APR__c, Term_mo__c, Product_Tier__c from Product__c where Installer_Account__c =\'' + installerId + '\' and State_Code__c = \'' + opprLst[0].Install_State_Code__c + '\' and Is_Active__c = true and Product_Loan_Type__c = \'' + opprLst[0].Product_Loan_Type__c+ '\' and Internal_Use_Only__c = false';                                                          
                                        if(opprLst[0].isACH__c){
                                            productQuery += ' and APR__c != 0 ORDER BY Product_Tier__c DESC, APR__c ASC';
                                        }else{
                                            productQuery += ' and NonACH_APR__c != 0 ORDER BY Product_Tier__c DESC, NonACH_APR__c ASC';                                
                                        }
                                        system.debug('### productQuery:' + productQuery);
                                        prdctList = Database.query(productQuery);
                                        //Added by Deepika as part of 3622 - End
                                        
                                        system.debug('Home prdctList - '+prdctList);
                                        
                                        if(!prdctList.isEmpty())
                                        {
                                            Set<String> prodSet = new Set<String>();
                                            if(opprLst[0].Product_Loan_Type__c == 'HII'){
                                                disclsrStr = disclsrObj[0].Disclosure_Text__c; 
                                                String prodStr = '';
                                                for(Product__c prod :prdctList){
                                                    String productName = '';
                                                    //Added by Deepika as part of Orange - 3622 - Start
                                                    if(opprLst[0].isACH__c)
                                                        productName = prod.APR__c+'-'+prod.Term_mo__c;
                                                    else
                                                        productName = prod.NonACH_APR__c+'-'+prod.Term_mo__c;
                                                    //Added by Deepika as part of Orange - 3622 - End
                                                    System.debug('productName '+productName);
                                                    if(opprLst[0].SLF_Product__r.Term_mo__c == prod.Term_mo__c){
                                                         if(!prodSet.Contains(productName))
                                                         {
                                                            prodset.add(productName);
                                                            //Added by Deepika as part of Orange - 3622 - Start
                                                            System.debug('opprLst[0].isACH__c'+opprLst[0].isACH__c);
                                                            if(opprLst[0].isACH__c)
                                                                prodStr += '<br />'+String.valueOf(prod.Term_mo__c/12)+' year loan with a '+String.valueOf(prod.APR__c)+'% Annual Percentage Rate';
                                                            else
                                                                prodStr += '<br />'+String.valueOf(prod.Term_mo__c/12)+' year loan with a '+String.valueOf(prod.NonACH_APR__c.setScale(2))+'% Annual Percentage Rate';
                                                             //Added by Deepika as part of Orange - 3622 - End   
                                                         }
                                                        System.debug('@@@prodStr '+prodStr);
                                                        System.debug('@@@prodSet '+prodSet);
                                                     }
                                                     
                                                }
                                                if(disclsrStr.contains('<br />AA year loan with a X.XX% Annual Percentage Rate<br />BB year loan with a Y.YY% Annual Percentage Rate')){
                                                    disclsrStr = disclsrStr.replace('<br />AA year loan with a X.XX% Annual Percentage Rate<br />BB year loan with a Y.YY% Annual Percentage Rate',prodStr);
                                                }
                                            }else if(opprLst[0].Product_Loan_Type__c == 'HIS' || opprLst[0].Product_Loan_Type__c == 'HIN')
                                            {
                                                disclsrStr = ' ';
                                            }
                                        }
                                        else
                                        {
                                            system.debug('### no products found in Home');
                                            disclsrStr = ' ';
                                        }*/
                                        //Commented by Ravi as part of ORANGE-7723 END
                                        
                                        disclsrStr = ' ';
                                    }
                                    else
                                    {
                                        // incase of error
                                        Database.rollback(sp);
                                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                        respBean.error.add(errorMsg);
                                        respBean.returnCode = '400';
                                        iRestRes = (IRestResponse)respBean;
                                    }
                                }                                                                          
                            }
                            else{
                                Database.rollback(sp);
                                errMsg = ErrorLogUtility.getErrorCodes('214',null);
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                respBean.error.add(errorMsg);
                                respBean.returnCode = '214';
                                iRestRes = (IRestResponse)respBean;
                                return iRestRes;
                            }
                        }
                    }
                    UnifiedBean.OpportunityWrapper projectIterate = new UnifiedBean.OpportunityWrapper();
                    UnifiedBean.DisclosureWrapper disclsrWrap = new UnifiedBean.DisclosureWrapper();
                    system.debug('disclsrStr 6 - '+disclsrStr);
                    disclsrWrap.disclosureText = disclsrStr;
                    projectIterate.disclosures= new List<UnifiedBean.DisclosureWrapper>();
                    projectIterate.disclosures.add(disclsrWrap);
                    respBean.projects.add(projectIterate);    
                                                                           
                    respBean.returnCode = '200';
                    iRestRes = (IRestResponse)respBean;
                    
                }
            }else{
                Database.rollback(sp);
                errMsg = ErrorLogUtility.getErrorCodes('400',null);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                respBean.error.add(errorMsg);
                respBean.returnCode = '400';
                iRestRes = (IRestResponse)respBean;
            }
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            FNIFacilityDecisionDataServiceImpl facltyDcsnObj = new FNIFacilityDecisionDataServiceImpl();
            facltyDcsnObj.gerErrorMsg(respBean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)respBean;
            system.debug('### DisclosureDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('DisclosureDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### iRestRes '+iRestRes);
        system.debug('### Exit from transformOutput() of '+DisclosureDataServiceImpl.class);

        return iRestRes;
    }
}