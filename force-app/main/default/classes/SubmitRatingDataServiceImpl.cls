/**
* Description: User validation logic
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suresh Kumar            10/26/2018          Created
******************************************************************************************/
public class SubmitRatingDataServiceImpl extends RestDataServiceBase{
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){ 
        system.debug('### Entered into transformOutput() of '+SubmitRatingDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        
        UnifiedBean unifiedRespBean = new UnifiedBean(); 
        unifiedRespBean.error = new list<UnifiedBean.errorWrapper>();
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        string errMsg = '';
        IRestResponse iRestRes;
        Savepoint sp = Database.setSavepoint();
        try{
            list<User> loggedInUsr = [select Id,contact.AccountId,LoginCount__c from User where id =: Userinfo.getUserId()];
            if(reqData.ratingDetails != null){

                Rating__c objRating = new Rating__c();
              //  objRating.Comments__c = reqData.ratingDetails.comments!=null ? reqData.ratingDetails.comments : '';
                objRating.Rating__c = reqData.ratingDetails.rating;
                objRating.No_Thanks__c = reqData.ratingDetails.noThanks;
                objRating.Source_of_Feedback__c = reqData.ratingDetails.source;
                objRating.Comments__c = reqData.ratingDetails.comments;
                objRating.User__c = userinfo.getUserId();
                objRating.Rating_DateTime__c = System.now();
                objRating.Installer__c = loggedInUsr[0].contact.AccountId;
                insert objRating;
                
                unifiedRespBean.returnCode = '200';
                unifiedRespBean.message = 'Rating submitted successfully';
            }
        
        else{
                iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('214',null);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                unifiedRespBean.error.add(errorMsg);
                unifiedRespBean.returnCode = '214';
            }
        }Catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedRespBean.error.add(errorMsg);
            unifiedRespBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedRespBean;
            system.debug('### SubmitRatingDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('SubmitRatingDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from transformOutput() of '+SubmitRatingDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        iRestRes = (IRestResponse)unifiedRespBean;
        return iRestRes;
    }
}