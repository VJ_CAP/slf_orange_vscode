@isTest
private class deleteOpportunityShareTest {

    private static testMethod void test() {
         
         SLFUtilityTest.createCustomSettings();
                
        //get the person account record type
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account(); 
        acc.name = 'Test Account';
        acc.RecordTypeId = accRecTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id,Is_Default_Owner__c = true);
        insert con;  
        
        
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        
        
        Account acc1 = new Account();
        acc1.name = 'Test Account';
        acc1.RecordTypeId = accRecTypeInfoMap.get('Business Account').getRecordTypeId();
        acc1.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.BillingStateCode = 'CA';
        acc1.Installer_Legal_Name__c='Bright Solar Planet';
        acc1.Solar_Enabled__c = true;
        insert acc1;
        
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acc1.Id,Is_Default_Owner__c = true,ReportsToid=con.id);
        insert con1;  
        
        
        User portalUser1 = new User(alias = 'testc1', Email='test1c@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test1', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass1@mail.com',ContactId = con1.Id);
        insert portalUser1;
        
        
        
        
            Account acc2 = new Account();
            acc2.LastName = 'Test Account';
            acc2.Push_Endpoint__c ='www.test.com';
            acc2.PersonEmail = 'slfaccount@slf.com';
            acc2.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
            acc2.Installer_Email__c = 'slfaccount@slf.com';
            acc2.Installer_Legal_Name__c='Bright Solar Planet';
            insert acc2;
            
            Account businessAcc = new Account();
            businessAcc.Name = 'Test Account';
            businessAcc.Push_Endpoint__c ='www.test.com';
            businessAcc.Installer_Legal_Name__c='Bright Solar Planet';
            businessAcc.Solar_Enabled__c = true;
            insert businessAcc;
            
            Product__c prodct = new Product__c();
            prodct.ACH__c = false;
            prodct.APR__c = 4.99;
            prodct.bump__c = false;
            prodct.Installer_Account__c = acc.Id;
            prodct.Internal_Use_Only__c = false;
            prodct.Is_Active__c = true;
            prodct.Long_Term_Facility__c = 'TCU';
            prodct.LT_Max_Loan_Amount__c =  100000.0;
            prodct.Max_Loan_Amount__c = 100000.0;
            prodct.State_Code__c = 'CA';
            prodct.FNI_Min_Response_Code__c = 10;
            prodct.Product_Display_Name__c = 'Test';
            prodct.Qualification_Message__c = '123';
            prodct.Term_mo__c = 12;
            prodct.Product_Tier__c = '0';
            insert prodct;
            
          
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Postal_Code__c = '94901';
            opp.Install_Street__c = '39 GLEN AVE';
            opp.Install_City__c = 'SAN RAFAEL';
            opp.SLF_Product__c = prodct.Id;
            opp.Combined_Loan_Amount__c = 10000;
            opp.Long_Term_Loan_Amount_Manual__c = 20000;
            opp.Co_Applicant__c = acc.Id;
            insert opp;
            
            
             Test.startTest();
            deleteOpportunityShare objbatch = new deleteOpportunityShare(null);
            //Database.executeBatch(new deleteOpportunityShare(), 50);
            list<opportunityShare> lst = [select id from opportunityShare];
            objbatch.start(null);
            objbatch.execute(null,lst);
            objbatch.finish(null);
            Test.stopTest();
            
            
    }

}