@isTest
public class SubmitRatingDataServiceTest {
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
    }
    
    private testMethod static void SubmitRatingDataServiceTest(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' and id = :loggedInUsr[0].contact.AccountId LIMIT 1]; 
        Map<String, String> ratingMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            String submitRating;
            Test.starttest();
            submitRating = '{"ratingDetails":{"rating":5,"comments":"Paul C. is pretty cool too!","source":"Wikilink","noThanks":false}}';
            MapWebServiceURI(ratingMap,submitRating,'submitrating');
            
            Test.StopTest();
            
        }
        
    }
    private testMethod static void SubmitRatingDataServiceTest2(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' and id = :loggedInUsr[0].contact.AccountId LIMIT 1]; 
        Map<String, String> ratingMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            String submitRating=null;
            Test.starttest();
            //submitRating = '{"ratingDetails":{"rating":5,"comments":"Paul C. is pretty cool too!","source":"Wikilink","noThanks":false}}';
            MapWebServiceURI(ratingMap,submitRating,'submitrating');
            
            Test.StopTest();
            
        }
        
    }
    private testMethod static void SubmitRatingDataServiceTest3(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' and id = :loggedInUsr[0].contact.AccountId LIMIT 1]; 
        Map<String, String> ratingMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            SubmitRatingDataServiceImpl objReqClass = new SubmitRatingDataServiceImpl();
            String submitRating;
            Test.starttest();
            // submitRating = '"ratingDetails":{"rating":5,"comments":"Paul C. is pretty cool too!","source":"Wikilink","noThanks":false}}';
            //  MapWebServiceURI(ratingMap,submitRating,'submitrating');
            objReqClass.transformOutput(null);
            Test.StopTest();
            
        }
        
    }
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }  
}