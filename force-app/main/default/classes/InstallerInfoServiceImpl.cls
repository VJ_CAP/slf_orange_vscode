/**
* Description: Installer Information related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh R           07/10/2017          Created
******************************************************************************************/
public with sharing class InstallerInfoServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + InstallerInfoServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean reqData ;
        if(string.isNotBlank(rw.reqDataStr))
        {
            reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        }else{
            reqData = new UnifiedBean();
        }
         
        
        UnifiedBean respData = new UnifiedBean();
                
        try{
            system.debug('### reqData '+reqData);
            UnifiedBean.InstallerInfoWrapper reqInstallerInfoWrapper;
            if(null != reqData.installerInfo)
            {
                reqInstallerInfoWrapper = reqData.installerInfo;
            }else{
                reqInstallerInfoWrapper = new UnifiedBean.InstallerInfoWrapper();
            }               
            UnifiedBean.InstallerInfoWrapper respInstallerInfoWrapper = new UnifiedBean.InstallerInfoWrapper();
            respData.installerInfo = respInstallerInfoWrapper;

            String accountId = null;
            List<user> loggedInUsrLst = [select id,name,contact.id, contact.account.id, contact.account.name from user where id=: userinfo.getUserId()];
            system.debug('### loggedInUsrLst '+loggedInUsrLst);
            if(!loggedInUsrLst.isEmpty() && loggedInUsrLst.size()>0)
            {
                accountId = loggedInUsrLst.get(0).contact.accountId;
            }
            
            if(accountId!=null && String.isNotBlank(accountId)){
                //Added new fields to query By Deepika on 6/2/2019 as part of Orange - 1930
                String queryStr = 'select id, Name, M0_Required__c, M1_Required__c, M2_Required__c, Kitting_Required_Documents_Solar__c, Permit_Required_Documents_Solar__c, Inspection_Required_Documents_Solar__c, Inspection_Ever_Enabled__c, Kitting_Ever_Enabled__c, Permit_Ever_Enabled__c from Account where id=\''+accountId+'\'';
                List<String> lstConditions = new List<String>();
                if(String.isNotBlank(reqInstallerInfoWrapper.milestoneFilter))
                {
                    if (reqInstallerInfoWrapper.milestoneFilter == 'm0RequiredDocuments') {
                        lstConditions.add('M0_Required__c');
                    }
                    if (reqInstallerInfoWrapper.milestoneFilter == 'm1RequiredDocuments') {
                        lstConditions.add('M1_Required__c');
                    }
                    if (reqInstallerInfoWrapper.milestoneFilter == 'm2RequiredDocuments') {
                        lstConditions.add('M2_Required__c');
                    }
                    //Added By Deepika on 6/2/2019 as part of Orange - 1930 - Start
                    if (reqInstallerInfoWrapper.milestoneFilter == 'KittingRequiredDocuments') {
                        lstConditions.add('Kitting_Required_Documents_Solar__c');
                    }
                    if (reqInstallerInfoWrapper.milestoneFilter == 'PermitRequiredDocuments') {
                        lstConditions.add('Permit_Required_Documents_Solar__c');
                    }
                    if (reqInstallerInfoWrapper.milestoneFilter == 'InspectionRequiredDocuments') {
                        lstConditions.add('Inspection_Required_Documents_Solar__c');
                    }
                    //Added By Deepika on 6/2/2019 as part of Orange - 1930 - End
                }
                
                string fieldsToquery = '';
                
                if(lstConditions!=null && lstConditions.size()>0){
                    for (integer i = 0; i < lstConditions.size(); i++) {
                        if (i == lstConditions.size() - 1) {
                            fieldsToquery = fieldsToquery + lstConditions.get(i);
                        } else {
                            fieldsToquery = fieldsToquery + lstConditions.get(i) + ', ';
                        }
                    }
                }
                
                if(string.isNotBlank(fieldsToquery))
                {
                    //Added new fields to query By Deepika on 6/2/2019 as part of Orange - 1930
                    queryStr = 'select id, Name, M0_Required__c, M1_Required__c, M2_Required__c, Kitting_Required_Documents_Solar__c, Permit_Required_Documents_Solar__c, Inspection_Required_Documents_Solar__c, Inspection_Ever_Enabled__c, Kitting_Ever_Enabled__c, Permit_Ever_Enabled__c from Account where id=\''+accountId+'\'';
                }
                system.debug('### queryStr before '+queryStr);
                
                try{
                    List<Account>  acctResults  = Database.query(queryStr);
                    if(acctResults!=null && acctResults.size()>0){
                        for(Account acct : acctResults){
                            
                            Map<String,String> mapFilterValues = new Map<String,String>();
                            mapFilterValues.put('m0requireddocuments','m0RequiredDocuments');
                            mapFilterValues.put('m1requireddocuments','m1RequiredDocuments');
                            mapFilterValues.put('m2requireddocuments','m2RequiredDocuments');
                            //Added By Deepika on 6/2/2019 as part of Orange - 1930 - Start
                            mapFilterValues.put('kittingrequireddocuments','KittingRequiredDocuments');
                            mapFilterValues.put('permitrequireddocuments','PermitRequiredDocuments');
                            mapFilterValues.put('inspectionrequireddocuments','InspectionRequiredDocuments');
                            //Added By Deepika on 6/2/2019 as part of Orange - 1930 - End
                            
                            respInstallerInfoWrapper.name = acct.name;
                            respInstallerInfoWrapper.milestoneFilter = reqInstallerInfoWrapper.milestoneFilter;
                           
                            if(((acct.M0_Required__c==null||acct.M0_Required__c=='') && (acct.M1_Required__c==null||acct.M1_Required__c=='') && (acct.M2_Required__c==null||acct.M2_Required__c=='')) || (acct.Inspection_Ever_Enabled__c && String.isBlank(acct.Inspection_Required_Documents_Solar__c)) || (acct.Permit_Ever_Enabled__c && String.isBlank(acct.Permit_Required_Documents_Solar__c)) || (acct.Kitting_Ever_Enabled__c && String.isBlank(acct.Kitting_Required_Documents_Solar__c)))
                            {
                                respData.returnCode = '260';
                                List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                                UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                                errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('260', null);
                                errorLst.add(errorWrap);
                                respData.error = errorLst;
                            }
                            else if(reqInstallerInfoWrapper.milestoneFilter!=null && reqInstallerInfoWrapper.milestoneFilter != ''){
                                respData.returnCode = '200';
                                if(!mapFilterValues.containsKey(reqInstallerInfoWrapper.milestoneFilter.toLowerCase() )){
                                    respData.returnCode = '200';
                                    respInstallerInfoWrapper.m0RequiredDocuments = documentResponseString(acct.M0_Required__c);
                                    respInstallerInfoWrapper.m1RequiredDocuments = documentResponseString(acct.M1_Required__c);
                                    respInstallerInfoWrapper.m2RequiredDocuments = documentResponseString(acct.M2_Required__c);
                                    //Added by Deepika on 6/2/2019 as part of Orange - 1930 - Start
                                    if(acct.Permit_Ever_Enabled__c)
                                        respInstallerInfoWrapper.permitRequiredDocuments  = documentResponseString(acct.Permit_Required_Documents_Solar__c);
                                    if(acct.Kitting_Ever_Enabled__c)
                                        respInstallerInfoWrapper.kittingRequiredDocuments  = documentResponseString(acct.Kitting_Required_Documents_Solar__c);
                                    if(acct.Inspection_Ever_Enabled__c)
                                        respInstallerInfoWrapper.inspectionRequiredDocuments  = documentResponseString(acct.Inspection_Required_Documents_Solar__c);
                                    //Added by Deepika on 6/2/2019 as part of Orange - 1930 - End
                                }                                
                                else{
                                    if( reqInstallerInfoWrapper.milestoneFilter.equalsIgnoreCase('m0RequiredDocuments')){
                                        respInstallerInfoWrapper.m0RequiredDocuments = documentResponseString(acct.M0_Required__c);
                                    }
                                    if(reqInstallerInfoWrapper.milestoneFilter.equalsIgnoreCase('m1RequiredDocuments')){
                                        respInstallerInfoWrapper.m1RequiredDocuments = documentResponseString(acct.M1_Required__c);
                                    }
                                    if(reqInstallerInfoWrapper.milestoneFilter.equalsIgnoreCase('m2RequiredDocuments')){
                                        respInstallerInfoWrapper.m2RequiredDocuments = documentResponseString(acct.M2_Required__c);
                                    }
                                    //Added by Deepika on 6/2/2019 as part of Orange - 1930 - Start
                                    if(acct.Permit_Ever_Enabled__c && reqInstallerInfoWrapper.milestoneFilter.equalsIgnoreCase('permitRequiredDocuments')){
                                        respInstallerInfoWrapper.permitRequiredDocuments = documentResponseString(acct.Permit_Required_Documents_Solar__c);
                                    }
                                    if(acct.Kitting_Ever_Enabled__c && reqInstallerInfoWrapper.milestoneFilter.equalsIgnoreCase('kittingRequiredDocuments')){
                                        respInstallerInfoWrapper.kittingRequiredDocuments = documentResponseString(acct.Kitting_Required_Documents_Solar__c);
                                    }
                                    if(acct.Inspection_Ever_Enabled__c && reqInstallerInfoWrapper.milestoneFilter.equalsIgnoreCase('inspectionRequiredDocuments')){
                                        respInstallerInfoWrapper.inspectionRequiredDocuments = documentResponseString(acct.Inspection_Required_Documents_Solar__c);
                                    }
                                    //Added by Deepika on 6/2/2019 as part of Orange - 1930 - End
                                }
                            }
                            else{
                                respData.returnCode = '200';
                                respInstallerInfoWrapper.m0RequiredDocuments = documentResponseString(acct.M0_Required__c);
                                respInstallerInfoWrapper.m1RequiredDocuments = documentResponseString(acct.M1_Required__c);
                                respInstallerInfoWrapper.m2RequiredDocuments = documentResponseString(acct.M2_Required__c);
                                //Added by Deepika on 6/2/2019 as part of Orange - 1930 - Start
                                if(acct.Permit_Ever_Enabled__c)
                                    respInstallerInfoWrapper.permitRequiredDocuments  = documentResponseString(acct.Permit_Required_Documents_Solar__c);
                                if(acct.Kitting_Ever_Enabled__c)
                                    respInstallerInfoWrapper.kittingRequiredDocuments  = documentResponseString(acct.Kitting_Required_Documents_Solar__c);
                                if(acct.Inspection_Ever_Enabled__c)
                                    respInstallerInfoWrapper.inspectionRequiredDocuments  = documentResponseString(acct.Inspection_Required_Documents_Solar__c);
                                //Added by Deepika on 6/2/2019 as part of Orange - 1930 - End
                            }
                        }
                    }
                }
                catch(Exception ex){
                    respData.returnCode = '400';
                    List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                    UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                    errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('400', null);
                    errorLst.add(errorWrap);
                    respData.error = errorLst;
                }
                //respData.returnCode = '200';
            }
            else{
                respData.returnCode = '389';
                respInstallerInfoWrapper.errorMessage = ErrorLogUtility.getErrorCodes('389', null);
            }
            res.response = (IRestResponse)respData;

        }catch(Exception err){
           system.debug('### InstallerInfoServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' InstallerInfoServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + InstallerInfoServiceImpl.class);
        return res.response ;
    }
    
    public String documentResponseString(String sSourceData){
            if(sSourceData!=null && String.isNotBlank(sSourceData)){
                String sConvertedData = null;
                List<String> documentTypeLst = sSourceData.split(';');
                if(documentTypeLst!=null && documentTypeLst.size()>0){
                    sConvertedData = '[';
                    for(integer i = 0; i < documentTypeLst.size(); i++){
                        String sDocumentType = documentTypeLst.get(i);
                        if(i == documentTypeLst.size() - 1) {
                            sConvertedData = sConvertedData + '{'+sDocumentType+'}';
                        }else{
                            sConvertedData = sConvertedData + '{'+sDocumentType+'},';
                        }
                    }
                    sConvertedData = sConvertedData+']';
                }
                return sConvertedData;
            }
            else{
                return sSourceData;
            }       
    }
}