/**
* Description: Credit Sync related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/

public class SyncCreditDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+SyncCreditDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        
        UnifiedBean respData = new UnifiedBean(); 
        respData.projects = new List<UnifiedBean.OpportunityWrapper>();
        //respData = reqData ;
        respData.error = new list<UnifiedBean.errorWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        projectWrap.applicants = new List<UnifiedBean.ApplicantWrapper>();
        projectWrap.stips = new List<UnifiedBean.StipsAndStatusWrapper>();
        projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
        projectWrap.systemDesigns = new List<UnifiedBean.SystemDesignWrapper>();
        projectWrap.quotes = new List<UnifiedBean.QuotesWrapper>();
        
        
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {   
        
            if(null != reqData.projects)
            {
                if(null != reqData.projects[0].credits)
                {
                    List<UnifiedBean.CreditsWrapper> creditRecLst = reqData.projects[0].credits;
                    Map<string,SLF_Credit__c> creditMap = new Map<string,SLF_Credit__c>();
                    Boolean isOppClosed = false;
                    set<string> creditSet = new set<string>();
                    set<Id> oppIdSet = new set<Id>();
                    set<Id> creditIdSet = new set<Id>();
                    List<SLF_Credit__c> SLFCreditList = new List<SLF_Credit__c>(); //Delete
                    for(UnifiedBean.CreditsWrapper creditIterate : creditRecLst)
                    {
                        if(creditIterate.id != null && string.isNotBlank(creditIterate.id))
                            creditSet.add(creditIterate.Id);
                    }
                    if(creditSet.Size()>1)
                    {
                       errMsg = ErrorLogUtility.getErrorCodes('397',null);
                       respData.returnCode = '397';
                    }else if(!creditSet.IsEmpty())
                    {
                        List<SLF_Credit__c> creditLst = [Select id,Name,Primary_Applicant__c,SLF_Product__r.Expiration_Date__c,Credit_Decision_Date__c,Opportunity__c,Status__c,IsSyncing__c,LT_FNI_Reference_Number__c,Opportunity__r.StageName,Opportunity__r.SyncedQuoteId,Opportunity__r.SyncedQuote.SLF_Product__c,Application_ID__c,Co_Applicant__c,SLF_Product__c,Total_Loan_Amount__c from SLF_Credit__c where id IN: creditSet];
                        system.debug('### creditLst '+creditLst);
                        if(!creditLst.isEmpty())
                        {
                            for(SLF_Credit__c creditIterate : creditLst)
                            {
                                if(creditIterate.SLF_Product__r.Expiration_Date__c != null && creditIterate.SLF_Product__r.Expiration_Date__c < system.today()){
                                    errMsg = ErrorLogUtility.getErrorCodes('362',null);
                                    iolist = new list<DataValidator.InputObject>{};
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    respData.error.add(errorMsg);
                                    respData.returnCode = '362';
                                    iRestRes = (IRestResponse)respData;
                                    return iRestRes;
                                }
                                if(creditIterate.Opportunity__r.StageName =='Closed Won'){
                                    isOppClosed = true;
                                }
                                string creditId = string.ValueOf(creditIterate.Id);
                                if(creditId.length()>15)
                                    creditId = creditId.subString(0,15);
                                    
                                creditMap.put(creditId,creditIterate);
                                oppIdSet.add(creditIterate.Opportunity__c);
                                
                            }
                        }else{
                            errMsg = ErrorLogUtility.getErrorCodes('395',null);
                            respData.returnCode = '395';                            
                        }
                    }else{
                        errMsg = ErrorLogUtility.getErrorCodes('395',null);
                        respData.returnCode = '395';
                    }
                    if(!oppIdSet.isEmpty()){
                    List<user> loggedInUsr = [select Id,contactid,contact.AccountId,contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,UserRole.Name from User where id =: userinfo.getUserId()];
  
                              
                    List<Opportunity> oppList = [Select id,Installer_Account__c,(select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c,Withdraw_Reason__c,Declined_Withdrawn_Date__c from Underwriting_Files__r) from Opportunity where id IN :oppIdSet]; 
 
                     Map<String,String> OwnercheckMap = SLFUtility.checkOpportunityOwner(oppList[0],loggedInUsr[0]);
                    if(OwnercheckMap != null){
                        errMsg = OwnercheckMap.values().get(0);
                        List<String> errList = new List<String>();
                        errList.addAll(OwnercheckMap.keyset());
                        respData.returnCode = errList[0];
                        
                    }else if(oppList[0].Underwriting_Files__r != null && oppList[0].Underwriting_Files__r[0].Project_Status__c != null && oppList[0].Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                        errMsg = ErrorLogUtility.getErrorCodes('366',null);
                        respData.returnCode = '366';
                    }           
                    else if(isOppClosed){
                        errMsg = ErrorLogUtility.getErrorCodes('233',null);
                        respData.returnCode = '233';
                    }else
                    {
                        List<Opportunity> updateOppLst = new List<Opportunity>();
                        if(!creditMap.isEmpty())
                        {
                            for(UnifiedBean.CreditsWrapper creditIterate : creditRecLst)
                            {
                                if(string.isNotBlank(creditIterate.id))
                                {
                                    if(creditIterate.id.length()>15)
                                        creditIterate.id = creditIterate.id.subString(0,15);
                                    
                                    if(creditMap.containsKey(creditIterate.Id))
                                    {
                                        SLF_Credit__c creditRec = creditMap.get(creditIterate.Id);
                                        creditRec.IsSyncing__c = true;
                                        SLFCreditList.add(creditRec);
                                        Opportunity oppRec = new Opportunity(id = creditRec.Opportunity__c);
                                        if(null != creditRec.Primary_Applicant__c)
                                            oppRec.AccountId = creditRec.Primary_Applicant__c; 
                                        if(null != creditRec.Co_Applicant__c)
                                            oppRec.Co_Applicant__c = creditRec.Co_Applicant__c;   
                                        /*                                      
                                        // Added as part of Orange-292 to de-sync Quote based on product change starts
                                        List<Credit_Opinion__c> crdOpinoinLst = [select Id,Name,Credit__c,Quote__c,Status__c,Credit__r.Status__c,Credit__r.IsSyncing__c,Credit__r.LastModifiedDate,Credit__r.Opportunity__r.SyncedQuoteId from Credit_Opinion__c where Credit__c =:creditRec.id and status__c = 'N/A'];
                                        if(!crdOpinoinLst.isEmpty()){
                                            for(Credit_Opinion__c creditOp :crdOpinoinLst){
                                                    if(creditOp.Credit__r.Opportunity__r.SyncedQuoteId != null && creditOp.Credit__r.Opportunity__r.SyncedQuoteId == creditOp.Quote__c){
                                                        //oppRec.SyncedQuoteId = null;
                                                        oppRec = SyncDesyncUtilities.desyncQuoteFields(oppRec);
                                                    }
                                            }
                                        }                                      
                                        //Orange-292 ends
                                        */
                                        //oppRec.Long_Term_Loan_Amount__c = creditRec.Long_Term_Loan_Amount__c;
                                        //oppRec.Total_Loan_Amount__c = creditRec.Total_Loan_Amount__c;
                                        if(null != creditRec.Total_Loan_Amount__c)
                                            oppRec.Combined_Loan_Amount__c = creditRec.Total_Loan_Amount__c;
                                        if(null != creditRec.Status__c)
                                            oppRec.Application_Status__c = creditRec.Status__c;
                                        if(null != creditRec.LT_FNI_Reference_Number__c)
                                            oppRec.FNI_Application_ID__c = creditRec.LT_FNI_Reference_Number__c;
                                        if(null != creditRec.Application_ID__c)
                                            oppRec.Application_ID__c = creditRec.Application_ID__c;
                                       
                                        updateOppLst.add(oppRec);
                                    }else{
                                        errMsg = ErrorLogUtility.getErrorCodes('395',null);
                                        respData.returnCode = '395';
                                    }
                                    
                                }else{
                                     errMsg = ErrorLogUtility.getErrorCodes('395',null);
                                    respData.returnCode = '395';
                                }
                            }
                            if(!updateOppLst.isEmpty()){
                                upsert updateOppLst;
                            }
                            if(!SLFCreditList.isEmpty()){
                                upsert SLFCreditList;
                            }
                            
                            respData = SLFUtility.getUnifiedBean(oppIdSet,'Orange',false);
                            respData.returnCode = '200';
                            iRestRes = (IRestResponse)respData;
                        }
                    
                    }
                    }
                    
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('396',null);
                    respData.returnCode = '396';
                }   
            }
            
            if(string.isNotBlank(errMsg))
            {
                iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                errMsg = errorMsg.errorMessage;
                respData.error.add(errorMsg);
                
            }
            iRestRes = (IRestResponse)respData;
        }catch(Exception err){
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            respData.error.add(errorMsg);
            respData.returnCode = '400';
            iRestRes = (IRestResponse)respData;
            system.debug('### SyncCreditDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('SyncCreditDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+SyncCreditDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
}