/**
* Description: Get Positions Logic
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Suresh Kumar            12/19/2018          Created
******************************************************************************************/
public with sharing class GetPositionsServiceImpl extends RESTServiceBase{  
    /**
* Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
*          
* @param rw    Get all POST request data.
*
* return res   Send response back with data and response status code.
*/
    public override IRestResponse fetchData(RequestWrapper rw){
        system.debug('### Entered into DrawRequestUtil.fetchData() of '+GetPositionsServiceImpl.class);
        //UnifiedBean reqData = (UnifiedBean)stsDtObj;
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        
        UnifiedBean unifiedRespBean = new UnifiedBean();
        unifiedRespBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        unifiedRespBean.error = new list<UnifiedBean.errorWrapper>();
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            string errMsg = '';
        IRestResponse iRestRes;
        Savepoint sp = Database.setSavepoint();
        try{
            String errorMsg = ' ';
            List<Position__c> positionsList = new list<Position__c>([Select id,Job_title__c from Position__c where Job_title__c!=null]);
            if(positionsList != null && positionsList.size()>0)
            {
                UnifiedBean.OpportunityWrapper projectIterate = new UnifiedBean.OpportunityWrapper();
                List<UnifiedBean.PositionWrapper> lstPositionWrapper = new List<UnifiedBean.PositionWrapper>();
                UnifiedBean.PositionWrapper positionWrap = new UnifiedBean.PositionWrapper();
                List<String> lstJobTitle = new List<String>();
                for(Position__c p : positionsList )
                {
                    lstJobTitle.add(p.Job_title__c);
                } 
                positionWrap.jobTitle = lstJobTitle;
                lstPositionWrapper.add(positionWrap);
                
                //projectIterate.positions = new List<UnifiedBean.PositionWrapper>();
                //projectIterate.positions.add(positionWrap);
                projectIterate.positions =lstPositionWrapper;
                
                unifiedRespBean.projects.add(projectIterate);
                unifiedRespBean.returnCode = '200';
                iRestRes = (IRestResponse)unifiedRespBean;
            }
            else{
                unifiedRespBean.returnCode = '400';
                //errMsg = ErrorLogUtility.getErrorCodes('400',null);
                errMsg = 'No records found.';
                UnifiedBean.errorWrapper errorMsgNew = SLFUtility.parseDataValidator(iolist,errMsg);
                unifiedRespBean.error.add(errorMsgNew);
                iRestRes = (IRestResponse)unifiedRespBean;
            }
        }
        Catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedRespBean.error.add(errorMsg);
            unifiedRespBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedRespBean;
            system.debug('### GetPositionsServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('GetPositionsServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of '+GetPositionsServiceImpl.class);
        iRestRes = (IRestResponse)unifiedRespBean;
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
    public void getErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
}