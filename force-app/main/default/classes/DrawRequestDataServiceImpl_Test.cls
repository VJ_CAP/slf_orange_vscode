/******************************************************************************************
* Description: Test class to cover DrawRequestDataServiceImpl services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkatesh            01/07/2019          Created
******************************************************************************************/
@isTest
public class DrawRequestDataServiceImpl_Test {
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
    }
    
    /**
*
* Description: This method is used to cover DrawRequestDataServiceImpl
*
*/   
    private testMethod static void drawRequestTest(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c,All_HI_Required_Documents__c,Has_Open_Stip__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        system.debug('### Has_Open_Stip__c==>:'+opp.Has_Open_Stip__c);
        system.debug('### All_HI_Required_Documents__c==>:'+opp.All_HI_Required_Documents__c);
                       
        Map<String, String> userMap = new Map<String, String>();
        TriggerFlags__c trgDrawReq = new TriggerFlags__c();
        trgDrawReq.Name ='Draw_Requests__c';
        trgDrawReq.isActive__c =true;
       
        insert trgDrawReq;
        System.runAs(loggedInUsr[0])
        {
            String drawReqStr;
            Test.starttest();
            // error 203
            drawReqStr = '{"projects": [{"id": "","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            // error 201
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","hashId":"'+opp.Hash_Id__c+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            // error 202
            drawReqStr = '{"projects": [{"id": "0062F000006BADd123","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            // Hash Id
            drawReqStr = '{"projects": [{"hashId":"'+opp.Hash_Id__c+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            // externalId
            drawReqStr = '{"projects": [{"externalId":"'+opp.Hash_Id__c+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            drawReqStr = '{"projects": [{"Id":"'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true,"progressPay":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            drawReqStr = '{"projects": [{"Id":"'+opp.Id+'","draws": [{"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":null,"amount": "100","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');

            Underwriting_File__c objUnderWriting = [select id,Approved_for_Payments__c from Underwriting_File__c where opportunity__r.id=:opp.id limit 1];
            
            Underwriting_File__c objUnderWritUpd = new Underwriting_File__c();
            objUnderWritUpd.id = objUnderWriting.id;
            objUnderWritUpd.Approved_for_Payments__c = true;
            
            update objUnderWritUpd;
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
           SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Opportunity oppUpd = new Opportunity();
            oppUpd.id = opp.Id;
            oppUpd.Has_Open_Stip__c = 'true';
            update oppUpd;

            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Opportunity oppUpd1 = new Opportunity();
            oppUpd1.id = opp.Id;
            oppUpd1.Block_Draws__c = true;
            update oppUpd1;
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Opportunity oppUpd2 = new Opportunity();
            oppUpd2.id = opp.Id;
            oppUpd2.All_HI_Required_Documents__c = false;
            update oppUpd2;
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":2,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
               
            // exception block
            drawReqStr = '[{}]';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            Test.StopTest();
        }
    }
        /**
*
* Description: This method is used to cover DrawRequestDataServiceImpl
*
*/   
    private testMethod static void drawRequestTest2(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c,All_HI_Required_Documents__c,Has_Open_Stip__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        
        system.debug('### Has_Open_Stip__c==>:'+opp.Has_Open_Stip__c);
        system.debug('### All_HI_Required_Documents__c==>:'+opp.All_HI_Required_Documents__c);
                       
        Map<String, String> userMap = new Map<String, String>();
        TriggerFlags__c trgDrawReq = new TriggerFlags__c();
        trgDrawReq.Name ='Draw_Requests__c';
        trgDrawReq.isActive__c =true;
       
        insert trgDrawReq;
        System.runAs(loggedInUsr[0])
        {
            String drawReqStr;
            Test.starttest();
            
            Underwriting_File__c objUnderWriting = [select id,Approved_for_Payments__c from Underwriting_File__c where opportunity__r.id=:opp.id limit 1];
            
            Underwriting_File__c objUnderWritUpd = new Underwriting_File__c();
            objUnderWritUpd.id = objUnderWriting.id;
            objUnderWritUpd.Approved_for_Payments__c = true;
            update objUnderWritUpd;
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            List<Draw_Requests__c> lstDrawRequests = new List<Draw_Requests__c> ();
            lstDrawRequests=[SELECT id,Status__c FROM Draw_Requests__c WHERE Underwriting__c=:objUnderWriting.id];
            System.debug('### lstDrawRequests==>:'+lstDrawRequests);
            // resend
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"id":"'+lstDrawRequests[0].id+'","resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            //resend no rec found
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"id":"'+opp.Id+'","resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Draw_Requests__c objDraw=new Draw_Requests__c();
            objDraw.id = lstDrawRequests[0].id;
            objDraw.Status__c ='Approved' ;
            update objDraw;
              //resend no rec found with req status
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"id":"'+lstDrawRequests[0].id+'","resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
           drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
           SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Draw_Requests__c objDraw2=new Draw_Requests__c();
            objDraw2.id = lstDrawRequests[0].id;
            objDraw2.Project_Complete_Certification__c=true;
            update objDraw2;
            // 
           drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
           SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            // 361
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":null,"amount": "100","isProjectComplete": false,"resendEmail":true}]}]}';
           SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
             // 356
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":2,"amount": "1000000","isProjectComplete": false,"resendEmail":true}]}]}';
           SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Test.StopTest();
        }
    }
     /**
*
* Description: This method is used to cover DrawRequestDataServiceImpl
*
*/   
    private testMethod static void drawRequestTest3(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c,All_HI_Required_Documents__c,Has_Open_Stip__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        system.debug('### Has_Open_Stip__c==>:'+opp.Has_Open_Stip__c);
        system.debug('### All_HI_Required_Documents__c==>:'+opp.All_HI_Required_Documents__c);
                       
        Map<String, String> userMap = new Map<String, String>();
        TriggerFlags__c trgDrawReq = new TriggerFlags__c();
        trgDrawReq.Name ='Draw_Requests__c';
        trgDrawReq.isActive__c =true;
       
        insert trgDrawReq;
        System.runAs(loggedInUsr[0])
        {
            String drawReqStr;
            Test.starttest();
            
            Underwriting_File__c objUnderWriting = [select id,Approved_for_Payments__c from Underwriting_File__c where opportunity__r.id=:opp.id limit 1];
            
            Underwriting_File__c objUnderWritUpd = new Underwriting_File__c();
            objUnderWritUpd.id = objUnderWriting.id;
            objUnderWritUpd.Approved_for_Payments__c = true;
            
            update objUnderWritUpd;
            
            
            Opportunity oppUpd = new Opportunity();
            oppUpd.id = opp.Id;
            oppUpd.Has_Open_Stip__c = 'true';
            update oppUpd;
            //343
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Opportunity oppUpd4 = new Opportunity();
            oppUpd4.id = opp.Id;
            oppUpd4.Has_Open_Stip__c = 'false';
            oppUpd4.All_HI_Required_Documents__c  = false;
            update oppUpd4;
            //347
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Opportunity oppUpd2 = new Opportunity();
            oppUpd2.id = opp.Id;
            oppUpd2.Has_Open_Stip__c = 'false';
            oppUpd2.Block_Draws__c = true;
            update oppUpd2;
            //344
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            
             Opportunity oppUpd3 = new Opportunity();
            oppUpd3.id = opp.Id;
            oppUpd3.Has_Open_Stip__c = 'false';
            oppUpd3.Block_Draws__c = false;
            oppUpd3.Credit_Decision_Date__c=date.today().addDays(-10);
            update oppUpd3;
            //346
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":null,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');

            Test.StopTest();
        }
    }
    
     /**
*
* Description: This method is used to cover DrawRequestDataServiceImpl
*
*/   
    private testMethod static void drawRequestTest4(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c,All_HI_Required_Documents__c,Credit_Decision_Date__c,Has_Open_Stip__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        system.debug('### Has_Open_Stip__c==>:'+opp.Has_Open_Stip__c);
        system.debug('### All_HI_Required_Documents__c==>:'+opp.All_HI_Required_Documents__c);
                       
        Map<String, String> userMap = new Map<String, String>();
        TriggerFlags__c trgDrawReq = new TriggerFlags__c();
        trgDrawReq.Name ='Draw_Requests__c';
        trgDrawReq.isActive__c =true;
       
        insert trgDrawReq;
        System.runAs(loggedInUsr[0])
        {
            String drawReqStr;
            Test.starttest();
            
            Underwriting_File__c objUnderWriting = [select id,Approved_for_Payments__c from Underwriting_File__c where opportunity__r.id=:opp.id limit 1];
            
            Opportunity oppUpd = new Opportunity();
            oppUpd.id = opp.Id;
            oppUpd.Combined_Loan_Amount__c = 200;
            oppUpd.Credit_Decision_Date__c = System.today()+2;
            oppUpd.All_HI_Required_Documents__c = true;
            update oppUpd;
            
            Underwriting_File__c objUnderWritUpd = new Underwriting_File__c();
            objUnderWritUpd.id = objUnderWriting.id;
            objUnderWritUpd.Approved_for_Payments__c = true;
            
            update objUnderWritUpd;
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            oppUpd.Combined_Loan_Amount__c = 10200;
            update oppUpd;
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000","isProjectComplete": true,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'drawrequest');
            
            Test.StopTest();
        }
    }
}