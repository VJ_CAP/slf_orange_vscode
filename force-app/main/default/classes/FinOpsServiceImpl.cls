/**
* Description: Opportunity Id Search related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suresh Kumar Yalla      08/01/2019          Created
******************************************************************************************/
public with sharing class FinOpsServiceImpl extends RESTServiceBase{ 
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){
        system.debug('### Entered into transformOutput() of '+FinOpsServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        system.debug('### reqData '+reqData);
        
        //List<UnifiedBean.UsersWrapper> userRecLst =  reqData.users;
        
        UnifiedBean responseBean = new UnifiedBean(); 
        //UnifiedBean.FinOpsReportWrapper finOpsReportWrapper = new UnifiedBean.FinOpsReportWrapper();
        //responseBean.finOpsReport = finOpsReportWrapper;
        responseBean.error = new list<UnifiedBean.errorWrapper>();
        String sBoxAccessToken = getBoxAccesstoken();
        //finOpsReportWrapper.boxAccessToken = getBoxAccesstoken();
        
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            List<user> userobj = new List<User>();
            List<Opportunity> opprLst;
             String opportunityId = '';
        String externalId;
        Savepoint sp = Database.setSavepoint();
        
        //UnifiedBean.UsersWrapper userRec = userRecLst!=null && !userRecLst.isEmpty() ? userRecLst[0] : null;
        try
        {
            if(reqData.startDate==null || reqData.startDate=='' || reqData.endDate==null || reqData.endDate=='' || reqData.reportType==null || reqData.reportType==''){
                getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                iRestRes = (IRestResponse)responseBean;
                return iRestRes;
            }
            
            //added by Adithya as part of 3443
            if(string.isBlank(reqData.projectCategory))
                reqData.projectCategory = 'Solar'; //If reqData.projectCategory is blank passing Solar as default value.
            
            String sReportType = reqData.reportType;
            String sStartDate = reqData.startDate;
            String sEndDate = reqData.endDate;
            Date dStartDate;
            Date dEndDate;
            try{
                dStartDate = Date.parse(sStartDate.substring(5,7)+'/'+sStartDate.substring(8,10)+'/'+sStartDate.substring(0,4));
                dEndDate = Date.parse(sEndDate.substring(5,7)+'/'+sEndDate.substring(8,10)+'/'+sEndDate.substring(0,4));
            }catch(Exception ex){
                getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('238',null),'238');
                iRestRes = (IRestResponse)responseBean;
                return iRestRes;
            }
                        
            if(dEndDate < dStartDate){
                getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('336',null),'336');
                iRestRes = (IRestResponse)responseBean;
                system.debug('### iRestRes '+iRestRes);
                return iRestRes;
            }
            if(dStartDate.daysBetween(dEndDate) > 45){
                getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('337',null),'337');
                iRestRes = (IRestResponse)responseBean;
                return iRestRes;
            }
            
            if(reqData.users == null){
                userobj = [SELECT Id, Username,UserRole.name,isInstallerReportEnabled__c,isNetReportEnabled__c,Is_Home_Remittance_Report_Enabled__c,isRemittanceReportEnabled__c,contact.Account.Name,contact.Account.FNI_Domain_Code__c FROM User where id=:userinfo.getuserId()];
            }
           
            system.debug('***userobj--'+userobj);
            if(userobj[0] != null){
                //if(UserObj[0].UserRole.name!=null && UserObj[0].UserRole.name!=''){
                    UnifiedBean.FinOpsReportWrapper finOpsReportWrapper = new UnifiedBean.FinOpsReportWrapper();
                    //finOpsReportWrapper.installerName = UserObj[0].contact.Account.Name;
                    finOpsReportWrapper.installerName = UserObj[0].contact.Account.FNI_Domain_Code__c;
                    //finOpsReportWrapper.boxAccessToken = sBoxAccessToken;

                    /*String userrolename = UserObj[0].UserRole.name;
                    if(userrolename.contains('Partner Executive')){
                        finOpsReportWrapper.isPartnerUser=true;
                        
                    }else{
                        finOpsReportWrapper.isPartnerUser=false;
                        getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('338',null),'338');
                        iRestRes = (IRestResponse)responseBean;
                        return iRestRes;
                    }*/
                if(sReportType.equalsIgnoreCase('Remittance')){
                    if(String.isBlank(reqData.projectCategory) || reqData.projectCategory=='Solar')
                    {
                        if(!userobj[0].isRemittanceReportEnabled__c){
                            getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('338',null),'338');
                            iRestRes = (IRestResponse)responseBean;
                            return iRestRes;
                        }
                    }
                    else if(reqData.projectCategory=='Home')
                    {
                         if(!userobj[0].Is_Home_Remittance_Report_Enabled__c){
                            getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('338',null),'338');
                            iRestRes = (IRestResponse)responseBean;
                            return iRestRes;
                        }   
                    }
                }else if(sReportType.equalsIgnoreCase('NetOut')){
                    if(!userobj[0].isNetReportEnabled__c){
                        getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('338',null),'338');
                        iRestRes = (IRestResponse)responseBean;
                        return iRestRes;
                    }
                }
                else if(sReportType.equalsIgnoreCase('Installer')){
                    if(!userobj[0].isInstallerReportEnabled__c){
                        getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('338',null),'338');
                        iRestRes = (IRestResponse)responseBean;
                        return iRestRes;
                    }
                }
                else{
                    getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                    iRestRes = (IRestResponse)responseBean;
                    return iRestRes;
                }
                    finOpsReportWrapper.boxAccessToken = sBoxAccessToken;
                    finOpsReportWrapper.remmitanceFolderId = SFSettings__c.getOrgDefaults().Remittance_Folder_Id__c;
                    finOpsReportWrapper.netOutFolderId = SFSettings__c.getOrgDefaults().Netout_Folder_Id__c;
                    finOpsReportWrapper.installerFolderId = SFSettings__c.getOrgDefaults().Installer_Folder_Id__c;
                    
                    //Updated By Adithya as part of 3443
                    //start
                    finOpsReportWrapper.homeRemittanceFolderId = SFSettings__c.getOrgDefaults().Home_Remittance_Folder_Id__c;
                    finOpsReportWrapper.projectCategory = reqData.projectCategory;
                    //End
                    
                    responseBean.finOpsReport = finOpsReportWrapper;
                    responseBean.returnCode = '200';
                    iRestRes = (IRestResponse)responseBean;
                    return iRestRes;
                //}
            }
            
        }catch(Exception err){
            Database.rollback(sp);
            errMsg = ErrorLogUtility.getErrorCodes('400', null);
            getErrorMsg(responseBean,errMsg,'400');
            iRestRes = (IRestResponse)responseBean;
            system.debug('### FinOpsServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('FinOpsServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }

        iRestRes = (IRestResponse)responseBean;
        system.debug('### Exit from transformOutput() of '+FinOpsServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        
        return iRestRes;
    }
    public void getErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        iolist = new list<DataValidator.InputObject>{};
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
    
    public String getBoxAccesstoken(){
        BoxPlatformApiConnection connection;
        String accessToken='';
        try
        {
            connection = BoxAuthentication.getConnection();
            System.debug('### Tracking: box connection: '+connection.accessToken);
        }
        catch (Exception e)
        {
            System.debug('### Error: Unable to authenticate with Box server: ' + e.getMessage());
        }
        
        return accessToken = (connection == null ? 'testaccesstokenonly' : connection.accessToken);
    }
}