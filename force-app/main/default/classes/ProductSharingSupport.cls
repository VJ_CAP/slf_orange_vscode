public without sharing class ProductSharingSupport 

//  this class contains methods called from triggers, so should be 'without sharing' to avoid unwanted access errors

{
    public ProductSharingSupport() 
    {
        //  nothing to do
    }

//  ******************************************************************

    public static void afterInsert (map<id,SObject> newMap)
    {
        set<Id> ids = new set<Id>{};

        for (SObject o : newMap.values())
        {
            if (o.get('Installer_Account__c') != null)
            {
                ids.add (o.Id);
            }
        }

        if (TransactionSupport.isNotProcessed('ProductSharingSupport.afterInsert') && ids.size() > 0) 
        {
            rebuildSharesForProducts (ids);
        }       
    }

//  ******************************************************************

    public static void afterUpdate (map<id,SObject> oldMap, map<id,SObject> newMap)
    {
        //  rebuild shares if the following occurs:

        //  1.  Installer_Account__c is updated
        //  2.  Owner is updated
        //  3.  Is_Active__c is updated
        //  4.  Internal_Use_Only__c is updated

        set<Id> ids = new set<Id>{};

        for (SObject o : newMap.values())
        {
            if (o.get('Installer_Account__c') != oldMap.get(o.Id).get('Installer_Account__c'))
            {
                ids.add (o.Id);
            }

            if (o.get('OwnerId') != oldMap.get(o.Id).get('OwnerId'))
            {
                ids.add (o.Id);
            }
            /*
            if (o.get('Is_Active__c') != oldMap.get(o.Id).get('Is_Active__c'))
            {
                ids.add (o.Id);
            } 

            if (o.get('Internal_Use_Only__c') != oldMap.get(o.Id).get('Internal_Use_Only__c'))
            {
                ids.add (o.Id);
            }
            */
        }
        system.debug('###ids '+ids); 
        if (TransactionSupport.isNotProcessed('ProductSharingSupport.afterUpdate')  && ids.size() > 0) 
        {
            rebuildSharesForProducts (ids); 
        }       
    }

//  ******************************************************************

    public static void rebuildSharesForProducts (set<Id> ids) 

    //  rebuilds shares for Products 

    {
        list<Product__c> recordsToProcess = new list<Product__c>{};

        recordsToProcess = [SELECT Id, Name, 
                                Installer_Account__c,
                                Is_Active__c,
                                Internal_Use_Only__c
                                FROM Product__c
                                WHERE Id IN :ids
                            ];

        set<Id> partnerAccountIds = new set<Id>{};

        for (Product__c p : recordsToProcess)
        {            
            if (p.Installer_Account__c != null) partnerAccountIds.add (p.Installer_Account__c);
        }

        map<String,Id> roleGroupsMap = PartnerSharingSupport.buildRoleGroupsMap (partnerAccountIds);
        system.debug('### roleGroupsMap '+roleGroupsMap);

        //  share maps

        map<String,SObject> sharesNeeded = new map<String,SObject>{};
        map<String,SObject> currentShares = new map<String,SObject>{};

        //  get all current Product shares, manual

        for (Product__share sh : [SELECT Id, ParentId, UserOrGroupId,
                                    AccessLevel
                                    FROM Product__share 
                                    WHERE ParentId IN :ids
                                    AND RowCause = 'Manual'])
        {
            currentShares.put (getProductShareKey (sh), sh);
        }

        for (Product__c p : recordsToProcess)
        {
            //  generate new shares for the Partner Accounts

            //if (p.Installer_Account__c != null  && p.Is_Active__c == true && p.Internal_Use_Only__c == false)
            if (p.Installer_Account__c != null  && p.Is_Active__c == true)
            {
                String key = p.Installer_Account__c + ':' +
                                    'Partner User' +
                                    ':Role';

                Id groupId = roleGroupsMap.get (key);

                if (groupId != null)
                {
                    Product__share sh = new Product__share (ParentId = p.Id, 
                                                        UserOrGroupId = groupId, 
                                                        AccessLevel = 'Read');
                    sharesNeeded.put (getProductShareKey (sh), sh);
                }
            }
        }
        PartnerSharingSupport.updateShares (sharesNeeded, currentShares);
    }

//******************************************************************

    private static String getProductShareKey (SObject sh)
    {
        return (String) sh.get ('ParentId') + '|' + 
                (String) sh.get ('UserOrGroupId') + '|' + 
                (String) sh.get ('AccessLevel');
    }
}