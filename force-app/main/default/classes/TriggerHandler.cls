/**
* Interface containing methods Trigger managers must implement to enforce best practice and bulkification of triggers.  
*
* @Author Salesforce
* @Date 19/11/2014
*/

public interface TriggerHandler {
    
    
    /** 
    * This method is called prior to execution of a BEFORE trigger. 
    * Use this to cache any data required into maps prior to execution of the trigger.
    * 
    * 
    * @param void
    */
    void bulkBefore();
    
    /**
    * This method is called prior to execution of an AFTER trigger. 
    * Use this to cache any data required into maps prior to execution of the trigger.                                                      
    * 
    * 
    * @param void
    */
   
    void bulkAfter();
    
    /**
    * Purpose: This method is called iteratively for each record to be inserted during 
    * a BEFORE trigger. Never execute any SOQL/SOSL operations in iterative method.
    * 
    * 
    * @param so - sObject
    */
    
    void beforeInsert(List<SObject> so);
    
    /**
    * This method is called iteratively for each record to be updated during 
    * a BEFORE trigger. Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSo - SObject
    * @param so - SObject                                                          
    */
   
    void beforeUpdate(map<id,SObject> oldMap, map<id,SObject> newMap);
    
    /**
    * This method is called iteratively for each record to be deleted during 
    * a BEFORE trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param so - SObject
    *                                                         
    */
    
    void beforeDelete(map<id,SObject> oldMap);
    
    /**
    *
    * This method is called iteratively for each record inserted during an AFTER
    * trigger. Always put field validation in the 'After' methods in case another trigger
    * has modified any values. The record is 'read only' by this point.  
    * Never execute any SOQL/SOSL operations in iterative method.
    *
    * @param so - SObject
    */
    
    void afterInsert(map<id,SObject> newMap);
 
    /**
    *
    * This method is called iteratively for each record updated during an 
    * AFTER trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSo - SObject
    * @param so - SObject
    */
     
    void afterUpdate(map<id,SObject> oldMap, map<id,SObject> newMap);
    
    /**
    * This method is called iteratively for each record deleted during an 
    * AFTER trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param so - SObject
    */
     
    void afterDelete(map<id,SObject> oldMap);
    
    /**
    *
    * This method is called once any record is Undeleted.                                                 
    *
    * @param so - SObject 
    *
    */
    
    void afterUndelete(map<id,SObject> newMap);
    
    /**
    * This method is called once all records have been processed by the trigger.
    * Use this method to accomplish any final operations such as creation or updates of other records.   
    *
    * @param - void                                               
    */
    
    void andFinally();

}