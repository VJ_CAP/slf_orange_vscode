/**
 * Test class for batchrebuildSharesForOppsModified
 */
@isTest()
public class batchrebuildSharesForOppsModifiedTest {

    static testMethod void updateOppsAccsShares() {
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        insert trgLst; 
        
        //create new Account
        Account testAccount = new Account();
        testAccount.FirstName = 'TestFN';
        testAccount.LastName = 'TestLN';
        testAccount.SSN__c = '666-66-6666';
        testAccount.Marital_Status__c = 'Unmarried';
        testAccount.Phone = '4344435555';
        testAccount.PersonEmail = 'test@test.com';
        testAccount.ShippingPostalCode = '12345';
        testAccount.BillingPostalCode = '12345';
        testAccount.PersonBirthdate = date.valueOf('1968-09-21');
        testAccount.Installer_Legal_Name__c='Bright planet Solar';
        

        insert testAccount;    
        System.debug('***Debug: ispersonaccount?: ' + testAccount.IsPersonAccount);

        //create new installer account
        Account testInstallerAccount = new Account();
        testInstallerAccount.Name = 'testInstaller';
        testInstallerAccount.Installer_Legal_Name__c='Bright planet Solar';
        testInstallerAccount.Solar_Enabled__c = true;
        insert testInstallerAccount;
        
        Contact testInstallerContact = new Contact();
        testInstallerContact.FirstName = 'fn';
        testInstallerContact.LastName = 'ln';
        testInstallerContact.Email = 'test@leancog.com';
        testInstallerContact.AccountId = testInstallerAccount.Id;
        
        insert testInstallerContact;
        
        //create new product
        Product__c testProd = new Product__c();
        testProd.Sighten_Product_UUID__c = '1';
        testProd.State_Code__c = 'CA';
        testProd.Installer_Account__c = testInstallerAccount.Id;
        testProd.FNI_Min_Response_Code__c = 2;
        testProd.Qualification_Message__c = 'you are prequaled';
        testProd.Product_Display_Name__c = 'testprod';
        testProd.Is_Active__c = true;
        testProd.Product_Tier__c = '0';
        insert testProd;
        
        //create new opportunity
        Opportunity testOppty = new Opportunity();
        testOppty.AccountId = testAccount.Id;
        testOppty.Co_Applicant__c = testAccount.Id;
        testOppty.Name = 'testOppty';
        testOppty.StageName = 'Qualified';
        testOppty.CloseDate = Date.today();
        testOppty.Install_City__c = 'Oakland';
        testOppty.Install_State_Code__c = 'CA';
        testOppty.Install_Postal_Code__c = '94611';
        testOppty.Install_Street__c = '1900 Magellan Dr';
        testOppty.Installer_Account__c = testInstallerAccount.Id;
        testOppty.Sighten_Product_UUID__c = '1';
        testOppty.Sales_Representative_Email__c = 'test@leancog.com';
        testOppty.Create_in_Sighten__c = true;
        testOppty.Combined_Loan_Amount__c = 240;
        
        insert testOppty;

        Test.StartTest();

        batchrebuildSharesForOppsModified oppsUpdate = new batchrebuildSharesForOppsModified();
        
        Database.executeBatch(oppsUpdate, 20);
        
        Test.StopTest();   
        System.AssertEquals(database.countquery('SELECT COUNT()'+' FROM Opportunity '), 1);
    }
}