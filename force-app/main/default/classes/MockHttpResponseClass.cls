@isTest
global class MockHttpResponseClass implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"data": [{"organization_name": "Tesla", "site_id": "842406ea-4e09-4c57-8252-d9304aae6fa7", "milestone_status": ["Pending Primary Approval"], "milestone_date_submitted": ["2019-01-21T10:33:11.451675+00:00"], "milestone_id": ["032689ec-cd4f-4f8a-be2d-53faa764c45a"], "milestone_status_abbreviated": ["PEN1"], "site_last_updated": "2019-01-21T10:33:11.764085+00:00", "milestone_name": ["Notice to Proceed"], "milestone_submitted_by_name": ["Channel Manager Admin"]}], "count": 1, "pages": 1, "status_code": 200, "messages": {"warning": [], "info": [], "error": [], "critical": []}, "has_next_page": false} ');
        res.setStatusCode(200);
        return res;
    }
}