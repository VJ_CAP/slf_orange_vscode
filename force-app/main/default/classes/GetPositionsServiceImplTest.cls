/******************************************************************************************
* Description: Test class to cover GetPositionsServiceImpl services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu                03/11/2019              Created
******************************************************************************************/
@isTest
public class GetPositionsServiceImplTest{
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
    }
    
    /**
*
* Description: This method is used to cover GetPositionsServiceImpl
*
*/   
    private testMethod static void getPositionsTest(){
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email from User where Email =: 'testc@mail.com'];
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name from opportunity where AccountId =: acc.Id LIMIT 1]; 
        List<Position__c> posList = new List<Position__c>();
        Position__c p1 = new Position__c();
        p1.Job_Title__c = 'ACCOUNT EXECUTIVE';
        posList.add(p1);
        insert posList;
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            String getPositions;
            Test.starttest();
            
            getPositions = '{"projects":[{"id":""}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,getPositions,'getpositions');
            
            getPositions = '{}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,getPositions,'getpositions');
            Test.StopTest();
        }
    }
    private testMethod static void getPositionsTest1(){
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email from User where Email =: 'testc@mail.com'];
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name from opportunity where AccountId =: acc.Id LIMIT 1];
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            String getPositions;
            Test.starttest();
            
            getPositions = '{"projects":[{"id":""}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,getPositions,'getpositions');
            GetPositionsServiceImpl getP = new GetPositionsServiceImpl();
            UnifiedBean ub = new UnifiedBean();
            ub.error = new List<UnifiedBean.errorWrapper>();
            getP.getErrorMsg(ub,'String','200');
            SLFRestDispatchTest.MapWebServiceURI(userMap,null,'getpositions');
            Test.StopTest();
        } 
    }
}