public class fni_Credit_Extension {
   private ApexPages.StandardController stdController;
    private Id creditId;
//    public String redirectUrl {public get; private set;}
    //public String redirectUrlSF1 {public get; private set;}
    public String redirectUrlSF1App {public get; private set;}    
//    public String message {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}

    public fni_Credit_Extension(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.creditId = stdController.getRecord().Id;
        shouldRedirect = false;
    }

  /*  public PageReference callFNI() {
        String res = fniCalloutManual.buildRequest(this.creditId);
        message = String.escapeSingleQuotes(res);
        shouldRedirect = true;
        //redirectUrl = stdController.view().getUrl();
        redirectUrlSF1App = this.creditId;
        //redirectUrlSF1 = 'salesforce1://sObject/' + this.LeadId + '/view';
        return null;
    }*/
    
    public PageReference creditConsent(){
        Credit__c thisCredit = [Select Id, Customer_has_authorized_credit_soft_pull__c FROM Credit__c WHERE Id =:this.creditId];
        thisCredit.Customer_has_authorized_credit_soft_pull__c = true;
        update thisCredit;
        shouldRedirect = true;
        redirectUrlSF1App = this.creditId;
        //this.stdController.save();
        return null;
    }
    
    public void save(){
        this.stdController.save();
    }
    
}