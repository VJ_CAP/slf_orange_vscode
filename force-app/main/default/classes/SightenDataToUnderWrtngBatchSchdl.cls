/**
* Description: Import milestone requests from Sighten into Salesforce using batch.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Deepika               21/20/2018             Created
******************************************************************************************/
global class SightenDataToUnderWrtngBatchSchdl implements Database.Batchable<sObject>,Schedulable,Database.Stateful,Database.AllowsCallouts {
    global String query;
    global Map<string,ImportMilestoneSightenResponse.Data> uuidMap = new Map<string,ImportMilestoneSightenResponse.Data>();
    global Set<String> uuidSet = new Set<String>();
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String baseURL;
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        if(!settings.IS_PRODUCTION__c){
            baseURL = settings.Sandbox_URL_Prefix__c;
        } else {
            baseURL = settings.Production_URL_Prefix__c;
        }
        
        DateTime currentDate = DateTime.Now().AddDays(-1);
        String convertedDate = currentDate.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setTimeout(120000);
        String endpoint = baseURL+'/api/data/quote/?metrics=milestone_submitted_by_name,milestone_id,site_id,site_last_updated,milestone_date_submitted,milestone_name,milestone_status,milestone_status_abbreviated,organization_name&quote_status=SIGN&milestone_date_submitted_after='+convertedDate+'&milestone_status_abbreviated=%22PEN1%22&limit=5000';
        
        system.debug('### endpoint URL - '+endpoint);
        
        request.setHeader('Authorization', 'Token '+settings.SIGHTEN_TOKEN__c);
        request.setHeader('Content-Type', 'application/json');
        
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        
        
        HttpResponse response = http.send(request);
        String responseBody = response.getBody();
        system.debug('### responseBody  - '+responseBody);
        
        if(response.getStatusCode() == 200)
        {
            ImportMilestoneSightenResponse  sightenResponseOppty = ImportMilestoneSightenResponse.parse(responseBody);
            system.debug('### sightenResponseOppty  - '+sightenResponseOppty);
            
            if(null != sightenResponseOppty.data)
            {
                for(ImportMilestoneSightenResponse.Data sightenDataIterate: sightenResponseOppty.data){
                    uuidMap.put(sightenDataIterate.site_id,sightenDataIterate);
                    uuidSet.add(sightenDataIterate.site_id);
                }
            }
            system.debug('### uuidMap  - '+uuidMap);
        }
        query = 'Select id,Name,EDW_Originated__c,Sighten_UUID__c,(select Id,Opportunity__c,M0_Approval_Requested_Date__c,M1_Approval_Requested_Date__c,M2_Approval_Requested_Date__c from Underwriting_Files__r) from Opportunity where Sighten_UUID__c IN: uuidSet';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> oppList){
        system.debug('### Entered into execute() of '+ SightenDataToUnderWrtngBatchSchdl.class); 
        system.debug('### oppList: '+ oppList); 
        Map<Id,Underwriting_File__c> undrwrtngMap = new Map<Id,Underwriting_File__c>();
        for(Opportunity oppObj : oppList){
            if(uuidMap.keyset().contains(oppObj.Sighten_UUID__c) && uuidMap.get(oppObj.Sighten_UUID__c) != null){
                ImportMilestoneSightenResponse.Data uuidData = new ImportMilestoneSightenResponse.Data();
                uuidData = uuidMap.get(oppObj.Sighten_UUID__c);
                if(!oppObj.Underwriting_Files__r.isEmpty()){
                    for(Underwriting_File__c undrwrtngObj : oppObj.Underwriting_Files__r){
                        if(!uuidData.milestone_name.isEmpty()){
                            for(Integer i = 0; i < uuidData.milestone_name.size(); i++){
                                //Ravi: Adding this if condition as part of new field added by Sighten
                                if(!(uuidData.milestone_submitted_by_name[i].equalsIgnoreCase('Business Processing Login') && (uuidData.milestone_name[i].equalsIgnoreCase('Conditional Approval') || uuidData.milestone_name[i].equalsIgnoreCase('Final Approval')) && (uuidData.organization_name.equalsIgnoreCase('Solar City') || uuidData.organization_name.equalsIgnoreCase('Tesla')))){
                                    if(uuidData.milestone_name[i] == 'Initial Approval' || uuidData.milestone_name[i] == 'Notice to Proceed'){
                                        if(undrwrtngObj.M0_Approval_Requested_Date__c == null){
                                            if(!uuidData.milestone_date_submitted.isEmpty()){
                                                if(uuidData.milestone_date_submitted[i].contains('T')){
                                                    if((uuidData.milestone_date_submitted[i].split('T'))[i] != null){
                                                        System.debug('date1:'+(uuidData.milestone_date_submitted[i].split('T')));
                                                        System.debug('date1:'+(uuidData.milestone_date_submitted[i].split('T'))[0]);
                                                        //System.debug('date1:'+(uuidData.milestone_date_submitted[i].split('T')).format('MM/dd/yyyy HH:mm:ss','America/New_York'));
                                                        List<String> submitdDate = new List<String>();
                                                        if((uuidData.milestone_date_submitted[i].split('T')[0]).contains('-'))
                                                            submitdDate = (uuidData.milestone_date_submitted[i].split('T')[0]).split('-');
                                                        List<String> submitdTime = new List<String>();
                                                        if((uuidData.milestone_date_submitted[i].split('T')[1]).contains(':'))
                                                            submitdTime = (uuidData.milestone_date_submitted[i].split('T')[1]).split(':');
                                                        if(submitdDate.size() > 0 && !(submitdDate.size() < 3) && submitdTime.size() > 0 && !(submitdTime.size() < 3)){
                                                            DateTime dtGmt = DateTime.newInstance(Integer.valueOf(submitdDate[0]), Integer.valueOf(submitdDate[1]), Integer.valueOf(submitdDate[2]), Integer.valueOf(submitdTime[0]), Integer.valueOf(submitdTime[1]), Integer.valueOf(submitdTime[2].substring(0, 1)));
                                                            
                                                            undrwrtngObj.M0_Approval_Requested_Date__c = dtGmt.addHours(-5);
                                                            System.debug('undrwrtngObj.M0_Approval_Requested_Date__c:'+undrwrtngObj.M0_Approval_Requested_Date__c);
                                                        }
                                                        undrwrtngMap.put(undrwrtngObj.Id,undrwrtngObj);
                                                    }
                                                }
                                            }
                                        }
                                    }else if(uuidData.milestone_name[i] == 'Conditional Approval'){
                                        if(undrwrtngObj.M1_Approval_Requested_Date__c == null){
                                            if(!uuidData.milestone_date_submitted.isEmpty()){
                                                if(uuidData.milestone_date_submitted[i].contains('T')){
                                                    if((uuidData.milestone_date_submitted[i].split('T'))[i] != null){
                                                        System.debug('date2:'+(uuidData.milestone_date_submitted[i].split('T')));
                                                        System.debug('date2:'+(uuidData.milestone_date_submitted[i].split('T'))[0]);
                                                        List<String> submitdDate = new List<String>();
                                                        if((uuidData.milestone_date_submitted[i].split('T')[0]).contains('-'))
                                                            submitdDate = (uuidData.milestone_date_submitted[i].split('T')[0]).split('-');
                                                        List<String> submitdTime = new List<String>();
                                                        if((uuidData.milestone_date_submitted[i].split('T')[1]).contains(':'))
                                                            submitdTime = (uuidData.milestone_date_submitted[i].split('T')[1]).split(':');
                                                        if(submitdDate.size() > 0 && !(submitdDate.size() < 3) && submitdTime.size() > 0 && !(submitdTime.size() < 3)){
                                                            DateTime dtGmt = DateTime.newInstance(Integer.valueOf(submitdDate[0]), Integer.valueOf(submitdDate[1]), Integer.valueOf(submitdDate[2]), Integer.valueOf(submitdTime[0]), Integer.valueOf(submitdTime[1]), Integer.valueOf(submitdTime[2].substring(0, 1)));
                                                            
                                                            undrwrtngObj.M1_Approval_Requested_Date__c = dtGmt.addHours(-5);
                                                            System.debug('undrwrtngObj.M1_Approval_Requested_Date__c:'+undrwrtngObj.M1_Approval_Requested_Date__c);
                                                        }
                                                        undrwrtngMap.put(undrwrtngObj.Id,undrwrtngObj);
                                                    }
                                                }
                                            }
                                        }
                                    }else if(uuidData.milestone_name[i] == 'Final Approval' || uuidData.milestone_name[i] == 'Final Approval - Dual Approval'){
                                        system.debug('### into FinalApproval');
                                        if(undrwrtngObj.M2_Approval_Requested_Date__c == null){
                                            system.debug('### into FinalApproval 1');
                                            if(!uuidData.milestone_date_submitted.isEmpty()){
                                                system.debug('### into FinalApproval 2');
                                                if(uuidData.milestone_date_submitted[i].contains('T')){
                                                    if((uuidData.milestone_date_submitted[i].split('T'))[i] != null){
                                                        System.debug('### date3:'+(uuidData.milestone_date_submitted[i].split('T')));
                                                        System.debug('### date3:'+(uuidData.milestone_date_submitted[i].split('T'))[0]);
                                                        List<String> submitdDate = new List<String>();
                                                        if((uuidData.milestone_date_submitted[i].split('T')[0]).contains('-'))
                                                            submitdDate = (uuidData.milestone_date_submitted[i].split('T')[0]).split('-');
                                                        List<String> submitdTime = new List<String>();
                                                        if((uuidData.milestone_date_submitted[i].split('T')[1]).contains(':'))
                                                            submitdTime = (uuidData.milestone_date_submitted[i].split('T')[1]).split(':');
                                                        if(submitdDate.size() > 0 && !(submitdDate.size() < 3) && submitdTime.size() > 0 && !(submitdTime.size() < 3)){
                                                            DateTime dtGmt = DateTime.newInstance(Integer.valueOf(submitdDate[0]), Integer.valueOf(submitdDate[1]), Integer.valueOf(submitdDate[2]), Integer.valueOf(submitdTime[0]), Integer.valueOf(submitdTime[1]), Integer.valueOf(submitdTime[2].substring(0, 1)));
                                                            
                                                            undrwrtngObj.M2_Approval_Requested_Date__c = dtGmt.addHours(-5);
                                                            System.debug('### undrwrtngObj.M2_Approval_Requested_Date__c:'+undrwrtngObj.M2_Approval_Requested_Date__c);
                                                        }
                                                        undrwrtngMap.put(undrwrtngObj.Id,undrwrtngObj);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(!undrwrtngMap.isEmpty())
            Database.update(undrwrtngMap.values(), false);
        system.debug('### Exit into execute() of '+ SightenDataToUnderWrtngBatchSchdl.class);
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    global void execute(SchedulableContext sc) {
        SightenDataToUnderWrtngBatchSchdl batchObj = new SightenDataToUnderWrtngBatchSchdl(); 
        database.executebatch(batchObj,1);
    }
}