@isTest
public class FetchProjectsDataServiceImplTest {    
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
        
    }
    
    private testMethod static void fetchProjectsTest(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1];
        Underwriting_File__c uwFile = [select id,Opportunity__r.Change_Order_Status__c,CO_Approval_Date__c,Opportunity__r.Installer_Account__r.M0_Required__c, M0_Approval_Date__c, M1_Approval_Date__c, M0_Approval_Requested_Date__c, M2_Approval_Date__c from Underwriting_File__c where Opportunity__c =: opp.Id LIMIT 1];
        Quote quote = [select Id,SLF_Product__c  from Quote where OpportunityId =: opp.Id LIMIT 1];
        Product__c prod = [select Id from Product__c where Id =: quote.SLF_Product__c LIMIT 1];
        prod.Expiration_Date__c = system.today();
        prod.is_Active__c = false;
        update prod;
        
        Test.startTest();
        Map<String, String> fetchProjectsURIMap = new Map<String, String>();
        
        string projectJsonreq = '';
        opp.Project_Category__c = 'Home';
        opp.Credit_Expiration_Date__c = system.today() + 30;
        update opp;
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 10"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.Project_Category__c = 'Solar';
        opp.Credit_Expiration_Date__c = system.today() + 30;
        update opp;
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 10"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","riskCategory":["PTO Exp"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "search", "pageNumber": "2","numberOfRecords": "200","riskCategory":["PTO Exp"],"stage":"Closed Won","projectStatus":"Project Withdrawn","projectStatusDetail":"M0 - Documents Needed"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "", "pageNumber": "1","numberOfRecords": "200","riskCategory":["PTO Exp"],"projectStatusDetail":"M0 - Documents Needed"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard","pageNumber": "1","numberOfRecords": "200","searchKey": "","stage": [  "New",  "Credit Approved",  "Credit Declined",  "Credit Pending Review","Closed Won"],"hasOpenStip":[  "TRUE",  "FALSE"  ],"projectStatusDetail":["",  "M0 - Documents Needed",  "M0 - Need Approval Request",  "M0 - In Review",  "M1 - Documents Needed",  "M1 - Need Approval Request",  "M1 - In Review",  "M2 - Documents Needed",  "M2 - Need Approval Request",  "M2 - In Review"  ],"projectStatus": ["",  "M0","M1","M2","Pending Loan Docs",  "Project Completed",  "M2 Payment Pending",  "Declined","Project Withdrawn"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard","pageNumber": "1","numberOfRecords": "200","searchKey": "","stage": ["Credit Declined LT7"],"hasOpenStip":[  "TRUE",  "FALSE"  ],"projectStatusDetail":["",  "M0 - Documents Needed",  "M0 - Need Approval Request",  "M0 - In Review",  "M1 - Documents Needed",  "M1 - Need Approval Request",  "M1 - In Review",  "M2 - Documents Needed",  "M2 - Need Approval Request",  "M2 - In Review"  ],"projectStatus": ["",  "M0","M1","M2","Pending Loan Docs",  "Project Completed",  "M2 Payment Pending",  "Declined","Project Withdrawn"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard","pageNumber": "1","numberOfRecords": "200","searchKey": "","stage": ["Credit Declined LT30"],"hasOpenStip":[  "TRUE",  "FALSE"  ],"projectStatusDetail":["",  "M0 - Documents Needed",  "M0 - Need Approval Request",  "M0 - In Review",  "M1 - Documents Needed",  "M1 - Need Approval Request",  "M1 - In Review",  "M2 - Documents Needed",  "M2 - Need Approval Request",  "M2 - In Review"  ],"projectStatus": ["",  "M0","M1","M2","Pending Loan Docs",  "Project Completed",  "M2 Payment Pending",  "Declined","Project Withdrawn"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        List<SLF_Credit__c> creds = [select Id from SLF_Credit__c where Opportunity__c =: opp.Id];
        for(SLF_Credit__c cred:creds)  {
            delete cred;
        }
        projectJsonreq = '{"filter": "search", "pageNumber": "1","numberOfRecords": "200","searchKey": "'+opp.id+'"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Home","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 10"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');

        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Home","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Home","pageNumber": "1","numberOfRecords": "200","riskCategory":["PTO Exp"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        prod.Expiration_Date__c = system.today() -1;
        update prod;
        projectJsonreq = '{"filter": "search", "pageNumber": "1","numberOfRecords": "200","searchKey": "'+opp.id+'"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.Project_Category__c = 'Solar';
        opp.Credit_Expiration_Date__c = system.today() + 30;
        uwFile.Opportunity__r.Change_Order_Status__c = 'Active';
        uwFile.M0_Approval_Date__c = system.today();
        uwFile.M1_Approval_Date__c = null;
        uwFile.M0_Approval_Requested_Date__c = system.today();
        uwFile.Opportunity__r.Installer_Account__r.M0_Required__c = 'Install Contracts';
        update opp;
        update uwFile;
        
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Solar","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.M1_Approval_Date__c = system.today() -10;
        update uwFile;
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Solar","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30","PTO Exp"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.M1_Approval_Date__c = system.today() -10;
        uwFile.M2_Approval_Date__c = null;
        update uwFile;
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Solar","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30","PTO Exp"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        //To cover exception block
        FetchProjectsDataServiceImpl fetchProjectsDataSrvImpl = new FetchProjectsDataServiceImpl();
        fetchProjectsDataSrvImpl.transformOutput(null);
        
        Test.stopTest();
        
    }
    private testMethod static void fetchProjectsTest1(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1];
        Underwriting_File__c uwFile = [select id,Opportunity__r.Change_Order_Status__c,CO_Approval_Date__c,Opportunity__r.Installer_Account__r.M0_Required__c, M0_Approval_Date__c, M1_Approval_Date__c, M0_Approval_Requested_Date__c, M2_Approval_Date__c from Underwriting_File__c where Opportunity__c =: opp.Id LIMIT 1];
        Quote quote = [select Id,SLF_Product__c  from Quote where OpportunityId =: opp.Id LIMIT 1];
        Product__c prod = [select Id from Product__c where Id =: quote.SLF_Product__c LIMIT 1];
        prod.Expiration_Date__c = system.today();
        prod.is_Active__c = false;
        update prod;
        
        Test.startTest();
        Map<String, String> fetchProjectsURIMap = new Map<String, String>();
        
        string projectJsonreq = '';
        
        projectJsonreq = '{"filter": "search", "pageNumber": "1","numberOfRecords": "200","searchKey": "'+opp.id+'"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","changeOrderStatus": ["Active"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","changeOrderStatus": ["Pending"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","changeOrderStatus": ["Active - Docs Needed"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","changeOrderStatus": ["Active - Need Approval Request"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","changeOrderStatus": ["Active - In Review"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","changeOrderStatus": [""]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","hasOpenStip":["TRUE"], "hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{etstttt :"filter": "", "pageNumber": "1","numberOfRecords": "200","searchKey": ""}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        List<SLF_Credit__c> creds = [select Id from SLF_Credit__c where Opportunity__c =: opp.Id];
        for(SLF_Credit__c cred:creds)  {
            delete cred;
        }
        projectJsonreq = '{"filter": "search", "pageNumber": "1","numberOfRecords": "200","searchKey": "'+opp.id+'"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');

        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Home","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 10"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');

        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Home","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Home","pageNumber": "1","numberOfRecords": "200","riskCategory":["PTO Exp"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        prod.Expiration_Date__c = system.today() -1;
        update prod;
        projectJsonreq = '{"filter": "search", "pageNumber": "1","numberOfRecords": "200","searchKey": "'+opp.id+'"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.Project_Category__c = 'Solar';
        opp.Credit_Expiration_Date__c = system.today() + 30;
        uwFile.Opportunity__r.Change_Order_Status__c = 'Active';
        uwFile.M0_Approval_Date__c = system.today();
        uwFile.M1_Approval_Date__c = null;
        uwFile.M0_Approval_Requested_Date__c = system.today();
        uwFile.Opportunity__r.Installer_Account__r.M0_Required__c = 'Install Contracts';
        update opp;
        update uwFile;
        
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Solar","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 30"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');

        opp.Credit_Expiration_Date__c = system.today() + 10;
        update opp;
        
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Solar","pageNumber": "1","numberOfRecords": "200","riskCategory":["Credit Exp 10"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.M1_Approval_Date__c = system.today() -10;
        uwFile.M2_Approval_Date__c = null;
        update uwFile;
         
        projectJsonreq = '{"filter": "search","searchKey": "'+opp.id+'","projectCategory":"Solar","pageNumber": "1","numberOfRecords": "200","riskCategory":["PTO Exp"],"hasOpenStip":["TRUE"],"hasInstallerOpenStip":["TRUE"],"hasCustomerInstallerOpenStip":["TRUE"]}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');       
        //To cover exception block
        FetchProjectsDataServiceImpl fetchProjectsDataSrvImpl = new FetchProjectsDataServiceImpl();
        fetchProjectsDataSrvImpl.transformOutput(null);
        
        Test.stopTest();
        
    }
    
    private testMethod static void fetchProjectsHomeTest(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1];
        Quote quote = [select Id,SLF_Product__c  from Quote where OpportunityId =: opp.Id LIMIT 1];
        Underwriting_File__c uwFile = [select id from Underwriting_File__c where Opportunity__c =: opp.Id LIMIT 1];
        Product__c prod = [select Id from Product__c where Id =: quote.SLF_Product__c LIMIT 1];
        prod.Expiration_Date__c = system.today();
        prod.is_Active__c = false;
        update prod;
        
        Test.startTest();
        Map<String, String> fetchProjectsURIMap = new Map<String, String>();
        
        string projectJsonreq = '';
        opp.Project_Category__c = 'Home';
        opp.StageName = 'Loan agreement sent';
        opp.Has_Open_Stip__c = 'True';
        update opp;
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'Closed Won';
        opp.Has_Open_Stip__c = 'True';
        update opp;
        uwFile.Project_Status__c = 'Payment Stage';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.Project_Status__c = 'Loan Agreement Review';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.Project_Status__c = 'Documents Needed';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.Has_Open_Stip__c = 'False';
        update opp;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.Project_Status__c = 'Payment Stage';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.Project_Status_Detail__c = 'Payment Requested';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.Project_Status_Detail__c = 'Change Order Requested';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.Project_Status__c = 'Loan Agreement Review';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        uwFile.Project_Status_Detail__c = null;
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        //To cover exception block
        FetchProjectsDataServiceImpl fetchProjectsDataSrvImpl = new FetchProjectsDataServiceImpl();
        fetchProjectsDataSrvImpl.transformOutput(null);
        
        Test.stopTest();
        
    }
    
    private testMethod static void fetchProjectsHomeTest2(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1];
        Quote quote = [select Id,SLF_Product__c  from Quote where OpportunityId =: opp.Id LIMIT 1];
        Underwriting_File__c uwFile = [select id,Opportunity__r.Change_Order_Status__c,CO_Approval_Date__c,Opportunity__r.Installer_Account__r.M0_Required__c from Underwriting_File__c where Opportunity__c =: opp.Id LIMIT 1];
        Product__c prod = [select Id from Product__c where Id =: quote.SLF_Product__c LIMIT 1];
        Box_Fields__c boxfields = [select id,Install_Contract_Received__c  from Box_Fields__c where Underwriting__c =: uwFile.Id LIMIT 1];
        prod.Expiration_Date__c = system.today();
        prod.is_Active__c = false;
        update prod;
        
        Test.startTest();
        Map<String, String> fetchProjectsURIMap = new Map<String, String>();
        
        string projectJsonreq = '';
        opp.Project_Category__c = 'Home';
        opp.StageName = 'Closed Won';
        opp.Has_Open_Stip__c = 'False';
        update opp;
        
        uwFile.Project_Status__c = 'Project Completed';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        // uwFile.Project_Status__c = 'Project Withdrawn';
        // uwFile.Withdraw_Reason__c = 'Customer Request';
        // update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'New';
        update opp;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'Credit Declined LT30';
        update opp;
        uwFile.Project_Status__c = null;
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'Credit Declined LT7';
        update opp;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'New';
        update opp;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        //To cover exception block
        FetchProjectsDataServiceImpl fetchProjectsDataSrvImpl = new FetchProjectsDataServiceImpl();
        fetchProjectsDataSrvImpl.transformOutput(null);
        
        Test.stopTest();
        
    }
    
    private testMethod static void fetchProjectsHomeTest3(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1];
        Quote quote = [select Id,SLF_Product__c  from Quote where OpportunityId =: opp.Id LIMIT 1];
        Underwriting_File__c uwFile = [select id,Opportunity__r.Change_Order_Status__c,CO_Approval_Date__c,Opportunity__r.Installer_Account__r.M0_Required__c from Underwriting_File__c where Opportunity__c =: opp.Id LIMIT 1];
        Product__c prod = [select Id from Product__c where Id =: quote.SLF_Product__c LIMIT 1];
        Box_Fields__c boxfields = [select id,Install_Contract_Received__c  from Box_Fields__c where Underwriting__c =: uwFile.Id LIMIT 1];
        prod.Expiration_Date__c = system.today();
        prod.is_Active__c = false;
        update prod;
        
        Test.startTest();
        Map<String, String> fetchProjectsURIMap = new Map<String, String>();
        
        string projectJsonreq = '';
        
        uwFile.Project_Status__c = 'Documents Needed';
        update uwFile;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'Credit Approved';
        update opp;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'Credit Pending Review';
        update opp;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'Credit Expired';
        update opp;
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Solar"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');
        
        opp.StageName = 'Closed Won';
        uwFile.Opportunity__r.Change_Order_Status__c = 'Active';
        uwFile.CO_Approval_Date__c = null;
        boxfields.Install_Contract_Received__c = null;
        uwFile.Opportunity__r.Installer_Account__r.M0_Required__c = 'Install Contracts'; 
        
        update opp;
        update uwFile;
        update boxfields;
        
        projectJsonreq = '{"filter": "dashboard", "pageNumber": "1","numberOfRecords": "200","projectCategory": "Home"}';
        MapWebServiceURI(fetchProjectsURIMap,projectJsonreq,'fetchprojects');   
        
         //To cover exception block
        FetchProjectsDataServiceImpl fetchProjectsDataSrvImpl = new FetchProjectsDataServiceImpl();
        fetchProjectsDataSrvImpl.transformOutput(null);
        
        Test.stopTest();
        
    }
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}