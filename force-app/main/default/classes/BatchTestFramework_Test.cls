@isTest
private class BatchTestFramework_Test 
{   
    @isTest static void test_method_one() 
    {
        SLFUtilityTest.createCustomSettings();
        
        Account a1 = new Account(Name = 'Test Account 1',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true);
        insert a1;

        JobSupport.submitJob('BatchTestFramework', '{"numberOfAccounts":10}');
    }
}