/**
* Description: Response returned for each operation is wrapped into a single object.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

global class ResponseWrapper { 

    public IRestResponse response{get; set;}
    
    /**
    * Description: Default constructor to initialize ResponseData
    */
    public ResponseWrapper(){
        //response = '';
    }
}