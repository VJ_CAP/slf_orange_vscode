@isTest(seeAllData=false)
public class FinOpsServiceImplTest {
    @testsetup static void createtestdata(){
        //SLFUtilityDataSetup.initData();   
        insertUserData();  
    }
    
    public static void insertUserData()    {
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        insert trgLst; 
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
    }
    
    /**
*
* Description: This method is used to cover PaymentApprovalServiceImpl
*
*/
    private testMethod static void finOpsReportTest(){
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            Test.starttest();
            
            String req;
            req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"Remittance"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            
            req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"NetOut"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            
            req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"Installer"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            
            req = '{"startDate":"2018-12-28","endDate":"2019-02-10","reportType":"Remittance","projectCategory":"Home"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            
            req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"Remittance1"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            
            //null scenario
            req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":""}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            
            // if(dEndDate < dStartDate)
            req = '{"startDate":"2019-03-10","endDate":"2019-01-10","reportType":"Remittance"}';
            
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            
            // if(dStartDate.daysBetween(dEndDate) > 45)
            req = '{"startDate":"2019-01-10","endDate":"2019-03-10","reportType":"Remittance"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreport');
            


            
            
            Test.StopTest();
            
        }
        
    }
}