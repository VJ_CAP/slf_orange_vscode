/******************************************************************************************
* Description: Test class to cover UpdateStipsDataServiceImpl Services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri             09/13/2019             Created
******************************************************************************************/
@isTest
public class ResendStipEmailDataServiceImplTest{    
    
    @testsetup static void createtestdata(){
        testDataInsert();
        
    }
    private static void testDataInsert(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
       
               
        //For UpdateStipsDataServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.Credit_Run__c = true;
        acc.Default_FNI_Loan_Amount__c = 25000;
        acc.Risk_Based_Pricing__c = true;
        acc.Credit_Waterfall__c = true;
        acc.Eligible_for_Last_Four_SSN__c = true;
        acc.Phone = '9874563210';
        acc.Push_Endpoint__c = 'www.google.com';
        acc.Subscribe_to_Push_Updates__c = true;
        insert acc;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
 
        //Insert SLF Product Record
            List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            //insert prod;
            prodList.add(prod);
            
            insert prodList;
            
            //Insert opportunity record
            List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            //insert personOpp;
            oppList.add(personOpp);
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = personAcc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            opp.ProductTier__c = '0';         
            opp.Type_of_Residence__c  = 'Own';
            opp.language__c = 'Spanish';
            opp.Project_Category__c = 'Solar';
            //insert opp;
            oppList.add(opp);
            
            insert oppList;
            
            Underwriting_File__c undStpbt = new Underwriting_File__c();
            undStpbt.Opportunity__c = opp.id;
            //undStpbt.Opportunity__c = opp.id;           
            insert undStpbt;
            
            
            Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', 
                                                              Email_Text__c = 'Test data', 
                                                              POS_Message__c = 'test',
                                                             Folder_Name__c='Government IDs',
                                                             Review_Classification__c = 'SLS I');
            insert sd;
            
            Stipulation__c stp = new Stipulation__c();
            stp.Status__c ='Completed';
            stp.Stipulation_Data__c = sd.id;
            stp.Underwriting__c =undStpbt.id;
            insert stp;
            
            //Insert Quote Record
            List<Quote> quoteList = new List<Quote>();
            Quote quoteRec = new Quote();
            quoteRec.Name ='Test quote';
            quoteRec.Opportunityid = opp.Id;
            quoteRec.SLF_Product__c = prod.Id ;
            //quoteRec.System_Design__c= sysDesignRec.id;
            //quoteRec.Accountid =acc.id;
            //insert quoteRec; 
            quoteList.add(quoteRec); 
            
            insert quoteList;
            
            //Insert Credit Record
            List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.id;
            //credit.Primary_Applicant__c = personAcc.id;
            //credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = prod.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'New';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            //insert credit;
            creditList.add(credit);
             insert creditList;
             
            List<Email_Validation_Keywords__c> emailkeywordsList = new List<Email_Validation_Keywords__c>();
            Email_Validation_Keywords__c emailKeys = new Email_Validation_Keywords__c();
            emailKeys.name = 'Solar';
            emailkeywordsList.add(emailKeys);
            insert emailkeywordsList;
            
            List<Email_Domain_Validation_List__c> emailDomList = new List<Email_Domain_Validation_List__c>();
            Email_Domain_Validation_List__c emailDomain = new Email_Domain_Validation_List__c();
            emailDomain.name = 'yahoo.com';
            emailDomList.add(emailDomain);
            insert emailDomList;
    }   
    
 
 /**
* Description: This method is used to cover ResendStipEmailServiceImpl and ResendStipEmailDataServiceImplTest  
*
*/
    
    private testMethod static void ResendStipEmailDataServiceImplTest(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,SLF_Product__c from opportunity where name = 'OpName' LIMIT 1]; 
        Product__c prod = [select Id,Name from Product__c where Name =: 'testprod' LIMIT 1];
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1]; 
        Stipulation__c stip = [select Id,Name,Status__c,fileName__c from Stipulation__c LIMIT 1];       
        System.runAs(loggedInUsr[0])
        {
            
            Test.starttest();
            Map<String, String> UpdateStipsMap = new Map<String, String>();
            
            String UpdateStips ;
            
            UpdateStips= '{"projects": [{"id": "'+opp.id+'","externalId": "","hashId": "","stips": [{"documentURL": "test@test.com","folderName": "Government IDs","fileName": "Test"}]}]}'; 
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            UpdateStips= '{"projects": [{"id": "","externalId": "","hashId": "","stips": []}]}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            UpdateStips= '{"projects": [{"id": "","stips": [{"documentURL": "https://sunlightfinancial.box.com/s/ff2b8cvte82p7azmr77ddflx0kwngr6k","folderName": "Government IDs","fileName": "SFTBR-0458.txt"}]}]}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            UpdateStips= '{"projects": [{"id": "5123","externalId": "3123","hashId": "1234","stips": [{"documentURL": "https://sunlightfinancial.box.com/s/ff2b8cvte82p7azmr77ddflx0kwngr6k","folderName": "Government IDs","fileName": "SFTBR-0458.txt"}]}]}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            UpdateStips= '{"projects": [{"id": "5123","externalId": "3123","hashId": "1234","stips": [{"documentURL": "test@test.com","folderName": "Government IDs","fileName": "Test"}]}]}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            UpdateStips= '{"projects": [{"id": "","externalId":"'+opp.id+'","stips": [{"documentURL": "test@test.com","folderName": "Government IDs","fileName": "Test"}]}]}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            UpdateStips= '{"projects": [{"id": "","externalId":"","hashId": "'+opp.Hash_Id__c+'","stips": [{"documentURL": "test@test.com","folderName": "Government IDs","fileName": "Test"}]}]}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail'); 
            
            UpdateStips= '{"projects": [{"id": "'+opp.id+'","externalId": "","hashId": "","stips": []}]}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            UpdateStips= '{"projects": []}';
            MapWebServiceURI(UpdateStipsMap,UpdateStips,'resendemail');
            
            ResendStipEmailServiceImpl updateStip = new ResendStipEmailServiceImpl();
            updateStip.fetchData(null);
            Test.stopTest();
            
        }  
    }
 
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}