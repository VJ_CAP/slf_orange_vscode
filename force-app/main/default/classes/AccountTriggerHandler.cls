/**
* Description: Account related Trigger logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma           08/04/2017          Created
******************************************************************************************/ 
public without sharing Class AccountTriggerHandler
{
    public static boolean isTrgExecuting = true ;
    /**
* Constructor to initialize
*
* @param isExecuting    Has value of which context it is executing.
*/
    public AccountTriggerHandler(boolean isExecuting)
    {
    }
    public static boolean runOnce(){
        if(isTrgExecuting){
            isTrgExecuting=false;
            
            return true;
        }else{
            return isTrgExecuting;
        }
    }
    public void OnBeforeInsert(List<Account> newAccList){//Added By Deepika on 4/2/2019 as part of Orange-1930
        system.debug('### Entered from OnBeforeInsert() of '+ AccountTriggerHandler.class);
        try{
            for(Account accIterate : newAccList){
                
                //Added By Adithya as part of 2515
                //Start
                if(null != accIterate.M0_Split__c)
                    accIterate.M0_Split_Effective_Date__c = system.Now();
                if(null != accIterate.M1_Split__c)
                    accIterate.M1_Split_Effective_Date__c = system.Now();
                if(null != accIterate.M2_Split__c)
                    accIterate.M2_Split_Effective_Date__c = system.Now();
                if(null != accIterate.Permit_Split__c)
                    accIterate.Permit_split_effective_date__c = system.Now();
                if(null != accIterate.Kitting_Split__c)
                    accIterate.Kitting_split_effective_date__c = system.Now();
                if(null != accIterate.Inspection_Split__c)
                    accIterate.Inspection_split_effective_date__c = system.Now();
                //Stop
                
                if(accIterate.Solar_Enabled__c){
                    if(accIterate.Permit_Enabled__c){
                        accIterate.Permit_Ever_Enabled__c = True;
                        if(accIterate.Permit_Split__c == null)
                            accIterate.Permit_Split__c = 0;
                    }else{
                        accIterate.Permit_Split__c = null;
                    }
                    if(accIterate.Kitting_Enabled__c){
                        accIterate.Kitting_Ever_Enabled__c = True;
                        if(accIterate.Kitting_Split__c == null)
                            accIterate.Kitting_Split__c = 0;
                    }else{
                        accIterate.Kitting_Split__c = null;
                    }
                    if(accIterate.Inspection_Enabled__c){
                        accIterate.Inspection_Ever_Enabled__c = True;
                        if(accIterate.Inspection_Split__c == null)
                            accIterate.Inspection_Split__c = 0;
                    }else{
                        accIterate.Inspection_Split__c = null;
                    }
                }
            }
            
        }catch(Exception err){
            
            system.debug('### AccountTriggerHandler.OnBeforeInsert():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('AccountTriggerHandler.OnBeforeInsert()',err.getLineNumber(),'OnBeforeInsert()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));            
        }
        system.debug('### Exit from OnBeforeInsert() of '+ AccountTriggerHandler.class);
    }
    
    //Srikanth - OnBeforeUpdate - 'Enabled for Orange' field is updated to match with isPartner field on Account - Orange-990 - 07/15/2018
    
    public void OnBeforeUpdate(Account[] newAccts, Map<Id,Account> oldMap){
        try{
            
            system.debug('### Entered into OnBeforeUpdate() of '+ AccountTriggerHandler.class);
            if(isTrgExecuting)
            {
                // runOnce();
                for(Account accIterate : newAccts){
                    if(accIterate.Routing_Number__c == NULL || accIterate.Routing_Number__c == ''){
                        accIterate.Routing_Number_Matches__c = false;    
                    }
                    //Added By Adithya as part of 2515
                    //Start
                    if(oldMap.get(accIterate.Id).M0_Split__c != accIterate.M0_Split__c)
                        accIterate.M0_Split_Effective_Date__c = system.Now();
                    if(oldMap.get(accIterate.Id).M1_Split__c != accIterate.M1_Split__c)
                        accIterate.M1_Split_Effective_Date__c = system.Now();
                    if(oldMap.get(accIterate.Id).M2_Split__c != accIterate.M2_Split__c)
                        accIterate.M2_Split_Effective_Date__c = system.Now();
                    if(oldMap.get(accIterate.Id).Permit_Split__c != accIterate.Permit_Split__c)
                        accIterate.Permit_split_effective_date__c = system.Now();
                    if(oldMap.get(accIterate.Id).Kitting_Split__c != accIterate.Kitting_Split__c)
                        accIterate.Kitting_split_effective_date__c = system.Now();
                    if(oldMap.get(accIterate.Id).Inspection_Split__c != accIterate.Inspection_Split__c)
                        accIterate.Inspection_split_effective_date__c = system.Now();
                    //Stop
                    
                    //Added By Deepika on 4/2/2019 as part of Orange-1930 - Start
                    if(accIterate.Solar_Enabled__c){
                        if(accIterate.Permit_Enabled__c){
                            accIterate.Permit_Ever_Enabled__c = True;
                            if(accIterate.Permit_Split__c == null)
                                accIterate.Permit_Split__c = 0;
                        }else{
                            accIterate.Permit_Split__c = null;
                        }
                        if(accIterate.Kitting_Enabled__c){
                            accIterate.Kitting_Ever_Enabled__c = True;
                            if(accIterate.Kitting_Split__c == null)
                                accIterate.Kitting_Split__c = 0;
                        }else{
                            accIterate.Kitting_Split__c = null;
                        }
                        if(accIterate.Inspection_Enabled__c){
                            accIterate.Inspection_Ever_Enabled__c = True;
                            if(accIterate.Inspection_Split__c == null)
                                accIterate.Inspection_Split__c = 0;
                        }else{
                            accIterate.Inspection_Split__c = null;
                        }
                    }
                    //Added By Deepika on 4/2/2019 as part of Orange-1930 - Stop
                    //Added by Ravi to set Enabled_for_Orange__c flag on newly created partner accounts
                    if(accIterate.RecordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId() && oldMap.get(accIterate.id).isPartner != accIterate.isPartner)
                        accIterate.Enabled_for_Orange__c = accIterate.isPartner;
                }
            }   
            
        }catch(Exception err){
            
            system.debug('### AccountTriggerHandler.OnBeforeUpdate():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('AccountTriggerHandler.OnBeforeUpdate()',err.getLineNumber(),'OnBeforeUpdate()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));            
        }
        system.debug('### Exit from OnBeforeUpdate() of '+ AccountTriggerHandler.class);
    }
    
    public void OnAfterUpdate(Account[] newAccts, Map<Id,Account> oldMap){
        
        try{
            system.debug('### Entered into OnAfterUpdate() of '+ AccountTriggerHandler.class);
            if(isTrgExecuting)
            {
                set<Id> accIdSet = new set<Id>();
                set<Id> accids=new set<Id>();
                for(Account accIterate: newAccts){
                   if((accIterate.Routing_Number_Matches__c != oldMap.get(accIterate.Id).Routing_Number_Matches__c ) || (accIterate.Account_Number__c != oldMap.get(accIterate.Id).Account_Number__c ) || (accIterate.Routing_Number__c!=oldMap.get(accIterate.Id).Routing_Number__c))
                        accIdSet.add(accIterate.Id);
                }
                
                runOnce();
                Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>();
                if(!accIdSet.isEmpty())
                {
                    //Orange-2040 12th sprint Udaya Kiran Start
                    List<Opportunity> oppTmpLst = [select id, Accountid,Account.Account_Number__c,Account.Name,Account.Routing_Number__c,ACH_APR_Process_Required__c,StageName,Account.ACH_true__c,Account.Routing_Number_Matches__c,Account_Number_Populated__c,Long_Term_Facility_Full_Name__c,Primary_Applicant_Email__c,Co_Applicant__r.PersonEmail,Has_Coapplicant__c, (Select id,Confirm_ACH_Information__c from Underwriting_Files__r) from opportunity where (StageName = 'Closed Won' OR StageName = 'Loan Agreement Sent') AND Accountid IN: accIdSet];
                    if(!oppTmpLst.isEmpty())
                    {
                        List<Attachment> attachList = new List<Attachment>();
                        List<Routing_Number__c> lstRN = [select Id,ACH_Routing_Numbers__c from Routing_Number__c ];
                        String validRoutingno = ''; 
                        boolean isRoutingNumberAvailable = false;
                        Routing_Number__c RoutingObj = new Routing_Number__c();
                        List<Messaging.SingleEmailMessage> lstEmails= new List<Messaging.SingleEmailMessage>();
                        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();

                        EmailTemplate emailTemplateRec = [SELECT Id,Name,TemplateType,HtmlValue,Subject,body FROM EmailTemplate WHERE DeveloperName ='PEF_Form_Mail'];         
                        
                        for(Routing_Number__c objRN : lstRN){
                            if(objRN.ACH_Routing_Numbers__c.length() < 32000){
                                RoutingObj = objRN;
                            }
                            if(validRoutingno == ''){
                                validRoutingno = ','+objRN.ACH_Routing_Numbers__c+',';
                            }else{
                                validRoutingno += objRN.ACH_Routing_Numbers__c+',';
                            }
                        }
                        for(Opportunity oppIterate : oppTmpLst){
                            oppMap.put(oppIterate.AccountId,oppIterate); 
                            //Added as part of 2040 sprint 12- by kalyani - start
                            if(null != oppIterate.Account.Routing_Number__c && !validRoutingno.contains(oppIterate.Account.Routing_Number__c) && null != oppIterate.Underwriting_Files__r && oppIterate.Underwriting_Files__r[0].Confirm_ACH_Information__c=='ACH Enrolled'){
                                if(RoutingObj != null){
                                    RoutingObj.ACH_Routing_Numbers__c = RoutingObj.ACH_Routing_Numbers__c+''+ oppIterate.Account.Routing_Number__c+',';
                                }else{
                                    RoutingObj.Name = 'Routing Numbers';
                                    RoutingObj.ACH_Routing_Numbers__c =  oppIterate.Account.Routing_Number__c+',';
                                }
                                isRoutingNumberAvailable = true;
                            }
                            if(null == oppIterate.Account.ACH_true__c && oppIterate.ACH_APR_Process_Required__c == true && oppIterate.StageName=='Closed Won' && oppIterate.Account.Routing_Number_Matches__c == true && oppIterate.Account.Account_Number__c != null ){
                                System.debug('Inside'); 
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                
                                List<string> toAddress = new List<string>();
                                toAddress.add(oppIterate.Primary_Applicant_Email__c);
                                
                                if(oppIterate.Has_Coapplicant__c==true)
                                    toAddress.add(oppIterate.Co_Applicant__r.PersonEmail);
                                if(!toAddress.isEmpty())
                                    mail.setToAddresses(toAddress);
                                
                                mail.setSubject(emailTemplateRec.Subject); 
                                String emailBody = emailTemplateRec.HtmlValue; 
                                emailBody = emailBody.replace('{!$Label.Sunlight_Logo}',system.label.sunlight_logo );
                                emailBody = emailBody.replace('{!Opportunity.Account}',oppIterate.Account.Name);
                                emailBody = emailBody.replace('{!Opportunity.Long_Term_Facility_Full_Name__c}',String.valueOf(oppIterate.Long_Term_Facility_Full_Name__c));
                                System.debug('email body'+emailbody);
                                
                                emailBody = emailBody.replace('<![CDATA[','');
                                emailBody = emailBody.replace(']]>','');
                                mail.setHtmlBody(emailBody);
                                lstEmails.add(mail); 
                                
                                //Upload to Box
                                //Start
                                List<Contact> conList = new List<Contact>();
                                conList = [SELECT Id, Email FROM Contact WHERE Name = 'Sunlight Financial' AND Email <> null LIMIT 1];
                                Messaging.SingleEmailMessage msg1 = new Messaging.SingleEmailMessage();
                                msg1.setTemplateId(emailTemplateRec.Id);
                                msg1.setTargetObjectId(conList[0].Id);
                                msg1.setSaveAsActivity(false);
                                msg1.setTreatTargetObjectAsRecipient(true);
                                msg1.setWhatId(oppIterate.Id);

                                Savepoint sp = Database.setSavepoint();
                                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg1 });
                                Database.rollback(sp);
                                
                                string body= '';
                                if(emailTemplateRec.TemplateType != 'Text'){
                                    body = mail.getHTMLBody();
                                    body = '<html>' + body + '</html>';
                                    system.debug('**SZ: line breaks = ' + body.indexOf('\r\n'));
                                }else{
                                    body = mail.getPlainTextBody();
                                }
                                system.debug('**SZ: body = ' + body);
                                System.debug('mail.getSubject()::'+mail.getSubject());
                                
                                attachList.add(new Attachment(
                                    Name = mail.getSubject() +' ' + (Datetime.now().format('yyyyMMdd_hhmmss')) + '.html',
                                    ContentType = 'text/html',
                                    Body = Blob.valueOf(body),
                                    ParentId = oppIterate.Id
                                ));
                                //End
                            }
                            if(!lstEmails.isEmpty()){
                                Messaging.sendEmail(lstEmails,false);  
                                
                            }
                            //commented by Adithya on 12/2/2019 as this is not required 
                           //update oppIterate;
                        }
                        //Updated isRoutingNumberAvailable flag by Adithya 
                        if(RoutingObj !=null && isRoutingNumberAvailable){
                            update RoutingObj;
                        }
                        if (attachList.size() > 0)
                        {
                            insert attachList;
                            Set<Id> attachmentIds = new Set<Id>();
                            for(Attachment att : attachList)
                            {
                                attachmentIds.add(att.Id);
                            }
                            AutomatedLoanConfirmationEmail.SendAttachmentsToBox (attachmentIds, true);
                        }
                        //Added as part of 2040 sprint 12- by kalyani - End
                    }
                }
                List<Opportunity> oppLstUpd = new List<Opportunity>(); 
                if(!oppMap.isEmpty())
                {
                    for(Account accIterate : newAccts){  
                   
                        if(oppMap.containsKey(accIterate.Id))   
                        {
                            System.debug('###accIterate'+accIterate);
                            if(accIterate.Account_Number__c != null && accIterate.Account_Number__c != '')
                            {
                                Opportunity oppRef = new Opportunity(Id = oppMap.get(accIterate.Id).Id);
                                oppRef.Account_Number_Populated__c = true;
                                oppLstUpd.add(oppRef);
                            }
                            else
                            {
                                Opportunity oppRef = new Opportunity(Id = oppMap.get(accIterate.Id).Id);
                                oppRef.Account_Number_Populated__c = false;
                                oppLstUpd.add(oppRef);
                            }
                        }
                    }
                }
                if(!oppLstUpd.isEmpty())
                {
                    Update oppLstUpd;
                }
                //Orange-2040 12th sprint Udaya Kiran End
                
                
                ID rectypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
                Map<Id,Account> accMap = new Map<Id,Account>(); // Added By Adithya as part of 3073
                for(Account accIterate : newAccts){          
                    if(rectypeid == accIterate.RecordTypeId)
                    {
                        accMap.put(accIterate.Id,accIterate); // Added By Adithya as part of 3073
                    }
                }
                if(!accMap.isEmpty())
                {
                    set<Id> opppIdSet = new set<Id>();
                    
                    List<Opportunity> oppLst = [select Id,name,Installer_Account__c,EDW_Originated__c,Installer_Account__r.Push_Endpoint__c,Installer_Account__r.Subscribe_to_Push_Updates__c,Accountid,Co_Applicant__c,(Select id,Confirm_ACH_Information__c from Underwriting_Files__r) from Opportunity where Installer_Account__r.Push_Endpoint__c != null AND Installer_Account__r.Subscribe_to_Push_Updates__c =: true AND EDW_Originated__c =: false AND StageName != 'Archive' AND (Accountid IN: accMap.keyset() OR Co_Applicant__c IN: accMap.keyset()) order by LastModifiedDate Desc];
                    Map<id,Account> accTempMap =  new Map<id,Account>(); // Added By Adithya as part of 3073
                    Map<Id,Id> accOppMap = new Map<Id,Id>(); // Added By Adithya as part of 3073
                    if(!oppLst.isEmpty())
                    {
                        // Updated as part of ORANGE - 3073 - Start
                        for(Opportunity oppIterate : oppLst){
                            opppIdSet.add(oppIterate.Id);
                            if(accMap.containsKey(oppIterate.AccountId))
                            {
                                system.debug('***Applicant********'+oppIterate.AccountId);
                                accTempMap.put(oppIterate.AccountId,accMap.get(oppIterate.AccountId));
                                accOppMap.put(oppIterate.AccountId,oppIterate.Id);
                            }
                            if(accMap.containsKey(oppIterate.Co_Applicant__c))
                            {
                                system.debug('***Co_Applicant__c********'+oppIterate.Co_Applicant__c);
                                accTempMap.put(oppIterate.Co_Applicant__c,accMap.get(oppIterate.Co_Applicant__c));
                                accOppMap.put(oppIterate.Co_Applicant__c,oppIterate.Id);
                            }
                            
                        } 
                        system.debug('***accOppMap********'+accOppMap);
                        System.debug('###oppMap'+oppMap);
                        // Updated as part of ORANGE - 3073 - End
                    }
                    
                    if(!opppIdSet.isEmpty() && !System.isBatch())
                    {
                        //Generate JSON Structure and create Status Audit Record
                        CreateUnifiedJSON.buildUnifiedJSON('Account','applicants',opppIdSet,accTempMap.Values(),oldMap,accOppMap);
                    }
                }   
            }   
        }catch(Exception err){
            
            system.debug('### AccountTriggerHandler.OnAfterUpdate():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('AccountTriggerHandler.OnAfterUpdate()',err.getLineNumber(),'OnAfterUpdate()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));            
        }
        system.debug('### Exit from OnAfterUpdate() of '+ AccountTriggerHandler.class);
    }
    
    
    
    public void accountSharetoServicePartner(List<Account> triggenew){
        Map<id,User> userMap = new Map<id,User>([Select id,Name,Username,UserRole.Name From User Where UserRole.DeveloperName = 'ServicePartnerUser']);
        UserRole objrole = [select Id,Name,DeveloperName from UserRole where DeveloperName = 'ServicePartnerUser' limit 1];
        Group objGroup;
        if(objrole != null)
            objGroup = [SELECT Id, RelatedId, DeveloperName,Type FROM Group WHERE RelatedId=:objrole.id and type ='Role'];
        List<AccountShare> AccShareRecordstoInsert= new List<Accountshare>();    
        
        for(Account acc :triggenew){
            if(!userMap.isEmpty() && userMap.get(acc.ownerid) != null){
                AccountShare objAccshare = new AccountShare();
                objAccshare.AccountId =acc.id;
                objAccshare.UserOrGroupId = objGroup.id;
                objAccshare.AccountAccessLevel = 'Edit';
                objAccshare.OpportunityAccessLevel='Edit';
                AccShareRecordstoInsert.add(objAccshare);
            }
        }
        System.debug('#### AccShareRecordstoInsert'+AccShareRecordstoInsert);
        if(!AccShareRecordstoInsert.isEmpty())
            insert AccShareRecordstoInsert;
    }
    
    
    
}