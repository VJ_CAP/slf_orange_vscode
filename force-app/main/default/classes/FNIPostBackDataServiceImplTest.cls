@isTest
public class FNIPostBackDataServiceImplTest {
    
    @testSetup private static void createTestData(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c=2.99;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        insert prod;
        
        //Insert opportunity record
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = acc.id;
        personOpp.Installer_Account__c = acc.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Install_Street__c ='Street';
        personOpp.Install_City__c ='InCity';
        personOpp.Combined_Loan_Amount__c =10000;
        personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
        //opp.Approved_LT_Facility__c ='TCU';
        personOpp.SLF_Product__c = prod.id;            
        insert personOpp;
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Street__c ='Street';
        opp.Install_City__c ='InCity';
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'TCU||123';
        //opp.Approved_LT_Facility__c = 'TCU';
        opp.SLF_Product__c = prod.id;
        opp.Opportunity__c = personOpp.Id; 
        opp.Block_Draws__c = false;
        opp.Has_Open_Stip__c = 'False';
        opp.All_HI_Required_Documents__c=true;
        opp.Max_Draw_Count__c=3;
        opp.ProductTier__c = '0';         
        opp.Type_of_Residence__c  = 'Own';
        opp.language__c = 'Spanish';
        opp.Project_Category__c = 'Solar';
        insert opp;  
        
        
        //Insert opportunity record
        Opportunity oppSales = new Opportunity();
        oppsales.name = 'OppNew';
        oppsales.CloseDate = system.today();
        oppsales.StageName = 'New';
        oppsales.AccountId = acc.id;
        oppsales.Installer_Account__c = acc.id;
        oppsales.Install_State_Code__c = 'CA';
        oppsales.Install_Street__c ='Street';
        oppsales.Install_City__c ='InCity';
        oppsales.Combined_Loan_Amount__c = 10000;
        oppsales.Partner_Foreign_Key__c = 'TCU||1234';
        //opp.Approved_LT_Facility__c = 'TCU';
        oppsales.SLF_Product__c = prod.id;
        oppsales.Opportunity__c = personOpp.Id;
        oppsales.Co_Applicant__c = acc.id;
        insert oppsales;
        
        //Insert Credit Record
        List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
        SLF_Credit__c credit = new SLF_Credit__c();
        credit.Opportunity__c = opp.id;
        credit.Primary_Applicant__c = acc.id;
        credit.Co_Applicant__c = acc.id;
        credit.SLF_Product__c = prod.Id ;
        credit.Total_Loan_Amount__c = 100;
        credit.Status__c = 'New';
        credit.LT_FNI_Reference_Number__c = '123';
        credit.Application_ID__c = '123';
        //insert credit;
        creditList.add(credit);
        
        //Insert Credit Record
        SLF_Credit__c credit2 = new SLF_Credit__c();
        credit2.Opportunity__c = personOpp.id;
        credit2.Primary_Applicant__c = acc.id;
        credit2.Co_Applicant__c = acc.id;
        credit2.SLF_Product__c = prod.Id ;
        credit2.Total_Loan_Amount__c = 100;
        credit2.Status__c = 'New';
        credit2.LT_FNI_Reference_Number__c = '123';
        credit2.Application_ID__c = '123';
        //insert credit2;
        creditList.add(credit2);
        
        //Insert Credit Record
        SLF_Credit__c salescredit = new SLF_Credit__c();
        salescredit.Opportunity__c = oppsales.id;
        salescredit.Primary_Applicant__c = acc.id;
        salescredit.Co_Applicant__c = acc.id;
        salescredit.SLF_Product__c = prod.Id ;
        salescredit.Total_Loan_Amount__c = 100;
        salescredit.Status__c = 'New';
        salescredit.LT_FNI_Reference_Number__c = '123';
        salescredit.Application_ID__c = '123';
        //insert salescredit;
        creditList.add(salescredit);
        
        insert creditList;
    }
    
    private testMethod static void FNIPostbackAPITest(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1]; 
        SLF_Credit__c cred1 = [SELECT Id, LT_FNI_Reference_Number__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1]; 
        
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            Map<String, String> fNIPostbackMap = new Map<String, String>();
            
            String fNIPostback;
            
            //for serviceImpl coverage
            fNIPostback = '{"credit": [{contacts": [{"applicant_type": "PRIMARY","middle_name": "","contact_id": "bf976a97-de20-4fb8-a066-5e4ad04a4956","last_name": "SILVA","first_name": "JOSE"}],"amount_approved_usd": "5","stipulations": ["CL4"],"decision": "P","term": "245","scorecard_id": "TCU","decision_datetime": "2017-09-06 18:49:50","external_app_id": "123456789"}],"zipcode": "85757","action_type": "all","street_address": "4601 W CALLE DON TOMAS","action": "process_decision","state_abbreviation": "AZ","solarsite_id": "2377eece-2d0b-4f08-9505-ad387059cb59","qualification_definition_id": "47e9103f-c8cc-445f-814b-6d1fc1174a12","city_name": "TUCSON"}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //null check - creditDecisionDate
            fNIPostback = '{"projects":[{"credits":[{"maxApprovedAmount":"50000","stipulations":["CL4"],"ltFNIDecision":"P","term":"245","creditDecisionDate":"","external_app_id":"20000002390338"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //null check - ltFNIDecision
            fNIPostback = '{"projects":[{"credits":[{"maxApprovedAmount":"50000","stipulations":["CL4"],"ltFNIDecision":"","term":"245","creditDecisionDate":"2017-11-11 18:49:50","fniReferenceNumber":"'+cred1.LT_FNI_Reference_Number__c+'"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //null check - maxApprovedAmount
            fNIPostback = '{"projects":[{"credits":[{"maxApprovedAmount":,"stipulations":["CL4"],"ltFNIDecision":"C","term":"245","creditDecisionDate":"2017-11-11 18:49:50","fniReferenceNumber":"'+cred1.LT_FNI_Reference_Number__c+'"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            fNIPostback = '{"projects":[{"credits":[{"maxApprovedAmount":"60000","stipulations":["CL4"],"ltFNIDecision":"P","term":"245","creditDecisionDate":"2017-11-11 18:49:50","fniReferenceNumber":"'+cred1.LT_FNI_Reference_Number__c+'"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //fniReferenceNumber check
            fNIPostback = '{"projects":[{"credits":[{"maxApprovedAmount":"5000","stipulations":["CL4"],"ltFNIDecision":"P","term":"245","creditDecisionDate":"2017-11-11 18:49:50","fniReferenceNumber":"123456"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //not null check - maxApprovedAmount
            fNIPostback = '{"projects":[{"credits":[{"ltStipRsn1":"CL4","ltFNIDecision":"P","creditDecisionDate":"2017-11-11 18:49:50","fniReferenceNumber":"'+cred1.LT_FNI_Reference_Number__c+'"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //Update success scenorio
            fNIPostback = '{"projects":[{"credits":[{"maxApprovedAmount":"60000","ltStipRsn1":"CL4","ltFNIDecision":"P","creditDecisionDate":"2017-11-11 18:49:50","fniReferenceNumber":"'+cred1.LT_FNI_Reference_Number__c+'"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //credit section check
            fNIPostback = '{"projects":[{"id":"'+opp.id+'"}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            //creditExpirationDate
                        fNIPostback = '{"projects":[{"credits":[{"maxApprovedAmount":"60000","ltStipRsn1":"CL4","ltFNIDecision":"P","creditDecisionDate":"2017-11-11 18:49:50","creditExpirationDate":"2017-11-11","fniReferenceNumber":"'+cred1.LT_FNI_Reference_Number__c+'"}]}]}';

        //    fNIPostback = '{"projects":[{"credits":[{"creditExpirationDate":"2017-11-11"}]}]}';
            MapWebServiceURI(fNIPostbackMap,fNIPostback,'getFNIPostBack');
            
            FNIPostBackDataServiceImpl fnipostbackObj = new FNIPostBackDataServiceImpl();
            fnipostbackObj.transformOutput(null);
            
            Test.stopTest();
        }
    }
/**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
    
    
}