/******************************************************************************************
* Description: Test class to cover SyncQuoteDataServiceImpl class
****/
@isTest
public class SyncQuoteDataServiceImplTest {    
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
       //testDataInsert();
        
    }
    
    /**
    *
    * Description: This method is used to cover SyncQuoteServiceImpl and SyncQuoteDataServiceImpl 
    *
    */ 
    private testMethod static void syncQuoteTest(){
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        
        Opportunity opp = [select Id,Name,SLF_Product__c, (Select id,name from Underwriting_Files__r)from opportunity where name = 'opName' LIMIT 1]; 
        
        Product__c prod = [select Id,Name from Product__c where Name =: 'testprod' LIMIT 1];
        Underwriting_File__c uwFile = [select id,(select id,Level__c,Amount__c from Draw_Requests__r order by Level__c desc)  from underwriting_file__c where Opportunity__r.id=:opp.id LIMIT 1];
        
        Quote quote = [SELECT opportunityid,SLF_Product__r.Expiration_Date__c,SLF_Product__c, Name from Quote where Opportunityid=:opp.Id LIMIT 1];
        
        system.debug('***###Quote Record###***'+quote);
        quote.Non_Solar_Amount__c = 100;
        update quote;
        
        SLF_Credit__c cred = [Select id,IsSyncing__c,Name,Status__c from SLF_Credit__c where Opportunity__c =: opp.id limit 1]; 
        
        Draw_Requests__c drawReq = new Draw_Requests__c();
        drawReq.Level__c = '1';
        drawReq.Amount__c = 100;
        drawReq.Underwriting__c = uwFile.Id;
        insert drawReq;
        
        cred.IsSyncing__c = true;
        cred.Status__c = 'Auto Approved';
        update cred;
        Credit_Opinion__c crdOne = new Credit_Opinion__c();
        crdOne.Quote__c = quote.id;
        crdOne.Credit__c = cred.id;
        crdOne.Status__c ='Auto Approved';
        insert crdOne;
        
        Account slfAcc = [SELECT Id FROM Account WHERE Name != 'Test Account' LIMIT 1]; 
        System.runAs(loggedInUsr[0])
        {
            Map<String, String> syncquoteMap = new Map<String, String>();
            String syncQuote;
            Test.startTest();
            
            //If Valid Quote id is provided.
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            //Exception
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" },{"id":"21211"} ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes":[ { "id": "" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "12345" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" },{"id":"21211"} ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'"]}}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            prod.Expiration_Date__c = system.today()-1;
            update prod;
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            prod.Product_Loan_Type__c = 'Solar Interest Only';
            prod.Expiration_Date__c = system.today();
            update prod;
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            opp.Project_Category__c = 'Solar';
            opp.StageName = 'Closed Won';
            update opp;
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            opp.Project_Category__c = 'Home';
            opp.StageName = 'Closed Won';
            update opp;
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            uwFile.Project_Status__c = 'Project Withdrawn';
            uwFile.Withdraw_Reason__c = 'Customer request';
            update uwFile;
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');   
            
            opp.Installer_Account__c = slfAcc.id;
            update opp;
            syncQuote = '{"projects": [{"id":"'+opp.id+'","quotes": [ { "id": "'+quote.Id+'" } ]}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
            
            syncQuote = '{"projects": [{"id":"'+opp.id+'"}]}';
            MapWebServiceURI(syncquoteMap,syncQuote,'syncquote');
                        
            Test.stopTest();
            //To cover exception block
            SyncQuoteDataServiceImpl suncQuote = new SyncQuoteDataServiceImpl();
            suncQuote.transformOutput(null);
            
        }
            
    } 
    
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}