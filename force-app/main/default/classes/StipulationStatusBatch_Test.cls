@isTest
public class StipulationStatusBatch_Test {   
    
    private testMethod static void testStipulationStatusBatch(){
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);   
        TriggerFlags__c trgflag3 = new TriggerFlags__c();
        trgflag3.Name ='Underwriting_File__c';
        trgflag3.isActive__c =true;
        trgrList.add(trgflag3);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='Stipulation__c';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        TriggerFlags__c trgflag5 = new TriggerFlags__c();
        trgflag5.Name ='Contact';
        trgflag5.isActive__c =true;
        trgrList.add(trgflag5);       
        TriggerFlags__c trgflag6 = new TriggerFlags__c();
        trgflag6.Name ='SLF_Credit__c';
        trgflag6.isActive__c =true;
        trgrList.add(trgflag6);
        TriggerFlags__c trgflag7 = new TriggerFlags__c();
        trgflag7.Name ='Funding_Data__c';
        trgflag7.isActive__c =true;
        trgrList.add(trgflag7);
        TriggerFlags__c trgflag8 = new TriggerFlags__c();
        trgflag8.Name ='Stipulation_Data__c';
        trgflag8.isActive__c =true;
        trgrList.add(trgflag8);
        TriggerFlags__c trgflag9 = new TriggerFlags__c();
        trgflag9.Name ='System_Design__c';
        trgflag9.isActive__c =true;
        trgrList.add(trgflag9);
        insert trgrList;
        
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account150';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';        
        accObj.Type = 'Facility'; 
        accObj.Prequal_Enabled__c = true; 
        accObj.Website = 'www.google.com';    
        accObj.Solar_Enabled__c = true;    
        insert accObj;
        
        Opportunity oppObj1 = new Opportunity(Name = 'OppName1',
                                              AccountId=accObj.Id,
                                              Co_Applicant__c = accObj.Id,
                                              StageName='Qualified',
                                              CloseDate=system.today(),
                                              Installer_Account__c = accObj.Id, 
                                              Install_State_Code__c = 'CA',
                                              Synced_Quote_Id__c = 'qId',
                                              Desync_Bypass__c = true,
                                              Install_Street__c='40 CORTE ALTA',
                                              Install_Postal_Code__c='94949',
                                              Install_City__c='California',
                                              EDW_Originated__c = false,
                                              Upload_Comment_Text__c = 'Testing data',
                                              Upload_Comment__c = true, Upload_Comment_TS__c = System.now() );
        
        insert oppObj1; 
        
        List<Underwriting_File__c> undFileList = new List<Underwriting_File__c>();
        Underwriting_File__c uwObj1 = new Underwriting_File__c(Opportunity__c = oppObj1.Id,
                                                               Project_Status__c = 'M2 Payment Pending',
                                                               M1_Approval_Requested_Date__c = system.Today());
        undFileList.add(uwobj1);
        
        insert undFileList;
        
        Stipulation_Data__c stipData  =  new Stipulation_Data__c(Name='ACH', Does_not_Block_M0__c = False, Does_not_Block_M1__c = False, Review_Classification__c = 'SLS II');
        insert stipData;
        //  Stipulation_Data__c stipData1  =  new Stipulation_Data__c(Name='APR',Suppress_In_Portal__c=false, Does_not_Block_M0__c = False, Does_not_Block_M1__c = False, Review_Classification__c = 'SLS II');
        //   insert stipData1;
        
        List<Stipulation__c> stips=new List<Stipulation__c>();
        Stipulation__c  stip1  =  new stipulation__c(Stipulation_Data__c = stipData.Id , Underwriting__c = undFileList[0].Id);
        stips.add(stip1);
        // Stipulation__c  stip2  =  new stipulation__c(Stipulation_Data__c = stipData1.Id , Underwriting__c = undFileList[0].Id);
        // stips.add(stip2);
        insert stips;
        
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =accObj.id;
        prod.Long_Term_Facility_Lookup__c =accObj.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.Internal_Use_Only__c = false;
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        prod.Product_Tier__c = '0';
        insert prod;
        
        Integer noOfDaysFromToday = 10;
        if(noOfDaysFromToday == null)
            noOfDaysFromToday = 0;
        SLF_Credit__c credit = new SLF_Credit__c();
        credit.Opportunity__c = oppObj1.id;
        credit.Primary_Applicant__c = accObj.id;
        credit.Co_Applicant__c = accObj.id;
        credit.SLF_Product__c = prod.Id ;
        credit.Total_Loan_Amount__c = 100;
        credit.Status__c = 'New';        
        credit.FNI_Credit_Expiration__c = system.Today().addDays(noOfDaysFromToday);
        credit.LT_FNI_Reference_Number__c = '123';
        credit.Application_ID__c = '123';
        insert credit;
        
        Set<id> uwIdSet = new Set<id>();        
        Test.startTest();
        StipulationStatusBatch  st1 = new StipulationStatusBatch(uwIdSet);
        DataBase.executeBatch(st1);
        
        uwIdSet.add(undFileList[0].Id);
        StipulationStatusBatch  st = new StipulationStatusBatch(uwIdSet);
        DataBase.executeBatch(st,50);
        Test.stopTest();
    }
}