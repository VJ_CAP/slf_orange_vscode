@RestResource(urlMapping='/setfilemetadata/*')
global with sharing class SetFileMetadata {

    global class Output
    {
        public Boolean success;
        public String errorMessage;
        public String fileName;
        public boolean isFileNameAlreadyExist;
        public String existFileId;
        public String loggedInUserName;
    }
    
    @httpPost
    global static Output createFileMetaData(String username,String fileid,String action){
        
        System.debug('### username:'+username);
        System.debug('### fileid:'+fileid);
        System.debug('### action:'+action);
        
        Output o = new Output();
        o.success = false;
        if(userName!=null && String.isNotEmpty(userName)){
            BoxPlatformApiConnection connection;

            try
            {
                connection = BoxAuthentication.getConnection ();
            }
            catch (Exception e)
            {
                o.errorMessage = 'Unable to authenticate with Box server: ' + e.getMessage();
                if (Test.isRunningTest() == false) return o;
            }

            String accessToken = (connection == null ? 'testaccesstokenonly' : connection.accessToken);
            try{
                String jbody = '';
                if(action.equalsIgnoreCase('Create')){
                    jbody = '{"Uploaded By":"'+username+'"}';
                }
                if(action.equalsIgnoreCase('Update')){
                    jbody = '[{"path":"/Uploaded By","value":"'+username+'","op":"replace"}]';
                }
                String returnCode = BoxFile.setFileMetadata(accessToken,fileid,jbody,action);
                System.debug('### returnCode: '+returnCode);
                if(returnCode=='201' || returnCode=='200'){
                    o.success = true;
                }
            }catch(Exception ex){
                o.errorMessage = ex.getMessage();
                System.debug('### Exception: '+ex.getMessage());
            }
        }
        return o;
    }
}