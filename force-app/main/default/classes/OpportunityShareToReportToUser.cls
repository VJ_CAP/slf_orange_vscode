/**
* Description: (OpportunityShareToReportToUser) this Helper Class will share the opportunity record and Accout record to Portal contact's Reportsto User.
*              Opportunity Owner(user) -->User Contact--> Reportsto(Contact) --> User of Reportsto Contact.
*   Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Brahmeswar Gajula          11/02/2018          Created
******************************************************************************************/

public without sharing class OpportunityShareToReportToUser {
    
    /*
* Description : This method will be used to share Opportunity and Account to new reportsto(User) opportunity owner
*/
    public void OportunityShareOnContactChange(Map<id,Contact> Triggernewmap ,Map<id,Contact> Triggeroldmap){
        try{ 
            Map<id,Contact> modifiedContactlist = new map<id,Contact>();
            set<string> newReporttoIDs = new set<string>();
            set<string> oldReportToIDs = new set<String>();
            List<string> oldReportUserIDs = new List<String>();
            Map<string,string> OppidPortaConatctID = new Map<string,string>();
            Map<string,string> PortalContactIdReportsToId = new map<string,string>();
            Map<string,string> ReportsToIdUserId = new map<string,string>();
            Map<string,string> oldReportsToIdUserId = new map<string,string>();
            Map<string,string> OppIdUserId = new map<string,string>();
            Map<string,string> AccidUserid = new map<string,string>();
            set<id> oppids = new set<id>();
            set<string> Accids = new set<string>();
            for(contact objcon : Triggernewmap.values()){
                
                if(Triggernewmap.get(objcon.id).ReportsToid != Triggeroldmap.get(objcon.id).ReportsToid){
                    modifiedContactlist.put(objcon.id,objcon);
                    if(Triggernewmap.get(objcon.id).ReportsToid != null){
                        newReporttoIDs.add(Triggernewmap.get(objcon.id).ReportsToid);
                        PortalContactIdReportsToId.put(objcon.id,objcon.ReportsToid);
                    }
                    if(Triggeroldmap.get(objcon.id).ReportsToid != null){
                        oldReportToIDs.add(Triggeroldmap.get(objcon.id).ReportsToid);
                        newReporttoIDs.add(Triggeroldmap.get(objcon.id).ReportsToid);
                    }
                }
            }
            
            if(newReporttoIDs != null && newReporttoIDs.size()>0){
                for(user userobj : [select id,contactid from user where contactid IN: newReporttoIDs]){
                    if(oldReportToIDs.contains(userobj.contactid)){
                        oldReportsToIdUserId.put(userobj.contactid,userobj.id);
                        oldReportUserIDs.add(userobj.id);
                    }else{
                        ReportsToIdUserId.put(userobj.contactid,userobj.id);
                    }
                }
                
            }
            
            List<Opportunity> Opportunitylist = new List<Opportunity>();
            
            if(modifiedContactlist != null && modifiedContactlist.size()>0){
                Opportunitylist = [select id,Accountid,Ownerid,Owner.contactid from Opportunity where Owner.contactid IN:modifiedContactlist.keySet()];
                if(Opportunitylist != null && Opportunitylist.size()>0){
                    for(Opportunity objopp : Opportunitylist){
                        oppids.add(objopp.id);
                        if(objopp.Accountid != null)
                            Accids.add(objopp.Accountid);
                        OppidPortaConatctID.put(objopp.id,objopp.Owner.contactid);
                    }
                    for(Opportunity objopp : Opportunitylist){
                        if(ReportsToIdUserId.get(PortalContactIdReportsToId.get(OppidPortaConatctID.get(objopp.id))) != null)
                            OppIdUserId.put(objopp.id,ReportsToIdUserId.get(PortalContactIdReportsToId.get(OppidPortaConatctID.get(objopp.id))));
                        AccidUserid.put(objopp.Accountid,ReportsToIdUserId.get(PortalContactIdReportsToId.get(OppidPortaConatctID.get(objopp.id))));
                    }
                }
            }
            SharingRecords(oppids,OppIdUserId,Accids,AccidUserid);
            deleteExistingShareRecords(oppids,Accids,oldReportUserIDs); 
        }catch(Exception ex)
        {
            system.debug('### OpportunityShareToReportToUser.OportunityShareOnContactChange():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('OpportunityShareToReportToUser.OportunityShareOnContactChange()',ex.getLineNumber(),'insertData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        
        
    }
    
    
    
    /*
* Description : This method will be used to share Opportunity and Account to reportsTo portaluser of opportunity Owner on Opportunity insert 
*/
    public void OpportunityshareOnOpportunityCreation(Map<id,Opportunity> triggerNewMap){
        try{
            map<string,string> oppidReporttoID = new map<string,string>();
            Map<string,String> OppIdOwenerID = new map<string,string>();
            Map<string,String> userIDContactId = new map<string,string>();
            Map<string,string> AccIDReprtsToId = new map<string,string>();
            map<string,string> ReportstoIDUserID = new map<string,string>();
            set<string> Accids = new set<string>();
            set<string> ReportstoIds = new set<string>();
            
            for( Opportunity objopp : triggerNewMap.values()){
                OppIdOwenerID.put(objopp.id,objopp.ownerid);
                Accids.add(objopp.Accountid);
            }
            for( User userval : [select id,contactid,contact.ReportsToid from user where id IN:OppIdOwenerID.values()]){
                if(userval.contact.ReportsToid != null){
                    ReportstoIds.add(userval.contact.ReportsToid);
                    userIDContactId.put(userval.id,userval.contact.ReportsToid);
                }
                
            }
            if(ReportstoIds != null && ReportstoIds.size()>0){
                for( User userval : [select id,contactid from user where contactid IN:userIDContactId.values()]){
                    ReportstoIDUserID.put(userval.contactid,userval.id);
                }
            }
            for( Opportunity objopp : triggerNewMap.values()){
                if(ReportstoIDUserID.get(userIDContactId.get((OppIdOwenerID.get(objopp.id))))!= null){
                    oppidReporttoID.put(objopp.id,ReportstoIDUserID.get(userIDContactId.get((OppIdOwenerID.get(objopp.id))))); 
                    AccIDReprtsToId.put(objopp.Accountid,ReportstoIDUserID.get(userIDContactId.get((OppIdOwenerID.get(objopp.id)))));
                }
            }
            
            SharingRecords(triggerNewMap.keySet(),oppidReporttoID,Accids,AccIDReprtsToId);
        }catch(Exception ex)
        {
            system.debug('### OpportunityShareToReportToUser.OpportunityshareOnOpportunityCreation():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('OpportunityShareToReportToUser.OpportunityshareOnOpportunityCreation()',ex.getLineNumber(),'insertData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
    }
    
    
    public void opportunityShareOnOwnerChangeOrAccountChange(Map<id,Opportunity> triggernewmap, Map<id,Opportunity> triggeroldmap){
        try{
            Map<id,Opportunity> ownerchangedopplist = new Map<id,Opportunity>();
            map<id,string> oldoppIDUserID = new map<id,string>();
            map<id,string> oldAccIDUserID = new map<id,string>();
            set<string> Accids = new set<string>();
            set<string> ReportsToIDs = new set<string>();
            list<string> oldUserids = new list<string>();
            for(opportunity objopp : triggernewmap.values()){
                if(triggernewmap.get(objopp.id).ownerid != triggeroldmap.get(objopp.id).ownerid){
                    ownerchangedopplist.put(objopp.id,objopp);
                    oldoppIDUserID.put(objopp.id,triggeroldmap.get(objopp.id).ownerid);
                    Accids.add(objopp.accountid);
                }
                else if(triggernewmap.get(objopp.id).Accountid != triggeroldmap.get(objopp.id).Accountid){
                    ownerchangedopplist.put(objopp.id,objopp);
                    Accids.add(triggeroldmap.get(objopp.id).Accountid);
                    oldAccIDUserID.put(triggeroldmap.get(objopp.id).Accountid,triggeroldmap.get(objopp.id).ownerid);//Accountid and opportunity owner
                }
            }
            if(oldoppIDUserID.values()!= null && oldoppIDUserID.values().size()>0){
                for(User userval : [select id,contactid,contact.ReportsToid from user where id IN:oldoppIDUserID.values()]){
                    if(userval.contact.ReportsToid != null)
                        ReportsToIDs.add(userval.contact.ReportsToid);
                }
                
                for(user userobj : [select id,contactid from user where contactid IN: ReportsToIDs]){
                    oldUserids.add(userobj.id);
                }
            }
            
            
            if(ownerchangedopplist != null && ownerchangedopplist.size()>0)
                OpportunityshareOnOpportunityCreation(ownerchangedopplist);
            if(oldoppIDUserID!= null && oldoppIDUserID.size()>0)
                deleteExistingShareRecords(oldoppIDUserID.keySet(),Accids,oldUserids);
            if(oldAccIDUserID != null && oldAccIDUserID.size()>0){
                deleteExistingShareRecords(new Set<id>(),Accids,oldUserids);
            }
        }catch(Exception ex)
        {
            system.debug('### OpportunityShareToReportToUser.opportunityShareOnOwnerChangeOrAccountChange():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('OpportunityShareToReportToUser.opportunityShareOnOwnerChangeOrAccountChange()',ex.getLineNumber(),'insertData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        
    }
    public void SharingRecords(set<id> Oppids,Map<string,String> OppAndUserMap,set<string> Accids,Map<string,String> AccIdAndUserID){
        try{
            list<OpportunityShare> OppShareRecordstoInsert = new list<OpportunityShare>();
            list<AccountShare> AccShareRecordstoInsert  = new list<AccountShare>();
            
            if(Oppids != null && Oppids.size()>0 && OppAndUserMap != null && OppAndUserMap.size()>0){
                OpportunityShare objoppshare;
                for( id val : Oppids){
                    if(OppAndUserMap.get(val) != null){
                        objoppshare = new OpportunityShare();
                        objoppshare.OpportunityId =val; 
                        objoppshare.UserOrGroupId = OppAndUserMap.get(val);
                        objoppshare.OpportunityAccessLevel = 'Edit';
                        OppShareRecordstoInsert.add(objoppshare);
                    }
                    
                }
            }
            if(Accids != null && Accids.size()>0 && AccIdAndUserID != null && AccIdAndUserID.size()>0){
                for(string accidval : Accids){
                    AccountShare objAccshare;
                    if(AccIdAndUserID.get(accidval) != null){
                        objAccshare = new AccountShare();
                        objAccshare.AccountId =accidval;
                        objAccshare.UserOrGroupId = AccIdAndUserID.get(accidval);
                        objAccshare.AccountAccessLevel = 'Edit';
                        objAccshare.OpportunityAccessLevel='Edit';
                        AccShareRecordstoInsert.add(objAccshare);
                    }
                    
                }
            }
            if(AccShareRecordstoInsert != null && AccShareRecordstoInsert.size()>0){
                Database.SaveResult[] result = Database.insert(AccShareRecordstoInsert, false);
            }
            if((AccShareRecordstoInsert == null || AccShareRecordstoInsert.size()==0 )&& OppShareRecordstoInsert != null && OppShareRecordstoInsert.size()>0){
                Database.SaveResult[] result = Database.insert(OppShareRecordstoInsert, false);
                System.debug('***result Opp---'+result);
            }
        }catch(Exception ex)
        {
            system.debug('### OpportunityShareToReportToUser.SharingRecords():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('OpportunityShareToReportToUser.SharingRecords()',ex.getLineNumber(),'insertData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
    }
    public void deleteExistingShareRecords(Set<id> oppids,Set<string> Accids, List<string> oldUserids){
        try{
            List<OpportunityShare> listtoDeleteOppshare =  new List<OpportunityShare>();
            List<AccountShare> listtoDeleteAccshare =  new List<AccountShare>();
            
            if(oppids != null & oppids.size()>0 && oldUserids != null && oldUserids.size()>0){
                listtoDeleteOppshare = [select id,OpportunityId,UserOrGroupId,RowCause from OpportunityShare where OpportunityId IN:oppids AND UserOrGroupId IN:oldUserids AND RowCause='Manual' ];
            }
            if(Accids != null & Accids.size()>0 && oldUserids != null && oldUserids.size()>0){
                listtoDeleteAccshare = [select id,RowCause from AccountShare where AccountId IN:Accids AND UserOrGroupId IN:oldUserids AND RowCause = 'Manual' ];
            }
            if(listtoDeleteOppshare!= null && listtoDeleteOppshare.size()>0)
                delete listtoDeleteOppshare;
            if(listtoDeleteAccshare != null && listtoDeleteAccshare.size()>0)
                delete listtoDeleteAccshare;
        }catch(Exception ex)
        {
            system.debug('### OpportunityShareToReportToUser.deleteExistingShareRecords():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('OpportunityShareToReportToUser.deleteExistingShareRecords()',ex.getLineNumber(),'insertData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}