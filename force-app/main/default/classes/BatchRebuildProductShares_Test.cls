@isTest (seealldata=true)
private class BatchRebuildProductShares_Test 
{
    
    @isTest static void test_method_one() 
    {
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true);
        insert a1;
        
        Account a2 = [SELECT Id, Name FROM Account WHERE Type = 'Partner' LIMIT 1];

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a2.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Internal_Use_Only__c = true,
                                            Product_Tier__c = '0');
        insert p1;

       Product__c p2 = new Product__c (Name = 'Test2', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Internal_Use_Only__c = true,
                                            Product_Tier__c = '0');
        insert p2;

        p1.Internal_Use_Only__c = false;
        update p1;
        
        p1.Installer_Account__c = a1.Id;
        update p1;

        p2.Installer_Account__c = a2.Id;
        update p2;

        Database.executeBatch (new BatchRebuildProductShares(), 50);
    }
}