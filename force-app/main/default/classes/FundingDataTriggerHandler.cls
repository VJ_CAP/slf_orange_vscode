/**
* Description: Trigger Business logic associated with Funding Data is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Raviteja Sajja          04/05/2019          Created 
******************************************************************************************/
public without sharing Class FundingDataTriggerHandler
{

    public static boolean isTrgExecuting = true ;
    /**
    * Constructor to initialize
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public FundingDataTriggerHandler(boolean isExecuting)
    {
    }
    public static boolean runOnce(){
        if(isTrgExecuting){
            isTrgExecuting=false;
            
            return true;
        }else{
            return isTrgExecuting;
        }
    }
    
    /**
    * Description: On Before Insert
    *
    * @param newDrawReqRecLst   List of Funding_Data__c records.
    *
    */
    public void OnBeforeInsert(List<Funding_Data__c> newFundingDataList){
        try{
            system.debug('### Entered into OnBeforeInsert() of '+ FundingDataTriggerHandler.class);
            for(Funding_Data__c fundingDataIterate : newFundingDataList)
            {
                
                if(null != fundingDataIterate.C_O_CPF_APR__c)
                    fundingDataIterate.CPF_Updated_APR__c = fundingDataIterate.C_O_CPF_APR__c;
                else if(null != fundingDataIterate.Cancelled_CPF_APR__c)
                    fundingDataIterate.CPF_Updated_APR__c = fundingDataIterate.Cancelled_CPF_APR__c;
                else if(null != fundingDataIterate.Capital_Provider_Funded_APR__c)
                    fundingDataIterate.CPF_Updated_APR__c = fundingDataIterate.Capital_Provider_Funded_APR__c;
                
                if(null != fundingDataIterate.C_O_CPF_Term__c)
                    fundingDataIterate.CPF_Updated_Term__c = fundingDataIterate.C_O_CPF_Term__c;
                else if(null != fundingDataIterate.Cancelled_CPF_Term__c)
                    fundingDataIterate.CPF_Updated_Term__c = fundingDataIterate.Cancelled_CPF_Term__c;
                else if(null != fundingDataIterate.Capital_Provider_Funded_Term__c )
                    fundingDataIterate.CPF_Updated_Term__c = fundingDataIterate.Capital_Provider_Funded_Term__c ;
                
                //Added as part of ORANGE-5466
                updateCPFUpdatedAPRTerm(newFundingDataList, null);
                
            }
        }catch(Exception ex)
        {
            system.debug('### FundingDataTriggerHandler.OnBeforeInsert():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('FundingDataTriggerHandler.OnBeforeInsert()',ex.getLineNumber(),'OnBeforeInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from OnBeforeInsert() of '+ FundingDataTriggerHandler.class);
    }
    
    /**
    * Description: On Before Update
    *
    * @param newFundingDataList   List of Funding_Data__c records.
    * @param oldFundingDataMap      Map of Old Funding_Data__c records.
    */
    public void OnBeforeUpdate(Funding_Data__c[] newFundingDataList,Map<Id,Funding_Data__c> oldFundingDataMap){
        try
        {
            system.debug('### Entered into OnBeforeUpdate() of '+ FundingDataTriggerHandler.class);
        
            for(Funding_Data__c fundingDataIterate : newFundingDataList)
            {
                
                if(null != fundingDataIterate.C_O_CPF_APR__c && fundingDataIterate.C_O_CPF_APR__c != oldFundingDataMap.get(fundingDataIterate.Id).C_O_CPF_APR__c)
                    fundingDataIterate.CPF_Updated_APR__c = fundingDataIterate.C_O_CPF_APR__c;
                else if(null != fundingDataIterate.Cancelled_CPF_APR__c && fundingDataIterate.Cancelled_CPF_APR__c != oldFundingDataMap.get(fundingDataIterate.Id).Cancelled_CPF_APR__c)
                    fundingDataIterate.CPF_Updated_APR__c = fundingDataIterate.Cancelled_CPF_APR__c;
                else if(null != fundingDataIterate.Capital_Provider_Funded_APR__c && fundingDataIterate.Capital_Provider_Funded_APR__c != oldFundingDataMap.get(fundingDataIterate.Id).Capital_Provider_Funded_APR__c)
                    fundingDataIterate.CPF_Updated_APR__c = fundingDataIterate.Capital_Provider_Funded_APR__c;
                
                if(null != fundingDataIterate.C_O_CPF_Term__c && fundingDataIterate.C_O_CPF_Term__c != oldFundingDataMap.get(fundingDataIterate.Id).C_O_CPF_Term__c)
                    fundingDataIterate.CPF_Updated_Term__c = fundingDataIterate.C_O_CPF_Term__c;
                else if(null != fundingDataIterate.Cancelled_CPF_Term__c && fundingDataIterate.Cancelled_CPF_Term__c != oldFundingDataMap.get(fundingDataIterate.Id).Cancelled_CPF_Term__c)
                    fundingDataIterate.CPF_Updated_Term__c = fundingDataIterate.Cancelled_CPF_Term__c;
                else if(null != fundingDataIterate.Capital_Provider_Funded_Term__c  && fundingDataIterate.Capital_Provider_Funded_Term__c  != oldFundingDataMap.get(fundingDataIterate.Id).Capital_Provider_Funded_Term__c )
                    fundingDataIterate.CPF_Updated_Term__c = fundingDataIterate.Capital_Provider_Funded_Term__c ;
                    
                //Added as part of ORANGE-5466
                updateCPFUpdatedAPRTerm(newFundingDataList, oldFundingDataMap);               
            }
        }catch(Exception ex)
        {
            system.debug('### FundingDataTriggerHandler.OnBeforeUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
           ErrorLogUtility.writeLog('FundingDataTriggerHandler.OnBeforeUpdate()',ex.getLineNumber(),'OnBeforeUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from OnBeforeUpdate() of '+ FundingDataTriggerHandler.class);
    }
    /**
    * Description: On After Update
    *
    * @param newFundingDataList     List of Funding_Data__c records.
    * @param oldFundingDataMap      Map of Old Funding_Data__c records.
    */
    public void onAfterUpdate(Funding_Data__c[] newFundingDataList,Map<Id,Funding_Data__c> oldFundingDataMap){
        try
        {
            system.debug('### Entered into onAfterUpdate() of '+ FundingDataTriggerHandler.class);  
            //Updated By Adithya on 8/23/2019 as part of ORANGE-6897
            //Start
            Map<Id,Underwriting_File__c> updateUnderwritingMap = new Map<Id,Underwriting_File__c>();
            Map<Id,Opportunity> updateOpptyMap = new Map<Id,Opportunity>();
                        
            for(Funding_Data__c fundingDataIterate : newFundingDataList)
            {
                if(null != fundingDataIterate.Draw_1_Pay_Amount__c && fundingDataIterate.Draw_1_Pay_Amount__c != oldFundingDataMap.get(fundingDataIterate.Id).Draw_1_Pay_Amount__c)
                {
                    Underwriting_File__c underwritingFileRec = new Underwriting_File__c(id=fundingDataIterate.Underwriting_ID__c);
                    underwritingFileRec.Project_Status__c = 'Project Completed';
                    underwritingFileRec.Project_Status_Detail__c = '';
                    updateUnderwritingMap.put(fundingDataIterate.Underwriting_ID__c,underwritingFileRec);
                }
                if(null != fundingDataIterate.Progress_Pay_Date__c &&  fundingDataIterate.Progress_Pay_Date__c != oldFundingDataMap.get(fundingDataIterate.Id).Progress_Pay_Date__c)
                {
                    Opportunity oppRec = new Opportunity(id=fundingDataIterate.Opportunity_ID__c);
                    oppRec.Progress_Pay_Date__c =  DateTime.newInstance(fundingDataIterate.Progress_Pay_Date__c.year(), fundingDataIterate.Progress_Pay_Date__c.month(), fundingDataIterate.Progress_Pay_Date__c.day());
                    updateOpptyMap.put(fundingDataIterate.Opportunity_ID__c,oppRec);
                }
            }
            if(!updateUnderwritingMap.isEmpty())
                update updateUnderwritingMap.Values();
             
             if(!updateOpptyMap.isEmpty())
                update updateOpptyMap.values();
            //End    
            
        }catch(Exception ex)
        {
            system.debug('### Exception in FundingDataTriggerHandler.onAfterUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('FundingDataTriggerHandler.onAfterUpdate()',ex.getLineNumber(),'onAfterUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from onAfterUpdate() of '+ FundingDataTriggerHandler.class);
    }
     /**
    * Description: Added as part of ORANGE-5466
    *
    * @param newFundingDataList   List of Funding_Data__c records.
    * @param oldFundingDataMap      Map of Old Funding_Data__c records.
    */
    public void updateCPFUpdatedAPRTerm(Funding_Data__c[] newFundingDataList,Map<Id,Funding_Data__c> oldFundingDataMap){        
        for(Funding_Data__c fundDataObj : newFundingDataList){
            Funding_Data__c oldFundingData;
            if(oldFundingDataMap != null && !oldFundingDataMap.isEmpty()){
                oldFundingData = oldFundingDataMap.get(fundDataObj.Id);
            }
            
            //Update CPF_Updated_APR__c field 
            if(fundDataObj.C_O_CPF_APR__c != null && (oldFundingData == null || (oldFundingData != null && fundDataObj.C_O_CPF_APR__c != oldFundingData.C_O_CPF_APR__c))){
                fundDataObj.CPF_Updated_APR__c = fundDataObj.C_O_CPF_APR__c;
            }else if(fundDataObj.Cancelled_CPF_APR__c != null && (oldFundingData == null || (oldFundingData != null && fundDataObj.Cancelled_CPF_APR__c != oldFundingData.Cancelled_CPF_APR__c))){
                fundDataObj.CPF_Updated_APR__c = fundDataObj.Cancelled_CPF_APR__c;
            }else if(fundDataObj.Capital_Provider_Funded_APR__c != null && (oldFundingData == null || (oldFundingData != null && fundDataObj.Capital_Provider_Funded_APR__c != oldFundingData.Capital_Provider_Funded_APR__c))){
                fundDataObj.CPF_Updated_APR__c = fundDataObj.Capital_Provider_Funded_APR__c;
            }
            
            //Update CPF_Updated_Term__c field 
            if(fundDataObj.C_O_CPF_Term__c != null && (oldFundingData == null || (oldFundingData != null && fundDataObj.C_O_CPF_Term__c != oldFundingData.C_O_CPF_Term__c))){
                fundDataObj.CPF_Updated_Term__c = fundDataObj.C_O_CPF_Term__c;
            }else if(fundDataObj.Cancelled_CPF_Term__c != null && (oldFundingData == null || (oldFundingData != null && fundDataObj.Cancelled_CPF_Term__c != oldFundingData.Cancelled_CPF_Term__c))){
                fundDataObj.CPF_Updated_Term__c = fundDataObj.Cancelled_CPF_Term__c;
            }else if(fundDataObj.Capital_Provider_Funded_Term__c != null && (oldFundingData == null || (oldFundingData != null && fundDataObj.Capital_Provider_Funded_Term__c != oldFundingData.Capital_Provider_Funded_Term__c))){
                fundDataObj.CPF_Updated_Term__c = fundDataObj.Capital_Provider_Funded_Term__c;
            }
        }
    }
}