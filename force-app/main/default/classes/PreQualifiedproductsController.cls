/**
* Description: getting all Pre-Qual records .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma           07/12/2019          Created
******************************************************************************************/
global without sharing class PreQualifiedproductsController 
{
global Id PrequalId{get;set;}
global List<ProductWrapper> getProductDetails()
{
    System.debug('### Entered into getProductDetails() of '+PreQualifiedproductsController.class);
    Set<String> tierval = new Set<String>();
    Set<String> facilities = new Set<String>();
    
    String productTypes = Label.Home_Type_Products;
    List<string> lstProducttypes = new List<String>();
    for(string typeval : productTypes.split('&&')){
        lstProducttypes.add(typeval);
    }
            System.debug('***lstProducttypes********'+lstProducttypes);
    Prequal__c prequalObj = [SELECT  DTI__c, FICO__c, Pre_Qual_Status__c,Term_mo__c, APR__c, 
                                                 Lending_Facility__c,FNI_Status_Message__c, Decision__c, 
                                                 Opportunity__c,Opportunity__r.Install_State_Code__c,Opportunity__r.Project_Category__c, Project_Category__c,
                                                 Installer_Account__c, SSN__c, DOB__c,Mailing_State__c,Id,HashID__c, 
                                                 Name,FNI_SOAP_Request__c,FNI_SOAP_Response__c,Pre_Qual_Authorized__c,
                                                 Decision_Date_Time__c,Stip_Code_1__c,Decision_Stips__c, CreatedDate 
                                                 FROM Prequal__c where id =: PrequalId ];
    String Installeridval = prequalObj.Installer_Account__c;
                
    for(Prequal_Decision__c objprequalD : [Select id,Name,Facility_Name__c,Max_DTI__c,MaxPayment__c,Prequal_Descision__c,StipCode__c,SLF_Max_Line__c,Max_Line__c,Product_Tier__c,Product_Type__c,Prequal__c from Prequal_Decision__c where Prequal__c =: PrequalId and Prequal_Descision__c ='A' order by Product_Tier__c asc]){
        
        if(null != objprequalD.Product_Tier__c)    {
            if(string.valueOf(objprequalD.Product_Tier__c).equals('0'))
                tierval.add(string.valueOf(objprequalD.Product_Tier__c)); 
            else if(!tierval.contains('0')) 
                tierval.add(string.valueOf(objprequalD.Product_Tier__c)); 
        }  
         
        if(objprequalD.Facility_Name__c != null){
            facilities.add(objprequalD.Facility_Name__c);
        }
    }
                System.debug('***tierval ********'+tierval );
            System.debug('***facilities ********'+facilities );
    system.debug('###lstProducttypes:'+lstProducttypes);
    
    List<Product__c> productList = new List<Product__c>();
    
    if(prequalObj.Project_Category__c != '' && prequalObj.Project_Category__c == 'Home'){ 
        productList = [select id,Long_Term_Facility__c,Long_Term_Facility_Lookup__c,State_Code__c,Product_Tier__c,Installer_Account__c,Term_mo__c,APR__c,Product_Loan_Type__c,Product_Display_Name__c,NonACH_Prod_Display__c 
                                            from Product__c where Installer_Account__c=:Installeridval 
                                            AND State_Code__c=:prequalObj.Opportunity__r.Install_State_Code__c
                                            AND Internal_Use_Only__c = FALSE 
                                            AND Is_Active__c = TRUE and Long_Term_Facility_Lookup__c IN :facilities
                                            AND APR__c != null AND Term_mo__c != null AND Product_Tier__c IN: tierval AND Product_Loan_Type__c IN:lstProducttypes order By Term_mo__c,APR__c ASC];
    }else{
        productList = [select id,Long_Term_Facility__c,Long_Term_Facility_Lookup__c,State_Code__c,Product_Tier__c,Installer_Account__c,Term_mo__c,APR__c,Product_Loan_Type__c,Product_Display_Name__c,NonACH_Prod_Display__c from Product__c where Installer_Account__c =: Installeridval 
                                            AND State_Code__c =: prequalObj.Opportunity__r.Install_State_Code__c
                                            AND Internal_Use_Only__c = FALSE 
                                            AND Is_Active__c = TRUE and Long_Term_Facility_Lookup__c IN : facilities
                                            AND APR__c != null AND Term_mo__c != null AND Product_Tier__c IN: tierval AND Product_Loan_Type__c NOT IN: lstProducttypes order By Product_Loan_Type__c,Term_mo__c,APR__c ASC];
    }
    
    Map<String,Product__c> prodMap = new Map<String,Product__c>();
    Map<string,List<String>> productMap = new Map<string,List<String>>();
    for(Product__c prodIterate : productList){
        String productName = '';
        
        //productName = prodIterate.APR__c+'-'+prodIterate.Term_mo__c; 
        productName = prodIterate.Product_Display_Name__c; 
        //if(prequalObj.Project_Category__c != '' && prequalObj.Project_Category__c == 'Home') 
            productName = productName+'-'+prodIterate.Product_Loan_Type__c;         
           System.debug('@@productName'+productName);
        if(productName != '' && !prodMap.ContainsKey(productName))
        {
            prodMap.put(productName,prodIterate);
            productName = prodIterate.NonACH_Prod_Display__c; 
            if(!prodMap.ContainsKey(productName))
                prodMap.put(productName,prodIterate);
            
            
            string str='';
            if(prequalObj.Project_Category__c == 'Home'){
               str = prodIterate.Term_mo__c/12+' Years '+prodIterate.APR__c+'%';
            }else{
                str = prodIterate.Product_Loan_Type__c + ' ' + prodIterate.Term_mo__c/12+' Years '+prodIterate.APR__c+'%';
            }
            string interestType = '';
            string productLoanType = '';
            
            
            //Deferred Interest:
            if(prodIterate.APR__c > 10.99 && prodIterate.Product_Loan_Type__c == 'HIS')
                interestType = 'Deferred Interest';
            
            //Installments:
            if(prodIterate.APR__c > 0 && prodIterate.Product_Loan_Type__c == 'HII')
                interestType = 'Installment';
            
            //Installments Deferred:
            if(prodIterate.APR__c > 0 && prodIterate.APR__c < 10.99 && prodIterate.Product_Loan_Type__c == 'HIS')
                interestType = 'Installment + 12 mo Deferred Interest';
            
            //Equal Payment:
            if(prodIterate.APR__c == 0 &&  prodIterate.Product_Loan_Type__c == 'HII')
                interestType = 'Equal Payment';
            
            //No Payment:
            if(prodIterate.Product_Loan_Type__c == 'HIN')
                interestType = 'No Payment';
                
             // Solar:
            if(prodIterate.Product_Loan_Type__c == 'Solar')
                productLoanType = 'Solar';
            
            // SolarPlus Roof:
            if(prodIterate.Product_Loan_Type__c == 'SolarPlus Roof')
                productLoanType = 'SolarPlus Roof';
            
            // Solar Interest Only:
            if(prodIterate.Product_Loan_Type__c == 'Solar Interest Only')
                productLoanType = 'Solar Interest Only';
                                 
            // Integrated Solar Shingles:
            if(prodIterate.Product_Loan_Type__c == 'Integrated Solar Shingles')
                productLoanType = 'Integrated Solar Shingles';
            
            // Non-PV Stand Alone Battery:
            if(prodIterate.Product_Loan_Type__c == 'Non-PV Stand Alone Battery')
                productLoanType = 'Non-PV Stand Alone Battery';
            
            // Battery Only:
            if(prodIterate.Product_Loan_Type__c == 'Battery Only')
                productLoanType = 'Battery Only';
            
            system.debug('@@@@interestType' +interestType);
            system.debug('@@@@productLoanType ' +productLoanType);
            
            if(prequalObj.Project_Category__c == 'Home')
            {
                if(interestType == 'Installment')
                    str = str +' (Installment)';
                else
                    str = str +' (Promo)';
            }
             if(prequalObj.Project_Category__c == 'Home')
             {
                if(productMap.containsKey(interestType))
                {
                    List<String> productNameLst = productMap.get(interestType);
                    productNameLst.add(str);
                    productMap.put(interestType,productNameLst);
                    system.debug('##### productMap inside if'+productMap);
                }else
                    productMap.put(interestType,new List<string> {str});
                    system.debug('##### productMap outside if'+productMap);
             }else{
             
                if(productMap.containsKey(productLoanType))
                {
                    List<String> productNameLst = productMap.get(productLoanType);
                    productNameLst.add(str);
                    productMap.put(productLoanType,productNameLst);
                    system.debug('##### productMap inside if'+productMap);
                }else
                    productMap.put(productLoanType,new List<string> {str});
                    system.debug('##### productMap outside if'+productMap);
             }
           
                    
        }
                
    }
    
    List<ProductWrapper> prodWrapperLst = new List<ProductWrapper>();
    /*if(prequalObj.Project_Category__c == 'Home')
    {*/
        //Sorting the map
        //start
        List<String> aList = new List<String>();
        aList.addAll(productMap.keySet());
        aList.sort();
        //End
        
        //adding the Map to Wrapper List
        for(string strIterate : aList)
        {
            ProductWrapper prodWrapper = new ProductWrapper();
            prodWrapper.productNames = new List<String>();
            prodWrapper.interestType = strIterate;
            prodWrapper.productNames = productMap.get(strIterate);          
            
            prodWrapperLst.add(prodWrapper);
        }
   /* }else{
        //Sorting the map
        //start
        List<String> aList = new List<String>();
        aList.addAll(prodMap.keySet());
        //aList.sort();
        //End
        for(string solarStrIterate : aList)
        {
            ProductWrapper prodWrapper = new ProductWrapper();
            prodWrapper.productNames = new List<String>();
            prodWrapper.interestType = solarStrIterate;
            prodWrapper.productNames = productMap.get(solarStrIterate);       
            prodWrapperLst.add(prodWrapper);
        }
    }*/
    system.debug('lstProducts'+prodWrapperLst);
    System.debug('### Exit from getProductDetails() of '+PreQualifiedproductsController.class); 
    return prodWrapperLst;   
}
global Class ProductWrapper{
    global string interestType{get;set;}
    global List<String> productNames{get;set;}      
}
}