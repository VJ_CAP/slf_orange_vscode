/* Author : Veereandranath Jalla
* Date   : 11/14/2018
* Description: This class is used to create a task and invoked from Skuid page
*
*/
/*-------------------------------------------------------------------------------
*  Initial Version Written By           Date Modified By      Description
*  Veereandranath Jalla                 11/14/2018            Initial version
*/ 
global class CreateTaskForUnderWrittingCTRL {
    global class Input {
        @InvocableVariable(required=true) global Id strUnderWrittingId;
        global Input() {
            //  nothing to do
        }
        global Input(Id strUnderWrittingId) {
            this.strUnderWrittingId = (String.valueOf(strUnderWrittingId)).substring(0,15);
        }
    }
    global class Output {   
        @InvocableVariable global String Message;
    }
    public class TaskException extends Exception{}
    
    @InvocableMethod(label='createtaskforUnderWritting')
    public static List<Output> InsertTask(List<Input> lstInput){
        List<Output> lstOutPuts = new List<Output>();  
        Output objOutput = new Output();    
        Set<Id> UnderWrittingIds = new Set<Id>();
        
        system.debug('Task Record is created..');
        
        for(input objI:lstInput){
            UnderWrittingIds.add(objI.strUnderWrittingId);
        }
        
        Underwriting_File__c objUWFile = [select Id,Project_Status__c,Opportunity__c,Account_Name__c,M0_Ready_for_Review_Date_Time__c,
                                          Permit_Ready_for_Review_Date_Time__c,Kitting_Ready_for_Review_Date_Time__c,Approved_for_Payments__c,
                                          Inspection_Ready_for_Review_Date_Time__c,CO_Approval_Requested_Date__c,CO_Approval_Date__c,
                                          CO_Ready_for_Review_Date_Time__c,CO_Ready_for_Review_Age__c, M0_Ready_for_Review_Age__c,
                                          M1_Ready_for_Review_Date_Time__c,M1_Ready_for_Review_Age__c,M2_Ready_for_Review_Date_Time__c,
                                          M2_Ready_for_Review_Age__c,Permit_Ready_for_Review_Age__c,Kitting_Ready_for_Review_Age__c,
                                          Inspection_Ready_for_Review_Age__c,Opportunity__r.Has_Open_Stip_M0__c,
                                          HIF_Ready_for_Review_Date_Time__c,HIF_Ready_for_Review_Age__c,
                                          Opportunity__r.Project_Category__c,HIDR_Ready_for_Review_Age__c,HIDR_Ready_for_Review_Date_Time__c,
                                          (Select id, Latest_LT_Loan_Agreement_Date_Time__c,Latest_Install_Contract_Date_Time__c 
                                           from Box_Fields__r where Latest_LT_Loan_Agreement_Date_Time__c != null AND 
                                           Latest_Install_Contract_Date_Time__c != null),
                                          (select Id,OwnerId,Owner.Name from Tasks where whatid in:UnderWrittingIds and 
                                           status = 'Open' and Type != 'Post-NTP Issue'),
                                          (select Id,Status__c,SLF_Approved__c from Draw_Requests__r where Status__c = 'Approved' and 
                                           SLF_Approved__c = null)
                                          from Underwriting_File__c where Id in:UnderWrittingIds limit 1];
        
        if(objUWFile != null){
            String subject = '';
            if(!objUWFile.Tasks.isEmpty()){               
                objOutput.Message = objUWFile.Tasks.get(0).Owner.Name+' is currently working on the '+objUWFile.Account_Name__c+' review. Please select the next record in the queue.';
                lstOutPuts.add(objOutput);
                //return lstOutPuts;
                throw new TaskException(objUWFile.Tasks.get(0).Owner.Name+' is currently working on the '+objUWFile.Account_Name__c+' review. Please select the next record in the queue.');                
            }
            Task objTask = new Task();
            objTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Underwriting Task').getRecordTypeId();
            objTask.WhatId = objUWFile.id;//strUnWrId;
            objTask.OwnerId = UserInfo.getUserId();
            objTask.Status = 'Open';
            objTask.ActivityDate = system.today();
            objTask.Start_Date_Time__c = system.now();
            objTask.IsReminderSet = true;
            if(objUWFile.CO_Approval_Requested_Date__c != null && objUWFile.CO_Approval_Date__c == null){
                objTask.Type = 'CO Review'; 
                objTask.ReminderDateTime = system.now().addMinutes(45);
                objTask.Due_Date_Time__c = system.now().addMinutes(60);
                objTask.Ready_for_Review_Date_Time__c = objUWFile.CO_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.CO_Ready_for_Review_Age__c;
                subject = 'CO';
            }
            else if((objUWFile.Project_Status__c == 'M0' || objUWFile.Project_Status__c == 'Pending Loan Docs') && objUWFile.Opportunity__r.Project_Category__c == 'Solar'){
                objTask.Type = 'M0 Review'; 
                objTask.ReminderDateTime = system.now().addMinutes(50);
                objTask.Due_Date_Time__c = system.now().addMinutes(60);
                objTask.Ready_for_Review_Date_Time__c = objUWFile.M0_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.M0_Ready_for_Review_Age__c;
                subject = objUWFile.Project_Status__c;
            }else if(objUWFile.Project_Status__c == 'M1'){
                objTask.Type = 'M1 Review';
                objTask.Ready_for_Review_Date_Time__c = objUWFile.M1_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.M1_Ready_for_Review_Age__c;
                objTask.Due_Date_Time__c = system.now().addMinutes(120);
                objTask.ReminderDateTime = system.now().addMinutes(100);
                subject = objUWFile.Project_Status__c;
            }else if(objUWFile.Project_Status__c == 'M2'){
                objTask.Type = 'M2 Review';
                objTask.Ready_for_Review_Date_Time__c = objUWFile.M2_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.M2_Ready_for_Review_Age__c;
                objTask.Due_Date_Time__c = system.now().addMinutes(15);
                objTask.ReminderDateTime = system.now().addMinutes(10);
                subject = objUWFile.Project_Status__c;
            }else if(objUWFile.Project_Status__c == 'Permit'){ // 1929 Added else for 1929
                objTask.Type = objUWFile.Project_Status__c+ ' Review';
                objTask.Ready_for_Review_Date_Time__c = objUWFile.Permit_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.Permit_Ready_for_Review_Age__c;
                objTask.Due_Date_Time__c = system.now().addMinutes(30);
                objTask.ReminderDateTime = system.now().addMinutes(20);
                subject = objUWFile.Project_Status__c;
            }else if(objUWFile.Project_Status__c == 'Kitting'){ // 1929 Added else for 1929
                objTask.Type = objUWFile.Project_Status__c+ ' Review';
                objTask.Ready_for_Review_Date_Time__c = objUWFile.Kitting_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.Kitting_Ready_for_Review_Age__c;
                objTask.Due_Date_Time__c = system.now().addMinutes(30);
                objTask.ReminderDateTime = system.now().addMinutes(20);
                subject = objUWFile.Project_Status__c;
            }else if(objUWFile.Project_Status__c == 'Inspection'){ // 1929 Added else for 1929
                objTask.Type = objUWFile.Project_Status__c+ ' Review';
                objTask.Ready_for_Review_Date_Time__c = objUWFile.Inspection_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.Inspection_Ready_for_Review_Age__c;
                objTask.Due_Date_Time__c = system.now().addMinutes(30);
                objTask.ReminderDateTime = system.now().addMinutes(20);
                subject = objUWFile.Project_Status__c;
            }//Added As part of Orange - 3964 - Start
            else if(!objUWFile.Box_Fields__r.isEmpty() && objUWFile.Opportunity__r.Project_Category__c == 'Home' && 
                objUWFile.Approved_for_Payments__c == false && 
                objUWFile.Opportunity__r.Has_Open_Stip_M0__c == false){ 
                objTask.Type = 'HIDR Review';
                objTask.Ready_for_Review_Date_Time__c = objUWFile.HIDR_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.HIDR_Ready_for_Review_Age__c;
                objTask.Due_Date_Time__c = system.now().addMinutes(15);
                objTask.ReminderDateTime = system.now().addMinutes(10);
                subject = 'HIDR ';
            }            
            else if(!objUWFile.Draw_Requests__r.isEmpty() && objUWFile.Opportunity__r.Has_Open_Stip_M0__c==false && 
            objUWFile.Opportunity__r.Project_Category__c == 'Home'){ 
                objTask.Type = 'HIF Review';
                objTask.Ready_for_Review_Date_Time__c = objUWFile.HIF_Ready_for_Review_Date_Time__c;
                objTask.Ready_for_Review_Age__c = objUWFile.HIF_Ready_for_Review_Age__c;
                objTask.Due_Date_Time__c = system.now().addMinutes(45);
                objTask.ReminderDateTime = system.now().addMinutes(35);
                subject = 'HIF ';
            }//Added As part of Orange - 3964 - End
            
            objTask.Subject = subject + 'Review' + '['+objUWFile.Id+'],'+'['+objUWFile.Opportunity__c+']';
            //objTask.Due_Date_Time__c = 
            try{
                system.debug('Task Record is created..');
                insert objTask;
                objOutput.Message = objTask.Id+ 'successfully created';
                lstOutPuts.add(objOutput);
                return lstOutPuts;
            }Catch(Exception ex){
                objOutput.Message = ex.getMessage();
                lstOutPuts.add(objOutput);
                return lstOutPuts;
            }
        }
        return null;        
    }
}