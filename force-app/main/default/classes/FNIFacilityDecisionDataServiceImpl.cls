/**
* Description: User and Contact Creation logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Brahmeswar             06/04/2018           Created
******************************************************************************/

public without sharing class FNIFacilityDecisionDataServiceImpl extends RestDataServiceBase{
   /*
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+FNIFacilityDecisionDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean respBean = new UnifiedBean(); 
        respBean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        Integer RandomNo;
        String FinalFacility ='';
        string errMsg = '';
        Map<string,string> mapConFieldInfo = new Map<string,string>();
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        List<String> FacilityNames = new List<String>();
        SET<String> PrequalIds = new SET<String>();
        String producType = '';
        Savepoint sp = Database.setSavepoint();
        try
        { 
            if(reqData.projects != null && reqData.projects[0].credits != null)
            {
                if(String.isNotBlank(reqData.projects[0].credits[0].fniSoftResp)){
                    if(reqData.prequal != null){
                        String PrequalId = reqData.prequal.id;
                        Map<string,string> LenderStatus = new map<string,string>();
                        String Installeridval = '';
                        Map<String,Product__c> LenderNameAndProductid = new MAp<string,Product__c>();
                        
                        System.debug('###producTyp '+producType);
                        //Added product realted fields in query by Deepika as part of Orange - 4341
                        Prequal__c objprequal = [SELECT  DTI__c, FICO__c, Pre_Qual_Status__c,Term_mo__c,Opportunity__r.SLF_Product__c, APR__c, Lending_Facility__c,FNI_Status_Message__c, Decision__c, 
                                                 Opportunity__c,Opportunity__r.Install_State_Code__c,Opportunity__r.Product_Loan_Type__c,Installer_Account__c, SSN__c, DOB__c,Mailing_State__c,Id, Name,FNI_SOAP_Request__c,FNI_SOAP_Response__c,Opportunity__r.SLF_Product__r.Promo_Term__c,Opportunity__r.SLF_Product__r.Promo_Percentage_Payment__c,Opportunity__r.SLF_Product__r.Promo_Fixed_Payment__c,Opportunity__r.SLF_Product__r.Promo_Percentage_Balance__c,Opportunity__r.SLF_Product__r.Interest_Type__c,Opportunity__r.SLF_Product__r.Promo_APR__c                                              
                                                 FROM Prequal__c where id=:PrequalId];
                        Installeridval = objprequal.Installer_Account__c;
                        System.debug('### objprequal: '+objprequal);
                        System.debug('### Opportunity__r.SLF_Product__c: '+objprequal.Opportunity__r.SLF_Product__c);
                        if(!String.isBlank(objprequal.Opportunity__r.Product_Loan_Type__c)){
                            producType = objprequal.Opportunity__r.Product_Loan_Type__c;
                        }else{
                            producType = 'Solar';
                        }
                        //Removed ACH__c from below query by Deepika as part of Orange - 3622
                        for(Product__c prdobj: [select id,Long_Term_Facility__c,Long_Term_Facility_Lookup__c,Installer_Account__c,
                                                Term_mo__c,APR__c,Promo_Term__c
                                                from Product__c where Installer_Account__c=:Installeridval 
                                                AND Term_mo__c=:objprequal.Term_mo__c 
                                                AND APR__c=:objprequal.APR__c 
                                                AND ACH__c=:TRUE 
                                                AND State_Code__c=:objprequal.Opportunity__r.Install_State_Code__c
                                                AND Internal_Use_Only__c = FALSE 
                                                AND Is_Active__c = TRUE 
                                                //Added product realted fields condition by Deepika as part of Orange - 4341 - Start
                                                AND Product_Loan_Type__c =:producType
                                                AND Promo_Term__c =:objprequal.Opportunity__r.SLF_Product__r.Promo_Term__c
                                                AND Promo_APR__c =:objprequal.Opportunity__r.SLF_Product__r.Promo_APR__c
                                                AND Promo_Percentage_Payment__c =:objprequal.Opportunity__r.SLF_Product__r.Promo_Percentage_Payment__c
                                                AND Promo_Fixed_Payment__c =:objprequal.Opportunity__r.SLF_Product__r.Promo_Fixed_Payment__c
                                                AND Promo_Percentage_Balance__c =:objprequal.Opportunity__r.SLF_Product__r.Promo_Percentage_Balance__c
                                                AND Interest_Type__c =:objprequal.Opportunity__r.SLF_Product__r.Interest_Type__c] ){
                                                //Added product realted fields condition by Deepika as part of Orange - 4341 - End
                            LenderNameAndProductid.put(prdobj.Long_Term_Facility__c,prdobj);
                            System.debug('prdobj.Promo_Term__c::'+prdobj.Promo_Term__c);
                        }
                        System.debug('###LenderNameAndProductid'+LenderNameAndProductid);
                        DOM.XmlNode rootNode = getFNIResponseNode(reqData.projects[0].credits[0].fniSoftResp);
                        
                        List<LenderWrapper> lstLenderWrap = parseFNIResponseXML(rootNode);
                        List<Prequal_Decision__c> lstprequalDesion = new List<Prequal_Decision__c>();
                        Prequal_Decision__c prequalDesion;
                        if(!lstLenderWrap.isEmpty()){
                            for(LenderWrapper objlender: lstLenderWrap){
                                System.debug('###objlender '+objlender);
                                prequalDesion = new Prequal_Decision__c();
                                if(!LenderNameAndProductid.isEmpty() && LenderNameAndProductid.get(objlender.FacilityName) != null)
                                    prequalDesion.Facility_Name__c = LenderNameAndProductid.get(objlender.FacilityName).Long_Term_Facility_Lookup__c;
                                prequalDesion.Max_DTI__c = objlender.MaxDTI!=null ? Decimal.valueOf(objlender.MaxDTI) : 0;
                                prequalDesion.MaxPayment__c = objlender.MaxPayment!=null ? Decimal.valueOf(objlender.MaxPayment) : 0;
                                prequalDesion.Prequal__c = PrequalId;
                                prequalDesion.Max_Line__c = objlender.Line!=null && objlender.Line!='' ? Decimal.valueOf(objlender.Line) : 0;
                                prequalDesion.Prequal_Descision__c = objlender.decision;
                                if(!LenderNameAndProductid.isEmpty() && LenderNameAndProductid.get(objlender.FacilityName) != null)
                                    prequalDesion.SLF_Product__c = LenderNameAndProductid.get(objlender.FacilityName).id;
                                prequalDesion.StipCode__c = objlender.stipRsn;
                                prequalDesion.Product_Type__c = objlender.ProductType;
                                LenderStatus.put(objlender.FacilityName,objlender.decision);
                                lstprequalDesion.add(prequalDesion);
                            }
                        }
                        System.debug('### lstprequalDesion: '+lstprequalDesion);
                        if(!lstprequalDesion.isEmpty())
                            insert lstprequalDesion;
                            
                        String FacilityVals = LoadBalanceUtilities.getFacility(Installeridval,objprequal.Mailing_State__c,LenderStatus,Integer.valueOf(objprequal.Term_mo__c));
                        String FinalFacilityName='';
                        if(FacilityVals.contains(':::')){
                            List<String> lststr = FacilityVals.split(':::');
                            if(!lststr.isEmpty()){
                                FinalFacilityName = lststr[0];
                                RandomNo = Integer.valueOf(lststr[1]);
                            }
                        }else{
                            FinalFacilityName = FacilityVals;
                        }
                        
                        System.debug('### Final Facility Name from Load balance: '+FinalFacilityName);
                        if(FinalFacilityName == 'NoRecords'){
                            errMsg = ErrorLogUtility.getErrorCodes('335', null);
                            respBean.returnCode = '335';
                            UnifiedBean.errorWrapper errorMsg = new UnifiedBean.errorWrapper();
                            errorMsg.errorMessage = errMsg;
                            respBean.error.add(errorMsg);
                            iRestRes = (IRestResponse)respBean;
                            return iRestRes;
                        }
                        
                        Opportunity ObjOpp = [select id,SyncedQuoteid,SLF_Product__c,Long_Term_APR__c,ACH__c,Long_Term_Term__c,Desync_Bypass__c,SLF_Product__r.ACH__c,SLF_Product__r.APR__c,SLF_Product__r.Term_mo__c from opportunity where id=:objprequal.Opportunity__c];
                        //Removed FinalProduct.ACH__c == ObjOpp.SLF_Product__r.ACH__c by Deepika as part of Orange - 3622
                        if(ObjOpp != null && ObjOpp.SLF_Product__c != null && LenderNameAndProductid.get(FinalFacilityName) != null){
                            Product__c FinalProduct  = LenderNameAndProductid.get(FinalFacilityName);
                            if(ObjOpp.SLF_Product__c!=null && (ObjOpp.SLF_Product__c != LenderNameAndProductid.get(FinalFacilityName).id && FinalProduct.APR__c == ObjOpp.SLF_Product__r.APR__c && FinalProduct.Term_mo__c == ObjOpp.SLF_Product__r.Term_mo__c) || (ObjOpp.SLF_Product__c == LenderNameAndProductid.get(FinalFacilityName).id) ){
                                ObjOpp.Desync_Bypass__c = true;
                            }
                        }
                        if(ObjOpp != null && ObjOpp.SyncedQuoteid != null && LenderNameAndProductid.get(FinalFacilityName) != null && ObjOpp.Desync_Bypass__c){
                            Quote objquote = new Quote();
                            objquote.id = ObjOpp.SyncedQuoteid;
                            objquote.SLF_Product__c = LenderNameAndProductid.get(FinalFacilityName).id;
                            update objquote;
                        }
                        
                        if(LenderNameAndProductid.get(FinalFacilityName) != null){
                            ObjOpp.SLF_Product__c = LenderNameAndProductid.get(FinalFacilityName).id;
                            if(reqData.projects[0].isACH != null){
                                ObjOpp.isACH__c = reqData.projects[0].isACH;//Added as part of Orange - 3622
                            }
                            if(!ObjOpp.Desync_Bypass__c){
                                ObjOpp = SyncDesyncUtilities.desyncQuoteFields(ObjOpp);
                            }
                            ObjOpp.Desync_Bypass__c = false;
                            if(ObjOpp != null)
                                update ObjOpp;
                            System.debug('### Updated opportunity: '+ObjOpp);
                        }
                        SLF_Credit__c objCredit = new SLF_Credit__c();
                        objCredit.id = reqData.projects[0].credits[0].id;
                        if(LenderNameAndProductid.get(FinalFacilityName) != null){
                            objCredit.SLF_Product__c = LenderNameAndProductid.get(FinalFacilityName).id;
                            if(objCredit != null)
                                update objCredit;
                        }
                        Prequal__c prequalobj = new Prequal__c(id =PrequalId,Random_Number__c=RandomNo,Final_Facility__c=FinalFacilityName);
                        update prequalobj;
                        
                        sp = Database.setSavepoint(); //added by Dax
                        
                        String creditExtRefNum = SLFUtility.guidIdGenerator();
                        Map<string,Account> accMap;
                        Opportunity oppSTLTRec =[Select id,Name,SLF_Product__c,StageName,Installer_Account__c,
                                                    AccountId,SLF_Product__r.Term_mo__c,SLF_Product__r.APR__c,  CloseDate,Short_Term_APR__c,
                                                    Short_Term_Term__c,Short_Term_Facility__c,Installer_Account__r.FNI_Domain_Code__c,
                                                    Monthly_Payment__c,Monthly_Payment_calc__c,Monthly_Payment_Escalated_Single_Loan__c,
                                                    Escalated_Monthly_Payment_calc__c,Long_Term_Term__c,Long_Term_Facility__c,Long_Term_APR__c,
                                                    Combined_Loan_Amount__c,Install_Cost_Cash__c,Initial_Payment__c,
                                                    Customer_has_authorized_credit_hard_pull__c,RecordTypeId,Install_Postal_Code__c,
                                                    Install_Street__c,Install_City__c,Install_State_Code__c,Co_Applicant__c,
                                                    SyncedQuoteId,SyncedQuote.Unofficial_HIS_PMT_Value__c,Short_Term_Loan_Amount_Manual__c,Long_Term_Loan_Amount_Manual__c,
                                                    Application_Status__c,Sales_Representative_Email__c,Sales_Representative_Manager_Email__c,
                                                    language__c,Product_Loan_Type__c,Installer_Account__r.Risk_Based_Pricing__c,ProductTier__c from Opportunity where id =: ObjOpp.Id];
                       
                        if(oppSTLTRec.AccountId != null || oppSTLTRec.Co_Applicant__c != null){
                              accMap = new Map<string,Account>([Select id,Credit_Run__c,firstName,lastName,SSN__c,Last_four_SSN__c,PersonEmail,PersonBirthdate,
                                                    Phone,MiddleName,Marital_Status__c,
                                                    personOtherPhone,BillingCountry,BillingStreet,BillingCity,BillingStateCode,
                                                    ShippingStateCode,BillingState,BillingPostalCode,ShippingCountry,
                                                    ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,Annual_Income__c,
                                                    Employer_Name__c,Length_of_Employment_Months__c,Length_of_Employment_Years__c,
                                                    Job_Title__c,RecordTypeId from Account where id =: oppSTLTRec.AccountId or id =:oppSTLTRec.Co_Applicant__c]);
                            
                        }    
                                
                        String HardpullXMLStr;
                        if(!accMap.isEmpty()){
                            Account acc1,acc2;
                            acc1 = (accMap.containsKey(oppSTLTRec.AccountId))?accMap.get(oppSTLTRec.AccountId):null;
                            acc2 = (accMap.containsKey(oppSTLTRec.Co_Applicant__c))?accMap.get(oppSTLTRec.Co_Applicant__c):null;
                        HardpullXMLStr = CreditWaterFall.buildHardPullFNIReq(acc1,acc2,oppSTLTRec,objCredit,creditExtRefNum);
                        }
                        System.debug('### Hardpull SOAP Body: '+HardpullXMLStr);
                        
                        respBean = SLFUtility.getUnifiedBean(new set<Id>{oppSTLTRec.Id},'Orange',false);
                        for(UnifiedBean.OpportunityWrapper  projectIterate : respBean.projects){
                            for(UnifiedBean.CreditsWrapper creditIterate : projectIterate.credits){
                                if(creditIterate.Id == objCredit.id)
                                {
                                    creditIterate.fniLTReq  = HardpullXMLStr;
                                }
                            }
                        }
                        respBean.returnCode = '200';
                        iRestRes = (IRestResponse)respBean;
                        System.debug('### Response Bean: '+iRestRes);
                        return iRestRes;
                    }else{
                        
                        
                    }
                }
            }   
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400', null);
            gerErrorMsg(respBean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)respBean;
            system.debug('### FNIFacilityDecisionDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('FNIFacilityDecisionDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### iRestRes '+iRestRes);
        system.debug('### Exit from transformOutput() of '+CreateUserDataServiceImpl.class);

        return iRestRes;
    }
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
    
    public static DOM.XMLNode getFNIResponseNode(String xmlString) {
        system.debug('### Entered into getFNIResponseNode() of '+ FNIFacilityDecisionDataServiceImpl.class);
        String sResponseString = '';
        DOM.Document doc=new DOM.Document();
        DOM.XMLNode nodevalue = null;
        try{
            Integer start = xmlString.indexOf('<ns3:RESPONSE');
            Integer stop = xmlString.indexOf('</ns3:RESPONSE>');
            if(start >= 0 && stop > 0) {
                sResponseString = xmlString.substring(start, stop+15);
                System.debug('Response XML String: '+sResponseString);
                doc.load(sResponseString);
                DOM.XMLNode responseNode = doc.getRootElement();
                
                //Get Childs of Response Node
                for(DOM.XMLNode childNode : responseNode.getChildElements()) {
                    if(childNode.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                        if(childNode.getName().trim() == 'PrequalDecisions') {
                            nodevalue = childNode;
                        }
                    }
                }
                System.debug('Node Name: '+ nodevalue.getName());
                System.debug('Node Name: '+nodevalue) ;
            }
        }
        catch(Exception ex)
        {
            system.debug('### FNIFacilityDecisionDataServiceImpl.getFNIResponseNode():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('FNIFacilityDecisionDataServiceImpl.getFNIResponseNode()',ex.getLineNumber(),'getFNIResponseNode()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        
        system.debug('### Exit from getFNIResponseNode() of '+ FNIFacilityDecisionDataServiceImpl.class);
        return nodevalue;
    }
    
    public static List<LenderWrapper> parseFNIResponseXML(DOM.XMLNode prequalDecNode) {
        system.debug('### Entered into parseFNIResponseXML() of '+FNIFacilityDecisionDataServiceImpl.class);
        List<LenderWrapper> lstLenderWrap = new List<LenderWrapper>();
        
        for (Dom.XMLNode lenderNode: prequalDecNode.getChildElements()) {
            if(lenderNode.getName()=='Lender'){
                LenderWrapper lenderWrap = new LenderWrapper();
                
                for(Dom.XMLNode lenderChild: lenderNode.getChildElements()){
                    if(lenderChild.getName()=='Name')
                        lenderWrap.FacilityName=lenderChild.getText().trim();
                    if(lenderChild.getName()=='Decision')
                        lenderWrap.decision=lenderChild.getText().trim();
                    if(lenderChild.getName()=='StipRsn')
                        lenderWrap.stipRsn=lenderChild.getText().trim();
                    if(lenderChild.getName()=='Line')
                        lenderWrap.Line=lenderChild.getText().trim();
                    if(lenderChild.getName()=='MaxPayment')
                        lenderWrap.MaxPayment=lenderChild.getText().trim();
                    if(lenderChild.getName()=='MaxDTI')
                        lenderWrap.MaxDTI=lenderChild.getText().trim();
                    if(lenderChild.getName()=='ProductType')
                        lenderWrap.ProductType=lenderChild.getText().trim();
                }
               
                lstLenderWrap.add(lenderWrap);
            }
        }
        system.debug('### Exit from parseFNIResponseXML() of '+ FNIFacilityDecisionDataServiceImpl.class);
        return lstLenderWrap;
    }
    
    public class LenderWrapper{
        public String FacilityName {get;set;}
        public String decision {get;set;}
        public String stipRsn {get;set;}
        public String Line {get;set;}
        public String MaxPayment {get;set;}
        public String MaxDTI {get;set;}
        public string ProductType{get;set;}
        
    }
}