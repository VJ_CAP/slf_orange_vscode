/**
* Description: Consumer Portal Token generating API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Sumalatha Ganga           11/18/2019         Created
******************************************************************************************/
public with sharing class ConsumerTokenDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: This class is used to generate the accesstoken for Consumer. 
    * Request from UI will be, Lastname of the consumer , Last 4 digits for consumer's SSN number & Hash id of the project.
    * Response from this class will be, defalut owner's username & password of the given project HashId to generate Token from Boomi end.
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+ConsumerTokenDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean respData = new UnifiedBean();
        respData.error = new List<UnifiedBean.ErrorWrapper>();
        respData.users = new List<UnifiedBean.UsersWrapper>();
        
        IRestResponse iRestRes;
        string errMsg = '';
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try{
            // quering Opportunity with the Hash id provided in Request.
            List<opportunity> OppList = [select id,AccountId, Installer_Account__c,Account.SSN__c,Account.Last_four_SSN__c, Account.lastname from Opportunity where Hash_Id__c=: reqdata.hashId];
            if(!OppList.isEmpty()){   
                String lastFourSSN = '';
                if(null != OppList[0].Account.SSN__c)
                {
                    lastFourSSN = OppList[0].Account.SSN__c.substring(OppList[0].Account.SSN__c.length()-4,OppList[0].Account.SSN__c.length());
                }                 
                // validating the ssn & Lastname provided in the request.
                if(reqData.lastFourSSN == lastFourSSN && reqData.lastName == OppList[0].Account.lastName)
                {
                    if(oppList[0].Installer_Account__c != null){
                        // Quering the username & pasword from the defalut user of the installer account.
                        List<User> lstUsers = [SELECT Id, Username, Password__c from User where 
                                               contact.AccountId =: oppList[0].Installer_Account__c AND Default_User__c =: true and IsActive =: true ];
                        system.debug('--lstUsers-->'+lstUsers);
                        if(!lstUsers.isEmpty()){
                            //If list has more than one record get first record
                            UnifiedBean.UsersWrapper Userinfo = new UnifiedBean.UsersWrapper();
                            Userinfo.username = lstUsers[0].username;
                            Userinfo.password = lstUsers[0].Password__c;
                            respData.users.add(Userinfo);
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
                             
                        }else{
                            errMsg = ErrorLogUtility.getErrorCodes('611',null);
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respData.error.add(errorMsg);
                            respData.returnCode = '611';
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
                        }
                    }
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('610',null);
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    respData.error.add(errorMsg);
                    respData.returnCode = '610';
                    iRestRes = (IRestResponse)respData;
                    return iRestRes;
                }
                    
            }else{                
                errMsg = ErrorLogUtility.getErrorCodes('609',null);                
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);                
                respData.error.add(errorMsg);
                respData.returnCode = '609';
                iRestRes = (IRestResponse)respData;
                system.debug('### iRestRes'+iRestRes);
                return iRestRes;                
            }
                    
        }catch(Exception ex){
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400',null);
            
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            respData.error = new List<UnifiedBean.ErrorWrapper>();
            respData.error.add(errorMsg);
            respData.returnCode = '400';
            system.debug('### ConsumerTokenDataServiceImpl.transformOutput():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('ConsumerTokenDataServiceImpl.transformOutput()',ex.getLineNumber(),'transformOutput()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }    
        system.debug('### Exit from transformOutput() of '+ConsumerTokenDataServiceImpl.class);
        system.debug('### iRestRes'+iRestRes);
        iRestRes = (IRestResponse)respData;
        return iRestRes;
    }
}