/******************************************************************************************
* Description: Test class to cover DocusignStatusTriggerHandler and DocusignTrg
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Ashish Rai            10/31/2017            Created
******************************************************************************************/
    
    
    @isTest
    public class DocusignStatusTriggerHandlerTest { 
        /*@testsetup static void createtestdata(){
            SLFUtilityDataSetup.initData();
        }*/
        
        private testMethod static void docusignTriggerHandlerTest(){            
            
            
            Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();          
            
            //get Custom setting data.
            SLFUtilityTest.createCustomSettings();
            SLF_Boomi_API_Details__c apidetails4 = new SLF_Boomi_API_Details__c();
            apidetails4.Name ='DocusignAPI';
            apidetails4.Username__c ='sdeveloperdocusign@gmail.com';
            apidetails4.Password__c ='Sunlight18!';
            apidetails4.Endpoint_URL__c ='https://demo.docusign.net/restapi/v2/accounts/4143849/envelopes/';
            apidetails4.APIKey__c ='91805a55-fd52-4b55-8d8d-0f268dc32370';
            apidetails4.SF_UserId__c ='DocusignAPI';
            insert apidetails4;   
            
            // Inserting Account records        
            Account Acc = new Account();
            Acc.name = 'Test Account';
            Acc.Push_Endpoint__c = 'https://requestb.in';
            Acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            Acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.BillingStateCode = 'CA';
            Acc.Installer_Email__c = 'test@slf.com';
            Acc.FNI_Domain_Code__c = 'Solar';
            Acc.Installer_Legal_Name__c='Bright Solar Planet';
            Acc.Subscribe_to_Push_Updates__c = true;
            Acc.Domain_list__c = 'Test';
            Acc.Solar_Enabled__c = true; 
            Acc.ACH_true__c = 'X';
            Acc.Requires_ACH_Validation__c = True;
            Acc.Routing_Number__c = '012345678';
            Acc.LexisNexis_IDLookupResult__c = 'Passed';
            Acc.LexisNexis_IDQuestionsResult__c = 'Failed';
            insert Acc;
            
            Draw_Allocation__c draw = new Draw_Allocation__c();
            draw.Account__c = acc.id;
            draw.Draw_Allocation__c = 0.2;
            draw.Level__c = '1';  
            insert draw;
                                
            //Insert SLF Product Record
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            insert prod;
            
            //Inserting opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName1';
            opp.SLF_Product__c = prod.id;
            opp.Hash_Id__c = 'LSYVEfyt16sHA3tzT60cekCSqlMmTariHrjdtLuOcf8=';
            opp.Customer_has_authorized_credit_hard_pull__c = true;
            
            opp.CloseDate = system.today();
            opp.StageName = 'Closed Won';
            opp.AccountId = Acc.id;
            opp.Installer_Account__c = Acc.id;
            opp.Co_Applicant__c = Acc.id;
            opp.Install_State_Code__c = 'CA'; 
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'Solar||123';
            opp.Change_Order_Identified__c = true;
            opp.Change_Order_Status__c = 'Pending';
            opp.Co_applicant_present_on_loan_agreement__c = NULL;   
            opp.Co_Applicant_Approved_for_Credit__c = NULL;
            opp.LT_Loan_Name_s_Signature_is_Present__c = false;
            opp.LT_Loan_Name_s_Matches_Credit_Bureau__c = false;
            opp.EDW_Originated__c = false;
            opp.Project_Category__c = 'Solar';
            opp.ACH_APR_Process_Required__c = true;
            opp.Change_Order_Number__c = 1;
            insert opp;
            
            List<Opportunity> oppList = new List<Opportunity>();

            Opportunity opp1 = new Opportunity();
            opp1.name = 'OpName';
            opp1.SLF_Product__c = prod.id;
            opp1.Hash_Id__c = 'LSYVEfyt16sHA3tzT60cqkCSqlMmTariHrjdtLuOcf8=';
            opp1.Customer_has_authorized_credit_hard_pull__c = true;
            
            opp1.CloseDate = system.today();
            opp1.StageName = 'Archive';
            opp1.AccountId = Acc.id;
            opp1.Installer_Account__c = Acc.id;
            opp1.Co_Applicant__c = Acc.id;
            opp1.Install_State_Code__c = 'CA'; 
            opp1.Combined_Loan_Amount__c = 15000;
            opp1.Partner_Foreign_Key__c = 'Solar||133';
            opp1.Change_Order_Identified__c = true;
            opp1.Change_Order_Status__c = 'Pending';
            opp1.Co_applicant_present_on_loan_agreement__c = NULL;   
            opp1.Co_Applicant_Approved_for_Credit__c = NULL;
            opp1.LT_Loan_Name_s_Signature_is_Present__c = false;
            opp1.LT_Loan_Name_s_Matches_Credit_Bureau__c = false;
            opp1.EDW_Originated__c = false;
            opp1.Project_Category__c = 'Solar';
            opp1.Opportunity__c = opp.Id;
            insert opp1;

            
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.Id;
            credit.IsSyncing__c = true;
            credit.Status__c = 'Auto Approved';
            credit.LT_StipRsn1__c = 'STIP847';
            credit.LT_StipRsn2__c = 'STIP847';
            credit.LT_StipRsn3__c = 'STIP847';
            credit.LT_StipRsn4__c = 'STIP847';
            insert credit;
            
            List<Stipulation_Data__c> stipLst = new List<Stipulation_Data__c>();
            Stipulation_Data__c ACHStipData = new Stipulation_Data__c();
            ACHStipData.Name = 'ACH';
            ACHStipData.CL_Code__c = '';
            ACHStipData.Insert_New_Stip__c = true;
            ACHStipData.Display_Folder_Text__c = 'Voided Check';
            ACHStipData.Review_Classification__c = 'SLS I';
            stipLst.add(ACHStipData);
            
            Stipulation_Data__c stipData = new Stipulation_Data__c();
            stipData.Name = 'OOW';
            stipData.CL_Code__c = 'STIP834';
            stipData.Insert_New_Stip__c = true;
            stipData.Review_Classification__c = 'SLS II';
            stipLst.add(stipData);
            
            Stipulation_Data__c stipData1 = new Stipulation_Data__c();
            stipData1.Name = 'APR';
            stipData1.CL_Code__c = 'STIP834';
            stipData1.Insert_New_Stip__c = true;
            stipData1.Review_Classification__c = 'SLS II';
            stipLst.add(stipData1);
            insert stipLst;
            
            Underwriting_File__c underWrFilRec = new Underwriting_File__c();
            underWrFilRec.name = 'Underwriting test';
            underWrFilRec.ACT_Review__c ='Yes';
            underWrFilRec.Opportunity__c = opp.Id;
            underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
            underWrFilRec.Title_Review_Complete_Date__c = system.today();
            underWrFilRec.Install_Contract_Review_Complete__c = true;
            underWrFilRec.Utility_Bill_Review_Complete__c = true;
            underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
            underWrFilRec.Installation_Photos_Review_Complete__c = true;
            underWrFilRec.CO_Requested_Date__c = system.now();
            //underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
            //underWrFilRec.Final_Design_Document_Received__c = system.today();
            underWrFilRec.Final_Design_Review_Complete__c = true;   
            
            underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
           
            insert underWrFilRec;
            
          /*  Stipulation__c ACHStip = new Stipulation__c();
            ACHStip.Stipulation_Data__c = ACHStipData.Id;
            ACHStip.Status__c = 'In Progress';
            ACHStip.Underwriting__c = underWrFilRec.Id;
            insert ACHStip;
            */
            
            Box_Fields__c boxFields = new Box_Fields__c();
            boxFields.Box_Install_Contracts_TS__c = SYSTEM.now();
            boxFields.Latest_Install_Contract_Date_Time__c = system.now()-2;
            boxFields.Latest_Roof_Install_Contract_Date_Time__c = system.now()-2;
            boxFields.Underwriting__c = underWrFilRec.Id;
            
            insert boxFields;
                
                
            Routing_Number__c rNum = new Routing_Number__c();
            rNum.name = 'Test';
            rNum.ACH_Routing_Numbers__c = '012345678';
            insert rNum;
                                   
            set<id>  ddst= new set<id>();
            //Inserting DocuSignStatus Records
            dsfs__DocuSign_Status__c dsfsObj = new dsfs__DocuSign_Status__c();
            dsfsObj.dsfs__Opportunity__c = opp.Id;
            dsfsObj.dsfs__Envelope_Status__c = 'Delivered'; 
            dsfsObj.dsfs__Subject__c = 'Documents for your DocuSign Signature';
            dsfsObj.dsfs__DocuSign_Envelope_ID__c = '368C9881-5638-47F1-82BC-A4D60C6DFDD8';
            insert dsfsObj;
            ddst.add(dsfsObj.id);
            
            dsfs__DocuSign_Status__c dsfsObj1 = new dsfs__DocuSign_Status__c();
            dsfsObj1.dsfs__Opportunity__c = opp.Id;
            dsfsObj1.dsfs__Envelope_Status__c = 'Sent'; 
            dsfsObj1.dsfs__Subject__c = 'Documents for your DocuSign Signature';
            dsfsObj1.dsfs__DocuSign_Envelope_ID__c = '368C9881-5638-47F1-82BC-A4D60C6DFDD8';
            
            insert dsfsObj1;
            ddst.add(dsfsObj1.id);
            
            dsfsObj1.dsfs__Envelope_Status__c = 'Delivered'; 
            Update dsfsObj1;
            
            list<id> drset = new list<id>();
            dsfs__DocuSign_Recipient_Status__c docRecipient = new dsfs__DocuSign_Recipient_Status__c();
            docRecipient.dsfs__DocuSign_Recipient_Email__c = 'test@gmail.com';
            docRecipient.dsfs__Parent_Status_Record__c = dsfsObj.id;
            docRecipient.dsfs__DocuSign_Routing_Order__c = 2;
            docRecipient.dsfs__DocuSign_Recipient_Id__c = '123456789' ;
            
            insert docRecipient;
            drset.add(docRecipient.id);
            
            opp.Combined_Loan_Amount__c = 22550;
            update opp;
            
            Acc.Push_Endpoint__c = 'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
            update Acc;
             dsfsObj1.dsfs__Envelope_Status__c = 'Completed'; 
            update dsfsObj1;
            
            //To cover try catch block
            DocusignStatusTriggerHandler docusignStsObj = new DocusignStatusTriggerHandler(true);
            docusignStsObj.OnAfterInsert(null);
            docusignStsObj.OnAfterUpdate(null,null);
            DocusignStatusTriggerHandler.UpdateAddress(ddst);
            
            String authResponse = BoxTestJsonResponseFactory.AUTH_USER_TOKEN;
            Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(authResponse, 'OK', 200));
            Blob bodyBlob = Blob.valueOf('Test Attachment Body');
            DownloadLoanDocs.uploadFileToBox('12314',bodyBlob,authResponse,'test');
            Test.starttest();
            DocusignStatusTriggerHandler.ResyncQuote(null,null);
            DocusignStatusTriggerHandler.seteOriginalVault(ddst);
            DocusignStatusTriggerHandler.UpdateAddress(null);
            DocusignStatusTriggerHandler.LexisNexisIDCheckResult(drset);
            Set<Id> oppSet = new Set<Id>();
            oppSet.add(opp.Id);
            DocusignStatusTriggerHandler.createOOWStip(oppSet);
            //Test.setMock(HttpCalloutMock.class, new MockHttpResponseClass());
            //DocusignStatusTriggerHandler.MoveContractBoxFolder(oppSet);
            Test.stoptest();
        
        }
         private testMethod static void docusignTriggerHandlerTest1(){            
            
            
            Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();          
            
            //get Custom setting data.
            SLFUtilityTest.createCustomSettings();
            SLF_Boomi_API_Details__c apidetails4 = new SLF_Boomi_API_Details__c();
            apidetails4.Name ='DocusignAPI';
            apidetails4.Username__c ='sdeveloperdocusign@gmail.com';
            apidetails4.Password__c ='Sunlight18!';
            apidetails4.Endpoint_URL__c ='https://demo.docusign.net/restapi/v2/accounts/4143849/envelopes/';
            apidetails4.APIKey__c ='91805a55-fd52-4b55-8d8d-0f268dc32370';
            apidetails4.SF_UserId__c ='DocusignAPI';
            insert apidetails4;   
            
            // Inserting Account records        
            Account Acc = new Account();
            Acc.name = 'Test Account';
            Acc.Push_Endpoint__c = 'https://requestb.in';
            Acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            Acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.BillingStateCode = 'CA';
            Acc.Installer_Email__c = 'test@slf.com';
            Acc.FNI_Domain_Code__c = '';
            Acc.Installer_Legal_Name__c='Bright Solar Planet';
            Acc.Subscribe_to_Push_Updates__c = true;
            Acc.Domain_list__c = 'Test';
            Acc.Solar_Enabled__c = true; 
            Acc.ACH_true__c = 'Y';
            Acc.Requires_ACH_Validation__c = True;
            Acc.Routing_Number__c = '012345678';
            Acc.LexisNexis_IDLookupResult__c = 'Passed';
            Acc.LexisNexis_IDQuestionsResult__c = 'Failed';
            insert Acc;
            
            Draw_Allocation__c draw = new Draw_Allocation__c();
            draw.Account__c = acc.id;
            draw.Draw_Allocation__c = 0.2;
            draw.Level__c = '1';  
            insert draw;
                                
            //Insert SLF Product Record
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            //prod.Product_Loan_Type__c='Home';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            insert prod;
            
            //Inserting opportunity record
            List<Opportunity> oppList = new List<Opportunity>();
            
            Opportunity opp1 = new Opportunity();
            opp1.name = 'OpName';
            opp1.SLF_Product__c = prod.id;
            opp1.Hash_Id__c = 'LSYVEfyt16sHA3tzT60cqkCSqlMmTariHrjdtLuOcf8=';
            opp1.Customer_has_authorized_credit_hard_pull__c = true;
            
            opp1.CloseDate = system.today();
            opp1.StageName = 'Closed Won';
            opp1.AccountId = Acc.id;
            opp1.Installer_Account__c = Acc.id;
            opp1.Co_Applicant__c = Acc.id;
            opp1.Install_State_Code__c = 'CA'; 
            opp1.Combined_Loan_Amount__c = 15000;
            opp1.Partner_Foreign_Key__c = 'Home||133';
            opp1.Change_Order_Identified__c = true;
            opp1.Change_Order_Status__c = 'Pending';
            opp1.Co_applicant_present_on_loan_agreement__c = NULL;   
            opp1.Co_Applicant_Approved_for_Credit__c = NULL;
            opp1.LT_Loan_Name_s_Signature_is_Present__c = false;
            opp1.LT_Loan_Name_s_Matches_Credit_Bureau__c = false;
            opp1.EDW_Originated__c = false;
            opp1.Project_Category__c = 'Home';
            oppList.add(opp1);
            
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.SLF_Product__c = prod.id;
            opp.Hash_Id__c = 'LSYVEfyt16sHA3tzT60cekCSqlMmTariHrjdtLuOcf8=';
            opp.Customer_has_authorized_credit_hard_pull__c = true;
            
            opp.CloseDate = system.today();
            opp.StageName = 'Closed Won';
            opp.AccountId = Acc.id;
            opp.Installer_Account__c = Acc.id;
            opp.Co_Applicant__c = Acc.id;
            opp.Install_State_Code__c = 'CA'; 
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'Home||123';
            opp.Change_Order_Identified__c = true;
            opp.Change_Order_Status__c = 'Pending';
            opp.Co_applicant_present_on_loan_agreement__c = NULL;   
            opp.Co_Applicant_Approved_for_Credit__c = NULL;
            opp.LT_Loan_Name_s_Signature_is_Present__c = false;
            opp.LT_Loan_Name_s_Matches_Credit_Bureau__c = false;
            opp.EDW_Originated__c = false;
            opp.Project_Category__c = 'Home';
            opp.ACH_APR_Process_Required__c = true;
            opp.Change_Order_Number__c = 1;
            opp.Opportunity__c = opp1.Id;
            oppList.add(opp);
            
            insert oppList; 
            
            //Insert Quote Record
            Quote syncQuoteRec = new Quote();
            syncQuoteRec.Name ='Test quote';
            syncQuoteRec.Opportunityid = opp.Id;
            insert syncQuoteRec;
            
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.Id;
            credit.IsSyncing__c = true;
            credit.Status__c = 'Auto Approved';
            credit.LT_StipRsn1__c = 'STIP847';
            credit.LT_StipRsn2__c = 'STIP847';
            credit.LT_StipRsn3__c = 'STIP847';
            credit.LT_StipRsn4__c = 'STIP847';
            insert credit;
            
            List<Stipulation_Data__c> stipLst = new List<Stipulation_Data__c>();
            Stipulation_Data__c ACHStipData = new Stipulation_Data__c();
            ACHStipData.Name = 'ACH';
            ACHStipData.CL_Code__c = '';
            ACHStipData.Insert_New_Stip__c = true;
            ACHStipData.Display_Folder_Text__c = 'Voided Check';
            ACHStipData.Review_Classification__c = 'SLS I';
            stipLst.add(ACHStipData);
            
            Stipulation_Data__c stipData = new Stipulation_Data__c();
            stipData.Name = 'OOW';
            stipData.CL_Code__c = 'STIP834';
            stipData.Insert_New_Stip__c = true;
            stipData.Review_Classification__c = 'SLS II';
            stipLst.add(stipData);
            
            Stipulation_Data__c stipData1 = new Stipulation_Data__c();
            stipData1.Name = 'APR';
            stipData1.CL_Code__c = 'STIP834';
            stipData1.Insert_New_Stip__c = true;
            stipData1.Review_Classification__c = 'SLS II';
            stipLst.add(stipData1);
            insert stipLst;
            
            Underwriting_File__c underWrFilRec = new Underwriting_File__c();
            underWrFilRec.name = 'Underwriting test';
            underWrFilRec.ACT_Review__c ='Yes';
            underWrFilRec.Opportunity__c = opp.Id;
            underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
            underWrFilRec.Title_Review_Complete_Date__c = system.today();
            underWrFilRec.Install_Contract_Review_Complete__c = true;
            underWrFilRec.Utility_Bill_Review_Complete__c = true;
            underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
            underWrFilRec.Installation_Photos_Review_Complete__c = true;
            //underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
            //underWrFilRec.Final_Design_Document_Received__c = system.today();
            underWrFilRec.Final_Design_Review_Complete__c = true;   
            
            underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
           
            insert underWrFilRec;
            
            Box_Fields__c boxFields = new Box_Fields__c();
            boxFields.Box_Install_Contracts_TS__c = SYSTEM.now();
            boxFields.Underwriting__c = underWrFilRec.Id;
            
            insert boxFields;
                
                
            Routing_Number__c rNum = new Routing_Number__c();
            rNum.name = 'Test';
            rNum.ACH_Routing_Numbers__c = '012345678';
            insert rNum;
                                   
            set<id>  ddst= new set<id>();
            //Inserting DocuSignStatus Records
            dsfs__DocuSign_Status__c dsfsObj = new dsfs__DocuSign_Status__c();
            //dsfsObj.dsfs__Opportunity__c = opp.Id;
            dsfsObj.dsfs__Envelope_Status__c = 'Delivered'; 
            dsfsObj.dsfs__Subject__c = 'Documents for your DocuSign Signature';
            dsfsObj.dsfs__DocuSign_Envelope_ID__c = '368C9881-5638-47F1-82BC-A4D60C6DFDD8';
            insert dsfsObj;
            ddst.add(dsfsObj.id);
            
            dsfs__DocuSign_Status__c dsfsObj1 = new dsfs__DocuSign_Status__c();
            //dsfsObj1.dsfs__Opportunity__c = opp.Id;
            dsfsObj1.dsfs__Envelope_Status__c = 'Sent'; 
            dsfsObj1.dsfs__Subject__c = 'Documents for your DocuSign Signature';
            dsfsObj1.dsfs__DocuSign_Envelope_ID__c = '368C9881-5638-47F1-82BC-A4D60C6DFDD8';
            
            insert dsfsObj1;
            ddst.add(dsfsObj1.id);
            
            dsfsObj1.dsfs__Envelope_Status__c = 'Delivered'; 
            Update dsfsObj1;
            
            list<id> drset = new list<id>();
            dsfs__DocuSign_Recipient_Status__c docRecipient = new dsfs__DocuSign_Recipient_Status__c();
            docRecipient.dsfs__DocuSign_Recipient_Email__c = 'test@gmail.com';
            docRecipient.dsfs__Parent_Status_Record__c = dsfsObj.id;
            docRecipient.dsfs__DocuSign_Routing_Order__c = 2;
            docRecipient.dsfs__DocuSign_Recipient_Id__c = '123456789' ;
            
            insert docRecipient;
            drset.add(docRecipient.id);
            
            opp.Combined_Loan_Amount__c = 22550;
            opp.SyncedQuoteid = syncQuoteRec.id;
            opp.Signed_Document_Quote_Reference__c = syncQuoteRec.id;
            update opp;
            
            dsfsObj1.dsfs__Opportunity__c = opp.Id;
            update dsfsObj1;
            
            Acc.Push_Endpoint__c = 'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
            update Acc;
            dsfsObj1.dsfs__Envelope_Status__c = 'Completed'; 
            update dsfsObj1;
            
            //To cover try catch block
            DocusignStatusTriggerHandler docusignStsObj = new DocusignStatusTriggerHandler(true);
            docusignStsObj.OnAfterInsert(null);
            docusignStsObj.OnAfterUpdate(null,null);
            DocusignStatusTriggerHandler.UpdateAddress(ddst);
            
            String authResponse = BoxTestJsonResponseFactory.AUTH_USER_TOKEN;
            Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(authResponse, 'OK', 200));
            Blob bodyBlob = Blob.valueOf('Test Attachment Body');
            DownloadLoanDocs.uploadFileToBox('12314',bodyBlob,authResponse,'test');
            Test.starttest();
            DocusignStatusTriggerHandler.ResyncQuote(null,null);
            DocusignStatusTriggerHandler.seteOriginalVault(ddst);
            DocusignStatusTriggerHandler.UpdateAddress(null);
            DocusignStatusTriggerHandler.LexisNexisIDCheckResult(drset);
            Set<Id> oppSet = new Set<Id>();
            oppSet.add(opp.Id);
            DocusignStatusTriggerHandler.createOOWStip(oppSet);
            //Test.setMock(HttpCalloutMock.class, new MockHttpResponseClass());
            //DocusignStatusTriggerHandler.MoveContractBoxFolder(oppSet);
            Test.stoptest();
        
        }
		
    }