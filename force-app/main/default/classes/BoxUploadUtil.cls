public class BoxUploadUtil 
{
    public class BoxUtilException extends Exception {}
    private static string UPLOAD_VERSION_URL = 'files/{0}/content';
    private static String SHARED_LINK_URL = 'folders/{0}/';
    @testVisible private static BoxApiConnection sharedAPI;
    private static boolean resetConnection = true;
    
    public class SiteDownloadWrapper
    {
        public Site_Download__c site_download {get; set;}
        public Attachment attachment {get; set;}
    }
        
     /**
      * Returns folderID for a child folder, and creates it if it does not exist
      **/
    
     public static String getChildFolderID(String parentBoxFolderId, String folderName, String accessToken)
     {
        BoxApiConnection api = new BoxApiConnection (accessToken);

         String fid = null;
         try {
             BoxFolder bf = new BoxFolder(api,parentBoxFolderId); 
             Boxfolder.Info f =  bf.createFolder(folderName);
             fid = f.Id;
         } catch (Exception e) {
             String m = e.getMessage();
             if (m.indexOf('item_name_in_use') > 0) {
                 Integer n = m.indexOf(',"id":');
                 Integer n2 = m.IndexOf('"',n+7);
                 fid = m.substring(n+7,n2); 
                 //throw new BoxUtilException('cause => ' + e.getCause() + ' msg => **'  + e.getMessage() + '**');
                 //throw new BoxUtilException('got fid => ' + fid);
                 return fid;
             } else {
                throw new BoxUtilException('cause => ' + e.getCause() + ' msg => **'  + e.getMessage() + '**');
             }
         }
         
         return fid;
     }

    public static String getChildFolderID(ID oppId, String parentBoxFolderId, String folderName, String accessToken)
    {
        // *** SS MAIN - set this to true or false to use token or toolkit
        Boolean USE_TOKEN = true;
        if (USE_TOKEN)
        {
            return getChildFolderIDToken(oppId, parentBoxFolderId, folderName, accessToken);
        } else 
        {
            return getChildFolderIDToolkit(oppId, parentBoxFolderId, folderName, accessToken);   
        }
    }

    //*** SS MAIN - This is the main method to create folders for UploadFileToBox
    //  alternative version by Alan Davies, with String accessToken as parameter 4

    public static String getChildFolderIDToken(ID oppId, String parentBoxFolderId, String folderName, String accessToken)
     {
        BoxApiConnection api = new BoxApiConnection (accessToken);

         String fid = null;
         try {
             if (Test.isRunningTest()) {
                 fid = '123456';
             } else {
                 BoxFolder bf = new BoxFolder(api,parentBoxFolderId); 
                 Boxfolder.Info f =  bf.createFolder(folderName);
                 fid = f.Id;
             }
         } catch (Exception e) {
             String m = e.getMessage();
             if (m.indexOf('item_name_in_use') > 0) {
                 Integer n = m.indexOf(',"id":');
                 Integer n2 = m.IndexOf('"',n+7);
                 fid = m.substring(n+7,n2); 
                 //throw new BoxUtilException('cause => ' + e.getCause() + ' msg => **'  + e.getMessage() + '**');
                 //throw new BoxUtilException('got fid => ' + fid);
                 return fid;
             } else {
                throw new BoxUtilException('cause => ' + e.getCause() + ' msg => **'  + e.getMessage() + '**');
             }
         }
         
         return fid;
     }

    //*** SS MAIN - This is the main method to create folders for UploadFileToBox
    //  alternative version by Alan Davies, with String accessToken as parameter 4

    public static String getChildFolderIDToolkit(Id oppId, String parentBoxFolderId, String folderName, String accessToken)
     {
         // *** if parentBoxFolderId is the root folder id from settings we invoke boxToolkit.createFolderForRecordId
         // *** else we invoke boxToolkit.createFolder
         // *** SS MAIN - assumption here is that the token will not expire

         String oppRootFolderID = SFSettings__c.getOrgDefaults().Root_Folder_ID__c;
         String boxFolderID = null;
         box.Toolkit boxToolkit = new box.Toolkit();
         if (parentBoxFolderId == oppRootFolderID)
         {
            if (!Test.isRunningTest()) {         
                boxFolderID = boxToolkit.createFolderForRecordId(oppId, null, true);
                System.Debug('****** called boxToolkit with oppId ' + oppId + ' returned ' + boxFolderId + ' most recent error =>' + boxToolkit.mostRecentError);
                if (boxToolkit.mostRecentError.trim().length() > 0 && boxToolkit.mostRecentError.indexOf('already exists') == -1)
                        throw new CustException('Error creating folder for opp : [' + boxToolkit.mostRecentError + ']');
            }
         } else {
             if (!Test.isRunningTest()) { 
                boxFolderID = boxToolkit.createFolder(folderName, parentBoxFolderId,null);
                if (boxToolkit.mostRecentError.trim().length() > 0 && boxToolkit.mostRecentError.indexOf('already exists') == -1)
                            throw new CustException('Error creating folder ' + folderName + ' for opp ' + OppId + ' : ' + boxToolkit.mostRecentError);
             }
         }
        
         if (parentBoxFolderId != oppRootFolderID) 
             boxToolkit.commitChanges();
         return boxFolderID;
     }

    public static String getOppFolderID(Id oppId)
     {

         String oppRootFolderID = SFSettings__c.getOrgDefaults().Root_Folder_ID__c;
         String boxFolderID = null;
         box.Toolkit boxToolkit = new box.Toolkit();
         if (!Test.isRunningTest()) 
         {         
                boxFolderID = boxToolkit.createFolderForRecordId(oppId, null, true);
                if (boxFolderID == null)
                    throw new CustException('Error creating folder for opp : [' + boxToolkit.mostRecentError + '] ' + oppId + ' = ' + oppId);
                System.Debug('****** called boxToolkit with oppId ' + oppId + ' returned ' + boxFolderId + ' most recent error =>' + boxToolkit.mostRecentError);
                if (boxToolkit.mostRecentError.trim().length() > 0 && boxToolkit.mostRecentError.indexOf('already exists') == -1)
                        throw new CustException('Error creating folder for opp : [' + boxToolkit.mostRecentError + '] indexof => ' + boxToolkit.mostRecentError.indexOf('already exists'));
         }
         
         boxToolkit.commitChanges();
         return boxFolderID;
         
     }
    

    public static String getSubFolderID(String parentBoxFolderId, String folderName)
     {


         String oppRootFolderID = SFSettings__c.getOrgDefaults().Root_Folder_ID__c;
         String boxFolderID = null;
         box.Toolkit boxToolkit = new box.Toolkit();
         if (!Test.isRunningTest()) { 
             boxFolderID = boxToolkit.createFolder(folderName, parentBoxFolderId,null);
             if (boxToolkit.mostRecentError.trim().length() > 0 && boxToolkit.mostRecentError.indexOf('already exists') == -1)
                 throw new CustException('Error creating folder ' + folderName + ' : ' + boxToolkit.mostRecentError);
         }
        
         boxToolkit.commitChanges();
         return boxFolderID;
     }

    
      public static String getFolderSharedLinkPortal(String boxFolderID, String token) 
     {
       
        //String access_token = BoxCCredentials__c.getValues('CredentialsBox').Access_Token__c;
        BoxApiConnection  api = new BoxApiConnection(token);

         
        BoxGenericJsonObject sharedLinkObject;
        
        try { 
        String url = api.getBaseUrl() + String.format(SHARED_LINK_URL, new String[] {boxFolderID});
        BoxApiRequest request = new BoxApiRequest(api, url, BoxApiRequest.METHOD_PUT);
        request.setBody('{"shared_link": {"access": "open"}}');
        request.setTimeout(api.timeout);
        request.addJsonContentTypeHeader();

        HttpResponse response = request.send();
        System.Debug('***** got Shared Link Response ' + response);
        if (response.getStatusCode() != 200) {
                throw new BoxUtilException('Unable to get Shared Link for folderID ' + boxFolderID + ' response => ' + response);
        }
        String responseBody = BoxApiRequest.getBoxResourceResponseBody(response, 'BoxFile.getFileInfo');
        BoxGenericJsonObject responseObject = new BoxGenericJsonObject(responseBody);
        sharedLinkObject =  new BoxGenericJsonObject(responseObject.getValue('shared_link'));
            
       } catch (Exception e) {
             throw new BoxUtilException(e.getMessage() + ' ' + e.getStackTraceString());
       } finally {
//              setBoxTokens();
       }
        return sharedLinkObject.getValue('url');
    }
    

    
     public static String getFolderSharedLink(String boxFolderID) 
     {
       
        BoxPlatformApiConnection api = BoxAuthentication.getConnection();

        BoxGenericJsonObject sharedLinkObject;
        
        try { 
        String url = api.getBaseUrl() + String.format(SHARED_LINK_URL, new String[] {boxFolderID});
        BoxApiRequest request = new BoxApiRequest(api, url, BoxApiRequest.METHOD_PUT);
        request.setBody('{"shared_link": {"access": "open"}}');
        request.setTimeout(api.timeout);
        request.addJsonContentTypeHeader();

        HttpResponse response = request.send();
        System.Debug('***** got Shared Link Response ' + response);
        if (response.getStatusCode() != 200) {
                throw new BoxUtilException('Unable to get Shared Link for folderID ' + boxFolderID + ' response => ' + response);
        }
        String responseBody = BoxApiRequest.getBoxResourceResponseBody(response, 'BoxFile.getFileInfo');
        BoxGenericJsonObject responseObject = new BoxGenericJsonObject(responseBody);
        sharedLinkObject =  new BoxGenericJsonObject(responseObject.getValue('shared_link'));
            
       } catch (Exception e) {
             throw new BoxUtilException(e.getMessage() + ' ' + e.getStackTraceString());
       } finally {
//              setBoxTokens();
       }
        return sharedLinkObject.getValue('url');
    }

     public static String getBoxFolderId(Id sfRecordId)
     {
        String boxFolderId;
        List<box__FRUP__c> fl =  new List<box__FRUP__C>([SELECT box__Folder_ID__c, box__Record_Id__c FROM box__FRUP__c where box__Record_ID__c = :sfRecordId]);
        
        //if (fl.size() == 0) 
          //  throw new BoxUtilException('Unable to find box folder for sf record id ' + sfRecordId);
        
         if (fl.size() == 0)
             return null;
         else
            return fl[0].box__Folder_ID__c;
     }
    
     public static List<String> getCategoryFolders()
     {
        Set<String> folderSet = new Set<String>();
        Map<string,SF_Category_Map__c> categoryFolderMappings = SF_Category_Map__c.getAll();

        for (SF_Category_Map__c s : categoryFolderMappings.values() ) 
        {
            folderSet.add(s.Folder_Name__c);
        }
        
        // *** return the set as a list, which will eliminate duplicates
        
        List<String> folderList = new List<String>();
        folderList.addAll(folderSet);
        return folderList;
     }
    
    
     public static List<Underwriting_File__c> queryCategoryMappingFieldsFromUnderwriting(String oppId)
     {
        Map<string,SF_Category_Map__c> categoryFolderMappings = SF_Category_Map__c.getAll();
        
        // ** create the list of fields that need to be queried
        String fieldList = '';
        Map<String, Boolean> fieldProcessedMap = new Map<String, Boolean>();
        for (SF_Category_Map__c s : categoryFolderMappings.values() ) 
        {
            if (!String.isBlank(s.Folder_Id_Field_on_Underwriting__c) && !fieldProcessedMap.containsKey(s.Folder_Id_Field_on_Underwriting__c))
            {
                fieldList = fieldList + s.Folder_Id_Field_on_Underwriting__c + ',';
                fieldProcessedMap.put(s.Folder_Id_Field_on_Underwriting__c,true);
            }
            if (!String.isBlank(s.TS_Field_On_Underwriting__c) && !fieldProcessedMap.containsKey(s.TS_Field_On_Underwriting__c))
            {
                fieldList = fieldList + s.TS_Field_On_Underwriting__c  + ',';  
                fieldProcessedMap.put(s.TS_Field_On_Underwriting__c,true);
            }
            
        }
        fieldList = fieldList.SubString(0,fieldList.length()-1);
        // Added as part of 1930 by Adithya
        if(string.isNotBlank(fieldList)){
            fieldList='(SELECT id,'+fieldList+' FROM Box_Fields__r)';
        }
        List<Underwriting_File__c> ul = Database.query('Select ' + fieldList + ' from Underwriting_File__c where Opportunity__c = :oppId LIMIT 1');
        return ul;
      }
    
      public static String generateSHA1Digest(Blob b)
      {
          Blob b1 =  Crypto.generateDigest('SHA1', b);

          return EncodingUtil.convertToHex(b1);
      }
    
    
     // *** SS ADD TO Upload a new version of file
    public static BoxFile.Info uploadNewVersionOfFile(String fileId, String fileName, Blob fileBody) {
        if (String.isEmpty(fileName)) {
            throw new BoxResource.BoxResourceException('fileName must not be null or empty when calling BoxFolder.uploadFile.');
        }
        if (fileBody == null || fileBody.size() == 0) {
            throw new BoxResource.BoxResourceException('fileBody must not be null or empty when calling BoxFolder.uploadFile.');
        }
        BoxFile.Info info = null;
        
        BoxPlatformApiConnection api = BoxAuthentication.getConnection();

        try {
            String url = api.getBaseUploadUrl() + String.format(UPLOAD_VERSION_URL, new String[] {fileId});
            BoxApiRequest request = new BoxApiRequest(api, url, BoxApiRequest.METHOD_POST);
            request.setTimeout(api.getTimeout());
            request.setMultipartFormBody(fileBody, 'tempname');
            
            HttpResponse response = request.send();
            String responseBody = BoxApiRequest.getBoxResourceResponseBody(response, 'BoxFolder.uploadFile');
            BoxGenericJsonObject responseObject = new BoxGenericJsonObject(responseBody);
            if (responseObject.getValue('total_count') != null) {
                list<String> fileEntries = BoxJsonObject.parseJsonObjectArray(responseObject.getValue('entries'));
                if (fileEntries.size() > 0) {
                    return new BoxFile.Info(fileEntries[0]);
                }
            }
        } catch (Exception e) {
             throw new BoxUtilException(e.getMessage());
         } finally {
//              setBoxTokens();
         }
        return info;
    }
      
}