@isTest(seeAllData=false)
public class TestSightenUtil {
    private testMethod static void TestSightenUtil(){
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);
        TriggerFlags__c trgflag3 = new TriggerFlags__c();
        trgflag3.Name ='SLF_Credit__c';
        trgflag3.isActive__c =true;
        trgrList.add(trgflag3);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='Contact';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        TriggerFlags__c trgflag5 = new TriggerFlags__c();
        trgflag5.Name ='User';
        trgflag5.isActive__c =true;
        trgrList.add(trgflag5);
        TriggerFlags__c trgflag6 = new TriggerFlags__c();
        trgflag6.Name ='Quote';
        trgflag6.isActive__c =true;
        trgrList.add(trgflag6);
        TriggerFlags__c trgflag7 = new TriggerFlags__c();
        trgflag7.Name ='Underwriting_File__c';
        trgflag7.isActive__c =true;
        trgrList.add(trgflag7);
        
        TriggerFlags__c trgflag8 = new TriggerFlags__c();
        trgflag8.Name ='Funding_Data__c';
        trgflag8.isActive__c =true;
        trgrList.add(trgflag8);
        
        insert trgrList;
                            
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';        
        accObj.Type = 'Facility'; 
        accObj.Prequal_Enabled__c = true;   
        accObj.Solar_Enabled__c = true;     
        insert accObj;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = accObj.Id);
        insert con; 
        
        
         
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        accObj.Credit_Run__c = false;
            update accObj;
            
        Product__c prdObj = new Product__c(Name='Test',
                                           Product_Loan_Type__c='Solar',
                                           Installer_Account__c = accObj.Id,
                                           State_Code__c='CA',
                                           FNI_Min_Response_Code__c=2,
                                           Product_Display_Name__c = 'Display Name',
                                           Qualification_Message__c = 'Message',
                                           Term_mo__c = 2,
                                           APR__c = 20,
                                           ACH__c = true,
                                           Internal_Use_Only__c = FALSE,
                                           Is_Active__c = TRUE,
                                           Long_Term_Facility__c = 'CVX',
                                           Product_Tier__c = '0');
        insert prdObj;
        
        Opportunity oppObj = new Opportunity(Name = 'OppName',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Qualified',
                                             CloseDate=system.today(),
                                             SLF_Product__c = prdObj.ID,
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             //Partner_Foreign_Key__c = 'CVX1254',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
        insert oppObj;  
        
        SLF_Credit__c slfCreditObj = new SLF_Credit__c();
        slfCreditObj.Opportunity__c = oppObj.id;
        slfCreditObj.Primary_Applicant__c = accObj.id;
        slfCreditObj.Co_Applicant__c = accObj.id;
        slfCreditObj.SLF_Product__c = prdObj.Id ;
        slfCreditObj.Total_Loan_Amount__c = 100;
        slfCreditObj.Status__c = 'New';
        slfCreditObj.LT_FNI_Reference_Number__c = '123';
        slfCreditObj.Application_ID__c = '123';
        insert slfCreditObj;

        
        Quote qObj = new Quote(name = 'quote',
        OpportunityId = oppObj.Id);
        insert qObj;
                
        List<Box_Upload__c> boxUploadList = new list<Box_Upload__c>();
        Box_Upload__c bp = new Box_Upload__c();
        bp.Source_URL__c = 'www.google.com';
        bp.Uploaded__c = false;
        bp.sighten_uuid__c = 'Test123';
        boxUploadList.add(bp);
        insert boxUploadList;
        
        List<Underwriting_File__c> undList = new List<Underwriting_File__c>();
        Underwriting_File__c un = new Underwriting_File__c();
        un.Opportunity__c = oppobj.id;
        undList.add(un);
        insert undList;
        
        List<Box_Fields__c> boxFieldList = new List<Box_Fields__c>();
        Box_Fields__c bf = new Box_Fields__c();
        bf.Underwriting__c = undList[0].id;
        boxFieldList.add(bf);
        insert boxFieldList;
        
        Map<Id, Attachment> attachmentMap = new Map<Id, Attachment>();
        Attachment acc =  new Attachment();
        acc.name = 'Test Attachment';
        acc.IsPrivate = false;
        acc.ParentID = oppobj.id;
        acc.Body = Blob.valueof('jkdhfi');
        insert acc;
        attachmentMap.put(acc.id,acc);
        
        Site_Download__c sd = new Site_Download__c();
        sd.Opportunity__c = oppobj.id;
        sd.Site_UUID__c = 'Test';
        //sd.Response_Length_Exceeded__c = true;
        insert sd;
        
        //box__Folder_ID__c fldrObj = new box__Folder_ID__c(Name='folder');
        //insert fldrObj;
        
        //box__FRUP__c boxObj = new box__FRUP__c(box__CollaborationID__c = 'Test');
        //box__FRUP__c boxObj = new box__FRUP__c(box__Record_ID__c=oppObj.Id,box__Folder_ID__c='test');
        //insert boxObj;
      
        Map<String, String> createincenMap = new Map<String, String>();
        
        oppObj = [Select id,Name,AccountId,Co_Applicant__c,StageName,CloseDate,SLF_Product__c,Installer_Account__c,
                  Install_State_Code__c,Synced_Quote_Id__c,Desync_Bypass__c,Install_Street__c,
                  Install_Postal_Code__c,Install_City__c,Hash_Id__c from Opportunity where id = :oppObj.Id];   
                  
         String jsonStr = '{'+
    '  \"data\": ['+
    '    {'+
    '    \"date_created\": \"2016-05-20T00:34:51.568704+00:00\",'+
    '    \"productName\": \"12 mo def interest promo (21.99%), then 48 mos 21.99%\",'+
    '    \"isSyncing\": true,'+    
    '    \"finalMonthlyPayment\": 123.44,'+
    '    \"term\": 60.0,'+
    '     \"apr\": 21.99,'+
    '    \"isACH\": true'+
    ' }]'+
    '}';      
        
        //sightenUtil.createQuote('test','test',oppObj, new List<Quote>{qObj}, new List<Product__c>{prdObj},true,new List<Opportunity>{oppObj},new List<Quote>{qObj} );
        sightenUtil.createQuote(jsonStr,'test',oppObj, new List<Quote>{qObj}, new List<Product__c>{prdObj},true,new List<Opportunity>{oppObj},new List<Quote>{qObj} );
        sightenUtil.updateMilestoneComments(qObj,'test',null);
        sightenUtil.updateMilestoneComments(qObj,'test','{"projects": [{"id": "+oppObj.Id+"}]}');
        sightenUtil.createFRUPEntries('test',oppObj.ID);
        sightenUtil.setMilestoneOnOpp(oppObj.ID,qObj.ID);
        sightenUtil.setMilestoneIdOnQuote(qObj);
        sightenUtil.createBoxFoldersAndUploads(new Box.ToolKit(),sd,boxUploadList,undList,boxFieldList,attachmentMap);
        
        //Opportunity opp = [Select Sighten_Milestone_UUID__c from Opportunity where id=:oppObj.Id];
        sightenUtil.updateSightenComments(oppObj.ID, 'test', 'test');
        
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp = new Opportunity(Name = 'OppName1',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Qualified',
                                             CloseDate=system.today(),
                                             SLF_Product__c = prdObj.ID,
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             //Partner_Foreign_Key__c = 'CVX1254',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
        oppList.add(opp);
        insert oppList;                                             
        System.debug('********Opportunity Record ID******'+oppList);        

        SFSettings__c sfSettings = new SFSettings__c();
        sfsettings.Site_Batch_Execution_TS__c =  system.now();
        sfSettings.Sandbox_URL_Prefix__c = 'www.google.com';
        sfsettings.Production_URL_Prefix__c = 'www.gogole.com';
        insert sfSettings;

       String reqStr = '{'+
    '"pages": 2,'+
    '"status_code": 200,'+
    '"count": 24,'+
    '"has_next_page": true,'+
    '"messages": {'+
    '"info": [],'+ 
    ' "critical": [],'+
    '"error": [],'+
    '"warning": []'+
    '},'+
    '"data": ['+
    '{'+
    ' "task_name": "Loan Agreement - TSLA",'+
            ' "site_last_updated": "'+System.Now()+'",'+
    '"task_id": "99b02ba3-db95-45be-9a48-2c73b2dd2815",'+
    '  "document_id": ['+
    '"e51e3f02-d495-4ff7-a5a5-cdcc8059993b",'+
    '"d60a1c8d-fe0d-4e3e-b519-0a255dcff9c4"'+
    ']'+
    '},'+
    '{'+
    '"task_name": "Installation Agreement",'+
    '"task_id": "8096b656-5fa9-42fa-be0c-f77369888636",'+
    '"document_id": ['+        
    ' ]'+
    ' }'+
    ']'+
    '}';
        System.debug('********Request String******'+reqStr);
        sightenUtil.getSiteRecordsWithSightenTS(oppList,reqStr,1,1);
        
        Quote q = [Select Sighten_Milestone_UUID__c from Quote where id=:qObj.ID];
        sightenUtil.updateMilestoneComments(qObj, q.Sighten_Milestone_UUID__c, 'test');
        
        sightenUtil.createFRUPEntry(null, null);
        sightenUtil.createFRUPEntries(null, null);
    }
}