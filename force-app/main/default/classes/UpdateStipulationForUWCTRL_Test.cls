/* Author : Veereandranath Jalla
 * Date  : Jan 17th 2019
 * Description: 
 * 
 */ 
@isTest
public class UpdateStipulationForUWCTRL_Test {
    public static testMethod void myUnitTest(){
         //get the person account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        insert trgAccflag;
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.Push_Endpoint__c ='www.test.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.M0_Split__c = 60;
        acc.M1_Split__c = 20;
        acc.M2_Split__c = 20;
        insert acc;
        SLFUtilityTest.createCustomSettings();

        set<Id> opppIdSet = new set<Id>();
        list<Opportunity> oppLst = new List<Opportunity>();
        for(Integer i=0; i<6; i++) 
        {
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            oppLst.add(opp);
            
            
        }
        insert oppLst;
        
        for(Opportunity oppIterate : oppLst) 
        {
            opppIdSet.add(oppIterate.Id);
        }

        //Insert Quote Record
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = oppLst[0].id;
        //quoteRec.SLF_Product__c = prod.Id ;
        //quoteRec.Accountid =acc.id;
        insert quoteRec;
        
        oppLst[0].SyncedQuoteId =  quoteRec.Id;
        update oppLst[0];
        
         //Insert System Design Record
        System_Design__c sysDesignRec = new System_Design__c();

        sysDesignRec.System_Cost__c = 11;
        sysDesignRec.Opportunity__c = oppLst[0].Id;
        sysDesignRec.Est_Annual_Production_kWh__c = 10;
        sysDesignRec.Inverter_Count__c = 11;
        sysDesignRec.Inverter_Model__c = '12';
        sysDesignRec.Module_Count__c = 10;
        sysDesignRec.Module_Model__c = '11';
        insert sysDesignRec;
        
        quoteRec.System_Design__c = sysDesignRec.Id;
        update quoteRec;
        
        List<Underwriting_File__c> underWrFilInsertLst = new List<Underwriting_File__c>();
        test.startTest();
        
        Underwriting_File__c underWrFilRec = new Underwriting_File__c();
        underWrFilRec.name = 'Underwriting File1';
        underWrFilRec.ACT_Review__c ='Yes';
        underWrFilRec.Opportunity__c = oppLst[0].Id;
        underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
        underWrFilRec.Title_Review_Complete_Date__c = system.today();
        underWrFilRec.Install_Contract_Review_Complete__c = true;
        underWrFilRec.Utility_Bill_Review_Complete__c = true;
        underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
        underWrFilRec.Installation_Photos_Review_Complete__c = true;
        //underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
        //underWrFilRec.Final_Design_Document_Received__c = system.today();
        underWrFilRec.Final_Design_Review_Complete__c = true;   
        underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
        underWrFilRec.Project_Status__c = 'M0';
        underWrFilRec.M0_Approval_Requested_Date__c = system.Today();
        underWrFilRec.Opportunity__c = oppLst[4].Id; 
        
        underWrFilRec.M0_Approval_Date__c = null;
        insert underWrFilRec;
        try{
            List<UpdateM1StipDetailsCTRL.input> M1lstUInput1 = new List<UpdateM1StipDetailsCTRL.Input>();
            UpdateM1StipDetailsCTRL.input uInput2 = new UpdateM1StipDetailsCTRL.Input(underWrFilRec.Id);
            M1lstUInput1.add(uInput2);
            UpdateM1StipDetailsCTRL.UpdateM1StipDetails(M1lstUInput1); 
        }catch(exception e){
            
        }
        system.assertEquals(underWrFilRec.M0_Approval_Date__c ,null);
        Stipulation_Data__c sData  = new Stipulation_Data__c();
        sData.Name = 'SD Test';
        sData.Description__c = 'Test';
        sData.Installer_Only_Email__c = true;
        sData.Review_Classification__c = 'SLS I';
        insert sData;        
       
        Stipulation__c stipTest  = new Stipulation__c();
        stipTest.Status__c = 'Documents Received';
        stipTest.Review_Start_Date_Time__c = null;
        stipTest.Stipulation_Data__c  = sData.id;
        stipTest.Underwriting__c =underWrFilRec.id;
        //stipTest.Case__c = caseTest.id;
        stipTest.ETC_Notes__c = 'test';
        insert stipTest;
        stipTest.Status__c = 'Documents Received';
        update stipTest;
        system.assertEquals(stipTest.Status__c,'Documents Received');
        system.assertEquals(stipTest.Review_Start_Date_Time__c,null);
        
        List<UpdateStipulationForUnderWrittingCTRL.input> lstUInput = new List<UpdateStipulationForUnderWrittingCTRL.Input>();
        UpdateStipulationForUnderWrittingCTRL.input uInput = new UpdateStipulationForUnderWrittingCTRL.Input(underWrFilRec.Id);
        lstUInput.add(uInput);
        UpdateStipulationForUnderWrittingCTRL.UpdateTaskStatus(lstUInput); 
        //uInput.strUnderWrittingId = underWrFilRec.Id;
        
        underWrFilRec.M0_Approval_Date__c = system.Today();
        underWrFilRec.M1_Approval_Requested_Date__c = system.Today();
        
        update underWrFilRec;
        TriggerFlags__c stipTrgFlag = new TriggerFlags__c(Name='Stipulation__c',isActive__c=true);
        insert stipTrgFlag;
         Stipulation__c stipTest1  = new Stipulation__c();
        stipTest1.Status__c = 'Documents Received';
        stipTest1.Review_Start_Date_Time__c = null;
        stipTest1.Stipulation_Data__c  = sData.id;
        stipTest1.Underwriting__c =underWrFilRec.id;
        //stipTest.Case__c = caseTest.id;
        stipTest1.ETC_Notes__c = 'test';
        insert stipTest1;
        stipTest1.Status__c = 'Documents Received';
        update stipTest1;
         TriggerFlags__c taskTrgFlag = new TriggerFlags__c(Name='Task',isActive__c=true);
        insert taskTrgFlag;
       
        Task objTask = new Task();
        objTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Underwriting Task').getRecordTypeId();
        objTask.WhatId = underWrFilRec.id;//strUnWrId;
        objTask.OwnerId = UserInfo.getUserId();
        objTask.Status = 'Open';
        objTask.Type = 'Post-NTP Issue';
        objTask.Subject = 'Post-NTP Issue Task' ;
        objTask.Start_Date_Time__c = system.now();
        objTask.Review_Start_Date_Time__c  = null;
        objTask.IsReminderSet = true;
        insert objTask;
        List<UpdateM1StipDetailsCTRL.input> M1lstUInput = new List<UpdateM1StipDetailsCTRL.Input>();
        UpdateM1StipDetailsCTRL.input uInput1 = new UpdateM1StipDetailsCTRL.Input(underWrFilRec.Id);
        M1lstUInput.add(uInput1);
        UpdateM1StipDetailsCTRL.UpdateM1StipDetails(M1lstUInput); 
    }
}