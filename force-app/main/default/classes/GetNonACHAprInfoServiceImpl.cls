/**
* Description: Fetch User API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Udaya Kiran            Jul/05/2019           Created
******************************************************************************************/
public with sharing class GetNonACHAprInfoServiceImpl extends RESTServiceBase{
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){  
         system.debug('### Entered into fetchData() of ' + GetNonACHAprInfoServiceImpl.class);
         ResponseWrapper res = new ResponseWrapper();
         GetNonACHAprInfoDataServiceImpl GetNonACHAprInfoDataServiceImplObj = new GetNonACHAprInfoDataServiceImpl();
         IRestResponse iRestRes;
         try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            res.response = GetNonACHAprInfoDataServiceImplObj.transformOutput(reqData);

        }catch(Exception err){
           
            system.debug('### GetNonACHAprInfoServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' GetNonACHAprInfoServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            
            UnifiedBean unifbean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            unifbean.error  = errorWrapperLst ;
            unifbean.returnCode = '214';
            iRestRes = (IRestResponse)unifbean;
            res.response= iRestRes;
        }
      system.debug('### Exit from fetchData() of ' + GetNonACHAprInfoServiceImpl.class);
      return res.response;     
    }
    
}