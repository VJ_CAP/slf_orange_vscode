/**
* Description: Report related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Deepika               11/09/2018              Created
******************************************************************************************/

public without sharing class SalesOperationReportingDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+SalesOperationReportingDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj; 
        system.debug('### reqData '+reqData);
        
        UnifiedBean unifiedBean = new UnifiedBean(); 
        unifiedBean.error = new list<UnifiedBean.errorWrapper>();
        
        
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        Savepoint sp = Database.setSavepoint();
        try
        {   
            Set<Id> ownerIDSet = new Set<Id>();
            Set<Id> installerIdSet = new Set<Id>();
            String userId = ''; 
            Boolean isExecutive = false;
            if(String.isNotBlank(reqData.userId))
            {
                userId = reqData.userId;                
            }
            else{
                userId = UserInfo.getUserId();               
            }
            System.debug('userId::'+userId);
            if(String.isNotBlank(UserId)){
                List<User> loggedInUser = new List<User>();
                loggedInUser = [Select id,contactId,
                                       UserRoleId,UserRole.Name,
                                       contact.AccountId,contact.Account.Inspection_Ever_Enabled__c,
                                       contact.Account.Kitting_Ever_Enabled__c,contact.Account.Permit_Ever_Enabled__c, contact.Account.ParentId 
                                       from User where id = :userId];
                System.debug('loggedInUser ::'+loggedInUser );
                if(!loggedInUser.isEmpty() && loggedInUser[0] != null){
                    
                    
                    
                    if(String.isBlank(reqData.userId) && loggedInUser[0].UserRoleId != null && loggedInUser[0].UserRole.Name.contains('Executive')){
                        isExecutive = true;
                        System.debug('reqData.userId::');
                        
                        //Added by Deepika as Part of Orange - 2092 - Start
                        Id installerIdProvided;
                        if(String.isBlank(reqData.installerId)){
                            installerIdProvided = loggedInUser[0].contact.AccountId;
                            installerIdSet.add(loggedInUser[0].contact.AccountId);
                        }else{
                            installerIdProvided = reqData.installerId;
                            installerIdSet.add(reqData.installerId);
                        }
                        
                        System.debug('loggedInUser[0].contactId'+loggedInUser[0].contactId);
                        System.debug('loggedInUser[0].contact.AccountId'+loggedInUser[0].contact.AccountId);
                        System.debug('loggedInUser[0].contact.Account.ParentId'+loggedInUser[0].contact.Account.ParentId);
                        System.debug('reqData.includeOrgHierarchy'+reqData.includeOrgHierarchy);
                        if(loggedInUser[0].contactId != null && loggedInUser[0].contact.AccountId != null && loggedInUser[0].contact.Account.ParentId == null && reqData.includeOrgHierarchy !=null && reqData.includeOrgHierarchy){
                            Map<Id,Account> accMap = new Map<Id,Account>([Select id from Account where ParentId = :installerIdProvided]);
                            if(!accMap.isEmpty()){
                                installerIdSet.addAll(accMap.keySet());
                            }                                                               
                        }                       
                        //Added by Deepika as Part of Orange - 2092 - End
                    }
                    else{
                        installerIdSet.add(loggedInUser[0].contact.AccountId);//Added by Deepika as Part of Orange - 2092
                        isExecutive = false;
                        ownerIDSet.add(userId);
                        if(reqData.includeHierarchy != null && reqData.includeHierarchy){
                            Id loggedInUSerConId = loggedInUser[0].contactId;
                            List<User> ownerList = new List<User>();
                            ownerList = [Select id from User where Contact.ReportsToId = :loggedInUSerConId];
                            for(User uObj : ownerList){
                                ownerIDSet.add(uObj.Id);
                            }                
                        }
                        System.debug('ownerIDSet::'+ownerIDSet);
                    }
                    
                    
                    //Added as part of Orange - 2358 - start
                    if(String.isBlank(reqData.projectCategory)) 
                        reqData.projectCategory = 'Solar';
                    //Added as part of Orange - 2358 - stop
                    
                    if(reqData.startDate != null && reqData.endDate != null ){
                        
                        Date startD = Date.valueOf(reqData.startDate);
                        Date endD = Date.valueOf(reqData.endDate);
                        System.debug('startD ::'+startD );
                        System.debug('endD ::'+endD );
                        DateTime startDt = Datetime.newInstance(startD.year(),startD.month(),startD.day(),0,0,0);
                        DateTime endDt = Datetime.newInstance(endD.year(),endD.month(),endD.day(),23,59,59);
                        System.debug('startDt ::'+startDt );
                        System.debug('endDt ::'+endDt );
                        String queryString = '';
                        //Added installerIdSet in query by Deepika as Part of Orange - 2092
                        queryString = 'SELECT Id,Project_Status_Detail__c, Opportunity__r.StageName,Opportunity__c,Project_Status__c,Opportunity__r.Installer_Account__r.Permit_Ever_Enabled__c,Opportunity__r.Installer_Account__r.Kitting_Ever_Enabled__c,Opportunity__r.Installer_Account__r.Inspection_Ever_Enabled__c,(Select id,Installer_Only_Email__c,Underwriting__c from Stipulations__r where Status__c != \'Completed\' AND Suppress_In_Portal__c = false) from Underwriting_File__c WHERE Opportunity__r.EDW_Originated__c = false AND Opportunity__r.StageName !=\'Archive\' AND Opportunity__r.Installer_Account__c IN :installerIdSet AND Opportunity__r.Project_Category__c = \''+reqData.projectCategory +'\' AND CreatedDate >= :startDt AND CreatedDate <= :endDt';
                        if(!isExecutive && !ownerIDSet.isEmpty())
                            queryString += ' AND Opportunity__r.ownerID IN :ownerIDSet';
                        List<Underwriting_File__c> undWList = new List<Underwriting_File__c>();
                        System.debug('queryString::'+queryString);
                        undWList = Database.query(queryString);
                        System.debug('undWList::'+undWList);                  
                        Map<String,Integer> stageCountMap = new Map<String,Integer>();
                        if(String.isNotBlank(reqData.type) && reqData.type == 'Sales'){
                            Set<Id> newStageOppId = new Set<Id>();
                            stageCountMap = new Map<String,Integer>{'New'=>0,'New - Prequalified'=>0,'Credit Application Sent'=>0,'Credit Pending Review'=>0,'Credit Approved'=>0,'Loan Agreement Sent'=>0,'Loan Agreement Signed'=>0,'Project Withdrawn'=>0};
                            Integer totalOppswithOpenStips = 0;
                            Set<Id> installerActionOppSet = new Set<Id>();
                            Set<Id> customerActionOppSet = new Set<Id>();
                            for(Underwriting_File__c uwObj : undWList){
                                if(uwObj.Project_Status__c == 'Project Withdrawn'){
                                    stageCountMap.put('Project Withdrawn',stageCountMap.get('Project Withdrawn')+1);
                                }
                                else{
                                    if(uwObj.Opportunity__r.StageName == 'Credit Application Sent'){
                                        stageCountMap.put('Credit Application Sent',stageCountMap.get('Credit Application Sent')+1);
                                    }
                                    else if(uwObj.Opportunity__r.StageName == 'Credit Pending Review'){
                                        stageCountMap.put('Credit Pending Review',stageCountMap.get('Credit Pending Review')+1);
                                    }
                                    else if(uwObj.Opportunity__r.StageName == 'Credit Approved'){
                                        stageCountMap.put('Credit Approved',stageCountMap.get('Credit Approved')+1);
                                    }
                                    else if(uwObj.Opportunity__r.StageName == 'Loan Agreement Sent'){
                                        stageCountMap.put('Loan Agreement Sent',stageCountMap.get('Loan Agreement Sent')+1);
                                    }
                                    else if(uwObj.Opportunity__r.StageName == 'Closed Won'){
                                        stageCountMap.put('Loan Agreement Signed',stageCountMap.get('Loan Agreement Signed')+1);
                                    }
                                    else if(uwObj.Opportunity__r.StageName == 'New'){
                                        newStageOppId.add(uwObj.Opportunity__c);
                                    }
                                }
                                //Added by Deepika as Part of Orange - 2035 - Start
                                if(!uwObj.Stipulations__r.isEmpty()){
                                    totalOppswithOpenStips += 1;
                                    for(Stipulation__c stipObj : uwObj.Stipulations__r){
                                        if(stipObj.Installer_Only_Email__c)
                                            installerActionOppSet.add(stipObj.Underwriting__c);
                                        else
                                            customerActionOppSet.add(stipObj.Underwriting__c);
                                    }
                                } 
                                                             
                            }
                            unifiedBean.stipsSummary = new List<UnifiedBean.RowWrapper>();
                            UnifiedBean.RowWrapper rowWrprObj1 = new UnifiedBean.RowWrapper();
                            rowWrprObj1.label = 'Total Number of Accounts';
                            rowWrprObj1.numberOfProjects = totalOppswithOpenStips;                                
                            unifiedBean.stipsSummary.add(rowWrprObj1);
                            UnifiedBean.RowWrapper rowWrprObj2 = new UnifiedBean.RowWrapper();
                            rowWrprObj2.label = 'Installer Action Required';
                            rowWrprObj2.numberOfProjects = installerActionOppSet.size();                                
                            unifiedBean.stipsSummary.add(rowWrprObj2);
                            UnifiedBean.RowWrapper rowWrprObj3 = new UnifiedBean.RowWrapper();
                            rowWrprObj3.label = 'Customer Action Required';
                            rowWrprObj3.numberOfProjects = customerActionOppSet.size();                                
                            unifiedBean.stipsSummary.add(rowWrprObj3);
                            //Added by Deepika as Part of Orange - 2035 - End
                            
                            Map<Id, Opportunity> oppMap;
                            if(!newStageOppId.isEmpty()){
                                oppMap = new Map<Id, Opportunity>([Select id,(select id from Prequals__r where Pre_Qual_Status__c = 'Auto Approved') from Opportunity where id IN:newStageOppId]);            
                            }
                            for(Underwriting_File__c uwObj : undWList){ 
                                if(uwObj.Project_Status__c != 'Project Withdrawn' && uwObj.Opportunity__r.StageName == 'New' && (oppMap == null || (!oppMap.isEmpty() && oppMap.get(uwObj.Opportunity__c).Prequals__r.isEmpty()))){                     
                                    stageCountMap.put('New',stageCountMap.get('New')+1);
                                }
                                else if(uwObj.Opportunity__r.StageName == 'New' && oppMap != null && !oppMap.isEmpty() && oppMap.containsKey(uwObj.Opportunity__c)  && oppMap.get(uwObj.Opportunity__c) != null && !oppMap.get(uwObj.Opportunity__c).Prequals__r.isEmpty()){
                                    stageCountMap.put('New - Prequalified',stageCountMap.get('New - Prequalified')+1);
                                }
                            }               
                        }
                        else if(String.isNotBlank(reqData.type) && reqData.type == 'Operations'){
                            Map<String,Integer> stageCountTempMap = new Map<String,Integer>{'M0 - Documents Needed'=>0, 'M0 - In Review'=>0, 'Permit - Documents Needed'=>null, 'Permit - In Review'=>null, 'Kitting - Documents Needed'=>null, 'Kitting - In Review'=>null, 'M1 - Documents Needed'=>0, 'M1 - In Review'=>0, 'Inspection - Documents Needed'=>null, 'Inspection - In Review'=>null, 'M2 - Documents Needed'=>0, 'M2 - In Review'=>0,'M2 Payment Pending'=>0,'Project Completed'=>0,'Project Withdrawn'=>0}; 
                            
                            Map<String,Integer> homeImprMap = new Map<String,Integer>{'Documents Needed'=>0, 'Loan Agreement In Review'=>0, 'Payment Stage'=>0, 'Payment Requested'=>0,'Project Completed'=>0,'Project Withdrawn'=>0}; 
                            
                            if(reqData.projectCategory == 'Solar'){//Added condition by Deepika as part of Orange - 2358 on 25-02-2018
                                for(Underwriting_File__c uwObj : undWList){
                                    system.debug('***Id***'+uwObj.Id+'**Project_Status**'+uwObj.Project_Status__c);
                                    
                                    if(null != uwObj.Project_Status__c)
                                    {
                                        if(uwObj.Project_Status_Detail__c == 'M0 - Documents Needed'){
                                            stageCountTempMap.put('M0 - Documents Needed',stageCountTempMap.get('M0 - Documents Needed')+1);
                                        }
                                        else if(uwObj.Project_Status_Detail__c == 'M0 - In Review'){
                                            stageCountTempMap.put('M0 - In Review',stageCountTempMap.get('M0 - In Review')+1);
                                        }                                   
                                        else if(uwObj.Project_Status_Detail__c == 'M1 - Documents Needed'){
                                            stageCountTempMap.put('M1 - Documents Needed',stageCountTempMap.get('M1 - Documents Needed')+1);
                                        }
                                        else if(uwObj.Project_Status_Detail__c == 'M1 - In Review'){
                                            stageCountTempMap.put('M1 - In Review',stageCountTempMap.get('M1 - In Review')+1);
                                        }
                                        else if(uwObj.Project_Status_Detail__c == 'M2 - Documents Needed'){
                                            stageCountTempMap.put('M2 - Documents Needed',stageCountTempMap.get('M2 - Documents Needed')+1);
                                        }
                                        else if(uwObj.Project_Status_Detail__c == 'M2 - In Review'){
                                            stageCountTempMap.put('M2 - In Review',stageCountTempMap.get('M2 - In Review')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'M2 Payment Pending'){
                                            stageCountTempMap.put('M2 Payment Pending',stageCountTempMap.get('M2 Payment Pending')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'Project Completed'){
                                            stageCountTempMap.put('Project Completed',stageCountTempMap.get('Project Completed')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'Project Withdrawn'){
                                            stageCountTempMap.put('Project Withdrawn',stageCountTempMap.get('Project Withdrawn')+1);
                                        }
                                        if(loggedInUser[0].contact.Account.Permit_Ever_Enabled__c){
                                            if(stageCountTempMap.get('Permit - Documents Needed') == null)
                                                stageCountTempMap.put('Permit - Documents Needed',0);
                                            if(stageCountTempMap.get('Permit - In Review') == null)
                                                stageCountTempMap.put('Permit - In Review',0);  
                                                                                         
                                            if(uwObj.Project_Status_Detail__c == 'Permit - Documents Needed'){                      
                                                stageCountTempMap.put('Permit - Documents Needed',stageCountTempMap.get('Permit - Documents Needed')+1);
                                            }
                                            else if(uwObj.Project_Status_Detail__c == 'Permit - In Review'){                        
                                                stageCountTempMap.put('Permit - In Review',stageCountTempMap.get('Permit - In Review')+1);
                                            }                                                                               
                                        }
                                        if(loggedInUser[0].contact.Account.Kitting_Ever_Enabled__c){
                                            if(stageCountTempMap.get('Kitting - Documents Needed') == null)
                                                stageCountTempMap.put('Kitting - Documents Needed',0);
                                            if(stageCountTempMap.get('Kitting - In Review') == null)
                                                stageCountTempMap.put('Kitting - In Review',0);
                                                
                                            if(uwObj.Project_Status_Detail__c == 'Kitting - Documents Needed'){                        
                                                stageCountTempMap.put('Kitting - Documents Needed',stageCountTempMap.get('Kitting - Documents Needed')+1);
                                            }
                                            else if(uwObj.Project_Status_Detail__c == 'Kitting - In Review'){                       
                                                stageCountTempMap.put('Kitting - In Review',stageCountTempMap.get('Kitting - In Review')+1);
                                            } 
                                        }
                                        if(loggedInUser[0].contact.Account.Inspection_Ever_Enabled__c){
                                            if(stageCountTempMap.get('Inspection - Documents Needed') == null)
                                                stageCountTempMap.put('Inspection - Documents Needed',0);
                                            if(stageCountTempMap.get('Inspection - In Review') == null)
                                                stageCountTempMap.put('Inspection - In Review',0);
                                                
                                            if(uwObj.Project_Status_Detail__c == 'Inspection - Documents Needed'){                     
                                                stageCountTempMap.put('Inspection - Documents Needed',stageCountTempMap.get('Inspection - Documents Needed')+1);
                                            }
                                            else if(uwObj.Project_Status_Detail__c == 'Inspection - In Review'){                        
                                                stageCountTempMap.put('Inspection - In Review',stageCountTempMap.get('Inspection - In Review')+1);
                                            }
                                        }
                                    }
                                }
                                 if(stageCountTempMap != null && !stageCountTempMap.isEmpty()){
                                    for(String mapKey : stageCountTempMap.KeySet()){
                                        if(stageCountTempMap.get(mapKey) != null)
                                            stageCountMap.put(mapKey,stageCountTempMap.get(mapKey));
                                    }
                                }
                            }
                            //Added by Deepika as part of Orange - 2358 on 25-02-2018 - Start
                            else if(reqData.projectCategory == 'Home'){
                                for(Underwriting_File__c uwObj : undWList){
                                    system.debug('***Id***'+uwObj.Id+'**Project_Status**'+uwObj.Project_Status__c);
                                    if(null != uwObj.Project_Status__c)
                                    {
                                        if(uwObj.Project_Status__c == 'Documents Needed'){
                                            homeImprMap.put('Documents Needed',homeImprMap.get('Documents Needed')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'Loan Agreement Review'){
                                            homeImprMap.put('Loan Agreement In Review',homeImprMap.get('Loan Agreement In Review')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'Payment Stage'){
                                            homeImprMap.put('Payment Stage',homeImprMap.get('Payment Stage')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'Payment Stage' && uwObj.Project_Status_Detail__c == 'Payment Requested'){
                                            homeImprMap.put('Payment Requested',homeImprMap.get('Payment Requested')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'Project Completed'){
                                            homeImprMap.put('Project Completed',homeImprMap.get('Project Completed')+1);
                                        }
                                        else if(uwObj.Project_Status__c == 'Project Withdrawn'){
                                            homeImprMap.put('Project Withdrawn',homeImprMap.get('Project Withdrawn')+1);
                                        }                                       
                                    }
                                }  
                                if(homeImprMap != null && !homeImprMap.isEmpty()){
                                    stageCountMap = new Map<String,Integer>();
                                    stageCountMap.putAll(homeImprMap);
                                }                              
                            }
                            //Added by Deepika as part of Orange - 2358 on 25-02-2018 - End 
                        }
                    
                        unifiedBean.returnCode = '200';
                        Integer total = 0;
                        unifiedBean.dataElements = new List<UnifiedBean.RowWrapper>();
                        for(String stage : stageCountMap.keySet()){
                            if(stageCountMap.get(stage) != null)
                                total += stageCountMap.get(stage);          
                        }
                        System.debug('stageCountMap::'+stageCountMap);
                        System.debug('total::'+total);
                        unifiedBean.total = String.valueOf(total);
                        for(String stage : stageCountMap.keySet()){
                            UnifiedBean.RowWrapper rowWrprObj = new UnifiedBean.RowWrapper();
                            rowWrprObj.status = stage;
                            rowWrprObj.numberOfProjects = stageCountMap.get(stage);
                            if(stageCountMap.get(stage) != null){
                                if(total != 0 && total != null){
                                    rowWrprObj.percentage = String.valueOf((Decimal.valueOf(stageCountMap.get(stage)*100)/Decimal.valueOf(total)).setScale(1))+'%';
                                    System.debug('rowWrprObj.percentage::'+rowWrprObj.percentage);
                                }else{
                                    rowWrprObj.percentage = '0%';
                                }
                            }
                            unifiedBean.dataElements.add(rowWrprObj);
                        }
                    }                    
                }
                else{
                    iolist = new list<DataValidator.InputObject>{};
                    errMsg = ErrorLogUtility.getErrorCodes('214', null);                    
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    unifiedBean.error.add(errorMsg);
                    unifiedBean.returnCode = '214';
                    iRestRes = (IRestResponse)unifiedBean;
                    return iRestRes;
                }
            }           
            
            iRestRes = (IRestResponse)unifiedBean;
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400', null);
            
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedBean.error.add(errorMsg);
            unifiedBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedBean;
            
            
            system.debug('### SalesOperationReportingDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('SalesOperationReportingDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+SalesOperationReportingDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }  
}