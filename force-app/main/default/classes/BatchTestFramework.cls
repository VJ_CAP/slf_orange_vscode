global with sharing class BatchTestFramework 
							implements Database.Batchable<SObject>,
										Database.Stateful, 
										Database.AllowsCallouts,
										Schedulable 
{
	Boolean testMode;
	public Job__c job; 

	class Parameters
	{
		Integer numberOfAccounts = 100;
	}

	Parameters params = new Parameters ();

//******************************************************************

	global BatchTestFramework() 
	{
		//  nothing to do
		//  JobSupport.submitJob('BatchTestFramework', '{"numberOfAccounts":10}');
		//  JobSupport.scheduleHourlyJob('BatchTestFramework', 5, '{"numberOfAccounts":10}');
	}

//******************************************************************

	global void execute(SchedulableContext sc)  
	{
		JobSupport.submitJob('BatchTestFramework', sc.getTriggerId());
	}

//******************************************************************

	global database.querylocator start (Database.BatchableContext bc)
	{		
        this.job = JobSupport.getJobForId (bc.getJobId());
		JobSupport.addLineToJobLog (this.job, 'Started', true);
        update this.job;
																		
        if (this.job.Parameters__c != null)
        {
            this.params = (Parameters) JSON.deserialize (this.job.Parameters__c, Parameters.class);
        }

        System.debug(this.params);

        JobSupport.addLineToJobLog (this.job, 'Parameters: ' + this.params, false);

		String query = 'SELECT Id, Name FROM Account LIMIT ' + this.params.numberOfAccounts;
							
		Database.Querylocator ql = Database.getqueryLocator(query); 
		
		return ql;   
	}

//******************************************************************

	global void execute (Database.BatchableContext bc, list<Account> accounts) 
	{						 

    	Batch__c batch = new Batch__c (Job__c = this.job.Id);
    	
    	JobSupport.addLineToBatchLog (batch, 'Started', true);
		insert batch;

		for (Account a : accounts)
		{
			JobSupport.addLineToBatchLog (batch, 'Processed Account OK: ' + a.Name, false);
		}

    	JobSupport.addLineToBatchLog (batch, 'Finished', true);
		update batch;
	}

//******************************************************************
	
	global void finish (Database.BatchableContext bc)
	{
		JobSupport.addLineToJobLog (this.job, 'Finished', true);	
        update this.job;        
    }
}