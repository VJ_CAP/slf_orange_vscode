/**
* Description: Validate Document Uploadintegration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class ValidateDocumentUploadServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ValidateDocumentUploadServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        ValidateDocumentUploadDataServiceImpl dataSrvImpl = new ValidateDocumentUploadDataServiceImpl(); 
        try{
            system.debug('### rw.reqDataStr...'+rw.reqDataStr);
            ValidateDocumentUploadDataObject reqData = (ValidateDocumentUploadDataObject)JSON.deserialize(rw.reqDataStr, ValidateDocumentUploadDataObject.class);
            system.debug('### reqData ...'+reqData);
            res.response = dataSrvImpl.transformOutput(reqData);
        }catch(Exception err){
           system.debug('### ValidateDocumentUploadServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           
           ErrorLogUtility.writeLog('ValidateDocumentUploadServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + ValidateDocumentUploadServiceImpl.class);
        return res.response ;
    }
}