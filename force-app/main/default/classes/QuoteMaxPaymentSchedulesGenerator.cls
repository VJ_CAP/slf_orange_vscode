/**
* Description: This Class is to Caleculate Payment Schedule Max Amount records 
*              from Max Amount Approved on Quote object.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer                 Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K           12/26/2018          Initial Build
******************************************************************************************/
public class QuoteMaxPaymentSchedulesGenerator {
    
    public static void run(){
        
        if(!(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))) return;
        
        Quote[] quotes = getQuotesInScope();
        
        if(trigger.isUpdate && null != quotes && !quotes.isEmpty())
            deleteStalePaymentSchedules(quotes);
        
        if(null != quotes && !quotes.isEmpty())
            generateNewPaymentSchedules(quotes);
    }
    
    private static Quote[] getQuotesInScope(){
        
        Quote[] quotes = (Quote[])trigger.new;
        
        Quote[] ret = new Quote[]{};
        
        for(Quote quote : quotes){
            
            if(isInScope(quote)) ret.add(quote);
        }
        
        return ret;
    }
    
    private static Boolean isInScope(Quote quote){
        
        if(quote.Product_Loan_Type__c != 'HII' && quote.Product_Loan_Type__c != 'HIS' && quote.Product_Loan_Type__c != 'HIN') //Updated by Ravi as part of 3666
            return false;
        
        if(trigger.isInsert){
            return quote.Max_Amount_Approved__c != null && quote.SLF_Product__c != null;
        }
        else if(trigger.isUpdate){
            return quote.Max_Amount_Approved__c != trigger.oldMap.get(quote.Id).get('Max_Amount_Approved__c')
                || quote.SLF_Product__c != trigger.oldMap.get(quote.Id).get('SLF_Product__c');
        }
        
        return false;
    }
    
    private static void deleteStalePaymentSchedules(Quote[] quotes){
        
        if(quotes.isEmpty()) return;
        
        delete [SELECT Id FROM Payment_Schedule_Max_Amount__c WHERE Quote__c IN: quotes];
    }
    
    private static void generateNewPaymentSchedules(Quote[] quotes){
        system.debug('in generateNewPaymentSchedules');
        List<Payment_Schedule_Max_Amount__c> newPaymentSchedules = new List<Payment_Schedule_Max_Amount__c>();
        
        Map<Id, Product__c> productMap = getProductMap(quotes);
         // Added by Venkat as part of 1621
        Map<Id,Quote> updateQuoteMap = new Map<Id,Quote>(); 
        Map<Id,Opportunity> updateOptyMap = new Map<Id,Opportunity>();
        //Added by Deepika as part of Orange - 3622
        Map<Id,Quote> quotewithOppMap = new Map<Id,Quote>([Select id, Max_Amount_Approved__c, Combined_Loan_Amount__c, 
                Opportunity.Long_Term_APR__c,Opportunity.Promo_APR__c from Quote where id IN : quotes]);
        
        system.debug('### quotewithOppMap - '+quotewithOppMap);
        
        /*Set<Id> optyIds = new Set<Id>();
        
        for(Quote quote : quotes){
            if(quote.OpportunityId != null){
                optyIds.add(quote.OpportunityId);
            }
        }
        
        Map<Id,Opportunity> optyMap = new Map<id,Opportunity>([Select id, Long_Term_APR__c from opportunity where id in : optyIds]); 
        */
        for(Quote qteRec : quotes){
            // Added by Venkat as part of 1621
            Quote quoteRec = new Quote(Id=qteRec.id);
            Opportunity optyRec = new Opportunity(Id=qteRec.OpportunityId);
            if(qteRec.Max_Amount_Approved__c != 0 && qteRec.Max_Amount_Approved__c != null && productMap.containsKey(qteRec.SLF_Product__c)){
                
                Product__c product = productMap.get(qteRec.SLF_Product__c);
                
                 //Modified by Deepika as part of Orange - 3622 - start
                if(!quotewithOppMap.isEmpty() && quotewithOppMap.containsKey(qteRec.Id)){
                    //Orange-9015 Udaya Kiran Start
                    //newPaymentSchedules.addAll(createPaymentSchedules(quotewithOppMap.get(qteRec.Id), qteRec.Max_Amount_Approved__c, product));
                    if(qteRec.Installer_Max_Loan_Amount__c != null){
                        newPaymentSchedules.addAll(createPaymentSchedules(quotewithOppMap.get(qteRec.Id), qteRec.Installer_Max_Loan_Amount__c, product));   
                    }
                        
                    //Orange-9015 Udaya Kiran End
                }
                //Modified by Deepika as part of Orange - 3622 - end
                
                //Added by Adithya to fix ORANGE-10775 on 9/25/2019
                //Start
                Map<id,List<Payment_Schedule_Max_Amount__c>> quoteMap = new Map<id,List<Payment_Schedule_Max_Amount__c>>();
                
                for(Payment_Schedule_Max_Amount__c paymentIterate : newPaymentSchedules){
                    if(quoteMap.containsKey(paymentIterate.Quote__c))
                    {
                        List<Payment_Schedule_Max_Amount__c> paymentRec = quoteMap.get(paymentIterate.Quote__c);
                        paymentRec.add(paymentIterate);
                        quoteMap.put(paymentIterate.Quote__c,paymentRec);
                    }else{
                        quoteMap.put(paymentIterate.Quote__c,new List<Payment_Schedule_Max_Amount__c>{paymentIterate});
                    }
                }
                List<Payment_Schedule_Max_Amount__c> respectiveQuotePaymentSchedules = new List<Payment_Schedule_Max_Amount__c>();
                respectiveQuotePaymentSchedules  = quoteMap.get(qteRec.Id);
                system.debug('respectiveQuotePaymentSchedules  ============'+respectiveQuotePaymentSchedules  );
                //End
                 Decimal totalPayment = 0;
                if(respectiveQuotePaymentSchedules != null){
                    // Added by Venkat as part of 1621- Start
                    system.debug('Promo_Monthly_Payment_Max__c - '+respectiveQuotePaymentSchedules[0].Amount__c);
                    quoteRec.Promo_Monthly_Payment_Max__c  = respectiveQuotePaymentSchedules[0].Amount__c;
                    optyRec.Promo_Monthly_Payment_Max_Amount__c = respectiveQuotePaymentSchedules[0].Amount__c;
                    system.debug('Standard_Monthly_Payment_Max_Amount__c - '+respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c);
                    quoteRec.Standard_Monthly_Payment_Max_Amount__c = respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c;
                    optyRec.Standard_Monthly_Payment_Max_Amount__c = respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c;
                    for(Payment_Schedule_Max_Amount__c  paymentIterate : respectiveQuotePaymentSchedules){
                        if(qteRec.Id == paymentIterate.Quote__c){//added If condition by Adithya to Fix ORANGE-10775 on 9/25/2019
                            totalPayment += paymentIterate.Amount__c * paymentIterate.Months__c;
                        }
                    } // If condition added for Orange-7660 defect.
                }
               
                
                system.debug('### Long Term APR - '+ quotewithOppMap.get(qteRec.Id).Opportunity.Long_Term_APR__c);
                system.debug('### Opty Id - '+qteRec.OpportunityId);
                
                system.debug('### qteRec - '+qteRec);
                system.debug('### isACH__c - '+qteRec.Opportunity.isACH__c);
                system.debug('### ACH_APR__c - '+qteRec.Opportunity.ACH_APR__c);
                system.debug('### NonACH_APR__c - '+qteRec.Opportunity.NonACH_APR__c);
                system.debug('### SLF_Product__c - '+qteRec.Opportunity.SLF_Product__c);
                system.debug('### qteRec.Opportunity - '+qteRec.Opportunity);
                system.debug('### Long_Term_APR__c - '+qteRec.Opportunity.Long_Term_APR__c);
                
                if(quotewithOppMap.get(quoteRec.Id).Opportunity.Long_Term_APR__c == 0.00 || quotewithOppMap.get(quoteRec.id).Opportunity.Long_Term_APR__c == null){
                    quoteRec.Total_of_Payments_Max_Amount__c = qteRec.Max_Amount_Approved__c;
                    optyRec.Total_of_Payments_Max_Amount__c = qteRec.Max_Amount_Approved__c;                    
                }else{
                    quoteRec.Total_of_Payments_Max_Amount__c = totalPayment;
                    optyRec.Total_of_Payments_Max_Amount__c = totalPayment;
                }
                system.debug('totalPayment - '+totalPayment);
                updateQuoteMap.put(quoteRec.Id,quoteRec);
                updateOptyMap.put(optyRec.Id,optyRec);
                // Added as  part of 1621 - End
            }

        }
        System.debug('@@@newPaymentSchedules'+newPaymentSchedules);
        insert newPaymentSchedules;
          // Added by Venkat as part of 1621- Start
        if(!updateOptyMap.isEmpty()){
            system.debug('updateOptyMap - '+updateOptyMap);
            update updateOptyMap.values();
        }
        if(!updateQuoteMap.isEmpty())
        {
            update updateQuoteMap.values();
        }
        // Added as  part of 1621 - End
    }
    
    private static List<Payment_Schedule_Max_Amount__c> createPaymentSchedules(Quote quoteObj, Decimal loanAmount, Product__c product){
        //Added by Deepika as part of Orange - 3622
        Decimal promoAPRInt = product.Promo_APR__c;
        /*
        //Commented By Adithya to fix ORANGE-10775 on 9/25/2019
        //start
        if(quoteObj != null){
            promoAPRInt = quoteObj.Opportunity.Promo_APR__c;
        }
        //End
        */
        //Added by Deepika as part of Orange - 3622
        Integer term = (Integer)product.Term_mo__c,
        //drawPeriod = product.Draw_Period__c == null ? 0 : (Integer)product.Draw_Period__c, //Updated by Ravi as part of 3666
        drawPeriod = product.Draw_Period__c == null ? null : (Integer)product.Draw_Period__c, 
        promoPeriod = product.Promo_Term__c == null ? 0 : (Integer)product.Promo_Term__c;
        
        Decimal APR = product.APR__c == null ? 0 : product.APR__c/100,
        promoAPR = promoAPRInt == null ? 0 : promoAPRInt/100, 
        
        /* Updated by Ravi as part of 3666
        promoFixedPayment = product.Promo_Fixed_Payment__c == null ? 0 : product.Promo_Fixed_Payment__c, 
        promoPercentBalance = product.Promo_Percentage_Balance__c == null ? 0 : product.Promo_Percentage_Balance__c/100, 
        promoPercentPayment = product.Promo_Percentage_Payment__c == null ? 0 : product.Promo_Percentage_Payment__c/100;
        */
        promoFixedPayment = product.Promo_Fixed_Payment__c,
        promoPercentBalance = product.Promo_Percentage_Balance__c == null ? null : product.Promo_Percentage_Balance__c/100, 
        promoPercentPayment = product.Promo_Percentage_Payment__c == null ? null : product.Promo_Percentage_Payment__c/100;
        
        LoanCalculator.PaymentSchedule[] paymentSchedules = LoanCalculator.getPaymentSchedules(
            loanAmount, term, APR, drawPeriod, promoPeriod, promoAPR, promoPercentPayment, promoPercentBalance, promoFixedPayment);

        List<Payment_Schedule_Max_Amount__c> ret = new List<Payment_Schedule_Max_Amount__c>();
        id quoteId = null;
        if(null != quoteObj && null != quoteObj.id)
            quoteId = quoteObj.id;
        for(Integer i = 0; i < paymentSchedules.size(); i++){
            
            ret.add(new Payment_Schedule_Max_Amount__c(
                Quote__c = quoteId,
                Serial_No__c = i + 1,
                Amount__c = paymentSchedules[i].Payment,
                Months__c = paymentSchedules[i].Months
            ));
        }
        
        return ret;
    }
    
    private static Map<Id, Product__c> getProductMap(Quote[] quotes){
        
        Set<Id> productIds = new Set<Id>();
        
        for(Quote quote : quotes){
            
            if(quote.SLF_Product__c != null){
                productIds.add(quote.SLF_Product__c);
            }
        }
        
        if(productIds.isEmpty())
            return new Map<Id, Product__c>();
        
        return new Map<Id, Product__c>([SELECT Product_Loan_Type__c, Term_mo__c, APR__c,  Promo_APR__c, Promo_Fixed_Payment__c, 
            Promo_Percentage_Balance__c, Promo_Percentage_Payment__c, Promo_Term__c, Draw_Period__c
            FROM Product__c WHERE Id IN: productIds]);
    }
}