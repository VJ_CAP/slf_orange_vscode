@isTest
private class SnapshotProcessLineItems_Test 
{   
    @isTest static void test_1 () 
    {
        SLFUtilityTest.createCustomSettings();
        
        Account a1 = new Account (Name = 'Test Facility', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c = true);
        insert a1;

        Account a2 = new Account (Name = 'Test Partner', Type = 'Partner',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c = true);
        insert a2;

        Funding_Snapshot__c fs1 = new Funding_Snapshot__c (Account__c = a1.Id);
        insert fs1;

        fs1.Processed__c = System.now();
        update fs1;

        Funding_Snapshot__c fs2 = new Funding_Snapshot__c (Account__c = a2.Id);
        insert fs2;

        fs2.Processed__c = System.now();
        update fs2;

        fs2.Processed__c = null;
        update fs2;



    }
}