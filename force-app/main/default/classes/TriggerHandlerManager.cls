/**
* Class for initializing and executing TriggerHandler methods.       
*
* @Author Salesforce
* @Date 19/11/2014
*/

public class TriggerHandlerManager {
    
    
    /**
    * This method creates and executes a trigger handler. 
    * 
    * 
    * 
    * @param t - Type
    */
    public static void createAndExecuteHandler(Type t)
    {
        // Get a handler appropriate to the object being processed
        TriggerHandler handler = getHandler(t);
        // Execute the handler to fulfil the trigger
        execute(handler);
    }
 
   
    /**
    * This method invokes the TriggerHandler methods . 
    * 
    * 
    * 
    * @param handler - TriggerHandler
    */
    
    private static void execute(TriggerHandler handler)
    {
        // Before Trigger
        if (Trigger.isBefore){
            // Call the bulk before to handle any caching of data and enable bulkification
            handler.bulkBefore();
 
            // Pass records to handler methods.
            if (Trigger.isDelete) {
                handler.beforeDelete(Trigger.oldMap);
            } 
            else if (Trigger.isInsert) {
                handler.beforeInsert(Trigger.new);
            } 
            else if (Trigger.isUpdate) {
                handler.beforeUpdate(Trigger.oldMap, Trigger.newMap); 
            }
        } 
        else {
            // Call the bulk after to handle any caching of data and enable bulkification
            handler.bulkAfter();
 
            // Iterate through the records deleted passing them to the handler.
            if (Trigger.isDelete) {
                handler.afterDelete(Trigger.oldMap);
            } 
            else if (Trigger.isInsert) {
                handler.afterInsert(Trigger.newMap);
            } 
            else if (Trigger.isUpdate) {
                handler.afterUpdate(Trigger.oldMap, Trigger.newMap);
            } 
            else if(Trigger.isUndelete) {
                handler.afterUndelete(Trigger.newMap);
            }
        }
 
        // Perform any post processing
        handler.andFinally();
    }
 
   
    /**
    * Get the named handler depending upon sObject Type . 
    * 
    * 
    * 
    * @param t - Type
    */
    
    private static TriggerHandler getHandler(Type t)
    {
        // Instantiate the type
        Object o = t.newInstance();
 
        // if its not an instance of ITrigger return null
        if (!(o instanceOf TriggerHandler)) {
            throw new TriggerHandlerException('No TriggerHandler named ' + t.getName());
        }
 
        return (TriggerHandler)o;
    }
    
    public class TriggerHandlerException extends Exception{}
}