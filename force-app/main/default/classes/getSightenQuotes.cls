public class getSightenQuotes implements Queueable, Database.AllowsCallouts{
    private String sightenUUID;
    private String stateCode;
    private ID opptyID;
    
    public getSightenQuotes(String sightenUUID, ID opptyID, String stateCode){
        
        //oppty.Sighten_UUID__c, oppty.Id, oppty.Install_State_Code__c
        this.sightenUUID = sightenUUID;
        this.opptyID = opptyID;
        this.stateCode = stateCode;
    }
    
    public void execute(QueueableContext context) {
        sightenCallout.getSightenQuotes(sightenUUID, opptyID, stateCode);
    }
}