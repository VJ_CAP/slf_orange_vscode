/**
* Description: Updating existing Underwriting Records to Solar RecordType 
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Brahmeswar Gajula      01/17/2019          Created
******************************************************************************************/
global class InstallerAccShareToParentAccountBatch implements Database.Batchable<sObject>{
    
     String strQuery = '';
     list<string> lstunderWriting = new list<string>();
     
     ID SolarRecordTypeID;
     public InstallerAccShareToParentAccountBatch(List<String> uWid){
        if(uWid != null && uWid.size()>0){
            lstunderWriting = uWid;
            strQuery = 'select id,Name,recordtypeid from Underwriting_File__c where id IN:lstunderWriting and Opportunity__c!= null';
        }else
           strQuery = 'select id,Name,recordtypeid from Underwriting_File__c where Opportunity__c!= null'; 
        
         SolarRecordTypeID = Schema.SObjectType.Underwriting_File__c.getRecordTypeInfosByName().get('Solar').getRecordTypeId();
        
     }
    
     global Database.QueryLocator start(Database.BatchableContext BC)
     {
         system.debug('***strQuery in start---'+strQuery);
         return Database.getQueryLocator(strQuery);
    
     }
     
     
    global void execute(Database.BatchableContext BC,List<Underwriting_File__c> lstUw)
    { 
        system.debug('***lstUw---'+lstUw);
        List<Underwriting_File__c> lstuWupdate = new List<Underwriting_File__c>();
        if(lstUw != null && lstUw.size()>0){
            for(Underwriting_File__c obj : lstUw){
                if(SolarRecordTypeID != null){
                    obj.recordtypeid = SolarRecordTypeID;
                    lstuWupdate.add(obj);
                }
            }
            system.debug('***lstuWupdate---'+lstuWupdate);
            if(lstuWupdate != null && lstuWupdate.size()>0){
                try{
                    Database.update(lstuWupdate);
                }catch(Exception ex){
                    System.debug('***Error--'+ex);
                }
                
            }
            
            
            
        }
    
        
    }
    global void finish(Database.BatchableContext BC)
    {
        System.debug('******* Existing UnderWriting Records are updated with record type -----');
    } 
     
   
    
}