/**
* Description: Status API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public with sharing class StatusDataServiceImpl extends RestDataServiceBase{
    
   /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        UnifiedBean unifBean = new UnifiedBean(); 
        
        StatusDataObject stsObj = (StatusDataObject)stsDtObj; 
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try{
            system.debug('### Entered into transformOutput() of '+StatusDataServiceImpl.class);
           // unifBean.projects = new List<UnifiedBean.OpportunityWrapper>();
          set<Id> OppIdset = new set<Id>();
           List<Opportunity> oppLst = new List<Opportunity>();
           Map<string,string> OppInvalidIdMap = new Map<string,string>();
           for(string reqIterate : stsObj.reqIdMap.keyset())
            {
                if(stsObj.oppMap.containskey(reqIterate))
                {
                    Opportunity oppRec = stsObj.oppMap.get(reqIterate);
                    oppLst.add(oppRec);
                }
            }
           
            if(!oppLst.isEmpty())
            {
                
                List<SLF_Credit__c>  creditList = [select Id,Name,Status__c from SLF_Credit__c where Opportunity__c =: oppLst[0].Id AND status__c =: 'Auto Approved' AND IsSyncing__c = true];
                
                if(creditList.isEmpty())
                {
                    List<Quote> quoteList = [select Id,Name,NonACH_APR__c, SLF_Product__r.Expiration_Date__c,OpportunityId,Opportunity.Installer_Account__r.Risk_Based_Pricing__c,Opportunity.Installer_Account__r.Credit_Waterfall__c from Quote where OpportunityId =: oppLst[0].Id AND IsSyncing = true];
                    
                    if(!quoteList.isEmpty())
                    {   
                        if(quoteList[0].SLF_Product__r.Expiration_Date__c < system.today() && quoteList[0].Opportunity.Installer_Account__r.Risk_Based_Pricing__c == false && quoteList[0].Opportunity.Installer_Account__r.Credit_Waterfall__c == false)
                        {
                            unifBean.returnCode = '362';
                            List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                            UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                            errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('362',null);
                            errorLst.add(errorWrap);
                            unifBean.error = errorLst;
                            return unifBean;
                        }
                    }
                }
                
            }
            
            
            for(string reqIterate : stsObj.reqIdMap.keyset())
            {
                UnifiedBean.OpportunityWrapper opportunityWrap = new UnifiedBean.OpportunityWrapper();
                opportunityWrap.credits = new List<UnifiedBean.CreditsWrapper>();
                opportunityWrap.stips = new List<UnifiedBean.StipsAndStatusWrapper>();
                opportunityWrap.contracts = new List<UnifiedBean.DocumentWrapper>();
                if(stsObj.oppMap.containskey(reqIterate))
                {
                    Opportunity oppRec = stsObj.oppMap.get(reqIterate);
                    
                    if(null != oppRec)
                    {
                        opportunityWrap.id  = oppRec.Id;
                        opportunityWrap.externalId  = oppRec.Partner_Foreign_Key__c ;
                        opportunityWrap.hashId  = oppRec.Hash_Id__c ;
                        // Added for ORANGE-2040 by Suma
                        //if(oppRec.ACH_APR_Process_Required__c)
                            opportunityWrap.nonACHApr = oppRec.NonACH_APR__c;
                        // End ORANGE-2040
                    }
                    if(stsObj.UnderwritingMap.containsKey(oppRec.Id))
                    {
                        List<Underwriting_File__c> UnderwritingLst = stsObj.UnderwritingMap.get(oppRec.Id);  
                        //Underwriting File information mapping
                        for(Underwriting_File__c underwritingIterate : UnderwritingLst)
                        {
                            if(UnderwritingIterate.Stipulations__r != null )
                            {
                                for(Stipulation__c stipulationIterate : UnderwritingIterate.Stipulations__r )
                                {
                                    UnifiedBean.StipsAndStatusWrapper stipDetails = new UnifiedBean.StipsAndStatusWrapper();
                                    stipDetails.id  = stipulationIterate.Id;
                                    stipDetails.name  = stipulationIterate.Stipulation_Data_Name__c;
                                    stipDetails.status  = stipulationIterate.Status__c;
                                    stipDetails.description = stipulationIterate.Description__c;
                                    if(null != stipulationIterate.Completed_Date__c)
                                        stipDetails.completedDate = String.ValueOf(stipulationIterate.Completed_Date__c);
                                    
                                    stipDetails.notes = stipulationIterate.ETC_Notes__c;
                                    stipDetails.installerOnlyEmail = stipulationIterate.Stipulation_Data__r.installer_only_email__c;
                                    stipDetails.errorMessage = '';
                                    opportunityWrap.stips.add(stipDetails);
                                }
                            }
                            opportunityWrap.projectStatus = underwritingIterate.Project_Status__c;
                            opportunityWrap.projectStatusDetail = underwritingIterate.Project_Status_Detail__c;
                            opportunityWrap.fundingStatus = underwritingIterate.Funding_status__c ;                            
                        }
                    }
                    OppIdset.add(oppRec.Id);
                    //unifBean = SLFUtility.getUnifiedBean(new set<Id>{oppRec.Id},'Orange');
                    //unifBean.returnCode = '200';
                }else{
                    
                    string errMsg = ErrorLogUtility.getErrorCodes('204',null);
                    OppInvalidIdMap.put(reqIterate,errMsg);
                }
            }
            if(!OppIdset.isEmpty())
            {
                unifBean = SLFUtility.getUnifiedBean(OppIdset,'Orange',false);
                system.debug('bean data in data service impl-----'+unifBean);
                unifBean.returnCode = '200';
            }
                
            
            if(!OppInvalidIdMap.isEmpty())  
            {
                unifBean.projects = new List<UnifiedBean.OpportunityWrapper>();
                for(string OppIterate : OppInvalidIdMap.keyset())
                {
                    UnifiedBean.OpportunityWrapper oppWrap = new UnifiedBean.OpportunityWrapper();
                    oppWrap.Id = OppIterate;
                    oppWrap.errorMessage = OppInvalidIdMap.get(OppIterate);
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,OppInvalidIdMap.get(OppIterate));
                    unifBean.error = new List<UnifiedBean.errorWrapper>();
                    unifBean.error.add(errorMsg);
                    unifBean.returnCode = '204';
                    unifBean.projects.add(oppWrap);
                }
            }
        }catch(Exception err){
            system.debug('### StatusDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('StatusDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null)); 
        }    
         system.debug('### Exit from transformOutput() of '+ StatusDataServiceImpl.class);
         return unifBean;
    }
    
}