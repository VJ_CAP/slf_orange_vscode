@isTest
public class RBPFNIFacilityDecisionDataSerImplTest {    
    
  @testsetup static void createtestdata(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
                
        TriggerFlags__c trgProdflag = new TriggerFlags__c();
        trgProdflag.Name ='Product__c';
        trgProdflag.isActive__c =true;
        trgLst.add(trgProdflag);
        
        TriggerFlags__c trgUwflag = new TriggerFlags__c();
        trgUwflag.Name ='Underwriting_File__c';
        trgUwflag.isActive__c =true;
        trgLst.add(trgUwflag);
        
        TriggerFlags__c trgFundingflag = new TriggerFlags__c();
        trgFundingflag.Name ='Funding_Data__c';
        trgFundingflag.isActive__c =true;
        trgLst.add(trgFundingflag);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();  
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con; 
        
        
        User portalUser = new User(alias = 'test1', Email='test1@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test1', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', 
        ProfileId = p.Id, 
        Default_User__c = true,                            
        //UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        
        insert portalUser;
        
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact personcon = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = personAcc.Id);
        insert personcon; 
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        personAcc.PersonBirthdate = Date.ValueOf('1929-03-10');
        update personAcc;
        
        AccountShare personAccShare = new AccountShare(); //a new empty AccountShare object
        personAccShare.userorgroupid = portalUser.Id;
        personAccShare.accountid = personAcc.Id;
        personAccShare.accountaccesslevel = 'Edit';
        personAccShare.OpportunityAccessLevel = 'None';
        personAccShare.CaseAccessLevel = 'None';
        insert personAccShare;
        
        
        System.runAs(portalUser)
        {
            Test.starttest();
            
        Product__c prod1 = new Product__c();
            prod1.Installer_Account__c =acc.id;
            prod1.Long_Term_Facility_Lookup__c =acc.id;
            prod1.Name='testprod1';
            prod1.FNI_Min_Response_Code__c=9;
            prod1.Product_Display_Name__c='test';
            prod1.Qualification_Message__c ='testmsg';
            prod1.Product_Loan_Type__c='HII';
            prod1.State_Code__c ='CA';
            prod1.ST_APR__c = 2.99;
            prod1.Long_Term_Facility__c = 'TCU';
            prod1.APR__c = 2.99;
            prod1.ACH__c = true;
            prod1.Internal_Use_Only__c = false;
            prod1.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod1.LT_Max_Loan_Amount__c =1000;
            prod1.Promo_Percentage_Payment__c = 20;
            prod1.Promo_Fixed_Payment__c = 100;
            prod1.Promo_Percentage_Balance__c = 20;
            prod1.Interest_Type__c = 'No Interest';
            prod1.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            //prod1.Product_Tier__c = '0';
            insert prod1;        
        
         //Insert SLF Product Record
            //List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='HIS';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            insert prod;
            //prodList.add(prod);
            
            
        
         Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            insert personOpp;
        
        //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            insert opp;
        
        SLF_Credit__c cred = new SLF_Credit__c();
            cred.Opportunity__c = opp.id;
            cred.Primary_Applicant__c = acc.id;
            cred.Co_Applicant__c = acc.id;
            cred.SLF_Product__c = prod.Id ;
            cred.Total_Loan_Amount__c = 100;
            cred.Status__c = 'New';
            cred.LT_FNI_Reference_Number__c = '123';
            cred.Application_ID__c = '123';
            insert cred;
        
        }
    }

     
     
     /*
* Description: This method is used to cover RBPFNIFacilityDecisionDataServiceImpl and RBPFNIFacilityDecisionService 
*
*/   
     //@testSetup 
     private testMethod static void RBPFNIFacilityDecisionServiceTestMethod(){
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'test1@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' and id = :loggedInUsr[0].contact.AccountId LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Project_Category__c,Name,Installer_Account__c,Install_State_Code__c,Partner_Foreign_Key__c,Product_Loan_Type__c,SLF_Product__c from opportunity where AccountId =: acc.Id LIMIT 1]; 
        system.debug('%%%Opportunity%%%' +opp);
        Product__c prod = [select Id,Name,Long_Term_Facility__c,Product_Tier__c,Is_Active__c,Term_mo__c,State_Code__c,Internal_Use_Only__c,APR__c,Installer_Account__c from Product__c where Name =: 'testprod' LIMIT 1];
        Product__c prod1 = [select Id,Name,Long_Term_Facility__c,Product_Tier__c,Is_Active__c,Term_mo__c,State_Code__c,Internal_Use_Only__c,APR__c,Installer_Account__c from Product__c where Name =: 'testprod1' LIMIT 1];
        
        system.debug('$$$prod list$$$' +prod);
        system.debug('$$$prod list1$$$' +prod1);
        
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1];
        
        System.runAs(loggedInUsr[0])
        {
            Test.starttest();
            Prequal__c preq = new Prequal__c();
            preq.Opportunity__c = opp.Id;
            preq.Installer_Account__c = acc.id;
            preq.Pre_Qual_Status__c ='Auto Approved';
            insert preq;
            
            Map<String, String> RBPFNIFacilityDecisionMap = new Map<String, String>();
            
            String RBPFNIFacilityDecision ;            
            
            RBPFNIFacilityDecision ='{"prequal":{"id":"'+preq.id+'"},"projects":[{"producType":"Solar","credits":[{"id":"'+cred.id+'","fniSoftResp":" <?xmlversion=\\"1.0\\"encoding=\\"UTF-8\\"standalone=\\"no\\"?> <soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"> <soap:Header/> <soap:Body> <ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"> <ns3:StatusCode>SUCCESS</ns3:StatusCode> <ns3:StatusMessage/> <ns3:FNIReferenceNumber>20000007248832</ns3:FNIReferenceNumber> <ns3:Decision>A</ns3:Decision> <ns3:DecisionDateTime>2018-09-07T13:55:24-05:00</ns3:DecisionDateTime> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:LastName>DONALD</ns3:LastName> <ns3:TransactionID>6BF60E49-35E5-4103-B157-1A351FC12777</ns3:TransactionID> <ns3:LenderID>SUN</ns3:LenderID> <ns3:Strategy> <ns3:StipRsn1/> <ns3:StipRsn2/> <ns3:StipRsn3/> <ns3:StipRsn4/> <ns3:LineAssignmentAmt>10000000</ns3:LineAssignmentAmt> <ns3:DTI>1.7</ns3:DTI> <ns3:LowFico>793</ns3:LowFico> <ns3:DebtForMax>485</ns3:DebtForMax> </ns3:Strategy> <ns3:PrequalDecisions> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Decision>D</ns3:Decision> <ns3:Line>7000000</ns3:Line> <ns3:StipRsn/> <ns3:MaxPayment>24514</ns3:MaxPayment> <ns3:MaxDTI>750</ns3:MaxDTI> </ns3:Lender> </ns3:PrequalDecisions> <ns3:ApplicationDate>2018-09-07T13:55:24-05:00</ns3:ApplicationDate> <ns3:Applicant> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:MiddleInitial/> <ns3:LastName>DONALD</ns3:LastName> <ns3:SSN/> <ns3:DOB/> <ns3:FICO>0793</ns3:FICO> <ns3:FICODSC1>LENGTHOFTIMESINCEMOSTRECENTACCOUNTESTABLISHED</ns3:FICODSC1> <ns3:FICODSC2>PROPORTIONOFBALANCETOHIGHCREDITONBANKREVOLVINGORALLREVOLVINGACCOUNTS</ns3:FICODSC2> <ns3:FICODSC3>CURRENTBALANCESONREVOLVINGACCOUNTS</ns3:FICODSC3> <ns3:FICODSC4>LENGTHOFREVOLVINGACCOUNTHISTORY</ns3:FICODSC4> </ns3:Applicant> <ns3:RBP> <ns3:Lenders> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Tiers> <ns3:Tier> <ns3:TierID>1</ns3:TierID> <ns3:StipRsn/> <ns3:MaxDTI>500</ns3:MaxDTI> <ns3:MaxPayment>-485</ns3:MaxPayment> <ns3:Decision>A</ns3:Decision> </ns3:Tier> </ns3:Tiers> </ns3:Lender> </ns3:Lenders> <ns3:GlobalDecision>A</ns3:GlobalDecision> </ns3:RBP> </ns3:RESPONSE> </soap:Body> </soap:Envelope>"}]}]}';
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            opp.Project_Category__c = 'Home';
            opp.SLF_Product__c = prod1.Id;
            update opp;
            
            system.debug('%%%opp^^^^' +opp);
            
            RBPFNIFacilityDecision =  '{ "prequal" : { "id" : "'+preq.Id+'" }, "projects" : [ { "producType" : "HIS", "credits" : [ { "id" : "'+cred.id+'", "fniSoftResp" : "" } ] } ] }'; 
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            system.debug('%%%RBI^^^' +RBPFNIFacilityDecision);
            
            RBPFNIFacilityDecision ='{"prequal":{"id":"'+preq.id+'"},"projects":[{"producType":"HIS","credits":[{"id":"'+cred.id+'","fniSoftResp":" <?xmlversion=\\"1.0\\"encoding=\\"UTF-8\\"standalone=\\"no\\"?> <soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"> <soap:Header/> <soap:Body> <ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"> <ns3:StatusCode>SUCCESS</ns3:StatusCode> <ns3:StatusMessage/> <ns3:FNIReferenceNumber>20000007248832</ns3:FNIReferenceNumber> <ns3:Decision>A</ns3:Decision> <ns3:DecisionDateTime>2018-09-07T13:55:24-05:00</ns3:DecisionDateTime> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:LastName>DONALD</ns3:LastName> <ns3:TransactionID>6BF60E49-35E5-4103-B157-1A351FC12777</ns3:TransactionID> <ns3:LenderID>SUN</ns3:LenderID> <ns3:Strategy> <ns3:StipRsn1/> <ns3:StipRsn2/> <ns3:StipRsn3/> <ns3:StipRsn4/> <ns3:LineAssignmentAmt>10000000</ns3:LineAssignmentAmt> <ns3:DTI>1.7</ns3:DTI> <ns3:LowFico>793</ns3:LowFico> <ns3:DebtForMax>485</ns3:DebtForMax> </ns3:Strategy> <ns3:PrequalDecisions> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Decision>D</ns3:Decision> <ns3:Line>7000000</ns3:Line> <ns3:StipRsn/> <ns3:MaxPayment>24514</ns3:MaxPayment> <ns3:MaxDTI>750</ns3:MaxDTI> </ns3:Lender> </ns3:PrequalDecisions> <ns3:ApplicationDate>2018-09-07T13:55:24-05:00</ns3:ApplicationDate> <ns3:Applicant> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:MiddleInitial/> <ns3:LastName>DONALD</ns3:LastName> <ns3:SSN/> <ns3:DOB/> <ns3:FICO>0793</ns3:FICO> <ns3:FICODSC1>LENGTHOFTIMESINCEMOSTRECENTACCOUNTESTABLISHED</ns3:FICODSC1> <ns3:FICODSC2>PROPORTIONOFBALANCETOHIGHCREDITONBANKREVOLVINGORALLREVOLVINGACCOUNTS</ns3:FICODSC2> <ns3:FICODSC3>CURRENTBALANCESONREVOLVINGACCOUNTS</ns3:FICODSC3> <ns3:FICODSC4>LENGTHOFREVOLVINGACCOUNTHISTORY</ns3:FICODSC4> </ns3:Applicant> <ns3:RBP> <ns3:Lenders> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Tiers> <ns3:Tier> <ns3:TierID>1</ns3:TierID> <ns3:StipRsn/> <ns3:MaxDTI>500</ns3:MaxDTI> <ns3:MaxPayment>-485</ns3:MaxPayment> <ns3:Decision>A</ns3:Decision> </ns3:Tier> </ns3:Tiers> </ns3:Lender> </ns3:Lenders> <ns3:GlobalDecision>A</ns3:GlobalDecision> </ns3:RBP> </ns3:RESPONSE> </soap:Body> </soap:Envelope>"}]}]}';
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            RBPFNIFacilityDecision ='{"prequal":{"id":"'+preq.id+'"},"projects":[{"producType":"HII","credits":[{"id":"'+cred.id+'","fniSoftResp":" <?xmlversion=\\"1.0\\"encoding=\\"UTF-8\\"standalone=\\"no\\"?> <soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"> <soap:Header/> <soap:Body> <ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"> <ns3:StatusCode>SUCCESS</ns3:StatusCode> <ns3:StatusMessage/> <ns3:FNIReferenceNumber>20000007248832</ns3:FNIReferenceNumber> <ns3:Decision>A</ns3:Decision> <ns3:DecisionDateTime>2018-09-07T13:55:24-05:00</ns3:DecisionDateTime> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:LastName>DONALD</ns3:LastName> <ns3:TransactionID>6BF60E49-35E5-4103-B157-1A351FC12777</ns3:TransactionID> <ns3:LenderID>SUN</ns3:LenderID> <ns3:Strategy> <ns3:StipRsn1/> <ns3:StipRsn2/> <ns3:StipRsn3/> <ns3:StipRsn4/> <ns3:LineAssignmentAmt>10000000</ns3:LineAssignmentAmt> <ns3:DTI>1.7</ns3:DTI> <ns3:LowFico>793</ns3:LowFico> <ns3:DebtForMax>485</ns3:DebtForMax> </ns3:Strategy> <ns3:PrequalDecisions> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Decision>D</ns3:Decision> <ns3:Line>7000000</ns3:Line> <ns3:StipRsn/> <ns3:MaxPayment>24514</ns3:MaxPayment> <ns3:MaxDTI>750</ns3:MaxDTI> </ns3:Lender> </ns3:PrequalDecisions> <ns3:ApplicationDate>2018-09-07T13:55:24-05:00</ns3:ApplicationDate> <ns3:Applicant> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:MiddleInitial/> <ns3:LastName>DONALD</ns3:LastName> <ns3:SSN/> <ns3:DOB/> <ns3:FICO>0793</ns3:FICO> <ns3:FICODSC1>LENGTHOFTIMESINCEMOSTRECENTACCOUNTESTABLISHED</ns3:FICODSC1> <ns3:FICODSC2>PROPORTIONOFBALANCETOHIGHCREDITONBANKREVOLVINGORALLREVOLVINGACCOUNTS</ns3:FICODSC2> <ns3:FICODSC3>CURRENTBALANCESONREVOLVINGACCOUNTS</ns3:FICODSC3> <ns3:FICODSC4>LENGTHOFREVOLVINGACCOUNTHISTORY</ns3:FICODSC4> </ns3:Applicant> <ns3:RBP> <ns3:Lenders> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Tiers> <ns3:Tier> <ns3:TierID>1</ns3:TierID> <ns3:StipRsn/> <ns3:MaxDTI>500</ns3:MaxDTI> <ns3:MaxPayment>-485</ns3:MaxPayment> <ns3:Decision>A</ns3:Decision> </ns3:Tier> </ns3:Tiers> </ns3:Lender> </ns3:Lenders> <ns3:GlobalDecision>A</ns3:GlobalDecision> </ns3:RBP> </ns3:RESPONSE> </soap:Body> </soap:Envelope>"}]}]}';
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            RBPFNIFacilityDecision ='{"prequal":{"id":"'+preq.id+'"},"projects":[{"producType":"Home","credits":[{"id":"'+cred.id+'","fniSoftResp":" <?xmlversion=\\"1.0\\"encoding=\\"UTF-8\\"standalone=\\"no\\"?> <soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"> <soap:Header/> <soap:Body> <ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"> <ns3:StatusCode>SUCCESS</ns3:StatusCode> <ns3:StatusMessage/> <ns3:FNIReferenceNumber>20000007248832</ns3:FNIReferenceNumber> <ns3:Decision>A</ns3:Decision> <ns3:DecisionDateTime>2018-09-07T13:55:24-05:00</ns3:DecisionDateTime> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:LastName>DONALD</ns3:LastName> <ns3:TransactionID>6BF60E49-35E5-4103-B157-1A351FC12777</ns3:TransactionID> <ns3:LenderID>SUN</ns3:LenderID> <ns3:Strategy> <ns3:StipRsn1/> <ns3:StipRsn2/> <ns3:StipRsn3/> <ns3:StipRsn4/> <ns3:LineAssignmentAmt>10000000</ns3:LineAssignmentAmt> <ns3:DTI>1.7</ns3:DTI> <ns3:LowFico>793</ns3:LowFico> <ns3:DebtForMax>485</ns3:DebtForMax> </ns3:Strategy> <ns3:PrequalDecisions> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Decision>D</ns3:Decision> <ns3:Line>7000000</ns3:Line> <ns3:StipRsn/> <ns3:MaxPayment>24514</ns3:MaxPayment> <ns3:MaxDTI>750</ns3:MaxDTI> </ns3:Lender> </ns3:PrequalDecisions> <ns3:ApplicationDate>2018-09-07T13:55:24-05:00</ns3:ApplicationDate> <ns3:Applicant> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:MiddleInitial/> <ns3:LastName>DONALD</ns3:LastName> <ns3:SSN/> <ns3:DOB/> <ns3:FICO>0793</ns3:FICO> <ns3:FICODSC1>LENGTHOFTIMESINCEMOSTRECENTACCOUNTESTABLISHED</ns3:FICODSC1> <ns3:FICODSC2>PROPORTIONOFBALANCETOHIGHCREDITONBANKREVOLVINGORALLREVOLVINGACCOUNTS</ns3:FICODSC2> <ns3:FICODSC3>CURRENTBALANCESONREVOLVINGACCOUNTS</ns3:FICODSC3> <ns3:FICODSC4>LENGTHOFREVOLVINGACCOUNTHISTORY</ns3:FICODSC4> </ns3:Applicant> <ns3:RBP> <ns3:Lenders> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Tiers> <ns3:Tier> <ns3:TierID>0</ns3:TierID> <ns3:StipRsn/> <ns3:MaxDTI>500</ns3:MaxDTI> <ns3:MaxPayment>-485</ns3:MaxPayment> <ns3:Decision>A</ns3:Decision> </ns3:Tier> </ns3:Tiers> </ns3:Lender> </ns3:Lenders> <ns3:GlobalDecision>A</ns3:GlobalDecision> </ns3:RBP> </ns3:RESPONSE> </soap:Body> </soap:Envelope>"}]}]}';
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            RBPFNIFacilityDecision =  'Exceptiondata'; 
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            Test.stoptest();
        }
    }
    
     
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}