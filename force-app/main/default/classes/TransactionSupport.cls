public class TransactionSupport 
{
//  ***************************************************

    private static Map<String,Integer> idsMap = new Map<String,Integer>{};  

//  ***************************************************

    public static Boolean isProcessed (String key, Integer times)        
    {
        Integer x = idsMap.get(key);

        if (x == 0) return false;  // locked

        if (x == null)  //  first time
        {
            idsMap.put (key,1); 
            return false; 
        }
        if (x < times)
        {
            idsMap.put (key,x + 1);
            return false; 
        }
        return true;
    }

//  ***************************************************

    public static Boolean isProcessed (String key)        
    {
    	return isProcessed (key, 0);
    }

//  ***************************************************

    public static Boolean isNotProcessed (String key)        
    {
    	return !(isProcessed (key, 0));
    }

//  ***************************************************

    //  for the current transaction only, blocks execution of a named key,
    //  such that blocked(key) returns true, and unblocked(key) returns false

    public static void block (String key)

    {
        idsMap.put (key,0);
        System.debug('*** TransactionSupport *** calls to ' + key + ' have been blocked');
    }

//  ***************************************************

    //  for the current transaction only, unblocks execution of a named key,
    //  such that blocked(key) returns false, and unblocked(key) returns true

    public static void unblock (String key)
    {
        idsMap.put (key,1);
        System.debug('*** TransactionSupport *** calls to ' + key + ' have been unblocked');
    }

//  ***************************************************

    //  if execution of a named key has been blocked by block(key), returns true

    public static Boolean isBlocked (String key)
    {
        Integer x = idsMap.get(key);

        if (x == 0) 
        {
            System.debug('*** TransactionSupport *** calls to ' + key + ' are blocked');
            return true;
        }
        System.debug('*** TransactionSupport *** calls to ' + key + ' are not blocked');
        return false;
    }

//  ***************************************************

    //  if execution of a named key has not been blocked by block(key), or has been unblocked by unblock(key) returns true

    public static Boolean isUnblocked (String key)
    {
        Integer x = idsMap.get(key);

        if (x != 0) 
        {
            System.debug('*** TransactionSupport *** calls to ' + key + ' are not blocked');
            return true;
        }
        System.debug('*** TransactionSupport *** calls to ' + key + ' are blocked');
        return false;
    }   

//  ***************************************************

}