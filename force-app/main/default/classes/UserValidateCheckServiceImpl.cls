/**
* Description: Applicant API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Suresh Kumar           06/12/2017           Created
******************************************************************************************/
public without sharing class UserValidateCheckServiceImpl extends RESTServiceBase {
    private String msg = '';
    
    /**
* Description:    
*          
*
* @param reqAP      Get all POST request action parameters
* @param reqData    Get all POST request data
*
* return res        Send response back with data and response status code
*/
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('Entered into fetchData() of ' + UserValidateCheckServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        string errMsg = '';
        UnifiedBean respBean = new UnifiedBean();
        
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            
            if(null == reqData.users || String.isBlank(reqData.users[0].hashId)){
                errMsg = ErrorLogUtility.getErrorCodes('398',null);
                gerErrorMsg(respBean,errMsg,'398');
            }else
            {
                try{
                    List<User> lstUser = [SELECT id,IsActive,Hash_Id__c,contactId,contact.AccountId,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c 
                                          FROM user WHERE Hash_Id__c=:reqData.users[0].hashId and IsActive=true];
                    if(lstUser!=null && lstUser.size()>0)
                    {
                        User objUser = lstUser.get(0);
                        if(objUser.contact.Reset_Password_Flag__c == true){
                            respBean.users = new List<UnifiedBean.UsersWrapper>();
                            
                            // check for DualStepverification
                            if(objUser.Ask_Questions__c==false){
                                if(reqData.users[0].selectedQuestionsAndAnswers!=null){
                                    validateQuetionsAndAnswer(respBean,objUser,reqData);
                                }else{
                                    getSelectedQuestions(respBean,objUser,false,reqData);
                                }  
                            }else{
                                // skip dual step verification
                                respBean.returnCode = '200';
                                respBean.message = ErrorLogUtility.getErrorCodes('200', null);
                            }
                            
                        }else{
                            errMsg = ErrorLogUtility.getErrorCodes('360',null);
                            gerErrorMsg(respBean,errMsg,'360');
                        }
                    }else{
                        errMsg = ErrorLogUtility.getErrorCodes('295',null);
                        gerErrorMsg(respBean,errMsg,'295');
                    }
                    iRestRes = (IRestResponse)respBean;
                    res.response= iRestRes;
                }catch(Exception exc){
                    System.debug('### Error occured: '+exc.getMessage());
                    errMsg = ErrorLogUtility.getErrorCodes('400',null);
                     ErrorLogUtility.writeLog('UserValidateCheckServiceImpl.fetchData()',exc.getLineNumber(),'fetchData()',exc.getMessage() + '###' +exc.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(exc,null,null,null,null));   
                    gerErrorMsg(respBean,errMsg,'400');
                    iRestRes = (IRestResponse)respBean;
                    res.response= iRestRes;
                }
                
            }
        }catch(Exception err){
            
            system.debug('### UserValidateCheckServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('UserValidateCheckServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            errMsg= ErrorLogUtility.getErrorCodes('214',null);
            gerErrorMsg(respBean,errMsg,'214');
            iRestRes = (IRestResponse)respBean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + UserValidateCheckServiceImpl.class);
        return res.response ;
    }
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        List<UnifiedBean.ErrorWrapper> errorList = new List<UnifiedBean.ErrorWrapper>();
        UnifiedBean.ErrorWrapper errorWraper = new UnifiedBean.ErrorWrapper();
        errorWraper.errorMessage = sErrorMsg;
        errorList.add(errorWraper);
        objUnifiedBean.error = errorList;
        objUnifiedBean.returnCode = sReturnCode;
    }
    /**
    * method
    **/
    public void getSelectedQuestions(UnifiedBean respBean,User objUser,Boolean isUpdate,UnifiedBean reqData){
        Boolean updateUser=false;
        UnifiedBean.UsersWrapper objUserWrp = new UnifiedBean.UsersWrapper();
        objUserWrp.selectedQuestions = new List<UnifiedBean.SelectedQuestionsWrapper>();
        UnifiedBean.SelectedQuestionsWrapper selctedQuestionWrp = new UnifiedBean.SelectedQuestionsWrapper();
        // validate user with two step verification
        boolean userFreezed = false;
        respBean.returnCode = '200';
        if(objUser.Question_Counter__c == 0   ){
            updateUser=isUpdate;
            objUserWrp.questionNumber = 1;
            selctedQuestionWrp.question=objUser.Question1__c;
            objUser.Question_Counter__c=+1;
        }else if(objUser.Question_Counter__c == 1){
            updateUser=isUpdate;
            objUserWrp.questionNumber = 2;
            selctedQuestionWrp.question=objUser.Question2__c;
            objUser.Question_Counter__c=objUser.Question_Counter__c+1;
        }else if(objUser.Question_Counter__c == 2){
             updateUser=isUpdate;
            objUserWrp.questionNumber = 3;
            selctedQuestionWrp.question=objUser.Question3__c;
            objUser.Question_Counter__c=objUser.Question_Counter__c+1;
        }else if(objUser.Question_Counter__c >= 3){
            userFreezed = true;
            freezeUser(respBean,objUser);
        }
        // Added this part of 5256
        if(userFreezed){
            
        }else{
            objUserWrp.selectedQuestions.add(selctedQuestionWrp);
            respBean.users.add(objUserWrp);
            system.debug('### respBean==>:'+respBean);
            if(updateUser)
            update objUser;
        }
       
        
        
    }
     /**
    * method
    **/
    public void validateQuetionsAndAnswer(UnifiedBean respBean,User objUser,UnifiedBean reqData){
        
        system.debug('### validateQuetionsAndAnswer==>');
        if(String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[0].question) && String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[0].answer)){
            
            if(reqData.users[0].questionNumber==1 ){
                 system.debug('### questionNumber==>'+reqData.users[0].questionNumber);
                if(reqData.users[0].selectedQuestionsAndAnswers[0].answer.toUpperCase() == objUser.Answer1__c.toUpperCase()){
                    system.debug('### Question==>'+reqData.users[0].selectedQuestionsAndAnswers[0].question);
                    respBean.returnCode = '200';
                    respBean.message = ErrorLogUtility.getErrorCodes('200', null);
                    objUser.Question_Counter__c=0;
                    update objUser; 
                }else{
                    objUser.Question_Counter__c = 0; // Added this part of 5256
                    getSelectedQuestions(respBean,objUser,true,reqData);
                }
            }else if(reqData.users[0].questionNumber==2 ){
                if(reqData.users[0].selectedQuestionsAndAnswers[0].answer.toUpperCase() == objUser.Answer2__c.toUpperCase()){
                    respBean.returnCode = '200';
                    respBean.message = ErrorLogUtility.getErrorCodes('200', null);
                    //Added by Udaya Kiran ORANGE-10751
                    //objUser.Question_Counter__c=null;
                    objUser.Question_Counter__c=0;
                    //Added by Udaya Kiran ORANGE-10751
                    update objUser; 
                }else{
                    getSelectedQuestions(respBean,objUser,true,reqData);
                }
            }else if(reqData.users[0].questionNumber==3 ){
                if(reqData.users[0].selectedQuestionsAndAnswers[0].answer.toUpperCase() == objUser.Answer3__c.toUpperCase()){
                    respBean.returnCode = '200';
                    //Added by Udaya Kiran ORANGE-10751
                    objUser.Question_Counter__c=0;
                    update objUser;
                    //Added by Udaya Kiran ORANGE-10751
                    respBean.message = ErrorLogUtility.getErrorCodes('200', null);
                }else{
                    freezeUser(respBean,objUser);
                }
            }
        }
        
    }
     /**
    * method
    **/
    public void freezeUser(UnifiedBean respBean,User objUser){
            // locak user
            List<UserLogin> lstUserLogin =[SELECT id, userid, isfrozen  FROM UserLogin WHERE userid=:objUser.Id LIMIT 1];
            if(!lstUserLogin.isEmpty()){
                lstUserLogin[0].isfrozen=true;
                update lstUserLogin[0];
                respBean.returnCode = '502';
                respBean.message = 'User Account is Locked!';
                // Added this part of 5256
                for(User objU:[select firstname,lastname, email from user where profile.Name='Partner Community Delegate' and contact.AccountId =:objUser.contact.AccountId]){
                  UnifiedBean.UsersWrapper objUserWrp1 = new UnifiedBean.UsersWrapper();
                  objUserWrp1.firstName = objU.FirstName;
                  objUserWrp1.lastName = objU.LastName;
                  objUserWrp1.email = objU.email;
                  respBean.users.add(objUserWrp1);
                }
            }
    }
}