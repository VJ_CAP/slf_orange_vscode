/**
* Description: This class is to encrypt/decrypt any given data. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Raviteja Sajja         12/06/2017           Created
******************************************************************************************/
public class SLFEncryption {
    
    public static final String ALGORITHM =  'AES256';
    public static final String ALGORITHM_NEW =  'AES128';
    
    /**
    * Description:  This method is used to encrypt the data.
    */
    public static String encryptData(String dataToEncrypt)
    {
        system.debug('### Entered into EncryptData() of '+ SLFEncryption.class);
        String encryptedValue = '';
        try{
            if(dataToEncrypt!=null && String.isNotBlank(dataToEncrypt)){
                String privateKey = SFSettings__c.getOrgDefaults().AES_Encryption_Key__c;
                if(privateKey!=null && String.isNotBlank(privateKey)){
                    Blob data = crypto.encryptWithManagedIV(ALGORITHM, Blob.valueOf(privateKey), Blob.valueOf(dataToEncrypt));
                    encryptedValue = EncodingUtil.base64Encode(data);
                }
            }
        }catch(Exception ex)
        {
            system.debug('### SLFEncryption.EncryptData():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SLFEncryption.EncryptData()',ex.getLineNumber(),'EncryptData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from EncryptData() of '+ SLFEncryption.class);
        return encryptedValue;
    }
    
    /**
    * Description:  This method is used to encrypt the data for Funding Package Service.
    */
    //Added by Sejal as a part of Orange-4664
    public static String encryptDataAES128(String dataToEncrypt)
    {
        system.debug('### Entered into EncryptData() of '+ SLFEncryption.class);
        String encryptedValue = '';
        try{
            if(dataToEncrypt!=null && String.isNotBlank(dataToEncrypt)){
                String privateKey = SFSettings__c.getOrgDefaults().FundingPacketAES128Key__c;
                if(privateKey!=null && String.isNotBlank(privateKey)){
                    Blob data = crypto.encryptWithManagedIV(ALGORITHM_NEW, Blob.valueOf(privateKey), Blob.valueOf(dataToEncrypt));
                    encryptedValue = EncodingUtil.base64Encode(data);
                }
            }
        }catch(Exception ex)
        {
            system.debug('### SLFEncryption.EncryptData():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SLFEncryption.EncryptData()',ex.getLineNumber(),'EncryptData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from EncryptData() of '+ SLFEncryption.class);
        return encryptedValue;
    }
    //End: Orange-4664
    
    /**
    * Description:  This method is used to decrypt the data.
    */
    public static String decryptData(String dataToDecrypt)
    {
        system.debug('### Entered into DecryptData() of '+ SLFEncryption.class);
        String decryptedValue = '';
        try{
            Blob encryptedData = EncodingUtil.base64Decode(dataToDecrypt);
            if(encryptedData!=null){
                String privateKey = SFSettings__c.getOrgDefaults().AES_Encryption_Key__c;
                if(privateKey!=null && String.isNotBlank(privateKey)){
                    Blob data = crypto.decryptWithManagedIV(ALGORITHM, Blob.valueOf(privateKey), encryptedData);
                    decryptedValue = data.toString();
                }
            }
        }catch(Exception ex)
        {
            system.debug('### SLFEncryption.DecryptData():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SLFEncryption.DecryptData()',ex.getLineNumber(),'DecryptData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from DecryptData() of '+ SLFEncryption.class);
        return decryptedValue;
    }
}