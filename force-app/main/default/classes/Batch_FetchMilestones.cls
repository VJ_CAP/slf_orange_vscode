global class Batch_FetchMilestones implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {

    Boolean errorEncountered;
    /**
     * This Batch fetches milestone ids from sighten
     * for all Quotes for all Opportunities
     * where Upload_Comment != null
     * 
     **/
    public Batch_FetchMilestones()
    {
        errorEncountered = false;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {

        // *** return all Quotes belonging to Opportunities where Upload_Comment__c == true
        String query = 'Select Id, Sighten_Quote_Id__c, OpportunityId from quote where OpportunityId in (SELECT Id FROM Opportunity where Upload_Comment__c = true and  EDW_Originated__c = true)';
                system.debug('Query String - '+query);          
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext sc)
    {
        Batch_FetchMilestones bf = new Batch_FetchMilestones();
        Database.executeBatch(bf,10);
    }
    
    global void execute(Database.BatchableContext BC,List<Quote> ql) 
    {
        try {   
            List<Opportunity> updateOppList = new List<Opportunity>();
            List<Quote> upsertQuoteList = new List<Quote>();
                        system.debug('list of quotes - '+ql);   
            for (quote q : ql) {
                if (sightenutil.setMilestoneIdOnQuote(q)) {
                        q.Milestone_UUID_Fetch_Failed__c = true;
                } else {
                        q.Milestone_UUID_Fetch_Failed__c = false;
                }
            }
           
            update ql;
        } catch (Exception e) {
            errorEncountered = true;
            throw new CustException(e);
        }
        
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        
        if (!Test.IsRunningTest()) {
            if (settings.CHAIN_BATCHES__c && !errorEncountered) {
                 Database.executeBatch(new Batch_UpdateMilestones(), 10);
            }
        }
    }
}