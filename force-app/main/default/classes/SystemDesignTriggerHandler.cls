/**
* Description: Trigger Business logic associated with System_Design__c is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         09/06/2017          Created 
******************************************************************************************/
public with sharing Class SystemDesignTriggerHandler
{
    public static boolean isTrgExecuting = true;
    
    public SystemDesignTriggerHandler(){
    }
    
    /**
    * Constructor to initialize
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public SystemDesignTriggerHandler(boolean isExecuting){
       // isTrgExecuting = isExecuting;
    }
    
    public static boolean runOnce(){
        if(isTrgExecuting){
            isTrgExecuting=false;
            
            return true;
        }else{
            return isTrgExecuting;
        }
    }
    /**
    * Description: On After Insert
    *
    * @param new System_Design__c records.
    */
    public  void OnAfterInsert(System_Design__c[] newSystemDesign){
        try
        {
            system.debug('### Entered into OnAfterInsert() of '+ SystemDesignTriggerHandler.class);
            set<Id> oppIds = new set<Id>();
            List<System_Design__c> newSystemNonArchivedLst = new List<System_Design__c>();
            for(System_Design__c sysDsnIterate : newSystemDesign){
                if(sysDsnIterate.Opportunity__c != null  && sysDsnIterate.Push_Endpoint__c && !sysDsnIterate.is_Non_Arch_EDW__c)
                    oppIds.add(sysDsnIterate.Opportunity__c);
                if(!sysDsnIterate.is_Non_Arch_EDW__c){
                    newSystemNonArchivedLst.add(sysDsnIterate);
                }
            }
            if(!oppIds.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
            {
                //Generate JSON Structure and create Status Audit Record
                String jsonNewList = JSON.serialize(newSystemNonArchivedLst);
                SLFUtility.callUnifiedJSON('System_Design__c','systemDesigns',oppIds,jsonNewList,'');
                
                //SLFUtility.buildUnifiedJSON('System_Design__c','systemDesigns',oppIds,newSystemNonArchivedLst,null);
            }
            syncSystemdesignToOpportunity(newSystemNonArchivedLst);
            //isTrgExecuting = false;
            
            system.debug('### Exit from OnAfterInsert() of '+ SystemDesignTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### SystemDesignTriggerHandler.OnAfterInsert()'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SystemDesignTriggerHandler.OnAfterInsert()',ex.getLineNumber(),'OnAfterInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    /**
    * Description: On After Update
    *
    * @param new System_Design__c records.
    */
    public void OnAfterUpdate(System_Design__c[] newSystemDesign,Map<Id,System_Design__c> oldSystemDesignMap){
        try
        {
            system.debug('### Entered into OnAfterUpdate() of '+ SystemDesignTriggerHandler.class);
            if(isTrgExecuting){
                runOnce();
                //Ravi: To exclude change order archived Opportunities
                List<System_Design__c> systemDesignList = new List<System_Design__c>();
                Map<Id,List<System_Design__c>> systemDesignMap = new Map<Id,List<System_Design__c>>();
                
               
                List<System_Design__c> newSystemNonArchivedLst = new List<System_Design__c>(); 
               
                set<Id> oppIds = new set<Id>();
                for(System_Design__c sysDsnIterate : newSystemDesign){
                    if(sysDsnIterate.Opportunity__c != null  && sysDsnIterate.Push_Endpoint__c)  
                        oppIds.add(sysDsnIterate.Opportunity__c);
                    if(!sysDsnIterate.is_Non_Arch_EDW__c){
                        newSystemNonArchivedLst.add(sysDsnIterate);
                    }
                }
                
                if(!oppIds.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
                {
                    //Generate JSON Structure and create Status Audit Record
                    String jsonNewList = JSON.serialize(newSystemNonArchivedLst);
                    String jsonOldMap = JSON.serialize(oldSystemDesignMap);
                    SLFUtility.callUnifiedJSON('System_Design__c','systemDesigns',oppIds,jsonNewList,jsonOldMap);
                    
                    //SLFUtility.buildUnifiedJSON('System_Design__c','systemDesigns',oppIds,newSystemNonArchivedLst,oldSystemDesignMap);
                }
                syncSystemdesignToOpportunity(newSystemNonArchivedLst);
                
                isTrgExecuting = false;
            }    
            system.debug('### Exit from OnAfterUpdate() of '+ SystemDesignTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### SystemDesignTriggerHandler.OnAfterUpdate()'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SystemDesignTriggerHandler.OnAfterUpdate()',ex.getLineNumber(),'OnAfterUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    
    /**
    * Description: To Sync System Design Fields to Opportunity
    * 
    * @param new System Design records.
    */
    
    public  void syncSystemdesignToOpportunity(System_Design__c[] newSystemDesign){
        try{
            system.debug('### Entered into syncSystemdesignToOpportunity() of '+ SystemDesignTriggerHandler.class);
            //fetching all records from SystemDesign_to_Opportunity_Sync custom setting
            List<SystemDesign_to_Opportunity_Sync__c> syncRecs = SystemDesign_to_Opportunity_Sync__c.getall().values();
            
            //Adding all SystemDesign_to_Opportunity_Sync records to oppSyncingMap Map
            Map<string,string>  oppSyncingMap = new  Map<string,string>();  
            Set<id> oppIds = new Set<id>();
            for(SystemDesign_to_Opportunity_Sync__c syncIterate : syncRecs)    
            {
                oppSyncingMap.put(syncIterate.From_System_Design__c,syncIterate.To_Opportunity__c);
            }   
            for(System_Design__c sysIterate : newSystemDesign){
                if(sysIterate.Opportunity__c != null){
                    oppIds.add(sysIterate.Opportunity__c);
                }
            }
            Map<Id,set<Id>> quoteOppMap = new Map<Id,set<Id>>();
            
            if(!oppIds.isEmpty())
            {
                List<Quote> syncQuotes = [Select Id, OpportunityId, opportunity.StageName, Combined_Loan_Amount__c, System_Design__c, SLF_Product__r.APR__c, SLF_Product__r.Term_mo__c From Quote Where Opportunityid IN: oppIds and IsSyncing =: true];
                system.debug('### syncQuotes '+syncQuotes);
                if(!syncQuotes.isEmpty())
                {
                    for(Quote quoteIterate : syncQuotes){
                        if(quoteOppMap.containsKey(quoteIterate.System_Design__c))
                        {
                            quoteOppMap.get(quoteIterate.System_Design__c).add(quoteIterate.OpportunityId);
                        }else{
                            quoteOppMap.put(quoteIterate.System_Design__c,new set<Id>{quoteIterate.OpportunityId});
                        }
                    }
                }
            }
            Map<Id,sObject> oppUpdateMap = new Map<Id,sObject>();
            for(System_Design__c sysIterate :  newSystemDesign){
                if(quoteOppMap.containsKey(sysIterate.Id))
                {
                    set<Id> oppIdsSet = quoteOppMap.get(sysIterate.Id);
                    system.debug('### oppIdsSet '+oppIdsSet);
                    for(Id oppId : oppIdsSet)
                    {
                        for(string strIterate : oppSyncingMap.keyset()) //Application_ID_c
                        {
                            sObject sObjOpp = Schema.getGlobalDescribe().get('Opportunity').newSObject() ;
                            
                            if(oppUpdateMap.containskey(sysIterate.Opportunity__c))
                            {
                                sObjOpp = oppUpdateMap.get(sysIterate.Opportunity__c);
                            }
                            string oppFld = oppSyncingMap.get(strIterate);
                            sObjOpp.put('Id' ,(String)oppId); 
                            sObjOpp.put(oppFld ,(object)sysIterate.get(strIterate)); 
                            oppUpdateMap.put(sysIterate.Opportunity__c,sObjOpp);
                        }
                    }
                }
            }
            
            System.debug('oppUpdateMap.Values()'+oppUpdateMap.Values());
            if(!oppUpdateMap.isEmpty())
                update oppUpdateMap.Values();
            
            system.debug('### Exit from syncSystemdesignToOpportunity() of '+ SystemDesignTriggerHandler.class);
        }catch(Exception err){
           
            system.debug('### SystemDesignTriggerHandler.syncSystemdesignToOpportunity():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('SystemDesignTriggerHandler.syncSystemdesignToOpportunity()',err.getLineNumber(),'syncSystemdesignToOpportunity()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            
        }
    
    }
    
    public void SystemDesignOwnerChangeBasedOnOpportunityOwner(System_Design__c[] newSysDesignlst){
        Set<ID> Oppids;
        system.debug('***newSysDesignlst---'+newSysDesignlst);
        if(newSysDesignlst!= null && newSysDesignlst.size()>0){//Thisi is Quote insert
                Oppids = new set<ID>();
            for(System_Design__c SysDesign : newSysDesignlst){
               if(SysDesign.Opportunity__c != null)
                    Oppids.add(SysDesign.Opportunity__c); 
            }
            
        }
        Map<string,Opportunity> idAndOppMap = new Map<string,Opportunity>();
        if(Oppids != null && Oppids.size()>0){
          for(Opportunity opps: [Select id,Name,Ownerid from Opportunity where id IN: Oppids]){
              idAndOppMap.put(opps.id,opps);
          }
        }
        for(integer i=0;i<newSysDesignlst.size();i++){
            if(idAndOppMap.get(newSysDesignlst[i].Opportunity__c) != null){
                newSysDesignlst[i].ownerid = idAndOppMap.get(newSysDesignlst[i].Opportunity__c).ownerid;
            }
        }
    }
}