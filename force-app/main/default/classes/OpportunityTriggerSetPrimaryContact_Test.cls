@istest
public class OpportunityTriggerSetPrimaryContact_Test 
{
    public static testmethod void test () 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings(); 
        
        Account a1 = new Account (Name = 'Test Account 1', FNI_Domain_Code__c = 'VisionSolar',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        RecordType rt1 = [SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount' LIMIT 1];

        Account a2 = new Account (LastName = 'Test Person Account 2', RecordTypeId = rt1.Id,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Account a3 = new Account (LastName = 'Test Person Account 3', RecordTypeId = rt1.Id,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a3;

        System.assertEquals(true, [SELECT IsPersonAccount FROM Account WHERE Id = :a2.Id].IsPersonAccount);

        Contact c1 = new Contact (LastName = 'Test', AccountId = a1.Id);
        insert c1;

        Opportunity o1 = new Opportunity (AccountId = a2.Id, Co_Applicant__c = a3.Id,
                                            Name = 'Test Opp 1', StageName = 'Test', CloseDate = System.today());
        insert o1;

        o1 = [SELECT Id, Primary_Contact__c, Co_Applicant__c FROM Opportunity WHERE Id = :o1.Id LIMIT 1];
        a2 = [SELECT Id, PersonContact.Id FROM Account WHERE Id = :a2.Id LIMIT 1];
        a3 = [SELECT Id, PersonContact.Id FROM Account WHERE Id = :a3.Id LIMIT 1];

        System.assertEquals(a2.PersonContact.Id, o1.Primary_Contact__c);
        System.assertEquals(a3.Id, o1.Co_Applicant__c);
    }
}