/******************************************************************************************
* Description: Test class to cover ApprovemilestoneM0DataServiceImpl class
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Kalyani         11/25/2019          Created
******************************************************************************************/
@isTest
public class ApprovemilestoneM0DataServiceImplTest {    
    
    @testsetup static void createtestdata(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgProdflag = new TriggerFlags__c();
        trgProdflag.Name ='Product__c';
        trgProdflag.isActive__c =true;
        trgLst.add(trgProdflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgAccflagDis = new TriggerFlags__c();
        trgAccflagDis.Name ='Disclosures__c';
        trgAccflagDis.isActive__c =true;
        trgLst.add(trgAccflagDis);
        
        insert trgLst;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', 
        ProfileId = p.Id, 
        Default_User__c = true,                            
        //UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testc@mail.com',ContactId = con.Id);
        
        insert portalUser;
        System.runAs (portalUser)
        { 
            //Insert Account record
            Account personAcc = new Account();
            personAcc.Name = 'SLFTest';
            personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.BillingStateCode = 'CA';
            personAcc.Installer_Legal_Name__c='Bright planet Solar';
            personAcc.Solar_Enabled__c = true;
            //personAcc.OwnerId = portalUser.Id;
            insert personAcc;
            
            //Insert SLF Product Record
                Product__c prod = new Product__c();
                prod.Installer_Account__c =acc.id;
                prod.Long_Term_Facility_Lookup__c =acc.id;
                prod.Name='testprod';
                prod.FNI_Min_Response_Code__c=9;
                prod.Product_Display_Name__c='test';
                prod.Qualification_Message__c ='testmsg';
                prod.Product_Loan_Type__c='Solar';
                prod.State_Code__c ='CA';
                prod.ST_APR__c = 2.99;
                prod.Long_Term_Facility__c = 'TCU';
                prod.APR__c = 2.99;
                prod.ACH__c = true;
                prod.Internal_Use_Only__c = false;
                // prod.Product_Tier__c ='1';
                //prod.Long_Term_Facility__c='TCU';
                prod.LT_Max_Loan_Amount__c =1000;
                prod.Term_mo__c = 120;
                // prod.Is_Active__c= true;
                prod.Product_Tier__c = '0';
                prod.NonACH_APR__c = 3.99;
                prod.Term_mo__c = 180;
                insert prod;
            
             //Insert opportunity record
                List<Opportunity> oppList = new List<Opportunity>();
                Opportunity personOpp = new Opportunity();
                personOpp.name = 'personOpp';
                personOpp.CloseDate = system.today();
                personOpp.StageName = 'Qualified';
                personOpp.AccountId = personAcc.id;
                personOpp.Installer_Account__c = acc.id;
                personOpp.Install_State_Code__c = 'CA';
                personOpp.Install_Street__c ='Street';
                personOpp.Install_City__c ='InCity';
                personOpp.Combined_Loan_Amount__c =10000;
                personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
                //opp.Approved_LT_Facility__c ='TCU';
                personOpp.SLF_Product__c = prod.id;            
                insert personOpp;
                
                 //Insert opportunity record
                Opportunity opp = new Opportunity();
                opp.Name = 'OppName';
                opp.CloseDate = system.today();
                opp.StageName = 'Qualified';
                opp.AccountId = personAcc.id;
                opp.Installer_Account__c = acc.id;
                opp.Install_State_Code__c = 'CA';
                opp.Install_Street__c ='Street';
                opp.Install_City__c ='InCity';
                opp.Combined_Loan_Amount__c = 10000;
                opp.Hash_Id__c = '1234';
                opp.Partner_Foreign_Key__c = 'TCU||123';
                //opp.Approved_LT_Facility__c = 'TCU';
                opp.SLF_Product__c = prod.id;
                opp.Opportunity__c = personOpp.Id; 
                opp.Block_Draws__c = false;
                opp.Has_Open_Stip__c = 'False';
                opp.All_HI_Required_Documents__c=true;
                opp.Max_Draw_Count__c=3;
                opp.isACH__c = true;
                insert opp;
                
                        //Insert System Design Record
                System_Design__c sysDesignRec = new System_Design__c();
                //sysDesignRec.Name = 'Test Sys';
                sysDesignRec.System_Cost__c = 11;
                sysDesignRec.Opportunity__c = opp.Id;
                sysDesignRec.Est_Annual_Production_kWh__c = 10;
                sysDesignRec.Inverter_Count__c = 11;
                //  sysDesignRec.Inverter_Make__c = '11';
                sysDesignRec.Inverter_Model__c = '12';
                //sysDesignRec.Inverter__c = 'TBD';
                sysDesignRec.Module_Count__c = 10;
                sysDesignRec.Module_Model__c = '11';
                sysDesignRec.System__c = 10;
                //sysDesignRec.System_Size_STC_kW__c = 12;
                insert sysDesignRec;
            
                Quote quoteRec = new Quote();
                quoteRec.Name ='Test quote';
                quoteRec.Opportunityid = opp.Id;
                quoteRec.SLF_Product__c = prod.Id ;
                //quoteRec.System_Design__c= sysDesignRec.id;
                //quoteRec.Accountid =acc.id;
                insert quoteRec; 
            System.debug('quoteRec***********'+quoteRec);
            
               opp.SyncedQuoteId = quoteRec.Id;
               opp.Hash_Id__c = '1234';
               update opp;
               
                SLF_Credit__c credit = new SLF_Credit__c();
                credit.Opportunity__c = opp.id;
                credit.Primary_Applicant__c = acc.id;
                credit.Co_Applicant__c = acc.id;
                credit.SLF_Product__c = prod.Id ;
                credit.Total_Loan_Amount__c = 100;
                credit.Status__c = 'New';
                credit.LT_FNI_Reference_Number__c = '123';
                credit.Application_ID__c = '123';
                insert credit;
                
                Underwriting_File__c undStpbt = new Underwriting_File__c();           
                undStpbt.Opportunity__c = opp.id;
                undStpbt.Approved_for_Payments__c = true;
                insert undStpbt;
                
                List<Equipment_Manufacturer__c> eqList = new List<Equipment_Manufacturer__c>();
                Equipment_Manufacturer__c equipRec1 = new Equipment_Manufacturer__c();
                equipRec1.End_Date__c = system.today()-1;
                equipRec1.Activation_Date__c =system.today()-6;
                equipRec1.Manufacturer_Type__c ='Mounting';
                equipRec1.Manufacturer__c ='Unirac';
                //insert equipRec1;
                eqList.add(equipRec1);
                
                Equipment_Manufacturer__c equipRec2 = new Equipment_Manufacturer__c();
                equipRec2.Activation_Date__c =system.today();
                equipRec2.End_Date__c =system.today()+6;
                equipRec2.Manufacturer_Type__c ='Mounting';
                equipRec2.Manufacturer__c ='Unirac';
                //insert equipRec2;
                eqList.add(equipRec2);
                
                insert eqList;
           }
    }
  

  /**
*
* Description: This method is used to cover ApprovemilestoneServiceImpl and ApprovemilestoneDataServiceImpl 
*
*/
    
    private testMethod static void approvemilestoneTest1(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account'LIMIT 1]; 
               System.debug('*****Account*******'+acc);

        //Account acc1 = [SELECT Id FROM Account where IsPersonAccount = true LIMIT 1];
        //System.debug('*****Account*******'+acc1);
        Opportunity opp = [select Id,Name,Hash_Id__c from opportunity where isACH__c = true LIMIT 1]; //where name = 'OppName' LIMIT 1
                System.debug('opp*****'+opp);

        Quote quoteRec = [Select id,Name from Quote where Opportunityid =: opp.id]; // LIMIT 1
        System.debug('quoteRec*****'+quoteRec);
        SLF_Credit__c credit =[Select id,IsSyncing__c from SLF_Credit__c where Opportunity__c =: opp.id Limit 1];
        
        List<Underwriting_File__c> underwritingLst  = [select Id,Opportunity__c,M0_Approval_Requested_Date__c, M1_Approval_Requested_Date__c, M2_Approval_Requested_Date__c,  (select id,Box_Installation_Photos_TS__c,Box_Install_Contracts_TS__c, Box_Loan_Agreements_TS__c, Box_Payment_Election_Form_TS__c, Box_Government_IDs_TS__c, Box_Utility_Bills_TS__c,Box_Final_Designs_TS__c,Box_Communications_TS__c,Box_Approval_Letters_TS__c from Box_Fields__r), 
                                                       Opportunity__r.Installer_Account__r.M0_Required__c, Opportunity__r.Installer_Account__r.M1_Required__c,Opportunity__r.Installer_Account__r.M2_Required__c from Underwriting_File__c where Opportunity__r.Id =: opp.id limit 1];
        
        List<Equipment_Manufacturer__c> equipRecLst =[select id,Is_Active__c from Equipment_Manufacturer__c LIMIT 2];
        Equipment_Manufacturer__c equipRec = new Equipment_Manufacturer__c();
        Equipment_Manufacturer__c equipRec1 = new Equipment_Manufacturer__c();
        for(Equipment_Manufacturer__c equipmentIterate : equipRecLst)
        {
            if(equipmentIterate.Is_Active__c)
                equipRec = equipmentIterate;
            else 
                equipRec1 = equipmentIterate;
        }
        System_Design__c sysDsiRec = [Select id,Name,Battery_Capacity__c,Battery_Count__c,ModuleMake__c,Project_Type__c,Ground_Mount_Type__c,
                                      BatteryMake__c,Battery_Model__c,Est_Annual_Production_kWh__c,
                                      Inverter_Count__c,InverterMake__c,Inverter_Model__c,Module_Count__c,
                                      Module_Model__c,Tilt__c,Opportunity__c,System__c
                                      from System_Design__c where Opportunity__c =: opp.id];
        List<SF_Category_Map__c> mapList = new List<SF_Category_Map__c>();
        
        SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                         Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                         TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
        mapList.add(cm1);
        
        
        quoteRec.System_Design__c= sysDsiRec.id;
        update quoteRec;
                    
        SF_Category_Map__c sfmap = new SF_Category_Map__c();
        sfmap.Name = 'Loan Agreement';
        sfmap.Folder_Name__c = 'Loan Agreements';
        sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
        sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
        sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_LT_Loan_Agreement_Date_Time__c';
        mapList.add(sfmap);
        insert mapList;
        
        Contact conObj = new Contact();
        conObj = [Select id from Contact limit 1];
        if(conObj != null){
            conObj.Email = 'test@gmail.com';
            update conObj;
        }
        acc.M0_Required__c = '';
        acc.M1_Required__c = '';
        acc.M2_Required__c = '';
        update acc;
        
        System.runAs(loggedInUsr[0])
        {
            String apprmilString;
            Map<String, String> apprmilMap = new Map<String, String>();
            Test.startTest();
            try{
                apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M0"}]}';
                MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M1"}]}';
                MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M2"}]}';
                MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":""}]}';
                MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M0"}]}';

                MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M2","werwert":"wert"}]}';
                MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
            }catch(Exception e){}
            Test.stopTest();
            
        }
        
    }
    
    /**
*
* Description: This method is used to cover ApprovemilestoneServiceImpl and ApprovemilestoneDataServiceImpl 
*
*/
    
    private testMethod static void approvemilestoneTest2(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account'LIMIT 1]; 
               System.debug('*****Account*******'+acc);

        //Account acc1 = [SELECT Id FROM Account where IsPersonAccount = true LIMIT 1];
        //System.debug('*****Account*******'+acc1);
        Opportunity opp = [select Id,Name,Hash_Id__c from opportunity where isACH__c = true LIMIT 1]; //where name = 'OppName' LIMIT 1
                System.debug('opp*****'+opp);

        Quote quoteRec = [Select id,Name from Quote where Opportunityid =: opp.id]; // LIMIT 1
        System.debug('quoteRec*****'+quoteRec);
        SLF_Credit__c credit =[Select id,IsSyncing__c from SLF_Credit__c where Opportunity__c =: opp.id Limit 1];
        
        List<Underwriting_File__c> underwritingLst  = [select Id,Opportunity__c,M0_Approval_Requested_Date__c, M1_Approval_Requested_Date__c, M2_Approval_Requested_Date__c,  (select id,Box_Installation_Photos_TS__c,Box_Install_Contracts_TS__c, Box_Loan_Agreements_TS__c, Box_Payment_Election_Form_TS__c, Box_Government_IDs_TS__c, Box_Utility_Bills_TS__c,Box_Final_Designs_TS__c,Box_Communications_TS__c,Box_Approval_Letters_TS__c from Box_Fields__r), 
                                                       Opportunity__r.Installer_Account__r.M0_Required__c, Opportunity__r.Installer_Account__r.M1_Required__c,Opportunity__r.Installer_Account__r.M2_Required__c,CO_Requested_Date__c from Underwriting_File__c where Opportunity__r.Id =: opp.id limit 1];
        underwritingLst[0].CO_Requested_Date__c = System.now();
        update underwritingLst;
        List<Equipment_Manufacturer__c> equipRecLst =[select id,Is_Active__c from Equipment_Manufacturer__c LIMIT 2];
        Equipment_Manufacturer__c equipRec = new Equipment_Manufacturer__c();
        Equipment_Manufacturer__c equipRec1 = new Equipment_Manufacturer__c();
        for(Equipment_Manufacturer__c equipmentIterate : equipRecLst)
        {
            if(equipmentIterate.Is_Active__c)
                equipRec = equipmentIterate;
            else 
                equipRec1 = equipmentIterate;
        }
        System_Design__c sysDsiRec = [Select id,Name,Battery_Capacity__c,Battery_Count__c,ModuleMake__c,Project_Type__c,Ground_Mount_Type__c,
                                      BatteryMake__c,Battery_Model__c,Est_Annual_Production_kWh__c,
                                      Inverter_Count__c,InverterMake__c,Inverter_Model__c,Module_Count__c,
                                      Module_Model__c,Tilt__c,Opportunity__c,System__c
                                      from System_Design__c where Opportunity__c =: opp.id];
        List<SF_Category_Map__c> mapList = new List<SF_Category_Map__c>();
        
        SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                         Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                         TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
        mapList.add(cm1);
        SF_Category_Map__c sfmap = new SF_Category_Map__c();
        sfmap.Name = 'Loan Agreement';
        sfmap.Folder_Name__c = 'Loan Agreements';
        sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
        sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
        sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_LT_Loan_Agreement_Date_Time__c';
        mapList.add(sfmap);
        insert mapList;
        
        quoteRec.System_Design__c= sysDsiRec.id;
        update quoteRec;
                    
        Contact conObj = new Contact();
        conObj = [Select id from Contact limit 1];
        if(conObj != null){
            conObj.Email = 'test@gmail.com';
            update conObj;
        }
        acc.M0_Required__c = '';
        acc.M1_Required__c = '';
        acc.M2_Required__c = '';
        update acc;
        
        System.runAs(loggedInUsr[0])
        {
            String apprmilString;
            Map<String, String> apprmilMap = new Map<String, String>();
            Test.startTest();
             try{
                     
                    
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"CO"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"Permit"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    apprmilString = '{"projects":[{"hashId":"'+opp.Hash_Id__c+'","milestoneRequest":"Permit"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    apprmilString = '{"projects":[{"externalId":"123","milestoneRequest":"Permit"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"Kitting"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"Inspection"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    sysDsiRec.Project_Type__c = 'Ground Mount PV';
                    update sysDsiRec;
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M1"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M2"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    opp.EDW_Originated__c = true;
                    update opp;
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"CO"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                }catch(Exception e){}
            Test.stopTest();
            
        }
        
    }
    

     /**
*
* Description: This method is used to cover ApprovemilestoneServiceImpl and ApprovemilestoneDataServiceImpl 
*
*/
    
    private testMethod static void approvemilestoneTest3(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account'LIMIT 1]; 
               System.debug('*****Account*******'+acc);

        //Account acc1 = [SELECT Id FROM Account where IsPersonAccount = true LIMIT 1];
        //System.debug('*****Account*******'+acc1);
        Opportunity opp = [select Id,Name,Hash_Id__c from opportunity where isACH__c = true LIMIT 1]; //where name = 'OppName' LIMIT 1
                System.debug('opp*****'+opp);

        Quote quoteRec = [Select id,Name from Quote where Opportunityid =: opp.id]; // LIMIT 1
        System.debug('quoteRec*****'+quoteRec);
        SLF_Credit__c credit =[Select id,IsSyncing__c from SLF_Credit__c where Opportunity__c =: opp.id Limit 1];
        
        List<Underwriting_File__c> underwritingLst  = [select Id,Opportunity__c,M0_Approval_Requested_Date__c, M1_Approval_Requested_Date__c, M2_Approval_Requested_Date__c,  (select id,Box_Installation_Photos_TS__c,Box_Install_Contracts_TS__c, Box_Loan_Agreements_TS__c, Box_Payment_Election_Form_TS__c, Box_Government_IDs_TS__c, Box_Utility_Bills_TS__c,Box_Final_Designs_TS__c,Box_Communications_TS__c,Box_Approval_Letters_TS__c from Box_Fields__r), 
                                                       Opportunity__r.Installer_Account__r.M0_Required__c, Opportunity__r.Installer_Account__r.M1_Required__c,Opportunity__r.Installer_Account__r.M2_Required__c,CO_Requested_Date__c from Underwriting_File__c where Opportunity__r.Id =: opp.id limit 1];
        underwritingLst[0].CO_Requested_Date__c = System.now();
        update underwritingLst;
        List<Equipment_Manufacturer__c> equipRecLst =[select id,Is_Active__c from Equipment_Manufacturer__c LIMIT 2];
        Equipment_Manufacturer__c equipRec = new Equipment_Manufacturer__c();
        Equipment_Manufacturer__c equipRec1 = new Equipment_Manufacturer__c();
        for(Equipment_Manufacturer__c equipmentIterate : equipRecLst)
        {
            if(equipmentIterate.Is_Active__c)
                equipRec = equipmentIterate;
            else 
                equipRec1 = equipmentIterate;
        }
        System_Design__c sysDsiRec = [Select id,Name,Battery_Capacity__c,Battery_Count__c,ModuleMake__c,Project_Type__c,Ground_Mount_Type__c,
                                      BatteryMake__c,Battery_Model__c,Est_Annual_Production_kWh__c,
                                      Inverter_Count__c,InverterMake__c,Inverter_Model__c,Module_Count__c,
                                      Module_Model__c,Tilt__c,Opportunity__c,System__c
                                      from System_Design__c where Opportunity__c =: opp.id];
        List<SF_Category_Map__c> mapList = new List<SF_Category_Map__c>();
        
        SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                         Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                         TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
        mapList.add(cm1);
        SF_Category_Map__c sfmap = new SF_Category_Map__c();
        sfmap.Name = 'Loan Agreement';
        sfmap.Folder_Name__c = 'Loan Agreements';
        sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
        sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
        sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_LT_Loan_Agreement_Date_Time__c';
        mapList.add(sfmap);
        insert mapList;
        
        Contact conObj = new Contact();
        conObj = [Select id from Contact limit 1];
        if(conObj != null){
            conObj.Email = 'test@gmail.com';
            update conObj;
        }
        acc.M0_Required__c = '';
        acc.M1_Required__c = '';
        acc.M2_Required__c = '';
        update acc;
        
        System.runAs(loggedInUsr[0])
        {
            String apprmilString;
            Map<String, String> apprmilMap = new Map<String, String>();
            Test.startTest();
             try{
                     
                    apprmilString = '{"projects":[{"id":"123456789","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"hashId":"123456789","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"externalId":"123456789","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"hashId":"'+opp.Hash_Id__c+'","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"externalId":"123","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    quoteRec.System_Design__c= sysDsiRec.id;
                    quoteRec.Combined_Loan_Amount__c = 70000;
                    update quoteRec;
                    
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"hashId":"'+opp.Hash_Id__c+'","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"externalId":"123","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    sysDsiRec.BatteryMake__c = equipRec.Id;
                    sysDsiRec.System__c = 2;
                    update sysDsiRec;
                    
                    apprmilString = '{"projects":[{"id":"'+opp.id+'","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"hashId":"'+opp.Hash_Id__c+'","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                    apprmilString = '{"projects":[{"externalId":"123","milestoneRequest":"M0"}]}';
                    MapWebServiceURI(apprmilMap,apprmilString,'getapprovemilestonem0');
                    
                }catch(Exception e){}
            Test.stopTest();
            
        }
        
    }
   
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}