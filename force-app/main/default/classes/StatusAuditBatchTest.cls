/******************************************************************************************
* Description: Test class to cover Status Audit Batch 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma K         06/27/2017          Created
******************************************************************************************/
@isTest
public with sharing class StatusAuditBatchTest {
    
    
    public testMethod static void StatusAuditBatchTestexecute(){
        
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        //Insert Account record
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Push_Endpoint__c ='http://mockbin.org';
        acc.Endpoint_Header__c ='https://connect.boomi.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Subscribe_to_Push_Updates__c= true;
        acc.Solar_Enabled__c = true; 
        insert acc;
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c =acc.id;
        insert opp;
        
        //Insert StatusAudit record
        Status_Audit__c sa = new Status_Audit__c();
        sa.JSON_Body__c = '{"projects":[{"stips":[{"stipStatus":"New","stipNotes":"Test","stipName":null,"stipId":"S-3","stipEmail":false,"stipDescription":"Verification of mortgage","stipCompletedDate":null},{"stipStatus":"In Progress","stipNotes":"Testing Stipulation","stipName":null,"stipId":"S-4","stipEmail":false,"stipDescription":"Loan agreement not customers legal name","stipCompletedDate":"2017-06-20"},{"stipStatus":"Completed","stipNotes":"Testing Stipulation","stipName":null,"stipId":"S-5","stipEmail":false,"stipDescription":"Loan agreement not customers legal name","stipCompletedDate":"2017-06-20"}],"projectStatus":null,"projectId":"006V000000713cz","fundingStatus":null,"errorMsg":null,"documents":[],"credits":[]}]}';
        sa.isProcessed__c = false;
        sa.Opportunity__c = opp.id;
        
        SLF_Boomi_API_Details__c objBoomiAPIDetails =new SLF_Boomi_API_Details__c();
        objBoomiAPIDetails.Name = 'StatusEndpoint';
        objBoomiAPIDetails.Username__c ='sunlightfinancial-WSHA3Z.Z72XBT';
        objBoomiAPIDetails.Password__c = 'p@ssw0Rd';
        objBoomiAPIDetails.Endpoint_URL__c ='https://connect.boomi.com/ws/rest/getstatus/statuspush/';
        objBoomiAPIDetails.viaBoomi__c = true;
        insert objBoomiAPIDetails;
        
        List<Status_Audit__c> lstSA= new List<Status_Audit__c>();
        lstSA.add(sa);        
        insert lstSA;
        
        Admin_Emails__c adEmail = new Admin_Emails__c();
        adEmail.Name ='Test Email';
        adEmail.Admin_Email__c = 'test@gmail.com';
        insert adEmail;
                
        Test.startTest(); 
        Integer stsCode = 200;
        StatusAuditBatch sab= new StatusAuditBatch(System.today(),System.today());
        Database.executeBatch(sab);
        StatusAuditBatch sab1= new StatusAuditBatch(null,null);        
        Test.stopTest(); 
    
    }
    
    
    
    
    public testMethod static void testschedule() {
    
        Batch_Jobs__c cnf = new Batch_Jobs__c();
        cnf.Name='Status Audit';
        cnf.Active__c =true;
        insert cnf;
        Test.StartTest();
        StatusAuditBatchSh sh1 = new StatusAuditBatchSh();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check',sch,sh1);
        Test.stopTest();
    }
    
}