/**
* Description: Equipment API related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh Raparthi        07/10/2017          Created
******************************************************************************************/
public with sharing class EquipmentServiceImpl extends RESTServiceBase{  
    private String msg = '';
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + EquipmentServiceImpl.class);
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        ResponseWrapper res = new ResponseWrapper();
        EquipmentDataServiceImpl equipmentDataSrvImpl = new EquipmentDataServiceImpl(); 
        IRestResponse iRestRes;
        string errMsg = '';
        try{
            system.debug('### rw.reqDataStr... '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData... '+reqData);
            
             res.response = equipmentDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
            
            system.debug('### EquipmentServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' EquipmentServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            
            UnifiedBean unifBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            unifBean.error  = errorWrapperLst ;
            unifBean.returnCode = '214';
            iRestRes = (IRestResponse)unifBean;
            res.response= iRestRes;
        }
       
        system.debug('### Exit from fetchData() of ' + EquipmentServiceImpl.class);
        return res.response;
    }
    
    
}