@isTest(seeAllData=false)
public class PrequalDecisionServiceImplTest{    
    
    private testMethod static void CreditRefactorTest(){
       
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);
        TriggerFlags__c trgflag3 = new TriggerFlags__c();
        trgflag3.Name ='SLF_Credit__c';
        trgflag3.isActive__c =true;
        trgrList.add(trgflag3);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='Contact';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        TriggerFlags__c trgflag5 = new TriggerFlags__c();
        trgflag5.Name ='User';
        trgflag5.isActive__c =true;
        trgrList.add(trgflag5);
        TriggerFlags__c trgflag6 = new TriggerFlags__c();
        trgflag6.Name ='Quote';
        trgflag6.isActive__c =true;
        trgrList.add(trgflag6);
        insert trgrList;
        
        //Insert Custom Endpoints Custom Setting Record
        List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
        SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
        endpoint1.Name = 'Pricing';
        endpoint1.Endpoint_URL__c =  'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
        endpoint.add(endpoint1);
        SLF_Boomi_API_Details__c endpoint2 = new SLF_Boomi_API_Details__c();
        endpoint2.Name = 'BoomiToAWSPricing';
        endpoint2.Endpoint_URL__c =  'https://test.connect.boomi.com/ws/rest/v1/dev/pricing/callaws/';
        endpoint.add(endpoint2);
        
        
        SLF_Boomi_API_Details__c apidetails = new SLF_Boomi_API_Details__c();
        apidetails.Name ='Boomi QA';
        apidetails.Username__c ='sunlightfinancial-WSHA3Z.N29SCE';
        apidetails.Password__c ='2458a5de-d689-4708-a4fb-d3594c067610';
        apidetails.Endpoint_URL__c ='https://test.connect.boomi.com/ws/soap12/prequal';
        apidetails.viaBoomi__c =true;
        endpoint.add(apidetails);
        
        SLF_Boomi_API_Details__c endpoint3 = new SLF_Boomi_API_Details__c();
        endpoint3.Name = 'FNI QA';
        endpoint3.Username__c =  'salesforceuser';
        endpoint3.Password__c = 'RC8N1CreKnz4rm2kMTCj';
        endpoint.add(endpoint3);
        
        SLF_Boomi_API_Details__c endpoint4 = new SLF_Boomi_API_Details__c();
        endpoint4.Name = 'FNI Prod';
        endpoint4.Username__c =  'salesforceuser';
        endpoint4.Password__c = 'AmtPHqSlK9sB6NXjSyq4';
        endpoint.add(endpoint4);
        insert endpoint;  
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';        
        accObj.Type = 'Facility'; 
        accObj.Prequal_Enabled__c = true;
        accObj.Solar_Enabled__c = true;        
        insert accObj;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = accObj.Id);
        insert con;  
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        System.runAs(loggedInUsr[0])
        {
            Product__c prdObj = new Product__c(Name='Test',
                                               Product_Loan_Type__c='Solar',
                                               Installer_Account__c = accObj.Id,
                                               State_Code__c='CA',
                                               FNI_Min_Response_Code__c=2,
                                               Product_Display_Name__c = 'Display Name',
                                               Qualification_Message__c = 'Message',
                                               Term_mo__c = 2,
                                               APR__c = 20,
                                               ACH__c = true,
                                               Internal_Use_Only__c = FALSE,
                                               Is_Active__c = TRUE,
                                               Long_Term_Facility__c = 'CVX',
                                               Product_Tier__c = '0');//,Long_Term_Facility_Lookup__c = accObj.Id
            insert prdObj;
            
            Opportunity oppObj = new Opportunity(Name = 'OppName',
                                                 AccountId=accObj.Id,
                                                 Co_Applicant__c = accObj.Id,
                                                 StageName='Qualified',
                                                 CloseDate=system.today(),
                                                 SLF_Product__c = prdObj.ID,
                                                 Installer_Account__c = accObj.Id, 
                                                 Install_State_Code__c = 'CA',
                                                 Synced_Quote_Id__c = 'qId',
                                                 //Partner_Foreign_Key__c = 'CVX1254',
                                                 Desync_Bypass__c = true,
                                                 Install_Street__c='40 CORTE ALTA',
                                                 Install_Postal_Code__c='94949',
                                                 Install_City__c='California');
            insert oppObj;        
                    
            SLF_Credit__c slfCreditObj = new SLF_Credit__c();
            slfCreditObj.Opportunity__c = oppObj.id;
            slfCreditObj.Primary_Applicant__c = accObj.id;
            slfCreditObj.Co_Applicant__c = accObj.id;
            slfCreditObj.SLF_Product__c = prdObj.Id ;
            slfCreditObj.Total_Loan_Amount__c = 100;
            slfCreditObj.Status__c = 'New';
            slfCreditObj.LT_FNI_Reference_Number__c = '123';
            slfCreditObj.Application_ID__c = '123';
            insert slfCreditObj;
            
            Prequal__c prqlObj = new Prequal__c(Opportunity__c = oppObj.Id,
                                                Installer_Account__c =accObj.Id,
                                                Middle_Name__c = 'K',
                                                SSN__c = '123456789',
                                                Term_mo__c = 2,
                                                DOB__c = system.today(),
                                                Mailing_State__c='CA',
                                                Mailing_Postal_Code__c = '25846',
                                                Annual_Income__c = 12300,
                                                Lending_Facility__c = 'CVX,TCU',
                                                Last_Name__c = 'Test',
                                                Employment_Years__c=5,
                                                Employment_Months__c = 1,
                                                Co_LastName__c = 'Test',
                                                Co_FirstName__c = 'Test',
                                                Co_Middle_Initial__c = 'w',
                                                Co_SSN__c = '123456758',
                                                Co_Date_of_Birth__c = System.today(),
                                                Co_Mailing_Street__c = 'Street',
                                                Co_Mailing_City__c = 'City',
                                                Co_Mailing_State__c = 'CA',
                                                Co_Mailing_Postal_Code__c = '45678',
                                                Co_Employer_Name__c = 'Employee',
                                                Co_Annual_Income__c = 200000,
                                                Co_Employment_Years__c = 1,
                                                Co_Employment_Months__c = 2,
                                                APR__c = 20
                                                );
            
            insert prqlObj;     
            
            Quote qObj = new Quote(name = 'quote',
            OpportunityId = oppObj.Id);
            insert qObj;
            
            Prequal_Decision__c prequalDec = new Prequal_Decision__c();
            prequalDec.SLF_Product__c = prdObj.id;
            prequalDec.Product_Type__c = 'HIN';
            prequalDec.Prequal__c = prqlObj.id;
            insert prequalDec;
            
            Map<String, String> createincenMap = new Map<String, String>();
            
            oppObj = [Select id,Name,AccountId,Co_Applicant__c,StageName,CloseDate,SLF_Product__c,Installer_Account__c,
                      Install_State_Code__c,Synced_Quote_Id__c,Desync_Bypass__c,Install_Street__c,
                      Install_Postal_Code__c,Install_City__c,Hash_Id__c from Opportunity where id = :oppObj.Id];
            
            String prequalRefactor;
            Test.startTest();            
            prequalRefactor = '{"projects" : [{"id": "'+oppObj.Id+'","prequals" : [{"id" : "'+prqlObj.Id+'","fniLTRes" : "<?xml version=\\"1.0\\" encoding=\\"UTF-8\\" standalone=\\"no\\"?><soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"><soap:Header/><soap:Body><ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage/><ns3:FNIReferenceNumber>20000005949557</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-07-03T09:47:53-05:00</ns3:DecisionDateTime><ns3:FirstName>BARBARA</ns3:FirstName><ns3:LastName>WILLEMSEN</ns3:LastName><ns3:TransactionID>2D96BF5B-FB88-4AC9-81C5-388A37A5CAB5</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2>VOI</ns3:StipRsn2><ns3:StipRsn3/><ns3:StipRsn4/><ns3:DTI>0</ns3:DTI><ns3:LowFico>812</ns3:LowFico><ns3:DebtForMax>35</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line/><ns3:StipRsn>VOI,</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line/><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>AAC</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line/><ns3:StipRsn>VOI,</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line/><ns3:StipRsn>VOI,</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>750</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision');
            
            oppObj.Project_Category__c = 'Home';
            update oppObj;
            prequalRefactor = '{"projects" : [{"id": "'+oppObj.Id+'","prequals" : [{"id" : "'+prqlObj.Id+'","decisionDetails":"P","fniLTRes" : "<?xml version=\\"1.0\\" encoding=\\"UTF-8\\" standalone=\\"no\\"?><soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"><soap:Header/><soap:Body><ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage/><ns3:FNIReferenceNumber>20000011504010</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2019-05-02T11:20:27-05:00</ns3:DecisionDateTime><ns3:FirstName>YESHAR</ns3:FirstName><ns3:LastName>DONALD</ns3:LastName><ns3:TransactionID>71B45728-60AD-4CC0-AA89-324069B2FFB1</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1/><ns3:StipRsn2/><ns3:StipRsn3/><ns3:StipRsn4/><ns3:LineAssignmentAmt>0</ns3:LineAssignmentAmt><ns3:DTI>18.3</ns3:DTI><ns3:LowFico>793</ns3:LowFico><ns3:DebtForMax>3074</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>5000000</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>11134</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI><ns3:ProductType>HII</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HII</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>AAC</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HII</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>5000000</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>11134</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>AAC</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HII</ns3:ProductType></ns3:Lender></ns3:PrequalDecisions><ns3:ProductDecision><ns3:ProductType>HII</ns3:ProductType><ns3:Decision>A</ns3:Decision></ns3:ProductDecision><ns3:ProductDecision><ns3:ProductType>HIS</ns3:ProductType><ns3:Decision>A</ns3:Decision></ns3:ProductDecision><ns3:ApplicationDate>2019-05-02T11:20:27-05:00</ns3:ApplicationDate><ns3:Applicant><ns3:FirstName>YESHAR</ns3:FirstName><ns3:MiddleInitial/><ns3:LastName>DONALD</ns3:LastName><ns3:SSN/><ns3:DOB/><ns3:FICO>0793</ns3:FICO><ns3:FICODSC1>LENGTH OF TIME SINCE MOST RECENT ACCOUNT ESTABLISHED</ns3:FICODSC1><ns3:FICODSC2>PROPORTION OF BALANCE TO HIGH CREDIT ON BANK REVOLVING OR ALL REVOLVING ACCOUNTS</ns3:FICODSC2><ns3:FICODSC3>CURRENT BALANCES ON REVOLVING ACCOUNTS</ns3:FICODSC3><ns3:FICODSC4>LENGTH OF REVOLVING ACCOUNT HISTORY</ns3:FICODSC4></ns3:Applicant><ns3:RBP><ns3:Lenders><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Tiers><ns3:Tier><ns3:TierID>1</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>550</ns3:MaxDTI><ns3:MaxPayment>139007</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HII</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>2</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HII</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>3</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HII</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>1</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>550</ns3:MaxDTI><ns3:MaxPayment>139007</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HIS</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>2</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HIS</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>3</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HIS</ns3:ProductType></ns3:Tier></ns3:Tiers></ns3:Lender></ns3:Lenders><ns3:GlobalDecision>A</ns3:GlobalDecision></ns3:RBP></ns3:RESPONSE></soap:Body></soap:Envelope>"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision');
            
            prequalRefactor = '{"projects" : [{"id": "'+oppObj.Id+'","prequals" : [{"id" : "'+prqlObj.Id+'","fniLTRes" : "<?xml version=\\"1.0\\" encoding=\\"UTF-8\\" standalone=\\"no\\"?><soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"><soap:Header/><soap:Body><ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage/><ns3:FNIReferenceNumber>20000011504010</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2019-05-02T11:20:27-05:00</ns3:DecisionDateTime><ns3:FirstName>YESHAR</ns3:FirstName><ns3:LastName>DONALD</ns3:LastName><ns3:TransactionID>71B45728-60AD-4CC0-AA89-324069B2FFB1</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1/><ns3:StipRsn2/><ns3:StipRsn3/><ns3:StipRsn4/><ns3:LineAssignmentAmt>0</ns3:LineAssignmentAmt><ns3:DTI>18.3</ns3:DTI><ns3:LowFico>793</ns3:LowFico><ns3:DebtForMax>3074</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>5000000</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>11134</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI><ns3:ProductType>HIN</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HII</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>AAC</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HII</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>5000000</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>11134</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>CVX</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>AAC</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HIS</ns3:ProductType></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>A</ns3:Decision><ns3:Line>0</ns3:Line><ns3:StipRsn/><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>0</ns3:MaxDTI><ns3:ProductType>HII</ns3:ProductType></ns3:Lender></ns3:PrequalDecisions><ns3:ProductDecision><ns3:ProductType>HII</ns3:ProductType><ns3:Decision>A</ns3:Decision></ns3:ProductDecision><ns3:ProductDecision><ns3:ProductType>HIS</ns3:ProductType><ns3:Decision>A</ns3:Decision></ns3:ProductDecision><ns3:ApplicationDate>2019-05-02T11:20:27-05:00</ns3:ApplicationDate><ns3:Applicant><ns3:FirstName>YESHAR</ns3:FirstName><ns3:MiddleInitial/><ns3:LastName>DONALD</ns3:LastName><ns3:SSN/><ns3:DOB/><ns3:FICO>0793</ns3:FICO><ns3:FICODSC1>LENGTH OF TIME SINCE MOST RECENT ACCOUNT ESTABLISHED</ns3:FICODSC1><ns3:FICODSC2>PROPORTION OF BALANCE TO HIGH CREDIT ON BANK REVOLVING OR ALL REVOLVING ACCOUNTS</ns3:FICODSC2><ns3:FICODSC3>CURRENT BALANCES ON REVOLVING ACCOUNTS</ns3:FICODSC3><ns3:FICODSC4>LENGTH OF REVOLVING ACCOUNT HISTORY</ns3:FICODSC4></ns3:Applicant><ns3:RBP><ns3:Lenders><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Tiers><ns3:Tier><ns3:TierID>1</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>550</ns3:MaxDTI><ns3:MaxPayment>139007</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HII</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>2</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HII</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>3</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HII</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>1</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>550</ns3:MaxDTI><ns3:MaxPayment>139007</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HIS</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>2</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HIS</ns3:ProductType></ns3:Tier><ns3:Tier><ns3:TierID>3</ns3:TierID><ns3:StipRsn/><ns3:MaxDTI>450</ns3:MaxDTI><ns3:MaxPayment>113174</ns3:MaxPayment><ns3:Decision>A</ns3:Decision><ns3:ProductType>HIS</ns3:ProductType></ns3:Tier></ns3:Tiers></ns3:Lender></ns3:Lenders><ns3:GlobalDecision>A</ns3:GlobalDecision></ns3:RBP></ns3:RESPONSE></soap:Body></soap:Envelope>"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision');
            
            prequalRefactor = null;
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision');
            
            PrequalDecisionServiceImpl prqlsiObj = new PrequalDecisionServiceImpl();
            prqlsiObj.fetchData(null);
            
            //prequalRefactor = '{"projects" : [{"prequals" : [{"id" : "'+prqlObj.Id+'","fniLTReq" : "<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:req="http://tko.fni.com/application/request.xsd" xmlns:tran="http://tko.fni.com/application/transaction.xsd"><soapenv:Header/><soapenv:Body><req:REQUEST><tran:TransactionControl><tran:UserName>salesforceuser</tran:UserName><tran:Password>RC8N1CreKnz4rm2kMTCj</tran:Password><tran:TransactionTimeStamp>2018-07-03T02:46:22.808Z</tran:TransactionTimeStamp><tran:Action>PREQUAL</tran:Action><tran:ExternalReferenceNumber>a1129000002fmkCAAQ</tran:ExternalReferenceNumber><tran:PartnerId>SALESFORCE</tran:PartnerId><tran:DomainName>BrightPlanet</tran:DomainName></tran:TransactionControl><req:RequestData><req:AppRequest><req:Applicants><req:Applicant><req:ApplicantType>PRIM</req:ApplicantType><req:Name><req:First>BARBARA</req:First><req:Last>WILLEMSEN</req:Last></req:Name><req:SSN>666295782</req:SSN><req:DateOfBirth>19320101</req:DateOfBirth><req:Addresses><req:Address><req:AddressType>CURR</req:AddressType><req:AddressLine1>40 CORTE ALTA</req:AddressLine1><req:City>NOVATO</req:City><req:State>CA</req:State><req:PostalCode>94949</req:PostalCode></req:Address></req:Addresses><req:Employers><req:Employer><req:EmploymentType>CURR</req:EmploymentType><req:CompanyName>CIST</req:CompanyName><req:EmploymentStatus>FT</req:EmploymentStatus><req:YearlyIncome>520000</req:YearlyIncome></req:Employer></req:Employers></req:Applicant></req:Applicants><req:Loan><req:CombinedAmount>28460.00</req:CombinedAmount><req:Term></req:Term><req:APR></req:APR><req:ProductType>LOA</req:ProductType></req:Loan></req:AppRequest></req:RequestData></req:REQUEST></soapenv:Body></soapenv:Envelope>"}]}]}';
            //SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision');
            
            /*prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision'); 
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "2012-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision'); 
            
            prequalRefactor = '{"projects": [{"externalId": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision'); 
                       
            prequalRefactor = '{"projects": [{"hashId": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision'); 
           
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","externalId": "'+oppObj.Id+'","hashId": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequaldecision');*/            
                      
            Test.stopTest();
        }
    }
}