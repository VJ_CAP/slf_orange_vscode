public class callFNILeadTriggerStaticHelper {
public static Map<String,boolean> mapCalledFNI = new Map<String,boolean>();
    public static boolean callFNI(Lead thisLead, Map<Id, Lead> oldLeadMap){
        //check if min required fields are completed
            System.debug('### In callFNI method...');
            if(thisLead.FirstName != null && 
               thisLead.LastName != null && 
               thisLead.Street != null &&
               thisLead.City != null &&
               thisLead.StateCode != null &&
               thisLead.PostalCode != null &&
               thisLead.Customer_has_authorized_credit_soft_pull__c == true &&
               thisLead.FNI_Decision__c != '99' &&
               thisLead.FNI_Decision__c != '0'
               //thisLead.FNI_Decision__c != 'E' &&
               //thisLead.FNI_Decision__c != ''
               ){
                    //check if FNI decision is set (if not, make first call) 
                   if(thisLead.FNI_Decision__c == null){
                       System.debug('### thisLead.FNI_Decision__c...');
                       //don't recall if there is an error (E), sunlight must clear the E to get a recall.  This prevents the recursion of a E being set.
                       return true;
                   }else{
                       //fni has already been called, check if min fields have changed
                       Lead oldLead = oldLeadMap.get(thisLead.Id);
                       if(thisLead.FirstName != oldLead.Firstname || 
                          thisLead.LastName != oldLead.LastName || 
                          thisLead.Street != oldLead.Street ||
                          thisLead.City != oldLead.City ||
                          thisLead.StateCode != oldLead.StateCode ||
                          thisLead.PostalCode != oldLead.PostalCode){
                              //some of the min fields have changed, recall fni
                              System.debug('### else...');
                              return true;
                          } else{
                              //min fields haven't changed, but if fni decision was 3, check if optional fields have changed - if so, call fni
                              //if(thisLead.FNI_Decision__c == '3')
                              if((thisLead.FNI_Decision__c == 'D' && thisLead.Stip_Code__c=='D27') || (thisLead.FNI_Decision__c == 'P' && thisLead.Stip_Code__c=='MI'))
                              {        
                                  if(thisLead.DOB__c != oldLead.DOB__c ||
                                     thisLead.Yearly_Income__c != oldLead.Yearly_Income__c){
                                         return true;
                                     }else if(thisLead.SSN__c != oldLead.SSN__c){
                                         if(thisLead.SSN__c != null){
                                             System.debug('### thisLead.SSN__c:' + thisLead.SSN__c.length() + ';');
                                             if(thisLead.SSN__c.length() == 11)
                                             {                                                 
                                                 return true;
                                             }
                                         }
                                     }
                              }
                          }
                   }
               }
        return false;
    }
}