global class Batch_UpdateSiteTS implements Database.Batchable<Site_Download__c>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {
    
    private Integer m_pageSize;
    private Integer m_page;
    Boolean m_stopBatch; // *** set when getSiteRecordsWithSightenTS returns 0 records
    
    public Batch_UpdateSiteTS(Integer pageSize, Integer page)
    {
        m_pageSize = pageSize;
        m_page = page;
    }
    public Batch_UpdateSiteTS()
    {

    }    
    
    global Iterable<Site_Download__c> start(Database.BatchableContext BC)
    {
        
        m_stopBatch = false;
        
        Boolean done = false;
        List<Site_Download__c> dl = new List<Site_Download__c>();
        List<Opportunity> ol = new List<Opportunity>([SELECT Id, Sighten_UUID__c FROM Opportunity where Sighten_UUID__c != null and Id NOT IN (Select Opportunity__c from Underwriting_File__c where Project_Status__c IN ('Project Withdrawn', 'Declined', 'Project Completed'))]); 
        List<StaticResource> sr = [Select body from StaticResource where Name = 'sighten_site_ts_response_json']; 
        String testResponse = sr[0].body.toString();
         
        while (!done)
        {
            System.Debug('**** calling getsite with page size =>' + m_pageSize + ' page number => ' + m_page);
            List<Site_Download__c> nxtList = sightenUtil.getSiteRecordsWithSightenTS(ol,testResponse,m_pageSize, m_page);  
            Site_Download__c f = nxtList.get(0);
            
            System.Assert(f != null, 'First element should never be null');
            if (f.Site_UUID__c == 'Out of Range' || Test.IsRunningTest()){
                done = true;
            }else{
                // *** if no matching sites in batch, just increment the page number 
                if(f.Site_UUID__C == 'No Matching Sites in Batch'){
                    m_page++;
                }else{
                    m_page++;
                    dl.addAll(nxtList);
                }
            }
        }
        return dl;
    }

   global void execute(SchedulableContext sc)
    {
        //throw new CustException('In start size => ' + m_pageSize + ' num => ' + m_page);
        Batch_UpdateSiteTS bf = new Batch_UpdateSiteTS(10000,1);
        Database.executeBatch(bf,2000);
    }
    
    global void execute(Database.BatchableContext BC,List<Site_Download__c> dl) 
    {
        //throw new CustException(' will upsert list of size ' + dl.size());
        System.Debug('****** upserting list of size => ' + dl.size());

        upsert dl Site_UUID__c;  

        
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        
        if (!Test.IsRunningTest()) {
            if (settings.CHAIN_BATCHES__c) {
                    Database.executeBatch(new Batch_FetchSiteTS(), 1);
            }
        }
    }
}