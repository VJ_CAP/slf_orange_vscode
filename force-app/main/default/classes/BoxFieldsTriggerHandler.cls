/**
* Description: Trigger Business logic associated with Box_Fields__c is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           01/31/2019          Created 
******************************************************************************************/
public with sharing Class BoxFieldsTriggerHandler
{
    public static boolean isTrgExecuting=true;
    public static boolean runOnce(){
        if(isTrgExecuting){
            isTrgExecuting=false;
            return true;
        }else{
            return isTrgExecuting;
        }
    }
    
    /**
    * Constructor to initialize
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public BoxFieldsTriggerHandler(boolean isExecuting){
       // isTrgExecuting = isExecuting;
    }
    
    /**
    * Description: This method is used to perform after update related logic.
    *
    * @param newBoxFieldsMap    Get updated Box_Fields__c records.
    * @param oldBoxFieldsMap    Get old Box_Fields__c records.
    */
    public void onAfterUpdate(Map<Id, Box_Fields__c> newBoxFieldsMap, Map<Id, Box_Fields__c> oldBoxFieldsMap){
        try
        {
            system.debug('### Entered into onAfterUpdate() of '+ BoxFieldsTriggerHandler.class);
            Map<Id,Box_Fields__c> uwMap = new Map<Id,Box_Fields__c>();
            Set<String> setStipDataNames = new Set<String>{'LAA', 'LAN', 'LAS','LAX'};
            Set<String> setStipDataNames1 = new Set<String>{'HIA','HIC','HIE','HIN','HIS','HII'};
            Set<Id> UWIds = new Set<Id>();
            
            for(Box_Fields__c boxFldIterate : newBoxFieldsMap.values()){
                UWIds.add(boxFldIterate.Underwriting__c);
                If((newBoxFieldsMap.get(boxFldIterate.Id).Latest_LT_Loan_Agreement_Date_Time__c != oldBoxFieldsMap.get(boxFldIterate.Id).Latest_LT_Loan_Agreement_Date_Time__c) || (newBoxFieldsMap.get(boxFldIterate.Id).Latest_Install_Contract_Date_Time__c != oldBoxFieldsMap.get(boxFldIterate.Id).Latest_Install_Contract_Date_Time__c ) && boxFldIterate.Project_Status__c != 'Project Completed' && boxFldIterate.Project_Status__c != 'Declined'){
                    uwMap.put(boxFldIterate.Underwriting__c,boxFldIterate);
                }
            }
            if(!uwMap.isEmpty()){
                system.debug('inside=============='+uwMap);
                list<Stipulation__c> lstStips = [select Id,Underwriting__c,CreatedDate,Stipulation_Data__r.Name,Status__c from Stipulation__c where Underwriting__c IN : uwMap.keyset() and Stipulation_Data__r.Name in ('LAA', 'LAN', 'LAS','LAX','HIA','HIC','HIE','HIN','HIS','HII') and Status__c in ('New','In Progress','Under Review')];
                list<Stipulation__c> lstStipsToUpdate = new  list<Stipulation__c> ();
                for(Stipulation__c stipIterate : lstStips){
                    system.debug('==========='+lstStips);
                    if(stipIterate.Createddate < uwMap.get(stipIterate.Underwriting__c).Latest_LT_Loan_Agreement_Date_Time__c && setStipDataNames.Contains(stipIterate.Stipulation_Data__r.Name)) {
                        stipIterate.Status__c = 'Documents Received';
                        system.debug('first IF=======');
                        lstStipsToUpdate.add(stipIterate);
                    }else if(stipIterate.Createddate < uwMap.get(stipIterate.Underwriting__c).Latest_Install_Contract_Date_Time__c && setStipDataNames1.Contains(stipIterate.Stipulation_Data__r.Name)){
                        stipIterate.Status__c = 'Documents Received'; 
                        lstStipsToUpdate.add(stipIterate);
                    }
                }
                if(!lstStipsToUpdate.isEmpty()){
                    system.debug('Update Call======='+lstStipsToUpdate);
                    database.update(lstStipsToUpdate);
                }
            }
            
            if(!UWIds.isEmpty())
            {
                List<Underwriting_File__c> updateUWLst = new List<Underwriting_File__c>();
                for(Id UWIdIterate : UWIds){
                    Underwriting_File__c UWRec = new Underwriting_File__c(id=UWIdIterate);
                    updateUWLst.add(UWRec);
                }
                if(!updateUWLst.isEmpty())
                {
                    update updateUWLst;
                }
            }
                    
        }
        catch(Exception ex)
        {
         system.debug('### BoxFieldsTriggerHandler.onAfterUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
         ErrorLogUtility.writeLog('BoxFieldsTriggerHandler.onAfterUpdate()',ex.getLineNumber(),'onAfterUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }  
        system.debug('### Exit from onAfterUpdate() of '+ BoxFieldsTriggerHandler.class);     
    }
 
}