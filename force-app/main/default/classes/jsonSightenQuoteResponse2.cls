public class jsonSightenQuoteResponse2 {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public class Incentives {
		public Boolean override_Z {get;set;} // in json: override
		public String date_created {get;set;} 
		public String incentive_type {get;set;} 
		public Created_by created_by {get;set;} 
		public List<Integer> annual_amounts {get;set;} 
		public String uuid {get;set;} 
		public String date_updated {get;set;} 
		public Created_by owned_by_user {get;set;} 
		public Integer upfront_amount {get;set;} 
		public String natural_id {get;set;} 
		public Created_by owned_by_organization {get;set;} 
		public String assigned_to {get;set;} 
		public Created_by modified_by {get;set;} 

		public Incentives(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'override') {
							override_Z = parser.getBooleanValue();
						} else if (text == 'date_created') {
							date_created = parser.getText();
						} else if (text == 'incentive_type') {
							incentive_type = parser.getText();
						} else if (text == 'created_by') {
							created_by = new Created_by(parser);
						} else if (text == 'annual_amounts') {
							annual_amounts = new List<Integer>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								annual_amounts.add(parser.getIntegerValue());
							}
						} else if (text == 'uuid') {
							uuid = parser.getText();
						} else if (text == 'date_updated') {
							date_updated = parser.getText();
						} else if (text == 'owned_by_user') {
							owned_by_user = new Created_by(parser);
						} else if (text == 'upfront_amount') {
							upfront_amount = parser.getIntegerValue();
						} else if (text == 'natural_id') {
							natural_id = parser.getText();
						} else if (text == 'owned_by_organization') {
							owned_by_organization = new Created_by(parser);
						} else if (text == 'assigned_to') {
							assigned_to = parser.getText();
						} else if (text == 'modified_by') {
							modified_by = new Created_by(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Incentives consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Messages {
		public List<Reasons> critical {get;set;} 
		public List<Reasons> info {get;set;} 
		public List<Reasons> warning {get;set;} 
		public List<Reasons> error {get;set;} 

		public Messages(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'critical') {
							critical = new List<Reasons>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								critical.add(new Reasons(parser));
							}
						} else if (text == 'info') {
							info = new List<Reasons>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								info.add(new Reasons(parser));
							}
						} else if (text == 'warning') {
							warning = new List<Reasons>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								warning.add(new Reasons(parser));
							}
						} else if (text == 'error') {
							error = new List<Reasons>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								error.add(new Reasons(parser));
							}
						} else {
							System.debug(LoggingLevel.WARN, 'Messages consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	//public Data data {get;set;} 
    public List<Data> data {get;set;} 
	public Integer status_code {get;set;} 
	public Messages messages {get;set;} 

	public jsonSightenQuoteResponse2(JSONParser parser) {
		while (parser.nextToken() != JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != JSONToken.VALUE_NULL) {
					if (text == 'data') {
                         
                        
						data = new List<Data>();
                        
                        //if next token is start object, just add it
                        if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                            System.debug('***Debug: current token: (object)' + parser.getCurrentToken());
                            data.add(new Data(parser));
                        }else{                        
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                       //         System.debug('***Debug: current token: ' + parser.getCurrentToken());
                                data.add(new Data(parser));
                           }
                        }
					} else if (text == 'status_code') {
						status_code = parser.getIntegerValue();
					} else if (text == 'messages') {
						messages = new Messages(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Reasons {

		public Reasons(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Reasons consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Data {
		public Double avg_monthly_solar_bill {get;set;} 
		public Double install_cost_cash {get;set;} 
		public String uuid {get;set;} 
		public String status {get;set;} 
		public List<Incentives> incentives {get;set;} 
		public Proposals proposals {get;set;} 
		public Created_by owned_by_user {get;set;} 
		public Created_by product {get;set;} 
		public String date_created {get;set;} 
		public Created_by owned_by_organization {get;set;} 
		public Created_by modified_by {get;set;} 
		public Product_eligibility product_eligibility {get;set;} 
		public String natural_id {get;set;} 
		public Proposals tasks {get;set;} 
		public Created_by system_Z {get;set;} // in json: system
		public Double consumer_savings_usd {get;set;} 
		public Integer rate_esc_pct {get;set;} 
		public Double consumer_savings_pct {get;set;} 
		public String date_updated {get;set;} 
		public Integer contract_term {get;set;} 
		public Integer amount_financed {get;set;} 
		public Double rate_contract {get;set;} 
		public Proposals milestones {get;set;} 
		public Created_by created_by {get;set;} 
		public Integer initial_payment {get;set;} 

		public Data(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'avg_monthly_solar_bill') {
							avg_monthly_solar_bill = parser.getDoubleValue();
						} else if (text == 'install_cost_cash') {
							install_cost_cash = parser.getDoubleValue();
						} else if (text == 'uuid') {
							uuid = parser.getText();
						} else if (text == 'status') {
							status = parser.getText();
						} else if (text == 'incentives') {
							incentives = new List<Incentives>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								incentives.add(new Incentives(parser));
							}
						} else if (text == 'proposals') {
							proposals = new Proposals(parser);
						} else if (text == 'owned_by_user') {
							owned_by_user = new Created_by(parser);
						} else if (text == 'product') {
							product = new Created_by(parser);
						} else if (text == 'date_created') {
							date_created = parser.getText();
						} else if (text == 'owned_by_organization') {
							owned_by_organization = new Created_by(parser);
						} else if (text == 'modified_by') {
							modified_by = new Created_by(parser);
						} else if (text == 'product_eligibility') {
							product_eligibility = new Product_eligibility(parser);
						} else if (text == 'natural_id') {
							natural_id = parser.getText();
						} else if (text == 'tasks') {
							tasks = new Proposals(parser);
						} else if (text == 'system') {
							system_Z = new Created_by(parser);
						} else if (text == 'consumer_savings_usd') {
							consumer_savings_usd = parser.getDoubleValue();
						} else if (text == 'rate_esc_pct') {
							rate_esc_pct = parser.getIntegerValue();
						} else if (text == 'consumer_savings_pct') {
							consumer_savings_pct = parser.getDoubleValue();
						} else if (text == 'date_updated') {
							date_updated = parser.getText();
						} else if (text == 'contract_term') {
							contract_term = parser.getIntegerValue();
						} else if (text == 'amount_financed') {
							amount_financed = parser.getIntegerValue();
						} else if (text == 'rate_contract') {
							rate_contract = parser.getDoubleValue();
						} else if (text == 'milestones') {
							milestones = new Proposals(parser);
						} else if (text == 'created_by') {
							created_by = new Created_by(parser);
						} else if (text == 'initial_payment') {
							initial_payment = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Created_by {
		public String link {get;set;} 
		public String uuid {get;set;} 
		public String natural_id {get;set;} 

		public Created_by(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'link') {
							link = parser.getText();
						} else if (text == 'uuid') {
							uuid = parser.getText();
						} else if (text == 'natural_id') {
							natural_id = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Created_by consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Proposals {
		public String link {get;set;} 

		public Proposals(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'link') {
							link = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Proposals consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Eligible {
		public Reasons reasons {get;set;} 
		public String name {get;set;} 
		public Integer qual_def_id {get;set;} 
		public String message {get;set;} 
		public String uuid {get;set;} 

		public Eligible(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'reasons') {
							reasons = new Reasons(parser);
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'qual_def_id') {
							qual_def_id = parser.getIntegerValue();
						} else if (text == 'message') {
							message = parser.getText();
						} else if (text == 'uuid') {
							uuid = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Eligible consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Product_eligibility {
		public List<Eligible> eligible {get;set;} 
		public List<Reasons> ineligible {get;set;} 
		public String user {get;set;} 
		public List<Reasons> pending {get;set;} 
		public List<Reasons> warning {get;set;} 

		public Product_eligibility(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'eligible') {
							eligible = new List<Eligible>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								eligible.add(new Eligible(parser));
							}
						} else if (text == 'ineligible') {
							ineligible = new List<Reasons>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								ineligible.add(new Reasons(parser));
							}
						} else if (text == 'user') {
							user = parser.getText();
						} else if (text == 'pending') {
							pending = new List<Reasons>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								pending.add(new Reasons(parser));
							}
						} else if (text == 'warning') {
							warning = new List<Reasons>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								warning.add(new Reasons(parser));
							}
						} else {
							System.debug(LoggingLevel.WARN, 'Product_eligibility consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static jsonSightenQuoteResponse2 parse(String json) {
		return new jsonSightenQuoteResponse2(System.JSON.createParser(json));
	}
}