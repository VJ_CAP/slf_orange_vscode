/**
* Description: Fetch Dealer information logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer                Date                Description
---------------------------------------------------------------------------
Adithya Sarma            05/06/2019           Created
*****************************************************************************************/
public without sharing class GetDealerInfoDataServiceImpl extends RestDataServiceBase{ 
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+GetDealerInfoDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean dealerInfoResp = new UnifiedBean(); 
        dealerInfoResp.error = new list<UnifiedBean.errorWrapper>();
        dealerInfoResp.users = new List<UnifiedBean.UsersWrapper>();
        dealerInfoResp.disclosures = new List<UnifiedBean.DisclosureWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
         list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        { 
            List<user> UserObj = [SELECT Id, Username,LoginCount__c, LastName,Email, Profile.Name, FirstName,isInstallerReportEnabled__c,isNetReportEnabled__c,isRemittanceReportEnabled__c,Is_Home_Remittance_Report_Enabled__c,contact.Account.Installer_Dedicated_Phone_Support__c,
                                   UserRole.Name,Hash_Id__c,contact.Account.Credit_Waterfall__c,
                                   contact.Account.Prequal_Enabled__c,contact.Account.Name,
                                   contact.Account.Risk_Based_Pricing__c, contact.Account.isLoanAgreementSuppressed__c,
                                   contact.Account.Eligible_for_Last_Four_SSN__c,Password__c,
                                   contact.Account.Home_Improvement_Enabled__c,
                                   contact.Account.Solar_Enabled__c,Training_Certification_Completed__c,
                                   contact.Account.Relationship_Manager__r.Name, contact.Account.Relationship_Manager__r.Email,contact.Account.Relationship_Manager__r.Phone, contact.Account.Relationship_Manager__r.FullPhotoUrl,                                  
                                   contact.Account.Inspection_Ever_Enabled__c,
                                   contact.Account.Kitting_Ever_Enabled__c,
                                   contact.Account.Permit_Ever_Enabled__c,
                                   Last_View_State__c,contact.Account.HI_Dealer_Code__c from User where 
                                   contact.Account.HI_Dealer_Code__c =: reqData.dealerCode AND Default_User__c =: true ];
             List<Disclosures__c> disclsrObj = new List<Disclosures__c>();
                            disclsrObj = [Select id, Type__c, Active__c,Disclosure_Text__c 
                                                 from Disclosures__c 
                                                 where Active__c = true and 
                                                 Type__c = 'Dealer'];  
                                                                                          
            
            if(!UserObj.isEmpty()) {
                 String strDisText = '';                                                   
                if(!disclsrObj.isEmpty()){
                    strDisText = disclsrObj.get(0).Disclosure_Text__c.replace('[Contractor\'s Name]',UserObj[0].Contact.Account.Name);
                } 
                if(null != UserObj[0].Password__c && string.isNotBlank(UserObj[0].Password__c))
                {
                    UnifiedBean.UsersWrapper Userinfo = new UnifiedBean.UsersWrapper();
                    Userinfo.hashId = UserObj[0].Hash_Id__c;
                    Userinfo.id = UserObj[0].id;
                    Userinfo.firstName = UserObj[0].FirstName;
                    Userinfo.lastName = UserObj[0].LastName;
                    Userinfo.email = UserObj[0].Email;
                    Userinfo.password = UserObj[0].Password__c;
                    Userinfo.role = UserObj[0].UserRole.Name;
                    Userinfo.profile = UserObj[0].Profile.Name;
                    Userinfo.loginCount = Integer.valueOf(UserObj[0].LoginCount__c);
                    Userinfo.dashboardView = (UserObj[0].Last_View_State__c != null?UserObj[0].Last_View_State__c:'');
                    system.debug('***Userinfo--'+Userinfo);
                    dealerInfoResp.returnCode = '200';
                    dealerInfoResp.users.add(Userinfo); 
                    UnifiedBean.InstallerInfoWrapper respInstallerInfoWrapper = new UnifiedBean.InstallerInfoWrapper();
                    respInstallerInfoWrapper.name = UserObj[0].contact.Account.Name;
                    respInstallerInfoWrapper.isWaterfallEnabled = UserObj[0].contact.Account.Credit_Waterfall__c;
                    respInstallerInfoWrapper.isPrequalEnabled = UserObj[0].contact.Account.Prequal_Enabled__c;
                    respInstallerInfoWrapper.isRBPEnabled = UserObj[0].contact.Account.Risk_Based_Pricing__c;
                    respInstallerInfoWrapper.isLast4SSNEnabled = UserObj[0].contact.Account.Eligible_for_Last_Four_SSN__c;
                    respInstallerInfoWrapper.isLoanAgreementSuppressed = UserObj[0].contact.Account.isLoanAgreementSuppressed__c;
                    respInstallerInfoWrapper.isSolarEnabled = UserObj[0].contact.Account.Solar_Enabled__c; 
                    respInstallerInfoWrapper.isHomeEnabled = UserObj[0].contact.Account.Home_Improvement_Enabled__c; 
                    if(UserObj[0].contact.Account.Solar_Enabled__c){
                        respInstallerInfoWrapper.isInspectionEnabled = UserObj[0].contact.Account.Inspection_Ever_Enabled__c;
                        respInstallerInfoWrapper.isKittingEnabled = UserObj[0].contact.Account.Kitting_Ever_Enabled__c;
                        respInstallerInfoWrapper.isPermitEnabled = UserObj[0].contact.Account.Permit_Ever_Enabled__c;
                    }
                    respInstallerInfoWrapper.supportNumber = UserObj[0].contact.Account.Installer_Dedicated_Phone_Support__c;
                    Userinfo.isTrainingCompleted = UserObj[0].Training_Certification_Completed__c;
                    Userinfo.trainingDocLink = label.Training_Document_Link;
                     
                    if(UserObj[0].contact.Account.Home_Improvement_Enabled__c){
                        Userinfo.homeDocLink1 = label.Home_Document_Link_1;
                        Userinfo.homeDocLink2 = label.Home_Document_Link_2;
                    }
                    Userinfo.isInstallerReportEnabled = UserObj[0].isInstallerReportEnabled__c;
                    Userinfo.isNetReportEnabled = UserObj[0].isNetReportEnabled__c;
                    Userinfo.isRemittanceReportEnabled = UserObj[0].isRemittanceReportEnabled__c;
                    Userinfo.isHomeRemittanceReportEnabled  = UserObj[0].Is_Home_Remittance_Report_Enabled__c; //added By Adithya as per 3443
                    SFSettings__c sfSettings = SFSettings__c.getOrgDefaults();
                    
                    //UnifiedBean.ApplicationInfoWrapper applicationInfoRec = new UnifiedBean.ApplicationInfoWrapper();
                    //applicationInfoRec.mobileAppVersion = sfSettings.Mobile_App_Version__c;
                    //dealerInfoResp.applicationInfo = applicationInfoRec;
                    dealerInfoResp.installerInfo = respInstallerInfoWrapper;
                    UnifiedBean.DisclosureWrapper disclsrWrap = new UnifiedBean.DisclosureWrapper();
                    disclsrWrap.disclosureText = strDisText;
                    dealerInfoResp.disclosures.add(disclsrWrap);
                    iRestRes = (IRestResponse)dealerInfoResp; 
                }else{
                    dealerInfoResp.returnCode = '332';
                    errMsg = ErrorLogUtility.getErrorCodes('332',null);
                }
            }else{
                dealerInfoResp.returnCode = '332';
                errMsg = ErrorLogUtility.getErrorCodes('332',null);
            } 
            if(string.isNotBlank(errMsg)){
                iolist = new list<DataValidator.InputObject>{};
                system.debug('***errMsg**'+errMsg);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                system.debug('***errorMsg**'+errorMsg);
                dealerInfoResp.error.add(errorMsg);
                iRestRes = (IRestResponse)dealerInfoResp;
                return iRestRes;
            }            
            
        }catch(Exception err){
            errMsg = ErrorLogUtility.getErrorCodes('400', null);
            iRestRes = (IRestResponse)dealerInfoResp;
            system.debug('### GetDealerInfoDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('GetDealerInfoDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from transformOutput() of '+GetDealerInfoDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
  
}