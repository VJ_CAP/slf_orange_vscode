global class Batch_ImageUpload implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    private Integer batchNumber { get; set;}
    
    DateTime lastExecutionTime;
    DateTime curBatchStartTime;
    
    public Batch_ImageUpload(Integer batch)
    {
        batchNumber = batch;
    }
    
    public Batch_ImageUpload(DateTime lastTime, DateTime curStartTime)
    {
        batchNumber = null;
        lastExecutionTime = lastTime;
        curBatchStartTime = curStartTime;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String batchNumberClause = '';
        if (batchNumber != null) {
            batchNumberClause = ' and Batch_Number__c >= :batchNumber';
        } 
        
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        String limitClause = '';
        if (settings.MAX_ROWS_IN_BATCH__c != null && String.valueOf(settings.MAX_ROWS_IN_BATCH__c).length() > 0) {
            limitClause = ' LIMIT ' + Integer.valueOf(settings.MAX_ROWS_IN_BATCH__c);
        }
        String query = 'SELECT Id,Upload_Attempts__c,Batch_Number__c,Box_Folder_ID__c,Category__c,Name__c,SF_Record_ID__c,Source_URL__c FROM Box_Upload__c where Uploaded__c  = false AND Manual__c = false ORDER BY LastModifiedDate DESC' + limitClause;  //  AND Previously_Uploaded__c  = false'
        
        
        
        if (settings.Use_Debug_Opportunities__c) { 
            // *** make sure Ids are 18 character
            List<String> strOppIds = settings.Debug_Opportunities__c.split(',');
            List<Id> oppIds = new List<Id>();
            for (String s : strOppIds) oppIds.add(s);
            
            if (oppIds.size() > 0) {    
                query = 'SELECT Id,Upload_Attempts__c,Batch_Number__c,Box_Folder_ID__c,Category__c,Name__c,SF_Record_ID__c,Source_URL__c FROM Box_Upload__c where SF_Record_ID__c in :oppIds';  
            } 
        }
        
        return Database.getQueryLocator(query);
    }

    
    global void execute(Database.BatchableContext BC,List<Box_Upload__c> ul) 
    {
        Set<id> oppIdset = new Set<id>();
        Map<string,SF_Category_Map__c> categoryFolderMappings = SF_Category_Map__c.getAll();
        BoxUtil.checkLimits();
        BoxUtil.abortBatchCheck();
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        //Added by Rajesh for Orange-1045 Starts
        //Added for dynamic query
        String fieldnames='';
        for (SF_Category_Map__c categs : categoryFolderMappings.values())
        {
            if(categs.Last_Run_TS_Field_on_Underwriting_File__c != null){
                if(!fieldnames.contains(categs.Last_Run_TS_Field_on_Underwriting_File__c)){
                    if (fieldnames == '') {
                        fieldnames = categs.Last_Run_TS_Field_on_Underwriting_File__c;
                    } else {
                        fieldnames += ',' + categs.Last_Run_TS_Field_on_Underwriting_File__c;
                    }
                }
            }
        }
        System.debug('### fieldnames'+fieldnames);
        
        for (Box_Upload__c u : ul) 
        {
            if(u.SF_Record_ID__c != null){
                oppIdset.add(u.SF_Record_ID__c);
            }
        }
        
        // Added by Adithya as part of 1930 
        
        String queryString ='SELECT id,Underwriting__r.Opportunity__c,'+fieldnames+' FROM Box_Fields__c where Underwriting__r.Opportunity__c In :oppIdset';
        
        
        /*
        system.debug('### OppIdSet : ' + oppIdset);
        
        String queryString = 'select Id,Project_Status__c,Opportunity__c,M0_Approval_Requested_Date__c,M1_Approval_Requested_Date__c,M2_Approval_Requested_Date__c,Box_Installation_Photos_TS__c,Box_Install_Contracts_TS__c,Box_Loan_Agreements_TS__c,Box_Payment_Election_Form_TS__c,Box_Approval_Letters_TS__c,Box_Final_Designs_TS__c,Box_Government_IDs_TS__c,Box_Utility_Bills_TS__c,Opportunity__r.SyncedQuoteId,Opportunity__r.Product_Loan_Type__c,'+fieldnames+' from Underwriting_File__c where Opportunity__c In :oppIdset';
        
        List<Underwriting_File__c> uwList = database.query(queryString);
        Map<id,Underwriting_File__c> oppUwMap = new Map<id,Underwriting_File__c>();
        for(Underwriting_File__c uwRec :uwList){
            oppUwMap.put(uwRec.Opportunity__c,uwRec);
        }
        */
        List<Box_Fields__c> BoxFldList = database.query(queryString);
        Map<id,Box_Fields__c> oppBoxFldMap = new Map<id,Box_Fields__c>();
        for(Box_Fields__c BoxFldIterate : BoxFldList){
            oppBoxFldMap.put(BoxFldIterate.Underwriting__r.Opportunity__c,BoxFldIterate);
        }
        
        
        
        //Orange-1045 ends
        Map<id,Box_Fields__c> BoxFldMap = new Map<id,Box_Fields__c>();
        for (Box_Upload__c u : ul) 
        {
             BoxUtil.abortBatchCheck();
            try {
                
             BoxUtil.checkLimits();
                
             u.Upload_Attempts__c++;
             u.Upload_Start_Time__c = System.Now();
             System.Debug('Processing batch => ' + u.Batch_Number__c + ' file => ' + u.Name__c + ' Url => ' +  u.Source_URL__c);
             BLOB body = sightenCallout.getResponseAsBlob('GET',u.Source_URL__c,'',settings.SIGHTEN_TOKEN__c,null);
             if (body == null) {
                    throw new BoxUtil.BoxUtilException('got body = null for url ' + u.Source_URL__c);
             }
             System.Debug('Got response of length ' + body.size());
             u.File_Size__c = body.size();
             
             //Added by Rajesh for Orange-1045 Starts
            if(u.Category__c != null){
                
                String ufileLatestTSFieldName = categoryFolderMappings.get(u.Category__c).Last_Run_TS_Field_on_Underwriting_File__c;
                system.debug('### Latest TS field :' + ufileLatestTSFieldName);
                id uwId = oppBoxFldMap.get(u.SF_Record_ID__c).id;
                system.debug('### Underwriting Id: '+ uwId);
                Box_Fields__c BoxFldRec;
                
                if(!BoxFldMap.containsKey(uwId)){
                    BoxFldRec = oppBoxFldMap.get(u.SF_Record_ID__c);
                }else{
                    BoxFldRec = BoxFldMap.get(uwId);
                }
                BoxFldRec.put(ufileLatestTSFieldName,System.Now());
                BoxFldMap.put(BoxFldRec.id,BoxFldRec);
            }
            system.debug('### BoxFldMap: '+ BoxFldMap);
            
            //Orange-1045 Ends
             
             BoxUtil.checkLimits();

             
            if (body.size() < 6000000) {
                 System.Debug('uploading file ' + u.Name__c + ' size =>' + body.size());
                 BoxUtil.uploadBoxFile(u.Box_Folder_ID__c,u.Name__c,'Submitted',body);
                 body = null;
                 u.Notes__c = '';
                 u.Opportunity__c = null;
                 u.Uploaded__c = true;
                 u.Upload_End_Time__c = System.Now();
                 break;
            } else {
                 u.Notes__c = 'file ' + u.Name__c + ' skipped. Body Size ' + body.size() + ' too large';
                 body = null;
                 u.Opportunity__c = u.SF_Record_ID__c;
                 u.Uploaded__c = false;
                 u.Upload_End_Time__c = System.Now();
                 System.Debug('skipping file ' + u.Name__c);
                 break;
            }
            
             
            } catch (Exception e) {
                String msg = e.getMessage();
                if (msg != null)
                    u.Notes__c = msg.Left(255);
                u.Upload_Error__c = e.getLineNumber() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + e.getStackTraceString();
                if (u.Notes__c != null) {
                    if (u.Notes__c.indexOf('Status=Unauthorized') == -1)
                        u.Opportunity__c = u.SF_Record_ID__c;
                    if (u.Notes__c.indexOf('Exceeded maximum time allotted') == -1)
                        u.Opportunity__c = u.SF_Record_ID__c;
                }
                u.Uploaded__c = false;
                break;
            }
        }
       
        try
        {
            if(!BoxFldMap.isEmpty())
                update BoxFldMap.values();
        }
        catch(Exception err)
        {
             system.debug('### Exception in execute() of '+Batch_ImageUpload.class + ': '+ err.getMessage());
             ErrorLogUtility.writeLog(' Batch_ImageUpload.execute()',err.getLineNumber(),'execute()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null)); 
        }
        
        // ** strange issue while deploying to production, breaks Test Class
        if (!Test.IsRunningTest())
         update ul;
    }
    
    global void finish(Database.BatchableContext BC) {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        settings.Site_Batch_Execution_TS__c = curBatchStartTime;
        upsert settings;
    }
}