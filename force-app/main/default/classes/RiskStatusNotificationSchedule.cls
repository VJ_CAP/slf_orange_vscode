global class RiskStatusNotificationSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        RiskStatusNotificationBatch b = new RiskStatusNotificationBatch(); 
        database.executebatch(b);
    }
}