@isTest
private class UploadPaymentsController_Test 
{
    
    @isTest static void test1 () 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();        
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer', M1_Split__c = 40,Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a2;

        Account a3 = new Account (Name = 'TCU', M2_Split__c = 40,Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright planet Solar', Solar_Enabled__c = true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Product_Tier__c = '0');
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;
        
        //UploadPaymentsController.Row rw = new UploadPaymentsController.Row(transactionDate = System.today();total = 500.0;m0=233.0;m1=100.0;m2=50.0;incentive =2342.0);
       
        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
        

        Reconciliations_Management__c rm = new Reconciliations_Management__c ();
        rm.Latest_Date_Processed__c = Date.newInstance(2000,1,1);
        insert rm;
            

        UploadPaymentsController c = new UploadPaymentsController ();

        c.validate();  //  empty input file

        c.inputFileContent = Blob.valueOf('4/2/15,,"$5,000.01","$16,354.77",');

        c.validate();  //  not enough columns

        c.inputFileContent = Blob.valueOf('4/x/15,,"$5,000.01","$16,354.77",,$500.00,,,,' + o1.Id + '\n' + 
                                            '4/2/15,,"$5,x00.01","$16,354.77",,$500.00,,,,,' + o1.Id);

        c.validate();  //  invalid date and amount

        c.inputFileContent = Blob.valueOf('4/1/15,,"$5,000.01","$16,354.77",,$500.00,,,,,invalid');

        c.validate();  //  invalid id
            
        c.inputFileContent = Blob.valueOf('4/1/15,,"$5,000.01","$16,354.77",,$500.00,,,,,' + o1.Id);

        c.validate();  //  good file
        c.save();
        
        
        c.validatePayments();        /*
        Underwriting_Transaction__c ut1 = [SELECT Id, Amount__c 
                                            FROM Underwriting_Transaction__c 
                                            WHERE Type__c = 'Installer M0 Payment Paid' LIMIT 1];

        System.assertEquals (5000.01, ut1.Amount__c);
        */
    }   
}