global class DeleteInPersonAttachmentsBtchScdl implements Database.Batchable<sobject>,Schedulable{
    
    global Set<Id> oppIdSet = new Set<Id>();
    global String query;
    
    global DeleteInPersonAttachmentsBtchScdl(Set<id> idSet){       
        query = 'SELECT Id FROM Attachment Where Name like \'%In Person Loan Agreement%\' and ParentId in (Select Id from opportunity)';
        if(idSet != null && !idSet.isEmpty()){
            oppIdSet.addAll(idSet);
            query += ' and ParentId in :oppIdSet';
        }  
        System.debug('oppIdSet :::'+oppIdSet ); 
        System.debug('query:::'+query);         
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext bc, List<Attachment> attchmntList){
        system.debug('Deleted Records');  
        delete attchmntList;  
    }
    
    global void finish(Database.BatchableContext bc){
      
    }
    
    global void execute(SchedulableContext sc){
        DeleteInPersonAttachmentsBtchScdl delAttachBatch = new DeleteInPersonAttachmentsBtchScdl(null);
        Database.ExecuteBatch(delAttachBatch);     
    }

}