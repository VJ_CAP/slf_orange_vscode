/**
* Description: StatusBean class to marshall and unmarshall request and response for Opportunity object.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public class ObjectRequestWrapper implements IRestRequest,IDataObject,IRestResponse {
   
    public string sobjectLst {get;set;}
    public String action {get;set;}
    public String objectType {get;set;}
    public String serviceName {get;set;}
    
    public ObjectRequestWrapper(){
      
    }
   
}