/**
* Description: Trigger Business logic associated with Stipulation is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sharma          10/05/2017          Created 
    Adithya Sharma          11/01/2018          Removed recursive updates on case object on after update call.  
******************************************************************************************/
public with sharing Class StipulationTriggerHandler
{
    public static boolean isTrgExecuting = true;
    
    /**
    * Constructor to initialize
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public StipulationTriggerHandler(boolean isExecuting){
        //isTrgExecuting = isExecuting;
    }
    
    /**
    * Static Block to stop recursive execution.
    *
    */
    public static boolean runOnce(){
        if(isTrgExecuting){
            isTrgExecuting=false;
            return true;
        }else{
            return isTrgExecuting;
        }
    }
    
    /**
    * Description: OnBeforeInsert
    *
    * @param new Stipulation records.
    */
    public void OnBeforeInsert(List<Stipulation__c> newStipulation){
        try {
            system.debug('### Entered into OnBeforeInsert() of '+ StipulationTriggerHandler.class);
            Set<Id> setUnderWrittingIds = new Set<Id>();
            
            for(Stipulation__c stipIterate : newStipulation){
                setUnderWrittingIds.add(stipIterate.Underwriting__c);
                if(stipIterate.Status__c == 'Completed') {
                    stipIterate.Completed_Date__c = System.Today();
                    stipIterate.Stipulation_Closed_By__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                }
            }
            
            system.debug('### Exit from OnBeforeInsert() of '+ StipulationTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### StipulationTriggerHandler.OnBeforeInsert():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('StipulationTriggerHandler.OnBeforeInsert()',ex.getLineNumber(),'OnBeforeInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    
    /**
    * Description: OnBeforeUpdate
    * 
    * @param new Stipulation records.
    */
    public void OnBeforeUpdate(List<Stipulation__c> newStipulation,Map<Id,Stipulation__c> oldStipulationMap){
        map<Id,Stipulation__c> mapStipulation =  new map<Id,Stipulation__c>();
        system.debug('newStipulation====='+newStipulation);
        try
        {   
           //  if(isTrgExecuting) { Commented for 2622
               // Set<Id> setStipIds = new Set<Id>();
               // for(Stipulation__c objStip : newStipulation){
                 //   if(objStip.Status__c != oldStipulationMap.get(objStip.Id).Status__c && objStip.Status__c == 'Documents Received' && objStip.Ready_for_Review_Date_Time__c == null){
                     //   setStipIds.add(objStip.Id);
                    //}
                //} 
               // if(!setStipIds.isEmpty()){ // Commented for 2622
                 //   mapStipulation = new map<Id,Stipulation__c>([select Id,Underwriting__c,Underwriting__r.M0_Approval_Date__c,Underwriting__r.Opportunity__r.Installer_Account__r.Name,Underwriting__r.Opportunity__r.Installer_Account__r.SSN__c FROM Stipulation__c where id in:setStipIds]);
                //}
                for(Stipulation__c stipIterate : newStipulation){   
                    if(stipIterate.Status__c != oldStipulationMap.get(stipIterate.Id).Status__c ){
                        if(stipIterate.Status__c == 'Completed'){
                            stipIterate.Completed_Date__c = System.Today(); 
                            stipIterate.Stipulation_Closed_By__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                            stipIterate.Completed_Date_Time__c = system.now();
                        }
                        if(stipIterate.Status__c == 'Under Review'){
                            stipIterate.Review_Start_Date_Time__c = null;
                            stipIterate.Ready_for_Review_Date_Time__c = null;
                        }
                        if(stipIterate.Status__c != oldStipulationMap.get(stipIterate.Id).Status__c && stipIterate.Status__c == 'Documents Received' && stipIterate.Ready_for_Review_Date_Time__c == null){
                            stipIterate.Ready_for_Review_Date_Time__c = system.now();
                        }
                        //Commented for 2622
                       // if(!setStipIds.isEmpty() && setStipIds.Contains(stipIterate.id) && mapStipulation != null && mapStipulation.get(stipIterate.Id) != null ){
                        //    stipIterate.Ready_for_Review_Date_Time__c = system.now();
                        //}
                    }
                } 
            //}
            system.debug('### Exit from OnBeforeUpdate() of '+ StipulationTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### StipulationTriggerHandler.OnBeforeUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('StipulationTriggerHandler.OnBeforeUpdate()',ex.getLineNumber(),'OnBeforeUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    /**
    * Description: This method is used to perform after insert related logic.
    *
    * @param newStipulationLst    Get inserted Stipulation__c records.
    */
    public  void onAfterInsert(Map<Id, Stipulation__c> newStipulationMap){
        try{
            system.debug('### Entered into onAfterInsert() of '+ StipulationTriggerHandler.class);            
            set<Id> oppIds = new set<Id>();
            set<Id> ufIds = new set<Id>{};
            List<Task> lstTasks = new List<Task>();
            string trgEvent = 'insert';
            Map<Id, Stipulation__c> stripMap = new Map<Id, Stipulation__c>();//For Status Push
            for(Stipulation__c stipIterate : newStipulationMap.values())
            {
                if(stipIterate.Push_Endpoint__c && stipIterate.Opp_EDW_Originated__c == false)
                {
                    oppIds.add(stipIterate.Opportunity_Id__c);
                    stripMap.put(stipIterate.Id,stipIterate);
                }
                if(null != stipIterate.Underwriting__c)
                    ufIds.add(stipIterate.Underwriting__c);
            }
            
            // Added by Veereandranath Jalla Orange -1516 - End        
            
            if(!oppIds.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
            {
                //Generate JSON Structure and create Status Audit Record
                String jsonNewList = JSON.serialize(stripMap.values());
                SLFUtility.callUnifiedJSON('Stipulation__c','stips',oppIds,jsonNewList,'');
                //SLFUtility.futureBuildStatusUnifiedJSON('Stipulation__c','stips',oppIds,jsonNewList,'');
            }
            
            if(!ufIds.isEmpty())
                StipulationsSupport.processStipulationsForUfs (ufIds);
            
            // isTrgExecuting = false;
            // Added by Veereandranath Jalla Orange -1516 - Start           
            List<Underwriting_File__c> lstUWrittingFiles = [SELECT Id,Name,M0_Ready_for_Review_Date_Time__c,M2_Ready_for_Review_Date_Time__c,Project_Status__c,M1_Ready_for_Review_Date_Time__c,Permit_Ready_for_Review_Date_Time__c,Kitting_Ready_for_Review_Date_Time__c,Inspection_Ready_for_Review_Date_Time__c,CO_Approval_Requested_Date__c,CO_Approval_Date__c,CO_Ready_for_Review_Date_Time__c,(SELECT Id,Status,Type,Review_Date_Time_Complete__c FROM Tasks where Status = 'Open') FROM Underwriting_File__c WHERE Id in:ufIds];
            for(Underwriting_File__c objUW:lstUWrittingFiles){
                if(objUW.CO_Approval_Requested_Date__c != null && objUW.CO_Approval_Date__c == null && objUW.CO_Ready_for_Review_Date_Time__c != null)
                        objUW.CO_Ready_for_Review_Date_Time__c = null;
                else if(objUW.Project_Status__c == 'M2' && objUW.M2_Ready_for_Review_Date_Time__c != null)
                        objUW.M2_Ready_for_Review_Date_Time__c = null;
                else if((objUW.Project_Status__c == 'M0' || objUW.Project_Status__c == 'Pending Loan Docs')&& objUW.M0_Ready_for_Review_Date_Time__c != null)
                        objUW.M0_Ready_for_Review_Date_Time__c = null;
                else if(objUW.Project_Status__c == 'M1' && objUW.M1_Ready_for_Review_Date_Time__c != null)
                        objUW.M1_Ready_for_Review_Date_Time__c = null;
                else if(objUW.Project_Status__c == 'Permit' && objUW.Permit_Ready_for_Review_Date_Time__c != null)
                        objUW.Permit_Ready_for_Review_Date_Time__c = null;
                else if(objUW.Project_Status__c == 'Kitting' && objUW.Kitting_Ready_for_Review_Date_Time__c != null)
                        objUW.Kitting_Ready_for_Review_Date_Time__c = null;
                else if(objUW.Project_Status__c == 'Inspection' && objUW.Inspection_Ready_for_Review_Date_Time__c != null)
                        objUW.Inspection_Ready_for_Review_Date_Time__c = null;
                else if(objUW.CO_Approval_Requested_Date__c != null && objUW.CO_Approval_Date__c == null && objUW.CO_Ready_for_Review_Date_Time__c != null)
                        objUW.CO_Ready_for_Review_Date_Time__c = null;
                    
                //objUW.Review_Status__c = null;
                for(Task objTask:objUW.Tasks){
                    if(objTask.Type == 'M0 Review')
                        objUW.M0_Ready_for_Review_Date_Time__c = null;
                    else if(objTask.Type == 'M1 Review')
                        objUW.M1_Ready_for_Review_Date_Time__c = null;
                    else if(objTask.Type == 'M2 Review')
                        objUW.M2_Ready_for_Review_Date_Time__c = null;
                    else if(objTask.Type == 'Permit Review')
                        objUW.Permit_Ready_for_Review_Date_Time__c = null;
                    else if(objTask.Type == 'Kitting Review')
                        objUW.Kitting_Ready_for_Review_Date_Time__c = null;
                    else if(objTask.Type == 'Inspection Review')
                        objUW.Inspection_Ready_for_Review_Date_Time__c = null;
                    else if(objTask.Type == 'CO Review')
                        objUW.CO_Ready_for_Review_Date_Time__c = null;
                    //Added As part of Orange - 3964 - Start
                    else if(objTask.Type == 'HIDR Review')
                        objUW.HIDR_Ready_for_Review_Date_Time__c = null;  
                    else if(objTask.Type == 'HIF Review')
                        objUW.HIF_Ready_for_Review_Date_Time__c = null;                    
                    //Added As part of Orange - 3964 - End
                    
                    objTask.Status = 'Complete - Stipped';
                    objTask.Review_Date_Time_Complete__c = System.now();
                    lstTasks.add(objTask);
                }
            }
            if(!lstUWrittingFiles.isEmpty()){
                update lstUWrittingFiles;
            }            
            if(!lstTasks.isEmpty()){
                update lstTasks;
            }
           
        }catch(Exception ex)
        {
            system.debug('### StipulationTriggerHandler.onAfterInsert():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('StipulationTriggerHandler.onAfterInsert()',ex.getLineNumber(),'onAfterInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from onAfterInsert() of '+ StipulationTriggerHandler.class);
    }
    
    /**
    * Description: This method is used to perform after update related logic.
    *
    * @param newStipulationMap    Get updated Stipulation__c records.
    * @param oldStipulationMap    Get old Stipulation__c records.
    */
    public void onAfterUpdate(Map<Id, Stipulation__c> newStipulationMap, Map<Id, Stipulation__c > oldStipulationMap){
        try{
            if(isTrgExecuting)
            {
                runOnce();
                system.debug('### Entered into onAfterUpdate() of '+ StipulationTriggerHandler.class);
               // if (TransactionSupport.isProcessed ('StipulationProcessStatusChange')) return;  //  only execute once in a transaction
                set<Id> caseIds = new set<Id>{};
                set<Id> AccIds = new set<Id>{};
                set<Id> ufIds = new set<Id>{};
                list<Stipulation__c> stipsToProcess = new list<Stipulation__c>{};
                set<Id> stipIds = new set<Id>{};
                Set<Id> undrWrtngIdSet = new Set<Id>();
                Map<id,Case> caseMap = new Map<id,Case>();
                
                //string trgEvent = 'update';
                set<Id> oppIds = new set<Id>();
                //Map<Id, Stipulation__c> stpMap = new Map<Id, Stipulation__c>();//Comented by Adithya because we are not reffering this anywhere in our code.
                Map<Id, Stipulation__c> stripMap = new Map<Id, Stipulation__c>();//For Status Push
                Map<string,string> stripAndAccid = new Map<string,string>();
                
                List<Stipulation__c> lstCompletedStips = new List<Stipulation__c>(); //Added by Suresh on 09/08/2018 (Orange-281)
                set<Id> stipUnderReviewIds = new set<Id>{};
                Map<id,set<id>> projectIdMap = new Map<id,set<id>>();//added as part of  1072 by venkat
                string stipulationStatusTxt = '';
                for(Stipulation__c stipIterate : newStipulationMap.values())
                {
                    
                    if(string.isBlank(stipulationStatusTxt))
                    {
                        stipulationStatusTxt = stipIterate.Status__c;
                    }else{
                        if(!stipulationStatusTxt.contains(stipIterate.Status__c))
                        {
                            stipulationStatusTxt = stipulationStatusTxt +';'+stipIterate.Status__c; 
                        }
                    }
                    
                    if(stipIterate.Status__c != oldStipulationMap.get(stipIterate.Id).Status__c)
                        undrWrtngIdSet.add(stipIterate.Underwriting__c);
                    
                    if(stipIterate.Push_Endpoint__c && stipIterate.Opp_EDW_Originated__c == false)
                    {
                        oppIds.add(stipIterate.Opportunity_Id__c);
                        stripMap.put(stipIterate.Id,stipIterate);
                    }
                    
                    //Added by Adithya on 11/09/2018 for (Orange-1466)
                    if (stipIterate.Status__c == 'Documents Received' && stipIterate.Status__c != oldStipulationMap.get(stipIterate.Id).Status__c)  //  Status has changed to Documents Received
                    {
                        Case caseRec = new Case(Id = stipIterate.Case__c);
                        caseRec.Stipulation_Email_Text__c = '';
                        caseRec.Installer_Only_Stipulation_Email_Text__c = '';
                        caseRec.Installer_Stipulation_Text_Populated__c = false;
                        caseRec.Stipulation_Text_Populated__c = false;
                        caseMap.put(caseRec.Id,caseRec);
                    }
                    
                    if (stipIterate.Status__c == 'Completed' && stipIterate.Status__c != oldStipulationMap.get(stipIterate.Id).Status__c)  //  Status has changed to Completed
                    {
                        caseIds.add (stipIterate.Case__c);
                        ufIds.add (stipIterate.Underwriting__c);
                        stipIds.add (stipIterate.Id);
                         stipUnderReviewIds.add(stipIterate.Id);
                        //Added by Suresh on 09/08/2018 (Orange-281)
                        lstCompletedStips.add(stipIterate);
                    }
                    
                    //Added by Suresh on 10/08/2018 (Orange-281)
                    if (stipIterate.Status__c == 'Under Review' && stipIterate.Status__c != oldStipulationMap.get(stipIterate.Id).Status__c)  //  Status has changed to Under Review
                    {
                        caseIds.add (stipIterate.Case__c);
                        stipUnderReviewIds.add(stipIterate.Id);
                    }
                    //End
                    //added as part of  1072 by venkat  - Start
                    if(stipIterate.Opportunity_Id__c != null){
                        set<id> tempSet = new set<id>();
                        tempSet=projectIdMap.containsKey(stipIterate.Opportunity_Id__c)?projectIdMap.get(stipIterate.Opportunity_Id__c):tempSet;
                        tempSet.add(stipIterate.id);
                        projectIdMap.put(stipIterate.Opportunity_Id__c,tempSet);//added as part of  1072 by venkat 
                    }
                    //added as part of  1072 by venkat  - End
                }
                
                //added as part of  1072 by venkat - Start
                if(!projectIdMap.isEmpty()){
                    CreateUnifiedJSON.validateEventChanges(oldStipulationMap,newStipulationMap,'Stipulation__c',projectIdMap);//added as part of  1072 by venkat
                   
                }
                 //added as part of  1072 by venkat - End
                System.debug('### Stipulation record Ids: '+stipUnderReviewIds);
                
                if(!oppIds.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
                {
                    //Generate JSON Structure and create Status Audit Record
                    String jsonNewList = JSON.serialize(stripMap.Values());
                    String jsonOldMap = JSON.serialize(oldStipulationMap);
                    SLFUtility.callUnifiedJSON('Stipulation__c','stips',oppIds,jsonNewList,jsonOldMap);
                }    
               
                System.debug('*** Before Query**');
                List<Stipulation__c> lstCompletedAndUnderReviewStips = [SELECT Id,Suppress_Completed_Email_Alert__c,Name,Case__c,Case__r.Accountid,Underwriting__c, ETC_Notes__c, Status__c,Underwriting__r.Opportunity__r.Account.Owner.Email,Stipulation_Data__r.Name, Stipulation_Data__r.Description__c,Stipulation_Data__r.Suppress_In_Portal__c,Case__r.Completed_Stip_Name_Text__c,Case__r.Under_Review_Stip_Name_Text__c,Suppress_In_Portal__c FROM Stipulation__c WHERE Id IN :stipUnderReviewIds];
                System.debug('### Completed and Under Review Stipulations: '+lstCompletedAndUnderReviewStips);
                
                //Seperate Completed and Under Review stips into different sets
                if(lstCompletedAndUnderReviewStips!=null && !lstCompletedAndUnderReviewStips.isEmpty()){
                    for(Stipulation__c objStips : lstCompletedAndUnderReviewStips){
                        if(objStips.Status__c=='Completed'){
                        System.debug('objStips in first for'+objStips.id);
                            stipsToProcess.add(objStips);

                        }
                    }
                }
                System.debug('### Completed Stipulations: '+stipsToProcess);
               
                //End of (To Support Orange-281) changes
                //  get Cases that need to be updated, with their Stips that are In Progress
                list<Case> casesToUpdate = new list<Case>([SELECT Id, Installer_Only_Stipulation_Email_Text__c,
                                                        Suppress_Completed_Email_Alert__c,Stipulation_Status__c,     
                                                          Installer_Stipulation_Text_Populated__c,Stipulation_Text_Populated__c,
                                                           Stipulation_Email_Text__c,Account_Owner_Email__c,
                                                           (SELECT Id, Status__c, Installer_Only_Email__c, ETC_Notes__c, Stipulation_Data__r.Name,Underwriting__r.Opportunity__r.Account.Owner.Email,
                                                            Stipulation_Data__r.Email_Text__c,Stipulation_Data__r.Suppress_In_Portal__c,Upload_Document__c FROM Stipulations__r
                                                            WHERE Status__c = 'In Progress')
                                                           FROM Case WHERE Id IN :caseIds]);
                //  get the parent UFs for the Stips into a map
                
                map<Id,Id> ufToOppMap = new map<Id,Id>{};
                    
                for (Underwriting_File__c uf : [SELECT Id, Opportunity__c FROM Underwriting_File__c WHERE Id IN :ufIds])
                {
                    ufToOppMap.put (uf.Id, uf.Opportunity__c);
                }
                
                //  get Opportunities that need comments updated
                map<Id,Opportunity> oppsMap = new map<Id,Opportunity>([SELECT Id, Name, Upload_Comment__c, Upload_Comment_Text__c, Upload_Comment_TS__c FROM Opportunity WHERE Id IN :ufToOppMap.values()]);
                //  for each Case, rebuild the email fields from Stips that are In Progress
                for (Case c : casesToUpdate)
                {
                    c.Stipulation_Email_Text__c = '';
                    c.Installer_Only_Stipulation_Email_Text__c = '';
                    c.Installer_Stipulation_Text_Populated__c = false;
                    c.Stipulation_Text_Populated__c = false;
                    c.Stipulation_Status__c = '';
                    StipulationsSupport.buildEmailFields (c, null, c.Stipulations__r);
                    caseMap.put(c.id,c);
                }
                   
                //Added by Suresh on 09/08/2018
                for(Stipulation__c objCompletedStip : lstCompletedAndUnderReviewStips){
                    System.debug('### Tracking - 1: '+objCompletedStip);
                    String description = objCompletedStip.Stipulation_Data__r.Description__c!=null?objCompletedStip.Stipulation_Data__r.Description__c:'';
                    Case objCase = new Case();
                    if(caseMap.containsKey(objCompletedStip.Case__c)){
                        objCase = caseMap.get(objCompletedStip.Case__c);
                    }else{  
                        objCase.id=objCompletedStip.Case__c;
                    }
                    objCase.Account_Owner_Email__c = objCompletedStip.Underwriting__r.Opportunity__r.Account.Owner.Email;
                    if(objCompletedStip.Status__c == 'Completed' && objCompletedStip.Status__c != oldStipulationMap.get(objCompletedStip.Id).Status__c){
                        objCase.Completed_Stip_Name_Text__c = objCase.Completed_Stip_Name_Text__c!=null?objCase.Completed_Stip_Name_Text__c+'\n'+'* '+description:'* '+description;    
                        //objCase.Under_Review_Stip_Name_Text__c = '';
                    }
                    system.debug('****objCompletedStip.Status__c***'+objCompletedStip.Status__c);
                    system.debug('****objCompletedStip.Under_Review_Stip_Name_Text__c***'+objCase.Under_Review_Stip_Name_Text__c);
                    if(objCompletedStip.Status__c =='Under Review' && objCompletedStip.Status__c != oldStipulationMap.get(objCompletedStip.Id).Status__c){
                        objCase.Under_Review_Stip_Name_Text__c = objCase.Under_Review_Stip_Name_Text__c!=null?objCase.Under_Review_Stip_Name_Text__c+'\n'+'* '+description:'* '+description;
                        
                        if(objCompletedStip.ETC_Notes__c != '' && objCompletedStip.ETC_Notes__c != null)
                        {
                            objCase.ETC_Notes__c = objCase.ETC_Notes__c!=null?objCase.ETC_Notes__c+'\n'+'* '+objCompletedStip.ETC_Notes__c:'* '+objCompletedStip.ETC_Notes__c;
                        }
                       // objCase.Completed_Stip_Name_Text__c = '';
                    }
                    system.debug('**22**objCompletedStip.Under_Review_Stip_Name_Text__c***'+objCase.Under_Review_Stip_Name_Text__c);
                    system.debug('**22**objCompletedStip.Completed_Stip_Name_Text__c***'+objCase.Completed_Stip_Name_Text__c);
                    //Added by Adithya to fix multiple email alerts issue
                    //Start
                    if(string.isNotBlank(stipulationStatusTxt))
                    {
                        objCase.Stipulation_Status__c  = stipulationStatusTxt;
                    }
                    //End
                    //Added by Adithya as part of ORANGE-14701 on 11/25/2019
                    objCase.Suppress_Completed_Email_Alert__c = objCompletedStip.Suppress_Completed_Email_Alert__c;
                    caseMap.put(objCase.id,objCase);
                }
                if(caseMap!=null && !caseMap.isEmpty()){
                    try{                    
                        update caseMap.values();
                    }catch(Exception ex){}
                }
                //End
                
                for(Stipulation__c stip : stipsToProcess){
                    stripAndAccid.put(stip.id,stip.Case__r.Accountid);
                    AccIds.add(stip.Case__r.Accountid);
                }
          
                List<ID> finalStips = new List<ID>();
                if(stipIds!= null && stipIds.size()>0){
                    finalStips = caseCompletedStatusonAccount(stipIds,AccIds,stripAndAccid);
                }
                system.debug('###finalStips '+finalStips);
                Set<Id> oppIdSet = new Set<Id>();
                String message='';
                // for each Stip, add comments on the appropriate Opportunity
                System.debug('### stipsToProcess:::'+stipsToProcess);
                System.debug('### finalStips :::'+finalStips);
                for (Stipulation__c s : stipsToProcess) 
                {
                    Opportunity o = oppsMap.get (ufToOppMap.get(s.Underwriting__c));
                    if(s.Stipulation_Data__r.Suppress_In_Portal__c == false)    //Added By Kalyani on 26/08/2019 as part of Orange-8380
                     message = '\n' +'********************'+s.Name + ' (' + s.Stipulation_Data__r.Name + ')' +
                        ' was resolved on ' + System.now().format('MM/dd/yyyy') + '. File is under review';
                    
                    if(o!=null){
                        if (o.Upload_Comment_Text__c == null) o.Upload_Comment_Text__c = '';
                        
                        o.Upload_Comment_Text__c += message;
                        o.Upload_Comment__c = true;
                        o.Upload_Comment_TS__c = System.now();
                        if(finalStips != null && finalStips.size()>0){
                            if(finalStips.contains(s.id)){
                             //   String msg = '\n'+'********************All stipulations clear as of '+System.now().format('MM/dd/yyyy'); 
                             //   o.Upload_Comment_Text__c +=msg;
                                oppIdSet.add(o.Id);
                            }
                        }
                    }
                }
                System.debug('### oppIdSet:::'+oppIdSet);
                if(!oppIdSet.isEmpty()){
                    for(Id oppId : oppIdSet){
                        Opportunity o = oppsMap.get (oppId);
                        if(o!=null){
                            if (o.Upload_Comment_Text__c == null) o.Upload_Comment_Text__c = '';
                            String msg = '\n'+'********************All stipulations clear as of '+System.now().format('MM/dd/yyyy'); 
                            o.Upload_Comment_Text__c +=msg;
                            System.debug('all cleared opty'+oppId);
                        }
                    }
                } 
                System.debug('### oppsMap.values():::'+oppsMap.values());
                List<Underwriting_File__c> uwUpdateList = new List<Underwriting_File__c>();//Added for Orange-1516
                if(!undrWrtngIdSet.isEmpty()){
                    
                    List<Underwriting_File__c> underWrtngList = new List<Underwriting_File__c>();
                    
                    underWrtngList = [Select id, Opportunity__c, (Select Id,Stipulation_Data__r.Name,Stipulation_Data__r.Review_Classification__c, Status__c,Installer_Only_Email__c,Stipulation_Data__r.Does_not_Block_M0__c, Stipulation_Data__r.Does_not_Block_M1__c, Stipulation_Data__r.Does_not_Block_M2__c , Stipulation_Data__r.Suppress_In_Portal__c from Stipulations__r) FROM Underwriting_File__c where id IN :undrWrtngIdSet];
                    
                    for(Underwriting_File__c uwfObj : underWrtngList){
                        Integer openStipCount = 0;
                        Integer m0OpenStipCount = 0;//Added By Deepika on 29/01/2019 as part of Orange-2081
                        Integer m1OpenStipCount = 0;//Added By Deepika on 29/01/2019 as part of Orange-2081
                        Integer m2OpenStipCount = 0;
                        Integer openStipInstallerCount = 0; //Added by venkat on 10/25/2018 as part of ORANGE-1392
                        Integer openStipCustomerCount = 0;//Added by venkat on 10/25/2018 as part of ORANGE-1392
                        Integer openEQPStipCount = 0;
                        Integer openINVStipCount = 0;
                        Opportunity oppObj = new Opportunity();
                        if(oppsMap.containsKey(uwfObj.Opportunity__c))
                            oppObj = oppsMap.get(uwfObj.Opportunity__c);
                        else
                            oppObj = new Opportunity(id=uwfObj.Opportunity__c);
                            
                        if(!uwfObj.Stipulations__r.isEmpty()){
                            for(Stipulation__c sObj : uwfObj.Stipulations__r){                            
                                if(sObj.Status__c != 'Completed'){ // Added as Part of 2040
                                    //if not completed then it means it is an open stip
                                    if(!sObj.Stipulation_Data__r.Suppress_In_Portal__c ){
                                        openStipCount += 1;  
                                    }
                                    //Added By Deepika on 29/01/2019 as part of Orange-2081 - Start
                                    if(!sObj.Stipulation_Data__r.Does_not_Block_M0__c)
                                        m0OpenStipCount += 1;
                                    if(!sObj.Stipulation_Data__r.Does_not_Block_M1__c)
                                        m1OpenStipCount += 1;
                                    if(!sObj.Stipulation_Data__r.Does_not_Block_M2__c)
                                        m2OpenStipCount += 1;
                                    //Added By Deepika on 29/01/2019 as part of Orange-2081 - End 
                                    if(sObj.Stipulation_Data__r.Name == 'EQP')
                                        openEQPStipCount += 1;
                                    if(sObj.Stipulation_Data__r.Name == 'INV')
                                        openINVStipCount += 1;
                                    //Added by venkat on 10/25/2018 as part of ORANGE-1392 -Start
                                    if(sObj.Installer_Only_Email__c){
                                        openStipInstallerCount += 1;
                                    }else{
                                        openStipCustomerCount += 1;
                                    }
                                    //Added by venkat on 10/25/2018 as part of ORANGE-1392 - End
                                }             
                            }
                            //Added By Deepika on 29/01/2019 as part of Orange-2081 - Start
                            if(m0OpenStipCount > 0){
                                oppObj.Has_Open_Stip_M0__c = true;
                            }
                            else{
                                oppObj.Has_Open_Stip_M0__c = false;
                            }  
                            if(m1OpenStipCount > 0){
                                oppObj.Has_Open_Stip_M1__c = true;
                            }
                            else{
                                oppObj.Has_Open_Stip_M1__c = false;
                            }
                            if(m2OpenStipCount > 0){
                                oppObj.Has_Open_Stip_M2__c = true;
                            }
                            else{
                                oppObj.Has_Open_Stip_M2__c = false;
                            } 
                             if(openEQPStipCount > 0){
                                oppObj.Has_Open_EQP_Stip__c = true;
                            }
                            else{
                                oppObj.Has_Open_EQP_Stip__c = false;
                            } if(openINVStipCount > 0){
                                oppObj.Has_Open_INV_Stip__c = true;
                            }
                            else{
                                oppObj.Has_Open_INV_Stip__c = false;
                            }                           
                            //Added By Deepika on 29/01/2019 as part of Orange-2081 - End 
                        }
                        
                        
                        if(openStipCount > 0  ){
                            oppObj.Has_Open_Stip__c = 'True';
                        }
                        else{
                            //uwUpdateList.add(uwfObj);//Added for Orange-1516 
                            oppObj.Has_Open_Stip__c = 'False';
                        }
                        
                        uwUpdateList.add(uwfObj);//Added for Orange-1516
                        
                        //Added by venkat on 10/25/2018 as part of ORANGE-1392 -Start
                        if(openStipInstallerCount > 0){
                            oppObj.Has_Open_Installer_Only_Stip__c = 'True';
                        }else{
                            oppObj.Has_Open_Installer_Only_Stip__c = 'False';
                        }
                        if(openStipCustomerCount > 0){
                            oppObj.Has_Open_Customer_and_Installer_Type_Sti__c = 'True';
                        }else{
                            oppObj.Has_Open_Customer_and_Installer_Type_Sti__c = 'False';
                        }
                        //Added by venkat on 10/25/2018 as part of ORANGE-1392 - End
                        if(!oppsMap.containsKey(uwfObj.Opportunity__c))
                            oppsMap.put(oppObj.Id,oppObj);
                    }
                }
                update oppsMap.values();
                //Added for Orange-1516
                if(!uwUpdateList.isEmpty())
                    update uwUpdateList;

                //isTrgExecuting = false;
                
            }  
        }catch(Exception ex)
        {
            system.debug('### StipulationTriggerHandler.onAfterUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('StipulationTriggerHandler.onAfterUpdate()',ex.getLineNumber(),'onAfterUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from onAfterUpdate() of '+ StipulationTriggerHandler.class);
    }
    
    /*
    *This method will be used to check all cases status completed or not
    * added as part of Orange-62
    */
    public List<ID> caseCompletedStatusonAccount(set<ID> stipIds,Set<ID> Accids,map<string,string> stripAndAccid){
        
        Set<string> AccidsHasPendingStip = new set<string>();
        boolean AllCasesClosed = true;
        list<ID> finalStipids = new List<ID>();
        map<string,List<Stipulation__c>> AccidAndStiplst = new map<string,List<Stipulation__c>>();
        map<string,List<case>> mapAccAndlstCase = new map<string,List<case>>();
        map<string,Stipulation__c> CaseidAndStip = new map<string,Stipulation__c>();
        System.debug('###Accids---'+Accids);
        System.debug('###stripAndAccid---'+stripAndAccid);
        Set<string> caseids = new set<string>();
        if(Accids != null && Accids.size()>0){
            List<Case> lstcase;
            for(Case casobj : [select id,Status,Accountid from case where Accountid IN:Accids]){
                caseids.add(casobj.id);
            }
        }
        if(caseids != null && caseids.size()>0){
            for(Stipulation__c stip : [select id,status__c,case__r.Accountid from Stipulation__c where case__c IN:caseids]){
                system.debug('###stip '+stip);
                if(stip.Status__c != 'Completed'){
                    AccidsHasPendingStip.add(stip.case__r.Accountid);
                }
            }
        }
        system.debug('###AccidsHasPendingStip '+AccidsHasPendingStip);
        system.debug('###CaseidAndStip '+CaseidAndStip);
        for(string stipidval :stipIds){
            if(stripAndAccid != null && stripAndAccid.get(stipidval)!= null){
                string Accid = stripAndAccid.get(stipidval);
                system.debug('###Accid :'+Accid);
                if(!AccidsHasPendingStip.contains(Accid)){
                    finalStipids.add(stipidval);
                }
            }
        }
        system.debug('###finalStipids '+finalStipids);
        if(finalStipids != null && finalStipids.size()>0)
        return finalStipids;

        return null;

    }
  
}