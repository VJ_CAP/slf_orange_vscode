/**
* Description: Genrate Dealer Code logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer                Date                Description
---------------------------------------------------------------------------
Adithya Sarma            05/09/2019           Created
*******************************************************************************/
public with sharing class AccountDealerCodeController {
    public Id accId{get;set;} //To store account Id.
    private ApexPages.StandardController stdController; //To store account StandardController instance.
    public Boolean shouldRedirect {public get; private set;} //Used to refresh the VF page.
    
    /**
    * Description: StandardController.
    *
    */
    public AccountDealerCodeController (ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.accId = stdController.getRecord().Id;
        shouldRedirect = false;
    } 
    
    /**
    * Description: Ganerate random 6 digit number and update it in HI_Dealer_Code__c field on Account object.
    *
    */
    public PageReference getDealerCode(){
        system.debug('### Entered into getDealerCode() of  '+AccountDealerCodeController.class);
        String dealerCode = getindex();
        system.debug('***In getDealerCode***'+dealerCode);
        system.debug('***In getDealerCode***'+dealerCode); 
        if(null != dealerCode){
            try{
                Account accRec = [select id,HI_Dealer_Code__c from Account where id =: accId];
                system.debug('***In accRec***'+accRec);
                if(string.isBlank(accRec.HI_Dealer_Code__c))
                {
                    Account acc = new Account(id = accRec.Id);
                    acc.HI_Dealer_Code__c = dealerCode;
                    update acc;
                    system.debug('***In acc*In If**'+acc);
                    shouldRedirect = true;
                }
            }catch(Exception err){
                ErrorLogUtility.writeLog('AccountDealerCodeController.getDealerCode()',err.getLineNumber(),'getDealerCode()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            }
        }
        system.debug('### Exit from getDealerCode() of '+AccountDealerCodeController.class);        
        return null;
    }
    
    /**
    * Description: Ganerate random 6 digit number.
    */
    public String getindex(){
        system.debug('### Entered into getindex() of '+AccountDealerCodeController.class);
        
        String dealerCode = String.ValueOf(Math.round((Math.random() * (900000) + 100000)));
        system.debug('***dealerCode***'+dealerCode);
        List<Account> accLst = [SELECT Id, HI_Dealer_Code__c FROM Account where HI_Dealer_Code__c =: dealerCode];
        system.debug('***accLst***'+accLst);
        if(null != accLst && !accLst.isEmpty()){
            getindex();
        }else{
            return dealerCode;
        }
        system.debug('### Exit from getindex() of '+AccountDealerCodeController.class);        
        return null;
    }
}