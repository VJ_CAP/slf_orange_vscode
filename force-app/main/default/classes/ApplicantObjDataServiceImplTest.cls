@isTest
public class ApplicantObjDataServiceImplTest {
    @testSetup private static void createTestData(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgUnderwritingflag = new TriggerFlags__c();
        trgUnderwritingflag.Name ='Underwriting_File__c';
        trgUnderwritingflag.isActive__c =true;
        trgLst.add(trgUnderwritingflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        Account acc1 = new Account();
        acc1.name = 'Test Account1';
        acc1.Installer_Email__c = 'test1@gmail.com';
        acc1.Inspection_Enabled__c = true;
        acc1.Kitting_Enabled__c = true;
        acc1.Permit_Enabled__c = true;
        acc1.Kitting_Split__c = 10;
        acc1.Permit_Split__c = 10;
        acc1.Inspection_Split__c =10 ;
        acc1.M0_Split__c=20;
        acc1.M1_Split__c=20;
        acc1.M2_Split__c=30;
        acc1.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc1.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.BillingStateCode = 'CA';
        acc1.FNI_Domain_Code__c = 'TCU';
        acc1.Installer_Legal_Name__c='Bright planet Solar';
        acc1.Solar_Enabled__c = true;
        //insert acc1;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c=2.99;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        insert prod;
        
        //Insert opportunity record
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = acc.id;
        personOpp.Installer_Account__c = acc.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Install_Street__c ='Street';
        personOpp.Install_City__c ='InCity';
        personOpp.Combined_Loan_Amount__c =10000;
        personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
        //opp.Approved_LT_Facility__c ='TCU';
        personOpp.SLF_Product__c = prod.id;            
        insert personOpp;
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Street__c ='Street';
        opp.Install_City__c ='InCity';
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'TCU||123';
        //opp.Approved_LT_Facility__c = 'TCU';
        opp.SLF_Product__c = prod.id;
        opp.Opportunity__c = personOpp.Id; 
        opp.Block_Draws__c = false;
        opp.Has_Open_Stip__c = 'False';
        opp.All_HI_Required_Documents__c=true;
        opp.Max_Draw_Count__c=3;
        opp.ProductTier__c = '0';         
        opp.Type_of_Residence__c  = 'Own';
        opp.language__c = 'Spanish';
        opp.Project_Category__c = 'Solar';
        opp.EDW_Originated__c = true;
        opp.Co_Applicant__c = acc.id;
        insert opp;
        
        List<Underwriting_File__c> undList = new List<Underwriting_File__c>();
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = opp.id;
        undStpbt.Approved_for_Payments__c = true;
        undStpbt.Contact_Email__c = 'testund@gmail.com';
        undList.add(undStpbt);
        insert undList;
        
    }
    private testMethod static void applicantObjectAPITest(){
        Test.startTest();
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        
        system.debug('***Account Record***'+acc);
        
        Contact con = new Contact(LastName ='testCon',FirstName = 'SLF',AccountId = acc.Id);
        insert con;
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,EDW_Originated__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c,Co_Applicant__c
                           from opportunity where accountid =: acc.id AND  Co_Applicant__c =: acc.id LIMIT 1];
        System.debug('***Opp Record***'+opp);                            
        Underwriting_File__c undFile = [SELECT id,Opportunity__c,Approved_for_Payments__c FROM Underwriting_File__c where Opportunity__r.id =: opp.id limit 1];
        system.debug('***Und File Record***'+undFile);
            
            List<Email_Validation_Keywords__c> emailkeywordsList = new List<Email_Validation_Keywords__c>();
            Email_Validation_Keywords__c emailKeys = new Email_Validation_Keywords__c();
            emailKeys.name = 'Solar';
            emailkeywordsList.add(emailKeys);
            insert emailkeywordsList;
            
            List<Email_Domain_Validation_List__c> emailDomList = new List<Email_Domain_Validation_List__c>();
            Email_Domain_Validation_List__c emailDomain = new Email_Domain_Validation_List__c();
            emailDomain.name = 'yahoo.com';
            emailDomList.add(emailDomain);
            insert emailDomList;   
            
            
            
            Map<String, String> applicantMap = new Map<String, String>();
            string applicantReq;
          
            
           applicantReq = '{"projects":[{"id":"'+opp.id+'","applicants":[{"id":"'+acc.id+'","email":"kalyani.konakalla@capgemini.com","phone":"4153289091","isEmailSuspicious":null,"isCustomerEmail":false}]}]}';
           MapWebServiceURI(applicantMap,applicantReq,'applicant');

         
            //valid scenario
             applicantReq = '{"projects":[{"applicants":[{"id":"'+acc.id+'","email":"kalyani.konakalla@capgemini.com","phone":"4153289091","isEmailSuspicious":null,"isCustomerEmail":false}]}]}';
             MapWebServiceURI(applicantMap,applicantReq,'applicant');
            
             Test.stopTest();
            
        
    } 
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
    
}