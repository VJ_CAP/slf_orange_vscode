/******************************************************************************************
* Description: Test class to cover CreateUserDataServiceImpl class
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri         09/17/2019                  Created
******************************************************************************************/
@isTest
public class CreateUserDataServiceImplTest {    
    
    @testsetup static void createtestdata(){
        //SLFUtilityDataSetup.initData();
       testDataInsert();
        
    }
    private static void testDataInsert(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.Credit_Run__c = true;
        acc.Default_FNI_Loan_Amount__c = 25000;
        acc.Risk_Based_Pricing__c = true;
        acc.Credit_Waterfall__c = true;
        acc.Eligible_for_Last_Four_SSN__c = true;
        acc.Phone = '9874563210';
        acc.Push_Endpoint__c = 'www.google.com';
        acc.Subscribe_to_Push_Updates__c = true;
        acc.M1_Approval_Days__c = 90;
        acc.M2_Approval_Days__c = 90;
        insert acc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        SFSettings__c sfsettings = new SFSettings__c();
        sfsettings.name = 'test';
        insert sfsettings;
    }
    /**
*
* Description: This method is used to cover CreateUserService
*
*/
    private testMethod static void createUserServiceTest(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contactid, contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =:'testc@mail.com'];
        
        Account acc = [SELECT Id,FNI_Domain_Code__c FROM Account WHERE Name = 'Test Account' LIMIT 1];        
        
        Contact conUsr = [select id, firstname,lastname from contact where LastName ='Financial' limit 1];
               
        System.runAs(loggedInUsr[0])
        { 
            Test.startTest();           
            string createUserJsonreqRole;
            Map<String, String> createUserMap= new Map<String, String>();          
            
            createUserJsonreqRole = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"testNewR@test.com","phone":"9263693291","isActive":true,"title":"sales manager - all reps","role":"Partner Manager","reportsTo":"","selectedQuestionsAndAnswers": [{"question": "What is your grandmother\'s (on your mother\'s side) maiden name?","answer": "test"},{"question": "What was the name of the hospital where you were born?","answer": "test"},{"question": "What was the name of the company where you had your first job?","answer": "test"}]}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            createUserJsonreqRole = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"testNewRV@test.com","phone":"9263693291","isActive":true,"title":"sales representative ","role":"Partner User","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            createUserJsonreqRole = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"testNew@test.com","phone":"9263693291","isActive":true,"title":"executive","role":"Partner Executive","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');            
            
            createUserJsonreqRole = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"executive","role":"Partner Executive","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            createUserJsonreqRole = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            createUserJsonreqRole = '{"users":[{"id":"","firstName":"Test","lastName":"test","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"executive","role":"Top Role","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            createUserJsonreqRole = '{"users":[{"firstName":"","lastName":"","email":"","phone":"","isActive":false,"title":"executive","role":"Top Role","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            createUserJsonreqRole = '{"users":[{"id":"","firstName":"Test","lastName":"test","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"Top Role","reportsTo":"1233"}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');

            createUserJsonreqRole = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"test","email":"testc1@mail.com","phone":"9263693291","isActive":true,"title":"TEst New","role":"Top Role","reportsTo":"'+conUsr.id+'"}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');

            createUserJsonreqRole = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"test","email":"testc1@mail.com","phone":"9263693291","isActive":true,"title":"TEst New","role":"Top Role","reportsTo":"'+loggedInUsr[0].contactid+'"}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');

            createUserJsonreqRole = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"test","email":"testNewRV@test.com","phone":"9263693291","isActive":true,"title":"TEst New","role":"Top Role","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');

            createUserJsonreqRole = '{"users":[{"id":"fsdfwer33342423","firstName":"Test","lastName":"test","email":"testNewRV@test.com","phone":"9263693291","isActive":true,"title":"TEst New","role":"Top Role","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');

            createUserJsonreqRole = '{"users":[{"id":"","firstName":"Test","lastName":"","email":"testNewRV@test.com","phone":"9263693291","isActive":true,"title":"TEst New","role":"Top Role","reportsTo":""}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            createUserJsonreqRole = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","isFrozen":false,"dashboardView":"list","isAchAcknowledged ":true,"itcStepDownAcknowledged":true,"isTrainingCompleted":true}]}';
            MapWebServiceURI(createUserMap,createUserJsonreqRole,'createuser');
            
            //To cover exception block
            CreateUserServiceImpl createUserServiceC = new CreateUserServiceImpl();
            createUserServiceC.fetchData(null);
            
            Test.stopTest();
            
        }         
    }   
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}