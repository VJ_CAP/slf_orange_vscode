/**
* Description: User validation logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Raviteja Sajja          10/18/2017          Created
******************************************************************************************/
public with sharing class ValidateUserServiceImpl extends RESTServiceBase{  
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back with data and response status code.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('###Entered into fetchData() of ' + ValidateUserServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        ValidateUserDataServiceImpl pricDataSrvImpl = new ValidateUserDataServiceImpl (); 

        try{
            system.debug('****rw.reqDataStr***'+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            res.response = pricDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
           system.debug('### ValidateUserServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' ValidateUserServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + ValidateUserServiceImpl.class);
        return res.response ;
    }
}