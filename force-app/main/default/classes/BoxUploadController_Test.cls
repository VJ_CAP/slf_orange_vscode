@isTest
private class BoxUploadController_Test
{

    static testMethod void test1 () 
    {
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgfundingDataflag = new TriggerFlags__c();
        trgfundingDataflag.Name ='Funding_Data__c';
        trgfundingDataflag.isActive__c =true;
        trgLst.add(trgfundingDataflag);
        
        insert trgLst;
        
        Account a1 = new Account (Name = 'Test Account 1',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true);
        insert a1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', AccountId = a1.Id, StageName = 'Test', CloseDate = System.today());
        insert o1;

        box__FRUP__c frup1 = new box__FRUP__c (box__Object_Name__c = 'Opportunity', box__Record_ID__c = o1.Id);
        insert frup1;

        ApexPages.currentPage().getParameters().put('id', o1.Id);

        BoxUploadController c = new BoxUploadController ();

        String accessToken = c.accessToken;

        c.oppId = o1.Id;
        c.oppName = 'Test Opp 1';
        c.usr = [SELECT Id FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1];

        Test.startTest();

        BoxUploadController.displayTree (accessToken, o1.Id);
        BoxUploadController.ensureFRUPEntryExists ('Test', o1.Id);
        BoxUploadController.getChildFolderId (o1.Id, o1.Name, 'Test', accessToken) ;
        BoxUploadController.getOppFolderID (o1.Id);
        BoxUploadController.getSubFolderId ('test', 'test');
        BoxUploadController.updateUnderwritingFile (o1.Id, 'Test', '123', accessToken);
        c.getUploadFolderList ();
//        c.getPageUrl ();
        c.getHasToken ();
        
        
        BoxUploadController.OAuthResult oauth = new BoxUploadController.OAuthResult();
        oauth.access_token = 'xxx';
        oauth.refresh_token = 'xxx';
        oauth.expires_in = 2000;
        BoxUploadController.createFolderResult fldr = new BoxUploadController.createFolderResult();
        fldr.token = 'hhh';
        fldr.folderId = 'foldrId';
        c.uploadFolder = 'dd';
        try{
            c.getPageUrl();
        }catch(Exception ex){}
        Test.stopTest();
    }

    static testMethod void test2 () 
    {
        ApexPages.currentPage().getParameters().put('id', null);

        try
        {
            BoxUploadController c2 = new BoxUploadController ();
            String accessToken = c2.accessToken;
            BoxUploadController.displayTree (accessToken, null);
        }
        catch (Exception e) {}
    }

    static testMethod void test3 () 
    {
        ApexPages.currentPage().getParameters().put('id', '006123123123123');
        
        try
        {
            BoxUploadController c3 = new BoxUploadController ();
            String accessToken = c3.accessToken;
            BoxUploadController.displayTree (accessToken, '006123123123123');            
        }
        catch (Exception e) {}
        
    }
}