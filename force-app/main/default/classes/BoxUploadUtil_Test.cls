@isTest
private class BoxUploadUtil_Test
{

    static testMethod void test1 () 
    {
        SF_Category_Map__c sf = new SF_Category_Map__c(Name='AHJ Approval',
                                                       Name__c='AHJ Approval',
                                                       Folder_Name__c='Other - Not Required',
                                                       Ignore__c=false,
                                                       Last_Run_TS_Field_on_Underwriting_File__c='LT_Loan_Agreement_Received__c',
                                                       Folder_Id_Field_on_Underwriting__c='boxId_Communications__c',
                                                       TS_Field_On_Underwriting__c='LT_Loan_Agreement_Received__c');
        insert sf;
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        insert trgLst;
        
        Account a1 = new Account (Name = 'Test Account 1',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true);
        insert a1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', AccountId = a1.Id, StageName = 'Test', CloseDate = System.today());
        insert o1;

        box__FRUP__c frup1 = new box__FRUP__c (box__Object_Name__c = 'Opportunity', box__Record_ID__c = o1.Id);
        insert frup1;

        ApexPages.currentPage().getParameters().put('id', o1.Id);

        BoxUploadController c = new BoxUploadController ();

        String accessToken = c.accessToken;

        c.oppId = o1.Id;
        c.oppName = 'Test Opp 1';
        c.usr = [SELECT Id FROM User WHERE Id = :Userinfo.getUserId() LIMIT 1];

        Test.startTest();
        
        BoxUploadUtil.SiteDownloadWrapper sdwObj = new BoxUploadUtil.SiteDownloadWrapper();
        sdwObj.site_download = null;
        sdwObj.attachment = null;
        
        SFSettings__c sfs = new SFSettings__c(Root_Folder_ID__c = '8437121497');
        insert sfs;
        
        String oppRootFolderID = SFSettings__c.getOrgDefaults().Root_Folder_ID__c;
        try{
            BoxUploadUtil.getChildFolderID(o1.Name, 'Test', accessToken);
        }catch(Exception e){}
        BoxUploadUtil.getChildFolderID(o1.Id, o1.Name, 'Test', accessToken);        
        BoxUploadUtil.getChildFolderIDToolkit(o1.Id, o1.Name, 'Test', accessToken);
        BoxUploadUtil.getChildFolderIDToolkit(o1.Id, '8437121497', 'Test', accessToken);        
        BoxUploadUtil.getOppFolderID(o1.Id);       
        String boxId = BoxUploadUtil.getSubFolderID('test', 'test'); 
        try{       
            BoxUploadUtil.getFolderSharedLinkPortal('Test',accessToken);
        }catch(Exception e){}
        try{ 
            BoxUploadUtil.getFolderSharedLink(boxId);
        }catch(Exception e){}
        BoxUploadUtil.getBoxFolderId(frup1.Id);
        BoxUploadUtil.getBoxFolderId(o1.Id);
        BoxUploadUtil.getCategoryFolders();
        BoxUploadUtil.queryCategoryMappingFieldsFromUnderwriting(o1.Id);
        string tempString = 'TheBlogReaders';
        Blob tempBlob = Blob.valueOf(tempString);
        BoxUploadUtil.generateSHA1Digest(tempBlob);
        try{
            BoxUploadUtil.uploadNewVersionOfFile('test', 'test',tempBlob);
        }catch(Exception e){}
        Test.stopTest();
    }
}