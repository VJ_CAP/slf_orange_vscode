@isTest
public class QuickQuoteServiceImplTest {
    @testSetup private static void createTestData(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        List<Product__c> prodLst = new List<Product__c>();
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c=2.99;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        prodLst.add(prod);
        
        Product__c prod1 = new Product__c();
        prod1.Installer_Account__c =acc.id;
        prod1.Long_Term_Facility_Lookup__c =acc.id;
        prod1.Name='testprod1';
        prod1.FNI_Min_Response_Code__c=8;
        prod1.Product_Display_Name__c='test1';
        prod1.Qualification_Message__c ='testmsg1';
        prod1.Product_Loan_Type__c='Solar Interest Only';
        prod1.State_Code__c ='CA';
        prod1.Long_Term_Facility__c = 'TCU';
        prod1.APR__c = 3.99;
        prod1.ACH__c = true;
        prod1.NonACH_APR__c=3.99;
        prod1.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod1.LT_Max_Loan_Amount__c =1000;
        prod1.Term_mo__c = 144;
        prod1.Is_Active__c= true;
        prod1.Product_Tier__c = '0';
        prodLst.add(prod1);
        
        insert prodLst;
        
    }
    private testMethod static void QuickQuotev2Test(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
       
        List<Product__c> products = [select Id,Name from Product__c];
        
        System.runAs(loggedInUsr[0])
        {
            Map<String, String> quickQuotev2Map = new Map<String, String>();
            
            String quickQuotev2 ;
            Test.startTest();
            
            quickQuotev2 = '';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
            quickQuotev2 = '{"products": []}';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
            quickQuotev2 = '{"products": [{"term": 120,  "stateName": "California",  "isACH": true,"alternatePaydown":22.5,"loanAmount":25000}]}';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
            quickQuotev2 = '{"products": [{"term": 144,  "stateName": "Florida", "isACH": true, "projectCategory":"Home"}]}';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
            quickQuotev2 = '{"products": [{"stateName": "California" }]}';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
            quickQuotev2 = '{"products": ["stateName": "California" ]}';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
      /*      for(Product__c product : products){
                product.Internal_Use_Only__c = false;
                product.Is_Active__c=true;
                product.Short_Term_Facility__c=null;
                update product;
            }*/
            
            quickQuotev2 = '{"products": [{"stateName": "California" }]}';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
            quickQuotev2 = '{"products": [{"term":144,"apr":3.99,"isACH": true, "projectCategory":"Solar","loanAmount":1000,"productType":"Solar Interest Only"}]}';
            MapWebServiceURI(quickQuotev2Map,quickQuotev2,'quickquote');
            
            Test.stopTest();
        }
    }
/**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}