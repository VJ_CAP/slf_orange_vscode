/**
* Description: Project Dupe Check related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           27/06/2018          Created
******************************************************************************************/

public class ProjectDupeCheckDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+ProjectDupeCheckDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        
        UnifiedBean respData = new UnifiedBean(); 
        respData.projects = new List<UnifiedBean.OpportunityWrapper>();
        respData.error = new list<UnifiedBean.errorWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        projectWrap.applicants = new List<UnifiedBean.ApplicantWrapper>();
        projectWrap.stips = new List<UnifiedBean.StipsAndStatusWrapper>();
        projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
        projectWrap.systemDesigns = new List<UnifiedBean.SystemDesignWrapper>();
        projectWrap.quotes = new List<UnifiedBean.QuotesWrapper>();
        
        
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {   
        
            if(null != reqData.projects)
            {
                if(reqData.projects[0].installZipCode == null || String.isBlank(reqData.projects[0].installZipCode))
                {
                    errMsg = ' installZipCode,';
                }
                if(reqData.projects[0].installStreet == null || String.isBlank(reqData.projects[0].installStreet))
                {
                    errMsg += ' installStreet,';
                }
                if(reqData.projects[0].installCity == null || String.isBlank(reqData.projects[0].installCity))
                {
                    errMsg += ' installCity,';
                }
                if(reqData.projects[0].installStateName == null || String.isBlank(reqData.projects[0].installStateName))
                {
                    errMsg += ' installStateName,';
                }
                if(string.isNotBlank(errMsg))
                {
                    errMsg = errMsg.removeEnd(','); 
                }
                iolist = new list<DataValidator.InputObject>{};
                if(string.isNotBlank(errMsg))
                {
                    errMsg = ErrorLogUtility.getErrorCodes('207',null)+errMsg;
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    respData.error.add(errorMsg);
                    respData.returnCode = '207';
                    iRestRes = (IRestResponse)respData;
                    return iRestRes;
                }
                if(string.isBlank(errMsg))
                {
                    String installStateCode = '%'+SLFUtility.getStateCode(reqData.projects[0].installStateName)+'%';
                    string installZipCode = reqData.projects[0].installZipCode;
                    string installStreet = '%'+reqData.projects[0].installStreet+'%';
                    string installCity = '%'+reqData.projects[0].installCity+'%';
                    
                    List<opportunity> oppLst = [select id,Partner_Foreign_Key__c,Install_Postal_Code__c,Install_Street__c,Install_City__c,Install_State_Code__c from opportunity where Install_Postal_Code__c =: installZipCode  AND Install_Street__c LIKE: installStreet AND Install_City__c Like:installCity AND Install_State_Code__c LIKE: installStateCode];
                    
                    if(!oppLst.isEmpty())
                    {
                        for(Opportunity oppIterate : oppLst)    
                        {
                            UnifiedBean.OpportunityWrapper oppRec = new UnifiedBean.OpportunityWrapper();
                            oppRec.id  = oppIterate.Id;
                            respData.projects.add(oppRec);
                        }
                        
                    }else{
                        respData.message  = 'No duplicates found';
                    }
                }
                respData.returnCode = '200';
            }
            iRestRes = (IRestResponse)respData;
        }catch(Exception err){
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            respData.error.add(errorMsg);
            respData.returnCode = '400';
            iRestRes = (IRestResponse)respData;
            system.debug('### ProjectDupeCheckDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('ProjectDupeCheckDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+ProjectDupeCheckDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
}