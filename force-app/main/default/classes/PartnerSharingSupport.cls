public without sharing class PartnerSharingSupport 
{
    public static map<String,Id> buildRoleGroupsMap (set<Id> partnerAccountIds)

    //  helper method used by all objectnameSharingSupport classes
    //  returns a map of Partner Role Groups, keyed by AccountId:Type

    {
        map<String,Id> roleGroupsMap = new map<String,Id>{};

        //  put all UserRoles for the Partner Accounts into a map keyed by Id

        map<Id,UserRole> rolesMap = new Map<Id,UserRole> ([SELECT Id, PortalRole, PortalAccountId
                                                            FROM UserRole 
                                                            WHERE PortalAccountId IN :partnerAccountIds]);

        //  PortalRole = Executive, Manager, Worker

        //  get related Groups - 2 for each Role (Type = Role and Type = RoleAndSubordinates)
        //  put the Group Id in the return map, keyed by AccountId:Type

        for (Group g : [SELECT Id, RelatedId, DeveloperName, Type FROM Group WHERE RelatedId IN :rolesMap.keySet()])
        {
            UserRole ur = rolesMap.get (g.RelatedId);

            String userRole = '';

            if (ur.PortalRole == 'Worker') userRole = 'Partner User';
            if (ur.PortalRole == 'User') userRole = 'Partner User';
            if (ur.PortalRole == 'Manager') userRole = 'Partner Manager';
            if (ur.PortalRole == 'Executive') userRole = 'Partner Executive';
            
            roleGroupsMap.put (ur.PortalAccountId + ':' + userRole + ':' + g.Type, g.Id);
        }

        return roleGroupsMap;
    }

//******************************************************************

    public static void updateShares (map<String,SObject> sharesNeeded, map<String,SObject> sharesToDelete)

    //  helper method used by all objectnameSharingSupport classes
    //  inserts shares still needed, and deletes shares no longer needed

    {
        //  work through shares needed list, if already in shares to delete list, 
        //  remove from both lists

        System.debug('ALANDEBUG sharesNeeded ' + sharesNeeded.keySet());
        System.debug('ALANDEBUG sharesToDelete ' + sharesToDelete.keySet());

        for (String key : sharesNeeded.keySet())
        {
            if (sharesToDelete.containsKey(key))
            {
                sharesToDelete.remove (key);
                sharesNeeded.remove (key);
            }
        }

        //  insert shares still needed, and delete shares no longer needed
        //  sort() needed to avoid chunk limit

        list<SObject> sharesNeededList = new list<SObject>(sharesNeeded.values());
        sharesNeededList.sort();

        list<SObject> sharesToDeleteList = new list<SObject>(sharesToDelete.values());
        sharesToDeleteList.sort();
    
        for (String s : DebugSupport.insertAndReturnErrors (sharesNeededList))
        {
            System.debug ('Insert errors: ' + s);
        }
    
    for (String s : DebugSupport.deleteAndReturnErrors (sharesToDeleteList))
        {
            System.debug ('Delete errors: ' + s);
        }


    }
    
    
    public static map<String,Id> buildRoleGroupsMap ()

    //  helper method used by all objectnameSharingSupport classes
    //  returns a map of Partner Role Groups, keyed by AccountId:Type

    {
        map<String,Id> roleGroupsMap = new map<String,Id>{};

        //  put all UserRoles for the Partner Accounts into a map keyed by Id
        Map<String, Partner_Roles__c> partnerRoles = Partner_Roles__c.getAll();
        system.debug('### partnerRoles:'+partnerRoles);      
        map<Id,UserRole> rolesMap = new Map<Id,UserRole> ([SELECT Id, PortalRole, PortalAccountId,Name, DeveloperName
                                                            FROM UserRole 
                                                            WHERE DeveloperName IN :partnerRoles.keyset()]);

        //  PortalRole = Executive, Manager, Worker

        //  get related Groups - 2 for each Role (Type = Role and Type = RoleAndSubordinates)
        //  put the Group Id in the return map, keyed by AccountId:Type

        for (Group g : [SELECT Id, RelatedId, DeveloperName, Type FROM Group WHERE RelatedId IN :rolesMap.keySet()])
        {
            UserRole ur = rolesMap.get (g.RelatedId);

            String userRole = '';

            if (ur.PortalRole == 'Worker') userRole = 'Partner User';
            if (ur.PortalRole == 'User') userRole = 'Partner User';
            if (ur.PortalRole == 'Manager') userRole = 'Partner Manager';
            if (ur.PortalRole == 'Executive') userRole = 'Partner Executive';
            
            roleGroupsMap.put (ur.PortalAccountId + ':' + userRole + ':' + g.Type, g.Id);
        }

        return roleGroupsMap;
    }
    
    
}