global class Batch_ProcessSite implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {

    DateTime lastExecutionTime;
    DateTime curBatchStartTime;
    Double  batchNumber;
    
    public Batch_ProcessSite()
    {
        BoxUtil.checkLimits();
        BoxUtil.abortBatchCheck();
        
        // **** load last execution batch time and set current batch start time
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        lastExecutionTime = settings.Site_Batch_Execution_TS__c;
        batchNumber = settings.Batch_Number__c;
        if (lastExecutionTime == null) {
            Date dt = date.parse('01/01/2000');
            lastExecutionTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
        }
        
        curBatchStartTime = System.Now();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.Debug('In start');
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        String limitClause = '';
        if (settings.MAX_ROWS_IN_BATCH__c != null && String.valueOf(settings.MAX_ROWS_IN_BATCH__c).length() > 0) {
            limitClause = ' LIMIT ' + Integer.valueOf(settings.MAX_ROWS_IN_BATCH__c);
        }

        // *** Query all successful responses in Site_Download__c
        String query = 'Select id, Response_Length_Exceeded__c, Site_UUID__C, Opportunity__c, Sighten_Response__c from Site_Download__c where Success__c = true and Processed__c = false order by CreatedDate DESC' + limitClause;
       
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext sc)
    {
        Batch_ProcessSite bf = new Batch_ProcessSite();
        Database.executeBatch(bf,1);
    }
    
    global void execute(Database.BatchableContext BC,List<Site_Download__c> sl) 
    {
        System.Debug('Entering execute with sl of size =>' + sl.size());
        List<Box_Upload__c> uploadList = new List<Box_Upload__c>();
        List<Underwriting_File__c> underwritingList = new List<Underwriting_File__c>();
        List<Box_Fields__c> boxFieldsList = new List<Box_Fields__c>();
        
        // *** gather all the attachments in this batch
        Map<Id, Attachment> attachmentMap = new Map<Id, Attachment>();
        Set<Id> siteIds = new Set<id>();
        for (Site_Download__c s : sl) siteIds.add(s.Id);
       
        List<Attachment> attList = new List<Attachment>([Select Id, ParentId, Name, Body from Attachment where parentId in :siteIds order by CreatedDate DESC]);
        System.Debug('*** got attList ' + attList.size() + ' list => ' + attList);
        for (Attachment a : attList) {
            if (!attachmentMap.containsKey(a.parentId))
                 attachmentMap.put(a.parentId, a);
        }
        System.Debug('*** atMap =>' + attachmentMap);
            
        box.Toolkit boxToolkit = new box.Toolkit();
        for (Site_Download__c site : sl) {
            System.Debug('*** processing site ' + site);
            try {
                //Updated by Adithya as part of 1930 
                sightenUtil.createBoxFoldersAndUploads(boxToolkit,site,uploadList,underwritingList,boxFieldsList,attachmentMap); 
                site.Processing_Notes__c = '';
                site.Processed__c = true;
            } catch (Exception e) {
                site.Processed__c = false;
                site.Processing_Notes__c = e.getMessage().Left(255);
                site.Processing_Error_Details__c  = e.getLineNumber() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + e.getStackTraceString();
            }
        }

        upsert sl Site_UUID__c;
        upsert uploadList sighten_uuid__c;
        system.debug('underwritingList Size - '+underwritingList.size());
        if(underwritingList != null && !underwritingList.isEmpty())
           upsert underwritingList;
        if(boxFieldsList != null && !boxFieldsList.isEmpty())   
        {
            List<Box_Fields__c> updateBoxFldList = new List<Box_Fields__c>();
            for(Box_Fields__c boxFldIterate : boxFieldsList)
            {
                if(null == boxFldIterate.Underwriting__c)
                {
                    Box_Fields__c boxFldRec = new Box_Fields__c();
                    boxFldRec = boxFldIterate;
                    boxFldRec.Underwriting__c = underwritingList[0].Id;
                    updateBoxFldList.add(boxFldRec);
                }else{
                    updateBoxFldList.add(boxFldIterate);
                }
                
            }
            if(!updateBoxFldList.isEmpty())
                upsert updateBoxFldList;
        }
        
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        
        if (!Test.IsRunningTest()) {
            if (settings.CHAIN_BATCHES__c) {
                Database.executeBatch(new Batch_ImageUpload(System.Now(),System.Now()), 1);
            }
        }
    }
}