@isTest
private class LoginAsUserServiceImpl_Test{
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
    }
    private testMethod static void loginAsUserTest(){
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Test.startTest();
        Map<String, String> loginAsURIMap = new Map<String, String>();        
        string projectJsonreq = '';
        projectJsonreq = '{"installerInfo": {"childAccounts": [{"id": "'+acc.Id+'"}]}}';
       // projectJsonreq = '{"installerInfo":{"childAccounts": [{"id": '+acc.Id+'}]}}';
        MapWebServiceURI(loginAsURIMap ,projectJsonreq,'loginas');
        Test.stopTest();
    }
    private testMethod static void loginAsUserTest2(){
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Test.startTest();
        Map<String, String> loginAsURIMap = new Map<String, String>();        
        string projectJsonreq = '';
        projectJsonreq = '{"installerInfo": {"childAccounts": [{"id": "'+loggedInUsr.get(0).contact.accountId+'"}]}}';
       // projectJsonreq = '{"installerInfo":{"childAccounts": [{"id": '+acc.Id+'}]}}';
        MapWebServiceURI(loginAsURIMap ,projectJsonreq,'loginas');
        Test.stopTest();
    }
    private testMethod static void loginAsUserTest3(){
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Test.startTest();
        Map<String, String> loginAsURIMap = new Map<String, String>();        
        string projectJsonreq = '';
        projectJsonreq = '{"installerInfo": {"childAccounts": [{"id": '+''+'}]}}';
      
        MapWebServiceURI(loginAsURIMap ,projectJsonreq,'loginas');
        Test.stopTest();
    }
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        if(string.isNotBlank(jsonString)){
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();   
    }
}