public class SoftPullCreditTriggerHandler{
    
    public static void OnAfterInsert(Map<Id,Credit__c> newMap){
        afterInsertUpdate(newMap.values());
    }
    
    public static void OnAfterUpdate(Map<Id,Credit__c> newMap, Map<Id,Credit__c> oldMap){
        afterInsertUpdate(newMap.values());
    }
    
    public static void OnBeforeInsert(List<Credit__c> newList){
        beforeInsertUpdate(newList,null);
    }
    
    public static void OnBeforeUpdate(Map<Id,Credit__c> newMap, Map<Id,Credit__c> oldMap){
        beforeInsertUpdate(newMap.values(),oldMap);
    }    
     
    public static void beforeInsertUpdate(List<Credit__c> newCreditList, Map<id,Credit__c> oldMap){
        system.debug('### Entered into beforeInsertUpdate() of '+ SoftPullCreditTriggerHandler.class);
        boolean callFNI = false; 
        //Query prequal table
        List<Prequalification_Criterion__c> prequalList = [SELECT Installer_Account__c, State_Code__c, FNI_Response_Code__c, Qualification_Message__c from Prequalification_Criterion__c];
        List<String> emailList = new List<String>();
        for(Credit__c thisCredit: newCreditList){
            //check if this is a supported installer/state
             boolean isSupported = false;
            for(Prequalification_Criterion__c prequalCrit: prequalList){
                if(thisCredit.Installer_Account__c == prequalCrit.Installer_Account__c && thisCredit.State_Code__c == prequalCrit.State_Code__c){
                    isSupported = true;
                    break;
                }
            }
            if(isSupported){
                if(oldMap != null){
                    Credit__c oldCredit = oldMap.get(thisCredit.Id);
                    callFNI = callFNITriggerStaticHelper.callFNI(
                        thisCredit.First_Name__c,
                        thisCredit.Last_Name__c, 
                        thisCredit.Street__c, 
                        thisCredit.City__c, 
                        thisCredit.State_Code__c, 
                        thisCredit.Postal_Code__c, 
                        thisCredit.Customer_has_authorized_credit_soft_pull__c, 
                        thisCredit.FNI_Decision__c, 
                        thisCredit.DOB__c, 
                        thisCredit.Yearly_Income__c, 
                        thisCredit.SSN__c, 
                        oldCredit.First_Name__c,
                        oldCredit.Last_Name__c,
                        oldCredit.Street__c,
                        oldCredit.City__c,
                        oldCredit.State_Code__c,
                        oldCredit.Postal_Code__c,
                        oldCredit.DOB__c,
                        oldCredit.Yearly_Income__c,
                        oldCredit.SSN__c,null);
                } else {
                    callFNI = callFNITriggerStaticHelper.callFNI(
                        thisCredit.First_Name__c,
                        thisCredit.Last_Name__c, 
                        thisCredit.Street__c, 
                        thisCredit.City__c, 
                        thisCredit.State_Code__c, 
                        thisCredit.Postal_Code__c, 
                        thisCredit.Customer_has_authorized_credit_soft_pull__c, 
                        thisCredit.FNI_Decision__c, 
                        thisCredit.DOB__c, 
                        thisCredit.Yearly_Income__c, 
                        thisCredit.SSN__c, 
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);   
                }
                if(callFNI){
                    
                    thisCredit.FNI_Decision__c = '0';
                    thisCredit.Prequal_Decision__c = 'Decision pending - please refresh page in a moment...';
                }    
            } else{
                thisCredit.FNI_Decision__c = '99';
                thisCredit.Prequal_Decision__c = 'Unfortunately, at this time, the Sunlight Financial Prequalification feature is not available for products in the state you have selected.';
            }
            if(thisCredit.Sales_Representative_Email__c != null)
            {
                emailList.add(thisCredit.Sales_Representative_Email__c);
                System.debug('### Debug: thisCredit.Sales_Representative_Email__c: ' + thisCredit.Sales_Representative_Email__c);
            }
         }
         if(emailList.size() > 0){
            Map<String, Id> contactMap = new Map<String, Id>();
            for(Contact contactResults : [Select Id, email from Contact where email = :emailList]){
                contactMap.put(contactResults.email, contactResults.Id);
                System.debug('### Debug: contactResults: ' + contactResults);
            }
        
            for(Credit__c updateCredit:newCreditList){
                updateCredit.Sales_Representative__c = contactMap.get(updateCredit.Sales_Representative_Email__c);
            }
        }
        system.debug('###Exit from beforeInsertUpdate() of '+ SoftPullCreditTriggerHandler.class);
     }
     public static void afterInsertUpdate(List<Credit__c> newCreditList){
        system.debug('### Entered into afterInsertUpdate() of '+ SoftPullCreditTriggerHandler.class);
        //Query prequal table
        List<Prequalification_Criterion__c> prequalList = [SELECT Installer_Account__c, State_Code__c, FNI_Response_Code__c, Qualification_Message__c from Prequalification_Criterion__c];
        List<Credit__c> fniCreditList = new List<Credit__c>();
        
        for(Credit__c thisCredit: newCreditList){
            //no need to check recordtype since the credit auth is unique to that record type
            //callFNI = callFNICreditTriggerStaticHelper.callFNI(thisCredit, oldMap);
            
            if(thisCredit.FNI_Decision__c == '0'){
                fniCreditList.add(thisCredit);
            }
        }
        
        for(Credit__c fniCredit : fniCreditList){
            //this is the non-bulkified portion - this call happens for each credit object, if FNI provides a bulk api, this can be editted
            fniCalloutManual.buildCreditRequestAsync(fniCredit.Id, JSON.serialize(prequalList)); 
            //fniCredit.FNI_Decision__c = '';
        }   
        system.debug('### Exit from afterInsertUpdate() of '+ SoftPullCreditTriggerHandler.class);
    }
}