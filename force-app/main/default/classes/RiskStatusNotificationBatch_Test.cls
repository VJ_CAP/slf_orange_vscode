@isTest
public class RiskStatusNotificationBatch_Test {
    @testsetup static void createtestdata(){
        
        //SLFUtilityDataSetup.initData();
    }
    private testMethod static void RiskStatusNotificationBatchexecute(){
         List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
         TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
          TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
         TriggerFlags__c trgFundingDataflag = new TriggerFlags__c();
        trgFundingDataflag.Name ='Funding_Data__c';
        trgFundingDataflag.isActive__c =true;
        trgLst.add(trgFundingDataflag);
        
        insert trgLst;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
	        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        // List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        
        System.runAs(portalUser)
        {
            //Insert Account record
            Account personAcc = new Account();
            personAcc.Name = 'SLFTest';
            personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.BillingStateCode = 'CA';
            personAcc.Installer_Legal_Name__c='Bright planet Solar';
            personAcc.Solar_Enabled__c = true;
            //personAcc.OwnerId = portalUser.Id;
            insert personAcc;
            
            Contact personcon = new Contact(LastName ='testCon',AccountId = personAcc.Id);
            insert personcon; 
            
            personAcc = [select Id from Account where Id=: personAcc.Id];
            
            personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
            update personAcc;
            List<Opportunity> oppList = new List<Opportunity>();
            for(integer i = 0;i<=3;i++){   
                Opportunity personOpp = new Opportunity();
                personOpp.name = 'personOpp'+i;
                personOpp.CloseDate = system.today();
                personOpp.StageName = 'Qualified';
                personOpp.AccountId = personAcc.id;
                personOpp.Installer_Account__c = acc.id;
                personOpp.Install_State_Code__c = 'CA';
                personOpp.Install_Street__c ='Street';
                personOpp.Install_City__c ='InCity';
                personOpp.Combined_Loan_Amount__c =10000;
                //personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
                if(i==2){
                     personOpp.Credit_Expiration_Date__c=date.today().addDays(10);
                }else
                personOpp.Credit_Expiration_Date__c=date.today().addDays(30);
                oppList.add(personOpp);
            }
            insert oppList;
            system.debug('## oppList==>:'+oppList.size());
            List<Underwriting_File__c> underwritingLst  = new List<Underwriting_File__c>();
             system.debug('## underwritingLst==>:'+underwritingLst.size());
            for(Underwriting_File__c objUnderwrt:[select Id,Opportunity__c,Opportunity__r.name,Opportunity__r.Credit_Expiration_Date__c,M0_Approval_Requested_Date__c, M0_Approval_Date__c, M1_Approval_Date__c, M1_Approval_Requested_Date__c, M2_Approval_Requested_Date__c,Opportunity__r.Installer_Account__r.M0_Required__c, Opportunity__r.Installer_Account__r.M1_Required__c,Opportunity__r.Installer_Account__r.M2_Required__c 
                                                           from Underwriting_File__c WHERE Opportunity__c IN:oppList]){
                if(objUnderwrt.Opportunity__r.name=='personOpp3'){
                    objUnderwrt.M0_Approval_Date__c=date.today();
                    objUnderwrt.M0_Approval_Requested_Date__c=date.today();
                    objUnderwrt.M1_Approval_Date__c=date.today().addDays(60);
                }
                if(objUnderwrt.Opportunity__r.name!='personOpp3'){
                     objUnderwrt.M0_Approval_Date__c=date.today();
                	objUnderwrt.M0_Approval_Requested_Date__c=date.today();   
                }
                underwritingLst.add(objUnderwrt);
            }

            update underwritingLst;
            
           
            Test.startTest(); 
            RiskStatusNotificationBatch riskBatch= new RiskStatusNotificationBatch();
            Database.executeBatch(riskBatch);
            Test.stopTest();
        }
    }
}