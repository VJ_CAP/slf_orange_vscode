/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Sejal                03/01/2020               Created
******************************************************************************************/
public class UserLoginHistoryServiceImpl extends RESTServiceBase{
    
    public override IRestResponse fetchData(RequestWrapper rw){
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        string errMsg = '';
        UnifiedBean respBean = new UnifiedBean();
        try{
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            
            List<User> lstUser = [SELECT Id,email,userName,IsActive,Hash_Id__c,Ignore_Count__c,contactId,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c 
                                  FROM user WHERE Id=:UserInfo.getUserId() and IsActive=true];
            if(lstUser!=null && lstUser.size()>0){
                User objUser = lstUser.get(0);
                //Added by Sreekar as part of Orange-11105 - Start
                User_Login_History__c objLoginHistory = new User_Login_History__c(); 
                
                if(reqData.ipAddress != null && reqData.version != null && reqData.source != null){
                    objLoginHistory.OwnerId = objUser.Id;
                    objLoginHistory.sourceIP__c = reqData.ipAddress;
                    objLoginHistory.version__c = reqData.version;
                    objLoginHistory.source__c = reqData.source;
                    objLoginHistory.Logged_In_User__c = objUser.Id;
                    insert objLoginHistory;
                }  
                
                }
            User defaultUser =  new User(); 
            
                if(reqData.hashid!=null){
                    List<Opportunity> oppLst = new List<Opportunity>();
                    oppLst = [SELECT id, Hash_Id__c, Installer_Account__c from Opportunity where Hash_Id__c =: reqData.hashid];
                    if(!oppLst.isEmpty()){
                        defaultUser = returnDefaultUser(oppLst[0].Installer_Account__c);
                    }else {
                        List<SLF_Credit__c> creditLst = new List<SLF_Credit__c>();
                        creditLst = [select Id,Opportunity__r.Installer_Account__c from SLF_Credit__c where Hash_Id__c =: reqData.hashId];
                        if(!creditLst.isEmpty())    {
                            defaultUser = returnDefaultUser(creditLst[0].Opportunity__r.Installer_Account__c);
                        }
                        else{
                            List<Prequal__c> preqLst = new List<Prequal__c>();
                            preqLst = [select id,Installer_Account__c from Prequal__c where HashID__c =: reqData.hashId];
                            if(!preqLst.isEmpty())    {
                                defaultUser = returnDefaultUser(preqLst[0].Installer_Account__c);
                            }else{
                                List<Draw_Requests__c> drawLst = new List<Draw_Requests__c>();
                                drawLst = [select id,Underwriting__r.Opportunity__r.Installer_Account__c from Draw_Requests__c where Hash_Id__c =: reqData.hashId];
                                if(!drawLst.isEmpty())    {
                                    defaultUser = returnDefaultUser(drawLst[0].Underwriting__r.Opportunity__r.Installer_Account__c);
                                }
                                else    {
                                    errMsg= ErrorLogUtility.getErrorCodes('350',null);
                                    gerErrorMsg(respBean,errMsg,'350');
                                    iRestRes = (IRestResponse)respBean;
                                    res.response= iRestRes;
                                    return iRestRes;
                                }
                            }
                        }
                    }
            
            }
                    
            if(defaultUser == null){
                errMsg= ErrorLogUtility.getErrorCodes('350',null);
                gerErrorMsg(respBean,errMsg,'350');
                iRestRes = (IRestResponse)respBean;
                res.response= iRestRes;
                return iRestRes;
            }
            else{
                respBean.username = defaultUser.Username;
                respBean.password = defaultUser.Password__c;
            }
            respBean.returnCode = '200';
            respBean.message = ErrorLogUtility.getErrorCodes('200', null);
            
            iRestRes = (IRestResponse)respBean;
        }catch(Exception err){
            
            system.debug('### UserLoginHistoryServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('UserLoginHistoryServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            errMsg= ErrorLogUtility.getErrorCodes('400',null);
            gerErrorMsg(respBean,errMsg,'400');
            iRestRes = (IRestResponse)respBean;
            res.response= iRestRes;
        }
        return iRestRes;
        
    }
    
    
     public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        List<UnifiedBean.ErrorWrapper> errorList = new List<UnifiedBean.ErrorWrapper>();
        UnifiedBean.ErrorWrapper errorWraper = new UnifiedBean.ErrorWrapper();
        errorWraper.errorMessage = sErrorMsg;
        errorList.add(errorWraper);
        objUnifiedBean.error = errorList;
        objUnifiedBean.returnCode = sReturnCode;
    }
    
    public User returnDefaultUser(Id accountId)    {
        List<User> lstUsers = [SELECT Id, Username,Email, Password__c from User where contact.AccountId =:accountId
                                       AND Default_User__c = true and IsActive = true ];
        if(lstUsers != null && !lstUsers.isEmpty())    {
            return lstUsers[0];
        }else{
            return null;
        }
    }
    
}