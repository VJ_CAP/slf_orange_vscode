@isTest
public class CreditExpirationBatchSchdl_Test{   
    
    private testMethod static void testBatch(){
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);      
        TriggerFlags__c trgflag3 = new TriggerFlags__c();
        trgflag3.Name ='SLF_Credit__c';
        trgflag3.isActive__c =true;
        trgrList.add(trgflag3);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='Underwriting_File__c';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        TriggerFlags__c trgflag5 = new TriggerFlags__c();
        trgflag5.Name ='Funding_Data__c';
        trgflag5.isActive__c =true;
        trgrList.add(trgflag5);
        insert trgrList;
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account150';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';        
        accObj.Type = 'Facility'; 
        accObj.Prequal_Enabled__c = true; 
        accObj.Website = 'www.google.com';  
        accObj.Solar_Enabled__c = true;     
        insert accObj;
        
        Opportunity oppObj = new Opportunity(Name = 'OppName',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Qualified',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California',
                                             EDW_Originated__c = false,
                                             Project_Category__c = 'Home');
        insert oppObj; 
        
        Underwriting_File__c uwObj = new Underwriting_File__c(Name = 'UwName', 
                                                          Opportunity__c = oppObj.id,
                                                          Approved_for_Payments__c = true);
        insert uwObj;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =accObj.id;
        prod.Long_Term_Facility_Lookup__c =accObj.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        //prod.ACH__c = true;
        prod.Internal_Use_Only__c = false;
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        prod.Product_Tier__c = '0';
        insert prod;
        
        SLF_Credit__c credit = new SLF_Credit__c();
        credit.Opportunity__c = oppObj.id;
        credit.Primary_Applicant__c = accObj.id;
        credit.Co_Applicant__c = accObj.id;
        credit.SLF_Product__c = prod.Id ;
        credit.Total_Loan_Amount__c = 100;
        credit.Status__c = 'New';        
        credit.FNI_Credit_Expiration__c = system.Today().addDays(-1);
        credit.LT_FNI_Reference_Number__c = '123';
        credit.Application_ID__c = '123';
        credit.M1_Approval_Requested__c = false;
        insert credit;
        
        Set<id> uwIdSet = new Set<id>();        
        Test.startTest();
            CreditExpirationBatchSchdl scheduleObj = new CreditExpirationBatchSchdl();
            String sch = '0 0 4 ? * *';
            System.assertEquals(sch , '0 0 4 ? * *');
            String jobID = system.schedule('Credit Expiration Job Test', sch, scheduleObj);
        Test.stopTest();
      }
}