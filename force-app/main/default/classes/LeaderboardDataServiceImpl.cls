/**
        * Description: Credit related integration logic .
        *
        *   Modification Log :
        ---------------------------------------------------------------------------
            Developer               Date                Description
        ---------------------------------------------------------------------------
            Brahmeswar           13/11/2018               Created
        ******************************************************************************************/

        public without sharing class LeaderboardDataServiceImpl extends RestDataServiceBase{
            
            /**
            * Description: Convert out response from salesforce sobject to wrapper (JSON)
            *
            * @param sObj           Convert the sobject record to bean.
            * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
            * @param msg            Each record specific message, if success/fail.
            * return null           IRestResponse returns object specific bean based on the consumer.
            */
            public override IRestResponse transformOutput(IDataObject stsDtObj){
                system.debug('### Entered into transformOutput() of '+LeaderboardDataServiceImpl.class);
                
                UnifiedBean reqData = (UnifiedBean)stsDtObj; 
                system.debug('### reqData '+reqData);
                
                UnifiedBean unifiedBean = new UnifiedBean(); 
                unifiedBean.projects = new List<UnifiedBean.OpportunityWrapper>();
                unifiedBean.error = new list<UnifiedBean.errorWrapper>();
                unifiedBean.rows = new List<UnifiedBean.RowWrapper>();
                
                
                IRestResponse iRestRes;
                
                string errMsg = '';
                list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
                Savepoint sp = Database.setSavepoint();
                try
                {   
                    Date startDt = Date.valueOf(reqData.startDate);
                    Date endDt = Date.valueOf(reqData.endDate);
                    system.debug('**startDt ****'+startDt );
                    system.debug('**endDt ****'+endDt );                    
                    User loggedInUsr = [select Id,contactid,UserRole.Name,contact.AccountId from User where Id =: userinfo.getUserId()];
                    String RecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
                    Set<Id> InstallerAccIds = new Set<Id>();
                    Set<Id> usrReportsToSet = new Set<Id>();
                    //Added as part of ORANGE-2092 By Brahmeswar
                    if(reqData.includeOrgHierarchy!=null && String.isBlank(reqData.installerId) && reqData.includeOrgHierarchy){
                        if(RecordTypeID != null && RecordTypeID != '' && loggedInUsr != null){
                            InstallerAccIds.add(loggedInUsr.contact.AccountId);
                            for(Account objAccIterate :[select Id,ParentId from Account where RecordTypeId=:RecordTypeID AND ParentId=:loggedInUsr.contact.AccountId]){
                                InstallerAccIds.add(objAccIterate.id);
                                
                            }
                        }
                        
                    }else if(!String.isBlank(reqData.installerId) && (reqData.includeOrgHierarchy== null||!reqData.includeOrgHierarchy)){
                            InstallerAccIds.add(reqData.installerId);
                    }else if( !loggedInUsr.UserRole.Name.contains('Executive')){   
                    
                        If(loggedInUsr.UserRole.Name.contains('User'))
                        {
                            for(User usrIterate : [select Id,contactid ,contact.AccountId from User where UserRoleId  =: loggedInUsr.UserRoleId]){
                                usrReportsToSet.add(usrIterate.Id);
                            }
                        }               
                        else{
                            for(User usrIterate : [select Id,contactid ,contact.AccountId from User where Contact.ReportsToId  =: loggedInUsr.contactId]){
                                usrReportsToSet.add(usrIterate.Id);
                            }
                        }
                    }
                    System.debug('###loggedInUsr:'+loggedInUsr);
                    System.debug('###RecordTypeID:'+RecordTypeID);
                    System.debug('###InstallerAccIds:'+InstallerAccIds);
                    
                    if(null != reqData.pageNumber && null != reqData.numberOfRecords)
                    {
                        //Added by Deepika as part of Orange - 2358 - start
                        if(String.isBlank(reqData.projectCategory)) 
                            reqData.projectCategory = 'Solar';
                        //Added by Deepika as part of Orange - 2358 - stop
                        Integer offset = 0;
                        if(reqData.pageNumber > 1){
                            offset= (reqData.pageNumber-1)*reqData.numberOfRecords;
                        }
                        System.debug('### offset '+offset);
                        
                        Integer totalNumber = offset + reqData.numberOfRecords;
                        
                        Set<string> oppidset = new Set<String>();
                        for (Underwriting_File__c uw : [select Opportunity__c from Underwriting_File__c where Project_Status__c = 'Project Withdrawn' AND Opportunity__r.stageName='Closed Won']){
                            oppidset.add(uw.Opportunity__c);
                        } 
                        AggregateResult[] count;
                        AggregateResult[] Opprecord;
                        
                        //Added as part of ORANGE-2092 By Brahmeswar
                        if(InstallerAccIds != null && InstallerAccIds.size()>0){
                            
                            count = [Select COUNT(Id) from Opportunity where StageName = 'Closed Won' AND CreatedDate >= :startDt AND CreatedDate <= :endDt AND ID NOT IN:oppidset AND Installer_Account__c IN:InstallerAccIds AND Project_Category__c = :reqData.projectCategory GROUP BY Owner.Name ];
                                
                            Opprecord = [Select ownerid,Owner.Name Ownr,COUNT(id) cout from Opportunity where StageName = 'Closed Won' AND CreatedDate >= :startDt AND CreatedDate <= :endDt AND ID NOT IN:oppidset AND Installer_Account__c IN:InstallerAccIds AND Project_Category__c = :reqData.projectCategory GROUP BY ownerid,Owner.Name ORDER BY COUNT(id) DESC,Owner.Name ASC];
                            
                            
                        }else{
                            //Added project category conditions in queries by Deepika as part of Orange - 2358 
                            if( !loggedInUsr.UserRole.Name.contains('Executive')) 
                            {
                                count = [Select COUNT(Id) from Opportunity where StageName = 'Closed Won' AND CreatedDate >= :startDt AND CreatedDate <= :endDt AND ID NOT IN:oppidset AND Installer_Account__c =: loggedInUsr.contact.AccountId AND OwnerId IN: usrReportsToSet AND Project_Category__c = :reqData.projectCategory GROUP BY Owner.Name ];
                                
                                Opprecord = [Select ownerid,Owner.Name Ownr,COUNT(id) cout from Opportunity where StageName = 'Closed Won' AND CreatedDate >= :startDt AND CreatedDate <= :endDt AND ID NOT IN:oppidset AND Installer_Account__c =: loggedInUsr.contact.AccountId  AND OwnerId IN: usrReportsToSet AND Project_Category__c = :reqData.projectCategory GROUP BY ownerid,Owner.Name ORDER BY COUNT(id) DESC,Owner.Name ASC];
                                
                            }else{
                                count = [Select COUNT(Id) from Opportunity where StageName = 'Closed Won' AND CreatedDate >= :startDt AND CreatedDate <= :endDt AND ID NOT IN:oppidset AND Installer_Account__c =:loggedInUsr.contact.AccountId AND Project_Category__c = :reqData.projectCategory GROUP BY Owner.Name ];
                                
                                Opprecord = [Select ownerid,Owner.Name Ownr,COUNT(id) cout from Opportunity where StageName = 'Closed Won' AND CreatedDate >= :startDt AND CreatedDate <= :endDt AND ID NOT IN:oppidset AND Installer_Account__c=:loggedInUsr.contact.AccountId AND Project_Category__c = :reqData.projectCategory GROUP BY ownerid,Owner.Name ORDER BY COUNT(id) DESC,Owner.Name ASC];
                            }
                        
                        }
                        
                        
                        
                        
                        
                        List<UnifiedBean.RowWrapper> temprowlist = new List<UnifiedBean.RowWrapper>();
                        if(!Opprecord.isEmpty())
                        {
                            system.debug('***offset***'+offset);
                            system.debug('***totalNumber***'+totalNumber);
                            system.debug('***Opprecord*size**'+Opprecord.size());
                            
                            if(totalNumber <= Opprecord.size())
                            {
                                for(Integer i= offset ; i < totalNumber ; i++)
                                {
                                    unifiedBean.RowWrapper rowobj = new unifiedBean.RowWrapper();
                                    rowobj.numberOfProjects = Integer.valueOf(Opprecord[i].get('cout'));
                                    rowobj.salesRepName = String.ValueOf(Opprecord[i].get('Ownr'));
                                    rowobj.rank = i +1;
                                    temprowlist.add(rowobj);
                                }
                            }else{
                                for(Integer i= offset ; i < Opprecord.size() ; i++)
                                {
                                    unifiedBean.RowWrapper rowobj = new unifiedBean.RowWrapper();
                                    rowobj.numberOfProjects = Integer.valueOf(Opprecord[i].get('cout'));
                                    rowobj.salesRepName = String.ValueOf(Opprecord[i].get('Ownr'));
                                    rowobj.rank =  i +1;
                                    temprowlist.add(rowobj);
                                }
                            }
                        }
                        if(string.isBlank(errMsg)){
                            unifiedBean.rows = temprowlist;
                            unifiedBean.returnCode = '200';
                            unifiedBean.totalNumberOfRecords = count.size();
                            unifiedBean.pageNumber = reqData.pageNumber;
                            unifiedBean.numberOfRecords = temprowlist.size();
                        }
                    }
                  
                    if(string.isNotBlank(errMsg))
                    {
                        Database.rollback(sp);
                        iolist = new list<DataValidator.InputObject>{};
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        errMsg = errorMsg.errorMessage;
                        unifiedBean.error.add(errorMsg);
                    }
                    iRestRes = (IRestResponse)unifiedBean;
                }catch(Exception err){
                    Database.rollback(sp);
                    iolist = new list<DataValidator.InputObject>{};
                    errMsg = ErrorLogUtility.getErrorCodes('400', null);
                    
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    unifiedBean.error.add(errorMsg);
                    unifiedBean.returnCode = '400';
                    iRestRes = (IRestResponse)unifiedBean;
                    
                    system.debug('### LeaderboardDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
                    
                    ErrorLogUtility.writeLog('LeaderboardDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
                }    
                system.debug('### Exit from transformOutput() of '+LeaderboardDataServiceImpl.class);
                system.debug('### iRestRes '+iRestRes);
                return iRestRes;
            }
        }