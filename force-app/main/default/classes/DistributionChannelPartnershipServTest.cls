@isTest(seeAllData=false)
public class DistributionChannelPartnershipServTest{    
    
    private testMethod static void DistributionChannelServTest(){
        
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgrList.add(trgUnderwrtflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgrList.add(trgAccflagFunding);
         
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgrList.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgrList.add(trgUserflag);
               
        insert trgrList;
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account150';
        accObj.Installer_Email__c = 'test@gmail.com';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';
        accObj.Solar_Enabled__c = true;        
        accObj.Type = 'Facility'; 
        accObj.Prequal_Enabled__c = true; 
        accObj.Website = 'www.google.com'; 
        accObj.M0_Split__c = 20;
        accObj.M1_Split__c = 60;
        accObj.M2_Split__c = 20;        
        insert accObj;
        
        Contact con = new Contact(LastName ='testCon',AccountId = accObj.Id);
        insert con;  
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        Opportunity oppObj = new Opportunity(Name = 'OppName9',
                                              AccountId=accObj.Id,
                                              Co_Applicant__c = accObj.Id,
                                              StageName='Closed Won',
                                              CloseDate=system.today(),
                                              Installer_Account__c = accObj.Id, 
                                              Install_State_Code__c = 'CA',
                                              Synced_Quote_Id__c = 'qId',
                                              Desync_Bypass__c = true,
                                              Install_Street__c='40 CORTE ALTA',
                                              Install_Postal_Code__c='94949',
                                              Install_City__c='California');
        insert oppObj;
    
        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = oppObj.Id,
                                                              Project_Status__c = 'M1');
        insert uwObj;
        
        Test.startTest();
        
        DistributionChannelPartnershipServ.createStipData(uwObj.Id,'test');        
        
        Stipulation_Data__c  sdObj = new Stipulation_Data__c(Name = 'VOI', 
                                                             Email_Text__c = 'Test data', 
                                                             POS_Message__c = 'test',
                                                             CL_Code__c='CL1',
                                                             Email_Template_Name__c='Test_Email_Template',
                                                             Review_Classification__c = 'SLS II');
        insert sdObj;
        
        DistributionChannelPartnershipServ.createStipData(uwObj.Id, sdObj.Name);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        insert trgStipflag;
        
        List<Stipulation_Data__c> stDList = new List<Stipulation_Data__c>();
        Stipulation_Data__c  sdObj1 = new Stipulation_Data__c(Name = 'VOI', 
                                                             Email_Text__c = 'Test data', 
                                                             POS_Message__c = 'test',
                                                             CL_Code__c='CL1',
                                                             Email_Template_Name__c='Test_Email_Template',
                                                             Review_Classification__c = 'SLS II');
        stDList.add(sdObj1);
        Stipulation_Data__c  sdObj2 = new Stipulation_Data__c(Name = 'INV', 
                                                             Email_Text__c = 'Test data', 
                                                             POS_Message__c = 'test',
                                                             CL_Code__c='CL1',
                                                             Email_Template_Name__c='Test_Email_Template',
                                                             Review_Classification__c = 'SLS II');
        stDList.add(sdObj2);
        
        insert stDList;
        
        
        System.runAs(portalUser){
            EmailTemplate emailTempObj = new EmailTemplate (developerName = 'Test_Email_Template', 
                                                            FolderId = UserInfo.getUserId(), 
                                                            TemplateType= 'Text', 
                                                            Name = 'Test_Email_Template',
                                                            HtmlValue = '<Image_URL> Test data'); 
            insert emailTempObj;
        }
        
        DistributionChannelPartnershipServ.createStipData(uwObj.Id,sdObj1.Name);
        DistributionChannelPartnershipServ.createStipData(uwObj.Id,sdObj2.Name);
        
        Funding_Data__c fndngDataObj = new Funding_Data__c(Underwriting_ID__c = uwObj.Id,
                                                           Equipment_Order_Number__c = 'Order Number',
                                                           Equipment_Order_Amount__c = 5000,
                                                           Equipment_Order_Date_Received__c = System.now());
                                                               
        insert fndngDataObj;
        
        
        DistributionChannelPartnershipServ.createStipData(uwObj.Id,sdObj1.Name);
        
        Test.stopTest();
    }
}