/******************************************************************************************
* Description: Test class to cover GetNonACHAprInfoDataServiceImpl Services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri             07/05/2019             Created
******************************************************************************************/
@isTest
public class GetNonACHAprInfoDataServiceImpl_Test {
    /**
*
* Description: This method is used to cover GetNonACHAprInfoDataServiceImpl
*
*/   
    private testMethod static void NonACHAprInfoTest(){
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        
        //Insert Profile
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        //For GetNonACHAprInfoDataServiceImpl Inserting Account record 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.HI_Dealer_Code__c = '123456';
        acc.Home_Improvement_Enabled__c = true;
        insert acc;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;  
        
        List<User> userList = new List<User>();
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',Default_User__c = true,LoginCount__c = 1,
                                   EmailEncodingKey='UTF-8', LastName='test', Password__c = 'Test@123',LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        userList.add(portalUser);
        insert userList;
        
        //Insert SLF Product Record
        List<Product__c> prodList = new List<Product__c>();
        //Solar product
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c = 3.24;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 180;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        //insert prod;
        prodList.add(prod);
        //Home Product
        Product__c prod1 = new Product__c();
        prod1.Installer_Account__c =acc.id;
        prod1.Long_Term_Facility_Lookup__c =acc.id;
        prod1.Name='testprod';
        prod1.FNI_Min_Response_Code__c=9;
        prod1.Product_Display_Name__c='test';
        prod1.Qualification_Message__c ='testmsg';
        prod1.Product_Loan_Type__c='HII';
        prod1.State_Code__c ='CA';
        prod1.ST_APR__c = 2.99;
        prod1.Long_Term_Facility__c = 'TCU';
        prod1.APR__c = 3.99;
        prod1.ACH__c = true;
        prod1.NonACH_APR__c = 3.24;
        prod1.Internal_Use_Only__c = false;
        //prod.Long_Term_Facility__c='TCU';
        prod1.LT_Max_Loan_Amount__c =1000;
        prod1.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod1.Product_Tier__c = '0';
        //insert prod;
        prodList.add(prod1);
        
        Product__c prod2 = new Product__c();
        prod2.Installer_Account__c =acc.id;
        prod2.Long_Term_Facility_Lookup__c =acc.id;
        prod2.Name='testprod1';
        prod2.FNI_Min_Response_Code__c=9;
        prod2.Product_Display_Name__c='test1';
        prod2.Qualification_Message__c ='testmsg1';
        prod2.Product_Loan_Type__c='HIS';
        prod2.State_Code__c ='CA';
        prod2.ST_APR__c = 2.99;
        prod2.Long_Term_Facility__c = 'TCU';
        prod2.APR__c = 3.99;
        prod2.ACH__c = true;
        prod2.NonACH_APR__c = 3.24;
        prod2.Internal_Use_Only__c = false;
        //prod.Long_Term_Facility__c='TCU';
        prod2.LT_Max_Loan_Amount__c =1000;
        prod2.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod2.Product_Tier__c = '0';
        //insert prod;
        prodList.add(prod2);
        insert prodList;
        
        //Insert Person Account
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact personcon = new Contact(LastName ='testCon',AccountId = personAcc.Id);
        insert personcon; 
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        personAcc.PersonBirthdate = Date.ValueOf('1929-03-10');
        update personAcc;
        
        AccountShare personAccShare = new AccountShare(); //a new empty AccountShare object
        personAccShare.userorgroupid = portalUser.Id;
        personAccShare.accountid = personAcc.Id;
        personAccShare.accountaccesslevel = 'Edit';
        personAccShare.OpportunityAccessLevel = 'None';
        personAccShare.CaseAccessLevel = 'None';
        insert personAccShare;
        //Insert Person Account
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];        
        System.runAs(loggedInUsr[0])
        {
        //Insert opportunity record
            List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            //insert personOpp;
            oppList.add(personOpp);
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = personAcc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            opp.ProductTier__c = '0';         
            opp.Type_of_Residence__c  = 'Own';
            opp.language__c = 'Spanish';
            opp.Project_Category__c = 'Solar';
            opp.Hash_Id__c = 'TestHashId123';
            opp.Application_Status__c = 'Auto Approved';
            //insert opp;
            opp.Credit_Expiration_Date__c = System.today()+1;
            oppList.add(opp);
            
            Opportunity opp1 = new Opportunity();
            opp1.name = 'OpNameHome';
            opp1.CloseDate = system.today();
            opp1.StageName = 'Qualified';
            opp1.AccountId = personAcc.id;
            opp1.Installer_Account__c = acc.id;
            opp1.Install_State_Code__c = 'CA';
            opp1.Install_Street__c ='Street1';
            opp1.Install_City__c ='InCity1';
            opp1.Combined_Loan_Amount__c = 10000;
            opp1.Partner_Foreign_Key__c = 'TCU||1234';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp1.SLF_Product__c = prod1.id;
            opp1.Opportunity__c = personOpp.Id; 
            opp1.Block_Draws__c = false;
            opp1.Has_Open_Stip__c = 'False';
            opp1.All_HI_Required_Documents__c=true;
            opp1.Max_Draw_Count__c=3;
            opp1.ProductTier__c = '0';         
            opp1.Type_of_Residence__c  = 'Own';
            //opp1.language__c = 'Spanish';
            opp1.Project_Category__c = 'Home';
            opp1.Hash_Id__c = 'TestHashId1234';
            opp1.Application_Status__c = 'Auto Approved';
            //insert opp;
            opp1.Credit_Expiration_Date__c = System.today()+1;
            oppList.add(opp1);
            
            insert oppList;
        
        //Home Opportunity
        /*Opportunity personOpp1 = new Opportunity();
        personOpp1.name = 'personOpp';
        personOpp1.CloseDate = system.today();
        personOpp1.StageName = 'Qualified';
        personOpp1.AccountId = personAcc.id;
        personOpp1.isACH__c = false;
        personOpp1.Project_Category__c = 'Home';
        personOpp1.Installer_Account__c = acc.id;
        personOpp1.Install_State_Code__c = 'CA';
        personOpp1.Install_Street__c ='Street';
        personOpp1.Install_City__c ='InCity';
        personOpp1.Combined_Loan_Amount__c =10000;
        personOpp1.Partner_Foreign_Key__c = 'TCU||abc456';
        //opp.Approved_LT_Facility__c ='TCU';
        personOpp1.Hash_Id__c = '654321';
        personOpp1.SLF_Product__c = prod1.id;
        
        //insert personOpp;
        oppList.add(personOpp1);*/
        
        
        Map<String, String> nonACHAprMap = new Map<String, String>();
        
            String nonACHAprStr;
            Test.starttest();
            
            nonACHAprStr = '{"projects":[{}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"id":"'+oppList[0].Id+'"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"id":"'+oppList[1].Id+'","projectCategory":"Home"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"id":"'+oppList[0].Id+'","term":180,"apr":2.99,"isACH":true}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"id":"'+oppList[0].Id+'","term":180,"apr":2.99,"isACH":true,"nonACHApr":3.24}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"externalId":"'+oppList[0].Partner_Foreign_Key__c+'","term":180,"apr":2.99,"isACH":true,"nonACHApr":3.24}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"hashId":"'+oppList[0].Hash_Id__c+'","term":180,"apr":2.99,"isACH":false,"nonACHApr":3.24}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"hashId":"","term":120,"apr":3.99,"isACH":true,"installStateName":"California","projectCategory":"Home"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"hashId":"","term":120,"apr":3.99,"isACH":false,"installStateName":"California","projectCategory":"Home"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"hashId":"'+opp1.Hash_Id__c+'","term":120,"apr":3.99,"isACH":true,"installStateName":"California","projectCategory":"Home"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"hashId":"'+opp1.Hash_Id__c+'","term":120,"apr":3.99,"isACH":false,"installStateName":"California","projectCategory":"Home"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"hashId":"","term":120,"apr":3.99,"isACH":true,"installStateName":"California","projectCategory":"Home","productType":"HII"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            nonACHAprStr = '{"projects":[{"externalId":"1234","term":120,"apr":3.99,"isACH":true,"installStateName":"California","projectCategory":"Home","productType":"HII"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            opp1.SLF_Product__c = prod2.id;
            update opp1;
            nonACHAprStr = '{"projects":[{"externalId":"1234","term":120,"apr":3.99,"isACH":true,"installStateName":"California","projectCategory":"Home","productType":"HIS"}]}';
            SLFRestDispatchTest.MapWebServiceURI(nonACHAprMap,nonACHAprStr,'getnonachapr');
            
            GetNonACHAprInfoServiceImpl  getNonACHApr = new GetNonACHAprInfoServiceImpl();
            getNonACHApr.fetchData(null);

            
            Test.StopTest();
        } 
    }
}