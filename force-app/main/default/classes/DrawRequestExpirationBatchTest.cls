/******************************************************************************************
* Description: Test class to cover DrawRequestExpirationBatch 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri             06/12/2019             Created
******************************************************************************************/
@isTest 
public class DrawRequestExpirationBatchTest 
{
    static testMethod void testMethod1() 
    {
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgUnderwritingflag = new TriggerFlags__c();
        trgUnderwritingflag.Name ='Underwriting_File__c';
        trgUnderwritingflag.isActive__c =true;
        trgLst.add(trgUnderwritingflag);
        
        TriggerFlags__c fundingTrg= new TriggerFlags__c();
        fundingTrg.Name ='Funding_Data__c';
        fundingTrg.isActive__c =true;
        trgLst.add(fundingTrg);
        
        TriggerFlags__c drawreqTrg = new TriggerFlags__c();
        drawreqTrg.Name ='Draw_Requests__c';
        drawreqTrg.isActive__c =true;
        trgLst.add(drawreqTrg);
        
        TriggerFlags__c userTrgFlag = new TriggerFlags__c();
        userTrgFlag.Name ='User';
        userTrgFlag.isActive__c =true;
        trgLst.add(userTrgFlag);
        
        insert trgLst;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();    
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        Contact con = new Contact(LastName ='testCon',
                                  AccountId = acc.Id,
                                  Email='test@gmail.com');
        insert con; 
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        User portalUser = new User(alias = 'testc', 
                                   Email='testc@mail.com', 
                                   IsActive = true,
                                   Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', 
                                   LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', 
                                   ProfileId = p.Id, 
                                   UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',
                                   ContactId = con.Id);
        insert portalUser;
        
        System.runAs(portalUser){
            
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.CloseDate = system.today();
            insert opp;
            
            Underwriting_File__c undRec = new Underwriting_File__c();           
            undRec.Opportunity__c = opp.id;
            undRec.Approved_for_Payments__c = true;
            undRec.M0_Approval_Date__c = system.Today();
            undRec.Project_Status__c = 'Documents Needed';
            undRec.All_Installers_M2_Queue__c = 'Yes';
            undRec.Title_Review_Complete_Date__c = system.today();
            undRec.Install_Contract_Review_Complete__c = true;
            undRec.Utility_Bill_Review_Complete__c = true;
            undRec.LT_Loan_Agreement_Review_Complete__c = true;
            undRec.Installation_Photos_Review_Complete__c = true;
            undRec.Final_Design_Review_Complete__c = true;   
            undRec.Funding_status__c = 'Under Review by Sunlight Financial';
            
            insert undRec;

            List<Draw_Requests__c> drawList = new List<Draw_Requests__c>();
            DateTime currentDate=system.now();
            currentDate = currentDate.addDays(Integer.ValueOf(System.Label.DrawRequestExpDays));
            for(integer i=0; i<=10; i++)
            {
                Draw_Requests__c drawReqRec = new Draw_Requests__c();
                drawReqRec.Amount__c = 1000;
                drawReqRec.Level__c = '1';
                drawReqRec.Underwriting__c = undRec.id;
                drawReqRec.Status__c = 'Requested';
                drawReqRec.Request_Date__c = currentDate;
                drawList.add(drawReqRec);
            }
            insert drawList;
            system.debug('****Draw Request Records****'+ drawList);
        }
        Test.startTest();
        DrawRequestExpirationBatch drawExBatch = new DrawRequestExpirationBatch();
        DataBase.executeBatch(drawExBatch);
        Test.stopTest();
    }
}