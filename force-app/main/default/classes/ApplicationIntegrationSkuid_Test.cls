@isTest
private class ApplicationIntegrationSkuid_Test 
{   
    @isTest static void test_method_one() 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        Account applicant = new Account(
            Name = 'Applicant',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true
        );
        insert applicant;

        Account coApplicant = new Account(
            Name = 'Co-Applicant',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true
        );
        insert coApplicant;

        Opportunity opp = new Opportunity(
            Name = 'Test Opportunity',
            StageName = 'Qualified',
            CloseDate = Date.today().addDays(-3),
            FNI_Application_Id__c = '',
            AccountId = applicant.Id,
            Co_Applicant__c = coApplicant.Id
        );
        insert opp;

        list<ApplicationIntegrationSkuid.Input> ilist = new list<ApplicationIntegrationSkuid.Input>{};

        ApplicationIntegrationSkuid.Input i1 = new ApplicationIntegrationSkuid.Input(opp.Id);
        ilist.add (i1);

        list<ApplicationIntegrationSkuid.Output> olist = ApplicationIntegrationSkuid.ApplicationIntegrationSkuid (ilist);
    }
}