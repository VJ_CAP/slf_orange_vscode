/**
* Description: Applicant API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh            07/10/2017          Created
******************************************************************************************/
public with sharing class ApplicantObjServiceImpl extends RESTServiceBase{  
    private String msg = '';
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ApplicantObjServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        ApplicantObjDataServiceImpl CrdtDataSrvImpl = new ApplicantObjDataServiceImpl(); 
        StatusDataObject stsDtObj = new StatusDataObject();
        IRestResponse iRestRes;
        
        try{
            system.debug('****rw.reqDataStr***'+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('****reqData ***'+reqData);
            
            res.response = CrdtDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
           
            system.debug('### ApplicantObjServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' ApplicantObjServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            
            UnifiedBean crdtben = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214',null);
            errorWrapperLst.add(errorWrapper);
            crdtben.error  = errorWrapperLst ;
            //crdtben.error.add(errorMsg);
            crdtben.returnCode = '214';
            iRestRes = (IRestResponse)crdtben;
            res.response= iRestRes;
        }
       
        system.debug('### Exit from fetchData() of ' + ApplicantObjServiceImpl.class);
        
        return res.response ;
    }
    
    
}