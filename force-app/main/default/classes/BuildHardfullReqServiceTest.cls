@isTest
public class BuildHardfullReqServiceTest{
    @testsetup static void createtestdata()
    {
       SLFUtilityDataSetup.initData();
    } 
    
    /**
    * Description: used for Map Webservice URI for any Object
    */
    Public Static void MapWebServiceURI(Map<String, String> reqMap,string jsonString,string uriPeram){
        system.debug('*******reqMapstart****'+reqMap);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        system.debug('*******reqMap****'+reqMap);
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
            system.debug('#########req.requestBody###############'+jsonString);
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();

            for(String str : reqMap.Keyset()){
                gen.writeStringField(str,reqMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
            system.debug('#########req.requestBody###############'+gen.getAsString());
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
      //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }

   
     private testMethod static void TransferOwnershipTest(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        opp.Hash_Id__c = loggedInUsr[0].Hash_Id__c;
        update opp;
        Product__c prod = [select Id,Name,Product_Tier__c,Term_mo__c,APR__c from Product__c where Name =: 'testprod' LIMIT 1];
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1];      
        
        System.runAs(loggedInUsr[0])
        {            
            Map<String, String> buildHardfullMap = new Map<String, String>();
            //creditRefactor with single Applicant
            
            String buildHardfull ;
            Test.startTest();
              buildHardfull = '{"projects":[{"id":"'+opp.Id+'","credits":[{"id":"'+cred.Id+'"}]}]}'; 
              MapWebServiceURI(buildHardfullMap,buildHardfull,'rbpbuildhardfullxml');
            
              buildHardfull = '{"projects":[{"id":"","credits":[{"id":""}]}]}'; 
              MapWebServiceURI(buildHardfullMap,buildHardfull,'rbpbuildhardfullxml');
            
              buildHardfull = '{"projects": "weeee",e4terte}'; 
              MapWebServiceURI(buildHardfullMap,buildHardfull,'rbpbuildhardfullxml');
             Test.stopTest();
           
        }
        
    }
    

}