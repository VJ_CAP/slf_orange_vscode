public class fni_Lead_Extension {
   private ApexPages.StandardController stdController;
    private Id leadId;
   // public String redirectUrl {public get; private set;}
    //public String redirectUrlSF1 {public get; private set;}
    public String redirectUrlSF1App {public get; private set;}    
   // public String message {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}

    public fni_Lead_Extension(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.leadId = stdController.getRecord().Id;
        shouldRedirect = false;
    }

  /*  public PageReference callFNI() {
        String res = fniCalloutManual.buildRequest(this.leadId);
        message = String.escapeSingleQuotes(res);
        shouldRedirect = true;
        //redirectUrl = stdController.view().getUrl();
        redirectUrlSF1App = this.leadId;
        //redirectUrlSF1 = 'salesforce1://sObject/' + this.LeadId + '/view';
        return null;
    }*/
    
    public PageReference creditConsent(){
        Lead thisLead = [Select Id, Customer_has_authorized_credit_soft_pull__c FROM Lead WHERE Id =:this.leadId];
        thisLead.Customer_has_authorized_credit_soft_pull__c = true;
        update thisLead;
        shouldRedirect = true;
        redirectUrlSF1App = this.leadId;
        //this.stdController.save();
        return null;
    }
    
    public void save(){
        this.stdController.save();
    }
    
}