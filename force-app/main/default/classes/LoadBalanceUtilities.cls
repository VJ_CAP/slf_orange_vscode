public class LoadBalanceUtilities {
   
    public static String getFacility(String installerName,String state, Map<String,String> mapDecision,Integer term){
        System.debug('### Entered into getFacility() of '+LoadBalanceUtilities.class);
        System.debug('###mapDecision'+mapDecision);
        String facility ='';
        //Map<String,String> mapDecision = new Map<String,String>();
        Map<String,String> facilityDecisionMap = new Map<String,String>();
        
        //Only one PrequalDecisions
        if(mapDecision!=null && mapDecision.size()==1){
            for(String sFacility : mapDecision.keySet()){
                facility = sFacility;
            }
            return facility;
        }
        //Multiple PrequalDecisions with A
        else if(mapDecision!=null && mapDecision.size()>1){
            //Prequal Decisions with Decision = A
            if(mapDecision.values().contains('A')){
                for(String sFacility : mapDecision.keySet()){
                    //Process only prequal decision with A and ignore all
                    if(mapDecision.get(sFacility) == 'A'){
                        facilityDecisionMap.put(sFacility,mapDecision.get(sFacility));
                    }
                }
            }
            //Prequal Decisions with Decision = P and without A
            else if(mapDecision.values().contains('P') && !mapDecision.values().contains('A')){
                
                //Process only prequal decision with P and ignore all
                for(String sFacility : mapDecision.keySet()){
                    if(mapDecision.get(sFacility) == 'P'){
                        facilityDecisionMap.put(sFacility,mapDecision.get(sFacility));
                    }
                }
            }
            //Prequal Decisions with Decision = D without Decision A and P
            else if(mapDecision.values().contains('D') && !mapDecision.values().contains('A') && !mapDecision.values().contains('P')){
                //Process only prequal decision with D and ignore all
                for(String sFacility : mapDecision.keySet()){
                    if(mapDecision.get(sFacility) == 'D'){
                        facilityDecisionMap.put(sFacility,mapDecision.get(sFacility));
                    }
                }
            }
        }
        if(facilityDecisionMap!=null && facilityDecisionMap.size()==1){
            for(String sFacility : facilityDecisionMap.keySet()){
                facility = sFacility;
            }
            return facility;
        }
        System.debug('installerName,state,facilityDecisionMap,term'+installerName+state+facilityDecisionMap+term);
        facility = processFacilities(installerName,state,facilityDecisionMap,term);
        System.debug('###facility :'+facility);
       return facility;
    }
    
  
    public static String processFacilities(String installerName,String state, Map<String,String> facilityDecisionMap,Integer term){
        System.debug('### Entered into processFacilities() of '+LoadBalanceUtilities.class);
        String facility ='';
        
        List<String> lstApprovedFacilities = new List<String>(facilityDecisionMap.keySet()); // List of approved facilities from FNI response
       
        //set<String> customFieldName = new set<String>();
        set<String> FacilityFieldSet = new set<String>(); //Facility fields set
       // set<String> AllocationFieldSet = new set<String>(); // Facility Allocation fields set
        Map<String,String> FacilityFiledMap = new Map<string,String>();
        
        String QueryString = 'Select ';
        for(String FieldName : State_Allocation__c.SObjectType.getDescribe().fields.getMap().keySet()){
            if(FieldName.contains('__c')||FieldName=='id'){
                QueryString+=FieldName+',';
                //customFieldName.add(FieldName);
            }
            if(FieldName.contains('facility_') && !FieldName.contains('_allocation__')){
                FacilityFieldSet.add(FieldName);
                FacilityFiledMap.put(FieldName,FieldName);
            }
            
            /*          
            if(FieldName.contains('_allocation__'))
                AllocationFieldSet.add(FieldName);
            */
        }
        if(!FacilityFieldSet.isEmpty()){
            
            for(integer i=1;i<=FacilityFieldSet.size();i++){
                string fieldval = FacilityFiledMap.get('facility_'+i+'__c');
                if(fieldval == 'facility_'+i+'__c'){
                    QueryString+=fieldval.removeEnd('c')+'r.FNI_Domain_Code__c,';
                }
            }
        }
        
        QueryString = QueryString.removeEnd(',');
        QueryString = QueryString+' from State_Allocation__c where INstaller_Account__c=:INstallerName AND State_Code__c=:state ';        
        if(!FacilityFieldSet.isEmpty()){
            QueryString+='AND (';
           
            for(integer i=1;i<=FacilityFieldSet.size();i++){
                string tempfieldval = 'facility_'+i+'__c';
                string fieldval = FacilityFiledMap.get(tempfieldval);
                if(fieldval == 'facility_'+i+'__c'){
                    if(i==1)
                        QueryString+=fieldval.removeEnd('c')+'r.FNI_Domain_Code__c IN : lstApprovedFacilities ';
                    else
                        QueryString+='OR '+fieldval.removeEnd('c')+'r.FNI_Domain_Code__c IN : lstApprovedFacilities ';
                }
                
            }
            QueryString+=')';
        }
        System.debug('### QueryString: '+QueryString);
        Map<String,Integer> facilityAllocationMap = new Map<String,Integer>();
        Map<String,Integer> facilityYearAllocationMap = new Map<String,Integer>();//Added By Adithya as part of 3966
        
        List<State_Allocation__c> lstStateAllocation = Database.query(QueryString);
        List<String> lstStateAllocationFacility = new list<String>();
        system.debug('**FacilityFieldSet**'+FacilityFieldSet);
        system.debug('**FacilityFiledMap**'+FacilityFiledMap);
        boolean isYearAllocationExist = false;
        if(lstStateAllocation!=null && lstStateAllocation.size()>0){
           
            //Added By Adithya as part of 3966
            if(null != term)
                term = term/12;
            
            system.debug('**term**'+term);
            
            for(State_Allocation__c stateAllocation : lstStateAllocation){
               
                if(!FacilityFieldSet.isEmpty()){
                   
                    for(Integer i=1; i<=FacilityFieldSet.size(); i++){
                        Integer allocationval = 0;
                        Integer yearAllocationVal = 0;//Added By Adithya as part of 3966
                        if(stateAllocation.get('facility_'+i+'_allocation__c') != null)
                            allocationval = Integer.valueOf(stateAllocation.get('facility_'+i+'_allocation__c'));
                        
                        //Added By Adithya as part of 3966
                        //start
                        system.debug('**term**'+term);
                        if(null != stateAllocation.get('X'+term+'_Year_Allocation_Total__c') && stateAllocation.get('X'+term+'_Year_Allocation_Total__c') != 0)
                        {
                            isYearAllocationExist = true;
                            if(null != stateAllocation.get('Facility_'+i+'_'+term+'_Year_Allocation__c') && null != stateAllocation.get('facility_'+i+'_allocation__c'))
                            {
                                Integer yearAllocation = Integer.valueOf(stateAllocation.get('facility_'+i+'_allocation__c')) * Integer.valueOf(stateAllocation.get('Facility_'+i+'_'+term+'_Year_Allocation__c'));
                                if(null != yearAllocation)
                                {
                                    yearAllocationVal = yearAllocation/100;
                                }
                            }
                        }
                        //End
                        
                        String facilityval = '';
                        if(stateAllocation.getSobject('facility_'+i+'__r') != null && stateAllocation.getSobject('facility_'+i+'__r').get('FNI_Domain_Code__c')!= null)
                            facilityval = (String)stateAllocation.getSobject('facility_'+i+'__r').get('FNI_Domain_Code__c');
                        if(facilityval != '' && lstApprovedFacilities.contains(facilityval)){
                            if(isYearAllocationExist)
                            {
                                if(yearAllocationVal != 0)
                                    lstStateAllocationFacility.add(facilityval);
                            }else{
                                lstStateAllocationFacility.add(facilityval);
                            }
                            facilityAllocationMap.put(facilityval,allocationval);
                            if(yearAllocationVal != 0)
                            {
                                facilityYearAllocationMap.put(facilityval,yearAllocationVal);//Added By Adithya as part of 3966
                            }                               
                        }
                    }
                }
            }
        }
        system.debug('### facilityAllocationMap: '+facilityAllocationMap);
        system.debug('### lstStateAllocationFacility: '+lstStateAllocationFacility);
        system.debug('### facilityYearAllocationMap: '+facilityYearAllocationMap);
        Integer randomNum;
        if(facilityAllocationMap.isEmpty()){
            return 'NoRecords';
        }else{
            Integer startRangeNum = 1;
            Integer endRangeNum =0;
            
            //Added By Adithya as part of 3966
            //start
            if(isYearAllocationExist)
            {
                 for(integer ints: facilityYearAllocationMap.values()){
                    endRangeNum = endRangeNum+ints;
                }
            }else{
                for(integer ints: facilityAllocationMap.values()){
                    endRangeNum= endRangeNum+ints;
                }   
            }
            //End
            system.debug('### startRangeNum: '+startRangeNum);
            system.debug('### endRangeNum: '+endRangeNum);

            // Generate random number based on facility allocation percentage values
            Double randomNumbInDoble = Math.floor(Math.random() * ((endRangeNum-startRangeNum)+1) + startRangeNum);
            randomNum = randomNumbInDoble.intValue();
            system.debug('### RandomNum: '+randomNum);
            integer tempVal = 0;
            
             for(String facilityName: lstStateAllocationFacility){
                integer facilityAllocation; 
                if(isYearAllocationExist)
                    facilityAllocation = facilityYearAllocationMap.get(facilityName);
                else
                    facilityAllocation = facilityAllocationMap.get(facilityName);
                
                if(tempVal==0){
                    if(randomNum > 0 && randomNum <= facilityAllocation){
                        facility = facilityName;
                        break;
                    }else{
                        tempVal = tempVal + facilityAllocation;
                    }
                }else{
                    if(randomNum>=tempVal+1 && randomNum <= tempVal+facilityAllocation){
                        facility=facilityName;
                        break;
                    }else{
                        tempVal = tempVal + facilityAllocation;
                    }
                }
            }
        }
        System.debug('### facility1--'+facility);
        System.debug('### Exit from processFacilities() of '+LoadBalanceUtilities.class); 
        String facilitywithRandomNo = facility+':::'+randomNum;
        System.debug('### facilitywithRandomNo--'+facilitywithRandomNo);
        return facilitywithRandomNo;
    }
}