/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Sejal Bhatt             27/08/2019              Created
******************************************************************************************/

global class UpdateCaseResendEmail {
    global class Input {
        @InvocableVariable(required=true) global Id stipId;
        global Input() {
            //  nothing to do
        }
        global Input(Id stipId) {
            this.stipId = (String.valueOf(stipId));
        }
    }
    global class Output {   
        @InvocableVariable global String Message;
    }
    
     /**
    * Description: Method to check Case.Resend_Email__c field from SKUID Page
    */
    @InvocableMethod(label='updateCaseResendEmailField')
    public static List<Output> updateCase(List<Input> lstInput){
        try{
            system.debug('### Entered into updateCase() of '+ UpdateCaseResendEmail.class);
            List<Output> lstOutPuts = new List<Output>();  
            Output objOutput = new Output();    
            Set<Id> stipIds = new Set<Id>();
            
            for(input objI:lstInput){
                stipIds.add(objI.stipId);
            }
            /*
            //Commented by Adithya
            //Start
            Stipulation__c stipObj = [select id,Case__c from Stipulation__c where id in:stipIds limit 1];
            Case caseObj = [select Id from Case where Id =:stipObj.Case__c limit 1];
            if(caseObj != null){
                caseObj.Resend_Email__c = true;
                update caseObj;
            }
            //End
            */
            //Updated By Adithya on 9/25/2019
            //Start
            List<Stipulation__c> stipList = [select id,Case__c,status__c,Underwriting__r.Opportunity__r.Primary_Applicant_Email__c,Underwriting__r.Opportunity__r.Sales_Representative_Email__c from Stipulation__c where id IN: stipIds];
            if(!stipIds.isEmpty())
            {
                if(null != stipList[0].Case__c)
                {
                    Case caseRec = new Case(id = stipList[0].Case__c);
                    caseRec.Resend_Email__c = true;
                    caseRec.Stipulation_Status__c = stipList[0].status__c;
                    caseRec.Stip_Email_1__c = stipList[0].Underwriting__r.Opportunity__r.Primary_Applicant_Email__c;
                    caseRec.Sales_Representative_Email__c = stipList[0].Underwriting__r.Opportunity__r.Sales_Representative_Email__c ;
                    update caseRec;
                }
                
            }
            //End
       }catch(Exception ex)
        {
            system.debug('### UpdateCaseResendEmail.updateCase():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('UpdateCaseResendEmail.updateCase()',ex.getLineNumber(),'updateCase()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
            system.debug('### Exited from updateCase() of '+ UpdateCaseResendEmail.class);
            return null;
     }
}