@isTest
private class ApplicationIntegrationUtilityTest {
    private static Boolean runalltests = true;

    @testSetup static void createTestData(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        insert trgLst;
        
        SLF_Boomi_API_Details__c slfIntDetials = new SLF_Boomi_API_Details__c();
        slfIntDetials.Name = 'Application Integration';
        slfIntDetials.Username__c = 'EXa2Ns4nkI3/eO28jtW6Fdj/24lyCKRICcIWFvpDpAU=';
        slfIntDetials.Password__c = '0ZyMD44G4iGqjtZ6kGUOOBHBbJY3P4VYd5A8iRI6gv8=';
        slfIntDetials.Endpoint_URL__c = 'https://sfws.sunlightfinancial.com/ws/ApplicationIntegration.svc/json/';
        slfIntDetials.Timeout__c = 60000;
        insert slfIntDetials;
        
        //SFSettings__c slfset = SFSettings__c();
        //slfset.AES_Encryption_Key__c = 'iasdgsagdasdksakdh72379217adoasl';
        //insert slfset;
        
        RecordType rt1 = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'PersonAccount' LIMIT 1];

        Account applicant = new Account(
            Lastname = 'Applicant',
            RecordTypeId = rt1.Id
        );
        insert applicant;

        Account coApplicant = new Account(
            Lastname = 'Co-Applicant',
            RecordTypeId = rt1.Id
        );
        insert coApplicant;

        Opportunity opp = new Opportunity(
            Name = 'Test Opportunity',
            AccountId = applicant.Id,
            StageName = 'Qualified',
            CloseDate = Date.today().addDays(-3),
            FNI_Application_Id__c = '11111'
        );
        insert opp;
    }

    @isTest
    private static void testApiError() {
        if(!runalltests) { return; }

        Opportunity opp = [SELECT Id, FNI_Application_Id__c FROM Opportunity LIMIT 1];

        Test.setMock(HttpCalloutMock.class, new ApplicationIntegrationCalloutMock(400));

        Test.startTest();

        try{
            SLF_Boomi_API_Details__c slfIntDetials = [select id,Name,Username__c,Password__c,Endpoint_URL__c,Timeout__c from SLF_Boomi_API_Details__c limit 1];
            ApplicationIntegrationUtility.objIntegrationDetails = slfIntDetials;
            ApplicationIntegrationUtility.updateRecordFromIntegration(opp.Id, opp.FNI_Application_Id__c);
        } catch(Exception ex){
            System.assert(ex.getMessage().contains('Could not connect to server; please Contact Sunlight Financial for help.'));
        }

        Test.stopTest();
    }

    @isTest
    private static void testApiSuccess() {
        if(!runalltests) { return; }
        
        List<Account> accounts = [SELECT Id, Name FROM Account];

        Opportunity opp = [SELECT Id, FNI_Application_Id__c, AccountId, Co_Applicant__c FROM Opportunity LIMIT 1];
        opp.AccountId = (accounts[0].Name == 'Applicant') ? accounts[0].Id : accounts[1].Id;
        opp.Co_Applicant__c = (accounts[0].Name == 'Applicant') ? accounts[1].Id : accounts[0].Id;
        update opp;

        Test.setMock(HttpCalloutMock.class, new ApplicationIntegrationCalloutMock(200));

        Test.startTest();

        ApplicationIntegrationUtility.updateRecordFromIntegrationFuture(opp.Id, opp.FNI_Application_Id__c);

        Test.stopTest();
    }
}