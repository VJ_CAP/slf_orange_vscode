global class batchrebuildSharesForOppsModified implements Database.Batchable<sObject>{
global Database.QueryLocator start(Database.BatchableContext bc) {
    return Database.getQueryLocator('SELECT Id FROM Opportunity');
}

global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
   set<Id> ids = new set<Id>{};
   for(Opportunity Opp : scope)
   {
       ids.add(Opp.Id);
   }
   OpportunitySharingSupport.rebuildSharesForOpportunitiesModified(ids);
   OpportunitySharingSupport.rebuildSharesForAccountsModified(ids);
}
global void finish(Database.BatchableContext BC){
    system.debug('###Completed###');
}
}