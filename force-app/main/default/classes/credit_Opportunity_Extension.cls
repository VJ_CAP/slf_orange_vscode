public class credit_Opportunity_Extension {
    private ApexPages.StandardController stdController;
    private Id opptyId;
   // public String redirectUrl {public get; private set;}
    //public String redirectUrlSF1 {public get; private set;}
    public String redirectUrlSF1App {public get; private set;}    
   // public String message {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
    public String message {public get; private set;}
    public Boolean hasError {public get; private set;}

    public credit_Opportunity_Extension(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        this.opptyId = stdController.getRecord().Id;
        shouldRedirect = false;
    }

    
    public PageReference getQuotes(){
        Opportunity oppty = [SELECT Id, Sighten_UUID__c, Install_State_Code__c FROM Opportunity where Id = :opptyId];
        if(oppty != null){
            try{
                sightenCallout.getSightenQuotes(oppty.Sighten_UUID__c, oppty.Id, oppty.Install_State_Code__c);
        		//update oppty;
                this.message = 'The Sighten quotes have been successfully added to this Opportunity!';
            } catch(Exception e){
                this.message = e.getMessage();
                this.hasError = true;
            }
            
        }
        shouldRedirect = true;
        redirectUrlSF1App = oppty.Id;
        //this.stdController.save();
        return null;
    }
    
    public PageReference runCredit(){
   		Opportunity oppty = [SELECT Id, SLF_Product__c, SyncedQuoteId, Customer_has_authorized_credit_hard_pull__c, Application_Status__c, Install_State_Code__c FROM Opportunity where Id = :opptyId];
        String exceptionMsg = '';
        if(oppty.Customer_has_authorized_credit_hard_pull__c == false){
            this.message = 'Customer must authorize Credit Pull';
        }else{ 
            if(oppty.SyncedQuoteId == null){
                if(oppty.SLF_Product__c == null) exceptionMsg += 'A product or quote must be selected to run credit, or the state selected may not have any products available';    
            }else{
                if(oppty.SLF_Product__c == null) exceptionMsg += 'The quote selected does not have a valid Sunlight product associated, please select a product';   
            }   
            if(exceptionMsg == ''){
                try{
                    fniCalloutManual.buildHardCreditRequest(oppty.Id);
                    this.message = 'Credit was successfully run!';
                }catch(Exception e){
                    System.debug('***Debug: e = ' + e);
                    this.message = e.getMessage();
                    this.hasError = true;
                }
            }else{           
                this.message = exceptionMsg;
            }
        }
        System.debug('***Debug: this.message = ' + this.message);
        shouldRedirect = true;
        redirectUrlSF1App = oppty.Id;
        return null;
    }
    
    public PageReference consentAndRunCredit(){
        Opportunity oppty = [SELECT Id, SLF_Product__c, SyncedQuoteId, Customer_has_authorized_credit_hard_pull__c, Application_Status__c, Install_State_Code__c FROM Opportunity where Id = :opptyId];
        oppty.Customer_has_authorized_credit_hard_pull__c = true;
        update oppty;
        shouldRedirect = true;
        redirectUrlSF1App = oppty.Id;
        
        //runCredit();
        return null;
    }
    
    public void save(){
        this.stdController.save();
    }
    
}