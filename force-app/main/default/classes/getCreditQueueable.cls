public class getCreditQueueable implements Queueable, Database.AllowsCallouts{

    private ID opptyID;
    
    public getCreditQueueable(ID opptyID){
        this.opptyID = opptyID;

    }
    
    public void execute(QueueableContext context) {
        fniCalloutManual.buildHardCreditRequest(opptyID);
    }
}