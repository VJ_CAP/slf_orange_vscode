@isTest 
private class DefaultTriggerHandler_Test{
    
    static testMethod void defaultTriggerHandlerMethod() {
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
           TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        insert trgLst;
        
         //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Account acc1 = new Account();
        acc1.name = 'Test Account1';
        acc1.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc1.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.BillingStateCode = 'CA';
        acc1.FNI_Domain_Code__c = 'CRB';
        acc1.Installer_Legal_Name__c='Bright Solar Planet';
        acc1.Solar_Enabled__c = true; 
        insert acc1;      
            
        Account acc2 = [SELECT Id FROM Account WHERE Name = 'Test Account1' LIMIT 1]; 
        
        
       
        
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        //User pu = getPortalUser('Partner Community User for Salesforce1', null, false);
        
        User u = [Select id, name, email, contactId, Contact.AccountId, Contact.Account.Owner.UserRoleId from User where ProfileId = :p.Id limit 1];
        
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        //test Data for CRB Facility 
        
        
               
        Contact con1 = new Contact(LastName ='testCon1',AccountId = acc1.Id);
        insert con1;  
        
        system.debug('***Role is***'+u.Contact.Account.Owner.UserRoleId);
        User portalUser1 = new User(alias = 'testc', Email='testc1@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test1', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = acc1.CreatedBy.UserRoleId,
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass1@mail.com',ContactId = con1.Id);
        insert portalUser1;
        
        system.debug('@@ I am here' +portalUser1.Contact.AccountId);

        acc1.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc1.BillingStateCode = 'CA';
        update acc1;
        
        System.runAs (portalUser1)
        { 
        
        //Insert Account record
        Account personAcc1 = new Account();
        personAcc1.Name = 'SLFTest';
        personAcc1.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc1.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc1.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc1.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc1.BillingStateCode = 'CA';
        personAcc1.Installer_Legal_Name__c='Bright Solar Planet';
        personAcc1.Solar_Enabled__c = true; 
        insert personAcc1;
        
        Contact personcon1 = new Contact(LastName ='testCon1',AccountId = personAcc1.Id);
        insert personcon1; 
        
        personAcc1 = [select Id from Account where Id=: personAcc1.Id];
        
        personAcc1.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc1;
        
        
            
            //Insert SLF Product Record
            Product__c prod1 = new Product__c();
            prod1.Installer_Account__c =acc1.id;
            prod1.Long_Term_Facility_Lookup__c =acc1.id;
            prod1.Short_Term_Facility_Lookup__c = acc1.id;
            prod1.Name='testprod1';
            prod1.FNI_Min_Response_Code__c=9;
            prod1.Product_Display_Name__c='test';
            prod1.Qualification_Message__c ='testmsg';
            prod1.State_Code__c ='CA';
            prod1.ST_APR__c = 88;
            prod1.Long_Term_Facility__c = '';
            prod1.APR__c=87;
            //prod1.ACH_APR__c=87;
            prod1.NonACH_APR__c=87;
            prod1.Internal_Use_Only__c = true;

            prod1.LT_Max_Loan_Amount__c =1000;
            prod1.Term_mo__c = 1;
            prod1.Product_Tier__c = '0';
            insert prod1;
            Test.startTest();
            //Insert opportunity record
            Opportunity personOpp1 = new Opportunity();
            personOpp1.name = 'personOpp1';
            personOpp1.CloseDate = system.today();
            personOpp1.StageName = 'Qualified';
            personOpp1.AccountId = personAcc1.id;
            personOpp1.Installer_Account__c = acc1.id;
            personOpp1.Install_State_Code__c = 'CA'; 
            personOpp1.Combined_Loan_Amount__c =10000;
            personOpp1.Partner_Foreign_Key__c = 'CRB||12345';
            //opp1.Approved_LT_Facility__c ='CRB';
            personOpp1.SLF_Product__c = prod1.id;
            
             system.debug('##'+acc1.Id+'**'+personAcc1.id);
             
            insert personOpp1;
            
            //Insert opportunity record
            Opportunity opp1 = new Opportunity();
            opp1.name = 'OpName';
            opp1.CloseDate = system.today();
            opp1.StageName = 'Qualified';
            opp1.AccountId = acc1.id;
            opp1.Installer_Account__c = acc1.id;
            opp1.Install_State_Code__c = 'CA'; 
            opp1.Combined_Loan_Amount__c = 10000;
            opp1.Partner_Foreign_Key__c = 'CRB||123';
            //opp1.Approved_LT_Facility__c = 'CRB';
            opp1.SLF_Product__c = prod1.id;
            opp1.Opportunity__c = personOpp1.Id;
            
            opp1.Application_Status__c = 'Auto Approved';
            opp1.Monthly_Payment__c = 100;
            opp1.Long_Term_Amount_Approved__c = 60000;
            opp1.short_term_loan_amount_manual__c = 20;
            opp1.long_term_loan_amount_manual__c = 100;
            
            insert opp1;
            
            
            personOpp1.Opportunity__c = opp1.Id;
            update personOpp1;
      
        Map<Id,Account> conMap = new Map<Id,Account>();
        conMap.put(acc2.Id,acc2);
        DefaultTriggerHandler obj = new DefaultTriggerHandler();
        obj.beforeInsert(conMap.values());
        obj.beforeUpdate(conMap,conMap);
        obj.beforeDelete(conMap);
        obj.afterInsert(conMap);
        obj.afterUpdate(conMap,conMap);
        obj.afterDelete(conMap);
        obj.afterUndelete(conMap);
        
        Map<Id,Opportunity> conMap1 = new Map<Id,Opportunity>();
        conMap1.put(opp1.Id,opp1); 
        OpportunityTriggerManager obj2 = new  OpportunityTriggerManager();
        obj2.beforeInsert(conMap1.values());
        obj2.beforeUpdate(conMap1,conMap1);
        obj2.beforeDelete(conMap1);
        obj2.afterInsert(conMap1);
        obj2.afterUpdate(conMap1,conMap1);
        obj2.afterDelete(conMap1);
        obj2.afterUndelete(conMap1);
        
        
    }
}
}