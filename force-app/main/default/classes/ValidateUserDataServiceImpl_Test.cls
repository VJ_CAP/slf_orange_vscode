/******************************************************************************************
* Description: Test class to cover ValidateUserDataServiceImpl
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri             09/17/2019             Created
******************************************************************************************/
@isTest
public class ValidateUserDataServiceImpl_Test{    
    
    @testsetup static void createtestdata(){
        //SLFUtilityDataSetup.initData();
       testDataInsert();
        
    }
    private static void testDataInsert(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
         TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        insert trgLst;
        
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.Credit_Run__c = true;
        acc.Default_FNI_Loan_Amount__c = 25000;
        acc.Risk_Based_Pricing__c = true;
        acc.Credit_Waterfall__c = true;
        acc.Eligible_for_Last_Four_SSN__c = true;
        acc.Phone = '9874563210';
        acc.Push_Endpoint__c = 'www.google.com';
        acc.Subscribe_to_Push_Updates__c = true;
        acc.M1_Approval_Days__c = 90;
        acc.M2_Approval_Days__c = 90;
        insert acc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c=2.99;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        insert prod;
        
        //Insert opportunity record
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = acc.id;
        personOpp.Installer_Account__c = acc.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Install_Street__c ='Street';
        personOpp.Install_City__c ='InCity';
        personOpp.Combined_Loan_Amount__c =10000;
        personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
        //opp.Approved_LT_Facility__c ='TCU';
        personOpp.SLF_Product__c = prod.id;            
        insert personOpp;
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Street__c ='Street';
        opp.Install_City__c ='InCity';
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'TCU||123';
        //opp.Approved_LT_Facility__c = 'TCU';
        opp.SLF_Product__c = prod.id;
        opp.Opportunity__c = personOpp.Id; 
        opp.Block_Draws__c = false;
        opp.Has_Open_Stip__c = 'False';
        opp.All_HI_Required_Documents__c=true;
        opp.Max_Draw_Count__c=3;
        opp.ProductTier__c = '0';         
        opp.Type_of_Residence__c  = 'Own';
        opp.language__c = 'Spanish';
        opp.Project_Category__c = 'Solar';
        insert opp;
    }
 
/**
*
* Description: This method is used to cover ValidateUserDataServiceImpl
*
*/
    private testMethod static void ValidateUserDataServiceTest(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com' limit 1];
        System.runAs(loggedInUsr[0])
        { 
            Test.startTest();
            string ValidateUserDataJsonReq;
            Map<String, String> ValidateUserDataMap = new Map<String, String>();
            
            ValidateUserDataJsonReq = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"sales manager - all reps","role":"Partner Manager","reportsTo":""}]}';
            MapWebServiceURI(ValidateUserDataMap,ValidateUserDataJsonReq,'validateuser');
            
            ValidateUserDataJsonReq = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"","phone":"","isActive":true,"title":"sales manager - all reps","role":"Partner Manager","reportsTo":""}]}';
            MapWebServiceURI(ValidateUserDataMap,ValidateUserDataJsonReq,'validateuser');
            
            ValidateUserDataJsonReq = '{"users": null}';
            MapWebServiceURI(ValidateUserDataMap,ValidateUserDataJsonReq,'validateuser');
            
            ValidateUserDataJsonReq = 'null';
            MapWebServiceURI(ValidateUserDataMap,ValidateUserDataJsonReq,'validateuser'); 
            
            //To cover exception block
            ValidateUserServiceImpl validateuserTest = new ValidateUserServiceImpl();
            validateuserTest.fetchData(null);
            
            Test.stopTest();            
        }
    }
        
    private testMethod static void ValidateUserAPITest(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1]; 
        Product__c prod = [select Id,Name from Product__c where Name =: 'testprod' LIMIT 1];
        
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            Map<String, String> validateUserMap = new Map<String, String>();
            
            String validateUser;
            
            //for serviceImpl 100% coverage
            validateUser = '{"email: "'+ loggedInUsr[0].email +'"}';
            MapWebServiceURI(validateUserMap,validateUser,'validateuser');
            
            //validateUser with email details.
            validateUser = '{"email": "'+ loggedInUsr[0].email +'"}';
            MapWebServiceURI(validateUserMap,validateUser,'validateuser');
            
            //validateUser with email details.
            validateUser = '{"userInfo": {"email": "'+ loggedInUsr[0].email +'"  }}';
            MapWebServiceURI(validateUserMap,validateUser,'validateuser');
            
            //validateUser with email details.
            validateUser = '{"userInfo": {"email": "testc1@mail.com"  }}';
            MapWebServiceURI(validateUserMap,validateUser,'validateuser');
            
            //validateUser with wrong email details -  // error scnerio
            validateUser = '{"email": ""}';
            MapWebServiceURI(validateUserMap,validateUser,'validateuser');
            
            
            
            Test.stopTest();
        }
    }

    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}