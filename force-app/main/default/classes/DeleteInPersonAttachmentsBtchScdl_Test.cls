@istest
public class DeleteInPersonAttachmentsBtchScdl_Test {
    private static testMethod void test() {   
        List<TriggerFlags__c> trgrFlgsList = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgrFlgsList.add(trgAccflag);
        
        TriggerFlags__c trgoppflag = new TriggerFlags__c();
        trgoppflag.Name ='Opportunity';
        trgoppflag.isActive__c =true;     
        trgrFlgsList.add(trgoppflag);
        insert trgrFlgsList;
    
        Account acc = new Account();
        acc.name = 'Test Account';
        insert acc;
        
        set<Id> aId = new set<id>();
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.AccountId = acc.id;
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';        
        insert opp;
        
        Attachment attach=new Attachment();     
        attach.Name='In Person Loan Agreement';
        Blob bodyBlob=Blob.valueOf('In Person Loan Agreement');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;                 
          
        Test.startTest();      
        DeleteInPersonAttachmentsBtchScdl  sch1 = new DeleteInPersonAttachmentsBtchScdl(new Set<Id>{opp.Id}); 
        String sch = '0 0 8 13 2 ?'; 
        System.schedule('Test Delete Attachments', sch, sch1); 
        Test.stopTest();   
        
        List<Attachment> att = [select id, name from Attachment where parent.id =: opp.id];
        System.assertEquals(1, att.size());
    }      
}