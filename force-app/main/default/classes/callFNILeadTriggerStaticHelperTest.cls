/*
* Description: This method is used to cover callFNILeadTriggerStaticHelper
*
*/
@isTest
public class callFNILeadTriggerStaticHelperTest {
    @testSetup private static void createTestData(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
                TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        insert trgLst;
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c=2.99;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        insert prod;
        
         //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Street__c ='Street';
        opp.Install_City__c ='InCity';
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'TCU||123';
        //opp.Approved_LT_Facility__c = 'TCU';
        opp.SLF_Product__c = prod.id;
      //  opp.Opportunity__c = personOpp.Id; 
        opp.Block_Draws__c = false;
        opp.Has_Open_Stip__c = 'False';
        opp.All_HI_Required_Documents__c=true;
        opp.Max_Draw_Count__c=3;
        opp.ProductTier__c = '0';         
        opp.Type_of_Residence__c  = 'Own';
        opp.language__c = 'Spanish';
        opp.Project_Category__c = 'Solar';
        insert opp;
        
    }
    private testMethod static void callFNILeadTriggerStaticHelperTest(){
        
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Reset_Password_Flag__c from User where Email =: 'testc@mail.com'];
        Lead lead = new Lead();
        lead.FirstName = 'Test';
        lead.LastName = 'FNI';
        lead.Street ='steee';
        lead.City = 'cal';
        lead.StateCode ='CA';
        lead.PostalCode ='235';
        lead.Customer_has_authorized_credit_soft_pull__c =true;
        lead.FNI_Decision__c = 'D';
        lead.Stip_Code__c ='D27';
        lead.SSN__c='666-66-6666';
        lead.Yearly_Income__c =150000.00;
        lead.LeadSource = 'Purchased List';
        lead.Lead_Source_Detail__c = 'Ohm Analytics';
        upsert lead;
        Lead lead1 = new Lead();
        lead1.FirstName = 'Test';
        lead1.LastName = 'FNI';
        lead1.Street ='steee';
        lead1.City = 'cal';
        lead1.StateCode ='CA';
        lead1.PostalCode ='235';
        lead1.Customer_has_authorized_credit_soft_pull__c =true;
        lead1.FNI_Decision__c = '';
        lead1.Stip_Code__c ='D27';
        lead1.SSN__c='666-66-6666';
        lead1.Yearly_Income__c =150000.00;
        lead1.LeadSource = 'Purchased List';
        lead1.Lead_Source_Detail__c = 'Ohm Analytics';
        upsert lead1;
        Map<id,Lead> mapLd = new Map<id,Lead>();
        mapLd.put(lead.id,lead);
        Map<id,Lead> mapLd1 = new Map<id,Lead>();
        mapLd1.put(lead1.id,lead1);
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            Boolean testboolean = callFNILeadTriggerStaticHelper.callFNI(lead,mapLd);
            Boolean testboolean1 = callFNILeadTriggerStaticHelper.callFNI(lead1,mapLd1);

            Test.stopTest();
            
        }    
    }
    private testMethod static void ServiceBaseTest(){
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1]; 
        RestDataServiceBase restDataServiceBase = new RestDataServiceBase();
        restDataServiceBase.transformOutput(null);
        restDataServiceBase.transformInput(null);
        restDataServiceBase.sObject2Bean(acc, null, null, null, null);
        
        //RestServiceBase
        RestServiceBase restServiceBase = new RestServiceBase();
        restServiceBase.fetchData(null);
        restServiceBase.deleteData(null);
        
        //ObjectBean
        ObjectBean objectbean = new ObjectBean();
        objectbean.result = new List<Account>{};
            
            //CreateIncentiveEligibleBean ref removing
            /*   CreateIncentiveEligibleBean  crbean = new CreateIncentiveEligibleBean();
crbean.prepaymentAmortizationTable = new List<String>();
crbean.message = 'testmsg';
crbean.quoteName = 'testquote';
crbean.product = 'testproduct';
crbean.monthlyPayment=2000.0;
crbean.returnCode= 'testCode';
crbean.finalMonthlyPayment= 500.0;
crbean.finalEscalatedMonthlyPayment= 600.0;
crbean.escalatedMonthlyPayment=300.0;
crbean.amortizationTable= new List<String>();               
CreateIncentiveEligibleBean.ErrorWrapper ew = new CreateIncentiveEligibleBean.ErrorWrapper();
ew.errorMessage = 'TesterrorMessage';
crbean.error = new List<CreateIncentiveEligibleBean.ErrorWrapper>();
*/
            //ObjectRequestWrapper        
            ObjectRequestWrapper objreqwrap = new ObjectRequestWrapper();
        objreqwrap.action = 'PSOT';
        objreqwrap.objectType = 'test';
        objreqwrap.serviceName = 'pricingAPI';
        objreqwrap.sobjectLst = 'testlist';
        
        //UnifiedBean
        UnifiedBean ubean = new UnifiedBean();
        ubean.message = 'testmsg';
        ubean.pageNumber = 1;
        ubean.numberOfRecords=1;
        ubean.totalNumberOfRecords=1;
        ubean.searchKey='testkey';
        ubean.filter='testfilter';
        UnifiedBean.PrequalWrapper lw = new UnifiedBean.PrequalWrapper();
        lw.annualIncome = 1000.00;
        lw.id = '12dkfj';
        lw.firstName = 'testFName';
        lw.lastName = 'testLName';
        lw.street = 'testStreet';
        lw.city = 'testcity';
        lw.state = 'testState';
        lw.zipCode = 'testZIP';
        lw.isCreditAuthorized = true;
        lw.ssn = '12346';
        lw.dateOfBirth = '01-01-1950';
        lw.decisionDetails = 'testdecisionDetails';
        lw.message = 'testmessage';
        lw.errorMessage = 'testerrorMessage';
        UnifiedBean.OpportunityWrapper ow = new UnifiedBean.OpportunityWrapper();
        ow.congaId = 'TestCongaId1';
        ow.ownerEmail = 'test@test.com';
        ow.stage = 'Approved';
        ow.selectedUtilityLseId = 'testSelect';
        UnifiedBean.UtilityChoicesWrapper uw = new UnifiedBean.UtilityChoicesWrapper();
        uw.lseId = 'testlseId';
        uw.name = 'testName';
        UnifiedBean.ApplicantWrapper aw = new UnifiedBean.ApplicantWrapper();
        aw.errorMessage = 'TesterrorMessage';
        UnifiedBean.DocumentWrapper dw = new UnifiedBean.DocumentWrapper();
        dw.id = 'testId12';
        dw.cancelReason = 'TestReason';
        dw.completedDateTime ='TestDate';
        dw.errorMessage = 'testErrorMsg';
        dw.sent = 'true';
        dw.status = 'TestStatus';
        dw.subject = 'TestSubject';
        UnifiedBean.QuotesWrapper qw = new UnifiedBean.QuotesWrapper();
        qw.productId = 'TestProdId12';
        qw.creditApprovalStatus = true;
        qw.warningMessage = 'TestWarnMsg';
        qw.errorMessage = 'TestErrMsg';
        UnifiedBean.FileWrapper fw = new UnifiedBean.FileWrapper();
        fw.fileName = 'TestName';
        fw.createdBy = 'TestUser';
        fw.modifiedAt = 'Test';
        UnifiedBean.FolderWrapper foldw= new UnifiedBean.FolderWrapper();
        foldw.folderName = 'TestFolder';
        UnifiedBean.UsersWrapper usrinfow = new UnifiedBean.UsersWrapper();
        usrinfow.phone = 'Test123';
        usrinfow.photoURL = 'TestPhoto';
        UnifiedBean.RequiredDocumentsWrapper rdw = new UnifiedBean.RequiredDocumentsWrapper();
        rdw.loanAgreements = 'TestLoan';
        rdw.finalDesigns = 'TestDesign';
        rdw.errorMessage = 'TestErrMsg';
        UnifiedBean.CreditsWrapper cw = new UnifiedBean.CreditsWrapper();
        cw.productId = 'TestProdId';
        UnifiedBean.SystemDesignWrapper sw = new UnifiedBean.SystemDesignWrapper();
        sw.errorMessage = 'TesterrorMsg';
        
        callFNILeadTriggerStaticHelper cFNITrgStaticHelper = new callFNILeadTriggerStaticHelper();
        //SLFUtilityTestData.dataLoad();
    }
/**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
    
}