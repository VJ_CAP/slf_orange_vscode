@isTest(seeAllData=false)
public class TestSightenCallout {
    
    @testVisible @testSetup static void setUpData()
    {
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        insert trgLst;
        
        // *** insert rows into SFSettings__c 
        List<StaticResource> rl = new List<StaticResource>([Select Id, Body from StaticResource where Name = 'sf_settings']);
        List<SFSettings__c> pl = (List<SFSettings__c>) JSON.deserialize(rl[0].Body.toString(), List<SFSettings__c>.class);
        insert pl;
        
        // *** insert rows into SF_Category_Map__c 
        
        List<StaticResource> r2 = new List<StaticResource>([Select Id, Body from StaticResource where Name = 'sf_category_map']);
        List<SF_Category_Map__c> cl = 
            (List<SF_Category_Map__c>)JSON.deserialize(r2[0].Body.tostring(), List<SF_Category_Map__c>.class);
        insert cl;
        
                 
        // *** insert rows into Box_Upload__c 
        List<StaticResource> bl = new List<StaticResource>([Select Id, Body from StaticResource where Name = 'box_upload_records']);
        List<Box_Upload__c> bl2 = (List<Box_Upload__c>) JSON.deserialize(bl[0].Body.toString(), List<Box_Upload__c>.class);
        Integer i=0;
        for (Box_Upload__c b : bl2) {
            b.sighten_uuid__c = String.valueOf(i);
            i++;
        }
        insert bl2;
        
         // *** create a Partner Community User, Partner Account, Contact, Opportunity
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User']; 

        Account a = new Account();

        a.FirstName = 'TestFN';
        a.LastName = 'TestLN';
        a.SSN__c = '666-66-6666';
        a.Marital_Status__c = 'Unmarried';
        a.Phone = '4344435555';
        a.PersonEmail = 'test@test.com';
        a.ShippingPostalCode = '12345';
        a.BillingPostalCode = '12345';
        a.PersonBirthdate = date.valueOf('1968-09-21');
        a.External_Comments__c = 'test external comment';
        a.Installer_Legal_Name__c='Bright planet Solar';
        insert a;
        
        
       /* String Username = 'User' + System.now().getTime() + '@user.com';
        
        User u = new User(Alias = 'firstu', Email='first@user.com', 
            EmailEncodingKey='UTF-8', FirstName = 'first', LastName='user', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=Username);
        u.ContactId = c.Id;
        insert u;*/
        

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Sighten_Product_UUID__c = 'test123', 
                                            Term_mo__c = 60,
                                            Product_Tier__c = '0');
        insert p1;
        
        Opportunity o = new Opportunity();
        o.Name = 'TestOppOne';
        o.StageName = 'Qualified';
        o.CloseDate = System.Today();
        o.Install_City__c = 'San Francisco';
        o.Install_Postal_Code__c = '94123';
        o.Install_State_Code__c = 'CA';
        o.Install_Street__c = '2263 Vallejo Street';
        o.Create_in_Sighten__c = true;
        o.AccountId = a.Id;
        o.Sighten_UUID__c = 'XYZ';
        o.Upload_Comment__c = true;
        o.Upload_Comment_Text__c = 'upload comment';
        insert o;
                
        Quote q1 = new Quote();
        q1.OpportunityId = o.Id;
        q1.Name = 'Quote 1';
        insert q1;
        
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
         settings.Use_Debug_Opportunities__c = false;
         settings.Process_Archived_Sites__c = true;
         //settings.Debug_Opportunities__c ='o.Id,o.Id';
         upsert settings SFSettings__c.Id;
        
        
        /*Quote q2 = new Quote();
        q2.OpportunityId = o.Id;
        q2.Name = 'Quote 2';
        insert q2;*/
    }

    public static testMethod void testSetup()
    {
        Test.startTest();
            SFSettings__c settings = SFSettings__c.getOrgDefaults();
            settings.Use_Debug_Opportunities__c = false;
            settings.Process_Archived_Sites__c = true;
            upsert settings SFSettings__c.Id;
            System.Assert(settings.SIGHTEN_TOKEN__c != null, 'Unable to find SIGHTEN_TOKEN');
            
            List<Opportunity> ol = new List<Opportunity>([Select Id, Install_Street__c, Install_City__c, Install_State_Code__c, Install_Postal_Code__c from Opportunity]);
            System.Assert(ol.size() == 1, 'Not able to find opportunity created in test method');
        Test.stopTest();
    }
        
    public static testMethod void testSiteDownload()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        String Username = 'User' + System.now().getTime() + '@user.com';
        User u = new User(Alias = 'firstu', Email='first@user.com', 
            EmailEncodingKey='UTF-8', FirstName = 'first', LastName='user', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName=Username);
        insert u;
        
      
        Account a1 = new Account();

        a1.FirstName = 'Solar';
        a1.LastName = 'City';
        a1.SSN__c = '666-66-6666';
        a1.PersonBirthdate = Date.today();
        a1.Marital_Status__c = 'Unmarried';
        a1.Phone = '4344435555';
        a1.PersonEmail = 'test@test.com';
        a1.ShippingPostalCode = '12345';
        a1.BillingPostalCode = '12345';
        a1.PersonBirthdate = date.valueOf('1968-09-21');
        a1.Installer_Legal_Name__c='Bright planet Solar';
        insert a1;
        
        
        Account a = new Account();

        a.FirstName = 'TestFN';
        a.LastName = 'TestLN';
        a.SSN__c = '666-66-6666';
        a.PersonBirthdate = Date.today();
        a.Marital_Status__c = 'Unmarried';
        a.Phone = '4344435555';
        a.PersonEmail = 'test@test.com';
        a.ShippingPostalCode = '12345';
        a.BillingPostalCode = '12345';
        a.PersonBirthdate = date.valueOf('1968-09-21');
        a.Installer_Legal_Name__c='Bright planet Solar';
        insert a;
                System.debug('act value'+a);

        Opportunity o = new Opportunity();
        o.Name = 'O1';
        o.StageName = 'Qualified';
        o.CloseDate = System.Today();
        o.Install_City__c = 'San Francisco';
        o.Install_State_Code__c = 'CA';
        o.Upload_Comment__c = true ;
        //o.Installer_Account__r.Name=a1.Name;
        o.Installer_Account__c=a1.Id;
        o.Install_Postal_Code__c = '94123';
        o.Install_Street__c = '2263 Vallejo Street';
        o.AccountId = a.Id;
        insert o;
        System.debug('opty value'+o);
        
       List<Opportunity> optylist = [Select Id from Opportunity where Upload_Comment__c = true AND Installer_Account__r.Name = 'Solar City'];
       System.debug('ol list'+optylist );
    

      

        //testOppty.Installer_Account__c = testInstallerAccount.Id;
        
        try {
            sightenCallout.getSightenSystem('sss',o.Id);
        } catch (Exception e) {}

        try {
            sightenCallout.getSightenQuotes('sss', o.Id, 'CA');
         } catch (Exception e) {}

    
        // *** test createFolderForObject
        List<Id> ol = new List<Id>();
        ol.add(o.Id);
        sightenCallout.createFolderForObject(ol);
        
        // ** test getDocsBySiteFromSighten
         
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock(); 
        mock.setStaticResource('sighten_site_response');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();
        Date dt = date.parse('01/01/2000');
        Site_Download__c dl;
        DateTime lastExecutionTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
        try {
            sightenCallout.downloadSiteFromSighten(o, lastExecutionTime);
            
            List<StaticResource> rl = new List<StaticResource>([Select Id, Body from StaticResource where Name = 'sighten_site_response']);
            
            Site_Download__c site = new Site_Download__c();
            site.Sighten_Response__c = rl[0].Body.toString();
            List<Box_Upload__c> uploadList = new List<Box_Upload__c>();
            List<Underwriting_File__c> underwritingList = new List<Underwriting_File__c>();
            box.Toolkit boxToolKit;
            sightenUtil.createBoxFoldersAndUploads(boxToolKit, site,uploadList,underwritingList,null,null);
            
                
         //dl = sightenCallout.getDocsBySiteFromSighten(o,lastExecutionTime);
        } catch (Exception e) {}
        Test.stopTest();
        
        // *** check that the Site_Download__c record contains the expected result
        //System.Assert(dl != null);
        //System.Assert(dl.Success__c == true);
        
        // *** check that the underwriting file was created
        List<Underwriting_File__c> fl = new List<Underwriting_File__c>([Select Id from Underwriting_File__c where Opportunity__c = :o.id]);
        //System.Assert(fl.size() == 1);
        
        // *** check that the box upload records were created
        List<Box_Upload__c> ul = new List<Box_Upload__c>([Select Id from Box_Upload__c]);
        //System.Assert(ul.size() == 5,'Expected 5 records received ' + ul.size());
        ul[0].Opportunity__c = o.Id;
        
        // should invoke the update trigger logic
        update ul;
        
        // should invoke the delete trigger logic
        delete ul[0];
        
    }
    
    public testMethod static void testCreateSightenSite()
    {
        List<Opportunity> ol = new List<Opportunity>([Select Id, Install_Street__c, Install_City__c, Install_State_Code__c, Install_Postal_Code__c from Opportunity]);
        Opportunity o = ol[0];
        list<Product__c> p = new List<Product__c>([select id from Product__c limit 1]);
        
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
         settings.Use_Debug_Opportunities__c = false;
         settings.Process_Archived_Sites__c = true;
            upsert settings SFSettings__c.Id;
  
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock(); 
        mock.setStaticResource('sighten_site_create_response');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        List<User> ul = new List<User>([Select Id from User]);
       // System.runAs(ul[0]) {
        Boolean ach = true;
        String prodId = p[0].id;
        Double costPerWatt = 1.00;
        Double upfrontPayment = 5000.00;
        Test.startTest(); 
            try { 
                sightenCallout.createSightenSite('first','last','first@last.com',o.Install_Street__c,o.Install_City__c,o.Install_State_Code__c,o.Install_Postal_Code__c,o.id,settings.SIGHTEN_TOKEN__c);
                sightenCallout.createSightenSiteAndSystem('first','last','first@last.com',o.Install_Street__c,o.Install_City__c,o.Install_State_Code__c,o.Install_Postal_Code__c,o.id,settings.SIGHTEN_TOKEN__c, 10.01, 15.90, 'test', 'test');
                sightenCallout.generateSightenQuote(prodId, o.id, costPerWatt, upfrontPayment, ach);
                box.Toolkit boxToolkit = new box.Toolkit();
                String boxId = sightenCallout.createBoxOpportunityFolder(boxToolkit, o.Id);
                
                sightenCallout.getResponseAsBlobAsync('POST', 'URL', 'test', 'token', 'restBody');
            } catch (Exception e) {}
                    
        String response = sightenCallout.createSightenSiteCallout('first','last','first@last.com',o.Install_Street__c,o.Install_City__c,o.Install_State_Code__c,o.Install_Postal_Code__c);
        Test.stopTest();
       // }
        
    }
    
        public testMethod static void testRest()
        {
            sightenCallout.getSightenSystemCallout('AAAA');
            
            DateTime dt = sightenCallout.parseSightenDate('2016-07-08 23:15:15.121404');
            
            try { 
                sightenCallout.uploadBatchToBox(107);
            } catch (Exception e) {}
        }

     public testMethod static void testBlob()
     {
         
       BLOB b = sightenCallout.getResponseAsBlob('POST', 'http://www.yahoo.com/', 'solar/quotegen/site/', 'aaa', 'ddd');
     }
    
     public testMethod static void testMilestone()
     {
        List<Opportunity> ol = new List<Opportunity>([Select Id, Install_Street__c, Install_City__c, Install_State_Code__c, Install_Postal_Code__c from Opportunity]);
        Opportunity o = ol[0];
        Test.startTest();
        sightenUtil.setMilestoneOnOpp(o.Id,'dummy-quote-uuid');
        sightenUtil.updateSightenComments(o.Id,'dummy-milestone-uuid','comment');
        List<Site_Download__c> sd = sightenCallout.getSiteRecordsWithSightenTS(5,5);
        Test.stopTest();
     }
    
    
     public testMethod static void testFRUP()
     {
        List<Opportunity> ol = new List<Opportunity>([Select Id, Install_Street__c, Install_City__c, Install_State_Code__c, Install_Postal_Code__c from Opportunity]);
        Opportunity o = ol[0];
        Test.startTest();
        sightenUtil.createFRUPEntry('BOXFOLDERIDUMMY',o.Id);
        Test.stopTest();
     }
}