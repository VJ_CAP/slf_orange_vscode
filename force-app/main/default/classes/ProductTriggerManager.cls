public class ProductTriggerManager extends DefaultTriggerHandler
{

    // Default Constructor
    public ProductTriggerManager() {}   

    //  ****************************************************************** 

    public override void beforeInsert(List<SObject> newSoList)
    {
    	//  empty
    }

    //  ****************************************************************** 

    public override void beforeUpdate( map<id,Sobject> oldSoMap,map<id,SObject> newSoMap)
    {
    	//  empty
    }

    //  ****************************************************************** 

    public override void afterInsert(map<id,SObject> newSoMap)
    {
    	ProductSharingSupport.afterInsert (newSoMap);
    }

    //  ****************************************************************** 

    public override void afterUpdate(map<id,SObject> oldSoMap, map<id,SObject> newSoMap)
    {
    	ProductSharingSupport.afterUpdate (oldSoMap, newSoMap);
    }

    //  ****************************************************************** 

    public override void beforeDelete (map<id,SObject> oldSoMap)
    {
    	//  empty
    }

    //  ******************************************************************      
}