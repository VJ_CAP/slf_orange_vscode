/**
* Description: New API created as part of ORANGE-3700 - LoginAs child installer accounts.
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------

******************************************************************************************/
public without sharing class LoginAsUserServiceImpl extends RESTServiceBase{
    /**
* Description:   
* @param reqAP      Get all POST request action parameters
* @param reqData    Get all POST request data
* return res        Send response back with data and response status code
*/
    public override IRestResponse fetchData(RequestWrapper rw){  
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        UnifiedBean loginAsInfoResp = new UnifiedBean();
        loginAsInfoResp.users = new List<UnifiedBean.UsersWrapper>();
        string errMsg = '';
        try{        
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            Set<Id> InstallerIds = new Set<Id>();
            for(UnifiedBean.ChildInstallersWrapper objCW:reqData.installerInfo.childAccounts){
                system.debug('======='+objCw.Id);
                if(objCW.Id != null){
                    InstallerIds.add(objCW.Id);
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('214',null);
                    gerErrorMsg(loginAsInfoResp,errMsg,'214');
                    iRestRes = (IRestResponse)loginAsInfoResp; 
                    res.response= iRestRes;
                }   
            }
            if(!InstallerIds.isEmpty()){
                List<User> lstUsers = [SELECT Id, Username,Email, Password__c from User where contact.AccountId in:InstallerIds 
                                       AND Default_User__c =: true and IsActive =: true ];
                if(lstUsers.isEmpty()){
                    errMsg = ErrorLogUtility.getErrorCodes('401',null);
                    gerErrorMsg(loginAsInfoResp,errMsg,'401');
                    iRestRes = (IRestResponse)loginAsInfoResp; 
                    res.response= iRestRes;
                }       
                for(User objU:lstUsers){
                    UnifiedBean.UsersWrapper Userinfo = new UnifiedBean.UsersWrapper();
                    Userinfo.email = objU.Email;
                    Userinfo.password = objU.Password__c;
                    loginAsInfoResp.returncode = '200';
                    loginAsInfoResp.users.add(Userinfo);
                    iRestRes = (IRestResponse)loginAsInfoResp; 
                    res.response= iRestRes;
                }
            }
        }catch(Exception err){
            ErrorLogUtility.writeLog('LoginAsUserServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            errMsg = ErrorLogUtility.getErrorCodes('214',null);
            gerErrorMsg(loginAsInfoResp,errMsg,'214');
            iRestRes = (IRestResponse)loginAsInfoResp; 
            return iRestRes;
        }
        return res.response;     
    }
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        List<UnifiedBean.ErrorWrapper> errorList = new List<UnifiedBean.ErrorWrapper>();
        UnifiedBean.ErrorWrapper errorWraper = new UnifiedBean.ErrorWrapper();
        errorWraper.errorMessage = sErrorMsg;
        errorList.add(errorWraper);
        objUnifiedBean.error = errorList;
        objUnifiedBean.returnCode = sReturnCode;
    }
    
}