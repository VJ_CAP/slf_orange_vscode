global class UpdateEnvelopIdOnOppBatch implements Database.Batchable<sObject> {
    global Set<Id> oppIdSet = new Set<Id>();
    global String query;
    
    global UpdateEnvelopIdOnOppBatch(Set<id> idSet){       
        query = 'Select id, Synced_DocuSign__c,(Select id, dsfs__DocuSign_Envelope_ID__c from R00N80000002fD9vEAE__r where dsfs__Envelope_Status__c = \'Completed\' Order by LastModifiedDate DESC limit 1) from Opportunity where StageName=\'Closed Won\' and id in (select dsfs__Opportunity__c from dsfs__DocuSign_Status__c where dsfs__Envelope_Status__c = \'Completed\')';
        if(idSet != null && !idSet.isEmpty()){
            oppIdSet.addAll(idSet);
            query += ' and id in :oppIdSet';
        }  
        System.debug('oppIdSet:::'+oppIdSet); 
        System.debug('query:::'+query);         
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<Opportunity> oppList){    
       system.debug('### Entered into execute() of '+ UpdateEnvelopIdOnOppBatch.class);  
       for(Opportunity oppObj : oppList){
           if(!oppObj.R00N80000002fD9vEAE__r.isEmpty() && oppObj.R00N80000002fD9vEAE__r[0].dsfs__DocuSign_Envelope_ID__c != null){
                oppObj.Synced_DocuSign__c = oppObj.R00N80000002fD9vEAE__r[0].dsfs__DocuSign_Envelope_ID__c.toLowercase();
           }
       }
       if(!oppList.isEmpty()){
           System.debug('###oppList:'+oppList);
           Database.update(oppList ,false);
       }
       system.debug('### Exit into execute() of '+ UpdateEnvelopIdOnOppBatch.class);
    }

   global void finish(Database.BatchableContext BC){
   }
}