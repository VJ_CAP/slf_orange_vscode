@isTest(seeAllData=false)
public class Batch_CreateOppFoldersTest {
    private testMethod static void Batch_CreateOppFoldersTest(){
        SFSettings__c settings = new SFSettings__c(MAX_ROWS_IN_BATCH__c = 3);
        insert settings;
        Site_Download__c siteDwnld = new Site_Download__c(Success__c = true,Site_UUID__c='2585');
        insert siteDwnld;
        List<Attachment> attchList = new List<Attachment>();
        Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob; 
        attach.parentId=siteDwnld.Id; 
        attach.ContentType = 'application/msword'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test'; 
        attchList.add(attach);
        insert attchList; 
        try{
            Batch_CreateOppFolders crtoppFldrBatchObj = new Batch_CreateOppFolders();
            Database.executeBatch(crtoppFldrBatchObj);
        }catch(Exception e){}
        try{
            Batch_CreateOppFolders crtoppFldrBatchObj1 = new Batch_CreateOppFolders();
            String sch = '20 30 8 10 2 ?';
            System.schedule('Test Job', sch, crtoppFldrBatchObj1);        
        }catch(Exception e){}
    }
}