/**
* Description: User and Contact Creation logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh                  03/05/2018           Created
******************************************************************************/

public without sharing class CreateUserDataServiceImpl extends RestDataServiceBase{
   /*
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+CreateUserDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        List<UnifiedBean.UsersWrapper> userRecLst =  reqData.users; 
        UnifiedBean createuserbean = new UnifiedBean(); 
        createuserbean.users = new List<UnifiedBean.UsersWrapper>();
        createuserbean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        User newUserRec = new User();
        Map<string,string> mapConFieldInfo = new Map<string,string>();
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        Savepoint sp = Database.setSavepoint();        
        try
        { 
            if(reqData.users != null)
            {
                 List<User> newUserList = new List<User>();
                 List<Contact> newContactList = new List<Contact>();
                 String installerAccID = '';
                 List<user> loggedInUsr = [select Id,X2020_ITC_Step_down_Acknowledged__c,contact.AccountId,contact.Account.Name,
                                          contact.Account.Default_FNI_Loan_Amount__c,contact.Account.FNI_Domain_Code__c,
                                          Training_Certification_Completed__c   
                                          from User where Id =: userinfo.getUserId()];
                 Profile profile = [SELECT Id FROM profile WHERE name= 'Partner Community User for Salesforce1' LIMIT 1];
                 system.debug('### loggedInUsr----'+loggedInUsr);
                 system.debug('### profile----'+profile);
                 UnifiedBean.UsersWrapper userRec = userRecLst[0];
                 String RoleName = '';
                 List<UserLicense> usersLicense = [select Id, Name, UsedLicenses, TotalLicenses, Status, MasterLabel, LicenseDefinitionKey from UserLicense where name='Partner Community'];
                 system.debug('###usersLicense--'+usersLicense);
                 if(!usersLicense.isEmpty() && usersLicense[0].UsedLicenses < usersLicense[0].TotalLicenses)
                 {
                    if(loggedInUsr[0] != null && loggedInUsr[0].contact != null)
                        RoleName =  loggedInUsr[0].contact.Account.Name+' ';
                    if(loggedInUsr[0] != null && loggedInUsr[0].contact.AccountId != null)                    
                        installerAccID = loggedInUsr[0].contact.AccountId;

                    /*** Role Name Start *****/
                     if(string.isNotBlank(userRec.title)){
                        String strrole = userRec.title;
                        if(strrole.toLowerCase() == 'executive' || strrole.toLowerCase() == 'operations manager/representative')
                            RoleName+='Partner Executive';
                        if(strrole.toLowerCase() == 'sales manager - all reps')
                            RoleName+='Partner Manager';
                        if(strrole.toLowerCase() == 'sales representative' || strrole.toLowerCase() =='sales manager - direct reports')
                            RoleName+='Partner User';
                     }

                      /*** Role Name End *****/
                    SFSettings__c SFseting =SFSettings__c.getOrgDefaults();
                     string OrgName = '';
                    Boolean updatecheck = false;//For not update contact if no values are passed in request
                     Contact objcon;
                     if(SFseting.Org_Name__c != null) 
                        OrgName = '.'+SFseting.Org_Name__c;
                        
                       List<Contact> ReportsToContact = new List<Contact>();
                        if(string.isNotBlank(userRec.reportsTo)){
                            ReportsToContact = [select id from Contact where id=:userRec.reportsTo];
                            if(ReportsToContact.size()==0){
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = ErrorLogUtility.getErrorCodes('375', null);
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                createuserbean.error.add(errorMsg);
                                createuserbean.returnCode = '375';
                                iRestRes = (IRestResponse)createuserbean;
                                return iRestRes; 
                            }
                        }
                        system.debug('***ReportsToContact---'+ReportsToContact);
                        
                        if(string.isNotBlank(userRec.id))//User update
                        {
                            List<user> userobj = [SELECT Id, X2020_ITC_Step_down_Acknowledged__c, Username, LastName, FirstName, MiddleName, Name, Email, Contact.ReportsToid,
                                            Contact.ReportsTo.Name,UserRole.Name,Phone, Alias, IsActive, UserRoleId, UserType, ContactId, 
                                            AccountId, PortalRole, IsPortalEnabled, LastViewedDate,Hash_Id__c, Last_View_State__c FROM User where id=:userRec.id];
                           
                            if(userobj != null && userobj.size()>0) 
                            {
                                List<User> userlist;
                                if(string.isNotBlank(userRec.email))
                                    userlist =[Select id,Email from User where id!=:userobj[0].Id and Email=:userRec.email and IsActive=:true];
                                system.debug('***userlist--'+userlist);
                                if(userlist != null && userlist.size()>0){
                                    iolist = new list<DataValidator.InputObject>{};
                                    errMsg = ErrorLogUtility.getErrorCodes('372', null);
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    createuserbean.error.add(errorMsg);
                                    createuserbean.returnCode = '372';
                                    iRestRes = (IRestResponse)createuserbean;
                                    return iRestRes;
                                }else{
                                    system.debug('###userobj-- '+userobj);
                                    newUserRec = userobj[0];
                                }
                                system.debug('****userobj[0].ContactId--'+userobj[0].ContactId);
                                if(userobj[0].ContactId != null){//Changing Reportsto on user update
                                     objcon = [select id,ReportsToid,firstName, lastName, email, phone from Contact where id=:userobj[0].ContactId];
                                    if(objcon != null){
                                        if(userRec.reportsTo != null && String.isNotBlank(userRec.reportsTo)){
                                            mapConFieldInfo.put('ReportsToid',userRec.reportsTo);
                                            updatecheck = true;
                                        }
                                        if(userRec.firstname != null && String.isNotBlank(userRec.firstname)){
                                            mapConFieldInfo.put('FirstName',userRec.firstname);
                                            updatecheck = true;
                                        }
                                        if(userRec.lastname != null && String.isNotBlank(userRec.lastname)){
                                            mapConFieldInfo.put('LastName',userRec.lastname);
                                            updatecheck = true;
                                        }
                                        if(userRec.email != null && String.isNotBlank(userRec.email)){
                                            mapConFieldInfo.put('Email',userRec.email);
                                            updatecheck = true;
                                        }
                                        if(userRec.phone != null && String.isNotBlank(userRec.phone)){
                                            mapConFieldInfo.put('Phone',userRec.phone);
                                            updatecheck = true;
                                        }
                                        
                                        system.debug('***updatecheck--'+updatecheck);
                                        if(updatecheck){
                                            
                                            if(ReportsToContact != null && ReportsToContact.size()>0 && ReportsToContact[0].id != objcon.id ){
                                                updateContact(string.valueOf(objcon.id),mapConFieldInfo);
                                            }else if(ReportsToContact != null && ReportsToContact.size()>0 && ReportsToContact[0].id == objcon.id){
                                                iolist = new list<DataValidator.InputObject>{};
                                                errMsg = ErrorLogUtility.getErrorCodes('385', null);
                                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                                createuserbean.error.add(errorMsg);
                                                createuserbean.returnCode = '385';
                                                iRestRes = (IRestResponse)createuserbean;
                                                return iRestRes;
                                            }
                                       
                                            
                                        }
                                    }
                                }
                            }else{
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = ErrorLogUtility.getErrorCodes('371', null);
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                createuserbean.error.add(errorMsg);
                                createuserbean.returnCode = '371';
                                iRestRes = (IRestResponse)createuserbean;
                                return iRestRes;
                            }
                        }else{//New User creation
                            
                            List<User> userlist = new list<User>();
                            if(string.isNotBlank(userRec.email))
                                    userlist =[Select id,Email from User where Email=:userRec.email and IsActive=:true];
                            if(!userlist.isEmpty()){
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = ErrorLogUtility.getErrorCodes('372', null);
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                createuserbean.error.add(errorMsg);
                                createuserbean.returnCode = '372';
                                iRestRes = (IRestResponse)createuserbean;
                                return iRestRes;  
                            }else{
                                Contact conRec = new Contact();
                                conRec.FirstName = userRec.firstname;
                                conRec.LastName = userRec.lastname;
                                conRec.Email = userRec.email;
                                conRec.Phone = userRec.phone;
                                conRec.Reset_Password_Flag__c =true;
                                conRec.Accountid = installerAccID;
                                if(String.isNotBlank(userRec.reportsTo))
                                    conRec.ReportsToid = userRec.reportsTo;
                                try
                                {
                                    insert conRec;
                                    newUserRec.contactid = conRec.id;
                                } catch(Exception err){
                                    system.debug('### into Contactexception '+err.getMessage());
                                    //Database.rollback(sp);
                                    iolist = new list<DataValidator.InputObject>{};
                                    errMsg = ErrorLogUtility.getErrorCodes('400', null);
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    createuserbean.error.add(errorMsg);
                                    createuserbean.returnCode = '400';
                                    iRestRes = (IRestResponse)createuserbean;
                                    return iRestRes;
                                }
                              }
                            }                        
                           if(userRec.firstname != null && userRec.firstname !='') 
                                newUserRec.FirstName = userRec.firstname;
                            if(userRec.lastname != null && userRec.lastname !='')
                                newUserRec.LastName = userRec.lastname;
                            if(userRec.lastname != null && userRec.lastname !='' && userRec.firstname != null && userRec.firstname !='')
                                newUserRec.communityNickname = userRec.firstname.substring(0,1)+userRec.lastname.substring(0,1)+' '+SFseting.Org_Name__c+'_'+Math.random();
                            if(userRec.email != null && userRec.email != '')    
                                newUserRec.Email = userRec.email;
                            if(userRec.phone != null && userRec.phone !='')
                                newUserRec.Phone = userRec.phone;
                            if(userRec.isActive != null && (userRec.isActive == true || userRec.isActive == false))
                                newUserRec.IsActive = userRec.isActive;
                            if(userRec.title != null && userRec.title != '')
                                newUserRec.Title =userRec.title;
                            
                            if(userRec.lastname != null && userRec.lastname !='' && userRec.firstname != null && userRec.firstname !='')
                                newUserRec.alias =userRec.firstname.substring(0,1)+userRec.lastname.substring(0,1)+SFseting.Org_Name__c;
                            if(userRec.isTrainingCompleted != null && userRec.isTrainingCompleted == true)
                                newUserRec.Training_Certification_Completed__c = userRec.isTrainingCompleted;//Added By Adithya on 12/07/2018 ORANGE-1521                                   
                            if(string.isBlank(newUserRec.id)){
                                newUserRec.Username = userRec.email+OrgName;
                                newUserRec.LocaleSidKey='en_US';
                                newUserRec.EmailEncodingKey='ISO-8859-1';
                                newUserRec.LanguageLocaleKey='en_US';
                                newUserRec.TimeZoneSidKey='America/Panama';
                                newUserRec.profileid = profile.Id;                                
                            }
                            //Added as part of ORANGE-3472 - Start
                            if(userRec.selectedQuestionsAndAnswers!=null && !userRec.selectedQuestionsAndAnswers.isEmpty()){
                                if(String.isNotBlank(userRec.selectedQuestionsAndAnswers[0].question) && String.isNotBlank(userRec.selectedQuestionsAndAnswers[0].answer)){
                                    newUserRec.Question1__c=userRec.selectedQuestionsAndAnswers[0].question;
                                    newUserRec.Answer1__c=userRec.selectedQuestionsAndAnswers[0].answer;
                                }
                                if(String.isNotBlank(userRec.selectedQuestionsAndAnswers[1].question) && String.isNotBlank(userRec.selectedQuestionsAndAnswers[1].answer)){
                                    newUserRec.Question2__c=userRec.selectedQuestionsAndAnswers[1].question;
                                    newUserRec.Answer2__c=userRec.selectedQuestionsAndAnswers[1].answer;
                                }
                                if(String.isNotBlank(userRec.selectedQuestionsAndAnswers[2].question) && String.isNotBlank(userRec.selectedQuestionsAndAnswers[2].answer)){
                                    newUserRec.Question3__c=userRec.selectedQuestionsAndAnswers[2].question;
                                    newUserRec.Answer3__c=userRec.selectedQuestionsAndAnswers[2].answer;
                                }
                            }
                                
                            //Added as part of ORANGE-3472 - End
                            //Added as part of ORANGE-5256 - Start
                            List<Messaging.SingleEmailMessage> lstEmails= new List<Messaging.SingleEmailMessage>();
                            if(userRec.isFrozen != null && !userRec.isFrozen){
                                UnfreezeUser(newUserRec.id,true);
                                newUserRec.Question1__c = '';
                                newUserRec.Question2__c = '';
                                newUserRec.Question3__c = '';
                                newUserRec.Answer1__c = '';
                                newUserRec.Answer2__c = '';
                                newUserRec.Answer3__c = '';
                                newUserRec.Question_Counter__c = 0;
                                // Sending an e-mail to the portal user to reset the password.
                                EmailTemplate et = [SELECT Id,HtmlValue,Subject,body FROM EmailTemplate WHERE DeveloperName ='Create_Password_Unlocked_Users'];                                
                                List<string> toAddress = new List<string>();
                                toAddress.add(newUserRec.Id);
                                String emailBody = et.HtmlValue; 
                                emailBody = emailBody.replace('{!$Label.Sunlight_Logo}',system.label.Sunlight_Logo);                                 
                                emailBody = emailBody.replace('{!Receiving_User.Name}', newUserRec.Name);
                                emailBody = emailBody.replace('Receiving_User.Hash_Id__c', EncodingUtil.urlEncode(newUserRec.hash_id__c,'UTF-8'));
                                emailBody=emailBody.replace('{!$Label.Credit_Application_link}',System.label.Credit_Application_link);
                                emailBody = emailBody.replace('{!$Label.Create_Password}',system.label.Create_Password);
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                    mail.setToAddresses(toAddress);
                                    mail.setSubject(et.Subject);                                    
                                    mail.setHtmlBody(emailBody);
                                    lstEmails.add(mail);      
                            }
                            if(!lstEmails.isEmpty()){
                                Messaging.sendEmail(lstEmails,false);  
                            }
                                
                            //Added as part of ORANGE-5256 - End
                            if(string.isNotBlank(userRec.dashboardView)){ // Added as part of Orange - 3471 by Sejal and Srinivas on 26/04/2019
                                if(userRec.dashboardView == 'tile')
                                    newUserRec.Last_View_State__c = 'Tile';
                                else if(userRec.dashboardView == 'list')
                                    newUserRec.Last_View_State__c = 'List';
                               
                            }
                            // Added as part of Orange- 3483
                            if(userRec.isAchAcknowledged != null){
                                newUserRec.ACH_Acknowledged__c = userRec.isAchAcknowledged;
                            } 
                            
                            // added by Arun J as a part of Orange 6921
                            if(userRec.itcStepDownAcknowledged){
                                newUserRec.X2020_ITC_Step_down_Acknowledged__c = userRec.itcStepDownAcknowledged;
                            }
                            
                            system.debug('###newUserRec 2--'+newUserRec);
                            try
                            {
                                List<User> lstuserToInsert;    
                                if(string.isNotBlank(newUserRec.id))
                                {
                                   update newUserRec;
                                    system.debug('###After update user --'+newUserRec);
                                    if(RoleName != null && newUserRec.id != null)
                                        assignRoletoUser(String.valueOf(newUserRec.id),RoleName);
                                }else{
                                    newUserRec.UserPreferencesShowEmailToExternalUsers = true;
                                    insert newUserRec;
                                    system.debug('###After Insert user --'+newUserRec);
                                    if(RoleName != null && newUserRec.id != null)
                                        assignRoletoUser(String.valueOf(newUserRec.id),RoleName);
                                    UserTriggerHandler.updateContact(new set<Id>{newUserRec.Contactid});
                                }
                            } catch(Exception err){ 
                                Database.rollback(sp);
                                system.debug('### into Userexception '+err.getMessage());
                                system.debug('### line number '+err.getLineNumber());
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = ErrorLogUtility.getErrorCodes('400', null);
                                gerErrorMsg(createuserbean,errMsg,'400',iolist);
                                iRestRes = (IRestResponse)createuserbean;
                                return iRestRes; 
                                }
                    if(string.isNotBlank(errMsg)){
                        Database.rollback(sp);
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        createuserbean.error.add(errorMsg);
                        iRestRes = (IRestResponse)createuserbean;
                    }  
            }else{
                iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('370', null);
                gerErrorMsg(createuserbean,errMsg,'370',iolist);
                iRestRes = (IRestResponse)createuserbean;
                ErrorLogUtility.writeLog('CreateUserDataServiceImpl.transformOutput()',53,'transformOutput()','You have already reached your user limits,Please contact Sunlight Financial for further assistance.'+ '###','Error','');    
            }
        }   
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400', null);
            gerErrorMsg(createuserbean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)createuserbean;
            system.debug('### CreateUserDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('CreateUserDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
       //Preparing Response data
       if(newUserRec.id != null){//Response preparation
           User UserObj = [SELECT Id, Username, LastName, FirstName, MiddleName, Name, Title, Email, Phone, 
                           Alias, CommunityNickname,Training_Certification_Completed__c,IsActive, UserRoleId, 
                           ProfileId, LastLoginDate, ContactId, AccountId, IsPortalEnabled, PortalRole,LastViewedDate,
                           contact.ReportsToid,contact.Account.Name,Hash_Id__c, Last_View_State__c,Answer1__c,Answer2__c,Answer3__c,
                           Question1__c,Question2__c,Question3__c,ACH_Acknowledged__c FROM User WHERE id=:newUserRec.id];
            //Added as part of ORANGE-5256
            UserLogin ULoginObj = UnfreezeUser(newUserRec.id,false);
           
           if(UserObj != null){
                UnifiedBean.UsersWrapper Userinfo = new UnifiedBean.UsersWrapper();
                //Commented by Adithya as part of 7020
                //Userinfo.selectedQuestionsAndAnswers = new List<UnifiedBean.SelectedQuestionsAndAnswerWrapper>(); // Added as part of ORANGE-3472
                //UnifiedBean.SelectedQuestionsAndAnswerWrapper selectdQAsWrp = new UnifiedBean.SelectedQuestionsAndAnswerWrapper();// Added as part of ORANGE-3472
                Userinfo.id = UserObj.id;
                Userinfo.userName = UserObj.Username;
                Userinfo.firstName = UserObj.FirstName;
                Userinfo.lastName = UserObj.LastName;
                Userinfo.hashId = UserObj.Hash_Id__c;
                Userinfo.name = UserObj.Name;
                Userinfo.email = UserObj.Email;
                Userinfo.phone = UserObj.Phone;
                Userinfo.isActive = UserObj.IsActive; 
                Userinfo.reportsTo = UserObj.contact.ReportsToid;
                Userinfo.title = UserObj.Title;
                Userinfo.isTrainingCompleted = UserObj.Training_Certification_Completed__c;//Added By Adithya on 12/07/2018 ORANGE-1521  
               Userinfo.dashboardView = UserObj.Last_View_State__c;//Added by Sejal and Srinivas on 26/04/2019 as a part of Orange - 3471
               Userinfo.isFrozen= ULoginObj.isfrozen; //Added as part of ORANGE-5256
               
               //Commented by Adithya as part of 7020
               //start
                /*
                if(String.isNotBlank(UserObj.Question1__c))
                selectdQAsWrp.question = UserObj.Question1__c;

                selectdQAsWrp.answer = String.isNotBlank(UserObj.Answer1__c)?UserObj.Answer1__c:'';
                Userinfo.selectedQuestionsAndAnswers.add(selectdQAsWrp);
                if(String.isNotBlank(UserObj.Question2__c)  )
                selectdQAsWrp = new UnifiedBean.SelectedQuestionsAndAnswerWrapper();
                selectdQAsWrp.question = UserObj.Question2__c;
                selectdQAsWrp.answer = String.isNotBlank(UserObj.Answer2__c)?UserObj.Answer2__c:'';
                Userinfo.selectedQuestionsAndAnswers.add(selectdQAsWrp);

                if(String.isNotBlank(UserObj.Question3__c) )
                selectdQAsWrp = new UnifiedBean.SelectedQuestionsAndAnswerWrapper();
                selectdQAsWrp.question = UserObj.Question3__c;
                selectdQAsWrp.answer =String.isNotBlank(UserObj.Answer3__c)?UserObj.Answer3__c:'';
                Userinfo.selectedQuestionsAndAnswers.add(selectdQAsWrp);
                */
                //End
                system.debug('###Userinfo--'+Userinfo);
                createuserbean.returnCode = '200';
                // Added this part of 3471.
                /*if(!String.isBlank(Userinfo.dashboardView)){
                    createuserbean.message = 'Success! Preference updated.'; 
                    Commented this peice of code for not sending expected response and raised defect-6963.
                } */
                //Added for ORANGE-6963 Defect.
                if(string.isNotBlank(userRecLst[0].dashboardView) || userRecLst[0].dashboardView != null){
                    createuserbean.message = 'Success! Preference updated.'; 
                    iRestRes = (IRestResponse)createuserbean; 
                    return iRestRes;  
                }// END of ORANGE-6963.     
            createuserbean.users.add(Userinfo);
            }
            iRestRes = (IRestResponse)createuserbean;
        }
        system.debug('### Exit from transformOutput() of '+CreateUserDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
    
    @future(callout=true)
    public static void assignRoletoUser(String usrid,String Rolename){
        User userobj;
        system.debug('###Rolename---'+Rolename);
        string hashidval;
        if(string.isNotBlank(usrid)){
            userobj  = [select id,name,email,UserRoleid from user where id=:usrid];
            hashidval = SLFUtility.getHashId(usrid);
        }
        List<Userrole> rl;  
        if(string.isNotBlank(Rolename))    
         rl = [SELECT id,Name from UserRole where Name=:RoleName];
        system.debug('###userobj---'+userobj);
        system.debug('###rl---'+rl);
        if(userobj != null && rl != null && rl.size()>0){
              userobj.UserRoleid = rl[0].id;
              userobj.Hash_Id__c = hashidval;
             try{
                update userobj;
              }catch(Exception err){
                system.debug('### CreateUserDataServiceImpl.assignRoletoUser():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
                ErrorLogUtility.writeLog('CreateUserDataServiceImpl.assignRoletoUser()',err.getLineNumber(),'assignRoletoUser()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null)); 
              }
              system.debug('###userobj 3---'+userobj);
        }
    }
    
    @future(callout=true)
    public static void updateContact(string contactid,map<string,string> ConFieldValues){
       System.debug('***contactid--'+contactid);
       System.debug('***ConFieldValues--'+ConFieldValues);
       Contact Conobj;
       if(contactid != null && contactid != ''){
           Conobj = [select id,ReportsToid,firstName, lastName, email, phone from Contact where id=:contactid];
           system.debug('****contactid---'+contactid);
           if(Conobj != null){
               if(ConFieldValues.get('ReportsToid') != null ){
                    Conobj.ReportsToid = ConFieldValues.get('ReportsToid');//Contactid
                }
                if(ConFieldValues.get('FirstName') != null ){
                    Conobj.FirstName = ConFieldValues.get('FirstName');
                }
                if(ConFieldValues.get('LastName') != null){
                    Conobj.LastName = ConFieldValues.get('LastName');
                }
                if(ConFieldValues.get('Email') != null){
                    Conobj.Email = ConFieldValues.get('Email');
                }
                if(ConFieldValues.get('Phone') != null){
                    Conobj.Phone = ConFieldValues.get('Phone');
                }
           }
       }
       if(Conobj.id != null){
           try{
                update Conobj;
           }catch(Exception err){
            system.debug('### CreateUserDataServiceImpl.updateContact():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('CreateUserDataServiceImpl.updateContact()',err.getLineNumber(),'updateContact()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
           }
           system.debug('****Conobj after update ---'+Conobj);
       }
    }
     public UserLogin UnfreezeUser(string strUserId,boolean dmlOperation){
            // locak user
        List<UserLogin> lstUserLogin =[SELECT id, userid, isfrozen  FROM UserLogin WHERE userid=:strUserId LIMIT 1];
        if(!lstUserLogin.isEmpty()){
            lstUserLogin[0].isfrozen = false;
            if(dmlOperation){
                update lstUserLogin[0];
            }
             return lstUserLogin[0];
        }
        return new UserLogin();
    }
    
}