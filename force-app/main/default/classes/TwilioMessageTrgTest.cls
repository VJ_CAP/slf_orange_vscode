/**
* Description: Test Class to Cover TwilioMessageTrg. 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma K         01/22/2019          Created 
******************************************************************************************/
@isTest
public class TwilioMessageTrgTest {
    public static testMethod void myUnitTest(){
            
        TriggerFlags__c tfg = new TriggerFlags__c();
        tfg.Name = 'TwilioMessageTrg';
        tfg.isActive__c = true;
        insert tfg;
                        
        TriggerFlags__c conTrgFlag = new TriggerFlags__c(Name='Contact',isActive__c=True);
        insert conTrgFlag;
        
        TriggerFlags__c accTrgFlag = new TriggerFlags__c(Name='Account',isActive__c=True);
        insert accTrgFlag;
        
        TriggerFlags__c oppTrgFlag = new TriggerFlags__c(Name='Opportunity',isActive__c=True);
        insert oppTrgFlag;
        
        TriggerFlags__c uwTrgFlag = new TriggerFlags__c(Name='Underwriting_File__c',isActive__c=True);
        insert uwTrgFlag;
        
        TriggerFlags__c drawTrgFlag = new TriggerFlags__c(Name='Draw_Requests__c',isActive__c=True);
        insert drawTrgFlag;
        
        TriggerFlags__c tlwoTrgFlag = new TriggerFlags__c(Name='TwilioSF__Message__c',isActive__c=True);
        insert tlwoTrgFlag;
        
        TriggerFlags__c fundingPackg= new TriggerFlags__c(Name='Funding_Data__c',isActive__c=True);
        insert fundingPackg;
        
        List<Twilio_Templates__c> LstTemplates = new List<Twilio_Templates__c>();
        Twilio_Templates__c temp = new Twilio_Templates__c();
        temp.Reply_Text__c = 'STOP';
        temp.Message_Type__c= ' OptOut';
        temp.Response_Text__c = 'Decision text';
        LstTemplates.add(temp);
         
        Twilio_Templates__c temp1 = new Twilio_Templates__c();
        temp1.Reply_Text__c = 'Yes';
        temp1.Response_Text__c = 'Decision text';
        LstTemplates.add(temp1);
        
        Twilio_Templates__c tempNo = new Twilio_Templates__c();
        tempNo.Reply_Text__c = 'No';
        tempNo.Response_Text__c = 'Decision text';
        LstTemplates.add(tempNo);
         
        Twilio_Templates__c temp2 = new Twilio_Templates__c();
        temp2.Reply_Text__c = 'START';
        temp2.Message_Type__c= 'OptIn';
        temp2.Response_Text__c = 'Decision text';
        LstTemplates.add(temp2);
        
        Twilio_Templates__c tempErr = new Twilio_Templates__c();
        tempErr.Reply_Text__c = 'Error';
        tempErr.Message_Type__c= 'Static';
        tempErr.Response_Text__c = 'Decision text';
        LstTemplates.add(tempErr);
        
        Twilio_Templates__c tempHelp = new Twilio_Templates__c();
        tempHelp.Reply_Text__c = 'Help';
        tempHelp.Response_Text__c = 'Decision text';
        tempHelp.Message_Type__c= 'Static';
        LstTemplates.add(tempHelp);
        insert LstTemplates ;
        
        List<Account> accList =  new List<Account>();
        Account accObj = new Account(name='Test Account',Solar_Enabled__c = True,SMS_Opt_in__c = True);
        accList.add(accObj);
        Account accObj1 = new Account(name='Test Account1',Solar_Enabled__c = True,SMS_Opt_in__c = True);
        accList.add(accObj1);
        insert accList;
        
        
        
        Draw_Allocation__c drwAllctn = new Draw_Allocation__c(Account__c=accList[0].Id);
        insert drwAllctn;
        
        Contact con = new Contact();
        con.AccountId = accList[0].Id;
        con.Phone = '9494908462';
        con.LastName = 'Test';
        insert con;
        
        Opportunity oppObj = new Opportunity(Name='OLX',AccountId=accList[0].id,StageName='Qualified',CloseDate = Date.parse('01/18/2019'));
        oppObj.Primary_Contact__c = con.Id;
        oppObj.Project_Category__c = 'Home';
        insert oppObj;
        
        Underwriting_File__c undrObj = new Underwriting_File__c(Name='ABC',Opportunity__c=oppObj.Id);
        insert undrObj;
        
        TwilioSF__Message__c twilioYes = new TwilioSF__Message__c();
        twilioYes.TwilioSF__Status__c = 'received';
        twilioYes.TwilioSF__Body__c = 'Yes';
        twilioYes.TwilioSF__From_Number__c = '+19494908462';
        twilioYes.TwilioSF__To_Number__c  = '12136993182';
        twilioYes.TwilioSF__Direction__c = 'inbound';
        insert twilioYes;
        
        List<Draw_Requests__c> drwReqList = new List<Draw_Requests__c>();
        Draw_Requests__c drawObj1 = new Draw_Requests__c(Amount__c= 10000,Primary_Applicant_Email__c='adithya.kousika@capgemini.com',Underwriting__c=undrObj.id,Status__c = 'Requested');
        drwReqList.add(drawObj1);
        Draw_Requests__c drawObj2 = new Draw_Requests__c(Amount__c= 10000,Primary_Applicant_Email__c='adithya.kousika@capgemini.com',Underwriting__c=undrObj.id,Status__c = 'Approved');
        drwReqList.add(drawObj2);
        insert drwReqList;
        Draw_Requests__c drawObj3 = new Draw_Requests__c(Amount__c= 10000,Primary_Applicant_Email__c='adithya.kousika@capgemini.com',Underwriting__c=undrObj.id,Status__c = 'Requested');
        insert drawObj3;
        
        TwilioSF__Message__c tfmYes = new TwilioSF__Message__c();
        tfmYes.TwilioSF__Status__c = 'received';
        tfmYes.TwilioSF__Body__c = 'Yes';
        tfmYes.TwilioSF__From_Number__c = '+19494908462';
        tfmYes.TwilioSF__To_Number__c  = '12136993182';
        tfmYes.TwilioSF__Direction__c = 'inbound';
        insert tfmYes;
        delete drawObj3;
        
        TwilioSF__Message__c tfmHelp = new TwilioSF__Message__c();
        tfmHelp.TwilioSF__Status__c = 'received';
        tfmHelp.TwilioSF__Body__c = 'test';
        tfmHelp.TwilioSF__From_Number__c = '+19494908462';
        tfmHelp.TwilioSF__To_Number__c  = '12136993182';
        tfmHelp.TwilioSF__Direction__c = 'inbound';
        insert tfmHelp;
        
        TwilioSF__Message__c tfmStop = new TwilioSF__Message__c();
        tfmStop.TwilioSF__Status__c = 'received';
        tfmStop.TwilioSF__Body__c = 'stop';
        tfmStop.TwilioSF__From_Number__c = '+19494908462';
        tfmStop.TwilioSF__To_Number__c  = '12136993182';
        tfmStop.TwilioSF__Direction__c = 'inbound';
        insert tfmStop;
        
        TwilioSF__Message__c tfmStart = new TwilioSF__Message__c();
        tfmStart.TwilioSF__Status__c = 'received';
        tfmStart.TwilioSF__Body__c = 'START';
        tfmStart.TwilioSF__From_Number__c = '+19494908462';
        tfmStart.TwilioSF__To_Number__c  = '12136993182';
        tfmStart.TwilioSF__Direction__c = 'inbound';
        insert tfmStart;
        
        TwilioSF__Message__c twilioYes1 = new TwilioSF__Message__c();
        twilioYes1.TwilioSF__Status__c = 'received';
        twilioYes1.TwilioSF__Body__c = 'Yes';
        twilioYes1.TwilioSF__From_Number__c = '+19494908462';
        twilioYes1.TwilioSF__To_Number__c  = '12136993182';
        twilioYes1.TwilioSF__Direction__c = 'inbound';
        insert twilioYes1;
        
        Draw_Requests__c drawObj4 = new Draw_Requests__c(Amount__c= 10000,Primary_Applicant_Email__c='adithya.kousika@capgemini.com',Underwriting__c=undrObj.id,Status__c = 'Requested');
        insert drawObj4;
        
        TwilioSF__Message__c tfmNo = new TwilioSF__Message__c();
        tfmNo.TwilioSF__Status__c = 'received';
        tfmNo.TwilioSF__Body__c = 'No';
        tfmNo.TwilioSF__From_Number__c = '+19494908462';
        tfmNo.TwilioSF__To_Number__c  = '12136993182';
        tfmNo.TwilioSF__Direction__c = 'inbound';
        insert tfmNo;
        
        Draw_Requests__c drawObj5 = new Draw_Requests__c(Amount__c= 10000,Primary_Applicant_Email__c='adithya.kousika@capgemini.com',Underwriting__c=undrObj.id,Status__c = 'Requested');
        insert drawObj5;
        
        TwilioSF__Message__c tfmHelp1 = new TwilioSF__Message__c();
        tfmHelp1.TwilioSF__Status__c = 'received';
        tfmHelp1.TwilioSF__Body__c = 'Help';
        tfmHelp1.TwilioSF__From_Number__c = '+19494908462';
        tfmHelp1.TwilioSF__To_Number__c  = '12136993182';
        tfmHelp1.TwilioSF__Direction__c = 'inbound';
        insert tfmHelp1;
        
        TwilioMessageTriggerHandler.runOnce();  
        
    }
}