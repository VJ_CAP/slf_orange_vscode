/**
* Description:  Validate Document Upload API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           10/18/2017          Created
******************************************************************************************/

public with sharing class ValidateDocumentUploadDataServiceImpl extends RestDataServiceBase{
    
    public static Integer ifc = 0; //Srikanth added to track if new folder is created or not
    
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        ValidateDocumentUploadDataObject validateDocUploadDataObject = new ValidateDocumentUploadDataObject(); 
        ValidateDocumentUploadDataObject reqData = (ValidateDocumentUploadDataObject)stsDtObj;
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try{    
            system.debug('### Entered into transformOutput() of '+ValidateDocumentUploadDataServiceImpl.class);
        
            ValidateDocumentUploadDataObject.OutputWrapper  o = new ValidateDocumentUploadDataObject.OutputWrapper();
            o.success = false;
            o.isFolderAlreadyExist = false;
            o.isFileNameAlreadyExist = false;
            o.loggedInUserName = userinfo.getFirstName()+' '+userinfo.getLastName();

            set<String> documentFolderNames = new set<String>{};
            String externalIdval;
            map<String,SF_Category_Map__c> categoryFolderMap = new map<String,SF_Category_Map__c>{};        

            for (SF_Category_Map__c cm : SF_Category_Map__c.getAll().values())
            {
                documentFolderNames.add (cm.Folder_Name__c);
            }
            
            if (documentFolderNames.contains(reqData.documentFolderType) == false && null != reqData.documentFolderType && !reqData.documentFolderType.containsIgnoreCase('Archive'))
            {
                o.returnCode = '280';
                o.errorMessage = ErrorLogUtility.getErrorCodes('280', null);
                
                validateDocUploadDataObject.output  = o; 
                return validateDocUploadDataObject;
            }
           
            if((reqData.opportunityId==null || String.isBlank(reqData.opportunityId)) && (reqData.externalId==null || String.isBlank(reqData.externalId))){
                o.returnCode = '203';
                o.errorMessage = ErrorLogUtility.getErrorCodes('203', null);
                validateDocUploadDataObject.output  = o; 
                return validateDocUploadDataObject;
            }
            if(reqData.opportunityId!=null && String.isNotBlank(reqData.opportunityId) && reqData.externalId!=null && String.isNotBlank(reqData.externalId)){
                o.returnCode = '206';
                o.errorMessage = ErrorLogUtility.getErrorCodes('206', null);
                
                validateDocUploadDataObject.output  = o; 
                return validateDocUploadDataObject;
            }
            system.debug('### opportunityId '+reqData.opportunityId);
            if(reqData.opportunityId!= null && String.isNotBlank(reqData.opportunityId))
            {
                if ([SELECT COUNT() FROM Opportunity WHERE Id =: reqData.opportunityId LIMIT 1] == 0)
                {
                    o.returnCode = '204';
                    o.errorMessage = ErrorLogUtility.getErrorCodes('204', null);
                    
                    validateDocUploadDataObject.output  = o; 
                    return validateDocUploadDataObject;
                }
            }
            else if(reqData.externalId != null && String.isNotBlank(reqData.externalId))
            {
                List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()]; 
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalIdval = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.externalId;
                    }else{
                        o.returnCode = '400';
                        o.errorMessage = ErrorLogUtility.getErrorCodes('400', null);
                        validateDocUploadDataObject.output  = o; 
                        return validateDocUploadDataObject;
                    }
                    
                }
                
                if ([SELECT COUNT() FROM Opportunity WHERE Partner_Foreign_Key__c = :externalIdval LIMIT 1] == 0)
                {
                    o.returnCode = '204';
                    o.errorMessage = ErrorLogUtility.getErrorCodes('204', null);
                    
                    validateDocUploadDataObject.output  = o; 
                    return validateDocUploadDataObject;
                }
            }


            map<String,String> disallowedStatusMessages = new map<String,String>
            {
                'Project Completed' => 'No more documents cannot be uploaded as the project has been completed',
                'Declined' => 'No more documents cannot be uploaded as the project has been declined',
                'Project Withdrawn' => 'No more documents cannot be uploaded as the project has been withdrawn'
            };

            try
            {
                String conditions = '';

                if (reqData.opportunityId != null && String.isNotBlank(reqData.opportunityId))
                {
                    conditions = 'Opportunity__c=\''+reqData.opportunityId+'\'';
                }
                else if (reqData.externalId!=null && String.isNotBlank(reqData.externalId))
                {
                    conditions = 'opportunity__r.Partner_Foreign_Key__c=\''+externalIdval+'\'';
                }

                String queryStr = 'SELECT Id, Project_Status__c,opportunity__r.id, opportunity__r.Application_Status__c,opportunity__r.StageName FROM Underwriting_File__c WHERE '+ conditions +' ORDER BY CreatedDate DESC LIMIT 1';

                System.debug('### Query string:'+queryStr);

                //Underwriting_File__c uf = [SELECT Id, Project_Status__c FROM Underwriting_File__c WHERE Opportunity__c = :opportunityId ORDER BY CreatedDate DESC LIMIT 1];
                
                List<Underwriting_File__c> underwritingLst = new List<Underwriting_File__c>();

                underwritingLst = Database.query(queryStr);

                if(underwritingLst != null && underwritingLst.size() > 0)
                {
                    Underwriting_File__c uf=underwritingLst.get(0);
                    reqData.opportunityId = uf.opportunity__r.id;
                    if((uf.opportunity__r.application_status__c == 'Auto Declined'|| uf.opportunity__r.application_status__c == 'Manual Declined')&&(uf.opportunity__r.StageName != 'Change Order Pending'))
                    {
                        o.returnCode = '282';
                        o.errorMessage = ErrorLogUtility.getErrorCodes('282', null);
                        validateDocUploadDataObject.output  = o; 
                        return validateDocUploadDataObject;
                    }else if(uf.Project_Status__c != null && uf.Project_Status__c == 'Project Withdrawn'){
                        o.returnCode = '366';
                        o.errorMessage = ErrorLogUtility.getErrorCodes('366', null);
                        validateDocUploadDataObject.output  = o; 
                        return validateDocUploadDataObject;
                    }
                }
                else
                {
                    //o.errorMessage = 'Unable to read Underwriting File';
                    o.returnCode = '281';
                    o.errorMessage = ErrorLogUtility.getErrorCodes('281', null);
                    validateDocUploadDataObject.output  = o; 
                    return validateDocUploadDataObject;
                }
            }
            catch (Exception e)
            {
                //o.errorMessage = 'Unable to read Underwriting File: ' + e.getMessage();
                o.returnCode = '281';
                o.errorMessage = ErrorLogUtility.getErrorCodes('281', null);
                validateDocUploadDataObject.output  = o; 
                return validateDocUploadDataObject;
            }

            BoxPlatformApiConnection connection;

            try
            {
                connection = BoxAuthentication.getConnection();
            }
            catch (Exception e)
            {
                o.errorMessage = 'Unable to authenticate with Box server: ' + e.getMessage();
            }

            o.accessToken = (connection == null ? 'testaccesstokenonly' : connection.accessToken);

            String opportunityFolderId;

            try
            {
                Box.Toolkit boxToolkit = new Box.Toolkit();
                opportunityFolderId = boxToolkit.createFolderForRecordId (reqData.opportunityId, null, true);

                if (opportunityFolderId == null)
                {
                    o.success = false;
                    o.errorMessage = 'Call to boxToolkit.createFolderForRecordId failed';
                    if (Test.isRunningTest() == false)
                    {
                        validateDocUploadDataObject.output  = o; 
                        return validateDocUploadDataObject;
                    }
                }
                else{
                    o.opportunityFolderId = opportunityFolderId;
                }
            }
            catch (Exception e) 
            {
                o.success = false;
                o.errorMessage = e.getMessage();
            }
            Boolean fileExists = false;

            o.fileName = reqData.fileName;
            
            String documentFolderId = null;
            if(opportunityFolderId!=null && connection!=null){
                documentFolderId = getChildFolderId (opportunityFolderId,reqData.documentFolderType, connection.accessToken);
            }
            //Srikanth - added isFolderCreated flag to the response
            if(ifc == 1)
            o.isFolderAlreadyExist = true;
            
             String sharedLink = '';
            if(documentFolderId!=null && connection!=null){
                sharedLink = getFolderSharedLinkPortal ( documentFolderId, connection.accessToken);
                BoxFolder f = new BoxFolder(connection, documentFolderId);
                
                //Added by Suresh to fix box.com error by handling custom exception
                list<BoxItem.Info> boxItems = null;
                try{
                    boxItems = f.getChildren();
                }
                catch(Exception err){
                    system.debug('### ValidateDocumentUploadDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
                    ErrorLogUtility.writeLog('ValidateDocumentUploadDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',reqData.opportunityId);
                }
                
                if(boxItems!=null && !boxItems.isEmpty()){
                    for (BoxItem.Info i : f.getChildren())
                    {
                        if (i.Name == reqData.fileName)
                        {
                            o.existFileId = i.Id;
                            fileExists = true;
                            break;
                        }
                    }
                }
            }
            //  check if file with name = fileName already exists, if so return a name with a timestamp
            if (fileExists == true)
            {
                o.isFileNameAlreadyExist = true;
                String fileExt = '';
                if(reqData.fileName.contains('.')){
                    fileExt = reqData.fileName.substring(reqData.fileName.indexOf('.'));
                    reqData.fileName = reqData.fileName.replace(fileExt, '');
                }
                o.fileName = reqData.fileName + '_' +System.now().format('yyyyMMddhhmmss') + fileExt;
            }
            
            //Comented by Adithya as part of 1930
            //Underwriting_File__c uf = [SELECT Id FROM Underwriting_File__c WHERE Opportunity__c = : reqData.opportunityId LIMIT 1];
            
            //Added by Adithya as part of 1930
            Box_Fields__c boxFldRec = [SELECT Id,Underwriting__c FROM Box_Fields__c WHERE Underwriting__r.Opportunity__c =: reqData.opportunityId LIMIT 1];
            
            for (SF_Category_Map__c cm : SF_Category_Map__c.getAll().values())
            {
                categoryFolderMap.put (cm.Folder_Name__c, cm);
            }
           
            String ufIDFieldName = null;
            String ufTSFieldName = null;

            SF_Category_Map__c cat = categoryFolderMap.get(reqData.documentFolderType);

            if (cat != null) 
            {
                if(!o.isFolderAlreadyExist)
                {
                    System.debug('### isFolderAlreadyExist1'+o.isFolderAlreadyExist);
                    if(!String.isEmpty(sharedLink))
                        boxFldRec.put (cat.Folder_Id_Field_on_Underwriting__c, sharedLink); //updated by Adithya as part of 1930
                        boxFldRec.put (cat.TS_Field_On_Underwriting__c, System.now()); //updated by Adithya as part of 1930
                        boxFldRec.put (cat.Last_Run_TS_Field_on_Underwriting_File__c, System.now()); //updated by Adithya as part of 1930               
                }else{
                    System.debug('### isFolderAlreadyExist2'+o.isFolderAlreadyExist);
                    boxFldRec.put (cat.Last_Run_TS_Field_on_Underwriting_File__c, System.now()); //updated by Adithya as part of 1930
                }

                if(!System.isFuture())  //Srikanth - added Future check to avoid uncommitted work pending error while uploading loan agreements through DocuSignStatusHandler
                {
                    o.sharedLink = sharedLink;
                    update boxFldRec; //updated by Adithya as part of 1930
                }
                else
                {
                    o.sharedLink = sharedLink;
                    o.uwId = boxFldRec.Underwriting__c; //updated by Adithya as part of 1930
                }
            }         

            o.documentFolderId = documentFolderId;
            o.success = true;
            
            
            system.debug('### output ' +o );
            validateDocUploadDataObject.output  = o; 
        }catch(Exception err){
            system.debug('### ValidateDocumentUploadDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('ValidateDocumentUploadDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null)); 
        }    
        system.debug('### output '+validateDocUploadDataObject );
        system.debug('### Exit from transformOutput() of '+ ValidateDocumentUploadDataServiceImpl.class);
        return validateDocUploadDataObject;
    }
    
    //  ************************************************************************

     public static String getChildFolderID(String parentBoxFolderId, String folderName, String accessToken)
     {
         system.debug('### Entered from getChildFolderID() of '+ ValidateDocumentUploadDataServiceImpl.class);
         BoxApiConnection api = new BoxApiConnection (accessToken);

         String fid = null;
         try 
         {
             BoxFolder bf = new BoxFolder(api,parentBoxFolderId); 
             Boxfolder.Info f =  bf.createFolder(folderName);
             fid = f.Id;
         } 
         catch (Exception e) 
         {
             String m = e.getMessage();
             if (m.indexOf('item_name_in_use') > 0) 
             {
                 Integer n = m.indexOf(',"id":');
                 Integer n2 = m.IndexOf('"',n+7);
                 fid = m.substring(n+7,n2); 
                 ifc = 1;  //Srikanth - indicating that new folder was created.
             }
        }  
        system.debug('### Exit from getChildFolderID() of '+ ValidateDocumentUploadDataServiceImpl.class);       
         return fid;
     }

//  ************************************************************************

      public static String getFolderSharedLinkPortal(String boxFolderID, String token) 
     {
        system.debug('### Entered from getFolderSharedLinkPortal() of '+ ValidateDocumentUploadDataServiceImpl.class);
        BoxApiConnection  api = new BoxApiConnection(token);
         
        BoxGenericJsonObject sharedLinkObject;
        
        try 
        { 
            String url = api.getBaseUrl() + String.format('folders/{0}/', new String[] {boxFolderID});
            BoxApiRequest request = new BoxApiRequest(api, url, BoxApiRequest.METHOD_PUT);
            request.setBody('{"shared_link": {"access": "open"}}');
            request.setTimeout(api.timeout);
            request.addJsonContentTypeHeader();

            HttpResponse response = request.send();
            System.Debug('### Shared Link Response ' + response);
            if (response.getStatusCode() != 200) 
            {
                return null;
            }
            String responseBody = BoxApiRequest.getBoxResourceResponseBody(response, 'BoxFile.getFileInfo');
            BoxGenericJsonObject responseObject = new BoxGenericJsonObject(responseBody);
            sharedLinkObject =  new BoxGenericJsonObject(responseObject.getValue('shared_link'));              
        } 
        catch (Exception e) {}
        system.debug('### Exit from getFolderSharedLinkPortal() of '+ ValidateDocumentUploadDataServiceImpl.class);
        return sharedLinkObject != NULL ? sharedLinkObject.getValue('url') : '';
    }

    
}