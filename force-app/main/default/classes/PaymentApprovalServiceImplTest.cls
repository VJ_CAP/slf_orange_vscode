@isTest(seeAllData=false)
public class PaymentApprovalServiceImplTest{    
    @testsetup static void createtestdata(){
        //SLFUtilityDataSetup.initData();    
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        TriggerFlags__c trgAccflagDrawRequest = new TriggerFlags__c();
        trgAccflagDrawRequest.Name ='Draw_Requests__c';
        trgAccflagDrawRequest.isActive__c =true;
        trgLst.add(trgAccflagDrawRequest);
        
        insert trgLst;
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
       
               
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.SMS_Opt_in__c = true;
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        system.debug('@@ I am here' +portalUser.Contact.AccountId);
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        //update acc;
        
        
        AccountShare accShare = new AccountShare(); //a new empty AccountShare object
        accShare.userorgroupid = portalUser.Id;
        accShare.accountid = con.accountid;
        accShare.accountaccesslevel = 'Edit';
        accShare.OpportunityAccessLevel = 'None';
        accShare.CaseAccessLevel = 'None';
        insert accShare;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact personcon = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = personAcc.Id);
        insert personcon; 
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        personAcc.PersonBirthdate = Date.ValueOf('1929-03-10');
        update personAcc;
        
        AccountShare personAccShare = new AccountShare(); //a new empty AccountShare object
        personAccShare.userorgroupid = portalUser.Id;
        personAccShare.accountid = personAcc.Id;
        personAccShare.accountaccesslevel = 'Edit';
        personAccShare.OpportunityAccessLevel = 'None';
        personAccShare.CaseAccessLevel = 'None';
        insert personAccShare;
        
        System.runAs(portalUser)    {
            //Insert SLF Product Record
            List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            //insert prod;
            prodList.add(prod);
            
            //Insert opportunity record
            List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;
            personOpp.Hash_Id__c = '12345678910';            
            //insert personOpp;
            oppList.add(personOpp);
            
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            opp.Hash_Id__c = '98765432100';
            //insert opp;
            oppList.add(opp);
            
            insert oppList;
            
            
            Underwriting_File__c undStpbt = new Underwriting_File__c();           
            undStpbt.Opportunity__c = opp.id;
            undStpbt.Approved_for_Payments__c = true;
            undStpbt.M0_Approval_Date__c = System.now();
            undStpbt.Project_Status__c = 'M1';
            insert undStpbt;
            
            List<Draw_Requests__c> drawList = new List<Draw_Requests__c>();
            Draw_Requests__c drawReq = new Draw_Requests__c();
            drawReq.Level__c = '1';
            drawReq.Amount__c = 2000;
            drawReq.Status__c = 'Expired';
            drawReq.Hash_Id__c = '7386820465';
            drawReq.Underwriting__c = undStpbt.id;
            drawList.add(drawReq);
            insert drawList;
            System.debug('****Draw Request List Test Class***'+drawList);
        }
    }
    /**
*
* Description: This method is used to cover PaymentApprovalServiceImpl
*
*/
    private testMethod static void paymentApprovalTest(){
        
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        
        Map<String, String> userMap = new Map<String, String>();
        Map<string,Draw_Requests__c> drawReqMap = new Map<string,Draw_Requests__c>();//key as hash Id and value as Draw Request
        
        System.runAs(loggedInUsr[0])
        {
            Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        Underwriting_File__c undStpbt  = [select id , Opportunity__c from Underwriting_File__c where Opportunity__c =: opp.id LIMIT 1]; 
        Draw_Requests__c dreq = [Select id,Hash_Id__c,Level__c,Amount__c,Status__c,Underwriting__c  FROM Draw_Requests__c where Underwriting__c =: undStpbt.id  LIMIT 1];
        
        dreq.Hash_Id__c = '';
        update dreq;
        
            Test.starttest();
            String drawReqStr;
            
            //drawReqStr = '{"projects":[{"id":"'+opp.id+'","hashId":"'+opp.Hash_Id__c+'","externalId":"'+opp.Partner_Foreign_Key__c+'","draws":[{"hashId":"7386820465","drawMethodology":"Proactive text","status":"Approved"}]}]}';
            //SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            //drawReqStr = '{"projects":[{"id":"'+opp.id+'","draws":[{"hashId":"7386820465","drawMethodology":"Proactive text","status":"Approved"}]}]}';
            //SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
             drawReqStr = '{"projects": [{"id":"'+opp.id+'","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
                        
            dreq.Hash_Id__c = '7386820465';
            update dreq;
            System.debug('***Draw Request***'+dreq);
            // error 203
            drawReqStr = '{"projects": [{"id":"","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","status": Approved}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            // error 201
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","hashId":"7896541230","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            // error 202
            drawReqStr = '{"projects": [{"id": "0062F000006BADd123","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            // externalId
            drawReqStr = '{"projects": [{"externalId":"'+opp.Hash_Id__c+'","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            // externalId
            drawReqStr = '{"projects": [{"externalId":"'+opp.Hash_Id__c+'","draws1": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            // Positive
            drawReqStr = '{"projects": [{"id":"'+opp.id+'","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            drawReqStr = '{"projects": [{"hashid":"'+opp.Hash_Id__c+'","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            drawReqStr = '{"projects":null}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
           
             drawReqStr = '{"projects": [{"id":"'+opp.id+'","draws": [{"drawMethodology": "Proactive text","status": Approved}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
             drawReqStr = '{"projects": [{"id":"","draws": [{"drawMethodology": "Proactive text","status": Approved}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true}],[{"drawLevel":2,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
 
            drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true}],[{"drawLevel":2,"amount": "10000000","isProjectComplete": false,"resendEmail":true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            drawReqStr = '{"projects": [{"hashid":"","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
             drawReqStr = '{"projects": [{"id": "'+opp.Id+'","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawLevel":1,"amount": "10000000","isProjectComplete": false,"resendEmail":true,"status": "Expired"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            dreq.Status__c = 'Requested';
            update dreq;
            
            drawReqStr = '{"projects": [{"id":"'+opp.id+'","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            dreq.Status__c = 'Approved';
            update dreq;
            
            drawReqStr = '{"projects": [{"id":"'+opp.id+'","draws": [{"hashId":"'+dreq.Hash_Id__c+'","drawMethodology": "Proactive text","isProjectComplete": false}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,drawReqStr,'paymentapproval');
            
            Test.StopTest();
        }
    }
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        system.debug('*******pricingMapstart****'+pricingMap);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        system.debug('*******pricingMap****'+pricingMap);
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
            system.debug('*****req.requestBody*****'+jsonString);
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
            system.debug('*****req.requestBody*****'+gen.getAsString());
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}