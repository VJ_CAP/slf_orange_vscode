public class QuotePaymentSchedulesGenerator {
    
    public static void run(){
        
        if(!(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))) return;
        
        Quote[] quotes = getQuotesInScope();
        
        if(trigger.isUpdate && null != quotes && !quotes.isEmpty())
            deleteStalePaymentSchedules(quotes);
        
        system.debug('quotes - '+quotes);
        if(null != quotes && !quotes.isEmpty())
            generateNewPaymentSchedules(quotes);
    }
    
    private static Quote[] getQuotesInScope(){
        
        Quote[] quotes = (Quote[])trigger.new;
        
        Quote[] ret = new Quote[]{};
        
        system.debug('### quotes - '+quotes);
        
        for(Quote quote : quotes){
            if(isInScope(quote)) 
            {
                system.debug('### isInScope - '+quote.Id+' : true');
                ret.add(quote);
            }
            else
            {
                system.debug('### isInScope - '+quote.Id+' : false');
            }
        }
        system.debug('### ret - '+ret);
        
        return ret;
    }
    
    private static Boolean isInScope(Quote quote){
        
        if(quote.Product_Loan_Type__c != 'HII' && quote.Product_Loan_Type__c != 'HIS' && quote.Product_Loan_Type__c != 'HIN' && quote.Product_Loan_Type__c != 'Solar Interest Only'){ //Updated by Ravi as part of 3666
            system.debug('### Checkpoint returning - false');
            return false;
        }
               
        if(trigger.isInsert){
            system.debug('### Checkpoint returning - '+quote.Combined_Loan_Amount__c != null && quote.SLF_Product__c != null);
            return quote.Combined_Loan_Amount__c != null && quote.SLF_Product__c != null;
        }
        else if(trigger.isUpdate){
            system.debug('### quote.Combined_Loan_Amount__c - '+quote.Combined_Loan_Amount__c);
            system.debug('### Trg Old Value - '+trigger.oldMap.get(quote.Id).get('Combined_Loan_Amount__c'));
            system.debug('### Quote Product - '+quote.SLF_Product__c);
            system.debug('### Trg Old Quote Product - '+trigger.oldMap.get(quote.Id).get('SLF_Product__c'));
            return quote.Combined_Loan_Amount__c != trigger.oldMap.get(quote.Id).get('Combined_Loan_Amount__c')
                || quote.SLF_Product__c != trigger.oldMap.get(quote.Id).get('SLF_Product__c');
        }
        else{
            return false;
        }
    }
    
    private static void deleteStalePaymentSchedules(Quote[] quotes){
        
        if(quotes.isEmpty()) return;
        
        delete [SELECT Id FROM Payment_Schedule__c WHERE Quote__c IN: quotes];
    }
    
    private static void generateNewPaymentSchedules(Quote[] quotes){
    
        system.debug('### Entered into generateNewPaymentSchedules() of '+ QuotePaymentSchedulesGenerator.class);
        System.debug('quotes - generateNewPaymentSchedules'+quotes);
        
        List<Payment_Schedule__c> newPaymentSchedules = new List<Payment_Schedule__c>();
        
        Map<Id, Product__c> productMap = getProductMap(quotes);
        
        Map<Id,Quote> updateQuoteMap = new Map<Id,Quote>(); 
        
        //Added by Deepika as part of Orange - 3622
        Map<Id,Quote> quotewithOppMap = new Map<Id,Quote>([Select id,isACH__c,Paydown_percent__c,Combined_Loan_Amount__c,Product_Loan_Type__c,Opportunity.Long_Term_APR__c, 
            Opportunity.Promo_APR__c from Quote where id in :quotes]);

        for(Quote quote : quotes){
            if(quote.Combined_Loan_Amount__c != null && productMap.containsKey(quote.SLF_Product__c)){
                Quote quoteRec = new Quote(Id=quote.id);

                Product__c product = productMap.get(quote.SLF_Product__c);
                 system.debug('### quote: '+quote);
                 system.debug('### product: '+product);
                 system.debug('### quoteRec: '+quoteRec);
                 //Added as part of ORANGE - ORANGE-2405 - Start
                 if((quote.Product_Loan_Type__c == 'HIS' || quote.Product_Loan_Type__c == 'HIN') && null != quote.Combined_Loan_Amount__c && null != quote.Long_Term_Term_mo__c){ // Updated by Ravi as part of 3666
                    if(quote.isACH__c && null!= quote.ACH_Promo_APR__c)
                    {
                        quoteRec.Unofficial_HIS_PMT_Value__c = LoanCalculator.PMT(quote.Combined_Loan_Amount__c,Integer.valueOf(quote.Long_Term_Term_mo__c),double.valueOf(quote.ACH_Promo_APR__c/100/12));
                    }
                    else if(null!= quote.NonACH_Promo_APR__c)
                    {
                        quoteRec.Unofficial_HIS_PMT_Value__c = LoanCalculator.PMT(quote.Combined_Loan_Amount__c,Integer.valueOf(quote.Long_Term_Term_mo__c),double.valueOf(quote.NonACH_Promo_APR__c/100/12));
                    }
                 }
                //Added as part of ORANGE - ORANGE-2405 - End
                
                //Modified by Deepika as part of Orange - 3622 - start
                if(!quotewithOppMap.isEmpty() && quotewithOppMap.containsKey(quote.Id)){
                    newPaymentSchedules.addAll(createPaymentSchedules(quotewithOppMap.get(quote.Id), quote.Combined_Loan_Amount__c, product,quote.Product_Loan_Type__c,quote.isACH__c,null));
                }
                //Modified by Deepika as part of Orange - 3622 - end
                
                /*
                quoteRec.Monthly_Payment__c = newPaymentSchedules[0].Amount__c;
                quoteRec.Final_Monthly_Payment__c  = newPaymentSchedules[0].Amount__c;
                
                quoteRec.Monthly_Payment_without_Prepay__c = newPaymentSchedules[newPaymentSchedules.size()-1].Amount__c;
                quoteRec.FinalMonthlyPaymentwithoutPrepay__c = newPaymentSchedules[newPaymentSchedules.size()-1].Amount__c;
                */
                //Updated by Ravi as part of 3666 - Start
                //quoteRec.Final_Monthly_Payment__c  = newPaymentSchedules[0].Amount__c;
                
                system.debug('### isACH - '+quote.isACH__c);
                system.debug('### newPaymentSchedules - '+newPaymentSchedules);
                //Added by Adithya to fix ORANGE-10775 on 9/25/2019
                //Start
                Map<Id,List<Payment_Schedule__c>> quoteMap = new Map<Id,List<Payment_Schedule__c>>();
                for(Payment_Schedule__c paymentIterate : newPaymentSchedules){
                    if(quoteMap.containsKey(paymentIterate.Quote__c))
                    {
                        List<Payment_Schedule__c> paymentRec = quoteMap.get(paymentIterate.Quote__c);
                        paymentRec.add(paymentIterate);
                        quoteMap.put(paymentIterate.Quote__c,paymentRec);
                    }else{
                        quoteMap.put(paymentIterate.Quote__c,new List<Payment_Schedule__c>{paymentIterate});
                    }
                }
                List<Payment_Schedule__c> respectiveQuotePaymentSchedules = quoteMap.get(quote.id);
                //End
                //Updated By Adithya as part of 2706
                //start
                if(quote.Product_Loan_Type__c == 'Solar Interest Only') //This logic is for Solar Interest Only deals.
                {
                    quoteRec.ACH_Monthly_Payment__c = respectiveQuotePaymentSchedules[1].Amount__c;
                    quoteRec.ACH_Final_Monthly_Payment__c  = respectiveQuotePaymentSchedules[1].Amount__c;
                    
                    quoteRec.ACH_Monthly_Payment_without_Prepay__c = respectiveQuotePaymentSchedules[2].Amount__c;
                    quoteRec.ACH_FinalMonthlyPaymentWOPrepay__c = respectiveQuotePaymentSchedules[2].Amount__c;
                    
                    quoteRec.NonACH_Monthly_Payment__c = respectiveQuotePaymentSchedules[4].Amount__c;
                    quoteRec.NonACH_Final_Monthly_Payment__c  = respectiveQuotePaymentSchedules[4].Amount__c;  
                    
                    quoteRec.NonACH_Monthly_Payment_WO_Prepay__c = respectiveQuotePaymentSchedules[5].Amount__c;
                    quoteRec.NonACH_FinalMonthlyPaymentWOPrepay__c = respectiveQuotePaymentSchedules[5].Amount__c;
                
                }else{ //This logic is for Home deals.
                    if(quote.isACH__c){
                        quoteRec.ACH_Monthly_Payment__c = respectiveQuotePaymentSchedules[0].Amount__c;
                        quoteRec.ACH_Final_Monthly_Payment__c  = respectiveQuotePaymentSchedules[0].Amount__c;
                        quoteRec.ACH_Monthly_Payment_without_Prepay__c = respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c;
                        quoteRec.ACH_FinalMonthlyPaymentWOPrepay__c = respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c;
                    }else{
                        quoteRec.NonACH_Monthly_Payment__c = respectiveQuotePaymentSchedules[0].Amount__c;
                        quoteRec.NonACH_Final_Monthly_Payment__c  = respectiveQuotePaymentSchedules[0].Amount__c;                    
                        quoteRec.NonACH_Monthly_Payment_WO_Prepay__c = respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c;
                        quoteRec.NonACH_FinalMonthlyPaymentWOPrepay__c = respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c;
                    }
                }
                //End
                
                // Added by Venkat as part of 1621- Start
                system.debug('Promo_Monthly_Payment_Max__c - '+respectiveQuotePaymentSchedules[0].Amount__c);
                system.debug('Standard_Monthly_Payment_Max_Amount__c - '+respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c);
                //quoteRec.Promo_Monthly_Payment_Max__c = newPaymentSchedules[0].Amount__c;
                //quoteRec.Standard_Monthly_Payment_Max_Amount__c = newPaymentSchedules[newPaymentSchedules.size()-1].Amount__c;
                
                Decimal totalPayment = 0;
                //Updated By Adithya as part of 2706
                //start             
                for(Payment_Schedule__c paymentIterate : respectiveQuotePaymentSchedules){
                    system.debug('totalPayment =========='+paymentIterate.Amount__c);
                    system.debug('totalPayment =========='+paymentIterate.Months__c);
                    system.debug('ACH_Type__c =========='+paymentIterate.ACH_Type__c);
                    if(paymentIterate.ACH_Type__c == null)
                    {
                        if(quote.isACH__c == true)
                            paymentIterate.ACH_Type__c = 'ACH';
                        else if(quote.isACH__c == false)
                            paymentIterate.ACH_Type__c = 'NonACH';
                    }
                    if(quote.isACH__c == true && paymentIterate.ACH_Type__c == 'ACH')
                    {
                        totalPayment += paymentIterate.Amount__c * paymentIterate.Months__c;
                    }else if(quote.isACH__c == false && paymentIterate.ACH_Type__c == 'NonACH'){
                        totalPayment += paymentIterate.Amount__c * paymentIterate.Months__c;
                    }
                }
                //End
                system.debug('totalPayment =========='+totalPayment );
               
                // If condition added for Orange-7660
                if(quotewithOppMap.get(quoteRec.Id).Opportunity.Long_Term_APR__c == 0.00 || quotewithOppMap.get(quoteRec.Id).Opportunity.Long_Term_APR__c == null){
                    quoteRec.Total_of_Payments_Loan_Amount__c = quote.Combined_Loan_Amount__c;
                }else{
                    quoteRec.Total_of_Payments_Loan_Amount__c = totalPayment;
                } 
                /*if(optyMap.get(quote.OpportunityId).Long_Term_APR__c == 0.00 || optyMap.get(quote.OpportunityId).Long_Term_APR__c == null){
                    quoteRec.Total_of_Payments_Loan_Amount__c = quote.Combined_Loan_Amount__c;                    
                }else{
                    quoteRec.Total_of_Payments_Loan_Amount__c = totalPayment;
                }*/
                // Added as part of 1621 - End
                updateQuoteMap.put(quoteRec.Id,quoteRec);
                
            }

        }
        
        insert newPaymentSchedules;
        if(!updateQuoteMap.isEmpty())
        {
            update updateQuoteMap.values();
            //String jsonMap = JSON.serialize(updateQuoteMap.Values());
            //updateQuote(jsonMap);
        }
    }
    
    public static List<Payment_Schedule__c> createPaymentSchedules(Quote quoteObj, Decimal loanAmount, Product__c product,string productType,Boolean isACH,Decimal paydownPercentage){
        
        system.debug('### Entered into createPaymentSchedules() of '+ QuotePaymentSchedulesGenerator.class);
        //Added by Deepika as part of Orange - 3622
        Decimal promoAPRInt = product.Promo_APR__c;
        
        /*
        //Commented By Adithya to fix ORANGE-10775 on 9/25/2019
        //start
        if(quoteObj != null){
            promoAPRInt = quoteObj.Opportunity.Promo_APR__c;
        }
        //End
        */
        //Added by Deepika as part of Orange - 3622
        
        Integer term = (Integer)product.Term_mo__c,
        //drawPeriod = product.Draw_Period__c == null ? 0 : (Integer)product.Draw_Period__c, //Updated by Ravi as part of 3666
        drawPeriod = product.Draw_Period__c == null ? null : (Integer)product.Draw_Period__c, 
        promoPeriod = product.Promo_Term__c == null ? 0 : (Integer)product.Promo_Term__c;
        
        Decimal APR = product.APR__c == null ? 0 : product.APR__c/100,
        promoAPR = promoAPRInt == null ? 0 : promoAPRInt/100, 
        NonACHAPR = product.NonACH_APR__c == null ? 0 : product.NonACH_APR__c/100,
        NonACHpromoAPR = product.NonACH_Promo_APR__c == null ? 0 : product.NonACH_Promo_APR__c/100,
        
        
        /* Updated by Ravi as part of 3666
        promoFixedPayment = product.Promo_Fixed_Payment__c == null ? 0 : product.Promo_Fixed_Payment__c, 
        promoPercentBalance = product.Promo_Percentage_Balance__c == null ? 0 : product.Promo_Percentage_Balance__c/100, 
        promoPercentPayment = product.Promo_Percentage_Payment__c == null ? 0 : product.Promo_Percentage_Payment__c/100;
        */
        promoFixedPayment = product.Promo_Fixed_Payment__c, 
        promoPercentBalance = product.Promo_Percentage_Balance__c == null ? null : product.Promo_Percentage_Balance__c/100, 
        promoPercentPayment = product.Promo_Percentage_Payment__c == null ? null : product.Promo_Percentage_Payment__c/100;
        
        LoanCalculator.PaymentSchedule[] paymentSchedules;
        if((null != quoteObj && (quoteObj.Product_Loan_Type__c == 'HIS' || quoteObj.Product_Loan_Type__c == 'HII' || quoteObj.Product_Loan_Type__c == 'HIN')) ||
            (string.isNotBlank(productType) && (productType == 'HIS' || productType == 'HII' || productType == 'HIN'))
        ){
                paymentSchedules = LoanCalculator.getPaymentSchedules(
                        loanAmount, term, APR, drawPeriod, promoPeriod, promoAPR, promoPercentPayment, promoPercentBalance, promoFixedPayment);
        }else if((null != quoteObj && quoteObj.Product_Loan_Type__c == 'Solar Interest Only') || (string.isNotBlank(productType) && productType == 'Solar Interest Only')){ //added by Adithya as part of ORANGE-2706 on 10/1/2019 
            //Start
            if(null != paydownPercentage)
                paydownPercentage = paydownPercentage/100;  
            else if(null != quoteObj && null != quoteObj.Paydown_percent__c)    
                paydownPercentage = quoteObj.Paydown_percent__c/100;
            else
                paydownPercentage = 0.3;
            
            Decimal deferredInterest = 0.00;
            Integer promoTerm = null; 
            promoAPR = null;
            NonACHpromoAPR = null;
            APR = product.APR__c == null ? 0 : product.APR__c/100;
            NonACHAPR = product.NonACH_APR__c == null ? 0 : product.NonACH_APR__c/100;
            /*
            if((null != quoteObj && quoteObj.isACH__c) || isACH){
                APR = product.APR__c == null ? 0 : product.APR__c/100;
            }else{
                APR = product.NonACH_APR__c == null ? 0 : product.NonACH_APR__c/100;
            }
            */
            string ACHType = 'ACH';
            paymentSchedules = (LoanCalculator.getPaymentSchedulesInterestOnly(loanAmount,deferredInterest, term,promoTerm ,promoAPR,APR,paydownPercentage,ACHType));
            
            ACHType = 'NonACH';
            paymentSchedules.addAll(LoanCalculator.getPaymentSchedulesInterestOnly(loanAmount,deferredInterest, term,promoTerm ,NonACHpromoAPR,NonACHAPR,paydownPercentage,ACHType));
            
        }//End
                
        
        List<Payment_Schedule__c> ret = new List<Payment_Schedule__c>();
        
        id quoteId = null;
        if(null != quoteObj && null != quoteObj.id)
            quoteId = quoteObj.id;
        
        for(Integer i = 0; i < paymentSchedules.size(); i++){
            system.debug('paymentSchedules[i].Payment============'+paymentSchedules[i].Payment);
             system.debug('paymentSchedules[i].Payment============'+paymentSchedules[i].Months);
            ret.add(new Payment_Schedule__c(
                Quote__c = quoteId,
                Serial_No__c = i + 1,
                Amount__c = paymentSchedules[i].Payment,
                Months__c = paymentSchedules[i].Months,
                ACH_Type__c = paymentSchedules[i].ACHType
            ));
        }
        
        return ret;
    }
    
    private static Map<Id, Product__c> getProductMap(Quote[] quotes){
        
        system.debug('### Entered into getProductMap() of '+ QuotePaymentSchedulesGenerator.class);
        System.debug('### trigger context'+Trigger.isInsert);
        System.debug('### trigger context'+Trigger.isUpdate);
        Set<Id> productIds = new Set<Id>();
        
        for(Quote quote : quotes){
            
            if(quote.SLF_Product__c != null){
                productIds.add(quote.SLF_Product__c);
            }
        }
        
        if(productIds.isEmpty())
            return new Map<Id, Product__c>();
        
        return new Map<Id, Product__c>([SELECT Product_Loan_Type__c, Term_mo__c, APR__c,  Promo_APR__c,NonACH_Promo_APR__c,NonACH_APR__c, Promo_Fixed_Payment__c, 
            Promo_Percentage_Balance__c, Promo_Percentage_Payment__c, Promo_Term__c, Draw_Period__c
            FROM Product__c WHERE Id IN: productIds]);
    }
}