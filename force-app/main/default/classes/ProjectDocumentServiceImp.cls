/**
* Description: Opportunity Id Search related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class ProjectDocumentServiceImp extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ProjectDocumentServiceImp.class);
        
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean reqData ;
        if(string.isNotBlank(rw.reqDataStr))
        {
            reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        }else{
            reqData = new UnifiedBean();
        }
        UnifiedBean respData = new UnifiedBean();
      
        try{
            system.debug('### reqData '+reqData);
           
            List<UnifiedBean.ProjectDocumentWrapper> reqProjectsLst ;
            
            if(null != reqData.projectDocuments)
            {
                reqProjectsLst = reqData.projectDocuments;
            }else{
                reqProjectsLst = new List<UnifiedBean.ProjectDocumentWrapper>();
            }
            
            List<UnifiedBean.ProjectDocumentWrapper> respProjectWrapLst = new List<UnifiedBean.ProjectDocumentWrapper>();
            respData.projectDocuments = respProjectWrapLst;
            
            for(UnifiedBean.ProjectDocumentWrapper reqProjectWrap : reqProjectsLst)
            {
                String opportunityId = reqProjectWrap.projectId;
                String externalId = reqProjectWrap.externalId;
                boolean anyErrors = false;
                String newexternalId;
                UnifiedBean.ProjectDocumentWrapper respProjectDocWrap= new UnifiedBean.ProjectDocumentWrapper();
                
                if((opportunityId==null || String.isBlank(opportunityId)) && (externalId==null || String.isBlank(externalId))){
                    respData.returnCode = '206';
                    respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('206',null);
                    anyErrors = true;
                }
                if(opportunityId!=null && String.isNotBlank(opportunityId) && externalId!=null && String.isNotBlank(externalId)){
                    respData.returnCode = '206';
                    respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('206',null);
                    anyErrors = true;
                }
                if(opportunityId!=null && String.isNotBlank(opportunityId))
                {
                    List<Opportunity> opprLst = [SELECT Id,Partner_Foreign_Key__c FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
                    
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        respData.returnCode = '204';
                        respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('204',null);
                        anyErrors = true;
                    }
                    else{
                        opportunityId = opprLst.get(0).Id;
                        if(opprLst.get(0).Partner_Foreign_Key__c != null){
                            externalId = opprLst.get(0).Partner_Foreign_Key__c.substringAfter('||');
                        }
                    }
                }
                
                else if(externalId!=null && String.isNotBlank(externalId))
                {
                    
                    List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                    if(!loggedInUsr.isEmpty())
                    {
                        if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                            newexternalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+externalId;
                          }else{
                            respData.returnCode = '400';
                            respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('400',null);
                        }
                        
                    }
                    List<opportunity> opprLst = [select id,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:newexternalId limit 1];
                    if (opprLst==null || opprLst.size() == 0){
                        respData.returnCode = '204';
                        respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('204',null);
                        anyErrors = true;
                    }
                    else
                    {
                        opportunityId = opprLst.get(0).id;
                        if(opprLst.get(0).Partner_Foreign_Key__c != null){
                        externalId = opprLst.get(0).Partner_Foreign_Key__c.substringAfter('||');
                        }
                    }
                }

                if(!anyErrors)
                {
                    if(String.isNotBlank(opportunityId)){
                        
                        String fieldnames='';
                        for (SF_Category_Map__c categs : SF_Category_Map__c.getAll().values())
                        {
                            if(categs.Folder_Id_Field_on_Underwriting__c != null){
                                if(!fieldnames.contains(categs.Folder_Id_Field_on_Underwriting__c)){
                                    if (fieldnames == '') {
                                        fieldnames = categs.Folder_Id_Field_on_Underwriting__c;
                                    } else {
                                        fieldnames += ',' + categs.Folder_Id_Field_on_Underwriting__c;
                                    }
                                }
                            }
                            if(categs.TS_Field_On_Underwriting__c != null){
                                if(!fieldnames.contains(categs.TS_Field_On_Underwriting__c)){
                                    if (fieldnames == '') {
                                        fieldnames = categs.TS_Field_On_Underwriting__c;
                                    } else {
                                        fieldnames += ',' + categs.TS_Field_On_Underwriting__c;
                                    }
                                }
                            }
                            if(categs.Last_Run_TS_Field_on_Underwriting_File__c != null){
                                if(!fieldnames.contains(categs.Last_Run_TS_Field_on_Underwriting_File__c)){
                                    if (fieldnames == '') {
                                        fieldnames = categs.Last_Run_TS_Field_on_Underwriting_File__c;
                                    } else {
                                        fieldnames += ',' + categs.Last_Run_TS_Field_on_Underwriting_File__c;
                                    }
                                }
                            }
                        }
                        System.debug('### fieldnames'+fieldnames);
                        // Added by Adithya as part of 1930 
                        if(string.isNotBlank(fieldnames)){
                            fieldnames='(SELECT id,'+fieldnames+' FROM Box_Fields__r)';
                        }
                        
                        String docuFields='';
                        List<Document_Map_Setting__mdt> documentMap = [Select id,DeveloperName,label,M0_Field_Name_s__c,M1_Field_Name_s__c,M2_Field_Name_s__c,Inspection_Field_Name_s__c,Kitting_Field_Name_s__c,Permit_Field_Name_s__c from Document_Map_Setting__mdt];// Added fields by Deepika on 5/2/2019 as part of 1930
                        Map<String,Document_Map_Setting__mdt> docNameMap = new Map<String,Document_Map_Setting__mdt>();
                        for (Document_Map_Setting__mdt docuMap : documentMap){
                            docNameMap.put(docuMap.label,docuMap);
                            if(docuMap.M0_Field_Name_s__c != null){
                                for(String st: docuMap.M0_Field_Name_s__c.split(';')){
                                    if(!docuFields.contains(st)){
                                        if (docuFields == '') {
                                            docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                        } else {
                                            docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                        }
                                    }
                                }
                            }
                            if(docuMap.M1_Field_Name_s__c != null){
                                for(String st: docuMap.M1_Field_Name_s__c.split(';')){
                                    if(!docuFields.contains(st)){
                                        if (docuFields == '') {
                                            docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                        } else {
                                            docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                        }
                                    }
                                }
                            }
                            if(docuMap.M2_Field_Name_s__c != null){
                                for(String st: docuMap.M2_Field_Name_s__c.split(';')){
                                    if(!docuFields.contains(st)){
                                        if (docuFields == '') {
                                            docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                        } else {
                                            docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                        }
                                    }
                                }
                            }
                            //Added By Deepika on 5/2/2019 as part of 1930 - Start
                            if(docuMap.Inspection_Field_Name_s__c != null){
                                for(String st: docuMap.Inspection_Field_Name_s__c.split(';')){
                                    if(!docuFields.contains(st)){
                                        if (docuFields == '') {
                                            docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                        } else {
                                            docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                        }
                                    }
                                }
                            }
                            if(docuMap.Kitting_Field_Name_s__c != null){
                                for(String st: docuMap.Kitting_Field_Name_s__c.split(';')){
                                    if(!docuFields.contains(st)){
                                        if (docuFields == '') {
                                            docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                        } else {
                                            docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                        }
                                    }
                                }
                            }
                            if(docuMap.Permit_Field_Name_s__c != null){
                                for(String st: docuMap.Permit_Field_Name_s__c.split(';')){
                                    if(!docuFields.contains(st)){
                                        if (docuFields == '') {
                                            docuFields = 'Opportunity__r.Installer_Account__r.'+st;
                                        } else {
                                            docuFields += ',Opportunity__r.Installer_Account__r.' +st;
                                        }
                                    }
                                }
                            }
                            //Added By Deepika on 5/2/2019 as part of 1930 - End
                        }
                        System.debug('### docuFields'+docuFields);
                        //removed box related fields from query as there is no reference as part of 1930
                        /*String queryString = 'select Id,Opportunity__c,Project_Status__c,M0_Approval_Requested_Date__c,M1_Approval_Requested_Date__c,Opportunity__r.owner.name,Opportunity__r.Change_Order_Status__c,Opportunity__r.isChangeOrderSubmitted__c,Opportunity__r.StageName,M2_Approval_Requested_Date__c,Box_Installation_Photos_TS__c,Box_Install_Contracts_TS__c,Box_Loan_Agreements_TS__c,Box_Payment_Election_Form_TS__c,Box_Approval_Letters_TS__c,Box_Final_Designs_TS__c,Box_Government_IDs_TS__c,Box_Utility_Bills_TS__c,Opportunity__r.SyncedQuoteId,Opportunity__r.Product_Loan_Type__c,(select id,Name,Status__c,Description__c,Completed_Date__c,ETC_Notes__c,Stipulation_Data__c,Stipulation_Data__r.Name,Stipulation_Data__r.Upload_Instruction__c,Folder_link__c,Stipulation_Data__r.Installer_Only_Email__c from Stipulations__r where Folder_link__c = null),'+docuFields+','+fieldnames+' from Underwriting_File__c WHERE Opportunity__c=\'' + opportunityId + '\'';*/
                        String queryString = 'select Id,Opportunity__c,Project_Status__c,M0_Approval_Requested_Date__c,M1_Approval_Requested_Date__c,Opportunity__r.owner.name,Opportunity__r.Change_Order_Status__c,Opportunity__r.isChangeOrderSubmitted__c,Opportunity__r.StageName,M2_Approval_Requested_Date__c,Opportunity__r.SyncedQuoteId,Opportunity__r.Product_Loan_Type__c,(select id,Name,Status__c,Description__c,Completed_Date__c,ETC_Notes__c,Stipulation_Data__c,Stipulation_Data__r.Name,Stipulation_Data__r.Upload_Instruction__c,Folder_link__c,Stipulation_Data__r.Installer_Only_Email__c from Stipulations__r where Folder_link__c = null),'+docuFields+','+fieldnames+' from Underwriting_File__c WHERE Opportunity__c=\'' + opportunityId + '\'';
                        List<Underwriting_File__c> uwLst = database.query(queryString);
                        Underwriting_File__c underwritingRec = uwLst.get(0);
                        Box_Fields__c boxFields = underwritingRec.Box_Fields__r[0]; //Added by Adithya as part of 1930 
                        
                        Document_Map_Setting__mdt reqDocFields = new Document_Map_Setting__mdt();
                        if(underwritingRec.Opportunity__r.Product_Loan_Type__c != null){
                            reqDocFields =docNameMap.get(underwritingRec.Opportunity__r.Product_Loan_Type__c);
                        }
                        
                        Map<String,String> milestoneFieldMap = new Map<String,String>();
                        milestoneFieldMap.put('M0','M0_Field_Name_s__c');
                        milestoneFieldMap.put('M1','M1_Field_Name_s__c');
                        milestoneFieldMap.put('M2','M2_Field_Name_s__c');
                        //Added by Deepika as part of 1930 - 5/2/2019 - Start
                        milestoneFieldMap.put('Permit','Permit_Field_Name_s__c');
                        milestoneFieldMap.put('Kitting','Kitting_Field_Name_s__c');
                        milestoneFieldMap.put('Inspection','Inspection_Field_Name_s__c');
                        //Added by Deepika as part of 1930 - 5/2/2019 - End
                        
                        for(String str : milestoneFieldMap.keyset()){
                            Set<String> missingList = new Set<String>();
                            if(milestoneFieldMap.containsKey(str) && reqDocFields.get(milestoneFieldMap.get(str)) != null){//Added by Deepika to avoid exceptions
                                System.debug('### docuMap'+reqDocFields.get(milestoneFieldMap.get(str)));
                                String docType= (String)reqDocFields.get(milestoneFieldMap.get(str));
                                if(docType != null){//Added by Deepika to handle exception for product type HII and HIS
                                    for(String st: docType.split(';')){
                                        String fieldName=(String)underwritingRec.getSobject('Opportunity__r').getSobject('Installer_Account__r').get(st);
                                        System.debug('### underwritingRec.get(docs)'+fieldName);
                                        if(String.isNotBlank(fieldName)){
                                            
                                            for(String doc: fieldName.split(';'))
                                            {
                                                
                                                for (SF_Category_Map__c categs : SF_Category_Map__c.getAll().values())
                                                {
                                                    if(categs.Folder_Name__c == doc){
                                                        System.debug('### boxFields.get(categs.TS_Field_On_Underwriting__c) '+boxFields.get(categs.TS_Field_On_Underwriting__c));
                                                        if(boxFields.get(categs.TS_Field_On_Underwriting__c)!= null)
                                                        {
                                                            break;
                                                        } 
                                                        else
                                                        {
                                                            missingList.add(doc);
                                                        }
                                                    }
                                                    
                                                }
                                                if(doc == 'Loan Agreements' && underwritingRec.Opportunity__r.Change_Order_Status__c =='Pending'){
                                                    missingList.add(doc);
                                                }
                                                
                                            }
                                        }
                                        System.debug('### str'+str);
                                        if(str == 'M0'){
                                            if(!missingList.isEmpty()){
                                                respProjectDocWrap.isAllM0RequiredDocs =false;
                                            }else{
                                                respProjectDocWrap.isAllM0RequiredDocs =true;
                                            }
                                            
                                        }else if(str == 'M1'){
                                            if(!missingList.isEmpty()){
                                                respProjectDocWrap.isAllM1RequiredDocs =false;
                                            }else{
                                                respProjectDocWrap.isAllM1RequiredDocs =true;
                                            }
                                            
                                        }else if(str == 'M2'){
                                            if(!missingList.isEmpty()){
                                                respProjectDocWrap.isAllM2RequiredDocs =false;
                                            }else{
                                                respProjectDocWrap.isAllM2RequiredDocs =true;
                                            }
                                            
                                        }
                                        //Added by Deepika as part of 1930 - 5/2/2019 - Start
                                        else if(str == 'Permit'){
                                            if(!missingList.isEmpty()){
                                                respProjectDocWrap.isAllPermitRequiredDocs =false;
                                            }else{
                                                respProjectDocWrap.isAllPermitRequiredDocs =true;
                                            }
                                            
                                        }else if(str == 'Kitting'){
                                            if(!missingList.isEmpty()){
                                                respProjectDocWrap.isAllKittingRequiredDocs =false;
                                            }else{
                                                respProjectDocWrap.isAllKittingRequiredDocs =true;
                                            }
                                            
                                        }else if(str == 'Inspection'){
                                            if(!missingList.isEmpty()){
                                                respProjectDocWrap.isAllInspectionRequiredDocs =false;
                                            }else{
                                                respProjectDocWrap.isAllInspectionRequiredDocs =true;
                                            }                                           
                                        }
                                        //Added by Deepika as part of 1930 - 5/2/2019 - End
                                    }
                                }
                            }
                        }
                        
                    }
                    String folderId;
                    
                    BoxPlatformApiConnection connection;
                    
                    try
                    {
                        connection = BoxAuthentication.getConnection ();
                    }
                    catch (Exception e)
                    {
                        respData.returnCode = '400';
                        respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('400',null);
                        System.debug('### Unable to authenticate with Box server: ' + e.getMessage());
                     }
                    
                    String accessToken = (connection == null ? 'testaccesstokenonly' : connection.accessToken);
                    BoxApiConnection apiConnection = new BoxApiConnection (accessToken);
                    
                    // get folder id for Opportunity, from FRUP table
                    try
                    {
                        folderId = [SELECT Id, box__Folder_ID__c,box__Record_ID__c FROM box__FRUP__c WHERE box__Record_ID__c=: opportunityId LIMIT 1].box__Folder_ID__c;
                        system.debug('### folderId '+ folderId);
                    }
                    catch (Exception e)
                    {
                        respData.returnCode = '400';
                        respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('400',null);
                        System.debug('### No file found: ' + e.getMessage());
                     }
                    
                    if(folderId!=null && String.isNotBlank(folderId)){
                        respData.returnCode = '200';
                        respProjectDocWrap.projectId = opportunityId;
                        respProjectDocWrap.externalId = externalId;
                         
                        try
                        {
                            respProjectDocWrap.folders = SLFUtility.getProjectFiles(apiConnection,folderId);
                        }
                        catch (Exception e) {
                            respData.returnCode = '400';
                            respProjectDocWrap.errorMessage = ErrorLogUtility.getErrorCodes('400',null);
                            System.debug('### Exception in ProjectDocument service: '+e.getMessage());
                        }
                    }
                    else{
                        respData.returnCode = '200';
                     }
                }
                
                respProjectWrapLst.add(respProjectDocWrap);
            }
            
            res.response = (IRestResponse)respData;

        }catch(Exception err){
           system.debug('### ProjectDocumentServiceImp.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' ProjectDocumentServiceImp.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + ProjectDocumentServiceImp.class);
        return res.response ;
    }
}