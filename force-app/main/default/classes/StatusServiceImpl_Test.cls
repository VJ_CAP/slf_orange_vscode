/******************************************************************************************
* Description: Test class to cover StatusServiceImpl
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri             09/16/2019             Created
******************************************************************************************/
@isTest
public class StatusServiceImpl_Test{    
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
        //  testDataInsert();
        
    }
    private testMethod static void statusAPITest(){
        //get the account record type
        // Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        //  Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE IsPersonAccount = true LIMIT 1]; 
        Opportunity opp = [select Id,Name,Hash_Id__c,(Select id,Name from Underwriting_Files__r) from opportunity where AccountId =: acc.Id LIMIT 1]; 
        Product__c prod = [select Id,Name from Product__c where Name =: 'testprod' LIMIT 1];
        //string OppId = string.ValueOf(opp.Id).subString(0,15);
        
        System.runAs(loggedInUsr[0])
        {
            List<Underwriting_File__c> underWrFilInsertLst = opp.Underwriting_Files__r;
            Stipulation_Data__c sData  = new Stipulation_Data__c();
            sData.Name = 'SD Test';
            sData.Review_Classification__c = 'SLS I';
            insert sData;
            
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.id;
            credit.Primary_Applicant__c = acc.id;
            credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = prod.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'Auto Approved';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            //insert credit;
            
            //Insert System Design Record
            System_Design__c sysDesignRec = new System_Design__c();
            //sysDesignRec.Name = 'Test Sys';
            sysDesignRec.System_Cost__c = 11;
            sysDesignRec.Opportunity__c = opp.Id;
            sysDesignRec.Est_Annual_Production_kWh__c = 10;
            sysDesignRec.Inverter_Count__c = 11;
            //  sysDesignRec.Inverter_Make__c = '11';
            sysDesignRec.Inverter_Model__c = '12';
            //sysDesignRec.Inverter__c = 'TBD';
            sysDesignRec.Module_Count__c = 10;
            sysDesignRec.Module_Model__c = '11';
            //sysDesignRec.System_Size_STC_kW__c = 12;
            insert sysDesignRec;
            System.debug('sysDesignRec***'+sysDesignRec);
            
            Quote quoteRec = new Quote();
            quoteRec.Name ='Test quote';
            quoteRec.Opportunityid = opp.Id;
            quoteRec.SLF_Product__c = prod.Id ;
            quoteRec.System_Design__c= sysDesignRec.id;
            
            insert quoteRec; 
            System.debug('quoteRec***'+quoteRec);
            
            
            List<Stipulation__c> stipInsertLst = new List<Stipulation__c>();
            for(Integer i=0; i<underWrFilInsertLst.size(); i++) 
            {
                Stipulation__c stipTest  = new Stipulation__c();
                stipTest.Status__c = 'New';
                stipTest.Completed_Date__c = system.today();
                stipTest.Stipulation_Data__c  = sData.id;
                stipTest.Underwriting__c =underWrFilInsertLst[i].id;
                stipTest.ETC_Notes__c = 'test';
                stipInsertLst.add(stipTest);
            }
            
            insert stipInsertLst;
            Test.startTest();
            //Used to cover getstatus service API
            Map<String, String> reqParamMap = new Map<String, String>();
            
            //Used to handil requested perameters
            
            reqParamMap.put('projectIds',opp.Id);
            reqParamMap.put('externalIds','');
            
            // Userd to Cover StatusServiceImpl class
            MapWebServiceURI(reqParamMap,'','getstatus');
            
            reqParamMap = new Map<String, String>(); 
            reqParamMap.put('projectIds',opp.Id);
            reqParamMap.put('externalIds','123');
            MapWebServiceURI(reqParamMap,'','getstatus');
            
            reqParamMap = new Map<String, String>(); 
            reqParamMap.put('projectIds','');
            reqParamMap.put('externalIds','123');
            MapWebServiceURI(reqParamMap,'','getstatus');
            
            reqParamMap = new Map<String, String>(); 
            reqParamMap.put('projectIds','');
            reqParamMap.put('externalIds','');
            MapWebServiceURI(reqParamMap,'','getstatus');
            
            reqParamMap = new Map<String, String>(); 
            string statusJsonreq = '{"projectIds": "'+opp.Id+',006V0000007pkf4", "externalIds":""}';            
            MapWebServiceURI(reqParamMap,statusJsonreq,'getstatus');
            
            reqParamMap = new Map<String, String>(); 
            statusJsonreq = '{"projectIds": "006V0000007pkf4", "externalIds":""}';            
            MapWebServiceURI(reqParamMap,statusJsonreq,'getstatus');
            
            reqParamMap = new Map<String, String>(); 
            statusJsonreq = '{"projectIds": "", "externalIds":"2222"}';            
            MapWebServiceURI(reqParamMap,statusJsonreq,'getstatus');
            
            reqParamMap = new Map<String, String>(); 
            statusJsonreq = '{"projectIds": "", "hashIds":"2222"}';            
            MapWebServiceURI(reqParamMap,statusJsonreq,'getstatus');
            
            reqParamMap = new Map<String, String>(); 
            statusJsonreq = '{"hashIds": "'+opp.Hash_Id__c+'", "externalIds":""}';            
            MapWebServiceURI(reqParamMap,statusJsonreq,'getstatus');
            
            StatusServiceImpl statusservImpl = new StatusServiceImpl();
            statusservImpl.fetchData(null);
            
            RequestActionParam reqPrm = new RequestActionParam();
            reqPrm.serviceName = 'getstatus';
            reqPrm.action = 'POST';
            reqPrm.operation = 'Insert';
            reqPrm.sObjId = opp.Id;
            
            //To Cover Try Catch Block
            RestFactory.invokeService(null);
            //RestFactory.executeService(null,null);
            
            Test.stopTest();
        }
        
        
    } 
    
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}