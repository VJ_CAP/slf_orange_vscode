/* Author : Rajesh
* Date   : 12/27/2018
* Description: This class is used to Update task,Stipulation data invoked from Skuid page
*
*/
/*-------------------------------------------------------------------------------
*  Initial Version Written By           Date Modified By      Description
*  Rajesh Raparthi                       12/27/2018           Initial version
*/ 
global class UpdateM1StipDetailsCTRL {
    global class Input {
        @InvocableVariable(required=true) global Id strUnderWrittingId;
        global Input() {
            //  nothing to do
        }
        global Input(Id strUnderWrittingId) {
            this.strUnderWrittingId = (String.valueOf(strUnderWrittingId)).substring(0,15);
        }
    }
    global class Output {   
        @InvocableVariable global String Message;
    }
    public class TaskException extends Exception{}
    
    @InvocableMethod(label='updateM1StipforUnderWritting')
    public static List<Output> UpdateM1StipDetails(List<Input> lstInput)
    {
        List<Output> lstOutPuts = new List<Output>();  
        Output objOutput = new Output();    
        Set<Id> UnderWrittingIds = new Set<Id>();
        List<Task> taskList = new List<Task>();
        List<Stipulation__c> updatedStipList = new List<Stipulation__c>();
        
        for(input objI:lstInput){
            UnderWrittingIds.add(objI.strUnderWrittingId);
        }
        
        Underwriting_File__c objUWFile = [select Id,M1_Ready_for_Review_Date_Time__c,M1_Ready_for_Review_Age__c,Account_Name__c,M1_Approval_Requested_Date__c,Approved_for_Payments__c,
                                          (select Id,OwnerId,Owner.Name,Review_Start_Date_Time__c from Tasks where (status = 'Open' and Type = 'Post-NTP Issue' and Review_Start_Date_Time__c = null)),(Select id,Name,Underwriting__c,Review_Start_Date_Time__c,OwnerId from Stipulations__r where ((Status__c = 'Documents Received' and Review_Start_Date_Time__c = null)) and (Underwriting__r.M1_Approval_Requested_Date__c != null or (Underwriting__r.Opportunity__r.Project_Category__c = 'Home' and Underwriting__r.Approved_for_Payments__c = true)))
                                          from Underwriting_File__c where Id in:UnderWrittingIds limit 1];
        
        if(objUWFile != null)
        {
            if(objUWFile.Tasks.isEmpty() && objUWFile.Stipulations__r.isEmpty()){
                objOutput.Message = objUWFile.Account_Name__c+' is Under review. Please select the next record in the queue.';
                lstOutPuts.add(objOutput);
                //return lstOutPuts;
                throw new TaskException(objUWFile.Account_Name__c+' is Under review. Please select the next record in the queue.');
            }
            if(!objUWFile.Tasks.isEmpty() && (objUWFile.M1_Approval_Requested_Date__c != null || objUWFile.Approved_for_Payments__c == true)){
                
                for(Task objTask :objUWFile.Tasks){
                    objTask.OwnerId = userinfo.getUserId(); 
                    objTask.Review_Start_Date_Time__c = system.now();
                    objTask.Status = 'In Progress';
                    taskList.add(objTask);
                }
            }
            
            
            if(!objUWFile.Stipulations__r.isEmpty()){
                
                for(Stipulation__c stip :objUWFile.Stipulations__r){
                    stip.Review_Start_Date_Time__c = system.now();
                    stip.OwnerId = userinfo.getUserId();
                    updatedStipList.add(stip);
                }
            } 
            try{
                if(!updatedStipList.isEmpty())
                    update updatedStipList;
                
                if(!taskList.isEmpty())
                    update taskList;
                lstOutPuts.add(objOutput);
                return lstOutPuts;
            }Catch(Exception ex){
                objOutput.Message = ex.getMessage();
                lstOutPuts.add(objOutput);
                return lstOutPuts;
            }
        }
        return null;   
    }
}