/**
* Description: Credit related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/

public class NotesDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+NotesDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### Request: '+reqData);
        
        UnifiedBean notesben = new UnifiedBean(); 
        notesben.projects = new List<UnifiedBean.OpportunityWrapper>();
        notesben.error = new list<UnifiedBean.errorWrapper>();
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
      
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        Savepoint sp = Database.setSavepoint();
        try
        {   
            String opportunityId = '';
            String externalId;
            if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
            {                 
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                notesben.returnCode = '201';
            } 
            else if(
                (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
            {
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                notesben.returnCode = '201';
            }else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
            {
                List<Opportunity> opprLst = [SELECT Id FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                
                if (opprLst==null || opprLst.size() == 0)
                {
                    notesben.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
                else{
                    opportunityId = opprLst.get(0).Id;
                }
            }
            else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
            {
                List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                        System.debug('***externalId'+externalId);
                    }else{
                        notesben.returnCode = '400';
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                    }
                    
                }
                if(String.isBlank(errMsg)){
                    List<opportunity> opprLst = [select id,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        notesben.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                    else{
                        opportunityId = opprLst.get(0).id;
                    }
                }
            }else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
            {
                List<opportunity> opprLst = [select id,Hash_Id__c from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                if (opprLst!=null && opprLst.size() > 0)
                {
                    opportunityId = opprLst.get(0).id;
                }else {
                    notesben.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            } 
            
            if(String.isBlank(errMsg)  && String.isNotBlank(opportunityId))
            {
                List<UnifiedBean.NotesWrapper> notesLst =  reqData.projects[0].notes;
                 List<Note> noteList = new List<Note>();
                if(null != reqData.projects[0].notes)
                {
                   
                    for(UnifiedBean.NotesWrapper noteRec : notesLst)
                    {
                        if(String.isBlank(noteRec.title)){
                            notesben.returnCode = '350';
                            errMsg = ErrorLogUtility.getErrorCodes('350',null);
                        }
                        if(string.isBlank(errMsg)){
                            Note n = new Note();
                            if(string.isNotBlank(noteRec.id))
                            {
                                n.id = noteRec.id;
                            }
                            if(string.isBlank(noteRec.id))
                                n.parentId = opportunityId;
                            if(string.isNotBlank(noteRec.body))
                                n.body = noteRec.body;
                            if(string.isNotBlank(noteRec.title)){
                                // added as part of title length validation
                                if(noteRec.title.length()<=80){
                                    n.title = noteRec.title;   
                                }else{
                                    // added as part of title length validation
                                    notesben.returnCode = '351';
                                    errMsg = ErrorLogUtility.getErrorCodes('351',null);                               
                                }  
                            }
                                
                            noteList.add(n);
                            
                        }
                    }
                    
                    
                }
                if(string.isBlank(errMsg))
                {
                    if(!noteList.isEmpty()){
                        upsert noteList;
                    }
                    
                    notesben = getUnifiedBean(new set<Id>{opportunityId});
                    notesben.returnCode = '200';
                }
                
            }
          
            if(string.isNotBlank(errMsg))
            {
                Database.rollback(sp);
                iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                errMsg = errorMsg.errorMessage;
                notesben.error.add(errorMsg);
            }
            iRestRes = (IRestResponse)notesben;
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            //notesben.error  = errorMsg;
            notesben.error.add(errorMsg);
            notesben.returnCode = '400';
            iRestRes = (IRestResponse)notesben;
            
            
            system.debug('### NotesDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('NotesDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Response: '+iRestRes);
        system.debug('### Exit from transformOutput() of '+NotesDataServiceImpl.class);
        return iRestRes;
    }
    
    
    public static UnifiedBean getUnifiedBean(set<Id> OppId)
    {
        UnifiedBean unifben = new UnifiedBean(); 
        unifben.projects = new List<UnifiedBean.OpportunityWrapper>();
        List<Opportunity> oppLst =  [Select id,Hash_Id__c,Partner_Foreign_Key__c,(SELECT Id, ParentId, Title, IsPrivate, Body, CreatedBy.Name, LastModifiedDate FROM Notes order by createddate desc)from Opportunity where id IN: OppId];
        
        unifben.returnCode = '200';
        
        if(!oppLst.isEmpty())
        {
            for(Opportunity oppIterate :oppLst)
            {
                
                UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
                projectWrap.notes = new List<UnifiedBean.NotesWrapper>();
                projectWrap.id = oppIterate.id;
                
                if(!String.isBlank(oppIterate.Partner_Foreign_Key__c))
                projectWrap.externalId = oppIterate.Partner_Foreign_Key__c.subString(oppIterate.Partner_Foreign_Key__c.lastIndexOf('|')+1, oppIterate.Partner_Foreign_Key__c.length());
               // projectWrap.externalId = oppIterate.Partner_Foreign_Key__c;
                
                if(!oppIterate.Notes.isEmpty())    
                {
                    for(Note noteRec :oppIterate.Notes){
                        system.debug('### into Credit section of Unified Bean');
                        UnifiedBean.NotesWrapper notesWrpr = new UnifiedBean.NotesWrapper();                        
                        notesWrpr.id = noteRec.id;
                        notesWrpr.body = noteRec.body;
                        notesWrpr.title = noteRec.title;
                        notesWrpr.createdBy = noteRec.CreatedBy.Name;
                        notesWrpr.lastModifiedDate = string.valueOf(noteRec.LastModifiedDate);
                
                        projectWrap.notes.add(notesWrpr);
                    }
                }
                unifben.projects.add(projectWrap);
            }
        }
        system.debug('****unifben*******'+unifben);
        return unifben; 
    }
}