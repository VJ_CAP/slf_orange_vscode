/**
* Description: This batch runs on expired product and the opportunities related to expired product will be desynced 
* Schedule Details - This will be runs every day once 
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Veereandranath Jalla    21/12/2018          Created 
******************************************************************************************/
global class DesyncQuotesBatch implements Database.Batchable<sObject>,Schedulable,Database.Stateful {
    global String strHTMLBody = 'The following quotes have been processed for Desync/Swapped <br/> <br/>'+
        '<table cellspacing="0" cellpadding="4" border="1"> <tr> <th> Quote ID </th> <th> Product ID (Expired) </th> <th> Product ID (Swapped) </th> </tr>';
    global boolean dataCheck = false;
    global Database.QueryLocator start(Database.BatchableContext BC){ 
       
        return Database.getQueryLocator('SELECT Id,Name,ACH_APR__c,NonACH_APR__c,isACH__c,SLF_Product__r.Product_Tier__c,SLF_Product__r.Product_Loan_Type__c,SLF_Product__r.ACH__c,SLF_Product__r.APR__c,SLF_Product__r.Term_mo__c,SLF_Product__r.State_Code__c,SLF_Product__r.Installer_Account__c,Installer_Account__r.Credit_Waterfall__c,Installer_Account__r.Risk_Based_Pricing__c,(SELECT Id,IsSyncing,SLF_Product__c FROM Quotes WHERE IsSyncing = TRUE limit 1),(select Id,Status__c from Credits__r where Status__c = \'Auto Approved\') FROM Opportunity WHERE SLF_Product__r.Expiration_Date__c = Yesterday ');
    }
    global void execute(Database.BatchableContext BC, List<Opportunity> lstOpps){
        Set<Decimal> setAPRs = new Set<Decimal>();
        Set<Decimal> setNonAPRs = new Set<Decimal>();
        Set<Decimal> setTerms = new Set<Decimal>();
        Set<String>  setStates = new Set<String>();
        Set<Id> setInstAccIds = new Set<Id>();
        Set<String> setProductTypes = new Set<String>();
        Set<boolean> setAchs = new Set<boolean>();
        Set<String> setPTier = new Set<String>();
        List<Quote>  lstQuotes = new List<Quote>();
        List<Opportunity> lstOppsToDesync = new List<Opportunity>();
        Map<String,Product__c> mapACHProduct = new Map<String,Product__c>();
        Map<String,Product__c> mapNonACHProduct = new Map<String,Product__c>();
        Map<String,Product__c> mapACHRiskBaseProduct = new Map<String,Product__c>();
        Map<String,Product__c> mapNonACHRiskBaseProduct = new Map<String,Product__c>();
        for(Opportunity objOpp:lstOpps){
            if(objOpp.Credits__r.IsEmpty()){
                if(objOpp.Installer_Account__r.Credit_Waterfall__c == true || objOpp.Installer_Account__r.Risk_Based_Pricing__c == true){
                    if(objOpp.ACH_APR__c!= null){
                        setAPRs.add(objOpp.ACH_APR__c);
                    }
                    if(objOpp.NonACH_APR__c != null){
                        setNonAPRs.add(objOpp.NonACH_APR__c);
                    }
                    if(objOpp.SLF_Product__r.Term_mo__c != null){
                        setTerms.add(objOpp.SLF_Product__r.Term_mo__c);
                    }
                    if(objOpp.SLF_Product__r.State_Code__c != null){
                        setStates.add(objOpp.SLF_Product__r.State_Code__c);
                    }
                    if(objOpp.SLF_Product__r.Installer_Account__c != null){
                        setInstAccIds.add(objOpp.SLF_Product__r.Installer_Account__c); 
                    }
                    if(objOpp.SLF_Product__r.Product_Loan_Type__c != null){
                        setProductTypes.add(objOpp.SLF_Product__r.Product_Loan_Type__c);
                    }
                    
                }
                if(objOpp.Installer_Account__r.Risk_Based_Pricing__c == true && objOpp.SLF_Product__r.Product_Tier__c != null){
                    setPTier.add(objOpp.SLF_Product__r.Product_Tier__c);
                }
            }
        }
        // querying for matching product when CWF true and RBP is false for the Opportunity installer account
        if(!setInstAccIds.isEmpty() && !setProductTypes.isEmpty() && !setAPRs.isEmpty() && !setTerms.isEmpty() && !setStates.isEmpty()   ){
            string strQuery = 'select Id,APR__c,Term_mo__c,State_Code__c,Expiration_Date__c,Installer_Account__c,Product_Loan_Type__c,ACH__c,Product_Tier__c,NonACH_APR__c FROM Product__c WHERE Installer_Account__c in:setInstAccIds and Product_Loan_Type__c in:setProductTypes  AND Term_mo__c in:setTerms AND State_Code__c in:setStates AND Is_Active__c = true AND (NonACH_APR__c in:setNonAPRs OR  APR__c in:setAPRs) and Expiration_Date__c = null ';
            
            for(Product__c objP:database.query(strQuery)) {
                mapACHProduct.put(objP.Installer_Account__c+'_'+objP.Product_Loan_Type__c+'_'+objP.APR__c+'_'+objP.Term_mo__c+'_'+objP.State_Code__c,objP);
                mapNonACHProduct.put(objP.Installer_Account__c+'_'+objP.Product_Loan_Type__c+'_'+objP.NonACH_APR__c+'_'+objP.Term_mo__c+'_'+objP.State_Code__c,objP);
                mapACHRiskBaseProduct.put(objP.Installer_Account__c+'_'+objP.Product_Loan_Type__c+'_'+objP.APR__c+'_'+objP.Term_mo__c+'_'+objP.State_Code__c+'_'+objP.Product_Tier__c,objP);
                mapNonACHRiskBaseProduct.put(objP.Installer_Account__c+'_'+objP.Product_Loan_Type__c+'_'+objP.NonACH_APR__c+'_'+objP.Term_mo__c+'_'+objP.State_Code__c+'_'+objP.Product_Tier__c,objP);
            }
        }
        system.debug('==========='+mapNonACHRiskBaseProduct+'=========='+mapACHRiskBaseProduct);
        List<Opportunity> lstOppsUpdate = new List<Opportunity>();
        for(Opportunity objOpp:lstOpps){
            if(objOpp.Credits__r.IsEmpty()){
                if(objOpp.Installer_Account__r.Risk_Based_Pricing__c == true ){
                    for(Quote objQ:objOpp.Quotes){
                        dataCheck = true;
                        if(objOpp.isACH__c){
                            if(mapACHRiskBaseProduct != null && mapACHRiskBaseProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.ACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c+'_'+objOpp.SLF_Product__r.Product_Tier__c) != null){
                                String swapProdId = mapACHRiskBaseProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.ACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c+'_'+objOpp.SLF_Product__r.Product_Tier__c).Id;
                                strHTMLBody += '<tr> <td>'+objQ.Id+'</td> <td>'+ objQ.SLF_Product__c +' </td> <td>'+ swapProdId+' </td> </tr>';
                                objQ.SLF_Product__c = swapProdId; 
                                lstQuotes.add(objQ);
                                objOpp.SLF_Product__c = swapProdId;
                                lstOppsUpdate.add(objOpp);
                            }
                        }else if(mapNonACHRiskBaseProduct != null && mapNonACHRiskBaseProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.NonACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c+'_'+objOpp.SLF_Product__r.Product_Tier__c) != null){
                            String swapProdId = mapNonACHRiskBaseProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.NonACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c+'_'+objOpp.SLF_Product__r.Product_Tier__c).Id;
                            strHTMLBody += '<tr> <td>'+objQ.Id+'</td> <td>'+ objQ.SLF_Product__c +' </td> <td>'+ swapProdId+' </td> </tr>';
                            objQ.SLF_Product__c = swapProdId; 
                            lstQuotes.add(objQ);
                            objOpp.SLF_Product__c = swapProdId;
                            lstOppsUpdate.add(objOpp);
                        }else{
                            strHTMLBody += '<tr> <td>'+objQ.Id+'</td> <td>'+ objQ.SLF_Product__c +' </td> <td>'+  +' </td> </tr>';
                            lstOppsToDesync.add(objOpp); 
                        }
                    }
                }else if(objOpp.Installer_Account__r.Credit_Waterfall__c == true){
                    for(Quote objQ:objOpp.Quotes){
                        dataCheck = true;
                        if(objOpp.isACH__c){
                            if(mapACHProduct != null && mapACHProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.ACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c) != null){
                                String swapProdId = mapACHProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.ACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c).Id;
                                strHTMLBody += '<tr> <td>'+objQ.Id+'</td> <td>'+ objQ.SLF_Product__c +' </td> <td>'+ swapProdId+' </td> </tr>';
                                objQ.SLF_Product__c = swapProdId;
                                lstQuotes.add(objQ);
                                objOpp.SLF_Product__c = swapProdId;
                                lstOppsUpdate.add(objOpp);
                            }
                        }else if(mapNonACHProduct != null && mapNonACHProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.NonACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c) != null){
                                String swapProdId = mapNonACHProduct.get(objOpp.SLF_Product__r.Installer_Account__c+'_'+objOpp.SLF_Product__r.Product_Loan_Type__c+'_'+objOpp.NonACH_APR__c+'_'+objOpp.SLF_Product__r.Term_mo__c+'_'+objOpp.SLF_Product__r.State_Code__c).Id;
                                strHTMLBody += '<tr> <td>'+objQ.Id+'</td> <td>'+ objQ.SLF_Product__c +' </td> <td>'+ swapProdId+' </td> </tr>';
                                objQ.SLF_Product__c = swapProdId;
                                lstQuotes.add(objQ);
                                objOpp.SLF_Product__c = swapProdId;
                                lstOppsUpdate.add(objOpp);
                        }
                        else{
                            strHTMLBody += '<tr> <td>'+objQ.Id+'</td> <td>'+ objQ.SLF_Product__c +' </td> <td>'+  +' </td> </tr>';
                            lstOppsToDesync.add(objOpp); 
                        }
                    } 
                }else{
                    for(Quote objQ:objOpp.Quotes){
                        dataCheck = true;
                         strHTMLBody += '<tr> <td>'+objQ.Id+'</td> <td>'+ objQ.SLF_Product__c +' </td> <td>'+  +' </td> </tr>';
                         lstOppsToDesync.add(objOpp); 
                    }
                }
            }
            system.debug('====='+strHTMLBody);
        }
        if(!lstOppsUpdate.isEmpty()){
            database.update(lstOppsUpdate,false);
        }
        if(!lstQuotes.isEmpty()){
            database.update(lstQuotes,false);
        }
        if(!lstOppsToDesync.isEmpty()){
            list<Opportunity> lstOppsToUpdate = SyncDesyncUtilities.updateDesyncFields(lstOppsToDesync,true);
            database.update(lstOppsToUpdate,false);
        }
        
        system.debug('strHTMLBody==========='+strHTMLBody);
        
    }
    global void execute (SchedulableContext SC){
        DesyncQuotesBatch batch = new DesyncQuotesBatch();
        database.executeBatch(batch,1);
    }
    global void finish(Database.BatchableContext BC){
        system.debug('strhtmlbody======='+strHTMLBody);
        if(dataCheck == false ){
            strhtmlbody += '<tr> <td colspan="3" style="text-align:center;"> No Data Found  </td> </tr> </table>';
        }else{
              strhtmlbody += '</table>';      
        }
        List<String> setEmails = new List<String>();
        List<Messaging.SingleEmailMessage> lstEmails = new list<Messaging.SingleEmailMessage>();
        List<OrgWideEmailAddress> senderEmail = [select id,DisplayName, Address from OrgWideEmailAddress where DisplayName ='Sunlight Financial Support' LIMIT 1];
        List<Admin_Emails__c> lstAEmails = [SELECT Id,Admin_Email__c,Functionality__c FROM Admin_Emails__c WHERE Functionality__c = 'Product Expiration Batch'];
        For(Admin_Emails__c objAE:lstAEmails){
            setEmails.add(objAE.Admin_Email__c);
        } 
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(setEmails);
        if(!senderEmail.isEmpty()){
            message.setOrgWideEmailAddressId(senderEmail.get(0).id);
        }
        message.setSubject('Product Expiration Batch'); 
        message.setHtmlBody(strHtmlBody);
        lstEmails.add(message); 
        system.debug('======'+lstEmails);
        if(!lstEmails.isEmpty()){
            messaging.sendEmail(lstEmails);
        }
    }
}