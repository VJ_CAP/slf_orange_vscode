/**
* Description: Amortization Schedule related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Brahmeswar           04/03/2019               Created
******************************************************************************************/

public without sharing class LoandocsAmortizationDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    Map<integer,AmortizationWrapper> mapRowNoAmortizatoin = new map<integer,AmortizationWrapper>();
    decimal loanAmountval;
    Map<integer,AmortizationRowInfo> RowNumberAndRowinfo = new Map<integer,AmortizationRowInfo>();
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+LoandocsAmortizationDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        
        UnifiedBean unifiedBean = new UnifiedBean(); 
        unifiedBean.error = new list<UnifiedBean.errorWrapper>();
        unifiedBean.projects  = new List<UnifiedBean.OpportunityWrapper>();
        unifiedBean.disclosures = new List<UnifiedBean.DisclosureWrapper>();
        //unifiedBean.error = new list<UnifiedBean.errorWrapper>();
        UnifiedBean respData = new UnifiedBean();
        respData.error = new list<UnifiedBean.errorWrapper>();
        String Quoteid  = '';
        double monthlyPayment = 0.00; // added for 11152
        double escalatedMonthlyPayment = 0.00;  // added for 11152
        double finalMonthlyPayment = 0.00;  // added for 11152
        double finalEscalatedMonthlyPayment = 0.00; // added for 11152
        
        loanAmountval = reqData.projects[0].quotes[0].loanAmount;
        String amortizationScheduleTable = reqData.projects[0].quotes[0].amortizationPrepaymentTable;
        String amortizationPrepaymentScheduleTable =reqData.projects[0].quotes[0].amortizationTable;
        //Start - Changed added by Udaya Kiran Orange-2040
        String amortizationScheduleTablenonACH = reqData.projects[0].quotes[0].amortizationPrepaymentTablenonACH;
        String amortizationPrepaymentScheduleTablenonACH =reqData.projects[0].quotes[0].amortizationTablenonACH;
        //End - Changed added by Udaya Kiran Orange-2040
        System.debug('###loanAmountval :'+loanAmountval);
        System.debug('*****amortizationScheduleTable****'+amortizationScheduleTable);
        System.debug('*****amortizationPrepaymentScheduleTable****'+amortizationPrepaymentScheduleTable);
        System.debug('*****amortizationScheduleTablenonACH****'+amortizationScheduleTablenonACH);
        System.debug('*****amortizationPrepaymentScheduleTablenonACH****'+amortizationPrepaymentScheduleTablenonACH);
                                                
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        Savepoint sp = Database.setSavepoint();
        
        Quoteid = reqData.projects[0].quotes[0].id;
        list<quote> quotesList = [select monthly_payment__c,FinalMonthlyPaymentwithoutPrepay__c,opportunity.Monthly_Payment__c,opportunity.Finance_Charge__c,opportunity.Product_Loan_Type__c,opportunity.Monthly_Payment_Escalated_Single_Loan__c,Monthly_Payment_without_Prepay__c,opportunity.Final_Monthly_Payment__c,Final_Monthly_Payment__c, opportunity.Long_Term_Term__c,opportunity.Total_Payments__c,opportunity.Combined_Loan_Amount__c from quote where id =: Quoteid ];        
        monthlyPayment = reqData.projects[0].quotes[0].monthlyPayment;
        escalatedMonthlyPayment = reqData.projects[0].quotes[0].escalatedMonthlyPayment;
        finalMonthlyPayment = reqData.projects[0].quotes[0].finalMonthlyPayment;
        finalEscalatedMonthlyPayment = reqData.projects[0].quotes[0].finalEscalatedMonthlyPayment;
        if(quotesList[0].opportunity.Product_Loan_Type__c == 'Solar'||quotesList[0].opportunity.Product_Loan_Type__c == 'Battery Only'||quotesList[0].opportunity.Product_Loan_Type__c == 'Integrated Solar Shingles'||quotesList[0].opportunity.Product_Loan_Type__c == 'Non-PV Stand Alone Battery'||quotesList[0].opportunity.Product_Loan_Type__c == 'SolarPlus Roof')
            {
                system.debug('Product_Loan_Type__c'+quotesList[0].opportunity.Product_Loan_Type__c);
                if(quotesList[0].opportunity.Finance_Charge__c < 0){ 
                    errMsg = ErrorLogUtility.getErrorCodes('608', null);
                    unifiedBean.returnCode = '608';
                }                 
                else if (monthlyPayment != quotesList[0].opportunity.Monthly_Payment__c || escalatedMonthlyPayment != quotesList[0].opportunity.Monthly_Payment_Escalated_Single_Loan__c|| finalMonthlyPayment != quotesList[0].Final_Monthly_Payment__c){
                    system.debug('comparision'+monthlyPayment+''+quotesList[0].opportunity.Monthly_Payment__c+'Escalatedmonthlypayment'+escalatedMonthlyPayment+'--'+quotesList[0].opportunity.Monthly_Payment_Escalated_Single_Loan__c+'finalMonthlyPayment'+finalMonthlyPayment+'--'+quotesList[0].Final_Monthly_Payment__c);
                    errMsg = ErrorLogUtility.getErrorCodes('608', null);
                    unifiedBean.returnCode = '608';
                }    
                if(quotesList[0].opportunity.Product_Loan_Type__c == 'Solar'||quotesList[0].opportunity.Product_Loan_Type__c == 'Battery Only'||quotesList[0].opportunity.Product_Loan_Type__c == 'Integrated Solar Shingles'||quotesList[0].opportunity.Product_Loan_Type__c == 'Non-PV Stand Alone Battery') 
                {
                    system.debug('### calculation check: Monthly payement--'+monthlyPayment+'Opp.longterm:'+quotesList[0].opportunity.Long_Term_Term__c+'escalatedmonthlypayment:'+escalatedMonthlyPayment+'finalescalatedmonthlypayment:'+finalEscalatedMonthlyPayment+'totalpayments:'+quotesList[0].opportunity.Total_Payments__c);
                    if(((17*monthlyPayment)+ ((quotesList[0].opportunity.Long_Term_Term__c - 19)*escalatedMonthlyPayment)+finalEscalatedMonthlyPayment).SetScale(2) !=quotesList[0].opportunity.Total_Payments__c) 
                    {      // Added by Arun J as a part of Orange 11152
                        system.debug('calculation check: Monthly payement--'+monthlyPayment+'Opp.longterm:'+quotesList[0].opportunity.Long_Term_Term__c+'escalatedmonthlypayment:'+escalatedMonthlyPayment+'finalescalatedmonthlypayment:'+finalEscalatedMonthlyPayment+'totalpayments:'+quotesList[0].opportunity.Total_Payments__c);
                        errMsg = ErrorLogUtility.getErrorCodes('608', null);
                        unifiedBean.returnCode = '608';
                    }   
                    system.debug('### Cond2 check: Monthly payement--'+monthlyPayment+'Opp.longterm:'+quotesList[0].opportunity.Long_Term_Term__c+'escalatedmonthlypayment:'+escalatedMonthlyPayment+'finalescalatedmonthlypayment:'+finalEscalatedMonthlyPayment+'Combined_Loan_Amount__c'+quotesList[0].opportunity.Combined_Loan_Amount__c+'financecharge:'+quotesList[0].opportunity.Finance_Charge__c);
                    system.debug('### Calc Financ Charge:' + (((17*monthlyPayment) + ((quotesList[0].opportunity.Long_Term_Term__c - 19) * escalatedMonthlyPayment) + finalEscalatedMonthlyPayment) - quotesList[0].opportunity.Combined_Loan_Amount__c));
                    if ((((17*monthlyPayment) + ((quotesList[0].opportunity.Long_Term_Term__c - 19) * escalatedMonthlyPayment) + finalEscalatedMonthlyPayment) - quotesList[0].opportunity.Combined_Loan_Amount__c).SetScale(2) != quotesList[0].opportunity.Finance_Charge__c) 
                    {        // Added by Arun J as a part of Orange 11152
                        system.debug('Cond2 check: Monthly payement--'+monthlyPayment+'Opp.longterm:'+quotesList[0].opportunity.Long_Term_Term__c+'escalatedmonthlypayment:'+escalatedMonthlyPayment+'finalescalatedmonthlypayment:'+finalEscalatedMonthlyPayment+'Combined_Loan_Amount__c'+quotesList[0].opportunity.Combined_Loan_Amount__c+'financecharge:'+quotesList[0].opportunity.Finance_Charge__c);
                        errMsg = ErrorLogUtility.getErrorCodes('608', null);
                        unifiedBean.returnCode = '608';
                    }  
                }
                else if (quotesList[0].opportunity.Product_Loan_Type__c == 'SolarPlus Roof')
                {   
                    system.debug('prod loan type:'+quotesList[0].opportunity.Product_Loan_Type__c);
                    // Added by Arun J as a part of Orange 11152
                    if ((((quotesList[0].opportunity.Long_Term_Term__c - 2) * escalatedMonthlyPayment)+finalEscalatedMonthlyPayment).SetScale(2) != quotesList[0].opportunity.Total_Payments__c)
                    {      
                        system.debug('cond3out : Longtermterm'+quotesList[0].opportunity.Long_Term_Term__c+'escalatedmonthlypayment'+escalatedMonthlyPayment+'finalescalatedmonthlypayment'+finalEscalatedMonthlyPayment+'Totalpayments'+quotesList[0].opportunity.Total_Payments__c);
                        errMsg = ErrorLogUtility.getErrorCodes('608', null);
                        unifiedBean.returnCode = '608';
                        
                    }
                    else if ((((quotesList[0].opportunity.Long_Term_Term__c - 2) * escalatedMonthlyPayment) + finalEscalatedMonthlyPayment - quotesList[0].opportunity.Combined_Loan_Amount__c ).SetScale(2) != quotesList[0].opportunity.Finance_Charge__c){     // Added by Arun J as a part of Orange 11152
                        system.debug('cond4out :Longtermterm'+quotesList[0].opportunity.Long_Term_Term__c+'escalatedmonthlypayment'+escalatedMonthlyPayment+'finalescalatedmonthlypayment'+finalEscalatedMonthlyPayment+'Combined_Loan_Amount__c'+quotesList[0].opportunity.Combined_Loan_Amount__c+'Financecharge'+quotesList[0].opportunity.Finance_Charge__c);
                        errMsg = ErrorLogUtility.getErrorCodes('608', null);
                        unifiedBean.returnCode = '608';
                    }
                }               
            }  
        if(string.isNotBlank(errMsg)){
            System.debug('entered into error block'+errMsg);
            iolist = new list<DataValidator.InputObject>{};
            unifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedBean.error.add(errorMsg);
            unifiedBean.returnCode = '608';
            iRestRes = (IRestResponse)unifiedBean;
            return iRestRes;
        }
        try
        {   
        
            
            String PrepayPercent = '30'; //// added part of 6921
            
            if(QuoteId != null & QuoteId != ''){ // Added part of 6921 
                List<Quote> lstQ = new List<Quote>();
                lstQ = [select id,Paydown_percent__c   from Quote where Id=:QuoteId];
                if(!lstQ.isEmpty() && lstQ.Size() > 0 && lstQ.get(0).Paydown_percent__c > 0 ){
                    PrepayPercent = String.valueof(lstQ.get(0).Paydown_percent__c);
                } 
            }
            
            list<Amortization_Schedule__c> lstAmortizationRecords = [select id  from Amortization_Schedule__c where Quote__c=:Quoteid];
            
            list<Amortization_Prepayment_Schedule__c> lstAmortizationPrepaymentRecords = [select id from Amortization_Prepayment_Schedule__c where Quote__c=:Quoteid];
            //Start - Changes added by Orange-2040 Udaya Kiran
            list<Amortization_Schedule_NONACH__c> lstAmortizationRecordsnonACH = [select id from Amortization_Schedule_NONACH__c where Quote__c=:Quoteid];
            list<Amortization_Prepayment_Schedule_NONACH__c> lstAmortizationPrepaymentRecordsnonACH = [select id from Amortization_Prepayment_Schedule_NONACH__c where Quote__c=:Quoteid];
            //End - Changes added by Orange-2040 Udaya Kiran 
            
            System.debug('### Total Amortization Schedule old records: '+lstAmortizationRecords.size());
            System.debug('### Total Amortization Prepayment Schedule old records: '+lstAmortizationPrepaymentRecords.size());
            
            List<AmortizationWrapper> lstAmortizationWrapper = parseData(amortizationScheduleTable,'NonePrepayment',PrepayPercent );
            List<AmortizationWrapper> lstAmortizationPrepaymentWrap = parseData(amortizationPrepaymentScheduleTable,'Prepayment',PrepayPercent);
            
            //Start - Changes added by Orange-2040 Udaya Kiran
            List<AmortizationWrapper> lstAmortizationWrappernonACH = new List<AmortizationWrapper>();
            List<AmortizationWrapper> lstAmortizationPrepaymentWrapnonACH = new List<AmortizationWrapper>();
            if(String.isNotBlank(amortizationScheduleTablenonACH)){
            lstAmortizationWrappernonACH = parseData(amortizationScheduleTablenonACH,'NonePrepayment',PrepayPercent); // Sending extra parameter PrepayPercent part of 6921
            }
            if(String.isNotBlank(amortizationPrepaymentScheduleTablenonACH)){
            lstAmortizationPrepaymentWrapnonACH = parseData(amortizationPrepaymentScheduleTablenonACH,'Prepayment',PrepayPercent ); // Sending extra parameter PrepayPercent part of 6921
            }
            //End - Changes added by Orange-2040 Udaya Kiran
            
            System.debug('### lstAmortizationWrapper::'+lstAmortizationWrapper);
            System.debug('### lstAmortizationPrepaymentWrap::'+lstAmortizationPrepaymentWrap);
            
            list<Amortization_Schedule__c> lstAmortization = new list<Amortization_Schedule__c>();
            list<Amortization_Prepayment_Schedule__c> lstAmortizationPrepayment = new list<Amortization_Prepayment_Schedule__c>();
            
            //Start - Orange-2040 Udaya Kiran
            list<Amortization_Schedule_NONACH__c> lstAmortizationnonACH = new list<Amortization_Schedule_NONACH__c>();
            list<Amortization_Prepayment_Schedule_NONACH__c> lstAmortizationPrepaymentnonACH = new list<Amortization_Prepayment_Schedule_NONACH__c>();
            //End - Orange-2040 Udaya Kiran
            
            if(lstAmortizationWrapper!=null && !lstAmortizationWrapper.isEmpty()){
                for(AmortizationWrapper objAmortizationWrapper : lstAmortizationWrapper){
                    Amortization_Schedule__c objAmSch = new Amortization_Schedule__c();
                        
                    objAmSch.Prepayment__c = objAmortizationWrapper.Prepayment;
                    objAmSch.Unpaid_Interest_negate__c = objAmortizationWrapper.UnpaidInterestNegate;
                    objAmSch.Beginning_Balance__c = objAmortizationWrapper.BeginningBalance;
                    objAmSch.Monthly_Payment__c = objAmortizationWrapper.MonthlyPayment;
                    objAmSch.Current_Interest__c = objAmortizationWrapper.CurrentInterest;
                    objAmSch.Unpaid_Interest__c = objAmortizationWrapper.UnpaidInterest;
                    objAmSch.Principle__c = objAmortizationWrapper.Principle;
                    objAmSch.End_Balance__c = objAmortizationWrapper.EndBalance;
                    objAmSch.Row_Number__c = objAmortizationWrapper.RowNumber;
                    objAmSch.Quote__c = Quoteid;
                    
                    lstAmortization.add(objAmSch);
                }
                
                if(lstAmortization!=null && !lstAmortization.isEmpty()){
                    insert lstAmortization;
                    System.debug('### Inserted Amortization Schedule records.');
                }
                
                if(lstAmortizationRecords != null && lstAmortizationRecords.size()>0){
                    delete lstAmortizationRecords;
                    System.debug('### Deleted old Amortization Schedule records.');
                }
            }
            
            if(lstAmortizationPrepaymentWrap!=null && !lstAmortizationPrepaymentWrap.isEmpty()){
                for(AmortizationWrapper objAmortizationWrapper : lstAmortizationPrepaymentWrap){
                    Amortization_Prepayment_Schedule__c objAmSch = new Amortization_Prepayment_Schedule__c();
                        
                    objAmSch.Prepayment__c = objAmortizationWrapper.Prepayment;
                    objAmSch.Unpaid_Interest_negate__c = objAmortizationWrapper.UnpaidInterestNegate;
                    objAmSch.Beginning_Balance__c = objAmortizationWrapper.BeginningBalance;
                    objAmSch.Monthly_Payment__c = objAmortizationWrapper.MonthlyPayment;
                    objAmSch.Current_Interest__c = objAmortizationWrapper.CurrentInterest;
                    objAmSch.Unpaid_Interest__c = objAmortizationWrapper.UnpaidInterest;
                    objAmSch.Principle__c = objAmortizationWrapper.Principle;
                    objAmSch.End_Balance__c = objAmortizationWrapper.EndBalance;
                    objAmSch.Row_Number__c = objAmortizationWrapper.RowNumber;
                    objAmSch.Quote__c = Quoteid;
                    
                    lstAmortizationPrepayment.add(objAmSch);
                }
                
                if(lstAmortizationPrepayment!=null && !lstAmortizationPrepayment.isEmpty()){
                    insert lstAmortizationPrepayment;
                    System.debug('### Inserted Amortization Prepayment Schedule records.');
                }
                
                if(lstAmortizationPrepaymentRecords != null && lstAmortizationPrepaymentRecords.size()>0){
                    delete lstAmortizationPrepaymentRecords;
                    System.debug('### Deleted old Amortization Prepayment Schedule records.');
                }
            }
             //Start - Changes added by Udaya Kiran Orange-2040
            if(lstAmortizationWrappernonACH!=null && !lstAmortizationWrappernonACH.isEmpty()){
                for(AmortizationWrapper objAmortizationWrapper : lstAmortizationWrappernonACH){
                    Amortization_Schedule_NONACH__c objAmSchNACH = new Amortization_Schedule_NONACH__c();
                        
                    objAmSchNACH.Prepayment__c = objAmortizationWrapper.Prepayment;
                    objAmSchNACH.Unpaid_Interest_negate__c = objAmortizationWrapper.UnpaidInterestNegate;
                    objAmSchNACH.Beginning_Balance__c = objAmortizationWrapper.BeginningBalance;
                    objAmSchNACH.Monthly_Payment__c = objAmortizationWrapper.MonthlyPayment;
                    objAmSchNACH.Current_Interest__c = objAmortizationWrapper.CurrentInterest;
                    objAmSchNACH.Unpaid_Interest__c = objAmortizationWrapper.UnpaidInterest;
                    objAmSchNACH.Principle__c = objAmortizationWrapper.Principle;
                    objAmSchNACH.End_Balance__c = objAmortizationWrapper.EndBalance;
                    objAmSchNACH.Row_Number__c = objAmortizationWrapper.RowNumber;
                    objAmSchNACH.Quote__c = Quoteid;
                    
                    lstAmortizationnonACH.add(objAmSchNACH);
                }
                
                if(lstAmortizationnonACH!=null && !lstAmortizationnonACH.isEmpty()){
                    insert lstAmortizationnonACH;
                    System.debug('### Inserted Amortization Schedule records NonACH.');
                }
                
                if(lstAmortizationRecordsnonACH != null && lstAmortizationRecordsnonACH.size()>0){
                    delete lstAmortizationRecordsnonACH;
                    System.debug('### Deleted old Amortization Schedule records NonACH.');
                }
            }
            if(lstAmortizationPrepaymentWrapnonACH!=null && !lstAmortizationPrepaymentWrapnonACH.isEmpty()){
                for(AmortizationWrapper objAmortizationWrapper : lstAmortizationPrepaymentWrapnonACH){
                    Amortization_Prepayment_Schedule_NONACH__c objAmSchnonACH = new Amortization_Prepayment_Schedule_NONACH__c();
                        
                    objAmSchnonACH.Prepayment__c = objAmortizationWrapper.Prepayment;
                    objAmSchnonACH.Unpaid_Interest_negate__c = objAmortizationWrapper.UnpaidInterestNegate;
                    objAmSchnonACH.Beginning_Balance__c = objAmortizationWrapper.BeginningBalance;
                    objAmSchnonACH.Monthly_Payment__c = objAmortizationWrapper.MonthlyPayment;
                    objAmSchnonACH.Current_Interest__c = objAmortizationWrapper.CurrentInterest;
                    objAmSchnonACH.Unpaid_Interest__c = objAmortizationWrapper.UnpaidInterest;
                    objAmSchnonACH.Principle__c = objAmortizationWrapper.Principle;
                    objAmSchnonACH.End_Balance__c = objAmortizationWrapper.EndBalance;
                    objAmSchnonACH.Row_Number__c = objAmortizationWrapper.RowNumber;
                    objAmSchnonACH.Quote__c = Quoteid;
                    
                    lstAmortizationPrepaymentnonACH.add(objAmSchnonACH);
                }
                
                if(lstAmortizationPrepaymentnonACH!=null && !lstAmortizationPrepaymentnonACH.isEmpty()){
                    insert lstAmortizationPrepaymentnonACH;
                    System.debug('### Inserted Amortization Prepayment Schedule records NonACH.');
                } 
                
                if(lstAmortizationPrepaymentRecordsnonACH != null && lstAmortizationPrepaymentRecordsnonACH.size()>0){
                    delete lstAmortizationPrepaymentRecordsnonACH;
                    System.debug('### Deleted old Amortization Prepayment Schedule records NonACH.');
                }
            }
            
            //End - Changes added by Udaya Kiran Orange-2040
            
            unifiedBean.returnCode = '200';
            unifiedBean.message ='Success';
            
            iRestRes = (IRestResponse)unifiedBean;
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400', null);
            
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedBean.error.add(errorMsg);
            unifiedBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedBean;
            
            system.debug('### LoandocsAmortizationDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('LoandocsAmortizationDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+LoandocsAmortizationDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
    
    
    public List<AmortizationWrapper> parseData(String Responseval,String type,String PrePercent){
        
        //List<Amortization_Schedule__c> lstAMSch= new List<Amortization_Schedule__c>();
        List<AmortizationWrapper> lstAMSch= new List<AmortizationWrapper>();
        AmortizationWrapper objAmSch;
        System.debug('### Table Data: '+Responseval);
        if(!String.isBlank(Responseval) && Responseval.contains('**')){
            String modifiedResponsevaltemp = Responseval.replace('*',':');
            //String modifiedResponseval = modifiedResponsevaltemp.replace('","','');Udaya Kiran 2040
            String modifiedResponseval = modifiedResponsevaltemp.replace(', ','');
            System.debug('### modifiedResponseval: '+modifiedResponseval);
            String Responseval1 = modifiedResponseval.replace('[]',';;');
            System.debug('### Responseval1: '+Responseval1);
            
            integer i=1;
            
            for(String strval : Responseval1.split('::::')){
                
                //objAmSch.Quote__c = Quoteidval;
                System.debug('### strval: '+strval);
                if(!strval.contains('------')){
                    if(strval.contains(';;')){
                        strval = strval.replaceFirst(';;', '');
                        System.debug('### strval1: '+strval);
                        List<String> Rowvalues  = strval.split(';;');
                        
                        AmortizationRowInfo rowInfoObj;
                        if(Rowvalues != null && Rowvalues.size()>0){
                            rowInfoObj = new AmortizationRowInfo();
                            rowInfoObj.Column1 = Rowvalues[0];
                            rowInfoObj.Column2 = Rowvalues[1];
                            rowInfoObj.Column3 = Rowvalues[2];
                            rowInfoObj.Column4 = Rowvalues[3];
                            rowInfoObj.Column5 = Rowvalues[4];
                            rowInfoObj.Column6 = Rowvalues[5];
                            rowInfoObj.Column7 = Rowvalues[6];
                            rowInfoObj.Column8 = Rowvalues[7];
                            RowNumberAndRowinfo.put(i,rowInfoObj);
                        }
                    }i++;
                }
            }
            System.debug('### RowNumberAndRowinfo: '+RowNumberAndRowinfo);
        }
        integer j=1;
        if(RowNumberAndRowinfo != null && RowNumberAndRowinfo.size()>0){
            
            if(loanAmountval != null){
                
                objAmSch = new AmortizationWrapper();
                objAmSch.BeginningBalance =loanAmountval;
                objAmSch.Prepayment = '0';
                objAmSch.UnpaidInterestNegate= 0.00;
                objAmSch.RowNumber = 1;
                objAmSch.MonthlyPayment= 0.00;
                objAmSch.CurrentInterest= 0.00;
                objAmSch.UnpaidInterest= 0.00;
                objAmSch.Principle= 0.00;
                objAmSch.EndBalance= 0.00;
                lstAMSch.add(objAmSch);                 
            }
            for(integer rownumberval : RowNumberAndRowinfo.keyset()){
                objAmSch = new AmortizationWrapper();
                AmortizationRowInfo rowobj = RowNumberAndRowinfo.get(rownumberval);
                if(type == 'Prepayment')
                    objAmSch.RowNumber = rownumberval+1;
                else    
                    objAmSch.RowNumber = rownumberval;
                if(type != 'Prepayment'){
                    if(j==1){
                        objAmSch.Prepayment = '0';
                    }else{
                        integer val = j-1;
                        objAmSch.Prepayment = String.valueOf(val); 
                    }
                }else{
                    if(j==1)
                        objAmSch.Prepayment = '0';
                    else if(j==18){
                        objAmSch.Prepayment = PrePercent +'% Prepayment';
                    }else if(j > 18){
                        objAmSch.RowNumber = rownumberval + 2;
                        integer val = j-1;
                        objAmSch.Prepayment = String.valueOf(val);
                    }else{
                        integer val = j-1;
                        objAmSch.Prepayment = String.valueOf(val);
                    }
                }
                if(!String.isBlank(rowobj.Column3)){
                    objAmSch.BeginningBalance = Decimal.valueOf(rowobj.Column3.trim());
                        
                if(!String.isBlank(rowobj.Column5))
                    objAmSch.CurrentInterest = Decimal.valueOf(rowobj.Column5.trim());
                
                if(!String.isBlank(rowobj.Column5) && !String.isBlank(rowobj.Column2)){
                    objAmSch.UnpaidInterestNegate = Decimal.valueOf(rowobj.Column5.trim())-Decimal.valueOf(rowobj.Column2.trim());
                    
                    
                if(type == 'Prepayment' && (j==18 || j==19)){
                    
                    if(j==18){
                        if(!String.isBlank(rowobj.Column1))
                            objAmSch.MonthlyPayment = Decimal.valueOf(rowobj.Column1.trim());
                        if(!String.isBlank(rowobj.Column5) && !String.isBlank(rowobj.Column2))
                            objAmSch.EndBalance = Decimal.valueOf(rowobj.Column5.trim()) - Decimal.valueOf(rowobj.Column2.trim());
                        
                        objAmSch.Principle = objAmSch.MonthlyPayment - objAmSch.EndBalance;
                        
                        objAmSch.UnpaidInterest = 0.00;
                        
                        AmortizationWrapper objAmSchCnl = objAmSch.clone();
                        objAmSchCnl.RowNumber = rownumberval + 2;
                        objAmSchCnl.Prepayment = '17';
                         if(!String.isBlank(rowobj.Column4))
                            objAmSchCnl.MonthlyPayment = Decimal.valueOf(rowobj.Column4.trim());
                        
                        objAmSchCnl.BeginningBalance = objAmSch.BeginningBalance - objAmSch.Principle;
                        objAmSchCnl.CurrentInterest = 0.00;
                        objAmSchCnl.UnpaidInterestNegate = 0.00;
                        
                        if(!String.isBlank(rowobj.Column7))
                            objAmSchCnl.Principle = objAmSchCnl.MonthlyPayment;
                    
                        objAmSchCnl.EndBalance = objAmSchCnl.MonthlyPayment - objAmSchCnl.Principle;
                        objAmSchCnl.UnpaidInterest = 0.00;
                        lstAMSch.add(objAmSchCnl);
                        
                    }
                    if(j==19){
                        if(!String.isBlank(rowobj.Column4))
                            objAmSch.MonthlyPayment = Decimal.valueOf(rowobj.Column4.trim());
                        
                        if(!String.isBlank(rowobj.Column7))
                            objAmSch.Principle = Decimal.valueOf(rowobj.Column7.trim());
                        
                        objAmSch.CurrentInterest = Decimal.valueOf(rowobj.Column5.trim());
                        objAmSch.UnpaidInterestNegate = Decimal.valueOf(rowobj.Column5.trim())-Decimal.valueOf(rowobj.Column2.trim());
                        objAmSch.EndBalance = objAmSch.MonthlyPayment - objAmSch.Principle;
                        objAmSch.UnpaidInterest = Decimal.valueOf(rowobj.Column2.trim());
                    }
                    
                }else{  
                    if(!String.isBlank(rowobj.Column4))
                        objAmSch.MonthlyPayment = Decimal.valueOf(rowobj.Column4.trim());
                    
                    if(!String.isBlank(rowobj.Column7))
                        objAmSch.Principle = Decimal.valueOf(rowobj.Column7.trim());
                    
                        objAmSch.EndBalance = objAmSch.MonthlyPayment - objAmSch.Principle;
                    
                    if(RowNumberAndRowinfo.get(rownumberval+1) != null){
                            AmortizationRowInfo rowobjtemp = RowNumberAndRowinfo.get(rownumberval+1);
                            objAmSch.UnpaidInterest = Decimal.valueOf(rowobjtemp.Column2.trim());
                    }else{
                        if(!String.isBlank(rowobj.Column2))
                            objAmSch.UnpaidInterest = Decimal.valueOf(rowobj.Column2.trim());
                        else    
                            objAmSch.UnpaidInterest = 0.00;
                    }
                }   
                System.debug('### objAmSch: '+objAmSch);
                lstAMSch.add(objAmSch);
                System.debug('### lstAMSch: '+lstAMSch);
                j++;
            }
        }
        
        }
      }
      return lstAMSch;   
    }
    public class AmortizationWrapper{
        public integer RowNumber{get;set;}  
        public String Prepayment{get; set;}
        public Decimal UnpaidInterestNegate{get; set;}
        
        public Decimal BeginningBalance{get; set;}
        public Decimal MonthlyPayment{get; set;}
        
        public Decimal CurrentInterest{get; set;}
        public Decimal UnpaidInterest{get; set;}
        
        public Decimal Principle{get; set;}
        public Decimal EndBalance{get; set;}
        public void AmortizationWrapper(){
            
            UnpaidInterestNegate= 0.00;
            BeginningBalance= 0.00;
            MonthlyPayment= 0.00;
            CurrentInterest= 0.00;
            UnpaidInterest= 0.00;
            Principle= 0.00;
            EndBalance= 0.00;
            
        }
    }
    
    public class AmortizationRowInfo{
        
        public String Column1 {get;set;}
        public String Column2 {get;set;}
        public String Column3 {get;set;}
        public String Column4 {get;set;}
        public String Column5 {get;set;}
        public String Column6 {get;set;}
        public String Column7 {get;set;}
        public String Column8 {get;set;}
            
    }
    
    
    
    
}