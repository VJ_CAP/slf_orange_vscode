@isTest
private class StipulationsSupport_Test 
{
    @TestSetup
    static void initData()
    {
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgFundingDataflag = new TriggerFlags__c();
        trgFundingDataflag.Name ='Funding_Data__c';
        trgFundingDataflag.isActive__c =true;
        trgLst.add(trgFundingDataflag);
        
        insert trgLst;
        
    }
    
    private testMethod static void test1 () 
    {
        
        Account a1 = new Account (Name = 'Test installer', 
                                  Type = 'Partner', 
                                  M0_Split__c = 40,
                                  Installer_Legal_Name__c='Bright planet Solar',
                                  Installer_Email__c = 'test@gmail.com',
                                  Solar_Enabled__c = true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c = true);
        insert a2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c = true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Is_Active__c = true,
                                            Product_Display_Name__c = 'Test',
                                            Product_Tier__c = '0');
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;

        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;

        Underwriting_File__c uf2 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf2;

        Stipulation_Data__c sd1 = new Stipulation_Data__c (Email_Text__c = 'Test Email Text 1', Review_Classification__c = 'SLS II');
        insert sd1;

        Stipulation_Data__c sd2 = new Stipulation_Data__c (Email_Text__c = 'Installer Only Test Email Text 1',
                                                            Installer_Only_Email__c = true, Review_Classification__c = 'SLS II');
        insert sd2;

        Stipulation_Data__c sd3 = new Stipulation_Data__c (Email_Text__c = 'Installer Only Test Email Text 2',
                                                            Installer_Only_Email__c = true, Review_Classification__c = 'SLS II');
        insert sd3;
                Stipulation_Data__c sd4 = new Stipulation_Data__c (Name='APR', Review_Classification__c = 'SLS II');
        insert sd4;
        
        //APR
        Stipulation__c s1 = new Stipulation__c (Stipulation_Data__c = sd4.Id, Underwriting__c = uf2.Id);
        insert s1;

        

        Stipulation__c s2 = new Stipulation__c (Stipulation_Data__c = sd1.Id, Underwriting__c = uf1.Id);
        insert s2;

/*      Stipulation__c s2 = new Stipulation__c (Stipulation_Data__c = sd2.Id, Underwriting__c = uf2.Id);
        insert s2;

        Stipulation__c s3 = new Stipulation__c (Stipulation_Data__c = sd3.Id, Underwriting__c = uf2.Id);
        insert s3;
*/
        /*list<StipulationsSupport.GenerateStipulationCasesInput> ilist = new list<StipulationsSupport.GenerateStipulationCasesInput>{};

        StipulationsSupport.GenerateStipulationCasesInput i1 = new StipulationsSupport.GenerateStipulationCasesInput(uf1.Id);
        ilist.add (i1);

        StipulationsSupport.GenerateStipulationCasesInput i2 = new StipulationsSupport.GenerateStipulationCasesInput(uf2.Id);
        ilist.add (i2);

        list<StipulationsSupport.GenerateStipulationCasesOutput> olist = StipulationsSupport.generateStipulationCases(ilist);


        Case c1 = [SELECT Id, Description, Origin, ContactId,  
                        OwnerId, Installer_Only_Stipulation_Email_Text__c,
                        Stipulation_Email_Text__c FROM Case LIMIT 1];


        System.assertEquals (false, olist[0].success);
        System.assertEquals (true, olist[1].success);
        System.assertEquals(1, [SELECT COUNT() FROM Case]);*/

    }   
}