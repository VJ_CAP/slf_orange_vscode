public without sharing class OpportunitySharingSupport 

//  this class contains methods called from triggers, so should be 'without sharing' to avoid unwanted access errors

{
    public OpportunitySharingSupport() 
    {
        //  nothing to do
    }

//  ******************************************************************

    public static void afterInsert (map<id,SObject> newMap)
    {
        set<Id> ids = new set<Id>{};

        for (SObject o : newMap.values())
        {
            if (o.get('Installer_Account__c') != null)
            {
                ids.add (o.Id);
            }
        }

        if (TransactionSupport.isNotProcessed('OpportunitySharingSupport.afterInsert') && ids.size() > 0) 
        {
            rebuildSharesForOpportunitiesModified(ids);
            rebuildSharesForAccountsModified(ids);
            
            //rebuildSharesForOpportunities (ids);
            //rebuildSharesForAccounts (ids);
        }       
    }

//  ******************************************************************

    public static void afterUpdate (map<id,SObject> oldMap, map<id,SObject> newMap)
    {
        //  rebuild shares if the following occurs:

        //  1.  Installer_Account__c is updated
        //  2.  AccountId is updated
        //  3.  Co_Applicant__c is updated
        //  4.  Owner is updated

        set<Id> ids = new set<Id>{};

        for (SObject o : newMap.values())
        {
            if (o.get('Installer_Account__c') != oldMap.get(o.Id).get('Installer_Account__c'))
            {
                ids.add (o.Id);
            }

            if (o.get('AccountId') != oldMap.get(o.Id).get('AccountId'))
            {
                ids.add (o.Id);
            }

            if (o.get('Co_Applicant__c') != oldMap.get(o.Id).get('Co_Applicant__c'))
            {
                ids.add (o.Id);
            }

            if (o.get('OwnerId') != oldMap.get(o.Id).get('OwnerId'))
            {
                ids.add (o.Id);
            }
        }

        if (TransactionSupport.isNotProcessed('OpportunitySharingSupport.afterUpdate')  && ids.size() > 0) 
        {
            rebuildSharesForOpportunitiesModified(ids);
            rebuildSharesForAccountsModified(ids);
            
            //rebuildSharesForOpportunities (ids);
            //rebuildSharesForAccounts (ids);
        }       
    }

//  ******************************************************************

    public static void rebuildSharesForOpportunities (set<Id> ids) 

    //  rebuilds shares for Opportunities 

    {
        list<Opportunity> recordsToProcess = new list<Opportunity>{};

        recordsToProcess = [SELECT Id, Name, 
                                RecordType.DeveloperName, AccountId, Account.Type, Owner.Name,
                                Installer_Account__c, Co_Applicant__c
                                FROM Opportunity
                                WHERE Id IN :ids
                            ];

        set<Id> partnerAccountIds = new set<Id>{};

        for (Opportunity o : recordsToProcess)
        {            
            if (o.Installer_Account__c != null) partnerAccountIds.add (o.Installer_Account__c);
        }

        map<String,Id> roleGroupsMap = PartnerSharingSupport.buildRoleGroupsMap (partnerAccountIds);

        //  share maps

        map<String,SObject> sharesNeeded = new map<String,SObject>{};
        map<String,SObject> currentShares = new map<String,SObject>{};

        //  get all current Opportunity shares, manual

        for (OpportunityShare sh : [SELECT Id, OpportunityId, UserOrGroupId,
                                    OpportunityAccessLevel
                                    FROM OpportunityShare 
                                    WHERE OpportunityId IN :ids
                                    AND RowCause = 'Manual'])
        {
            currentShares.put (getOpportunityShareKey (sh), sh);
        }

        for (Opportunity o : recordsToProcess)
        {
            //  generate new shares for the Partner Accounts

            if (o.Installer_Account__c != null)  
            {
                String key = o.Installer_Account__c + ':' +
                                    'Partner User' +
                                    ':Role';

                Id groupId = roleGroupsMap.get (key);

                if (groupId != null)
                {
                    OpportunityShare sh = new OpportunityShare (OpportunityId = o.Id, 
                                                        UserOrGroupId = groupId, 
                                                        OpportunityAccessLevel = 'Edit');
                    sharesNeeded.put (getOpportunityShareKey (sh), sh);
                }
            }
        }
        PartnerSharingSupport.updateShares (sharesNeeded, currentShares);
    }

//  ******************************************************************

    public static void rebuildSharesForAccounts (set<Id> ids) 

    //  rebuilds shares for Accounts 

    {
        list<Account> recordsToProcess = new list<Account>{};

        map<Id,Id> accToInstallerMap = new map<Id,Id>{};
        set<Id> accIds = new set<Id>{};

        for (Opportunity o : [SELECT Id, Name, 
                                AccountId,
                                Installer_Account__c, 
                                Co_Applicant__c
                                FROM Opportunity
                                WHERE Id IN :ids
                            ])
        {
            if (o.AccountId != null) accToInstallerMap.put (o.AccountId, o.Installer_Account__c);
            if (o.Co_Applicant__c != null) accToInstallerMap.put (o.Co_Applicant__c, o.Installer_Account__c);
        }

        recordsToProcess = [SELECT Id FROM Account WHERE Id IN :accToInstallerMap.keySet()];

        set<Id> partnerAccountIds = new set<Id>{};

        for (Account a : recordsToProcess)
        {            
            Id installerAccount = accToInstallerMap.get (a.Id);
            if (installerAccount != null) partnerAccountIds.add (installerAccount);
            accIds.add (a.Id);
        }

        map<String,Id> roleGroupsMap = PartnerSharingSupport.buildRoleGroupsMap (partnerAccountIds);

        //  share maps

        map<String,SObject> sharesNeeded = new map<String,SObject>{};
        map<String,SObject> currentShares = new map<String,SObject>{};

        //  get all current Account shares, manual

        for (AccountShare sh : [SELECT Id, AccountId, UserOrGroupId,
                                    AccountAccessLevel,
                                    OpportunityAccessLevel,
                                    ContactAccessLevel,
                                    CaseAccessLevel
                                    FROM AccountShare 
                                    WHERE AccountId IN :accIds
                                    AND RowCause = 'Manual'])
        {
            currentShares.put (getAccountShareKey (sh), sh);
        }

        for (Account a : recordsToProcess)
        {
            //  generate new shares for the Partner Accounts

            Id installerAccount = accToInstallerMap.get (a.Id);
            if (installerAccount != null) partnerAccountIds.add (installerAccount);

            if (installerAccount != null)  
            {
                String key = installerAccount + ':' +
                                    'Partner User' +
                                    ':Role';

                Id groupId = roleGroupsMap.get (key);

                if (groupId != null)
                {
                    AccountShare sh = new AccountShare (AccountId = a.Id, 
                                                        UserOrGroupId = groupId);

                    try { sh.put ('AccountAccessLevel', 'Edit'); } catch (Exception e) {}
                    try { sh.put ('OpportunityAccessLevel', 'None'); } catch (Exception e) {}
                    try { sh.put ('ContactAccessLevel', 'None'); } catch (Exception e) {}
                    try { sh.put ('CaseAccessLevel', 'None'); } catch (Exception e) {}

                    sharesNeeded.put (getAccountShareKey (sh), sh);
                }
            }
        }
        PartnerSharingSupport.updateShares (sharesNeeded, currentShares);
    }

//******************************************************************

    private static String getAccountShareKey (AccountShare ash)
    {
        return ash.AccountId + '|' + 
                ash.UserOrGroupId + '|' + 
                ash.AccountAccessLevel + '|' +
                ash.OpportunityAccessLevel + '|' +
//                ash.ContactAccessLevel + '|' +
                ash.CaseAccessLevel;
    }

//******************************************************************

    private static String getOpportunityShareKey (OpportunityShare sh)
    {
        return sh.OpportunityId + '|' + 
                sh.UserOrGroupId + '|' + 
                sh.OpportunityAccessLevel;
    }

//******************************************************************

    /*
    private static String getCustomShareKey (SObject sh)
    {
        return (String) sh.get ('ParentId') + '|' + 
                (String) sh.get ('UserOrGroupId') + '|' + 
                (String) sh.get ('AccessLevel');
    }
    */

//******************************************************************

    /*
    public static void addCreatorToOpportunityTeam (list<Opportunity> opportunities)
    {

        list<OpportunityTeamMember> oppTeamMembers = new list<OpportunityTeamMember>{};

        for (Opportunity o : opportunities)
        {
            oppTeamMembers.add (new OpportunityTeamMember (OpportunityId = o.Id, 
                                                            UserId = o.CreatedById, 
                                                            TeamMemberRole = 'Partner Creator'));
        }

        System.debug('****** Inserting Partner Creator as Opportunity Team Member');

        for (String s : DebugSupport.insertAndReturnErrors (oppTeamMembers))
        {
            System.debug ('Insert OpportunityTeamMember errors: ' + s);
        }

    }
    */    
    
    public static void rebuildSharesForOpportunitiesModified (set<Id> ids) 
    {
        list<Opportunity> recordsToProcess = new list<Opportunity>{};

        recordsToProcess = [SELECT Id, Name, 
                                RecordType.DeveloperName, AccountId, Account.Type, Owner.Name,
                                Installer_Account__c, Co_Applicant__c
                                FROM Opportunity
                                WHERE Id IN :ids
                            ];

        map<String,Id> roleGroupsMap = PartnerSharingSupport.buildRoleGroupsMap ();

        map<String,SObject> sharesNeeded = new map<String,SObject>{};
        map<String,SObject> currentShares = new map<String,SObject>{};


        for (OpportunityShare sh : [SELECT Id, OpportunityId, UserOrGroupId,
                                    OpportunityAccessLevel
                                    FROM OpportunityShare 
                                    WHERE OpportunityId IN :ids
                                    AND RowCause = 'Manual'])
        {
            currentShares.put (getOpportunityShareKey (sh), sh);
        }

        for (Opportunity o : recordsToProcess)
        {
        
             for (String keyName : roleGroupsMap.keySet())
             {
                system.debug('### KeyName :' + keyName);
                if(!keyName.contains('RoleAndSubordinates'))
                {
                    system.debug('### KeyName in IF :' + keyName);
                    Id groupId = roleGroupsMap.get(keyName);
        
                    if (groupId != null)
                    {
                        OpportunityShare sh = new OpportunityShare (OpportunityId = o.Id, UserOrGroupId = groupId, OpportunityAccessLevel = 'Edit');
                        sharesNeeded.put (getOpportunityShareKey (sh), sh);
                    }
                 }
             }
        }
        system.debug('### sharesNeeded:'+ sharesNeeded);
        PartnerSharingSupport.updateShares (sharesNeeded, currentShares);
    }
 
 
    public static void rebuildSharesForAccountsModified (set<Id> ids) 
    {
        list<Account> recordsToProcess = new list<Account>{};

        map<Id,Id> accToInstallerMap = new map<Id,Id>{};
        set<Id> accIds = new set<Id>{};

        for (Opportunity o : [SELECT Id, Name, AccountId, Installer_Account__c, Co_Applicant__c FROM Opportunity WHERE Id IN :ids])
        {
            if (o.Co_Applicant__c != null) accToInstallerMap.put (o.Co_Applicant__c, o.Co_Applicant__c);
        }

        recordsToProcess = [SELECT Id FROM Account WHERE Id IN :accToInstallerMap.keySet()];

        map<String,Id> roleGroupsMap = PartnerSharingSupport.buildRoleGroupsMap ();
        system.debug('### rolesMap:' + roleGroupsMap);
        //  share maps

        map<String,SObject> sharesNeeded = new map<String,SObject>{};
        map<String,SObject> currentShares = new map<String,SObject>{};

        //  get all current Account shares, manual

        for (AccountShare sh : [SELECT Id, AccountId, UserOrGroupId, AccountAccessLevel, OpportunityAccessLevel, ContactAccessLevel, CaseAccessLevel FROM AccountShare WHERE AccountId IN :accToInstallerMap.keySet() AND RowCause = 'Manual'])
        {
            currentShares.put (getAccountShareKey (sh), sh);
        }
        
        system.debug('### currentShares: '+ currentShares);
        
        for (Account a : recordsToProcess)
        {
            for (String keyName : roleGroupsMap.keySet())
             {
                system.debug('### KeyName :' + keyName);
                if(!keyName.contains('RoleAndSubordinates'))
                {
                    system.debug('### KeyName in IF :' + keyName);
                    Id groupId = roleGroupsMap.get(keyName);
                    
                    if (groupId != null)
                    {
                        AccountShare sh = new AccountShare (AccountId = a.Id, UserOrGroupId = groupId);
                        /*
                        sh.AccountAccessLevel = 'Edit';
                        sh.OpportunityAccessLevel = 'Edit';
                        sh.ContactAccessLevel = 'Edit';
                        sh.CaseAccessLevel = 'None';
                        */
        
                        try { sh.put ('AccountAccessLevel', 'Edit'); } catch (Exception e) {}
                        try { sh.put ('OpportunityAccessLevel', 'None'); } catch (Exception e) {}
                        try { sh.put ('ContactAccessLevel', 'Edit'); } catch (Exception e) {}
                        try { sh.put ('CaseAccessLevel', 'None'); } catch (Exception e) {}
        
                        sharesNeeded.put (getAccountShareKey (sh), sh);
                    }
                }
            }
        }
        system.debug('### sharesNeeded:' + sharesNeeded);
        PartnerSharingSupport.updateShares (sharesNeeded, currentShares);
    }
 
    
}