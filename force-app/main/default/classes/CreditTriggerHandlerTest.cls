/**
* Description: Test class for CreditTrg trigger and CreditTriggerHandler class. 
*
*   Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma K         09/26/2017          Created
******************************************************************************************/
@isTest
public class CreditTriggerHandlerTest {
    @testsetup static void createtestdata(){
        // SLFUtilityDataSetup.initData();
        insertUserData();
    }
    private static void insertUserData(){
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();   
        
        //get the person account record type
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        Case caseTest = new Case();
        caseTest.Status = 'New';
        caseTest.Origin = 'Email';
        insert caseTest;
        
        Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.Push_Endpoint__c ='www.test.com';
        acc.PersonEmail = 'slfaccount@slf.com';
        acc.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
        acc.Installer_Email__c = 'slfaccount@slf.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Subscribe_to_Push_Updates__c = true;
        acc.Subscribe_to_Event_Updates__c=true;
        acc.SMS_Opt_in__c=true;
        insert acc;
        
        List<Contact> conList = new List<Contact>();
        Contact cObj1 = new Contact(LastName = 'TestCon',Email='test@gmail.com');
        conList.add(cObj1);
        Contact cObj2 = new Contact(LastName = 'Sunlight Financial',Email='Sunlight@gmail.com');
        conList.add(cObj2);
        insert conList;        
        
        Product__c prodct = new Product__c();
        prodct.ACH__c = false;
        prodct.APR__c = 4.99;
        prodct.bump__c = false;
        prodct.Installer_Account__c = acc.Id;
        prodct.Internal_Use_Only__c = false;
        prodct.Is_Active__c = true;
        prodct.Long_Term_Facility__c = 'TCU';
        prodct.LT_Max_Loan_Amount__c =  100000.0;
        prodct.Max_Loan_Amount__c = 100000.0;
        prodct.State_Code__c = 'CA';
        prodct.FNI_Min_Response_Code__c = 10;
        prodct.Product_Display_Name__c = 'Test';
        prodct.Qualification_Message__c = '123';
        prodct.Product_Tier__c = '0';
        insert prodct;
        
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Postal_Code__c = '94901';
        opp.Install_Street__c = '39 GLEN AVE';
        opp.Install_City__c = 'SAN RAFAEL';
        opp.SLF_Product__c = prodct.Id;
        opp.Combined_Loan_Amount__c = 10000;
        opp.Long_Term_Loan_Amount_Manual__c = 20000;
        opp.Co_Applicant__c = acc.Id;
        opp.Project_Category__c='Solar';
        insert opp;
        
   
        
        
        //Insert Quote Record
        List<Quote> quoteList = new List<Quote>();
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = opp.id;
        quoteRec.SLF_Product__c = prodct.Id ;
        quoteRec.Monthly_Payment__c = 1000;
        quoteRec.Combined_Loan_Amount__c = 9999;
        quoteRec.includesBattery__c=true;
        //quoteRec.Accountid =acc.id;
        quoteList.add(quoteRec);
        
        Quote quoteRecOne = new Quote();
        quoteRecOne.Name ='Test quote';
        quoteRecOne.Opportunityid = opp.id;
        quoteRecOne.SLF_Product__c = prodct.Id ;
        quoteRecOne.Monthly_Payment__c = 1000;
        quoteRecOne.Combined_Loan_Amount__c = 999;
        
        //quoteRec.Accountid =acc.id;
        quoteList.add(quoteRecOne);
        insert quoteList;
        
        opp.SyncedQuoteId = quoteRec.Id;
        
        update opp;        
        
        
        Underwriting_File__c uwf = new Underwriting_File__c(
            Send_Confirmation_Email__c = true,
            Opportunity__c = opp.Id,
            M1_Approval_Requested_Date__c=date.Today()
        );
        insert uwf;
        
       //insert new SFSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), Root_Folder_ID__c='0');
        
        Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', Email_Text__c = 'Test data', POS_Message__c = 'test',CL_Code__c='CL1',Email_Template_Name__c='Test Email Template',Review_Classification__c='SLS I');
        insert sd;
        
        Stipulation__c stp = new Stipulation__c();
        stp.Status__c ='New';
        stp.Stipulation_Data__c = sd.id;
        
        stp.Underwriting__c =uwf.id;
        insert stp;
        
        Twilio_Templates__c twilioRec = new Twilio_Templates__c(Reply_Text__c='Welcome',
                                                                Response_Text__c='Welcome'
                                                               );
        Insert twilioRec;
        
        Document  document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'Sunlight_Logo1';
        document.IsPublic = true;
        document.Name = 'Sunlight Logo';
        document.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert document;
        
        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'name';
        validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
        validEmailTemplate.TemplateType = 'text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        
        insert validEmailTemplate;
    }
    /**
* Description: Test method to cover StipulationTriggerHandler funtionality.
*/
    public testMethod static void creditTriggerTest()
    {
        Opportunity opp=[SELECT id,Combined_Loan_Amount__c,Long_Term_Loan_Amount_Manual__c,AccountId,Co_Applicant__c FROM Opportunity WHERE name='OpName'];
        Product__c prodct=[SELECT id FROM Product__c WHERE State_Code__c='CA'];
        Quote quoteRecOne=[SELECT id FROM Quote WHERE Opportunityid =:opp.Id LIMIT 1];
        Contact cObj=[SELECT id FROM Contact WHERE LastName='TestCon'];
        List<SLF_Credit__c> slfCrdtLst = new List<SLF_Credit__c>();
        Stipulation__c stipRec = [SELECT id, Credit_Record__c FROM Stipulation__c WHERE Underwriting__r.Opportunity__c=:opp.Id];
        
        for(Integer i=0; i<2; i++)
        {
            SLF_Credit__c slfCredit = new SLF_Credit__c();
            slfCredit.SLF_Product__c = prodct.Id;
            slfCredit.Total_Loan_Amount__c = opp.Combined_Loan_Amount__c;
            slfCredit.Long_Term_Loan_Amount__c  = opp.Long_Term_Loan_Amount_Manual__c;
            slfCredit.Primary_Contact__c = cObj.Id;
            if(null != opp.AccountId)
                slfCredit.Primary_Applicant__c = opp.AccountId;
            
            if(null != opp.Co_Applicant__c)
                slfCredit.Co_Applicant__c = opp.Co_Applicant__c;
            
            slfCredit.Opportunity__c = opp.Id ;
            if(i == 0){
                slfCredit.Status__c = 'Auto Approved';
                slfCredit.IsSyncing__c = true ;
            }
            
            else{
                slfCredit.Status__c = 'Manual Approved';
                slfCredit.IsSyncing__c = false ;
            }
            
            
            slfCredit.LT_StipRsn1__c = 'D15';
            
            slfCredit.Long_Term_Amount_Approved__c = 1000;
            slfCredit.Customer_has_authorized_credit_hard_pull__c = true;
            slfCrdtLst.add(slfCredit);
        }
        
        Test.startTest();    
        insert slfCrdtLst;
        stipRec.Credit_Record__c=slfCrdtLst[1].Id;
        Update stipRec;
        CreditTriggerHandler.isTrgExecuting =true;
        slfCrdtLst[1].Status__c='Auto Approved';
        slfCrdtLst[1].LT_StipRsn1__c = 'D16';
        slfCrdtLst[1].IsSyncing__c=true;
        Update slfCrdtLst[1];
        
        CreditTriggerHandler.isTrgExecuting =true;
        slfCrdtLst[1].Status__c='Expired';
        slfCrdtLst[1].LT_StipRsn1__c = 'CL1';
        slfCrdtLst[1].IsSyncing__c=true;
        Update slfCrdtLst[1];
        
        CreditTriggerHandler.isTrgExecuting =true;
        opp.ACH_APR_Process_Required__c = true;
        update opp;
        Update slfCrdtLst[1];
        
        CreditTriggerHandler.DesyncCreditandQuote(new Set<Id>{quoteRecOne.id});

        Test.stopTest();
        
    }
    
    public testMethod static void creditTriggerTest2()
    {
        Opportunity opp=[SELECT id,Combined_Loan_Amount__c,Long_Term_Loan_Amount_Manual__c,AccountId,Co_Applicant__c FROM Opportunity WHERE name='OpName'];
        Product__c prodct=[SELECT id FROM Product__c WHERE State_Code__c='CA'];
        Quote quoteRecOne=[SELECT id FROM Quote WHERE Opportunityid =:opp.Id LIMIT 1];
        Contact cObj=[SELECT id FROM Contact WHERE LastName='TestCon'];
        List<SLF_Credit__c> slfCrdtLst = new List<SLF_Credit__c>();
        Stipulation__c stipRec = [SELECT id, Credit_Record__c FROM Stipulation__c WHERE Underwriting__r.Opportunity__c=:opp.Id];
        
        for(Integer i=0; i<2; i++)
        {
            SLF_Credit__c slfCredit = new SLF_Credit__c();
            slfCredit.SLF_Product__c = prodct.Id;
            slfCredit.Total_Loan_Amount__c = opp.Combined_Loan_Amount__c;
            slfCredit.Long_Term_Loan_Amount__c  = opp.Long_Term_Loan_Amount_Manual__c;
            slfCredit.Primary_Contact__c = cObj.Id;
            if(null != opp.AccountId)
                slfCredit.Primary_Applicant__c = opp.AccountId;
            
            if(null != opp.Co_Applicant__c)
                slfCredit.Co_Applicant__c = opp.Co_Applicant__c;
            
            slfCredit.Opportunity__c = opp.Id ;
            if(i == 0){
                slfCredit.Status__c = 'Auto Approved';
                slfCredit.IsSyncing__c = true ;
            }
            
            else{
                slfCredit.Status__c = 'Manual Approved';
                slfCredit.IsSyncing__c = false ;
            }
            
            
            slfCredit.LT_StipRsn1__c = 'D15';
            
            slfCredit.Long_Term_Amount_Approved__c = 1000;
            slfCredit.Customer_has_authorized_credit_hard_pull__c = true;
            slfCrdtLst.add(slfCredit);
        }
        
        Test.startTest();    
        insert slfCrdtLst;
        
        CreditTriggerHandler.isTrgExecuting =true;
        opp.ACH_APR_Process_Required__c = true;
        opp.Project_Category__c = 'Home';
        update opp;
        slfCrdtLst[1].LT_StipRsn2__c = 'D16';
        slfCrdtLst[1].LT_StipRsn3__c = 'D16';
        slfCrdtLst[1].LT_StipRsn4__c = 'D16';
        Update slfCrdtLst[1];
        
        CreditTriggerHandler.isTrgExecuting =true;
        slfCrdtLst[0].LT_StipRsn1__c = 'D15';
        slfCrdtLst[0].Bureau_Primary_DoB__c = '19670813';
        Update slfCrdtLst[0];
        
        CreditTriggerHandler.isTrgExecuting =true;
        slfCrdtLst[0].LT_StipRsn1__c = 'D16';
        slfCrdtLst[0].Bureau_Primary_DoB__c = null;
        cObj.Birthdate = system.today();
        update cObj;
        /*slfCrdtLst[1].Date_of_Birth__c = '9/4/2019 5:32 AM';*/
        Update slfCrdtLst[0];
                
        Test.stopTest();
        
    }
    
}