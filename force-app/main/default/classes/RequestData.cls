/**
* Description: RequestData class has properties for each sObject which comes in as part of request
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public class RequestData{
    //List of customerIds with cama saparated values come's in as request
    public string customerIds {get; set;}
    
    //List of externalIds with cama saparated values come's in as request
    public string externalIds {get; set;}
    
    //A string installerCode come's in as request which is a unic code available in Account Object
   // public string installerCode{get; set;}
    
  
}