/******************************************************************************************
* Description: Test class to cover DrawRequestDataServiceImpl services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkatesh            01/02/2019          Created
******************************************************************************************/
@isTest
public class GetProjectTypeServiceImpl_Test {
	 @testsetup static void createtestdata(){
        // SLFUtilityDataSetup.initData();
        insertUserData();
    }
    private static void insertUserData(){
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();   
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        insert trgLst;
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'testMagnolia@gmail.com';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Magnolia';
        acc.Home_Improvement_Enabled__c=True;
        insert acc;
        List<Contact> conList=new List<Contact>();
        Contact conUser = new Contact(LastName ='conUser',AccountId = acc.Id);
        insert conUser;
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
            system.debug('#### roleMap-->:'+role.DeveloperName+'### role id-->:'+ role.Id);
        }
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        List<user> lstUser = new List<user>();
        User portalUser = new User(alias = 'testc', 
                                   Email='testMagnoliaUser@mail.com', 
                                   IsActive = true,
                                   Hash_Id__c='333',
                                   EmailEncodingKey='UTF-8', 
                                   LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', 
                                   ProfileId = p.Id, 
                                  // UserRoleId = roleMap.get('BrightPlanetSolarPartnerUser'),
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testMagnoliaUser@mail.com',
                                   ContactId = conUser.Id);
        lstUser.add(portalUser);
        system.debug('#### portalUser-->:'+portalUser);
        user adminUser=[SELECT id FROM User WHERE id=:UserInfo.getUserId()];
        system.runAs(adminUser){
            insert lstUser;
        }
        Project_Type__c objProjType=new Project_Type__c();
        objProjType.is_Active__c=true;
        objProjType.name='Other';
        objProjType.Type__c ='Home';
          system.runAs(adminUser){
            insert objProjType;
        }
        system.debug('#### roleMap-->:'+roleMap);
    }
    private testMethod static void getProjectTypeTest(){

        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testMagnoliaUser@mail.com'];
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            
        String projectTypeStr;
        Test.starttest();
        projectTypeStr = '{"projectTypes": [{"isActive": true,"projectCategory":"Home"}]}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,projectTypeStr,'getProjectTypes');
            
        projectTypeStr = '{"projectTypes": [{"isActive": true,"projectCategory":""}]}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,projectTypeStr,'getProjectTypes');
            
        projectTypeStr = '{}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,projectTypeStr,'getProjectTypes');
        
            // exception block
        projectTypeStr = '[{}]';
        SLFRestDispatchTest.MapWebServiceURI(userMap,projectTypeStr,'getProjectTypes');
        
        GetProjectTypeDataServiceImpl  clsGetProjectType = new GetProjectTypeDataServiceImpl();
            list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
                 UnifiedBean unifiedBean = new UnifiedBean(); 
       // clsGetProjectType.gerErrorMsg(unifiedBean,'sErrorMsg', 'sReturnCode', iolist);
        Test.StopTest();

        }
    }
}