public with sharing class DebugSupport 
{
    private static final String className = 'DebugSupport';

    public static Boolean sendDebugEmail (String content, String subject, String toAddress)
    {
        if (content == null || content.length() == 0) return false;

        list<Messaging.SingleEmailMessage> messages = new list<Messaging.SingleEmailMessage>{}; 
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

        message.setToAddresses(new String[] {toAddress});
        message.setSubject(subject);
        message.setPlainTextBody(content);

        messages.add (message);

        list<Messaging.Sendemailresult> sendEmailResults = Messaging.sendEmail (messages);

        return sendEmailResults[0].success;
    }

//******************************************************************

    public static list<String> insertAndReturnErrors (list<SObject> objectsToInsert)
    {
        list<String> errors = new list<String>{};
        list<Database.SaveResult> srs = new list<Database.SaveResult>{};

        try
        {
            srs = Database.insert (objectsToInsert, false);
        }
        catch (Exception e)
        {
            //  nothing
        }
        
        Integer sub = 0;
        for (Database.SaveResult sr : srs)
        {
            if (sr.isSuccess() == false)
            {
                for (Database.Error err : sr.getErrors()) 
                {
                    String msg = 'Insert of Object failed : ' + err.getMessage();
                    errors.add (msg);
               	}
            }
            sub++;
        }
        return errors;
    }

//******************************************************************

    public static list<String> updateAndReturnErrors (list<SObject> objectsToUpdate)
    {
        list<String> errors = new list<String>{};
        list<Database.SaveResult> srs = new list<Database.SaveResult>{};

        try
        {
            srs = Database.update (objectsToUpdate, false);
        }
        catch (Exception e)
        {
            //  nothing
        }

        Integer sub = 0;
        for (Database.SaveResult sr : srs)
        {
            if (sr.isSuccess() == false)
            {
                for (Database.Error err : sr.getErrors()) 
                {
                    String msg = 'Update to Object ' + objectsToUpdate[sub].Id + ' failed : ' + err.getMessage();
                    errors.add (msg);
                }
            }
            sub++;
        }
        return errors;
    }

//******************************************************************

    public static list<String> deleteAndReturnErrors (list<SObject> objectsToDelete)
    {
        list<String> errors = new list<String>{};
        list<Database.DeleteResult> drs = new list<Database.DeleteResult>{};

        try
        {
            drs = Database.delete (objectsToDelete, false);
        }
        catch (Exception e)
        {
            //  nothing
        }

        Integer sub = 0;
        for (Database.DeleteResult dr : drs)
        {
            if (dr.isSuccess() == false)
            {
                for (Database.Error err : dr.getErrors()) 
                {
                    String msg = 'Delete of Object ' + objectsToDelete[sub].Id + ' failed : ' + err.getMessage();
                    errors.add (msg);
            	}
            }
            sub++;
        }
        return errors;
    }  

//******************************************************************

    public static list<String> insertAndReturnErrors (SObject objectToInsert)
    {
        return insertAndReturnErrors (new list<SObject>{objectToInsert});
    }

//******************************************************************

    public static list<String> updateAndReturnErrors (SObject objectToUpdate)
    {
        return updateAndReturnErrors (new list<SObject>{objectToUpdate});
    }

//******************************************************************

    public static list<String> deleteAndReturnErrors (SObject objectToDelete)
    {
        return deleteAndReturnErrors (new list<SObject>{objectToDelete});
    }

//******************************************************************

}