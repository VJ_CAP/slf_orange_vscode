/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkat               05/03/2019              Created
******************************************************************************************/
public class UserProfileServiceImpl extends RESTServiceBase{
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse fetchData(RequestWrapper rw){
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        string errMsg = '';
        UnifiedBean respBean = new UnifiedBean();
        String queryParam='';
        String userQuery='SELECT id,IsActive,Hash_Id__c,contactId,FirstName,LastName,Email,Phone,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c FROM user WHERE';
        try{
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            respBean.users = new List<UnifiedBean.UsersWrapper>();
            
            if(null == reqData.users){
                errMsg= ErrorLogUtility.getErrorCodes('214',null);
                gerErrorMsg(respBean,errMsg,'214');
                iRestRes = (IRestResponse)respBean;
                res.response= iRestRes;
                return iRestRes;
            }else if(null != reqData.users  && String.isBlank(reqData.users[0].hashId)  && String.isBlank(reqData.users[0].Id)){
                errMsg= ErrorLogUtility.getErrorCodes('369',null);
                gerErrorMsg(respBean,errMsg,'369');
                iRestRes = (IRestResponse)respBean;
                res.response= iRestRes;
                return iRestRes;
            }else if(null != reqData.users  && String.isNotBlank(reqData.users[0].hashId)  && String.isNotBlank(reqData.users[0].Id)){
                errMsg= ErrorLogUtility.getErrorCodes('373',null);
                gerErrorMsg(respBean,errMsg,'373');
                iRestRes = (IRestResponse)respBean;
                res.response= iRestRes;
                return iRestRes;
            }else if(String.isNotBlank(reqData.users[0].hashId)){
                queryParam=reqData.users[0].hashId;
                userQuery+=' Hash_Id__c=:queryParam';
            }else if(String.isNotBlank(reqData.users[0].Id)){
                queryParam=reqData.users[0].Id;
                userQuery+=' Id=:queryParam';
            }
            if(String.isNotBlank(userQuery)){
                userQuery+=' AND IsActive=true';
                system.debug('### userQuery==>:'+userQuery);
                List<User> lstUser = DataBase.query(userQuery);
                if(lstUser!=null && lstUser.size()>0){
                    User objUser = lstUser.get(0);
                    generateResponse(respBean, objUser, reqData);   
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('295',null);
                    gerErrorMsg(respBean,errMsg,'295');
                }
            }
            
            iRestRes = (IRestResponse)respBean;
            // res.response= iRestRes;
            
        }catch(Exception err){
            
            system.debug('### DualStepVerificationServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('DualStepVerificationServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            errMsg= ErrorLogUtility.getErrorCodes('400',null);
            gerErrorMsg(respBean,errMsg,'400');
            iRestRes = (IRestResponse)respBean;
            res.response= iRestRes;
        }
        return iRestRes;
    }
    
    public void generateResponse(UnifiedBean respBean,User objUserRec, UnifiedBean reqData){
        
        UnifiedBean.UsersWrapper objUserWrp = new UnifiedBean.UsersWrapper();
        objUserWrp.questions = new List<UnifiedBean.SelectedQuestionsWrapper>();
        objUserWrp.selectedQuestionsAndAnswers = new List<UnifiedBean.SelectedQuestionsAndAnswerWrapper>();
        UnifiedBean.SelectedQuestionsWrapper questionsWrp = new UnifiedBean.SelectedQuestionsWrapper();
        UnifiedBean.SelectedQuestionsAndAnswerWrapper selectdQAsWrp = new UnifiedBean.SelectedQuestionsAndAnswerWrapper();
        
        objUserWrp.firstName=objUserRec.FirstName;
        objUserWrp.lastName=objUserRec.LastName;
        objUserWrp.email=objUserRec.email;
        objUserWrp.hashId=objUserRec.Hash_Id__c;
        objUserWrp.id=objUserRec.Id;
        objUserWrp.phone=objUserRec.Phone;
        
        if(String.isNotBlank(objUserRec.Question1__c))
        selectdQAsWrp.question = objUserRec.Question1__c;
        selectdQAsWrp.answer = String.isNotBlank(objUserRec.Answer1__c)?objUserRec.Answer1__c:'';
        objUserWrp.selectedQuestionsAndAnswers.add(selectdQAsWrp);
        if(String.isNotBlank(objUserRec.Question2__c)  )
            selectdQAsWrp = new UnifiedBean.SelectedQuestionsAndAnswerWrapper();
        selectdQAsWrp.question = objUserRec.Question2__c;
        selectdQAsWrp.answer = String.isNotBlank(objUserRec.Answer2__c)?objUserRec.Answer2__c:'';
        objUserWrp.selectedQuestionsAndAnswers.add(selectdQAsWrp);
        
        if(String.isNotBlank(objUserRec.Question3__c) )
            selectdQAsWrp = new UnifiedBean.SelectedQuestionsAndAnswerWrapper();
        selectdQAsWrp.question = objUserRec.Question3__c;
        selectdQAsWrp.answer =String.isNotBlank(objUserRec.Answer3__c)?objUserRec.Answer3__c:'';
        objUserWrp.selectedQuestionsAndAnswers.add(selectdQAsWrp);
        
        List<Security_Question__mdt> securityQuestionList = [SELECT id,Question__c FROM Security_Question__mdt];
        
        if(!securityQuestionList.isEmpty()){
            for(Security_Question__mdt objMtd:securityQuestionList){
                questionsWrp = new UnifiedBean.SelectedQuestionsWrapper();
                questionsWrp.question=objMtd.Question__c;
                objUserWrp.questions.add(questionsWrp);
            } 
            
        }else{
            // Security questions not configured
        }
        respBean.users.add(objUserWrp);
        respBean.returnCode = '200';
    }
    /**
* method
**/
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        List<UnifiedBean.ErrorWrapper> errorList = new List<UnifiedBean.ErrorWrapper>();
        UnifiedBean.ErrorWrapper errorWraper = new UnifiedBean.ErrorWrapper();
        errorWraper.errorMessage = sErrorMsg;
        errorList.add(errorWraper);
        objUnifiedBean.error = errorList;
        objUnifiedBean.returnCode = sReturnCode;
    }
}