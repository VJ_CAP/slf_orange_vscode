global class Batch_CreateInSighten implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {

    //Database.executeBatch(new Batch_FetchSite(), 1);
    private Set<Id> opptyIdSet = new Set<Id>();
    public Map<Id,String> conSigtnTknMap = new Map<Id,String>();    
    
    public Batch_CreateInSighten(Set<Id> idSet)
    {
        opptyIdSet.addAll(idSet);
    }
    
    public Batch_CreateInSighten(Set<Id> idSet,Map<Id,String> sigtnTknMap)
    {
         opptyIdSet.addAll(idSet);
         if(sigtnTknMap!=null){
            conSigtnTknMap.putAll(sigtnTknMap);
         }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        /* Currently we are processing all opportunities, since we do not have a way to tell which 
         * sites have changed. Eventually, we should be able to filter only those opportunities that
         * need to be modified.
         * 
         */
        String query = 'SELECT Id, OwnerId, AccountId, Install_Street__c, Install_City__c, Install_State_Code__c, Install_Postal_Code__c,Account.FirstName, Account.LastName,Account.PersonEmail,Owner.contactId FROM Opportunity WHERE Id IN :opptyIdSet And Create_in_Sighten__c = true'; 
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext sc)
    {
        
    }
 
    global void execute(Database.BatchableContext BC,List<Opportunity> ol) 
    {
        String sightenToken; 
        for (Opportunity curOppty : ol) {      
            if(curOppty.Owner.contactId != null && conSigtnTknMap.containsKey(curOppty.Owner.contactId)){
                sightenToken = conSigtnTknMap.get(curOppty.Owner.contactId);
            }
            sightenCallout.createSightenSite(curOppty.Account.FirstName, curOppty.Account.LastName, curOppty.Account.PersonEmail, curOppty.Install_Street__c, curOppty.Install_City__c, curOppty.Install_State_Code__c, curOppty.Install_Postal_Code__c, curOppty.Id, sightenToken);  
        }
    }
    
    global void finish(Database.BatchableContext BC) 
    {

    }
}