@RestResource(urlMapping='/GetProducts/*')
global with sharing class RestControllerGetProduct 
{
    @httpGet
    global static List<Product__c> getProducts()
    {
        String stateCode = RestContext.request.params.get('stateCode');
        System.debug('***Debug: UserInfo.getOrganizationId(): ' + UserInfo.getOrganizationId());

        if (stateCode != null)
        {
           //Removed ACH__c from below query by Deepika as part of Orange - 3622
           return [SELECT Id, Product_Display_Name__c, State_Code__c
                    FROM Product__c 
                    WHERE State_Code__c = :stateCode
                    AND Is_Active__c = true]; 
        }
        else
        {
            //Removed ACH__c from below query by Deepika as part of Orange - 3622
             return [SELECT Id, Product_Display_Name__c, State_Code__c
                        FROM Product__c
                        WHERE Is_Active__c = true];
        }
    }
}