@isTest
public class FundingSnapshotBoxUpload_Test {
    @testsetup static void createtestdata(){
        SLFUtilityTest.createCustomSettings();   
        
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        Case caseTest = new Case();
        caseTest.Status = 'New';
        caseTest.Origin = 'Email';
        insert caseTest;
        
        Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.Push_Endpoint__c ='www.test.com';
        acc.PersonEmail = 'slfaccount@slf.com';
        acc.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
        acc.Installer_Email__c = 'slfaccount@slf.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Subscribe_to_Push_Updates__c = true;
        acc.Subscribe_to_Event_Updates__c=true;
        acc.SMS_Opt_in__c=true;
        insert acc;
        
        Contact cObj = new Contact(LastName = 'TestCon',Email='test@gmail.com');
        insert cObj;
        
        Product__c prodct = new Product__c();
        prodct.ACH__c = false;
        prodct.APR__c = 4.99;
        prodct.bump__c = false;
        prodct.Installer_Account__c = acc.Id;
        prodct.Internal_Use_Only__c = false;
        prodct.Is_Active__c = true;
        prodct.Long_Term_Facility__c = 'TCU';
        prodct.LT_Max_Loan_Amount__c =  100000.0;
        prodct.Max_Loan_Amount__c = 100000.0;
        prodct.State_Code__c = 'CA';
        prodct.FNI_Min_Response_Code__c = 10;
        prodct.Product_Display_Name__c = 'Test';
        prodct.Qualification_Message__c = '123';
        prodct.Product_Tier__c = '0';
        insert prodct;
        
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Postal_Code__c = '94901';
        opp.Install_Street__c = '39 GLEN AVE';
        opp.Install_City__c = 'SAN RAFAEL';
        opp.SLF_Product__c = prodct.Id;
        opp.Combined_Loan_Amount__c = 10000;
        opp.Long_Term_Loan_Amount_Manual__c = 20000;
        opp.Co_Applicant__c = acc.Id;
        opp.Project_Category__c='Solar';
        insert opp;                   
        
        Underwriting_File__c uwf = new Underwriting_File__c(
            Send_Confirmation_Email__c = true,
            Opportunity__c = opp.Id,
            M1_Approval_Requested_Date__c=date.Today()
        );
        insert uwf;        
        
        Document  document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'Sunlight_Logo1';
        document.IsPublic = true;
        document.Name = 'Sunlight Logo';
        document.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert document;
      
             
    }
    /**
    *
    * Description: This method is used to cover EquipmentServiceImpl and EquipmentDataServiceImpl 
    *
    **/    
    private testMethod static void fundingSnapshotTest(){
        test.startTest();
        //get the account record type
         ID facilityAccRectypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Facility').getRecordTypeId();      
        Account facility = new Account();
        facility.Name = 'SLFTest';
        facility.RecordTypeId = facilityAccRectypeid;
        facility.FNI_Domain_Code__c='CRB';
        insert facility;
        Account acc = [SELECT Id FROM Account WHERE IsPersonAccount = true LIMIT 1]; 
        Opportunity opp = [select Id,Name,Hash_Id__c from opportunity where AccountId =: acc.Id LIMIT 1]; 
        List<Underwriting_File__c> underwritingLst  = [select Id,M1_Approval_Date__c,Opportunity__c,M0_Approval_Requested_Date__c, M1_Approval_Requested_Date__c, M2_Approval_Requested_Date__c,
                                                       Opportunity__r.Installer_Account__r.M0_Required__c, Opportunity__r.Installer_Account__r.M1_Required__c,Opportunity__r.Installer_Account__r.M2_Required__c from Underwriting_File__c where Opportunity__r.Id =: opp.id limit 1];
         
        underwritingLst[0].M1_Approval_Date__c=Date.today().addDays(-10);
        Update underwritingLst[0];
        
        Funding_Snapshot__c fsObj = new Funding_Snapshot__c(Account__c = facility.Id);
        insert fsObj;
        
        Funding_Snapshot_Line_Item__c fsliObj = new Funding_Snapshot_Line_Item__c(Funding_Snapshot__c = fsObj.Id,Underwriting_File__c=underwritingLst[0].ID);
        insert fsliObj;
        
        Map<id,Funding_Snapshot__c> accSnapMap = new Map<id,Funding_Snapshot__c>();
        accSnapMap.put(facility.Id,fsObj); 
        
        CreateFundingSnapshotRecords objBatch= new CreateFundingSnapshotRecords();
        dataBase.executeBatch(objBatch);
        FundingSnapshotBoxUpload objBatch1= new FundingSnapshotBoxUpload(accSnapMap);
        dataBase.executeBatch(objBatch1);
        test.stopTest();
    }
}