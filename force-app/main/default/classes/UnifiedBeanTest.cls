@isTest(SeeAllData=false)
public class UnifiedBeanTest {
    private testMethod static void test1(){
        UnifiedBean UnifiedBeanobj = new UnifiedBean();
        UnifiedBeanobj.returnCode = null;
        //UnifiedBeanobj.amountDrawn=null;
        UnifiedBeanobj.returnCode=null;
        UnifiedBeanobj.projectIds=null;
        UnifiedBeanobj.externalIds=null;
        UnifiedBeanobj.hashIds=null;
        UnifiedBeanobj.message =null;
        UnifiedBeanobj.pageNumber=null;
        UnifiedBeanobj.numberOfRecords=null;
        UnifiedBeanobj.projectCategory =null;
        UnifiedBeanobj.totalNumberOfRecords=null;
        UnifiedBeanobj.searchKey=null;
        UnifiedBeanobj.isActive= false;
        UnifiedBeanobj.includeHierarchy= false;
        UnifiedBeanobj.isCreditWaterFall= false;
        UnifiedBeanobj.isRiskBasedPricing= false;
        UnifiedBeanobj.filter=null;
        UnifiedBeanobj.writeLogs =null;
        UnifiedBeanobj.isRBPApproved = false;
        UnifiedBeanobj.stage=null;
        UnifiedBeanobj.projectStatusDetail=null;
        UnifiedBeanobj.projectStatus=null;
        UnifiedBeanobj.hasOpenStip = null;
        UnifiedBeanobj.installerOnlyEmail= 'true';
        UnifiedBeanobj.hasInstallerOpenStip= null;
        UnifiedBeanobj.hasCustomerInstallerOpenStip = null;
        UnifiedBeanobj.error= null;
            UnifiedBeanobj.prequal= null;
            UnifiedBeanobj.projects= null;
            UnifiedBeanobj.equipmentManufacturers= null;
            UnifiedBeanobj.projectTypes= null;
            UnifiedBeanobj.products= null;
            UnifiedBeanobj.installerInfo= null;
            UnifiedBeanobj.projectDocuments= null;
            UnifiedBeanobj.users= null;
            UnifiedBeanobj.ratingDetails= null;
            UnifiedBeanobj.finOpsReport= null;
            UnifiedBeanobj.reportType= null;
            UnifiedBeanobj.startDate= null;
            UnifiedBeanobj.endDate= null;
            UnifiedBeanobj.type= null;
            UnifiedBeanobj.total= null;
            UnifiedBeanobj.userId= null;
            UnifiedBeanobj.dataElements= null;
            UnifiedBeanobj.rows= null;
            UnifiedBeanobj.executives= null;
            UnifiedBeanobj.salesreps= null;
            UnifiedBeanobj.managers= null;
            
        UnifiedBean.RowWrapper RW = new UnifiedBean.RowWrapper();
            RW.status = null;
            RW.percentage= null;
            RW.numberOfProjects = 10;
            RW.rank = 1;
        UnifiedBean.UserWrapper uw= new UnifiedBean.UserWrapper();
            uw.userId = null;
            uw.name = null;
            uw.role = null;
            
        UnifiedBean.ManagerWrapper M= new UnifiedBean.ManagerWrapper();
            M.userId= null;
            M.name= null;
            M.role= null;
            M.hasReportsTo= null;
            
        UnifiedBean.OpportunityWrapper  opp= new UnifiedBean.OpportunityWrapper();
            opp.id= null;
            opp.externalId= null;
            opp.isNew = true;
            opp.hashId= null;
            opp.congaId= null;
            opp.ownerEmail= null;
            opp.milestoneRequest= null;
            opp.message= null;
            opp.stage= null;
            opp.createdDate= null;
            opp.nonSolarAmount =1;
            opp.term =1;
            opp.apr =1;
            opp.isACH = true;
            opp.sendLinkEmail =true;
            opp.salesRepresentativeEmail= null;
            opp.salesRepresentativeManagerEmail= null;
            opp.productType= null;
            opp.changeOrderStatus= null;
            opp.projectCategory= null;
            opp.typeOfResidence= null;
            opp.projectTypeOther= null;
            opp.projectType= null;
            opp.requestedLoanAmount =1;
            opp.amountDrawn=1;
            opp.amountRemaining=1;
            opp.loanType= null;
            opp.approvedForPayments=true;
            opp.blockDraw=true;
            opp.maxAvailable=1;
            opp.drawCount= null;
            opp.drawExpired=true;
            opp.financeCharge=1;
            opp.amountFinanced=1;
            opp.totalOfPayments=1;
            opp.monthlyPayment=1;
            opp.noOfPayments=1;
            opp.monthlyPaymentEscalated=1;
                opp.finalPayment=1;
                opp.prepayAmountFinanced=1;
                opp.isSMSAuthorized=true;
                opp.installerCode = null;
                opp.isCreditAuthorized=true;
                opp.projectWithDrawnReason= null;
                opp.projectStatus= null;
                opp.projectStatusDetail= null;
                opp.fundingStatus= null;
                opp.installerName= null;
                opp.installStreet= null;
                opp.installCity= null;
                opp.language= null;
          opp.installZipCode= null;
              opp.installStateName= null;
              opp.errorMessage= null;
              opp.selectedUtilityLseId= null;
              opp.selectedUtilityName= null;
              opp.totalLoanAmount= null;
              opp.m0ApprovalRequestedDate= null;
              opp.m1ApprovalRequestedDate= null;
              opp.m2ApprovalRequestedDate= null;
              opp.ownerName= null;
              opp.batteryAmount= 1;
              opp.promoAPR= 1;
              opp.promoFixedPayment= 1;
              opp.promoBalance= 1;
              opp.promoPercentPayment= 1;
              opp.promoTerm= 1;
              opp.drawPeriod= 1;
          opp.utilityChoices= null;
              opp.applicants= null;
              opp.stips= null;
              opp.contracts= null;
              opp.credits= null;
              opp.systemDesigns= null;
              opp.quotes= null;
              opp.notes= null;
              opp.prequals= null;
              opp.draws= null;
              opp.disclosures= null;
              opp.firstLoanPaymentDate=null;
              opp.envelopeURL=null;
        UnifiedBean.DisclosureWrapper dis= new UnifiedBean.DisclosureWrapper();
        dis.disclosureText = null;
            dis.type =null;
            
        UnifiedBean.ErrorWrapper err= new UnifiedBean.ErrorWrapper();
        err.errorMessage=null;
            err.errorInstance=null;
        UnifiedBean.ErrorInstanceWrapper err1= new UnifiedBean.ErrorInstanceWrapper();
        err1.fieldName=null;
            err1.regexExpression=null;
            err1.errorMessage=null;
            
        UnifiedBean.PrequalWrapper pql= new UnifiedBean.PrequalWrapper();
        pql.id =null;
        pql.firstName=null;
        pql.lastName =null;    
        pql.street =null;
        pql.city=null;
        pql.state=null; 
        pql.zipCode=null; 
        pql.isCreditAuthorized= true;
        pql.annualIncome=1;
        pql.ssn=null;
        pql.dateOfBirth=null;
        pql.decisionDetails=null;
        pql.message=null; 
        pql.errorMessage=null; 
        pql.fniLTReq=null;
        pql.fniLTRes=null;
        pql.status=null;
        pql.lineAmount=1;
        pql.isPrequalAuthorized = true;
        pql.createdDateTime=null;
        pql.decisionDateTime=null;
        pql.stipReason=null;
        pql.products=null; 
        UnifiedBean.UtilityChoicesWrapper uc= new UnifiedBean.UtilityChoicesWrapper();
        uc.lseId=null;
            uc.name=null;
        UnifiedBean.ApplicantWrapper app= new UnifiedBean.ApplicantWrapper();
        app.id=null; 
        app.firstName=null;
        app.lastName =null;
        app.middleInitial=null;
        app.lastFourSSN =null;
        app.ssn =null;
        app.dateOfBirth=null;        
        app.phone=null; 
        app.otherPhone=null; 
        app.email=null;
        app.errorMessage=null; 
        app.mailingStreet=null; 
        app.mailingCity=null; 
        app.mailingStateName=null; 
        app.mailingZipCode=null; 
        app.residenceStreet =null;     
        app.residenceCity=null; 
        app.residenceStateName =null;
        app.residenceZipCode=null; 
        app.jobTitle=null;
        app.employerName=null; 
        app.isPrimary =true;
        app.isCreditRun=true;
        app.isSMSAuthorized=true;
        app.employmentMonths=1;
        app.employmentYears=1; 
        app.annualIncome =1;
        UnifiedBean.StipsAndStatusWrapper st= new UnifiedBean.StipsAndStatusWrapper();
        st.id=null;
        st.name=null; 
        st.status=null; 
        st.description=null; 
        st.completedDate=null;
        st.notes=null;
        st.uploadInstruction=null;
        st.displayFolderText=null; 
        st.folderName=null; 
        st.fileName=null; 
        st.modifiedAt=null; 
        st.documentURL=null; 
        st.errorMessage=null;
        st.installerOnlyEmail =true;
        UnifiedBean.DocumentWrapper dc = new UnifiedBean.DocumentWrapper();
        dc.id=null;
        dc.sent=null;
        dc.subject=null;
        dc.completedDateTime=null;
        dc.cancelReason=null;
        dc.status=null;
        dc.errorMessage=null;
        UnifiedBean.CreditsWrapper cr= new UnifiedBean.CreditsWrapper();
        cr.id = null;
        cr.name= null;
        cr.creditDecisionDate= null;         
        cr.status= null; 
        
        cr.fniSTReq= null;
        cr.fniLTReq= null;
        cr.hashId = null;
        cr.message= null;
        
        cr.productId= null;
        cr.productName= null;
        cr.applicantId= null;
        cr.applicantFirstName= null;
        cr.applicantLastName= null;
        cr.coApplicantId= null;
        cr.coApplicantFirstName= null;
        cr.coApplicantLastName= null;
        cr.creditExpirationDate= null;
        cr.creditExtensionRequestDate= null;
        cr.creditExtensionStatus= null;
        cr.creditExtensionStatusDate= null;      
        
        cr.errorMessage = null;
      
        cr.ltStipRsn1= null; 
        cr.ltStipRsn2 = null;
        cr.ltStipRsn3 = null;
        cr.ltStipRsn4 = null;
        cr.ltFNIDecision= null; 
        cr.fniReferenceNumber= null; 
        cr.fniPostBackRequest= null; 
        cr.stipText= null;
        
        cr.clPrimaryApplicantFirstName= null;
        cr.clPrimaryApplicantLastName= null;
        cr.clPrimaryApplicantMiddleName= null;
        cr.clPrimaryApplicantSSN= null;
        cr.clPrimaryApplicantDOB= null;
        
        cr.clCoApplicantFirstName= null;
        cr.clCoApplicantLastName= null;
        cr.clCoApplicantMiddleName= null;
        cr.clCoApplicantSSN= null;
        cr.clCoApplicantDOB= null;
        
        cr.clOwnerOfRecord= null;
        cr.clStreetAddress= null;
        cr.clCity= null;
        cr.clZipCode= null;
        cr.clStateCode= null;
        cr.fniSoftResp= null;
        cr.stipReason = null;
        
        cr.isSyncing = true;
        cr.isExtended= true;
        cr.maxApprovedAmount = 1;
        cr.productMaxLoanAmount= 1; 
        cr.productMinLoanAmount= 1;
        UnifiedBean.SystemDesignWrapper sy= new UnifiedBean.SystemDesignWrapper();
        sy.id =null;
        sy.azimuth=null;
        sy.moduleMake=null;        
        sy.moduleModel=null;
        sy.moduleCount=null;
        sy.inverterMake =null;
        sy.inverterModel=null;
        sy.inverterCount=null;
        sy.estAnnualProductionkwh=null; 
        sy.tilt=null; 
        sy.systemSize =null;
        sy.systemSizeAc=null;
        sy.batteryCapacity=null;
        sy.batteryCount =null;       
        sy.batteryMake=null;
        sy.batteryModel=null;
        
        sy.waterheaterMake=null; 
        sy.waterheaterMakeName=null; 
        sy.waterheaterCount=null;
        sy.waterheaterModel=null;
        sy.groundMountManufacturer=null; 
        sy.groundMountType=null;
        sy.taxLiabilityPercentageCap=null;
        sy.isLowIncome=null; 
        sy.locallyManufacturedComponents =null;
        sy.rmIdLocalContractorAndManufacturer=null; 
        sy.systemCostMinusIncentives=null; 
        sy.recRetained=null;
        sy.projectType=null;
        sy.systemPayments=null;
        sy.errorMessage=null; 
        sy.moduleMakeName=null; 
        sy.inverterMakeName=null; 
        sy.batteryMakeName=null; 
        sy.groundMountManufacturerName=null; 
        sy.requiredIncentiveFields=null;
        UnifiedBean.QuotesWrapper qo= new UnifiedBean.QuotesWrapper();
        qo.id=null; 
        qo.productId=null;
        qo.productName=null;
        qo.productType=null; 
        qo.expirationDate=null;  
        qo.creditApprovalStatus=null;
        qo.systemDesignId=null;
        qo.isSyncing=null;
        qo.quoteNumber=null;
        qo.createdDateTime=null;
        qo.upfrontPayment =null;        
        qo.nonSolarAmount =null;
        qo.systemCost=null; 
        qo.loanAmount=null; 
        qo.finalMonthlyPayment =null;
        qo.finalEscalatedMonthlyPayment=null; 
        qo.monthlyPayment=null; 
        qo.escalatedMonthlyPayment =null;
        qo.warningMessage =null;
        qo.infoMessages =null;
        qo.errorMessage =null;
        qo.batteryAmount =null;
        qo.approvingCredits=null;
        qo.amortizationTable =null;
        qo.state=null;
        qo.term=null;   
        qo.apr=null; 
        qo.isACH =null;
        qo.promoAPR=null;
        qo.promoTerm =null;
        qo.promoFixedPayment  =null;
        qo.promoBalance =null;
        qo.promoPercentPayment=null;
        qo.drawPeriod  =null;
        qo.paymentCountPromo =null;
        qo.paymentCountStandard =null;
        UnifiedBean.InfoMessageWrapper im = new  UnifiedBean.InfoMessageWrapper();
        im.header= null;
        im.message = null;
        UnifiedBean.NotesWrapper no= new UnifiedBean.NotesWrapper();
        no.id=null; 
        no.title=null;
        no.body=null;
        no.createdBy=null;
        no.lastModifiedDate=null;
        UnifiedBean.EquipmentWrapper eq= new UnifiedBean.EquipmentWrapper();
        eq.id=null; 
        eq.manufacturer=null;
        eq.manufacturerType =null;        
        eq.isActive=null; 
        eq.activationDate=null; 
        eq.endDate =null;
        UnifiedBean.ProjectTypeWrapper pro= new UnifiedBean.ProjectTypeWrapper();
         pro.id=null; 
        pro.name=null;
        pro.projectCategory =null;
        pro.isActive=null; 
        UnifiedBean.ProductWrapper prod= new UnifiedBean.ProductWrapper();
        prod.id=null; 
        prod.name=null;
        prod.loanType =null; 
        prod.tier =null;       
        prod.term=null; 
        prod.apr=null; 
        prod.isACH =null;
       
        prod.stateName =null;
        prod.productMaxLoanAmount =null;
        prod.productType =null;
        prod.productMinLoanAmount =null;
        prod.productLoanType =null;
        prod.productDisclosure =null; 
        prod.interestType =null; 
        prod.uiName =null; 
        prod.ShowTiers =null; 
       
        prod.promoAPR =null; 
        prod.promoFixedPayment =null;
        prod.promoBalance =null;
        prod.promoPercentPayment =null;
        prod.promoTerm =null;
        prod.drawPeriod =null;  
        
        prod.projectCategory =null;
        prod.loanAmount =null; 
        prod.finalMonthlyPayment =null;
        prod.finalEscalatedMonthlyPayment=null; 
        prod.monthlyPayment=null; 
        prod.escalatedMonthlyPayment =null;
        prod.paymentCountPromo   =null;
        prod.paymentCountStandard   =null;
        UnifiedBean.InstallerInfoWrapper ins = new UnifiedBean.InstallerInfoWrapper();
        ins.errorMessage=null; 
        ins.name=null;
        ins.milestoneFilter=null;
        ins.m0RequiredDocuments =null;        
        ins.m1RequiredDocuments=null; 
        ins.m2RequiredDocuments=null;
        ins.hiRequiredDocuments=null;
        ins.rmFullName=null;
        ins.rmEmail=null;
        ins.rmWorkNumber=null;
        ins.rmFullPhotoUrl=null;
        ins.isPrequalEnabled=null;
        ins.isWaterfallEnabled=null;
        ins.isRBPEnabled=null;
        ins.isLast4SSNEnabled =null;
        ins.isLoanAgreementSuppressed=null;
        ins.eligibleForHWH=null;
        ins.isSolarEnabled =null; 
        ins.isHomeEnabled =null;
        UnifiedBean.RequiredDocumentsWrapper req= new UnifiedBean.RequiredDocumentsWrapper();
        req.errorMessage=null; 
        req.installationContract=null;
        req.loanAgreements =null;        
        req.finalDesigns =null;
        UnifiedBean.ProjectDocumentWrapper proj= new UnifiedBean.ProjectDocumentWrapper();
        proj.errorMessage=null; 
        proj.projectId=null;
        proj.externalId =null;    
        proj.isAllM0RequiredDocs =null;
        proj.isAllM1RequiredDocs =null;
        proj.isAllM2RequiredDocs =null; 
        proj.isAllHIRequiredDocs =null;
       proj.folders=null;
        UnifiedBean.UsersWrapper user1= new UnifiedBean.UsersWrapper();
        user1.newOwnerId=null;
        user1.newOwnerHashId=null;
        user1.newOwnerEmail=null;
        user1.userName=null; 
        user1.firstName=null;
        user1.lastName=null;
        user1.hashId=null; 
        user1.installerName=null;
        user1.photoURL=null;
        user1.id=null; 
        user1.name=null; 
        user1.email=null;
        user1.phone=null;
        user1.newPassword=null;                              
        user1.role=null;
        user1.lastLogin=null;
        user1.loginCount=null;
        user1.isActive=null;
        user1.reportsTo=null; 
        user1.profile=null; 
        user1.title=null;
        user1.contactid=null;
        user1.isTrainingCompleted=null;
        user1.trainingDocLink=null;
        
        
        user1.isInstallerReportEnabled=null;
        user1.isNetReportEnabled=null;
        user1.isRemittanceReportEnabled=null;
        UnifiedBean.FolderWrapper fol= new UnifiedBean.FolderWrapper();
        fol.folderName=null;
            fol.milestone=null;
            fol.files=null;
            
        UnifiedBean.FileWrapper file= new UnifiedBean.FileWrapper();
        file.fileId=null;
        file.fileName=null; 
        file.modifiedAt=null;
        file.createdBy =null;
        UnifiedBean.RatingsWrapper rat= new UnifiedBean.RatingsWrapper();
        rat.rating=null;
        rat.comments=null;
        rat.source=null;
        rat.noThanks=null;
        
        UnifiedBean.DrawRequestWrapper draw= new UnifiedBean.DrawRequestWrapper();
        draw.drawLevel=null;
        draw.amount=null;
        draw.isProjectComplete=null;
        draw.requestDate=null;
        draw.status=null;
        draw.approvalDate=null;
        draw.drawMethodology=null;
        draw.declineDate=null;
        draw.hashId=null;
        UnifiedBean.FinOpsReportWrapper fin= new UnifiedBean.FinOpsReportWrapper();
        fin.installerName=null;
        fin.remmitanceFolderId=null;
        fin.netOutFolderId=null;
        fin.installerFolderId=null;
        fin.boxAccessToken=null;

        
        
    }
}