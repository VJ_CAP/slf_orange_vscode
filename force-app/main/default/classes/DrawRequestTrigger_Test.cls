@isTest
public class DrawRequestTrigger_Test
{
    static testMethod void myInsrtMthd() 
    {
        TriggerFlags__c checkFlag = new TriggerFlags__c(Name='Account',isActive__c=True);
        insert checkFlag;
        
        TriggerFlags__c checkFlg = new TriggerFlags__c(Name='Opportunity',isActive__c=True);
        insert checkFlg;
        
        TriggerFlags__c chckFlg = new TriggerFlags__c(Name='Underwriting_File__c',isActive__c=True);
        insert chckFlg;
        
        TriggerFlags__c quoteFlg = new TriggerFlags__c(Name='Quote',isActive__c=True);
        insert quoteFlg;
        
        TriggerFlags__c systemDesignFlg = new TriggerFlags__c(Name='System_Design__c',isActive__c=True);
        insert systemDesignFlg;
        
        TriggerFlags__c chkFlg = new TriggerFlags__c(Name='Draw_Requests__c',isActive__c=True);
        insert chkFlg;
        
        TriggerFlags__c fundingPackg= new TriggerFlags__c(Name='Funding_Data__c',isActive__c=True);
        insert fundingPackg;
        
        Account accObj = new Account(name='Test Account',Solar_Enabled__c=True);
        insert accObj;
        
        Product__c prod = new Product__c();
            prod.Installer_Account__c =accObj.id;
            prod.Long_Term_Facility_Lookup__c =accObj.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            insert prod;
            
        
        Opportunity oppObj = new Opportunity(Name='OLX',AccountId=accObj.id,StageName='Qualified',CloseDate = Date.parse('01/18/2019'));
        insert oppObj;
        
            //Insert System Design Record
            System_Design__c sysDesignRec = new System_Design__c();
            //sysDesignRec.Name = 'Test Sys';
            sysDesignRec.System_Cost__c = 11;
            sysDesignRec.Opportunity__c = oppObj.Id;
            sysDesignRec.Est_Annual_Production_kWh__c = 10;
            sysDesignRec.Inverter_Count__c = 11;
            //  sysDesignRec.Inverter_Make__c = '11';
            sysDesignRec.Inverter_Model__c = '12';
            //sysDesignRec.Inverter__c = 'TBD';
            sysDesignRec.Module_Count__c = 10;
            sysDesignRec.Module_Model__c = '11';
            //sysDesignRec.System_Size_STC_kW__c = 12;
            insert sysDesignRec;
        
        Quote quoteRec = new Quote();
        quoteRec.Name = 'Test Quote';
        quoteRec.Opportunityid = oppObj.id;
        quoteRec.SLF_Product__c = prod.Id ;
        quoteRec.System_Design__c= sysDesignRec.id;
        insert quoteRec;
                      
        Opportunity opp = [select id from opportunity where name = 'OLX'];
        opp.SyncedQuoteid = quoteRec.id;
        update opp;
                
        Underwriting_File__c undrObj = new Underwriting_File__c(Name='ABC',Opportunity__c=oppObj.Id);
        insert undrObj;
                
        Draw_Requests__c drawObj = new Draw_Requests__c(Amount__c= 10000,Underwriting__c=undrObj.id,Status__c = 'Requested',Approval_Date__c=System.now(),Primary_Applicant_Email__c = 'test@gmail.com');
        insert drawObj;
        
        drawObj.Amount__c=1000;
        drawObj.Project_Complete_Certification__c = true;
        drawObj.Status__c = 'Approved';
        update drawObj;
        
        
        List<Payment_Schedule_Max_Amount__c> paymentList = new List<Payment_Schedule_Max_Amount__c>();
        Payment_Schedule_Max_Amount__c paymentScMARec = new Payment_Schedule_Max_Amount__c();
        paymentScMARec.Amount__c = 1000;
        paymentScMARec.Months__c = 3;
        paymentScMARec.Quote__c = quoteRec.id;
        paymentScMARec.Serial_No__c = 1;
        paymentList.add(paymentScMARec);
        insert paymentList;
        system.debug('*********Payment Record*******'+paymentScMARec);
        system.debug('*********Payment List*********'+paymentList);
        
        system.debug('***** Is Primary Quote Check'+ opp);   
        
        drawObj.Project_Complete_Certification__c = false;
        drawObj.SLF_Approved__c = System.now();
        update drawObj;
        
        List<Payment_Schedule_Max_Amount__c> paymentList1 = new List<Payment_Schedule_Max_Amount__c>();
        Payment_Schedule_Max_Amount__c paymentScMARec1 = new Payment_Schedule_Max_Amount__c();
        paymentScMARec1.Amount__c = 1000;
        paymentScMARec1.Months__c = 3;
        paymentScMARec1.Quote__c = quoteRec.id;
        paymentScMARec1.Serial_No__c = 2;
        paymentList1.add(paymentScMARec1);
        insert paymentList1;
        system.debug('*********Payment Record*******'+paymentScMARec1);
        system.debug('*********Payment List*********'+paymentList1);
        
        drawObj.Project_Complete_Certification__c = true;
        update drawObj;
         
    }
}