@isTest(seeAllData=false)
public class Batch_CreateInSightenTest {
	private testMethod static void Batch_CreateInSightenTest(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();       
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);       
        insert trgLst;
        
        Account a1 = new Account (Name = 'Solar City', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true);
        insert a1;
        
        Account a2 = new Account (firstName = 'Test customer', LastName = 'Test customer', PersonEmail = 'abc@gmail.com', External_comments__c = 'Value 1',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true);
        insert a2;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = a1.Id);
        insert con;  
        
        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                        FNI_Min_Response_Code__c = 1, 
                        Installer_Account__c = a1.Id, 
                        Qualification_Message__c = 'test', 
                        State_Code__c = 'CA', 
                        Is_Active__c = true,
                        Product_Display_Name__c = 'Test',
                        Product_Tier__c = '0');
        insert p1;
        
        RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'Customer_Opportunity_Record_Type' LIMIT 1];

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                           AccountId = a2.Id,
                           RecordTypeId = rt.Id,
                           Installer_Account__c = a1.Id,
                           Install_State_Code__c = 'CA',
                           SLF_Product__c = p1.Id,
                           Upload_Comment_Text__c = 'Value 2',
                           StageName = 'Closed Won',
                           Install_Street__c = 'Rockwell',
                           Install_City__c = 'Connecticut',  
                           Install_Postal_Code__c  = '06107',             
                           CloseDate = System.today().addDays(30),
                           Create_in_Sighten__c = true);
        insert o1;
        
        Map<Id,String> sigtnTknMap=new Map<Id,String>();
        sigtnTknMap.put('1','test');
        
        Test.startTest();
        try{
        Batch_CreateInSighten batchCrtSigtnObj = new Batch_CreateInSighten(new Set<Id>{o1.Id});
        Database.executeBatch(batchCrtSigtnObj);
        }catch(Exception e){}
        
        try{
        Batch_CreateInSighten batchCrtSigtnObj2 = new Batch_CreateInSighten(new Set<Id>{o1.Id},new Map<Id,String>());
        Database.executeBatch(batchCrtSigtnObj2);
        }catch(Exception e){}
        
        try{
        String sch = '20 30 8 10 2 ?';
        Batch_CreateInSighten batchCrtSigtnObj3 = new Batch_CreateInSighten(new Set<Id>{o1.Id},new Map<Id,String>());
        System.schedule('Test Job', sch, batchCrtSigtnObj3);
        }catch(Exception e){}
        
         try{
        String sch = '20 30 8 10 2 ?';
        Batch_CreateInSighten batchCrtSigtnObj3 = new Batch_CreateInSighten(new Set<Id>{o1.Id},sigtnTknMap);
        System.schedule('Test Job', sch, batchCrtSigtnObj3);
        }catch(Exception e){}
        Test.stopTest();
        
    }
}