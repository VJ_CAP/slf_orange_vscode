@istest
public class FundingPackageServiceTest {
     @testsetup static void createtestdata(){
        //SLFUtilityDataSetup.initData();
        insertUserData();
    }
     private static void insertUserData(){
         //get Custom setting data.
        SLFUtilityTest.createCustomSettings();   
        
        //get the person account record type
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        Case caseTest = new Case();
        caseTest.Status = 'New';
        caseTest.Origin = 'Email';
        insert caseTest;
        
        Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.Push_Endpoint__c ='www.test.com';
        acc.PersonEmail = 'slfaccount@slf.com';
        acc.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
        acc.Installer_Email__c = 'slfaccount@slf.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Subscribe_to_Push_Updates__c = true;
        acc.Subscribe_to_Event_Updates__c=true;
        acc.SMS_Opt_in__c=true;
        insert acc;
        
        Contact cObj = new Contact(LastName = 'TestCon',Email='test@gmail.com');
        insert cObj;
        
        Product__c prodct = new Product__c();
        prodct.ACH__c = false;
        prodct.APR__c = 4.99;
        prodct.bump__c = false;
        prodct.Installer_Account__c = acc.Id;
        prodct.Internal_Use_Only__c = false;
        prodct.Is_Active__c = true;
        prodct.Long_Term_Facility__c = 'TCU';
        prodct.LT_Max_Loan_Amount__c =  100000.0;
        prodct.Max_Loan_Amount__c = 100000.0;
        prodct.State_Code__c = 'CA';
        prodct.FNI_Min_Response_Code__c = 10;
        prodct.Product_Display_Name__c = 'Test';
        prodct.Qualification_Message__c = '123';
        prodct.Product_Tier__c = '0';
        insert prodct;
        
         Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 

        Account newPersonAccount = new Account();
         newPersonAccount.FirstName = 'Fred';
         newPersonAccount.MiddleName = 'T';
         newPersonAccount.LastName = 'Smith';
         newPersonAccount.RecordTypeId =  recTypeInfoMap.get('Person Account').getRecordTypeId();
         insert newPersonAccount;

        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = newPersonAccount.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Postal_Code__c = '94901';
        opp.Install_Street__c = '39 GLEN AVE';
        opp.Install_City__c = 'SAN RAFAEL';
        opp.SLF_Product__c = prodct.Id;
        opp.Combined_Loan_Amount__c = 10000;
        opp.Long_Term_Loan_Amount_Manual__c = 20000;
        opp.Co_Applicant__c = newPersonAccount.Id;
        opp.Project_Category__c='Solar';
        insert opp;
      
        //Insert Quote Record
        List<Quote> quoteList = new List<Quote>();
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = opp.id;
        quoteRec.SLF_Product__c = prodct.Id ;
        quoteRec.Monthly_Payment__c = 1000;
        quoteRec.Combined_Loan_Amount__c = 9999;
        quoteRec.includesBattery__c=true;
        //quoteRec.Accountid =acc.id;
        quoteList.add(quoteRec);
        
        Quote quoteRecOne = new Quote();
        quoteRecOne.Name ='Test quote';
        quoteRecOne.Opportunityid = opp.id;
        quoteRecOne.SLF_Product__c = prodct.Id ;
        quoteRecOne.Monthly_Payment__c = 1000;
        quoteRecOne.Combined_Loan_Amount__c = 999;

        //quoteRec.Accountid =acc.id;
        quoteList.add(quoteRecOne);
        insert quoteList;
        
        opp.SyncedQuoteId = quoteRec.Id;

        update opp;        
        
        
            Underwriting_File__c uwf = new Underwriting_File__c(
            Send_Confirmation_Email__c = true,
            Opportunity__c = opp.Id,
            M1_Approval_Requested_Date__c=date.Today()
        );
             insert uwf;

       List<Funding_Data__c> fundList = new List<Funding_Data__c>();
       Funding_Data__c fundData = new Funding_Data__c();
       fundData.Underwriting_ID__c = uwf.id;
       
       insert fundData;
       
       M1_Automation__c m1CustomSetting = new M1_Automation__c(Name='1',
        Skip_Markers__c=false,
        Start_Marker__c='TestStart',
        isRequired__c=true,
        Folder_Name__c='Emails',
        File_Filter__c='Largest',
        Facility__c='TCU',
        End_Marker__c='TestEnd',
        EDW_Originated__c=false,
        Dest_File_Name__c='<OpportunityID> - Membership Form.pdf',
        Data_Room__c='123456');

       insert m1CustomSetting;

       SFSettings__c slfencrypt = new SFSettings__c();
            slfencrypt.Encrypt_Private_Key__c = 'SLFOrange';
            slfencrypt.Name = 'PrivateKey';
            slfencrypt.Funding_Package_End_Point__c='www.test.com';
            insert slfencrypt;
     }

    Public testmethod static void getFundingPackageDataTest(){  
    Opportunity opp=[SELECT id,Combined_Loan_Amount__c,Long_Term_Loan_Amount_Manual__c,AccountId,Co_Applicant__c FROM Opportunity WHERE name='OpName'];
           
    test.starttest();
    FundingPackageService.getFundingPackageData(opp.ID,False);
    test.stopTest();
        
    }
    
    
    
}