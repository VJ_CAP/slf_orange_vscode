/**
* Description: RevertChangeOrder related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           10/17/2017          Created
******************************************************************************************/
public with sharing class RevertChangeOrderServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + RevertChangeOrderServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        RevertChangeOrderDataServiceImpl revertChangeOrderDataSrvImpl = new RevertChangeOrderDataServiceImpl(); 
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            res.response = revertChangeOrderDataSrvImpl.transformOutput(reqData);
        }catch(Exception err){
           system.debug('### RevertChangeOrderServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           
           ErrorLogUtility.writeLog(' RevertChangeOrderServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + RevertChangeOrderServiceImpl.class);
        return res.response ;
    }
    
    
}