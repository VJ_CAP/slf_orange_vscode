/**
* Description: This Batch will update existing opportunity record's project category value to Solar   
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Brahmeswar Gajula      01/17/2019          Created
******************************************************************************************/
global class OppShareToParentInstallerAccountBatch implements Database.Batchable<sObject>{
     String strQuery = '';
     list<string> lstoppids = new list<string>();
     global OppShareToParentInstallerAccountBatch(List<String> Oppid){
        if(Oppid != null && Oppid.size()>0){
            lstoppids = Oppid;
            strQuery = 'select id,Project_Category__c from Opportunity where id IN:lstoppids';
        }else
           strQuery = 'select id,Project_Category__c from Opportunity'; 
          
     }
     
     global Database.QueryLocator start(Database.BatchableContext BC)
     {
     system.debug('***strQuery in start---'+strQuery);
     return Database.getQueryLocator(strQuery);
    
     }
    global void execute(Database.BatchableContext BC,List<Opportunity> lstOpps)
    {   
        System.debug('*******lstOpps-----'+lstOpps);    
        List<Opportunity> lstOppforUpdate = new List<Opportunity>();
           if(lstOpps != null && lstOpps.size()>0){
               for(opportunity obj : lstOpps){
                   obj.Project_Category__c  ='Solar';
                   System.debug('*******obj-----'+obj);
                   lstOppforUpdate.add(obj);
               }
               
           }
           System.debug('*******lstOppforUpdate-----'+lstOppforUpdate);
        if(lstOppforUpdate != null && lstOppforUpdate.size()>0){
            try{
                System.debug('*******lstOppforUpdate-----'+lstOppforUpdate);
                database.update(lstOppforUpdate);
            }catch(Exception ex){
                System.debug('*******Error -----'+ex);
            }
        }
           
    }
    global void finish(Database.BatchableContext BC)

    {
        System.debug('******* Existing Opportunity Record update is completed -----');
    }
    
    
   
    
    
    
}