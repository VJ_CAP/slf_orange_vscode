/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkat               03/27/2019              Created
******************************************************************************************/
public class M1AutomationWrapper {
    
	public String projectID;
	public String boxToken;
	public String folderID;
	public String updateURL;
	public String dataRoom;
	public String customerFirstName;
	public String customerLastName;
	public String installerName;
	public String customerStreetAddress;
	public String dayFolder;
	public List<FolderWrapper> folders;
    
    public class FolderWrapper {
		public String folderName;
		public Boolean skipMarkers;
		public Boolean isRequired;
		public String fileFilter;
		public List<DestinationFileWrapper> destinationFiles;
	}
    
	public class DestinationFileWrapper {
		public String destFileName;
		public String startMarker;
		public String endMarker;
	}

}