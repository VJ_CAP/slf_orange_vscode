/**
* Description: ObjectBean class to marshall and unmarshall request and response.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/

public class ObjectBean implements IRestRequest, IRestResponse {
   
    public List<Sobject> result{get;set;}
    
}