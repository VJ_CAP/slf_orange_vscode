/******************************************************************************************
* Description: Test class to cover UnderwritingQueueController Services 
*
******************************************************************************************/
@isTest
public class UnderwritingQueueControllerTest{

    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
    }
    private testMethod static void UnderwritingQueueControllerTest_1(){
      
        Opportunity opp = [select Id,Name,Opportunity__c,Install_Street__c,Has_Open_Stip_M0__c,Project_Category__c,Synced_Credit_Id__c from opportunity where name = 'opName' LIMIT 1]; 
        
        Underwriting_File__c UnderFile = [SELECT Id,Project_Status__c,Opportunity__c FROM Underwriting_File__c where Opportunity__c =: opp.Id  LIMIT 1];
        
        Stipulation_Data__c  sd1 = new Stipulation_Data__c(Name = 'VOI',Suppress_In_Portal__c=true, Email_Text__c = 'Test data', POS_Message__c = 'test', Review_Classification__c = 'SLS I; SLS II');
        insert sd1;
        
        system.debug('@@@stipData:'+sd1);
        Stipulation__c stp1 = new Stipulation__c();
        stp1.Status__c ='Documents Received';
        stp1.Stipulation_Data__c = sd1.id;
        stp1.Review_Start_Date_Time__c = null;
        stp1.Underwriting__c =UnderFile.id;
        insert stp1;
        
        stp1.Status__c ='Documents Received';
        update stp1;
        
        opp.Project_Category__c = 'Home';
        opp.Has_Open_Stip_M0__c = false;
        update opp;
        
        Box_Fields__c boxfields = new  Box_Fields__c();
        boxfields.Underwriting__c =  UnderFile.id;
        boxfields.Latest_LT_Loan_Agreement_Date_Time__c = system.now();
        boxfields.Latest_Install_Contract_Date_Time__c =system.now();
        insert boxfields;
        
        Task objTask = new Task();
        objTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Underwriting Task').getRecordTypeId();
        objTask.WhatId = UnderFile.id;//strUnWrId;
        objTask.OwnerId = UserInfo.getUserId();
        objTask.Status = 'Open';
        objTask.Subject = UnderFile.Project_Status__c + 'Review' ;
        objTask.ActivityDate = system.today();
        objTask.Start_Date_Time__c = system.now();
        objTask.IsReminderSet = true;
        objTask.Type = 'Post-NTP Issue';
        insert objTask;
        
        Task taskM1Review = new Task();
        taskM1Review.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Underwriting Task').getRecordTypeId();
        taskM1Review.WhatId = UnderFile.id;//strUnWrId;
        taskM1Review.OwnerId = UserInfo.getUserId();
        taskM1Review.Status = 'Complete';
        taskM1Review.Subject = UnderFile.Project_Status__c + 'Review' ;
        taskM1Review.ActivityDate = system.today();
        taskM1Review.Start_Date_Time__c = system.now();
        taskM1Review.IsReminderSet = true;
        taskM1Review.Type = 'M1 Review';
        insert taskM1Review;
        
        Draw_Requests__c drawReq = new Draw_Requests__c(Amount__c= 10000,Primary_Applicant_Email__c='adithya.kousika@capgemini.com',Underwriting__c=UnderFile.id,Status__c = 'Approved');
        insert drawReq; 
        Test.startTest(); 
        
        UnderwritingQueueController.getM0StipQueueData(true,true);
        UnderwritingQueueController.getM0StipQueueData(false,true);
        UnderwritingQueueController.getM0StipQueueData(true,false);
        
        UnderwritingQueueController.createUWTask(UnderFile.id,'slfops');
        UnderwritingQueueController.createUWTask(UnderFile.id,'m0stip');
        UnderwritingQueueController.createUWTask(UnderFile.id,'postntpstip');
        UnderwritingQueueController.createUWTask(UnderFile.id,'hifunding');
        
        UnderwritingQueueController.getSLFOpsQueueData();
        UnderwritingQueueController.getSLFOpsQueueCount();
        UnderwritingQueueController.getM1QueueData();
        UnderwritingQueueController.getM1QueueCount();
        UnderwritingQueueController.getM2QueueData();
        UnderwritingQueueController.getM2QueueCount();
        UnderwritingQueueController.getM0StipQueueCount();
        
        UnderFile.M1_Approval_Requested_Date__c = Date.Today();
        UnderFile.Approved_for_Payments__c = true;
        update UnderFile;
        
        stp1.Status__c ='Documents Received';
        update stp1;
        
        UnderwritingQueueController.getPostNtpStipQueueData();
        UnderwritingQueueController.getPostNtpStipQueueCount();
        
        objTask.Type = 'M1 Review'; 
        update objTask;
        
        
        UnderwritingQueueController.getHIFundingQueueData();
        UnderwritingQueueController.getHIFundingQueueCount();
        //For Searchable
        List<Id> fixedSearchResults = new List<Id>();
        fixedSearchResults.add(UnderFile.Id);
        fixedSearchResults.add(opp.Id);
        Test.setFixedSearchResults(fixedSearchResults);
        UnderwritingQueueController.getSearchData('Test');
        UnderwritingQueueController.getQueueCounts(true);
        UnderwritingQueueController.getQueueCounts(false);
        
        Test.stopTest();
    }
}