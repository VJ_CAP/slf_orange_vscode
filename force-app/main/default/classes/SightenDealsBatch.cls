global class SightenDealsBatch implements Database.Batchable<sObject>{
	global Set<Id> oppIdSet = new Set<Id>();
    global String query;
    global SightenDealsBatch(Set<id> idSet){
    	query = 'Select id, Name, Hash_Id__c from Opportunity where (Hash_Id__c = null OR Hash_Id__c = \'\') AND StageName != \'Archive\'';
        if(idSet != null && !idSet.isEmpty()){
            oppIdSet.addAll(idSet);
            query += ' and id in :oppIdSet';
        }  
        System.debug('oppIdSet:::'+oppIdSet); 
        System.debug('query:::'+query);    
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> oppList){
        if(!oppList.isEmpty()){
            for(Opportunity optyObj : oppList){
            String hashid = SLFUtility.getHashId(optyObj.id);
            system.debug('### hashid'+hashid);
            optyObj.Hash_Id__c = hashid;
        }               
        update oppList;
        }        
    }

    global void finish(Database.BatchableContext BC){
    }
}