@isTest
public class Batch_FetchMilestones_test {
    @testSetup static void setUpData(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgAccflagDis = new TriggerFlags__c();
        trgAccflagDis.Name ='Disclosures__c';
        trgAccflagDis.isActive__c =true;
        trgLst.add(trgAccflagDis);
        
        TriggerFlags__c trgAccflagTsk = new TriggerFlags__c();
        trgAccflagTsk.Name ='Task';
        trgAccflagTsk.isActive__c =true;
        trgLst.add(trgAccflagTsk);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst;
        
        /* List<SF_Category_Map__c> catList = new List<SF_Category_Map__c>();
SF_Category_Map__c sfmap = new SF_Category_Map__c();
sfmap.Name = 'Loan Agreement';
sfmap.Folder_Name__c = 'Loan Agreements';
sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'LT_Loan_Agreement_Received__c';
catList.add(sfmap);

SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
catList.add(cm1);
insert sfmap;
*/
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        system.debug('@@ I am here' +portalUser.Contact.AccountId);
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        //update acc;
        
        
        AccountShare accShare = new AccountShare(); //a new empty AccountShare object
        accShare.userorgroupid = portalUser.Id;
        accShare.accountid = con.accountid;
        accShare.accountaccesslevel = 'Edit';
        accShare.OpportunityAccessLevel = 'None';
        accShare.CaseAccessLevel = 'None';
        insert accShare;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact personcon = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = personAcc.Id);
        insert personcon; 
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        personAcc.PersonBirthdate = Date.ValueOf('1929-03-10');
        update personAcc;
        
        AccountShare personAccShare = new AccountShare(); //a new empty AccountShare object
        personAccShare.userorgroupid = portalUser.Id;
        personAccShare.accountid = personAcc.Id;
        personAccShare.accountaccesslevel = 'Edit';
        personAccShare.OpportunityAccessLevel = 'None';
        personAccShare.CaseAccessLevel = 'None';
        insert personAccShare;
        
        //Added by Suresh Kumar to cover Orange-1606 
        List<Draw_Allocation__c> lstDrawAllocInst = new List<Draw_Allocation__c>();
        Draw_Allocation__c drawAlloc = new Draw_Allocation__c();
        drawAlloc.Account__c = acc.id;
        drawAlloc.Level__c = '1';
        drawAlloc.Draw_Allocation__c = 50;
        lstDrawAllocInst.add(drawAlloc);
        
        Draw_Allocation__c drawAlloc1 = new Draw_Allocation__c();
        drawAlloc1.Account__c = acc.id;
        drawAlloc1.Level__c = '2';
        drawAlloc1.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc1);
        
        Draw_Allocation__c drawAlloc2 = new Draw_Allocation__c();
        drawAlloc2.Account__c = acc.id;
        drawAlloc2.Level__c = '3';
        drawAlloc2.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc2);
        
        insert lstDrawAllocInst;
        //End
        
        System.runAs (portalUser)
        { 
            Document doc1 = new Document();
            doc1.Body = Blob.valueOf('Some Text');
            doc1.ContentType = 'application/pdf';
            doc1.DeveloperName = 'my_document';
            doc1.IsPublic = true;
            doc1.Name = 'Sunlight Logo';
            doc1.FolderId = [select id from folder where name = 'Shared Documents'].id;
            insert doc1;
            
            //Insert SLF Product Record
            List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            //insert prod;
            prodList.add(prod);
            
            //Inserting duplicate SLF Product Records
            Product__c prod1 = new Product__c();
            prod1.Installer_Account__c =acc.id;
            prod1.Long_Term_Facility_Lookup__c =acc.id;
            prod1.Name='testprod';
            prod1.FNI_Min_Response_Code__c=9;
            prod1.Product_Display_Name__c='test';
            prod1.Qualification_Message__c ='testmsg';
            prod1.Product_Loan_Type__c='Solar';
            prod1.State_Code__c ='CA';
            prod1.ST_APR__c = 2.99;
            prod1.Long_Term_Facility__c = 'TCU';
            prod1.APR__c = 2.99;
            prod1.ACH__c = true;
            prod1.Internal_Use_Only__c = true;
            prod1.LT_Max_Loan_Amount__c =1000;
            prod1.Term_mo__c = 144;
            prod1.Product_Tier__c = '0';
            //insert prod1;
            prodList.add(prod1);
            
            //Inserting duplicate SLF Product Records
            Product__c prod2 = new Product__c();
            prod2.Installer_Account__c =acc.id;
            prod2.Long_Term_Facility_Lookup__c =acc.id;
            prod2.Name='testprod';
            prod2.FNI_Min_Response_Code__c=9;
            prod2.Product_Display_Name__c='test';
            prod2.Product_Loan_Type__c='Battery Only';
            prod2.Qualification_Message__c ='testmsg';
            prod2.State_Code__c ='CA';
            prod2.ST_APR__c = 2.99;
            prod2.Long_Term_Facility__c = 'TCU';
            prod2.APR__c = 2.99;
            prod2.ACH__c = true;
            prod2.Internal_Use_Only__c = true;
            prod2.LT_Max_Loan_Amount__c =1000;
            prod2.Term_mo__c = 144;
            prod2.Product_Tier__c = '0';
            //insert prod2;
            prodList.add(prod2);
            
            insert prodList;
            
            //Insert SLF Product Record
            List<Product_Proxy__c> prdctPrxyLst = new List<Product_Proxy__c>();
            Product_Proxy__c prodProxy = new Product_Proxy__c();
            prodProxy.ACH__c = true;
            prodProxy.APR__c = 2.99;
            prodProxy.Installer__c =acc.id;
            prodProxy.Internal_Use_Only__c = false;
            prodProxy.Is_Active__c = true;
            prodProxy.SLF_ProductID__c = prod.Id;
            prodProxy.Name = 'testprod';
            prodProxy.State_Code__c ='CA';
            prodProxy.Term_mo__c = 120;
            //insert prodProxy;
            prdctPrxyLst.add(prodProxy);
            
            Product_Proxy__c prodProxy1 = new Product_Proxy__c();
            prodProxy1.ACH__c = true;
            prodProxy1.APR__c = 2.99;
            prodProxy1.Installer__c =acc.id;
            prodProxy1.Internal_Use_Only__c = false;
            prodProxy1.Is_Active__c = true;
            prodProxy1.SLF_ProductID__c = prod1.Id;
            prodProxy1.Name = 'testprod';
            prodProxy1.State_Code__c ='CA';
            prodProxy1.Term_mo__c = 144;
            //insert prodProxy1;
            prdctPrxyLst.add(prodProxy1);
            
            Product_Proxy__c prodProxy2 = new Product_Proxy__c();
            prodProxy2.ACH__c = true;
            prodProxy2.APR__c = 2.99;
            prodProxy2.Installer__c =acc.id;
            prodProxy2.Internal_Use_Only__c = false;
            prodProxy2.Is_Active__c = true;
            prodProxy2.SLF_ProductID__c = prod2.Id;
            prodProxy2.Name = 'testprod';
            prodProxy2.State_Code__c ='CA';
            prodProxy2.Term_mo__c = 144;
            //insert prodProxy2;
            prdctPrxyLst.add(prodProxy2);
            
            Product_Proxy__c prodProxy3 = new Product_Proxy__c();
            prodProxy3.ACH__c = true;
            prodProxy3.APR__c = 5.99;
            prodProxy3.Installer__c =acc.id;
            prodProxy3.Internal_Use_Only__c = false;
            prodProxy3.Is_Active__c = true;
            prodProxy3.SLF_ProductID__c = prod2.Id;
            prodProxy3.Name = 'testprod';
            prodProxy3.State_Code__c ='NY';
            prodProxy3.Term_mo__c = 120;
            //insert prodProxy3;
            prdctPrxyLst.add(prodProxy3);
            
            insert prdctPrxyLst;
            
            test.starttest();
            
            //Insert opportunity record
            List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            //insert personOpp;
            oppList.add(personOpp);
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            opp.Upload_Comment__c=true;
            opp.EDW_Originated__c=true;
            //insert opp;
            oppList.add(opp);
            
            insert oppList;
            
            //data for StipulationStatusBatch class test.
            
            Underwriting_File__c undStpbt = new Underwriting_File__c();           
            undStpbt.Opportunity__c = opp.id;
            undStpbt.Approved_for_Payments__c = true;
            insert undStpbt;
            
            
            Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', Email_Text__c = 'Test data', POS_Message__c = 'test', Review_Classification__c = 'SLS II');
            insert sd;
            
            Stipulation__c stp = new Stipulation__c();
            stp.Status__c ='New';
            stp.Stipulation_Data__c = sd.id;
            
            stp.Underwriting__c =undStpbt.id;
            insert stp;
            
            //data end
            personOpp.Upload_Comment__c=true;
            personOpp.EDW_Originated__c=true; 
            personOpp.Opportunity__c = opp.Id;
            update personOpp;
            
            //Insert opportunity record
            Opportunity oppSales = new Opportunity();
            oppsales.name = 'OppNew';
            oppsales.CloseDate = system.today();
            oppsales.StageName = 'New';
            oppsales.AccountId = acc.id;
            oppsales.Installer_Account__c = acc.id;
            oppsales.Install_State_Code__c = 'CA';
            oppsales.Install_Street__c ='Street';
            oppsales.Install_City__c ='InCity';
            oppsales.Combined_Loan_Amount__c = 10000;
            oppsales.Partner_Foreign_Key__c = 'TCU||1234';
            //opp.Approved_LT_Facility__c = 'TCU';
            oppsales.SLF_Product__c = prod.id;
            oppsales.Opportunity__c = personOpp.Id;
            oppsales.Co_Applicant__c = acc.id;
            oppsales.Upload_Comment__c=true;
            oppsales.EDW_Originated__c=true; 
            insert oppsales;
            
            List<Equipment_Manufacturer__c> eqList = new List<Equipment_Manufacturer__c>();
            Equipment_Manufacturer__c equipRec1 = new Equipment_Manufacturer__c();
            equipRec1.End_Date__c = system.today()-1;
            equipRec1.Activation_Date__c =system.today()-6;
            equipRec1.Manufacturer_Type__c ='Mounting';
            equipRec1.Manufacturer__c ='Unirac';
            //insert equipRec1;
            eqList.add(equipRec1);
            
            Equipment_Manufacturer__c equipRec2 = new Equipment_Manufacturer__c();
            equipRec2.Activation_Date__c =system.today();
            equipRec2.End_Date__c =system.today()+6;
            equipRec2.Manufacturer_Type__c ='Mounting';
            equipRec2.Manufacturer__c ='Unirac';
            //insert equipRec2;
            eqList.add(equipRec2);
            
            insert eqList;
            
            //Equipment_Manufacturer__c equipRec =[select id,Is_Active__c from Equipment_Manufacturer__c where id =:equipRec2.id];
            //system.debug('****equiplst***'+equipRec.Is_Active__c);
            
            
            //system.debug('****equiplst***'+equipRec.Is_Active__c);
            system.debug('****equiplst1***'+equipRec1.Is_Active__c);
            
            
            //Insert System Design Record
            System_Design__c sysDesignRec = new System_Design__c();
            //sysDesignRec.Name = 'Test Sys';
            sysDesignRec.System_Cost__c = 11;
            sysDesignRec.Opportunity__c = personOpp.Id;
            sysDesignRec.Est_Annual_Production_kWh__c = 10;
            sysDesignRec.Inverter_Count__c = 11;
            //  sysDesignRec.Inverter_Make__c = '11';
            sysDesignRec.Inverter_Model__c = '12';
            //sysDesignRec.Inverter__c = 'TBD';
            sysDesignRec.Module_Count__c = 10;
            sysDesignRec.Module_Model__c = '11';
            //sysDesignRec.System_Size_STC_kW__c = 12;
            insert sysDesignRec;
            
            //Insert Quote Record
            List<Quote> quoteList = new List<Quote>();
            Quote quoteRec = new Quote();
            quoteRec.Name ='Test quote';
            quoteRec.Opportunityid = oppsales.id;//opp.Id
            quoteRec.SLF_Product__c = prod.Id ;
            quoteRec.System_Design__c= sysDesignRec.id;
            //quoteRec.Accountid =acc.id;
            //insert quoteRec; 
            quoteList.add(quoteRec); 
            
            Quote quoteRec1 = new Quote();
            quoteRec1.Name ='Test quote';
            quoteRec1.Opportunityid = opp.Id;
            quoteRec1.SLF_Product__c = prod.Id ;
            quoteRec1.System_Design__c= sysDesignRec.id;
            //quoteRec.Accountid =acc.id;
            //insert quoteRec; 
            quoteList.add(quoteRec1);            
            
            
            Quote quoteRec2 = new Quote();
            quoteRec2.Name ='Test quote';
            quoteRec2.Opportunityid = personOpp.id;
            quoteRec2.SLF_Product__c = prod.Id ;
            quoteRec2.System_Design__c= sysDesignRec.id;
            //insert quoteRec2; 
            quoteList.add(quoteRec2);
            insert quoteList;
            
            
        }
    }
    
    public static testMethod void Batch_FetchMilestones()
    {
        Test.startTest();
        Database.executeBatch(new Batch_FetchMilestones(), 50);
        Test.stopTest();
    }
    public static testMethod void testscheduleBatch_FetchMilestones() 
    {
        Test.StartTest();
        Batch_FetchMilestones sh1 = new Batch_FetchMilestones();
        String sch = '0 0 23 * * ?'; system.schedule('Test Schedule Batch_FetchSiteTS', sch, sh1);
        Test.stopTest();
    }
}