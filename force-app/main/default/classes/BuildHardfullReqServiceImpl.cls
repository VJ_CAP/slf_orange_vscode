/**
* Description: Project Withdrawn related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh.R                07/25/2018          Created
******************************************************************************************/
public with sharing class BuildHardfullReqServiceImpl extends RESTServiceBase{  
   
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + BuildHardfullReqServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean responseBean = new UnifiedBean();
        responseBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        system.debug('### reqData '+reqData);
        responseBean.error = new list<UnifiedBean.errorWrapper>();
        
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {
            if(reqData.projects != null && reqData.projects[0].credits != null)
            {
                String creditExtRefNum = SLFUtility.guidIdGenerator();
                Map<string,Account> accMap;
                Opportunity oppSTLTRec =[Select id,Name,SLF_Product__c,StageName,Installer_Account__c,
                                            AccountId,SLF_Product__r.Term_mo__c,SLF_Product__r.APR__c,  CloseDate,Short_Term_APR__c,
                                            Short_Term_Term__c,Short_Term_Facility__c,Installer_Account__r.FNI_Domain_Code__c,
                                            Monthly_Payment__c,Monthly_Payment_calc__c,Monthly_Payment_Escalated_Single_Loan__c,
                                            Escalated_Monthly_Payment_calc__c,Long_Term_Term__c,Long_Term_Facility__c,Long_Term_APR__c,
                                            Combined_Loan_Amount__c,Install_Cost_Cash__c,Initial_Payment__c,
                                            Customer_has_authorized_credit_hard_pull__c,RecordTypeId,Install_Postal_Code__c,
                                            Install_Street__c,Install_City__c,Install_State_Code__c,Co_Applicant__c,
                                            SyncedQuoteId,SyncedQuote.Unofficial_HIS_PMT_Value__c,Short_Term_Loan_Amount_Manual__c,Long_Term_Loan_Amount_Manual__c,
                                            Application_Status__c,Sales_Representative_Email__c,Sales_Representative_Manager_Email__c,
                                            language__c,Product_Loan_Type__c,Installer_Account__r.Risk_Based_Pricing__c,ProductTier__c from Opportunity where id =: reqData.projects[0].Id];
                
                //Changed to SOQL query because FNI call should happen as Credit driven insted of opportunity driven
                SLF_Credit__c objCredit = [select Id,Name,Primary_Applicant__c,Co_Applicant__c from SLF_Credit__c where id =: reqData.projects[0].credits[0].id];
                
                
                if(objCredit.Primary_Applicant__c != null || objCredit.Co_Applicant__c != null){
                      accMap = new Map<string,Account>([Select id,Credit_Run__c,firstName,lastName,SSN__c,Last_four_SSN__c,PersonEmail,PersonBirthdate,
                                            Phone,MiddleName,Marital_Status__c,
                                            personOtherPhone,BillingCountry,BillingStreet,BillingCity,BillingStateCode,
                                            ShippingStateCode,BillingState,BillingPostalCode,ShippingCountry,
                                            ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,Annual_Income__c,
                                            Employer_Name__c,Length_of_Employment_Months__c,Length_of_Employment_Years__c,
                                            Job_Title__c,RecordTypeId from Account where id =: objCredit.Primary_Applicant__c or id =:objCredit.Co_Applicant__c]);
                    
                }    
                        
                String HardpullXMLStr;
                
                if(!accMap.isEmpty()){
                    Account acc1,acc2;
                    acc1 = (accMap.containsKey(objCredit.Primary_Applicant__c))?accMap.get(objCredit.Primary_Applicant__c):null;
                    acc2 = (accMap.containsKey(objCredit.Co_Applicant__c))?accMap.get(objCredit.Co_Applicant__c):null;
                      
                HardpullXMLStr = CreditWaterFall.buildHardPullFNIReq(acc1,acc2,oppSTLTRec,objCredit,creditExtRefNum);
                }
                System.debug('### Hardpull SOAP Body: '+HardpullXMLStr);
                
                responseBean = SLFUtility.getUnifiedBean(new set<Id>{oppSTLTRec.Id},'Orange',false);
                for(UnifiedBean.OpportunityWrapper  projectIterate : responseBean.projects){
                    for(UnifiedBean.CreditsWrapper creditIterate : projectIterate.credits){
                        if(creditIterate.Id == objCredit.id)
                        {
                            creditIterate.fniLTReq  = HardpullXMLStr;
                        }
                    }
                }
                responseBean.returnCode = '200';
                iRestRes = (IRestResponse)responseBean;
                res.response = iRestRes;
                system.debug('###..response'+res.response);   
            }
            
        }catch(Exception err){
            system.debug('### BuildHardfullReqServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' BuildHardfullReqServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
        }
        system.debug('### Exit from fetchData() of ' + BuildHardfullReqServiceImpl.class);
        
        return responseBean;
        
    }
}