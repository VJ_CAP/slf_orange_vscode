/**
* Description: Opportunity Id Search related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class ProductDetailServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
* Description:    
*          
*
* @param reqAP      Get all POST request action parameters
* @param reqData    Get all POST request data
*
* return res        Send response back with data and response status code
*/
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ProductDetailServiceImpl.class);
        IRestResponse iRestRes;
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean reqData ;
        try{
            System.debug('###rw::'+rw);
            System.debug('###rw.reqDataStr::'+rw.reqDataStr);
            if(string.isNotBlank(rw.reqDataStr))
            {
                reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            }else{
                reqData = new UnifiedBean();
            }
            
            UnifiedBean respData = new UnifiedBean();
            respData.products = new List<UnifiedBean.ProductWrapper>();
            
            
            system.debug('### reqData '+reqData);
            List<UnifiedBean.ProductWrapper> prodLst ;
            if(null != reqData.products)
            {
                prodLst = reqData.products;
            }else{
                prodLst = new List<UnifiedBean.ProductWrapper>();
            }
            UnifiedBean.ProductWrapper objProductWrapper ;
            if(!prodLst.isEmpty())
            {
                objProductWrapper = prodLst.get(0); 
            }else{
                objProductWrapper = new UnifiedBean.ProductWrapper();
            }
            
            string conditions = '';
           List<String> lstConditions = new List<String>();
            
            if (null !=objProductWrapper.id && String.isNotBlank(objProductWrapper.id)) {
                lstConditions.add('id=\'' + objProductWrapper.id + '\'');
            }
            if (null !=objProductWrapper.name && String.isNotBlank(objProductWrapper.name)) {
                lstConditions.add('Product_Display_Name__c=\'' + objProductWrapper.name + '\'');
            }
            if (null !=objProductWrapper.loanType && String.isNotBlank(objProductWrapper.loanType)) {
                lstConditions.add('Loan_Type__c=\'' + objProductWrapper.loanType + '\'');
            }
            if (null != objProductWrapper.term) {
                lstConditions.add('Term_mo__c=' + objProductWrapper.term);
            }
             //Modified by Deepika as part of Orange - 3622 - Start
            /*if (null != objProductWrapper.apr) {
                lstConditions.add('APR__c=' + objProductWrapper.apr);
            }
            if (null != objProductWrapper.isACH) {
                lstConditions.add('ACH__c=' + objProductWrapper.isACH);
            }*/
            if(null != objProductWrapper.isACH && objProductWrapper.isACH) {
                lstConditions.add('APR__c=' + objProductWrapper.apr);
                if(objProductWrapper.productType == 'HIS')
                    lstConditions.add('Promo_APR__c =' + objProductWrapper.promoAPR);
            }else if(null != objProductWrapper.isACH && !objProductWrapper.isACH){ //Added by Ravi as part of ORANGE-3622
                lstConditions.add('NonACH_APR__c =' + objProductWrapper.apr);
                if(objProductWrapper.productType == 'HIS')
                    lstConditions.add('NonACH_Promo_APR__c =' + objProductWrapper.promoAPR);
            }           
            //Modified by Deepika as part of Orange - 3622 - end
            if (null !=objProductWrapper.stateName && String.isNotBlank(objProductWrapper.stateName)) {
                lstConditions.add('State_Code__c=\'' + SLFUtility.getStateCode(objProductWrapper.stateName) + '\'');
            }
            if (null != objProductWrapper.productMaxLoanAmount) {
                lstConditions.add('Max_Loan_Amount__c=' + objProductWrapper.productMaxLoanAmount);
            }
            
            
            //Default filter condition
            lstConditions.add('Internal_Use_Only__c=false');
            lstConditions.add('Is_Active__c=true');
            //Added as part of ORANGE-1595 START
            String productTypes = Label.Home_Type_Products;
            List<string> lstProducttypes = new List<String>();
            for(string typeval : productTypes.split('&&')){
                lstProducttypes.add(typeval);
            }
            system.debug('###lstProducttypes:'+lstProducttypes);
            system.debug('###reqData.products:'+reqData.products);
            if(reqData.products != null && reqData.products[0].productType != null){
                system.debug('###productType:'+reqData.products[0].productType);
                if(string.isNotBlank(reqData.products[0].productType)){
                    if(reqData.products[0].productType == 'Home')
                        lstConditions.add('Product_Loan_Type__c IN: '+'lstProducttypes');
                        //lstConditions.add('Product_Tier__c = \'0\''); // Added for Orange-8308
                    if(reqData.products[0].productType == 'Solar')
                        lstConditions.add('Product_Loan_Type__c NOT IN: '+'lstProducttypes');
                }else{
                    lstConditions.add('Product_Loan_Type__c NOT IN: '+'lstProducttypes');
                }
            }else{
                lstConditions.add('Product_Loan_Type__c NOT IN: '+'lstProducttypes');
            }  
           
            //Added as part of ORANGE-1595 END

            boolean eligibleForCreditWaterfall = false;
            boolean eligibleForRBP = false;
            
            List<user> loggedInUsr = [select Id,contactid,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,UserRole.Name,contact.Account.Eligible_for_Last_Four_SSN__c,contact.Account.Credit_Waterfall__c,contact.Account.Risk_Based_Pricing__c from User where Id =: userinfo.getUserId()];
            
            eligibleForCreditWaterfall = loggedInUsr[0].contact.Account.Credit_Waterfall__c;
            eligibleForRBP = loggedInUsr[0].contact.Account.Risk_Based_Pricing__c;
            String installerAccId = loggedInUsr[0].contact.AccountId;
            
            
            boolean hasValue = false;
            List<Product__c>  prodResults = new List<Product__c>();
            try{
               String oppTier;
               String loanType = '';
                if(reqData.projects != null){                   
                    String conditionchecks ='';
                    if((String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                        ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                        ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                        ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
                    {
                        hasValue = false;
                        UnifiedBean unifBean = new UnifiedBean(); 
                        List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
                        UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
                        errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('208',null);
                        errorWrapperLst.add(errorWrapper);
                        unifBean.error  = errorWrapperLst ;
                        unifBean.returnCode = '208';
                        iRestRes = (IRestResponse)unifBean;
                        res.response= iRestRes;
                        return res.response ;
                    }else if(String.isNotBlank(reqData.projects[0].id))
                    {
                        Set<String> ids = new Set<String>{reqData.projects[0].id};
                        conditionchecks += ' OR Id = ' + ':ids' ;
                        hasValue = true;
                    
                    }else if(String.isNotBlank(reqData.projects[0].externalId))
                    {
                        String externalId;
                        if(!loggedInUsr.isEmpty())
                        {
                            if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                                externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                                System.debug('### externalId '+externalId);
                            }else{
                                UnifiedBean unifBean = new UnifiedBean(); 
                                List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
                                UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
                                errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('400',null);
                                errorWrapperLst.add(errorWrapper);
                                unifBean.error  = errorWrapperLst ;
                                unifBean.returnCode = '400';
                                iRestRes = (IRestResponse)unifBean;
                                res.response= iRestRes;
                                return res.response ;
                            }
                            
                        }
                        system.debug('### externalId '+externalId);
                        conditionchecks += ' OR Partner_Foreign_Key__c = ' + ':externalId'   ;
                        hasValue = true;
                    
                    }else if(String.isNotBlank(reqData.projects[0].hashId))
                    {
                        Set<String> hashIds = new Set<String>{reqData.projects[0].hashId};
                        conditionchecks += ' OR Hash_Id__c IN ' + ':hashIds'  ;
                       hasValue = true;
                    
                    }
                    List<Opportunity> oppResults = new List<Opportunity>();
                    Set<id> prodIds = new Set<id>();
                    Set<String> opptiers = new Set<String>();
                    if(hasValue){
                        String oppQuery = 'select id,Name,Credit_Decision__c,StageName,Product_Loan_Type__c,Partner_Foreign_Key__c,Hash_Id__c,Project_Category__c,ProductTier__c,Change_Order_Status__c,SLF_Product__c,(Select id,Name from Quotes),(select id from Credits__r where IsSyncing__c = true) from Opportunity where '+conditionchecks;
                        oppQuery= oppQuery.replaceFirst(' OR ', ' ');
                        system.debug('### oppQuery '+oppQuery );
                        oppResults = Database.query(oppQuery);
                        system.debug('### oppResults '+oppResults );
                        Set<id> quoteIds = new Set<id>();
                        if(!oppResults.isEmpty()){
                            if(oppResults[0].Quotes != null){
                                for(Quote quoteRec :oppResults[0].Quotes){
                                    quoteIds.add(quoteRec.id);
                                }
                            }
                            if(oppResults[0].ProductTier__c != null){
                                oppTier = oppResults[0].ProductTier__c;
                            }else{
                                oppTier = '0';
                            }
                            //Added by Deepika on 27/12/2018 (Orange-1593) - start
                            if(oppResults[0].Project_Category__c == 'Home' && !oppResults[0].Credits__r.isEmpty()){                               
                                loanType = oppResults[0].Product_Loan_Type__c;
                                System.debug('loanType :'+loanType);
                            }
                            //Added by Deepika on 27/12/2018 (Orange-1593) - end
                        }
                        if(!quoteIds.isEmpty()){
                            List<Credit_Opinion__c> crdOpinoinLst = [select Id,Name,Credit__c,Quote__c,Status__c,Credit__r.Status__c,Credit__r.IsSyncing__c,Credit__r.LastModifiedDate,Credit__r.Opportunity__c,Quote__r.SLF_Product__c,Quote__r.SLF_Product__r.Expiration_Date__c from Credit_Opinion__c where Quote__c IN: quoteIds and status__c IN ('Manual Approved','Auto Approved') and Credit_Expiration__c = false ];  
                            system.debug('### crdOpinoinLst '+crdOpinoinLst );
                            for(Credit_Opinion__c creditOp :crdOpinoinLst){
                               if((creditOp.Credit__r.Status__c =='Auto Approved' || creditOp.Credit__r.Status__c =='Manual Approved') && (creditOp.Quote__r.SLF_Product__r.Expiration_Date__c >= Date.today() || ((oppResults[0].Change_Order_Status__c == 'Pending' || oppResults[0].StageName == 'Closed Won')&& oppResults[0].SLF_Product__c == creditOp.Quote__r.SLF_Product__c))){
                                    prodIds.add(creditOp.Quote__r.SLF_Product__c);
                                }
                            }
                        }
                        
                        if(!prodIds.isEmpty()){
                            system.debug('###prodIds '+prodIds);
                              //Removed ACH__c field from query by Deepika as part of Orange - 3622
                            prodResults = [select id,NonACH_Promo_APR__c,NonACH_Prod_Display__c,Promo_Term__c,Interest_Type__c,NonACH_APR__c,Installer_Account__c,Non_Solar_Eligible__c,Product_Loan_Type__c,Min_Loan_Amount__c,Product_Disclosure__c,NonACH_UI_Display_Name__c,UI_Display_Name__c,Name,Loan_Type__c,Promo_APR__c,Promo_Fixed_Payment__c,Product_Display_Name__c,Term_mo__c,APR__c,ACH__c,State_Code__c,Promo_Percentage_Balance__c,Promo_Percentage_Payment__c,Max_Loan_Amount__c,Product_Tier__c,Draw_Period__c from Product__c where id IN :prodIds and Is_Active__c=false];
                            System.debug('prodResults:'+prodResults);
                           
                        }
                    }
                }
                System.debug('### Opportunity Tier: '+oppTier);
                //Added by Deepika on 27/12/2018 (Orange-1593) - start
                if(loanType != null && loanType != '' && eligibleForRBP){
                    System.debug('loanType :'+loanType);
                    lstConditions.add('((product_loan_type__c = \'' + loanType+ '\' and Product_Tier__c =\'' + oppTier+ '\') or (product_loan_type__c != \'' + loanType +'\' and Product_Tier__c = \'0\'))');
                }//Added by Deepika on 27/12/2018 (Orange-1593) -end
                else if((oppTier!=null && oppTier!='') && eligibleForRBP){
                    lstConditions.add('Product_Tier__c=\'' + oppTier + '\'');
                }
                //Added by Deepika on 25/10/2018 (Orange-1401)
                else if (null !=objProductWrapper.ShowTiers && !objProductWrapper.ShowTiers.isEmpty() && eligibleForRBP) {
                    System.debug('objProductWrapper.ShowTiers::'+objProductWrapper.ShowTiers);
                    if(objProductWrapper.ShowTiers[0] != 'All'){
                        List<String> tierList = new List<String>();
                        for(String tierStr : objProductWrapper.ShowTiers){
                            tierList.add(String.valueOf(tierStr));
                        }
                        lstConditions.add('Product_Tier__c IN :tierList');
                    }            
                }
                else{
                    lstConditions.add('Product_Tier__c =\'0\'');
                }
                
                if (String.isNotBlank(installerAccId)) {
                    lstConditions.add('Installer_Account__c=\'' + installerAccId + '\'');
                }
                if(lstConditions!=null && lstConditions.size()>0){
                    for (integer i = 0; i < lstConditions.size(); i++) {
                        if (i == lstConditions.size() - 1) {
                            conditions = conditions + lstConditions.get(i);
                        } else {
                            conditions = conditions + lstConditions.get(i) + ' and ';
                        }
                    }
                }
                 String queryStr = '';
                 //chagned order by in queries by Deepika on 25/10/2018 (Orange-1401)
                if(string.isNotBlank(conditions))
                {
                    //Removed ACH__c field from query by Deepika as part of Orange - 3622
                    queryStr = 'select id,Name,ACH__c,NonACH_APR__c,NonACH_Prod_Display__c,NonACH_Promo_APR__c,Non_Solar_Eligible__c,Product_Loan_Type__c,Installer_Account__c,Interest_Type__c,Min_Loan_Amount__c,Product_Disclosure__c,UI_Display_Name__c,Product_Display_Name__c,Loan_Type__c,Term_mo__c,APR__c,State_Code__c,Max_Loan_Amount__c,Product_Tier__c,Promo_APR__c,Promo_Term__c,Promo_Fixed_Payment__c,Promo_Percentage_Balance__c,Promo_Percentage_Payment__c,NonACH_UI_Display_Name__c,Draw_Period__c from Product__c where '+conditions+' order by Product_Loan_Type__c,Term_mo__c,Product_Tier__c,Promo_Term__c ASC';
                }else{
                    //Removed ACH__c field from query by Deepika as part of Orange - 3622
                    queryStr = 'select id,Non_Solar_Eligible__c,NonACH_Prod_Display__c, NonACH_Promo_APR__c,Product_Loan_Type__c,Installer_Account__c,Interest_Type__c,Min_Loan_Amount__c,Product_Disclosure__c,UI_Display_Name__c,Name,Loan_Type__c,Product_Display_Name__c,Term_mo__c,APR__c,NonACH_APR__c,State_Code__c,Max_Loan_Amount__c,Product_Tier__c,Promo_APR__c,Promo_Term__c,Promo_Fixed_Payment__c,Promo_Percentage_Balance__c,Promo_Percentage_Payment__c,NonACH_UI_Display_Name__c,Draw_Period__c from Product__c where Internal_Use_Only__c=false and Is_Active__c=false AND Installer_Account__c = :installerAccId order by Term_mo__c,Product_Tier__c ASC';
                } 
                
                system.debug('### queryStr after '+queryStr);
                List<Product__c> prodList = Database.query(queryStr);
                 system.debug('###prodList '+prodList);
                if(!prodList.isEmpty())
                    prodResults.addAll(prodList);
                System.debug('prodResults:'+prodResults);          
                Map<String,Product__c> prodMap = new Map<String,Product__c>();
                Map<String,Product__c> nonAchProdMap = new Map<String,Product__c>();//6832 
                Boolean ProductDisplayName = false;
                Boolean nonAchProductDisplayName = false;
                
                if(!prodResults.isEmpty()){
                    for(Product__c prodIterate : prodResults)
                    {                       
                        ProductDisplayName = false;
                        nonAchProductDisplayName = false;//6832 
                        //Added prodIterate.Promo_Term__c and prodIterate.Interest_Type__c condition by Deepika on 27/12/2018 (Orange-1593) 
                        String productNameAch;
                        String productNameNonAch;//6832 
                        
                        //Ravi: For De-duping HII products as part of ORANGE-2382
                        //Removed ACH__c field from deduping logic by Deepika as part of Orange - 3622
                        if(prodIterate.Product_Loan_Type__c != 'HII'){
                            productNameAch = prodIterate.APR__c+'-'+prodIterate.Term_mo__c+'-'+'-'+prodIterate.State_Code__c+'-'+prodIterate.Product_Loan_Type__c+'-'+prodIterate.Promo_Term__c+'-'+prodIterate.Interest_Type__c+'-'+prodIterate.Installer_Account__c+'-'+prodIterate.Product_Tier__c+'-'+prodIterate.Loan_Type__c+'-'+prodIterate.Promo_APR__c+'-'+prodIterate.Promo_Fixed_Payment__c+'-'+prodIterate.Promo_Percentage_Payment__c+'-'+prodIterate.Promo_Percentage_Balance__c+'-'+prodIterate.Draw_Period__c;//Modified Deduping logic by Deepika on 11/01/2019 (Orange-1735)
                            productNameNonAch = prodIterate.NonACH_APR__c+'-'+prodIterate.Term_mo__c+'-'+'-'+prodIterate.State_Code__c+'-'+prodIterate.Product_Loan_Type__c+'-'+prodIterate.Promo_Term__c+'-'+prodIterate.Interest_Type__c+'-'+prodIterate.Installer_Account__c+'-'+prodIterate.Product_Tier__c+'-'+prodIterate.Loan_Type__c+'-'+prodIterate.NonACH_Promo_APR__c+'-'+prodIterate.Promo_Fixed_Payment__c+'-'+prodIterate.Promo_Percentage_Payment__c+'-'+prodIterate.Promo_Percentage_Balance__c+'-'+prodIterate.Draw_Period__c;//Modified Deduping logic by Deepika on 11/01/2019 (Orange-1735)
                        }else if(prodIterate.Product_Loan_Type__c == 'HII'){
                            productNameAch = prodIterate.APR__c+'-'+prodIterate.Term_mo__c+'-'+'-'+prodIterate.State_Code__c+'-'+prodIterate.Product_Loan_Type__c;
                            productNameNonAch = prodIterate.NonACH_APR__c+'-'+prodIterate.Term_mo__c+'-'+'-'+prodIterate.State_Code__c+'-'+prodIterate.Product_Loan_Type__c;
                        }
                        
                        system.debug('productNameAch - '+productNameAch);
                        if(!prodMap.ContainsKey(productNameAch))
                        {
                            prodMap.put(productNameAch,prodIterate);
                            ProductDisplayName = true;
                        }   
                        //6832 - start
                        if(!nonAchProdMap.ContainsKey(productNameNonAch))
                        {
                            nonAchProdMap.put(productNameNonAch,prodIterate);
                            nonAchProductDisplayName = true;
                        } 
                        //6832 - end
                        System.debug('ProductDisplayName::'+ProductDisplayName);
                        System.debug('nonAchProductDisplayName::'+nonAchProductDisplayName);
                        
                        if(ProductDisplayName)
                        {
                            System.debug('productdisplayach'+hasValue);
                            UnifiedBean.ProductWrapper prodRec = new UnifiedBean.ProductWrapper();
                            prodRec.id = prodIterate.Id;
                            prodRec.name = prodIterate.Product_Display_Name__c;                         
                            prodRec.loanType =  prodIterate.Loan_Type__c;
                            prodRec.tier = prodIterate.Product_Tier__c;//Added by Deepika on 25/10/2018 (Orange-1401)
                            prodRec.term =  prodIterate.Term_mo__c;
                            prodRec.apr =  prodIterate.APR__c;
                            prodRec.isACH =  true; //ACH__c should be returned in the response by Ravi as part of Orange-3622
                            prodRec.stateName =  SLFUtility.getStateName(prodIterate.State_Code__c);
                            prodRec.productMaxLoanAmount =  prodIterate.Max_Loan_Amount__c;
                            prodRec.productType = prodIterate.Product_Loan_Type__c;
                            prodRec.productMinLoanAmount = prodIterate.Min_Loan_Amount__c;
                            prodRec.productDisclosure = prodIterate.Product_Disclosure__c; //Added by venkatesh on 23/11/2018 (Orange-1593)
                            prodRec.interestType = prodIterate.Interest_Type__c;
                            prodRec.uiName = prodIterate.UI_Display_Name__c; //Added by Deepika on 26/12/2018 (Orange-1593)
                            prodRec.promoAPR = prodIterate.Promo_APR__c;//Start: Added by Bindu 27/12/2018 as part of Orange-1735
                            prodRec.promoFixedPayment = prodIterate.Promo_Fixed_Payment__c;
                            prodRec.promoBalance = prodIterate.Promo_Percentage_Balance__c;
                            prodRec.promoPercentPayment = prodIterate.Promo_Percentage_Payment__c;
                            prodRec.promoTerm = Integer.valueOf(prodIterate.Promo_Term__c);
                            prodRec.drawPeriod = Integer.valueOf(prodIterate.Draw_Period__c);//End: Added by Bindu 27/12/2018 as part of Orange-1735

                            
                            System.debug('### prodRec:::'+prodRec);
                            System.debug('prodIterate.Product_Loan_Type__c'+prodIterate.Product_Loan_Type__c);
                            System.debug('prodIterate.Product_Tier__c'+prodIterate.Product_Tier__c);
                            //No project Id in request and productype=HIS and Tier=0
                            if(!hasValue && prodIterate.Product_Loan_Type__c=='HIS' && (prodIterate.Product_Tier__c == '' || prodIterate.Product_Tier__c== '0')){                                
                                respData.products.add(prodRec);//Adding ACH Products
                                
                            }
                            //Ravi: For De-duping HII products as part of ORANGE-2382
                            else if(!hasValue && prodIterate.Product_Loan_Type__c=='HII'){                                
                                respData.products.add(prodRec);//Adding ACH Products    
                            }
                            else if(!hasValue && prodIterate.Product_Loan_Type__c =='HIN' && prodIterate.Product_Tier__c== '0')
                            {
                                if(prodRec!= null)
                                    respData.products.add(prodRec);                                
                            }
                            //No project Id in the request and productType!=HIS
                            else if(!hasValue && prodIterate.Product_Loan_Type__c !='HIS' && prodIterate.Product_Loan_Type__c !='HIN')
                           {
                               respData.products.add(prodRec);//Adding ACH Products
                           }
                           //Project Id in the request BAU
                           else if(hasValue)
                           {
                                respData.products.add(prodRec);//Adding ACH Products
                           }
                        }  
                        //6832 - start     
                        if(nonAchProductDisplayName){
                            UnifiedBean.ProductWrapper prodRec = new UnifiedBean.ProductWrapper();
                            prodRec.id = prodIterate.Id+'f';
                            prodRec.name = prodIterate.NonACH_Prod_Display__c;                         
                            prodRec.loanType =  prodIterate.Loan_Type__c;
                            prodRec.tier = prodIterate.Product_Tier__c;//Added by Deepika on 25/10/2018 (Orange-1401)
                            prodRec.term =  prodIterate.Term_mo__c;
                            prodRec.apr =  prodIterate.NonACH_APR__c;
                            prodRec.isACH =  false; //ACH__c should be returned in the response by Ravi as part of Orange-3622
                            prodRec.stateName =  SLFUtility.getStateName(prodIterate.State_Code__c);
                            prodRec.productMaxLoanAmount =  prodIterate.Max_Loan_Amount__c;
                            prodRec.productType = prodIterate.Product_Loan_Type__c;
                            prodRec.productMinLoanAmount = prodIterate.Min_Loan_Amount__c;
                            prodRec.productDisclosure = prodIterate.Product_Disclosure__c; //Added by venkatesh on 23/11/2018 (Orange-1593)
                            prodRec.interestType = prodIterate.Interest_Type__c;
                            prodRec.uiName = prodIterate.NonACH_UI_Display_Name__c; // Added this part of 6937 //Added by Deepika on 26/12/2018 (Orange-1593)
                            prodRec.promoAPR = prodIterate.NonACH_Promo_APR__c;//Start: Added by Bindu 27/12/2018 as part of Orange-1735
                            prodRec.promoFixedPayment = prodIterate.Promo_Fixed_Payment__c;
                            prodRec.promoBalance = prodIterate.Promo_Percentage_Balance__c;
                            prodRec.promoPercentPayment = prodIterate.Promo_Percentage_Payment__c;
                            prodRec.promoTerm = Integer.valueOf(prodIterate.Promo_Term__c);
                            prodRec.drawPeriod = Integer.valueOf(prodIterate.Draw_Period__c);//End: Added by Bindu 27/12/2018 as part of Orange-1735
                            
                            System.debug('### prodRec:::'+prodRec);
                            //No project Id in request and productype=HIS and Tier=0
                            if(!hasValue && prodIterate.Product_Loan_Type__c=='HIS' && (prodIterate.Product_Tier__c == '' || prodIterate.Product_Tier__c== '0')){
                                if(prodRec!= null)
                                    respData.products.add(prodRec); //Adding Non-ACH Products
                            }
                            //Ravi: For De-duping HII products as part of ORANGE-2382
                            else if(!hasValue && prodIterate.Product_Loan_Type__c=='HII'){                               
                                if(prodRec!= null)
                                    respData.products.add(prodRec); //Adding Non-ACH Products
                                
                            }
                            else if(!hasValue && prodIterate.Product_Loan_Type__c =='HIN' && prodIterate.Product_Tier__c== '0')
                            {
                                if(prodRec!= null)
                                    respData.products.add(prodRec);
                            }
                            //No project Id in the request and productType!=HIS
                            else if(!hasValue && prodIterate.Product_Loan_Type__c !='HIS' && prodIterate.Product_Loan_Type__c !='HIN')
                           {
                                if(prodRec!= null)
                                    respData.products.add(prodRec); //Adding Non-ACH Products
                           }
                           //Project Id in the request BAU
                           else if(hasValue)
                           {
                                if(prodRec!= null)
                                    respData.products.add(prodRec); //Adding Non-ACH Products                               
                           }
                        }
                        //6832 - end
                    }
                     respData.returnCode = '200';
                }
                
                else{
                    respData.returnCode = '390';
                    List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                    UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                    errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('390',null);
                    errorLst.add(errorWrap);
                    respData.error = errorLst;
                }
            } catch(Exception ex){
                respData.returnCode = '400';
                List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('400',null);
                errorLst.add(errorWrap);
                respData.error = errorLst;
                 ErrorLogUtility.writeLog('ProductDetailServiceImpl.fetchData()',ex.getLineNumber(),'fetchData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
            }
            
            res.response = (IRestResponse)respData;
            system.debug('### response-->:'+res.response);
        } catch(Exception err){
            system.debug('### ProductDetailServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' ProductDetailServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            
            UnifiedBean unifBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214',null);
            errorWrapperLst.add(errorWrapper);
            unifBean.error  = errorWrapperLst ;
            unifBean.returnCode = '214';
            iRestRes = (IRestResponse)unifBean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + ProductDetailServiceImpl.class);
        return res.response ;
    }
    
    
}