/**
* Description: Project Withdrawn related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh.R                07/25/2018          Created
******************************************************************************************/
public with sharing class ProjectWithdrawServiceImpl extends RESTServiceBase{  
   
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ProjectWithdrawServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean responseBean = new UnifiedBean();
        responseBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        system.debug('### reqData '+reqData);
        responseBean.error = new list<UnifiedBean.errorWrapper>();
        
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {
            String opportunityId = '';
            String externalId;
            if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
            {                 
                errMsg = ErrorLogUtility.getErrorCodes('203',null);
                responseBean.returnCode = '203';
            } 
            else if(
                (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
            {
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                responseBean.returnCode = '201';
            }else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
            {
                List<Opportunity> opprLst = [SELECT Id FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                
                if (opprLst==null || opprLst.size() == 0)
                {
                    responseBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
                else{
                    opportunityId = opprLst.get(0).Id;
                }
            }
            else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
            {
                List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                        System.debug('### externalId '+externalId);
                    }else{
                        responseBean.returnCode = '400';
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                    }
                    
                }
                if(String.isBlank(errMsg))
                {
                    List<opportunity> opprLst = [select id,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        responseBean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                    else{
                        opportunityId = opprLst.get(0).id;
                    }
                }
            }else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
            {
                List<opportunity> opprLst = [select id,Hash_Id__c from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                if (opprLst!=null && opprLst.size() > 0)
                {
                    opportunityId = opprLst.get(0).id;
                }else {
                    responseBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            } 
            opportunity oppRec = new opportunity();
            List<opportunity> oppLst = new List<opportunity>();
            Underwriting_File__c underwritingRec;
            if(String.isNotBlank(opportunityId))
            {
                 oppLst = [Select id,Name,EDW_Originated__c,Project_Category__c,(select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c,Withdraw_Reason__c,Withdraw_Reason_Detail__c,Declined_Withdrawn_Date__c from Underwriting_Files__r) from Opportunity where id =: opportunityId];
            }
            oppRec = oppLst[0];
            
            if(!oppLst.isEmpty() && oppLst.get(0).Project_Category__c == 'Solar')
            {
                
                system.debug('### oppRec '+oppRec);
                if(oppRec.EDW_Originated__c == true){
                    errMsg = ErrorLogUtility.getErrorCodes('218',new List<String>{String.valueOf(oppRec.id)}); 
                    responseBean.returnCode = '218';
                    
                }else if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                    responseBean.returnCode = '366';
                    errMsg = ErrorLogUtility.getErrorCodes('366',null);
                }
                
                if(oppRec.Underwriting_Files__r != null && String.isBlank(errMsg))
                {
                    underwritingRec = oppRec.Underwriting_Files__r[0];
                    if(reqData.projects[0].projectWithDrawnReason !=null && String.isNotBlank(reqData.projects[0].projectWithDrawnReason)){
                        if((underwritingRec.Project_Status__c == null || underwritingRec.Project_Status__c == 'M0' || underwritingRec.Project_Status__c == 'M1' || underwritingRec.Project_Status__c == 'Pending Loan Docs' || underwritingRec.Project_Status__c == 'Permit' || underwritingRec.Project_Status__c == 'Kitting') && (underwritingRec.Project_Status_Detail__c == 'M0 - Documents Needed' || underwritingRec.Project_Status_Detail__c == 'M0 - Need Approval Request' || underwritingRec.Project_Status_Detail__c == 'M0 - In Review' || underwritingRec.Project_Status_Detail__c == 'M1 - Documents Needed' || underwritingRec.Project_Status_Detail__c == 'M1 - Need Approval Request' || underwritingRec.Project_Status_Detail__c == null || underwritingRec.Project_Status_Detail__c == 'Change Order Requested' || underwritingRec.Project_Status_Detail__c == 'Permit - Documents Needed' || underwritingRec.Project_Status_Detail__c == 'Permit - Need Approval Request' || underwritingRec.Project_Status_Detail__c == 'Permit - In Review' ||  underwritingRec.Project_Status_Detail__c == 'Kitting - Documents Needed' || underwritingRec.Project_Status_Detail__c == 'Kitting - Need Approval Request' || underwritingRec.Project_Status_Detail__c == 'Kitting - In Review')){ // Updated as Part of 8681
                        underwritingRec.Project_Status__c = 'Project Withdrawn';
                        underwritingRec.Withdraw_Reason__c = reqData.projects[0].projectWithDrawnReason;
                        underwritingRec.Withdraw_Reason_Detail__c = reqData.projects[0].projectWithDrawnDetail; // Added as part of 3624
                        underwritingRec.Declined_Withdrawn_Date__c = System.now();
                        update underwritingRec;
                        responseBean = SLFUtility.getUnifiedBean(new set<Id>{oppRec.Id},'Orange',false);
                        responseBean.returnCode = '200';
                        
                        }else{
                            responseBean.returnCode = '364';
                            errMsg = ErrorLogUtility.getErrorCodes('364',null);
                        }
                    }else{
                            responseBean.returnCode = '365';
                            errMsg = ErrorLogUtility.getErrorCodes('365',null);
                        }
                }
                
            }else {
                      // Added as Part of 7051 by Sreekar - Start
                List<Underwriting_file__c> lstUnderWriting = [select id,opportunity__c,Project_Status__c,Withdraw_Reason__c,Withdraw_Reason_Detail__c,Declined_Withdrawn_Date__c,(select id,Level__c,Amount__c,Status__c from Draw_Requests__r where Status__c = 'Approved') from underwriting_file__c where opportunity__c =:opportunityId];
               if(reqData.projects[0].projectWithDrawnReason !=null && String.isNotBlank(reqData.projects[0].projectWithDrawnReason)){
                     if(lstUnderWriting[0].Draw_Requests__r.size() > 0)
                     {    
                         responseBean.returnCode = '364';
                        errMsg = ErrorLogUtility.getErrorCodes('364',null);
                     }else{
                        Underwriting_file__c underwritingRec1 = new Underwriting_file__c();
                      underwritingRec1 =lstUnderWriting.get(0);
                    underwritingRec1.Project_Status__c = 'Project Withdrawn';
                    underwritingRec1.Withdraw_Reason__c = reqData.projects[0].projectWithDrawnReason;
                    underwritingRec1.Withdraw_Reason_Detail__c = reqData.projects[0].projectWithDrawnDetail; // Added as part of 3624
                    underwritingRec1.Declined_Withdrawn_Date__c = System.now();
                    update underwritingRec1;
                     responseBean = SLFUtility.getUnifiedBean(new set<Id>{oppRec.Id},'Orange',false);
                     responseBean.returnCode = '200';
                } 
            }else{
                   responseBean.returnCode = '365';
                   errMsg = ErrorLogUtility.getErrorCodes('365',null);
                   }
            }
             // Added as Part of 7051 by Sreekar - End
           
            if(String.isNotBlank(errMsg)){
                iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                responseBean.error.add(errorMsg);
                iRestRes = (IRestResponse)responseBean;
                res.response = iRestRes;
                return responseBean;
            }
               
            system.debug('###..response'+res.response);              
            iRestRes = (IRestResponse)responseBean;
            res.response = iRestRes;
            system.debug('###..response'+res.response);   
            
        }catch(Exception err){
            system.debug('### ProjectWithdrawServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' ProjectWithdrawServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
        }
        system.debug('### Exit from fetchData() of ' + ProjectWithdrawServiceImpl.class);
        
        return responseBean;
        
    }
}