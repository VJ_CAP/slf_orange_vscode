/**
* Description: Resend Stip Email Integration logic.
*
* Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma           10/24/2019          Created
************************************************************************************/
public without sharing class ResendStipEmailDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+ResendStipEmailDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean respData = new UnifiedBean();
        respData.error = new List<UnifiedBean.ErrorWrapper>();
        respData.projects = new List<UnifiedBean.OpportunityWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        
        IRestResponse iRestRes;
        string errMsg = '';
        String opportunityId = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            try{
                List<Opportunity> opprLst = new List<Opportunity>();
                if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
                {                 
                    errMsg = ErrorLogUtility.getErrorCodes('203',null);
                    respData.returnCode = '203';                
                } 
                else if(
                    (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                    ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                    ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                    ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
                {
                    errMsg = ErrorLogUtility.getErrorCodes('201',null);
                    respData.returnCode = '201';
                }else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
                {
                    opprLst = [SELECT Id FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                    
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        respData.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                    else{
                        opportunityId = opprLst.get(0).Id;
                    }
                }
                else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
                {
                    String externalId;
                    List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                    if(!loggedInUsr.isEmpty())
                    {
                        if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                            externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                        }else{
                            respData.returnCode = '400';
                            errMsg = ErrorLogUtility.getErrorCodes('400',null);
                        }
                    }
                    if(String.isBlank(errMsg)){
                        opprLst = [select id from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                        if (opprLst==null || opprLst.size() == 0)
                        {
                            respData.returnCode = '202';
                            errMsg = ErrorLogUtility.getErrorCodes('202',null);
                        }
                        else{
                            opportunityId = opprLst.get(0).id;
                        }
                        if (!opprLst.isEmpty()){
                            if(opprLst.get(0).ACH_APR_Process_Required__c==true){
                                // projectWrap.nonACHApr=opprLst.NonACH_APR__c;
                            }
                        }
                    }
                    
                }else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
                {
                    opprLst = [select id from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                    if (opprLst!=null && opprLst.size() > 0)
                    {
                        opportunityId = opprLst.get(0).id;
                    }else {
                        respData.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                } 
                
                if(String.isBlank(errMsg) && String.isNotBlank(opportunityId))
                {
                    set<Id> OppId = new set<Id>();
                    OppId.add(opportunityId);
                    List<Stipulation__c> stipList = [select id,Case__c,status__c,Installer_Only_Email__c,Underwriting__r.Opportunity__c,Underwriting__r.Opportunity__r.Primary_Applicant_Email__c from Stipulation__c where Underwriting__r.Opportunity__c IN: OppId AND Status__c != 'Completed' AND Installer_Only_Email__c = false];
                    Map<id,Case> caseUpdateMap = new Map<id,Case>();
                    if(!stipList.isEmpty())
                    {
                        for(Stipulation__c stipIterate : stipList)
                        {
                            if(null != stipIterate.Case__c)
                            {
                                Case caseRec = new Case();
                                if(caseUpdateMap.containsKey(stipIterate.Case__c))
                                    caseRec = caseUpdateMap.get(stipIterate.Case__c);
                                else
                                    caseRec.id = stipIterate.Case__c;
                                
                                caseRec.Resend_Email__c = true;
                                if(string.isBlank(caseRec.Stipulation_Status__c))
                                {
                                    caseRec.Stipulation_Status__c  = stipIterate.Status__c;
                                }else{
                                    if(!caseRec.Stipulation_Status__c.contains(stipIterate.Status__c))
                                    {
                                        caseRec.Stipulation_Status__c = caseRec.Stipulation_Status__c +';'+stipIterate.Status__c;
                                    }
                                }
                                caseRec.Stip_Email_1__c = stipIterate.Underwriting__r.Opportunity__r.Primary_Applicant_Email__c;
                                caseUpdateMap.put(stipIterate.Case__c,caseRec);
                            }
                        }
                        if(!caseUpdateMap.isEmpty())
                            update caseUpdateMap.Values();
                        
                        respData.returnCode = '200';    
                        //respData.message = 'Success! Stip email resend initiated.'; //commented and modified as part of Orange-14707
                        respData.message  = 'The stip email has been resent.';
                    }else{
                        respData.returnCode = '319';    
                        errMsg = ErrorLogUtility.getErrorCodes('319', null);
                        //respData.message = 'Success! Stip email resend initiated.';   
                    }
                    system.debug('caseUpdateMap=========='+caseUpdateMap);
                                    
                    system.debug('respData=========='+respData);
                    iRestRes = (IRestResponse)respData;
                    system.debug('### iRestRes '+iRestRes);
                }
                if(string.isNotBlank(errMsg))
                {
                    //Database.rollback(sp);
                    iolist = new list<DataValidator.InputObject>{};
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    errMsg = errorMsg.errorMessage;
                    respData.error.add(errorMsg);
                }
            }catch(Exception ex){
                iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400',null);
                
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                respData.error = new List<UnifiedBean.ErrorWrapper>();
                respData.error.add(errorMsg);
                respData.returnCode = '400';
                system.debug('### ResendStipEmailDataServiceImpl.transformOutput():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
                ErrorLogUtility.writeLog('ResendStipEmailDataServiceImpl.transformOutput()',ex.getLineNumber(),'transformOutput()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
            }    
        system.debug('### Exit from transformOutput() of '+ResendStipEmailDataServiceImpl.class);
        system.debug('### iRestRes'+iRestRes);
        iRestRes = (IRestResponse)respData;
        return iRestRes;
    }
 
}