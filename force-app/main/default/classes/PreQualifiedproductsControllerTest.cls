@IsTest 
public class PreQualifiedproductsControllerTest {
    
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
    }
    
    private testMethod static void prequaltest(){
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account accObj = [select id from Account where id= :loggedInUsr[0].contact.AccountId];
        
        Account personAcc = [SELECT Id FROM Account WHERE IsPersonAccount = true LIMIT 1]; 
        //Account facilityAcc = [select id from Account where Name =: 'Technology Credit Union' Limit 1];
        
        Opportunity oppObj = [select Id,Name,Hash_Id__c,Partner_Foreign_Key__c from opportunity where AccountId =: personAcc.Id LIMIT 1]; 
        
        Product__c prod = [select id,Is_Active__c,State_Code__c,Installer_Account__c,Internal_Use_Only__c,APR__c,Term_mo__c,Product_Tier__c,Product_Loan_Type__c from Product__c where Installer_Account__c =: accObj.id AND Internal_Use_Only__c = false LIMIT 1];
        System.runAs(loggedInUsr[0])
        {
            Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
            
            //create facility account
            Account facilityAcc = new Account();
            facilityAcc.name = 'Technology Credit Union';
            facilityAcc.RecordTypeId = recTypeInfoMap.get('Facility').getRecordTypeId();
            facilityAcc.FNI_Domain_Code__c = 'TCU1';
            insert facilityAcc;
            
            Prequal__c prqlObj = new Prequal__c(Opportunity__c = oppObj.Id,
                                                Installer_Account__c =accObj.Id,
                                                Middle_Name__c = 'K',
                                                SSN__c = '123456789',
                                                Term_mo__c = 2,
                                                DOB__c = system.today(),
                                                Mailing_State__c='CA',
                                                Mailing_Postal_Code__c = '25846',
                                                Annual_Income__c = 12300,
                                                Lending_Facility__c = 'CVX,TCU',
                                                Last_Name__c = 'Test',
                                                Employment_Years__c=5,
                                                Employment_Months__c = 1,
                                                Co_LastName__c = 'Test',
                                                Co_FirstName__c = 'Test',
                                                Co_Middle_Initial__c = 'w',
                                                Co_SSN__c = '123456758',
                                                Co_Date_of_Birth__c = System.today(),
                                                Co_Mailing_Street__c = 'Street',
                                                Co_Mailing_City__c = 'City',
                                                Co_Mailing_State__c = 'CA',
                                                Co_Mailing_Postal_Code__c = '45678',
                                                Co_Employer_Name__c = 'Employee',
                                                Co_Annual_Income__c = 200000,
                                                Co_Employment_Years__c = 1,
                                                Co_Employment_Months__c = 2,
                                                APR__c = 20
                                                
                                               );
            
            insert prqlObj;
            List<Prequal_Decision__c> preqDec = new List<Prequal_Decision__c>();
            Prequal_Decision__c prequalDecision = new Prequal_Decision__c ( Prequal__c = prqlObj.id,Product_Tier__c=0,Prequal_Descision__c='A',Facility_Name__c=facilityAcc.id
                                                                          );
            preqDec.add(prequalDecision);
            Prequal_Decision__c prequalDecision1 = new Prequal_Decision__c ( Prequal__c = prqlObj.id,Product_Tier__c=1,Prequal_Descision__c='A',Facility_Name__c=facilityAcc.id
                                                                           );                           
            preqDec.add(prequalDecision1);
            
            insert preqDec;
            
            Map<String, String> createincenMap = new Map<String, String>();
            
            String prequalRefactor;
            
            Test.startTest();
            
            PreQualifiedproductsController   productcontroller = new PreQualifiedproductsController();
            productcontroller.PrequalId = prqlObj.Id;
            productcontroller.getProductDetails();
            
            Prequal__c prequalObj = [SELECT id, name FROM Prequal__c where Opportunity__c =: oppObj.Id  LIMIT 1];
            Prequal_Decision__c  prequalDecsObj = [SELECT id, name FROM Prequal_Decision__c  where Prequal__c =: prqlObj.id  LIMIT 1];
            List<Product__c>  productList = [select id FROM Product__c where Installer_Account__c =:accObj.Id AND Internal_Use_Only__c = false];
            
            prod.is_Active__c = true;
            prod.Long_Term_Facility_Lookup__c = facilityAcc.id;
            update prod;
            system.debug('@@@@Product - '+prod+'        @@##PreqDecision'+prequalDecision.Facility_Name__c);
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"id":"'+prequalObj.Id+'","isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal');
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"id":"'+prequalObj.Id+'"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal'); 
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "2012-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal');
            
            
            oppObj.Project_Category__c = 'Home';
            update oppObj;
            prequalObj.Middle_Name__c = 'D';
            update prequalObj;
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"id":"'+prequalObj.Id+'","isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal');
            
            Test.stopTest();
            
        } 
    }
    
    private testMethod static void prequaltest2(){
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account accObj = [select id from Account where id= :loggedInUsr[0].contact.AccountId];
        
        Account personAcc = [SELECT Id FROM Account WHERE IsPersonAccount = true LIMIT 1]; 
        //Account facilityAcc = [select id from Account where Name =: 'Technology Credit Union' Limit 1];
        
        Opportunity oppObj = [select Id,Name,Hash_Id__c,Partner_Foreign_Key__c from opportunity where AccountId =: personAcc.Id LIMIT 1]; 
        oppObj.Project_Category__c = 'Solar';
        update oppObj;
        
        Product__c prod = [select id,Is_Active__c,State_Code__c,Installer_Account__c,Internal_Use_Only__c,APR__c,Term_mo__c,Product_Tier__c,Product_Loan_Type__c from Product__c where Installer_Account__c =: accObj.id AND Internal_Use_Only__c = false LIMIT 1];
        
        System.runAs(loggedInUsr[0])
        {
            Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
            
            //create facility account
            Account facilityAcc = new Account();
            facilityAcc.name = 'Technology Credit Union';
            facilityAcc.RecordTypeId = recTypeInfoMap.get('Facility').getRecordTypeId();
            facilityAcc.FNI_Domain_Code__c = 'TCU1';
            insert facilityAcc;
            
            Prequal__c prqlObj = new Prequal__c(Opportunity__c = oppObj.Id,
                                                Installer_Account__c =accObj.Id,
                                                Middle_Name__c = 'K',
                                                SSN__c = '123456789',
                                                Term_mo__c = 2,
                                                DOB__c = system.today(),
                                                Mailing_State__c='CA',
                                                Mailing_Postal_Code__c = '25846',
                                                Annual_Income__c = 12300,
                                                Lending_Facility__c = 'CVX,TCU',
                                                Last_Name__c = 'Test',
                                                Employment_Years__c=5,
                                                Employment_Months__c = 1,
                                                Co_LastName__c = 'Test',
                                                Co_FirstName__c = 'Test',
                                                Co_Middle_Initial__c = 'w',
                                                Co_SSN__c = '123456758',
                                                Co_Date_of_Birth__c = System.today(),
                                                Co_Mailing_Street__c = 'Street',
                                                Co_Mailing_City__c = 'City',
                                                Co_Mailing_State__c = 'CA',
                                                Co_Mailing_Postal_Code__c = '45678',
                                                Co_Employer_Name__c = 'Employee',
                                                Co_Annual_Income__c = 200000,
                                                Co_Employment_Years__c = 1,
                                                Co_Employment_Months__c = 2,
                                                APR__c = 20
                                                
                                               );
            
            insert prqlObj;
            List<Prequal_Decision__c> preqDec = new List<Prequal_Decision__c>();
            Prequal_Decision__c prequalDecision = new Prequal_Decision__c ( Prequal__c = prqlObj.id,Product_Tier__c=0,Prequal_Descision__c='A',Facility_Name__c=facilityAcc.id
                                                                          );
            preqDec.add(prequalDecision);
            Prequal_Decision__c prequalDecision1 = new Prequal_Decision__c ( Prequal__c = prqlObj.id,Product_Tier__c=1,Prequal_Descision__c='A',Facility_Name__c=facilityAcc.id
                                                                           );                           
            preqDec.add(prequalDecision1);
            
            insert preqDec;
            
            Map<String, String> createincenMap = new Map<String, String>();
            
            String prequalRefactor;
            
            Test.startTest();
            
            PreQualifiedproductsController   productcontroller = new PreQualifiedproductsController();
            productcontroller.PrequalId = prqlObj.Id;
            productcontroller.getProductDetails();
            
            Prequal__c prequalObj = [SELECT id, name FROM Prequal__c where Opportunity__c =: oppObj.Id  LIMIT 1];
            Prequal_Decision__c  prequalDecsObj = [SELECT id, name FROM Prequal_Decision__c  where Prequal__c =: prqlObj.id  LIMIT 1];
            List<Product__c>  productList = [select id FROM Product__c where Installer_Account__c =:accObj.Id AND Internal_Use_Only__c = false];
            
            prod.is_Active__c = true;
            prod.Long_Term_Facility_Lookup__c = facilityAcc.id;
            prod.Product_Loan_Type__c = 'HII';
            update prod;
            system.debug('@@@@Product - '+prod+'        @@##PreqDecision'+prequalDecision.Facility_Name__c);
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"id":"'+prequalObj.Id+'","isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal');
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"id":"'+prequalObj.Id+'"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal'); 
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "2012-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal');
            
            
            Test.stopTest();
            
        } 
    }
    private testMethod static void prequaltest3(){
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account accObj = [select id from Account where id= :loggedInUsr[0].contact.AccountId];
        
        Account personAcc = [SELECT Id FROM Account WHERE IsPersonAccount = true LIMIT 1]; 
        //Account facilityAcc1 = [select id from Account where Name = 'Test Account' Limit 1];
        
        Opportunity oppObj = [select Id,Name,Hash_Id__c,Partner_Foreign_Key__c from opportunity where AccountId =: personAcc.Id LIMIT 1]; 
        oppObj.Project_Category__c = 'Home';
        update oppObj;
        
        Product__c prod = [select id,Is_Active__c,State_Code__c,Installer_Account__c,Internal_Use_Only__c,APR__c,Term_mo__c,Product_Tier__c,Product_Loan_Type__c,Long_Term_Facility_Lookup__c from Product__c where Installer_Account__c =: accObj.id AND Internal_Use_Only__c = false LIMIT 1];
        
        System.debug('###testprod'+prod);
        System.runAs(loggedInUsr[0])
        {
            Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
            Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
            //create facility account
            Account facilityAcc = new Account();
            facilityAcc.name = 'Technology Credit Union';
            facilityAcc.RecordTypeId = recTypeInfoMap.get('Facility').getRecordTypeId();
            facilityAcc.FNI_Domain_Code__c = 'TCU1';
            insert facilityAcc;
            
			prod.Product_Tier__c = '1';
        	prod.Product_Loan_Type__c = 'HII';
        	prod.Is_Active__c = true;
            prod.Long_Term_Facility_Lookup__c = facilityAcc.Id;
        	update prod;            
            Prequal__c prqlObj = new Prequal__c(Opportunity__c = oppObj.Id,
                                                Installer_Account__c =accObj.Id,
                                                Middle_Name__c = 'K',
                                                SSN__c = '123456789',
                                                Term_mo__c = 2,
                                                DOB__c = system.today(),
                                                Mailing_State__c='CA',
                                                Mailing_Postal_Code__c = '25846',
                                                Annual_Income__c = 12300,
                                                Lending_Facility__c = 'CVX,TCU',
                                                Last_Name__c = 'Test',
                                                Employment_Years__c=5,
                                                Employment_Months__c = 1,
                                                Co_LastName__c = 'Test',
                                                Co_FirstName__c = 'Test',
                                                Co_Middle_Initial__c = 'w',
                                                Co_SSN__c = '123456758',
                                                Co_Date_of_Birth__c = System.today(),
                                                Co_Mailing_Street__c = 'Street',
                                                Co_Mailing_City__c = 'City',
                                                Co_Mailing_State__c = 'CA',
                                                Co_Mailing_Postal_Code__c = '45678',
                                                Co_Employer_Name__c = 'Employee',
                                                Co_Annual_Income__c = 200000,
                                                Co_Employment_Years__c = 1,
                                                Co_Employment_Months__c = 2,
                                                APR__c = 20
                                               );
            
            insert prqlObj;
            List<Prequal_Decision__c> preqDec = new List<Prequal_Decision__c>();
            
            Prequal_Decision__c prequalDecision1 = new Prequal_Decision__c ( Prequal__c = prqlObj.id,Product_Tier__c=1,Prequal_Descision__c='A',Facility_Name__c=facilityAcc.Id
                                                                           );                           
            preqDec.add(prequalDecision1);
            
            insert preqDec;
            
            Map<String, String> createincenMap = new Map<String, String>();
            
            String prequalRefactor;
            
            Test.startTest();
            
            PreQualifiedproductsController   productcontroller = new PreQualifiedproductsController();
            productcontroller.PrequalId = prqlObj.Id;
            productcontroller.getProductDetails();
            
            Prequal__c prequalObj = [SELECT id, name FROM Prequal__c where Opportunity__c =: oppObj.Id  LIMIT 1];
            Prequal_Decision__c  prequalDecsObj = [SELECT id, name FROM Prequal_Decision__c  where Prequal__c =: prqlObj.id  LIMIT 1];
            List<Product__c>  productList = [select id FROM Product__c where Installer_Account__c =:accObj.Id AND Internal_Use_Only__c = false];
            
            prod.is_Active__c = true;
            prod.Long_Term_Facility_Lookup__c = facilityAcc.id;
            prod.Product_Loan_Type__c = 'HII';
            update prod;
            
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"id":"'+prequalObj.Id+'","isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal');
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "1961-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"id":"'+prequalObj.Id+'"}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal'); 
            
            prequalRefactor = '{"projects": [{"id": "'+oppObj.Id+'","applicants": [{"id": "'+accObj.Id+'","annualIncome": 20000,"firstName":"YESHAR","lastName": "DONALD","middleName": "DONALD","ssn": "123456789","lastFourSSN": "1234","email":"test@test.com","PersonEmail": "test@test.com","dateOfBirth": "2012-1-1","phone": "4154571263","otherPhone":"4154571263","mailingStreet": "40 CORTE ALTA","mailingCity": "California","mailingZipCode": "94949","residenceStreet": "40 CORTE ALTA","residenceCity": "California","residenceZipCode": "94949","employerName": "CSTONE","employmentMonths": 9,"employmentYears": 1,"jobTitle": "Developer","isPrimary": true}],"prequals":[{"isPrequalAuthorized": true}]}]}';
            SLFRestDispatchTest.MapWebServiceURI(createincenMap,prequalRefactor,'prequal');
            
            
            Test.stopTest();
            
        } 
    }
    
}