public class SLFProductTiggerHandler{
    public void beforeInsert(List<Product__c> newList){
        system.debug('### Entered into beforeInsert() of '+ SLFProductTiggerHandler.class);
        updateTermFacilityLookups(newList);
        system.debug('### Exit from beforeInsert() of '+ SLFProductTiggerHandler.class);
    }
    public void beforeUpdate(Map<Id,Product__c> newMap, Map<Id,Product__c> oldMap){
        system.debug('### Entered into beforeUpdate() of '+ SLFProductTiggerHandler.class);
        updateTermFacilityLookups(newMap.values());
        sendEmailtoCustomersonProductExpiration(newMap.values(),oldMap);
        system.debug('### Exit from beforeUpdate() of '+ SLFProductTiggerHandler.class);
    }
    public void updateTermFacilityLookups(List<Product__c> newList){
        system.debug('### Entered into updateTermFacilityLookups() of '+ SLFProductTiggerHandler.class);
        set<String> facilityKeys = new set<String>{};
            map<String,Account> facilityAccountsMap = new map<String,Account>{};        
                for (Product__c p : newList)
            {
                if (p.Short_Term_Facility__c != null) 
                    facilityKeys.add (p.Short_Term_Facility__c);
                if (p.Long_Term_Facility__c != null) 
                    facilityKeys.add (p.Long_Term_Facility__c);
            }        
        for (Account a : [SELECT Id, Facility_Picklist_Key__c FROM Account WHERE RecordType.Name = 'Facility' AND Facility_Picklist_Key__c IN :facilityKeys])
        {
            facilityAccountsMap.put (a.Facility_Picklist_Key__c, a);
        }        
        for (Product__c p : newList)
        {
            Account s = facilityAccountsMap.get (p.Short_Term_Facility__c);            
            if (s != null) 
                p.Short_Term_Facility_Lookup__c = s.Id;            
            Account l = facilityAccountsMap.get (p.Long_Term_Facility__c);            
            if (l != null) 
                p.Long_Term_Facility_Lookup__c = l.Id;
        }
        system.debug('### Exit from updateTermFacilityLookups() of '+ SLFProductTiggerHandler.class);
    }
    // Added this for 1642 
    public void sendEmailtoCustomersonProductExpiration(List<Product__c> newList,Map<Id,Product__c> oldMap){
        Set<Id> ProductIds = new Set<Id>();  
        Map<Id,Product__c> mapProduct = new Map<Id,Product__c>(); // From the quote relation ship query not able to retrive the expiration date so using map to get the expiration date
        for (Product__c p : newList){
            if(p.expiration_date__c != null && oldMap.get(p.Id).expiration_date__c != p.expiration_date__c && p.is_Active__c == false && oldMap.get(p.Id).is_Active__c == true){
                productIds.add(p.Id);
                mapProduct.put(p.Id,p);
            }
        }
        List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage>();
        List<document> documentList = [select id,name from document where Name='Sunlight Logo' LIMIT 1];
        string strOrgId = UserInfo.getOrganizationId();
        // get instance of org. ex: cs19.salesforce.com
        string strOrgInst = URL.getSalesforceBaseUrl().getHost();
        strOrgInst = strOrgInst.substring(0, strOrgInst.indexOf('.')) + '.content.force.com';
        string strDocUrl = URL.getSalesforceBaseUrl().getProtocol() + '://c.' + strOrgInst + '/servlet/servlet.ImageServer?id=' + documentList[0].Id + '&oid=' + strOrgId;
        system.debug('=========='+strDocUrl);
        Datetime pastDate = system.today().addDays(-10);
        List<OrgWideEmailAddress> senderEmail = [select id,DisplayName, Address from OrgWideEmailAddress where DisplayName ='Sunlight Financial Support' LIMIT 1];
        List<Opportunity> lstOpp = [SELECT Id,(SELECT Id,IsSyncing,SLF_Product__c,SLF_Product_Name__c,SLF_Product__r.Expiration_Date__c,Account.PersonEmail,Account.Name FROM Quotes WHERE IsSyncing = TRUE and SLF_Product__c in:productIds and CreatedDate >=: PastDate  limit 1),(select Id,Status__c from Credits__r where Status__c = 'Auto Approved') FROM Opportunity WHERE SLF_Product__c in:productIds and StageName in ('Credit Application Sent','New')];
        for(Opportunity opp:lstOpp){
            if(opp.Credits__r.IsEmpty()){ 
                for(Quote objQ:opp.Quotes){
                    if(objQ.SLF_Product__c != null && mapProduct != null && mapProduct.get(objQ.SLF_Product__c) != null){
                        String expDt = mapProduct.get(objQ.SLF_Product__c).Expiration_Date__c.format();
                        string strHTMLBody = 'Hi '+objQ.Account.Name+',<br/><br/>';
                        strHTMLBody+= 'Just a friendly reminder that your product '+ objQ.SLF_Product_Name__c + ' will expire on '+ expDt +'. To proceed with this product, please complete the credit process before the expiration date.<br/><br/>';
                        strHTMLBody+= 'Questions? Please contact Sunlight Financial at (888) 850-3359, option 1 or '+ '<a href="mailto:support@sunlightfinancial.com"> support@sunlightfinancial.com.  </a> <br/><br/>';
                        strHTMLBody+= 'Thank you,<br/>';
                        strHTMLBody += 'The Sunlight Financial team <br/>';  
                        strHTMLBody += '<img src="'+strDocUrl+'"  /> <br/>' ;   
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        List<String> ToAddresses = new List<String>();
                        ToAddresses.add(objQ.Account.PersonEmail);
                        message.setToAddresses(ToAddresses);
                        if(!senderEmail.isEmpty()){
                            message.setOrgWideEmailAddressId(senderEmail.get(0).id); 
                        }
                        message.setSubject('Sunlight Financial Alert - Your Product is About to Expire!');  
                        message.setHtmlBody(strHtmlBody);
                        lstEmails.add(message); 
                    }
                }   
            }
        }
        system.debug('lstEmails========='+lstEmails);
        if(!lstEmails.isEmpty()){
            Messaging.sendemail(lstEmails);
        }
    }
}