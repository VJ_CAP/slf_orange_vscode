/**
* Description: Import milestone requests from Sighten into Salesforce.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           12/20/2018          Created
******************************************************************************************/
public class ImportMilestoneSightenResponse {

    public class Messages {
        public List<Error> critical;
        public List<Error> error;
        public List<Error> info;
        public List<Error> warning;
    }

    public Integer status_code;
    public List<Data> data;
    public Messages messages;
    public String pages;
    public String has_next_page; 
    public String count;
    
    public class Error {
        public String timestamp;
        public String message;
        public String error_code;
        public String resolution;
        public String msg_code;
        public String reason;
    }
    
    public class Data {
        public List<String> milestone_name;
        public List<String> milestone_status;
        public List<String> milestone_status_abbreviated;
        public String site_id;
        public String organization_name;
        public String site_last_updated;
        public List<String> milestone_id;
        public List<String> milestone_submitted_by_name;
        public List<String> milestone_date_submitted;
    }

    public static ImportMilestoneSightenResponse parse(String json) {
        return (ImportMilestoneSightenResponse) System.JSON.deserialize(json, ImportMilestoneSightenResponse.class);
    }
}