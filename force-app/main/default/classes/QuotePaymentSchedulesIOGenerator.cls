/**
* Description: Payment Schedules for Interest Only Business logic associated with Quote is handled here. 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma K         09/30/2019          Created 
******************************************************************************************/
public class QuotePaymentSchedulesIOGenerator {
    
    public static void run(){
        
        if(!(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))) return;
        
        Quote[] quotes = getQuotesInScope();
        
        if(trigger.isUpdate && null != quotes && !quotes.isEmpty())
            deleteStalePaymentSchedules(quotes);
        
        system.debug('quotes - '+quotes);
        if(null != quotes && !quotes.isEmpty())
            generateNewPaymentSchedules(quotes);
    }
    
    private static Quote[] getQuotesInScope(){
        
        Quote[] quotes = (Quote[])trigger.new;
        
        Quote[] ret = new Quote[]{};
        
        system.debug('### quotes - '+quotes);
        
        for(Quote quote : quotes){
            if(isInScope(quote)) 
            {
                system.debug('### isInScope - '+quote.Id+' : true');
                ret.add(quote);
            }
            else
            {
                system.debug('### isInScope - '+quote.Id+' : false');
            }
        }
        system.debug('### ret - '+ret);
        
        return ret;
    }
    
    private static Boolean isInScope(Quote quote){
                        
        if(trigger.isInsert){
            system.debug('### Checkpoint returning - '+quote.Combined_Loan_Amount__c != null && quote.SLF_Product__c != null);
            return quote.Combined_Loan_Amount__c != null && quote.SLF_Product__c != null && quote.Product_Loan_Type__c == 'Solar Interest Only';
        }
        else if(trigger.isUpdate){
            system.debug('### quote.Combined_Loan_Amount__c - '+quote.Combined_Loan_Amount__c);
            system.debug('### Trg Old Value - '+trigger.oldMap.get(quote.Id).get('Combined_Loan_Amount__c'));
            system.debug('### Quote Product - '+quote.SLF_Product__c);
            system.debug('### Trg Old Quote Product - '+trigger.oldMap.get(quote.Id).get('SLF_Product__c'));
            return (quote.Combined_Loan_Amount__c != trigger.oldMap.get(quote.Id).get('Combined_Loan_Amount__c')
                || quote.SLF_Product__c != trigger.oldMap.get(quote.Id).get('SLF_Product__c')) && quote.Product_Loan_Type__c == 'Solar Interest Only';
              
        }
        else{
            return false;
        }
    }
    
    private static void deleteStalePaymentSchedules(Quote[] quotes){
        
        if(quotes.isEmpty()) return;
        
        delete [SELECT Id FROM Payment_Schedule_Interest_Only__c WHERE Quote__c IN: quotes];
    }
    
    private static void generateNewPaymentSchedules(Quote[] quotes){
    
        system.debug('### Entered into generateNewPaymentSchedules() of '+ QuotePaymentSchedulesIOGenerator.class);
        System.debug('quotes - generateNewPaymentSchedules'+quotes);
        
        List<Payment_Schedule_Interest_Only__c> newPaymentSchedules = new List<Payment_Schedule_Interest_Only__c>();
        
        Map<Id, Product__c> productMap = getProductMap(quotes);
        
        Map<Id,Quote> updateQuoteMap = new Map<Id,Quote>(); 
        
        Map<Id,Quote> quotewithOppMap = new Map<Id,Quote>([Select id,Paydown_percent__c,Combined_Loan_Amount__c,isACH__c,Product_Loan_Type__c, Opportunity.Long_Term_APR__c, Opportunity.Promo_APR__c from Quote where id in :trigger.newMap.KeySet()]);
        for(Quote quote : quotes){
            if(quote.Combined_Loan_Amount__c != null && productMap.containsKey(quote.SLF_Product__c)){
                Quote quoteRec = new Quote(Id=quote.id);

                Product__c product = productMap.get(quote.SLF_Product__c);
                 system.debug('### quote: '+quote);
                 system.debug('### product: '+product);
                 system.debug('### quoteRec: '+quoteRec);
                if(!quotewithOppMap.isEmpty() && quotewithOppMap.containsKey(quote.Id)){
                    newPaymentSchedules.addAll(createPaymentSchedules(quotewithOppMap.get(quote.Id), quote.Combined_Loan_Amount__c, product,quote.Product_Loan_Type__c,quote.isACH__c,null));
                }
                system.debug('### isACH - '+quote.isACH__c);
                system.debug('### newPaymentSchedules - '+newPaymentSchedules);
                
                Map<Id,List<Payment_Schedule_Interest_Only__c>> quoteMap = new Map<Id,List<Payment_Schedule_Interest_Only__c>>();
                for(Payment_Schedule_Interest_Only__c paymentIterate : newPaymentSchedules){
                    if(quoteMap.containsKey(paymentIterate.Quote__c))
                    {
                        List<Payment_Schedule_Interest_Only__c> paymentRec = quoteMap.get(paymentIterate.Quote__c);
                        paymentRec.add(paymentIterate);
                        quoteMap.put(paymentIterate.Quote__c,paymentRec);
                    }else{
                        quoteMap.put(paymentIterate.Quote__c,new List<Payment_Schedule_Interest_Only__c>{paymentIterate});
                    }
                }
                
                
                List<Payment_Schedule_Interest_Only__c> respectiveQuotePaymentSchedules = quoteMap.get(quote.id);
                if(null != respectiveQuotePaymentSchedules)
                {
                    //quoteRec.Monthly_Payment_Interest_Only__c = respectiveQuotePaymentSchedules[1].Amount__c;
                   //quoteRec.Escalated_Monthly_Payment_IO__c = respectiveQuotePaymentSchedules[respectiveQuotePaymentSchedules.size()-1].Amount__c;
                    updateQuoteMap.put(quoteRec.Id,quoteRec);
                }
                
                quoteRec.ACH_Monthly_Payment_Interest_Only__c  = respectiveQuotePaymentSchedules[1].Amount__c;
                quoteRec.ACH_Escalated_Monthly_Payment_IO__c = respectiveQuotePaymentSchedules[2].Amount__c;
                
                quoteRec.NonACH_Monthly_Payment_IO__c = respectiveQuotePaymentSchedules[4].Amount__c;
                quoteRec.NonACH_Escalated_Monthly_Payment_IO__c = respectiveQuotePaymentSchedules[5].Amount__c;
                
            }

        }
        if(!newPaymentSchedules.isEmpty())
            insert newPaymentSchedules;
        
        if(!updateQuoteMap.isEmpty())
            update updateQuoteMap.values();
        
    }
    
    public static List<Payment_Schedule_Interest_Only__c> createPaymentSchedules(Quote quoteObj, Decimal loanAmount, Product__c product,string productType,Boolean isACH,Decimal paydownPercentage){
    
        system.debug('### Entered into createPaymentSchedules() of '+ QuotePaymentSchedulesIOGenerator.class);
        Decimal promoAPRInt = product.Promo_APR__c;
        Integer term = (Integer)product.Term_mo__c,
        drawPeriod = product.Draw_Period__c == null ? null : (Integer)product.Draw_Period__c, 
        promoPeriod = product.Promo_Term__c == null ? 0 : (Integer)product.Promo_Term__c;
        
        Decimal APR = product.APR__c == null ? 0 : product.APR__c/100,
        promoAPR = promoAPRInt == null ? 0 : promoAPRInt/100, 
        NonACHAPR = product.NonACH_APR__c == null ? 0 : product.NonACH_APR__c/100,
        NonACHpromoAPR = product.NonACH_Promo_APR__c == null ? 0 : product.NonACH_Promo_APR__c/100,
        promoFixedPayment = product.Promo_Fixed_Payment__c, 
        promoPercentBalance = product.Promo_Percentage_Balance__c == null ? null : product.Promo_Percentage_Balance__c/100, 
        promoPercentPayment = product.Promo_Percentage_Payment__c == null ? null : product.Promo_Percentage_Payment__c/100;
        
        List<LoanCalculator.PaymentSchedule> paymentScheduleLst = new List<LoanCalculator.PaymentSchedule>();
       // paymentScheduleLst = LoanCalculator.getPaymentSchedules(
        //              loanAmount, term, APR, drawPeriod, promoPeriod, promoAPR, promoPercentPayment, promoPercentBalance, promoFixedPayment);
        if((null != quoteObj && quoteObj.Product_Loan_Type__c == 'Solar Interest Only') || (string.isNotBlank(productType) && productType == 'Solar Interest Only')){
            //Start
            
            if(null != paydownPercentage)
                paydownPercentage = paydownPercentage/100;  
            else if(null != quoteObj && null != quoteObj.Paydown_percent__c)    
                paydownPercentage = quoteObj.Paydown_percent__c/100;
            else
                paydownPercentage = 0.3;
            
            Decimal deferredInterest = 0.00;
            Integer promoTerm = product.Promo_Term__c == null ? null : Integer.valueOf(product.Promo_Term__c); 
            
            if((null != quoteObj && quoteObj.isACH__c) || isACH)
            {
                APR = product.APR__c == null ? 0 : product.APR__c/100;
            }else{
                APR = product.NonACH_APR__c == null ? 0 : product.NonACH_APR__c/100;
            }
            APR = product.APR__c == null ? 0 : product.APR__c/100;
            NonACHAPR = product.NonACH_APR__c == null ? 0 : product.NonACH_APR__c/100;
            promoAPR = product.Promo_APR__c == null ? 0 : product.Promo_APR__c/100;
            NonACHpromoAPR = product.NonACH_Promo_APR__c == null ? 0 : product.NonACH_Promo_APR__c/100;
            
            //paymentScheduleLst = (LoanCalculator.getPaymentSchedulesInterestOnly(loanAmount,deferredInterest, term,promoTerm ,promoAPR,APR,paydownPercentage));
            
            string ACHType = 'ACH';
            paymentScheduleLst = (LoanCalculator.getPaymentSchedulesInterestOnly(loanAmount,deferredInterest, term,promoTerm ,promoAPR,APR,paydownPercentage,ACHType));
            
            ACHType = 'NonACH';
            paymentScheduleLst.addAll(LoanCalculator.getPaymentSchedulesInterestOnly(loanAmount,deferredInterest, term,promoTerm ,NonACHpromoAPR,NonACHAPR,paydownPercentage,ACHType));
        }               
                        
        system.debug('paymentScheduleLst=========='+paymentScheduleLst);
        
        List<Payment_Schedule_Interest_Only__c> ret = new List<Payment_Schedule_Interest_Only__c>();
        if(null != paymentScheduleLst)
        {
            id quoteId = null;
            if(null != quoteObj && null != quoteObj.id)
                quoteId = quoteObj.id;
            
            system.debug('paymentScheduleLst===size======='+paymentScheduleLst.size());
            system.debug('paymentScheduleLst=========='+paymentScheduleLst);
            for(Integer i = 0; i < paymentScheduleLst.size(); i++){
                system.debug('paymentScheduleLst[i].Payment============'+paymentScheduleLst[i].Payment);
                system.debug('paymentScheduleLst[i].Payment============'+paymentScheduleLst[i].Months);
                ret.add(new Payment_Schedule_Interest_Only__c(
                    Quote__c = quoteId,
                    Serial_No__c = i + 1,
                    Amount__c = paymentScheduleLst[i].Payment,
                    Months__c = paymentScheduleLst[i].Months,
                    ACH_Type__c = paymentScheduleLst[i].ACHType
                ));
            }
        }
        return ret;
    }
    
    private static Map<Id, Product__c> getProductMap(Quote[] quotes){
        
        system.debug('### Entered into getProductMap() of '+ QuotePaymentSchedulesIOGenerator.class);
        System.debug('### trigger context'+Trigger.isInsert);
        System.debug('### trigger context'+Trigger.isUpdate);
        Set<Id> productIds = new Set<Id>();
        
        for(Quote quote : quotes){
            
            if(quote.SLF_Product__c != null){
                productIds.add(quote.SLF_Product__c);
            }
        }
        
        if(productIds.isEmpty())
            return new Map<Id, Product__c>();
        
        return new Map<Id, Product__c>([SELECT Product_Loan_Type__c, Term_mo__c, APR__c,  Promo_APR__c,NonACH_Promo_APR__c,NonACH_APR__c,Promo_Fixed_Payment__c, 
            Promo_Percentage_Balance__c, Promo_Percentage_Payment__c, Promo_Term__c, Draw_Period__c
            FROM Product__c WHERE Id IN: productIds]);
    }
}