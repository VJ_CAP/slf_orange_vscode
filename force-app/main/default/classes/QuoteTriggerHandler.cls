/**
* Description: Trigger Business logic associated with Quote is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         09/06/2017          Created 
******************************************************************************************/
public with sharing Class QuoteTriggerHandler
{
    public static boolean isTrgExecuting = true;
    public static SLF_Boomi_API_Details__c objBoomiAPIDetails;
    
    /**
    * Constructor to initialize
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public QuoteTriggerHandler(boolean isExecuting){
        //isTrgExecuting = isExecuting;
    }
    
    public static boolean runOnce(){
        if(isTrgExecuting){
            isTrgExecuting=false;
            
            return true;
        }else{
            return isTrgExecuting;
        }
    }
    
    /**
    * Description: On After Insert
    *
    * @param new Quote records.
    */
      
    
    public  void onAfterInsert(Quote[] newQuote){
        try
        {
            system.debug('### Entered into onAfterInsert() of '+ QuoteTriggerHandler.class);
            //Ravi: To exclude change order archived Opportunities
            List<Quote> quoteList = new List<Quote>(); 
            Map<Id,List<Quote>> quoteMap = new Map<Id,List<Quote>>();
            Map<Id,Quote> oldQuoteMap = new Map<Id,Quote>();
            
            for(Quote quoteIterate : newQuote){
                if(quoteIterate.OpportunityId != null){
                   if(quoteMap.containsKey(quoteIterate.OpportunityId))
                    {
                        quoteMap.get(quoteIterate.OpportunityId).add(quoteIterate);
                    }else{
                        quoteMap.put(quoteIterate.OpportunityId,new List<Quote>{quoteIterate});
                    }
                    oldQuoteMap.put(quoteIterate.Id,quoteIterate);
                }
            }

            List<Quote> newQuoteAarchivedLst = SLFUtility.nonAarchivedOpportunities(quoteMap);
            if(!newQuoteAarchivedLst.isEmpty())
            {
                set<Id> oppIds = new set<Id>();
                createCreditOpinion(newQuote);
                //Commented by Rajesh on 08/24/2018 for Orange-1166
                //deSyncCreditRecord(newQuote);
                
                Set<Id> quoteSet = new Set<Id>();
                for(Quote quoteIterate : newQuoteAarchivedLst){
                    quoteSet.add(quoteIterate.Id);
                    if(quoteIterate.OpportunityId != null && quoteIterate.Push_Endpoint__c)
                        oppIds.add(quoteIterate.OpportunityId);
                }
                // Added by Suresh: Below if condition to avoid future call out if batch is running
                runPricing(quoteSet);
               if(!oppIds.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
                {
                    //Generate JSON Structure and create Status Audit Record
                    String jsonNewList = JSON.serialize(newQuoteAarchivedLst);
                    SLFUtility.callUnifiedJSON('Quote','quotes',oppIds,jsonNewList,null);
                    
                    //isTrgExecuting = false;
                }
            }
            system.debug('### Exit from onAfterInsert() of '+ QuoteTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### Exception in QuoteTriggerHandler.onAfterInsert():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('QuoteTriggerHandler.onAfterInsert()',ex.getLineNumber(),'onAfterInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    /**
    * Description: On After Update
    *
    * @param new Quote records.
    */
    public void onAfterUpdate(Quote[] newQuote,Map<Id,Quote> oldQuoteMap){
        try
        {
            system.debug('### Entered into onAfterUpdate() of '+ QuoteTriggerHandler.class);
            Map<Id,List<Quote>> quoteMap = new Map<Id,List<Quote>>();
            if(isTrgExecuting)
            {
                runOnce();
                for(Quote quoteIterate : newQuote){
                    if(quoteIterate.OpportunityId != null){
                       if(quoteMap.containsKey(quoteIterate.OpportunityId))
                        {
                            quoteMap.get(quoteIterate.OpportunityId).add(quoteIterate);
                        }else{
                            quoteMap.put(quoteIterate.OpportunityId,new List<Quote>{quoteIterate});
                        }
                    }
                }
            }
            List<Quote> newQuoteAarchivedLst = SLFUtility.nonAarchivedOpportunities(quoteMap);
            if(!newQuoteAarchivedLst.isEmpty())
            {
                system.debug('### into afterupdate ');
                set<Id> oppIds = new set<Id>();
                for(Quote quoteIterate : newQuoteAarchivedLst){
                    if(quoteIterate.OpportunityId != null && quoteIterate.Push_Endpoint__c)
                        oppIds.add(quoteIterate.OpportunityId);
                }
                if(!oppIds.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
                {
                   system.debug('### into Json creation '+oppIds);
                    //Generate JSON Structure and create Status Audit Record
                    String jsonNewList = JSON.serialize(newQuoteAarchivedLst);
                    String jsonOldMap = JSON.serialize(oldQuoteMap);
                    SLFUtility.callUnifiedJSON('Quote','quotes',oppIds,jsonNewList,jsonOldMap);
                }
            }
        }catch(Exception ex)
        {
            system.debug('### Exception in QuoteTriggerHandler.onAfterUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('QuoteTriggerHandler.onAfterUpdate()',ex.getLineNumber(),'onAfterUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from onAfterUpdate() of '+ QuoteTriggerHandler.class);
    }
    
    //Commented by Rajesh on 08/24/2018 for Orange-1166 (Need to check with daxesh)
    /**
    * Description: create credit opinion record
    *
    * @param new Quote records.
    */
    /*
    public static void deSyncCreditRecord(Quote[] newQuote){
        try{
            system.debug('### Entered into deSyncCreditRecord() of '+ QuoteTriggerHandler.class);
            set<Id> QuoteIdSet = new set<Id>();
            for(Quote quoteIterate : newQuote){
                if(null != quoteIterate.Monthly_Payment__c && quoteIterate.Monthly_Payment__c <> 0)
                    QuoteIdSet.add(quoteIterate.Id);
            }
            
            if(!QuoteIdSet.isEmpty())
            {
                system.debug('QuoteIdSet is not empty - '+QuoteIdSet);
                list<Quote> quoteLst = [select Id,Name,CreditApproved__c,(select Id,Quote__c,Credit__c from Credit_Opinion__r),OpportunityId from Quote where Id IN: QuoteIdSet];
                
                if(!quoteLst.isEmpty())
                {
                    system.debug('quoteLst is not empty - '+quoteLst);
                    set<Id> opptyIdset = new set<Id>(); 
                    set<Id> SLFCreditSet = new set<Id>();
                    Map<Id,SLF_Credit__c> deSyncSLFCreditMap = new Map<Id,SLF_Credit__c>();
                    for(Quote quoteIterate : quoteLst ){
                        opptyIdset.add(quoteIterate.OpportunityId);
                        if(!quoteIterate.Credit_Opinion__r.isEmpty())
                        {
                                for(Credit_Opinion__c creditOpnIterate : quoteIterate.Credit_Opinion__r){
                                    if(quoteIterate.CreditApproved__c == true)
                                    {
                                        if(null != creditOpnIterate.Credit__c)
                                        {
                                            opptyIdset.add(quoteIterate.OpportunityId);
                                            SLFCreditSet.add(creditOpnIterate.Credit__c);
                                        }
                                    }else{
                                        if(null != creditOpnIterate.Credit__c)
                                        {
                                            SLF_Credit__c slfRec = new SLF_Credit__c(id = creditOpnIterate.Credit__c);
                                            slfRec.IsSyncing__c = false;
                                            deSyncSLFCreditMap.put(creditOpnIterate.Credit__c,slfRec);
                                        }   
                                    }
                                     
                                }
                        }
                    }
                    system.debug('deSyncSLFCreditMap - '+deSyncSLFCreditMap);
                    if(!deSyncSLFCreditMap.isEmpty())
                    {
                        update deSyncSLFCreditMap.Values();
                    }
                        
                    //desync all credit records and sync new credit record
                    if(!SLFCreditSet.isEmpty())         
                        SyncDesyncUtilities.deSyncCreditRecord(opptyIdset,SLFCreditSet);
                          
                }   
            }
            system.debug('### Exit from deSyncCreditRecord() of '+ QuoteTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### Exception in QuoteTriggerHandler.deSyncCreditRecord():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
           ErrorLogUtility.writeLog('QuoteTriggerHandler.deSyncCreditRecord()',ex.getLineNumber(),'deSyncCreditRecord()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    */
    /**
    * Description: create credit opinion record
    *
    * @param new Quote records.
    */
    public  void createCreditOpinion(Quote[] newQuote){
        try{
            system.debug('### Entered into createCreditOpinion() of '+ QuoteTriggerHandler.class);
            set<Id> oppIdSet = new set<Id>();
            for(Quote quoteIterate : newQuote){
                oppIdSet.add(quoteIterate.OpportunityId);
            }
            system.debug('### oppIdSet '+oppIdSet.size());
            //New Credit opinion records creation for existing credit records related to Opportunity and associating with the new quote.
            if(!oppIdSet.isEmpty())
            {
                List<Credit_Opinion__c> creditOpnInsert = new List<Credit_Opinion__c>();
                List<SLF_Credit__c> creditupdatedlist = new List<SLF_Credit__c>();
                Map<Id,List<SLF_Credit__c>> creditMap = new Map<Id,List<SLF_Credit__c>>();
           
                List<SLF_Credit__c> slfCreditLst = [select Id,Name,Opportunity__r.Project_Category__c,SLF_Product__c,Co_Applicant__c,Long_Term_Amount_Approved__c,SLF_Product__r.Min_Loan_Amount__c,LT_Facility__c,Opportunity__c,IsSyncing__c,Status__c,Product_Tier__c,Product_Loan_Type__c from SLF_Credit__c where (Status__c =: 'Auto Approved' OR Status__c =: 'Manual Approved' OR Status__c =: 'Auto Declined' OR Status__c =: 'Manual Declined' OR Status__c =: 'Pending Review') AND Opportunity__c =: oppIdSet order by LastModifiedDate Desc];
                for(SLF_Credit__c creditIterate : slfCreditLst)
                {
                    if(creditMap.containsKey(creditIterate.Opportunity__c))
                    {
                        creditMap.get(creditIterate.Opportunity__c).add(creditIterate);
                    }else{
                        creditMap.put(creditIterate.Opportunity__c,new List<SLF_Credit__c>{creditIterate});
                    }
                }
                Set<id> credoppidSet = new Set<id>();
                
                Boolean syncLatestCredit = false;
                
                for(Quote quoteIterate : newQuote){
                    if(creditMap.containsKey(quoteIterate.OpportunityId))
                    {
                        List<SLF_Credit__c> slfCreditRecLst = creditMap.get(quoteIterate.OpportunityId);
                        if(!slfCreditRecLst.isEmpty())
                        {
                            for(SLF_Credit__c creditIterate : slfCreditRecLst)
                            {
                                Credit_Opinion__c crdOptnRec = new Credit_Opinion__c();
                                crdOptnRec.Quote__c = quoteIterate.id;
                                crdOptnRec.Credit__c = creditIterate.Id;
                                //Added Product_Loan_Type__c condetion as part of ORANGE-1595   
                                if(quoteIterate.Combined_Loan_Amount__c <= creditIterate.Long_Term_Amount_Approved__c && quoteIterate.Combined_Loan_Amount__c >= creditIterate.SLF_Product__r.Min_Loan_Amount__c && quoteIterate.LT_Product_Facility__c == creditIterate.LT_Facility__c && quoteIterate.Product_Tier__c == creditIterate.Product_Tier__c && (creditIterate.Opportunity__r.Project_Category__c == 'Solar' || (creditIterate.Opportunity__r.Project_Category__c == 'Home' && quoteIterate.Product_Loan_Type__c == creditIterate.Product_Loan_Type__c)))
                                {    
                                    crdOptnRec.Status__c = creditIterate.Status__c;
                                    
                                    //Srikanth - 03/29 - Made changes as per Orange-201 - Find the latest credit based on Credit Opinion from Quote
                                    if(!syncLatestCredit)
                                    {
                                        if(creditIterate.Status__c =='Auto Approved' || creditIterate.Status__c =='Manual Approved'){
                                            creditIterate.IsSyncing__c = true;
                                            syncLatestCredit = true;
                                            creditupdatedlist.add(creditIterate);
                                        }
                                    }
                                    
                                }   
                                else{
                                    crdOptnRec.Status__c = 'N/A';
                                    if(creditIterate.IsSyncing__c==true){
                                        creditIterate.IsSyncing__c=false;
                                        creditupdatedlist.add(creditIterate);
                                        credoppidSet.add(creditIterate.Opportunity__c);
                                    }
                                }
                                creditOpnInsert.add(crdOptnRec);
                            }
                        }
                    }
                }
                if(!creditOpnInsert.isEmpty())
                    insert creditOpnInsert;
                //Srikanth - 02_27 commented our desync logic due to other issues
                
                if(!credoppidSet.isEmpty())
                    SyncDesyncUtilities.DesyncCreditFields(credoppidSet);
                
                if(!creditupdatedlist.isEmpty())
                    update creditupdatedlist;
                
                
                    
                
            }
        }catch(Exception ex)
        {
            system.debug('### QuoteTriggerHandler.createCreditOpinion():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
          ErrorLogUtility.writeLog('QuoteTriggerHandler.createCreditOpinion()',ex.getLineNumber(),'createCreditOpinion()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from createCreditOpinion() of '+ QuoteTriggerHandler.class);
    }
    /*
    Public static void DesyncQuote(Set<Id> oppIds)
    {
        system.debug('### Entered into DesyncCreditandQuote() of '+ QuoteTriggerHandler.class);
        try
        {*/
            /*List<Opportunity> OppLst = [Select id,Name,SyncedQuoteid,Customer_has_authorized_credit_hard_pull__c,Short_Term_Amount_Approved__c,Long_Term_Amount_Approved__c,Application_Status__c From Opportunity where id IN :oppIds];
            List<SLF_Credit__c> creditsUpdate =new List<SLF_Credit__c>();
            List<Opportunity> oppstoUpdate =new List<Opportunity>();
            
                for(id opty :oppIds){
                    Opportunity opps = new Opportunity(id = opty);
                    opps.Application_Status__c = 'New';  //Srikanth - Revision - when Credit Opinion is N/A upon running a Quote
                    opps.Long_Term_Amount_Approved__c = null;
                    opps.Short_Term_Amount_Approved__c = null;
                    opps.Credit_Expiration_Date__c = null;
                    opps.Customer_has_authorized_credit_hard_pull__c = false;
                    opps.CL_City__c = null;
                    opps.CL_Owner_of_Record__c = null;
                    opps.CL_State_Code__c = null;
                    opps.CL_Street_Address__c = null;
                    opps.CL_ZipCode__c = null; 
                    // Orange 1291 Mapping
                    opps.Bureau_Primary_Applicant_First_Name__c = null;
                    opps.Bureau_Primary_Applicant_Middle_Name__c = null;
                    opps.Bureau_Primary_Applicant_Last_Name__c=null;
                    opps.Bureau_Primary_SSN__c = null;
                    opps.Bureau_Primary_DoB__c = null;
                    opps.Bureau_Co_Applicant_First_Name__c=null;
                    opps.Bureau_Co_Applicant_Middle_Name__c = null;
                    opps.Bureau_Co_Applicant_Full_Last__c = null;
                    opps.Bureau_Co_Applicant_SSN__c=null;
                    opps.Bureau_Co_Applicant_DoB__c = null;
                    opps.APN__c = null;
                    opps.Legal_Description__c=null; 
                    opps.Trust_Indicator__c = null;
                    opps.County__c=null;
                    
                    oppstoUpdate.add(opps);
                }
          
            if(!oppstoUpdate.isEmpty()){
                system.debug('### oppstoUpdate '+ oppstoUpdate);
                update oppstoUpdate;
            }
                
            system.debug('### Exit from DesyncCreditandQuote() of '+ QuoteTriggerHandler.class);
        }catch(Exception err){
                
            system.debug('### QuoteTriggerHandler.DesyncCreditandQuote():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
           ErrorLogUtility.writeLog('QuoteTriggerHandler.DesyncCreditandQuote()',err.getLineNumber(),'DesyncCreditandQuote()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));         
        }
    } 
    */
    /**
    * Description: Calling Boomi to do pricing
    *
    * @param new Quote records.
    */
    
    //@future(callout=true)
    public static void runPricing(Set<Id> quoteSet){
        try{ 
                /*********************System Design Syncing Logic**********************/

                //fetching all records from SystemDesign_to_Opportunity_Sync custom setting
                List<SystemDesign_to_Opportunity_Sync__c> syncRecs = SystemDesign_to_Opportunity_Sync__c.getall().values();
                String fieldnames='';
                
                //Adding all SystemDesign_to_Opportunity_Sync records to oppSyncingMap Map
                Map<string,string>  oppSyncingMap = new  Map<string,string>();  
                for(SystemDesign_to_Opportunity_Sync__c syncIterate : syncRecs)    
                {
                    oppSyncingMap.put(syncIterate.From_System_Design__c,syncIterate.To_Opportunity__c);
                    if (syncIterate.From_System_Design__c != null) {
                        fieldnames += ','+syncIterate.From_System_Design__c;
                    }
                } 
                
                Map<Id,set<Id>> quoteOppMap = new Map<Id,set<Id>>();
                List<System_Design__c> sysdesigns = new List<System_Design__c>();
                if(!quoteSet.isEmpty())
                {
                    List<Quote> syncQuotes = [Select Id, OpportunityId, opportunity.StageName, Combined_Loan_Amount__c, System_Design__c, SLF_Product__r.APR__c, SLF_Product__r.Term_mo__c From Quote Where id IN: quoteSet];
                    if(!syncQuotes.isEmpty())
                    {
                        for(Quote quoteIterate : syncQuotes){
                            if(quoteOppMap.containsKey(quoteIterate.System_Design__c))
                            {
                                quoteOppMap.get(quoteIterate.System_Design__c).add(quoteIterate.OpportunityId);
                            }else{
                                quoteOppMap.put(quoteIterate.System_Design__c,new set<Id>{quoteIterate.OpportunityId});
                            }
                        }
                        system.debug('### fieldnames  '+ fieldnames);
                        Set<Id> sysdesignSet = new Set<Id>();
                        if(!quoteOppMap.isEmpty())
                        {
                            sysdesignSet = quoteOppMap.keyset();
                        }
                        if(!String.isBlank(fieldnames)){
                            String sysdesignsQuery = 'Select Id,Opportunity__c'+fieldnames+' from System_Design__c Where id IN: sysdesignSet';
                            system.debug('### sysdesignsQuery  '+ sysdesignsQuery);
                            sysdesigns =  database.query(sysdesignsQuery);
                        }
                    }
                }
                if(!sysdesigns.isEmpty())
                {
                    Map<Id,sObject> oppUpdateMap = new Map<Id,sObject>();
                    for(System_Design__c sysIterate :  sysdesigns){
                        if(quoteOppMap.containsKey(sysIterate.Id))
                        {
                            set<Id> oppIdsSet = quoteOppMap.get(sysIterate.Id);
                            for(Id oppId : oppIdsSet)
                            {
                                for(string strIterate : oppSyncingMap.keyset()) //Application_ID_c
                                {
                                    sObject sObjOpp = Schema.getGlobalDescribe().get('Opportunity').newSObject() ;
                                    
                                    if(oppUpdateMap.containskey(sysIterate.Opportunity__c))
                                    {
                                        sObjOpp = oppUpdateMap.get(sysIterate.Opportunity__c);
                                    }
                                    string oppFld = oppSyncingMap.get(strIterate);
                                    sObjOpp.put('Id' ,(String)oppId); 
                                    sObjOpp.put(oppFld ,(object)sysIterate.get(strIterate)); 
                                    oppUpdateMap.put(sysIterate.Opportunity__c,sObjOpp);
                                }
                            }
                        }
                    }
                    system.debug('### CheckPoint oppUpdateMap '+oppUpdateMap);
                    if(!oppUpdateMap.isEmpty())
                        update oppUpdateMap.Values();
                }
           // }
        }catch(Exception ex)
        {
            system.debug('### QuoteTriggerHandler.runPricing():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
           ErrorLogUtility.writeLog('QuoteTriggerHandler.runPricing()',ex.getLineNumber(),'runPricing()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    
    public void QuoteOwnerChangeBasedOnOpportunityOwner(Quote[] newQuotelst){
        Set<ID> Oppids;
        system.debug('### newQuotelst---'+newQuotelst);
        if(newQuotelst!= null && newQuotelst.size()>0){//Thisi is Quote insert
                Oppids = new set<ID>();
            for(Quote Qt : newQuotelst){
               if(Qt.OpportunityId != null)
                    Oppids.add(Qt.OpportunityId); 
            }
            
        }
        Map<string,Opportunity> idAndOppMap = new Map<string,Opportunity>();
        if(Oppids != null && Oppids.size()>0){
          for(Opportunity opps: [Select id,Name,Ownerid from Opportunity where id IN: Oppids]){
              idAndOppMap.put(opps.id,opps);
          }
        }
        system.debug('### idAndOppMap-'+idAndOppMap);
        for(integer i=0;i<newQuotelst.size();i++){
            if(idAndOppMap.get(newQuotelst[i].OpportunityId) != null){
                system.debug('### Opp owner val-'+idAndOppMap.get(newQuotelst[i].OpportunityId).ownerid);
                newQuotelst[i].ownerid = idAndOppMap.get(newQuotelst[i].OpportunityId).ownerid;
            }
        }
       
    }
    // Added as part of ORANGE-2040 
    public void onBeforeUpdate(List<Quote> newQuoteList){
         // Added Query by Sreekar As Part of 16329 
        Map<Id,Quote> mapQuote = new Map<Id,Quote>([select Id,includesBattery__c ,Product_Loan_Type__c,Combined_Loan_Amount__c,System_Design__r.System__c,OpportunityId from Quote where Id in:newQuoteList ]);
        system.debug('### Entered into onBeforeUpdate() of '+ QuoteTriggerHandler.class);
         set<Id> QuoteIdSet = new set<Id>();
           if(newQuoteList != null && newQuoteList.size() > 0 && mapQuote != null && mapQuote.size()> 0){
            
            updateQuoteMasterFields(newQuoteList,mapQuote);
            system.debug('### End onBeforeUpdate() of '+ QuoteTriggerHandler.class);
          }  
        }
    
    public void updateQuoteMasterFields(List<Quote> newQuoteList,Map<Id,Quote> mapQuote){
    // Added by Sreekar As Part of 16329 - Start
        for(Quote qt: newQuoteList){
            if(mapQuote != null && mapQuote.get(qt.Id) != null){
                if( mapQuote.get(qt.Id).Product_Loan_Type__c != null && (mapQuote.get(qt.Id).Product_Loan_Type__c == 'Solar' || mapQuote.get(qt.Id).Product_Loan_Type__c == 'Solar Interest Only') && mapQuote.get(qt.Id).Combined_Loan_Amount__c  != null && mapQuote.get(qt.Id).System_Design__c != null  && mapQuote.get(qt.Id).System_Design__r.System__c != null && ( ( mapQuote.get(qt.Id).Combined_Loan_Amount__c / (mapQuote.get(qt.Id).System_Design__r.System__c *1000)) > 6  && ( mapQuote.get(qt.Id).Combined_Loan_Amount__c / (mapQuote.get(qt.Id).System_Design__r.System__c *1000)) <= 10 )){               
                qt.includesBattery__c = true;
                }else if( mapQuote.get(qt.Id).Product_Loan_Type__c != null && (mapQuote.get(qt.Id).Product_Loan_Type__c == 'Integrated Solar Shingles' || mapQuote.get(qt.Id).Product_Loan_Type__c == 'Solar Plus Roof') && mapQuote.get(qt.Id).Combined_Loan_Amount__c != null && mapQuote.get(qt.Id).System_Design__c != null  && mapQuote.get(qt.Id).System_Design__r.System__c != null && ( ( mapQuote.get(qt.Id).Combined_Loan_Amount__c/ (mapQuote.get(qt.Id).System_Design__r.System__c)) > 10 )){
                qt.includesBattery__c = True;
              }   
            }
             // Added by Sreekar As Part of 16329 - End
            if(qt.isACH__c){                
                if(qt.ACH_Monthly_Payment__c != null)
                    qt.Monthly_Payment__c = qt.ACH_Monthly_Payment__c;
                if(qt.ACH_Monthly_Payment_without_Prepay__c != 0 && qt.ACH_Monthly_Payment_without_Prepay__c != null)
                    qt.Monthly_Payment_without_Prepay__c = qt.ACH_Monthly_Payment_without_Prepay__c;
                if(qt.ACH_FinalMonthlyPaymentWOPrepay__c != 0 && qt.ACH_FinalMonthlyPaymentWOPrepay__c != null)
                   qt.FinalMonthlyPaymentwithoutPrepay__c = qt.ACH_FinalMonthlyPaymentWOPrepay__c;  
                if(qt.ACH_Final_Monthly_Payment__c != null)
                     qt.Final_Monthly_Payment__c  = qt.ACH_Final_Monthly_Payment__c;
                     
                /*if(qt.ACH_Monthly_Payment_without_Prepay__c != 0 && qt.ACH_Monthly_Payment_without_Prepay__c != null)
                    qt.Monthly_Payment_Escalated_Single_Loan__c   = qt.ACH_Monthly_Payment_without_Prepay__c;
                if(qt.ACH_Final_Monthly_Payment__c != 0 && qt.ACH_Final_Monthly_Payment__c != null)
                    qt.Final_Payment_Single_Loan__c   = qt.ACH_Final_Monthly_Payment__c; */
                
                if(qt.Product_Loan_Type__c == 'Solar Interest Only')
                {
                    if(qt.ACH_Monthly_Payment_Interest_Only__c != null)
                        qt.Monthly_Payment_Interest_Only__c  = qt.ACH_Monthly_Payment_Interest_Only__c;
                    if(qt.ACH_Escalated_Monthly_Payment_IO__c != null)
                        qt.Escalated_Monthly_Payment_IO__c  = qt.ACH_Escalated_Monthly_Payment_IO__c;
                }else{
                    qt.Monthly_Payment_Interest_Only__c = null;
                    qt.Escalated_Monthly_Payment_IO__c  = null;
                }
            }else{
                if(qt.NonACH_Monthly_Payment__c != null)
                    qt.Monthly_Payment__c = qt.NonACH_Monthly_Payment__c;
                if(qt.NonACH_Monthly_Payment_WO_Prepay__c != 0 && qt.NonACH_Monthly_Payment_WO_Prepay__c != null)
                    qt.Monthly_Payment_without_Prepay__c= qt.NonACH_Monthly_Payment_WO_Prepay__c;
                if(qt.NonACH_FinalMonthlyPaymentWOPrepay__c != 0 && qt.NonACH_FinalMonthlyPaymentWOPrepay__c!= null)                   
                    qt.FinalMonthlyPaymentwithoutPrepay__c  = qt.NonACH_FinalMonthlyPaymentWOPrepay__c; //Updated by Ravi to fix ORANGE-16502
                 
                    //qt.FinalMonthlyPaymentwithoutPrepay__c  = qt.NonACH_Monthly_Payment_WO_Prepay__c ;
                     
                if(qt.NonACH_Final_Monthly_Payment__c != null )
                    qt.Final_Monthly_Payment__c  = qt.NonACH_Final_Monthly_Payment__c;
               /* if(qt.NonACH_Monthly_Payment_WO_Prepay__c != 0 && qt.NonACH_Monthly_Payment_WO_Prepay__c != null)
                    qt.Monthly_Payment_Escalated_Single_Loan__c = qt.NonACH_Monthly_Payment_WO_Prepay__c;
                if(qt.NonACH_Final_Monthly_Payment__c != 0 && qt.NonACH_Final_Monthly_Payment__c!= null)                   
                     qt.Final_Payment_Single_Loan__c   = qt.NonACH_Final_Monthly_Payment__c ;
               */
                
                
                if(qt.Product_Loan_Type__c == 'Solar Interest Only')
                {
                    if(qt.NonACH_Monthly_Payment_IO__c != null)
                        qt.Monthly_Payment_Interest_Only__c  = qt.NonACH_Monthly_Payment_IO__c;
                    if(qt.NonACH_Escalated_Monthly_Payment_IO__c != null)
                        qt.Escalated_Monthly_Payment_IO__c  = qt.NonACH_Escalated_Monthly_Payment_IO__c;
                }else{
                    qt.Monthly_Payment_Interest_Only__c = null;
                    qt.Escalated_Monthly_Payment_IO__c  = null;
                }
            } // End of ORANGE-2040.
       }
   }
}