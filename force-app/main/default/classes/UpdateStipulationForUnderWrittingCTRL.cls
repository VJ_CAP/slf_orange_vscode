/* Author : Veereandranath Jalla
* Date   : 12/19/2018
* Description: This class is used to Update Stipulation and invoked from Skuid page
*
*/
/*-------------------------------------------------------------------------------
*  Initial Version Written By           Date Modified By      Description
*  Rajesh                               12/19/2018            Initial version
*/ 
global class UpdateStipulationForUnderWrittingCTRL {
    global class Input {
        @InvocableVariable(required=true) global Id strUnderWrittingId;
        global Input() {
            //  nothing to do
        }
        global Input(Id strUnderWrittingId) {
            this.strUnderWrittingId = (String.valueOf(strUnderWrittingId)).substring(0,15);
        }
    }
    global class Output {   
        @InvocableVariable global String Message;
    }
    public class TaskException extends Exception{}
    
        
    @InvocableMethod(label='UpdateStipulationOwner')
    public static List<Output> UpdateTaskStatus(List<Input> lstInput){
        List<Output> lstOutPuts = new List<Output>();  
        Output objOutput = new Output();    
        Set<Id> UnderWrittingIds = new Set<Id>();
        List<Stipulation__c> updatedStipList = new List<Stipulation__c>();
        
        
        for(input objI:lstInput){
            UnderWrittingIds.add(objI.strUnderWrittingId);
        }
        
        List<Stipulation__c> stipList = [Select id,Name,Underwriting__c,Review_Start_Date_Time__c,OwnerId from Stipulation__c where Underwriting__c IN :UnderWrittingIds and Status__c = 'Documents Received' and Review_Start_Date_Time__c = null and (Underwriting__r.M1_Approval_Requested_Date__c = null or (Underwriting__r.Opportunity__r.Project_Category__c = 'Home' and Underwriting__r.Approved_for_Payments__c = false))];
        
        if(!stipList.isEmpty()){
                
            for(Stipulation__c stip :stipList){
                stip.Review_Start_Date_Time__c = system.now();
                stip.OwnerId = userinfo.getUserId();
                updatedStipList.add(stip);
            }
        }
        else        
        {
            Underwriting_File__c objUWFile = [select Id,M1_Ready_for_Review_Date_Time__c, M1_Ready_for_Review_Age__c, Account_Name__c,M1_Approval_Requested_Date__c                                
                                          from Underwriting_File__c where Id in:UnderWrittingIds limit 1];
            throw new TaskException(objUWFile.Account_Name__c+' is Under review. Please select the next record in the queue.');
        }
        try{
            if(!updatedStipList.isEmpty())
                update updatedStipList;
            
            lstOutPuts.add(objOutput);
            return lstOutPuts;
        }Catch(Exception ex){
            objOutput.Message = ex.getMessage();
            lstOutPuts.add(objOutput);
            return lstOutPuts;
        }
        
        //return null;        
    }
}