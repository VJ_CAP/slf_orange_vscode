/**
* Description: 
*
*    Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Sreekar               28/08/2019              Created
******************************************************************************************/
global class EmailAlertsToCustomerAndInstaller{
    
    @InvocableMethod(label='emailAlertsToCustomerAndInstaller')
    global static void triggerEmail(List<Opportunity> lstOpp){
        system.debug('Inside Execute method--'+lstOpp);
        List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage>();
        List<string> toAddressCustomer = new List<string>();
        List<string> toAddressInstaller = new List<string>();
        List<String> ccAddressOps = new List<string>();
        Map<String,EmailTemplate> mapEmailTemplate = new Map<String,EmailTemplate>();
        list<EmailTemplate> lstEmailTemplate = [SELECT Id,DeveloperName,HtmlValue,Subject,body,BrandTemplateId FROM EmailTemplate WHERE DeveloperName in ( 'Customer_Email','Email_to_Partners_and_Internalteams')];
        for(EmailTemplate objE:lstEmailTemplate){
            mapEmailTemplate.put(objE.DeveloperName,objE);
        }
        String emailBodyCustomer = mapEmailTemplate.get('Customer_Email').HtmlValue;
        String emailBodyInstaller = mapEmailTemplate.get('Email_to_Partners_and_Internalteams').HtmlValue;
        try{
            if(!lstOpp.isEmpty()){
                    List<Opportunity> lstOppNew = [Select Id, 
                    Name, Project_Category__c,Installer_Account_Name__c,Applicant_Last_Name__c,Applicant_First_Name__c,
                                                Credit_Expiration_Date__c,Primary_Applicant_Email__c,Installer_Account__r.Installer_Email__c,
                                                Installer_Account__r.Stip_Email_1__c,Installer_Account__r.Stip_Email_3__c,
                                                Installer_Account__r.Operations_Email__c,Installer_Account__r.Stip_Email_2__c,
                                                Installer_Account__r.Relationship_Manager__c, Installer_Account__r.Relationship_Manager__r.email,
                                                owner.email, Change_Order_Status__c,StageName,
                                                (select id,name, Withdraw_Reason__c,Withdraw_Reason_Detail__c from underwriting_files__r),
                                                Sales_Representative_Email__c
                                                FROM Opportunity where id =: lstOpp[0].Id];
              
                OrgWideEmailAddress[] supportEmailID = [select Id from OrgWideEmailAddress where Address = 'support@sunlightfinancial.com'];
                Messaging.SingleEmailMessage mailToCustomer = new Messaging.SingleEmailMessage();
                Messaging.SingleEmailMessage mailToInstaller = new Messaging.SingleEmailMessage();
                system.debug('***OppList***'+lstOppNew);
                for(Opportunity Opp: lstOppNew){
                    system.debug('***Opp Stage Name***'+opp.StageName);
                    system.debug('*** Opp Change Order Status***'+opp.Change_Order_Status__c);

                    if(opp.StageName == 'Closed Won' || (opp.StageName != null && (opp.Change_Order_Status__c == 'Active' || opp.Change_Order_Status__c == 'Pending'))) //modified as a part of 12667
                    { 
                        if(opp.Primary_Applicant_Email__c != null){
                           toAddressCustomer.add(opp.Primary_Applicant_Email__c);
                           
                        } 
                        System.debug('###toAddressCustomer'+toAddressCustomer);

                        if(opp.Installer_Account__r.Installer_Email__c != null){
                            toAddressInstaller.add(opp.Installer_Account__r.Installer_Email__c);
                        }
                        System.debug('###toAddressInstaller'+toAddressCustomer);

                        if(opp.Installer_Account__r.Stip_Email_1__c != null)
                            ccAddressOps.add(opp.Installer_Account__r.Stip_Email_1__c);
                        if(opp.Installer_Account__r.Stip_Email_2__c != null)
                            ccAddressOps.add(opp.Installer_Account__r.Stip_Email_2__c);
                        if(opp.Installer_Account__r.Stip_Email_3__c != null)
                            ccAddressOps.add(opp.Installer_Account__r.Stip_Email_3__c);
                        if(opp.Installer_Account__r.Operations_Email__c != null)
                            ccAddressOps.add(opp.Installer_Account__r.Operations_Email__c);
                        if(opp.Installer_Account__r.Relationship_Manager__c != null)
                            ccAddressOps.add(opp.Installer_Account__r.Relationship_Manager__r.email);
                        if(opp.owner.email != null)
                           ccAddressOps.add(opp.owner.email);                        
                        if(opp.Sales_Representative_Email__c != null) //Added as part of 8616 by Sejal
                            ccAddressOps.add(opp.Sales_Representative_Email__c);
                        
                        System.debug('###ccAddressOps'+ccAddressOps);
                        System.debug('###toAddressCustomer'+toAddressCustomer);
                        System.debug('###toAddressInstaller'+toAddressInstaller);
                            
                        if(!toAddressCustomer.isEmpty()){
                            mailToCustomer.setToAddresses(toAddressCustomer);                        
                            mailToCustomer.setHtmlBody(mapEmailTemplate.get('Customer_Email').HtmlValue); 
                            mailToCustomer.setSubject(mapEmailTemplate.get('Customer_Email').Subject);
                            emailBodyCustomer = emailBodyCustomer.replace('{!URLFOR($Label.Sunlight_Logo)}',label.Sunlight_Logo);emailBodyCustomer = emailBodyCustomer.replace('{!Opportunity.Installer_Account_Name__c}', opp.Installer_Account_Name__c);
                            emailBodyCustomer = emailBodyCustomer.replace(']]>','');  
                            //Commented By Adithya as this below code wont work                         
                            //emailBodyCustomer = emailBodyCustomer .replace('{!$Label.Sunlight_Logo}', label.Sunlight_Logo); //added as apart of Orange-12667
                            mailToCustomer.setHtmlBody(emailBodyCustomer); 
                            // Setting ORG WIDE Email adderess 
                            mailToCustomer.setOrgWideEmailAddressId(supportEmailID.get(0).Id);
                            lstEmails.add(mailToCustomer);
                            system.debug('###lstEmails'+lstEmails);
                        }
                            
                       if(toAddressInstaller.size()>0 && ccAddressOps.size()>0 ){
                            mailToInstaller.setToAddresses(toAddressInstaller);
                            mailToInstaller.setccAddresses(ccAddressOps);
                            mailToInstaller.setHtmlBody((mapEmailTemplate.get('Email_to_Partners_and_Internalteams').HtmlValue));
                            mailToInstaller.setSubject(mapEmailTemplate.get('Email_to_Partners_and_Internalteams').Subject);
                            String sub =mapEmailTemplate.get('Email_to_Partners_and_Internalteams').Subject;
                            sub = sub.replace('{!Opportunity.Applicant_First_Name__c}', opp.Applicant_First_Name__c);
                            sub = sub.replace('{!Opportunity.Applicant_Last_Name__c}', opp.Applicant_Last_Name__c);
                              
                            mailToInstaller.setSubject(sub);
                            emailBodyInstaller = emailBodyInstaller.replace('{!URLFOR($Label.Sunlight_Logo)}',label.Sunlight_Logo); 
                            emailBodyInstaller = emailBodyInstaller.replace('{!Opportunity.Applicant_First_Name__c}', opp.Applicant_First_Name__c);
                            emailBodyInstaller = emailBodyInstaller.replace('{!Opportunity.Applicant_Last_Name__c}', opp.Applicant_Last_Name__c);
                            emailBodyInstaller = emailBodyInstaller.replace('{!Opportunity.Installer_Account_Name__c}', opp.Installer_Account_Name__c);

                            emailBodyInstaller = emailBodyInstaller.replace('{!Opportunity.Project_Category__c}', opp.Project_Category__c);
                            
                            //Added by Adithya to fix ORANGE-14541on 11/15/2019
                            if(null != Opp.underwriting_files__r[0].Withdraw_Reason_Detail__c)
                                emailBodyInstaller = emailBodyInstaller.replace('{!Underwriting_File__c.Withdraw_Reason_Detail__c}', Opp.underwriting_files__r[0].Withdraw_Reason_Detail__c);
                            else
                                emailBodyInstaller = emailBodyInstaller.replace('{!Underwriting_File__c.Withdraw_Reason_Detail__c}', Opp.underwriting_files__r[0].Withdraw_Reason__c);
                             // Added by Sreekar to Fix orange-14618   
                             if(null != Opp.Credit_Expiration_Date__c){
                            emailBodyInstaller = emailBodyInstaller.replace('{!Opportunity.Credit_Expiration_Date__c}', String.valueOf(opp.Credit_Expiration_Date__c));
                            }else{
                            emailBodyInstaller = emailBodyInstaller.replace('{!Opportunity.Credit_Expiration_Date__c}','');
                            }
                            emailBodyInstaller= emailBodyInstaller.replace(']]>','');
                            //Commented By Adithya as this below code wont work
                            //emailBodyInstaller = emailBodyInstaller .replace('{!$Label.Sunlight_Logo}', label.Sunlight_Logo); //added as apart of Orange-12667
                            mailToInstaller.setHtmlBody(emailBodyInstaller);
                            lstEmails.add(mailToInstaller);
                        }     
                        // Sending an email if lstEmail is not Empty.
                        if(!lstEmails.isEmpty()){
                            System.debug('######lstEmails'+lstEmails.size());                        
                               Messaging.sendEmail(lstEmails,false); 
                        }
                    }
                }
            } 
        }   
        catch(exception err){
            System.debug('Exception ==>:'+err.getMessage()+'### at Line==>:'+err.getLineNumber());
            ErrorLogUtility.writeLog(' CreateEquipmentBoxFolder.CreateNewFolder()',err.getLineNumber(),'CreateNewFolder()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
        }
    }
}