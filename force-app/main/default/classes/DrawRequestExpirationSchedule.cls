global class DrawRequestExpirationSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        DrawRequestExpirationBatch b = new DrawRequestExpirationBatch(); 
        database.executebatch(b);
    }
}