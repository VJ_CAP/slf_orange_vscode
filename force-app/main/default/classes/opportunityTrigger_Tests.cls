@isTest
public class opportunityTrigger_Tests {
    
    @TestSetup
    static void initData()
    {
        
       TriggerFlags__c chkFlg = new TriggerFlags__c(Name='Draw_Requests__c',isActive__c=True);
        insert chkFlg;
        
       //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        TCU_County_Zipcodes__c tuczip = new TCU_County_Zipcodes__c();
        tuczip.Name = '94611';
        tuczip.Zipcode__c = '94611';
        tuczip.County__c = 'California';
        insert tuczip;
       
        Portal_Users__c portalusr = new Portal_Users__c();
        portalusr.Name = UserInfo.getFirstName();
        portalusr.User_Name__c  = UserInfo.getName();
        insert portalusr;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        UserRole r = [select id,name,DeveloperName from UserRole where name = 'Service Partner User' limit 1];
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;  
        acc.Requires_ACH_APR_Validation__c = true;  
        insert acc;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id,Is_Default_Owner__c = true);
        insert con;  
        
        
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testcNew@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclasscc@mail.com',ContactId = con.Id);
        insert portalUser;
        
        
        
        test.starttest();
        list<Account> lstAcc = new list<Account>();
        //create new Account
        
        Account testAccount = new Account();
        testAccount.FirstName = 'TestFN';
        testAccount.LastName = 'TestLN';
        testAccount.SSN__c = '666-66-6666';
        testAccount.Marital_Status__c = 'Unmarried';
        testAccount.Phone = '4344435555';
        testAccount.PersonEmail = 'test@test.com';
        testAccount.ShippingPostalCode = '12345';
        testAccount.BillingPostalCode = '12345';
        testAccount.PersonBirthdate = date.valueOf('1968-09-21');
        testAccount.Installer_Legal_Name__c='Bright Solar Planet';
        lstAcc.add(testAccount);
        
        //create new installer account
        Account testInstallerAccount = new Account();
        testInstallerAccount.Name = 'testInstaller';
        testInstallerAccount.Push_Endpoint__c = 'www.slftest.com';
        testInstallerAccount.Installer_Legal_Name__c='Bright Solar Planet';
        testInstallerAccount.Solar_Enabled__c = true; 
        lstAcc.add(testInstallerAccount);
        //insert testInstallerAccount;
        
        if(lstAcc != null && lstAcc.size()>0)
            insert lstAcc;
        
        List<Draw_Allocation__c> drawList = new List<Draw_Allocation__c>();
        Draw_Allocation__c drawAll = new Draw_Allocation__c();
        drawAll.Account__c = testInstallerAccount.id;
        drawAll.Level__c = '1';
        drawAll.Draw_Allocation__c = 100;
        drawList.add(drawAll);
        insert drawAll;
        
        Contact testInstallerContact = new Contact();
        testInstallerContact.FirstName = 'fn';
        testInstallerContact.LastName = 'ln';
        testInstallerContact.Email = 'test@leancog.com';
        testInstallerContact.AccountId = testInstallerAccount.Id;
        //testInstallerContact.Is_Default_Owner__c = true;
        insert testInstallerContact;
        
        //create new product
        Product__c testProd = new Product__c();
        testProd.Sighten_Product_UUID__c = '1';
        testProd.State_Code__c = 'CA';
        testProd.Installer_Account__c = acc.Id;
        testProd.FNI_Min_Response_Code__c = 2;
        testProd.Qualification_Message__c = 'you are prequaled';
        testProd.Product_Display_Name__c = 'testprod';
        testProd.Is_Active__c = true;
        testProd.Product_Tier__c = '0';
        testProd.Product_Loan_Type__c  = 'Solar';
        insert testProd;
        
        //List<Opportunity> lstopp = new List<Opportunity>();
        //create new opportunity
        Opportunity testOppty = new Opportunity();
        testOppty.AccountId = testAccount.Id;
        testOppty.Co_Applicant__c = testAccount.Id;
        testOppty.Name = 'testOppty';
        testOppty.StageName = 'Archive';
        testOppty.CloseDate = Date.today();
        testOppty.Install_City__c = 'Oakland';
        testOppty.Install_State_Code__c = 'AL';
        testOppty.Install_Postal_Code__c = '94611';
        testOppty.Install_Street__c = '1900 Magellan Dr';
        testOppty.Installer_Account__c = acc.Id;
        testOppty.Sighten_Product_UUID__c = '1';
        testOppty.Sales_Representative_Email__c = 'test11@leancog.com';
        testOppty.Create_in_Sighten__c = true;
        testOppty.Combined_Loan_Amount__c = 240;
        testOppty.Email_Application_Request__c = true;
        testOppty.Application_Status__c = 'New';
        testOppty.Routing_Number_Validated__c = false;
        testOppty.isACH__c = true;
        testOppty.Dealer_Terms_Accepted__c = true;
        testOppty.Account_Numbers_Match__c = false;
        testOppty.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Customer Opportunity Record Type').getRecordTypeId();
        insert testOppty;
        
        testOppty.Routing_Number_Validated__c = true;
        testOppty.Account_Numbers_Match__c = true;
        update testOppty;
        //lstopp.add(testOppty);
        
        //create new opportunity
        Opportunity testOpptyOne = new Opportunity();
        testOpptyOne.AccountId = testAccount.Id;
        testOpptyOne.Co_Applicant__c = testAccount.Id;
        testOpptyOne.Name = 'testOpptyOne';
        testOpptyOne.StageName = 'Archive';
        testOpptyOne.CloseDate = Date.today();
        testOpptyOne.Install_City__c = 'Oakland';
        testOpptyOne.Install_State_Code__c = 'CA';
        testOpptyOne.Install_Postal_Code__c = '94611';
        testOpptyOne.Install_Street__c = '1900 Magellan Dr';
        testOpptyOne.Installer_Account__c = acc.Id;
        testOpptyOne.Sighten_Product_UUID__c = '1';
        testOpptyOne.Sales_Representative_Email__c = 'test@leancog.com';
        testOpptyOne.Create_in_Sighten__c = true;
        testOpptyOne.Combined_Loan_Amount__c = 0;
        testOpptyOne.Application_Status__c = 'Pending Review';
        testOpptyOne.Routing_Number_Validated__c = false;
        testOpptyOne.isACH__c = true;
        testOpptyOne.Dealer_Terms_Accepted__c = true;
        testOpptyOne.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Customer Opportunity Record Type').getRecordTypeId();
        //lstopp.add(testOpptyOne);
        insert testOpptyOne;
        testOpptyOne.Routing_Number_Validated__c = true;
        update testOpptyOne;
        /*if(lstopp != null && lstopp.size()>0)
            insert lstopp;
            Opportunity opp = [select id,Routing_Number_Validated__c from opportunity where Id IN:lstopp AND Sales_Representative_Email__c = 'test11@leancog.com'];
            opp.Routing_Number_Validated__c = true;
            update opp;*/
            
         //Insert Credit Record
            List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = testOppty.id;
            credit.Primary_Applicant__c = acc.id;
            //credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = testProd.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'New';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            //insert credit;
            creditList.add(credit);
            
            insert creditList;
            
        try{
            fniCalloutManual.buildHardCreditRequest(testOppty.Id);
        }catch(Exception e){
           
            
        }                    
        test.stoptest();
        testAccount.BillingStreet = '123 elm';
        testAccount.BillingCity = 'Oakland';
        testAccount.BillingState = 'California';
        testAccount.ShippingStreet = '123 elm';
        testAccount.ShippingCity = 'Oakland';
        testAccount.ShippingState = 'California';
        testAccount.Length_of_Employment_Months__c = 2;
        testAccount.Length_of_Employment_Years__c = 3;
        update testAccount;
        
        List<Underwriting_File__c> undFileList = new List<Underwriting_File__c>();
        Underwriting_File__c underWrFilRec = new Underwriting_File__c();
        underWrFilRec.name = 'Underwriting FileOne';
        underWrFilRec.ACT_Review__c ='Yes';
        underWrFilRec.Opportunity__c = testOppty.Id;
        underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
        underWrFilRec.Title_Review_Complete_Date__c = system.today();
        underWrFilRec.Install_Contract_Review_Complete__c = true;
        underWrFilRec.Utility_Bill_Review_Complete__c = true;
        underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
        underWrFilRec.Installation_Photos_Review_Complete__c = true;
        //underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
        //underWrFilRec.Final_Design_Document_Received__c = system.today();
        underWrFilRec.Final_Design_Review_Complete__c = true;   
        //underWrFilRec.PTO_Received__c = system.today();
        underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
        undFileList.add(underWrFilRec);
        insert undFileList;
        
        Draw_Requests__c drawObj = new Draw_Requests__c(Amount__c= 10000,Underwriting__c=undFileList[0].id,Status__c = 'Requested',Approval_Date__c=System.now(),Primary_Applicant_Email__c = 'test@gmail.com');
        insert drawObj;
        
       system.debug('@@@@drawObj'+drawObj); 
        
       
        
    }
    public testMethod static void testOpportunityTriggerforProduct(){
        /*Portal_Users__c portalusr = new Portal_Users__c();
        portalusr.Name = UserInfo.getFirstName();
        portalusr.User_Name__c  = UserInfo.getUserName();
        insert portalusr;*/
        
        Account acc = [SELECT Id FROM Account WHERE FirstName = 'TestFN' LIMIT 1]; 
        List<Opportunity> opp = [select Id,Name,Install_State_Code__c,Customer_has_authorized_credit_hard_pull__c,Application_Status__c,RecordTypeId from opportunity where AccountId =: acc.Id]; 
        
        
        opp[0].Install_State_Code__c = 'CA';
        opp[0].Install_Postal_Code__c = '75050';
        opp[0].Customer_has_authorized_credit_hard_pull__c = true;
        opp[0].Application_Status__c = 'Auto Approved';
        
        opp[1].Install_State_Code__c = 'CA';
        opp[1].Customer_has_authorized_credit_hard_pull__c = true;
        opp[1].Application_Status__c = 'Expired';
        update opp;
        System.debug('opp1'+opp[0].Application_Status__c);
        set<Id> oppsetId = new set<Id>();
        oppsetId.add(opp[1].Id);
        
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetQuotes';
        req.addParameter('opportunity', opp[0].Id);        
        req.httpMethod = 'POST';//HTTP Request Type
        req.requestBody = Blob.valueof('');
        RestContext.request = req;
        RestContext.response= res;
       
        Product__c testProdRest = new Product__c();
        testProdRest.Sighten_Product_UUID__c = '1';
        testProdRest.State_Code__c = 'CA';
        testProdRest.Installer_Account__c = acc.Id;
        testProdRest.FNI_Min_Response_Code__c = 2;
        testProdRest.Qualification_Message__c = 'you are prequaled';
        testProdRest.Product_Display_Name__c = 'testprod';
        testProdRest.Is_Active__c = true;
        testProdRest.Product_Tier__c = '0';
        testProdRest.Product_Loan_Type__c  = 'Solar';
        insert testProdRest;
        
        String  stateCode = 'CA';
        System.debug('***Debug: RESTController.getQuotes(): ' + RESTController.getQuotes());
        String response = RESTController.getQuotes();
        
        req.requestURI = '/services/apexrest/GetCredit';
        try{
            RESTControllerCredit.getCredit();}
        catch(Exception e){
            
        }
        
        
        RestControllerGetProduct.getProducts();
        req.addParameter('stateCode', 'CA');
        RestControllerGetProduct.getProducts();
        test.starttest();
        // To cover Exception Block
        OpportunityTriggerHandler oppTrgHandler = new OpportunityTriggerHandler(true);
        
        oppTrgHandler.OnBeforeInsert(null);
        oppTrgHandler.OnBeforeUpdate(null,null);
        oppTrgHandler.OnAfterInsert(null);
        oppTrgHandler.OnAfterUpdate(null,null);
        oppTrgHandler.createCredit(null,null);
        oppTrgHandler.createUnderwriting(null);
        oppTrgHandler.updateUnderwriting(null,null);
        oppTrgHandler.UpdateOpportunityOwner(null,null);
        oppTrgHandler.createUnderwritingArchiveRecord(oppsetId);
        
        test.stoptest();
       
         //get the person account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        
        Account acc1 = new Account();
        acc1.name = 'Test Account';
        acc1.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc1.BillingStreet='Teststreet';
        acc1.BillingCity='testcity';
        acc1.BillingState='Alaska';
        acc1.BillingPostalCode='12445';
        acc1.ShippingStreet='Teststreet';
        acc1.ShippingCity='testcity';
        acc1.ShippingState='Alaska';
        acc1.ShippingPostalCode='12345';
        acc1.Installer_Legal_Name__c='Bright Solar Planet';
        acc1.Solar_Enabled__c = true;   
        insert acc1;

        Contact con = new Contact(LastName = 'Contact Last Name', AccountId = acc1.id,Is_Default_Owner__c = true);
        insert con;

        User portalUser = new User();
        portalUser.ProfileID = [Select Id From Profile Where Name='Partner Community User for Salesforce1'].id;
        portalUser.EmailEncodingKey = 'ISO-8859-1';
        portalUser.LanguageLocaleKey = 'en_US';
        portalUser.TimeZoneSidKey = 'America/New_York';
        portalUser.LocaleSidKey = 'en_US';
        portalUser.FirstName = 'first';
        portalUser.LastName = 'last';
        portalUser.Username = 'testSLF@appirio.com';   
        portalUser.CommunityNickname = 'testUser123';
        portalUser.Alias = 't1';
        portalUser.Email = 'no@email.com';
        portalUser.IsActive = true;
        portalUser.ContactId = con.Id;      
        insert portalUser;
        
        AccountShare accShare = new AccountShare(); //a new empty AccountShare object
        accShare.userorgroupid = portalUser.Id;
        accShare.accountid = con.accountid;
        accShare.accountaccesslevel = 'Edit';
        accShare.OpportunityAccessLevel = 'None';
        accShare.CaseAccessLevel = 'None';
        insert accShare;
        

    }
    
    private testMethod static void testOpprUnderWritter(){
        
        
       //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        TCU_County_Zipcodes__c tuczip = new TCU_County_Zipcodes__c();
        tuczip.Name = '94613';
        tuczip.Zipcode__c = '94613';
        tuczip.County__c = 'California';
        insert tuczip;
       
        /*Portal_Users__c portalusr = new Portal_Users__c();
        portalusr.Name = UserInfo.getFirstName();
        portalusr.User_Name__c  = UserInfo.getName();
        insert portalusr;*/
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        UserRole r = [select id,name,DeveloperName from UserRole where Name = 'Service Partner User' limit 1];
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;   
        acc.Requires_ACH_APR_Validation__c = true;
        insert acc;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id,Is_Default_Owner__c = true);
        insert con;  
        
        
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testcT', Email='testcNewTest@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='testCT', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclasscc12@mail.com',ContactId = con.Id);
        insert portalUser;
        
        
        
        test.starttest();
        list<Account> lstAcc = new list<Account>();
        //create new Account
        
        Account testAccount = new Account();
        testAccount.FirstName = 'TestFN';
        testAccount.LastName = 'TestLN';
        testAccount.SSN__c = '666-66-6666';
        testAccount.Marital_Status__c = 'Unmarried';
        testAccount.Phone = '4344435555';
        testAccount.PersonEmail = 'test@test.com';
        testAccount.ShippingPostalCode = '12345';
        testAccount.BillingPostalCode = '12345';
        testAccount.PersonBirthdate = date.valueOf('1968-09-21');
        testAccount.Installer_Legal_Name__c='Bright Solar Planet';           
        lstAcc.add(testAccount);
        //insert testAccount;    
        
        
        System.debug('***Debug: ispersonaccount?: ' + testAccount.IsPersonAccount);
         
        //create new installer account
        Account testInstallerAccount = new Account();
        testInstallerAccount.Name = 'testInstaller';
        testInstallerAccount.Push_Endpoint__c = 'www.slftest.com';
        testInstallerAccount.Installer_Legal_Name__c='Bright Solar Planet';
        testInstallerAccount.Solar_Enabled__c = true; 
        lstAcc.add(testInstallerAccount);
       
        if(lstAcc != null && lstAcc.size()>0)
            insert lstAcc;
        
        List<Draw_Allocation__c> drawList = new List<Draw_Allocation__c>();
        Draw_Allocation__c drawAll = new Draw_Allocation__c();
        drawAll.Account__c = testInstallerAccount.id;
        drawAll.Level__c = '1';
        drawAll.Draw_Allocation__c = 100;
        drawList.add(drawAll);
        insert drawAll;
        
        Contact testInstallerContact = new Contact();
        testInstallerContact.FirstName = 'fn';
        testInstallerContact.LastName = 'ln';
        testInstallerContact.Email = 'test@leancog.com';
        testInstallerContact.AccountId = testInstallerAccount.Id;
        insert testInstallerContact;
        
        //create new product
        Product__c testProd = new Product__c();
        testProd.Sighten_Product_UUID__c = '1';
        testProd.State_Code__c = 'CA';
        testProd.Installer_Account__c = acc.Id;
        testProd.FNI_Min_Response_Code__c = 2;
        testProd.Qualification_Message__c = 'you are prequaled';
        testProd.Product_Display_Name__c = 'testprod';
        testProd.Product_Tier__c = '0';
        testProd.Is_Active__c = true;
        testProd.Product_Loan_Type__c  = 'Solar';
        insert testProd;
        
        List<Opportunity> lstopp = new List<Opportunity>();
        //create new opportunity
        Opportunity testOppty = new Opportunity();
        testOppty.AccountId = testAccount.Id;
        testOppty.Co_Applicant__c = testAccount.Id;
        testOppty.Name = 'testOppty';
        testOppty.StageName = 'Qualified';
        testOppty.CloseDate = Date.today();
        testOppty.Install_City__c = 'Oakland';
        testOppty.Install_State_Code__c = 'AL';
        testOppty.Install_Postal_Code__c = '94611';
        testOppty.Install_Street__c = '1900 Magellan Dr';
        testOppty.Installer_Account__c = acc.Id;
        testOppty.Sighten_Product_UUID__c = '1';
        testOppty.Sales_Representative_Email__c = 'test123@leancog.com';
        testOppty.Create_in_Sighten__c = true;
        testOppty.Combined_Loan_Amount__c = 240;
        testOppty.Email_Application_Request__c = true;
        testOppty.isACH__c = true;
        testOppty.Account_Numbers_Match__c = false;
        //testOppty.Dealer_Terms_Accepted__c = true;
        testOppty.Routing_Number_Validated__c = false;
        testOppty.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Customer Opportunity Record Type').getRecordTypeId();
        //lstopp.add(testOppty);
        insert testOppty;
        testOppty.Routing_Number_Validated__c = true;
        testOppty.Account_Numbers_Match__c = true;
        update testOppty;
        
        
        //create new opportunity
        Opportunity testOpptyOne = new Opportunity();
        testOpptyOne.AccountId = testAccount.Id;
        testOpptyOne.Co_Applicant__c = testAccount.Id;
        testOpptyOne.Name = 'testOpptyOne';
        testOpptyOne.StageName = 'Qualified';
        testOpptyOne.CloseDate = Date.today();
        testOpptyOne.Install_City__c = 'Oakland';
        testOpptyOne.Install_State_Code__c = 'CA';
        testOpptyOne.Install_Postal_Code__c = '94611';
        testOpptyOne.Install_Street__c = '1900 Magellan Dr';
        testOpptyOne.Installer_Account__c = acc.Id;
        testOpptyOne.Sighten_Product_UUID__c = '1';
        testOpptyOne.Sales_Representative_Email__c = 'test@leancog.com';
        testOpptyOne.Create_in_Sighten__c = true;
        testOpptyOne.Combined_Loan_Amount__c = 240;
        testOpptyOne.Application_Status__c = 'Auto Approved';
        testOpptyOne.Routing_Number_Validated__c = false;
        testOpptyOne.isACH__c = true;
        testOpptyOne.Account_Numbers_Match__c = false;
        //testOpptyOne.Dealer_Terms_Accepted__c = true;
        testOpptyOne.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Customer Opportunity Record Type').getRecordTypeId();
        //lstopp.add(testOpptyOne);
        insert testOpptyOne;
        //testOpptyOne.Routing_Number_Validated__c = true;
        
        testOpptyOne.Account_Numbers_Match__c = true;
        update testOpptyOne;
        
        Opportunity opp = [Select id,Routing_Number_Validated__c,Application_Status__c,RecordTypeId from Opportunity where Sales_Representative_Email__c = 'test123@leancog.com'];
        opp.Application_Status__c = 'Auto Approved';
        system.debug('@@opportunity' +opp);
        
        map<String, Id> roleMap1 = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap1.put(role.DeveloperName, role.Id);
        }
        
        Map<id,User> userMap = new Map<id,User>();
        userMap = new Map<id,User>([Select id,Name,Username,UserRole.Name From User Where id = '005W00000043E45IAE' AND UserRole.DeveloperName = 'ServicePartnerUser']);
        system.debug('###usermap' +userMap);
        
        /*if(lstopp != null && lstopp.size()>0)
            insert lstopp;
            Opportunity opp = [Select id,Routing_Number_Validated__c from Opportunity where id IN:lstopp AND Sales_Representative_Email__c = 'test123@leancog.com'];
            Opp.Routing_Number_Validated__c = true;
            update opp;*/
        try{
            fniCalloutManual.buildHardCreditRequest(testOppty.Id);
        }catch(Exception e){
           
            
        }                    
        test.stoptest();
        testAccount.BillingStreet = '123 elm';
        testAccount.BillingCity = 'Oakland';
        testAccount.BillingState = 'California';
        testAccount.ShippingStreet = '123 elm';
        testAccount.ShippingCity = 'Oakland';
        testAccount.ShippingState = 'California';
        testAccount.Length_of_Employment_Months__c = 2;
        testAccount.Length_of_Employment_Years__c = 3;
        update testAccount;
        
        
        Underwriting_File__c underWrFilRec = new Underwriting_File__c();
        underWrFilRec.name = 'Underwriting FileOne';
        underWrFilRec.ACT_Review__c ='Yes';
        underWrFilRec.Opportunity__c = testOppty.Id;
        underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
        underWrFilRec.Title_Review_Complete_Date__c = system.today();
        underWrFilRec.Install_Contract_Review_Complete__c = true;
        underWrFilRec.Utility_Bill_Review_Complete__c = true;
        underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
        underWrFilRec.Installation_Photos_Review_Complete__c = true;
        //underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
       // underWrFilRec.Final_Design_Document_Received__c = system.today();
        underWrFilRec.Final_Design_Review_Complete__c = true;   
       
        underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
        insert underWrFilRec;
        
    }
    
    
}