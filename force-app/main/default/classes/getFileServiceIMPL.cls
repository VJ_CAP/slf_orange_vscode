/**
* Description: Opportunity Id Search related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suma                  08/29/2019              Created
******************************************************************************************/
public with sharing class getFileServiceIMPL extends RESTServiceBase{ 
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){
        system.debug('### Entered into transformOutput() of '+FinOpsServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        system.debug('### reqData '+reqData);
        UnifiedBean responseBean = new UnifiedBean();
        responseBean.error = new list<UnifiedBean.errorWrapper>();
        UnifiedBean.FinOpsReportWrapper finOpsReportWrapper = new UnifiedBean.FinOpsReportWrapper();
        //String sBoxAccessToken = getBoxAccesstoken();
        FinOpsServiceImplV2 finOps = new FinOpsServiceImplV2();
        String sBoxAccessToken = finOps.getBoxAccesstoken();
        
        IRestResponse iRestRes;
        string errMsg = '';
        Savepoint sp = Database.setSavepoint();
        
        try
        {
            if(reqData==null || reqData.id == null || reqData.id == ''){
                finOps.getErrorMsg(responseBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                iRestRes = (IRestResponse)responseBean;
                return iRestRes;
            }            
            finOpsReportWrapper.boxAccessToken = sBoxAccessToken;                    
            responseBean.finOpsReport = finOpsReportWrapper;
            responseBean.returnCode = '200';
            iRestRes = (IRestResponse)responseBean;
            return iRestRes;
        }catch(Exception err){
            Database.rollback(sp);
            errMsg = ErrorLogUtility.getErrorCodes('400', null);
            finOps.getErrorMsg(responseBean,errMsg,'400');
            iRestRes = (IRestResponse)responseBean;
            system.debug('### FinOpsServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('FinOpsServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }

        iRestRes = (IRestResponse)responseBean;
        system.debug('### Exit from transformOutput() of '+FinOpsServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        
        return iRestRes;
    }   
   
 }