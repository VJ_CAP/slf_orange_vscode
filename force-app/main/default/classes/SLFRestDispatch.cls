/**
* Description: Apex REST Webservice to Create and Retrieve data from Salesforce.
*       1. doPost()     : All inbound POST calls comes in to this method.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

@RestResource(urlMapping='/api/*')
global with sharing class SLFRestDispatch{
    
    /**
     * Description:  All inbound POST calls comes in to this method.
     *
     * return res    Returns an instance of ResponseWrapper
     */
    @HttpPost
    global static IRestResponse doPost(){ 
        system.debug('### Entered into doPost() of '+ SLFRestDispatch.class);
        
        ResponseWrapper res = new ResponseWrapper(); //Response wrapper class with Status code, message and data 
        try{
           
            //Returns the RestRequest object for our Apex REST method.
            RestRequest request = RestContext.request;
           
            //Returns the RestResponse for our Apex REST method.
            RestResponse response = RestContext.response;
           
            //Access the request body with input data coming in the JSON format
            String jSONRequestBody = request.requestBody.toString().trim();
            system.debug('### jSONRequestBody ' + jSONRequestBody);
            
            //Assigning request body to Request wrapper for processing the respose
            string reqDataStr = jSONRequestBody;
           // IRestRequest reqData;
            
            String serviceName = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1, request.requestURI.length());
            system.debug('### serviceName ' + serviceName);
            RequestActionParam reqAP = new RequestActionParam();
            reqAP.serviceName = serviceName;
            reqAP.action = 'POST';
           
           /* ============================================== INTERFACE IMPLEMENTATION START =========================== */
            RequestWrapper rw = new RequestWrapper();
           // rw.reqData = reqData; 
            rw.reqAction = reqAP;
            rw.reqDataStr = reqDataStr;
            system.debug('### rw '+rw);
            res = RestFactory.invokeService(rw);
            response.addHeader('Content-Type', 'application/json');
            /* ============================================== INTERFACE IMPLEMENTATION END ============================= */
        }catch(Exception err){
             system.debug('### SLFRestDispatch.doPost():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
             ErrorLogUtility.writeLog('SLFRestDispatch.doPost()',err.getLineNumber(),'doPost()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));  
        }
        system.debug('### Exit from doPost() of '+ SLFRestDispatch.class);
        return res.response;
     }
 
}