@isTest
public class CreditExtensionRequestServiceImplTest{ 
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();        
    }
         /**
*
* Description: This method is used to cover CreditExtensionRequestServiceImpl 
*
*/
    
    private testMethod static void equipmentCreateTest(){
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        opp.Hash_Id__c = loggedInUsr[0].Hash_Id__c;
        update opp;
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1];      
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            String projectTypeStr;
            Test.startTest();
             projectTypeStr ='{"projects":[{"id":"'+opp.Id+'","credits":[{"id":"'+cred.Id+'"}]}]}';
             
            SLFRestDispatchTest.MapWebServiceURI(userMap,projectTypeStr,'creditextensionrequest');
            Test.stopTest();
        }
    } 
    private testMethod static void equipmentCreateTest1(){
        List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        opp.Hash_Id__c = loggedInUsr[0].Hash_Id__c;
        update opp;
         SLF_Credit__c cred = [SELECT Id,name, Opportunity__c,Primary_Applicant__c,Co_Applicant__c,FNI_Credit_Expiration__c,IsSyncing__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1];     
         cred.IsSyncing__c = true;
         cred.FNI_Credit_Expiration__c = system.today()+1;
         update cred;
         system.debug('******Credit Record********'+cred);
         Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
            String projectTypeStr;
            Test.startTest();
             projectTypeStr ='{"projects":[{"id":"'+opp.Id+'","credits":[{"id":"'+cred.Id+'"}]}]}';
            
            SLFRestDispatchTest.MapWebServiceURI(userMap,projectTypeStr,'creditextensionrequest');
            Test.stopTest();
        }
    }  
    
                
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        system.debug('*******pricingMapstart****'+pricingMap);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        system.debug('*******pricingMap****'+pricingMap);
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
            system.debug('*****req.requestBody*****'+jsonString);
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
            system.debug('*****req.requestBody*****'+gen.getAsString());
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}