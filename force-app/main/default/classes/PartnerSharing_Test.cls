@isTest
private class PartnerSharing_Test 
{
     
    @isTest static void test1 () 
    {
        SLFUtilityTest.createCustomSettings();
        Partner_Roles__c roles = new Partner_Roles__c();
        roles.Name ='ServicePartnerUser';
        roles.Role_Name__c ='ServicePartnerUser';
        insert roles;
        Account a1 = new Account (Name = 'Test installer 1', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        Account a2 = new Account (Name = 'Test installer 2', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a3 = new Account (Name = 'Test customer 1',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a3;

        Account a4 = new Account (Name = 'Test customer 2',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a4;

        Account a5 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a5;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        insert p1;
        
        Document doc1 = new Document();
        doc1.Body = Blob.valueOf('Some Text');
        doc1.ContentType = 'application/pdf';
        doc1.DeveloperName = 'my_document';
        doc1.IsPublic = true;
        doc1.Name = 'Sunlight Logo';
        doc1.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert doc1;


        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a3.Id,
                                            Co_Applicant__c = a4.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;

        o1.Co_Applicant__c = null;
        update o1;

        p1.Installer_Account__c = a2.Id;
        update p1;

        p1.Installer_Account__c = a1.Id;
        update p1;
        
        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
        
    }

    @isTest static void test2 () 
    {
        
        SLFUtilityTest.createCustomSettings();
        Partner_Roles__c roles = new Partner_Roles__c();
        roles.Name ='ServicePartnerUser';
        roles.Role_Name__c ='ServicePartnerUser';
        insert roles;
        Account a1 = new Account (Name = 'Test installer 1', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        Account a2 = new Account (Name = 'Test installer 2', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a3 = new Account (Name = 'Test customer 1',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a3;

        Account a4 = new Account (Name = 'Test customer 2',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a4;

        Account a5 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a5;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a3.Id,
                                            Co_Applicant__c = a4.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;

        Database.executeBatch (new BatchRebuildOpportunityShares(), 50);
    }   

    @isTest static void test3 () 
    {
        SLFUtilityTest.createCustomSettings(); 
        Partner_Roles__c roles = new Partner_Roles__c();
        roles.Name ='ServicePartnerUser';
        roles.Role_Name__c ='ServicePartnerUser';
        insert roles;
        
        Account a1 = new Account (Name = 'Test installer 1', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        Account a2 = new Account (Name = 'Test installer 2', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a3 = new Account (Name = 'Test customer 1',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a3;

        Account a4 = new Account (Name = 'Test customer 2',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a4;

        Account a5 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a5;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a3.Id,
                                            Co_Applicant__c = a4.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;

        Database.executeBatch (new BatchRebuildProductShares(), 50);
    }
}