@isTest
public class NotesServiceImplTest {
    @testSetup private static void createTestData(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c=2.99;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        insert prod;
        
        //Insert opportunity record
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = acc.id;
        personOpp.Installer_Account__c = acc.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Install_Street__c ='Street';
        personOpp.Install_City__c ='InCity';
        personOpp.Combined_Loan_Amount__c =10000;
        personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
        //opp.Approved_LT_Facility__c ='TCU';
        personOpp.SLF_Product__c = prod.id;            
        insert personOpp;
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Street__c ='Street';
        opp.Install_City__c ='InCity';
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'TCU||123';
        //opp.Approved_LT_Facility__c = 'TCU';
        opp.SLF_Product__c = prod.id;
        opp.Opportunity__c = personOpp.Id; 
        opp.Block_Draws__c = false;
        opp.Has_Open_Stip__c = 'False';
        opp.All_HI_Required_Documents__c=true;
        opp.Max_Draw_Count__c=3;
        opp.ProductTier__c = '0';         
        opp.Type_of_Residence__c  = 'Own';
        opp.language__c = 'Spanish';
        opp.Project_Category__c = 'Solar';
        insert opp;
      
        
        
        
    }
    private testMethod static void setNotesServiceImplTest(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name,Opportunity__c,Synced_Credit_Id__c from opportunity where AccountId =: acc.Id AND Opportunity__c != null LIMIT 1]; 
        
        System.runAs(loggedInUsr[0])
        {
            Map<String, String> setNotesMap = new Map<String, String>(); 
            String setNotesService;
            
            
            
            //Valid Request
            setNotesService = '{"projects":[{"id":"","notes":[{"id":"","title":"test1","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            setNotesService = '{"projects":[{"id":"'+opp.id+'","notes":[{"id":"","title":"test1","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            setNotesService = '{"projects":[{"id":"'+opp.id+'","externalId":"123","notes":[{"id":"","title":"test1","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            setNotesService = '{"projects":[{"id":"'+opp.id+'","externalId":"","notes":[{"id":"","title":"","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            setNotesService = '{"projects":[{"id":"","externalId":"123","notes":[{"id":"","title":"test1","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            setNotesService = '{"projects":[{"id":"12345","externalId":"","notes":[{"id":"","title":"test1","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            setNotesService = '{"projects":[{"id":"","externalId":"1234534","notes":[{"id":"","title":"test1","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            setNotesService = '{"projects":[{"id":"","hashId":"1234534","notes":[{"id":"","title":"test1","body":"update1"}]}]}';
            MapWebServiceURI(setNotesMap,setNotesService,'setnotes');
            
            
            //To cover exception block
            NotesDataServiceImpl createApp = new NotesDataServiceImpl();
            createApp.transformOutput(null);
            NotesServiceImpl createAppSrsImpl = new NotesServiceImpl();
            createAppSrsImpl.fetchData(null); 
            
        }
    }
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
    
}