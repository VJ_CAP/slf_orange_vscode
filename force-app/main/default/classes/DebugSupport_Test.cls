@isTest
private class DebugSupport_Test  
{
    static testMethod void test1 () 
    {
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        insert trgAccflag;
        Account a1 = new Account (Name = 'Test Account 1',Installer_Legal_Name__c='Bright Solar Planet');
        Account a2 = new Account (Name = 'Test Account 2',Installer_Legal_Name__c='Bright Solar Planet');
        Account a3 = new Account ();
        Account a4 = new Account (Name = 'Test Account 4',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a4;
        Account a5 = new Account (Name = 'Test Account 5',Installer_Legal_Name__c='Bright Solar Planet');

        DebugSupport.sendDebugEmail('Test Content', 'Test Subject', 'test@mailinator.com');

        DebugSupport.insertAndReturnErrors(new list<Account>{a1,a2,a3});
        a1.Name = null;
        DebugSupport.updateAndReturnErrors(new list<Account>{a1,a2,a3});
        DebugSupport.deleteAndReturnErrors(new list<Account>{a4});
        DebugSupport.insertAndReturnErrors(a5);
        a5.Name = null;
        DebugSupport.updateAndReturnErrors(a5);
        DebugSupport.deleteAndReturnErrors(a5);
        
        DebugSupport.sendDebugEmail('', '', '');
    }   
    
    static testMethod void test2 () 
    {
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        insert trgAccflag;     
        Account a1 = new Account (Name = 'Test Account 1',Installer_Legal_Name__c='Bright Solar Planet');
        Account a2 = new Account (Name = 'Test Account 2',Installer_Legal_Name__c='Bright Solar Planet');
        Account a3 = new Account ();
        Account a4 = new Account (Name = 'Test Account 4',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a4;
        Account a5 = new Account (Name = 'Test Account 5',Installer_Legal_Name__c='Bright Solar Planet');

        DebugSupport.sendDebugEmail('Test Content', 'Test Subject', 'test@mailinator.com');
        
        DebugSupport.sendDebugEmail('', '', '');


    }
}