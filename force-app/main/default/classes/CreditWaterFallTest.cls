@isTest
public class CreditWaterFallTest{
    private testMethod static void testCreditWaterFall(){
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);
        TriggerFlags__c trgflag3 = new TriggerFlags__c();
        trgflag3.Name ='SLF_Credit__c';
        trgflag3.isActive__c =true;
        trgrList.add(trgflag3);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='State_Allocation__c';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        insert trgrList;
        
        //Insert Custom Endpoints Custom Setting Record
        List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
        SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
        endpoint1.Name = 'Pricing';
        endpoint1.Endpoint_URL__c =  'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
        endpoint.add(endpoint1);
        SLF_Boomi_API_Details__c endpoint2 = new SLF_Boomi_API_Details__c();
        endpoint2.Name = 'BoomiToAWSPricing';
        endpoint2.Endpoint_URL__c =  'https://test.connect.boomi.com/ws/rest/v1/dev/pricing/callaws/';
        endpoint.add(endpoint2);
        
        
        SLF_Boomi_API_Details__c apidetails = new SLF_Boomi_API_Details__c();
        apidetails.Name ='Boomi QA';
        apidetails.Username__c ='sunlightfinancial-WSHA3Z.N29SCE';
        apidetails.Password__c ='2458a5de-d689-4708-a4fb-d3594c067610';
        apidetails.Endpoint_URL__c ='https://test.connect.boomi.com/ws/soap12/prequal';
        apidetails.viaBoomi__c =true;
        endpoint.add(apidetails);
        
        SLF_Boomi_API_Details__c endpoint3 = new SLF_Boomi_API_Details__c();
        endpoint3.Name = 'FNI QA';
        endpoint3.Username__c =  'salesforceuser';
        endpoint3.Password__c = 'RC8N1CreKnz4rm2kMTCj';
        endpoint.add(endpoint3);
        
        SLF_Boomi_API_Details__c endpoint4 = new SLF_Boomi_API_Details__c();
        endpoint4.Name = 'FNI Prod';
        endpoint4.Username__c =  'salesforceuser';
        endpoint4.Password__c = 'AmtPHqSlK9sB6NXjSyq4';
        endpoint.add(endpoint4);
        insert endpoint;  
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'Solar';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';        
        accObj.Type = 'Partner';
        accObj.Solar_Enabled__c=true;
        insert accObj;
        
        //Facility Account
        Account accObj1 = new Account();
        accObj1.name = 'Test Facility';
        accObj1.RecordTypeId = recTypeInfoMap.get('Facility').getRecordTypeId();
        accObj1.BillingStateCode = 'CA';
        accObj1.Type = 'Facility';
        accObj1.Solar_Enabled__c=true;
        insert accObj1;
        
       Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.MiddleName = 'E';
        acc.SSN__c = '1234568789';
        acc.Last_four_SSN__c = '8789';
        acc.Push_Endpoint__c ='www.test.com';
        acc.PersonEmail = 'slfaccount@slf.com';
        acc.RecordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        acc.Installer_Email__c = 'slfaccount@slf.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Push_Endpoint__c = 'https://www.google.com/';
        acc.Eligible_for_Last_Four_SSN__c = true;
        acc.Marital_Status__c = 'Married';
        acc.BillingPostalCode = '25846';
        acc.Phone = '9876543210';
        acc.Employer_Name__c = 'dax';
        Acc.PersonBirthdate = Date.newInstance(1997, 12, 9);
        acc.ShippingPostalCode = '98562';
        acc.PersonOtherPhone = '9876532410';
        acc.Length_of_Employment_Years__c = 6.00;
        acc.Length_of_Employment_Months__c = 8.00;
        acc.Job_Title__c = 'Developer';
        insert acc;
        
        Account acc1 = new Account();
        acc1.LastName = 'Test Account';
        acc1.MiddleName = 'E';
        acc1.SSN__c = '123456987';
        acc1.Last_four_SSN__c = '6987';
        acc1.Push_Endpoint__c ='www.test.com';
        acc1.PersonEmail = 'slfaccount@slf.com';
        acc1.RecordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        acc1.Installer_Email__c = 'slfaccount@slf.com';
        acc1.Installer_Legal_Name__c='Bright planet Solar';
        acc1.Push_Endpoint__c = 'https://www.google.com/';
        acc1.Eligible_for_Last_Four_SSN__c = true;
        acc1.Marital_Status__c = 'Married';
        acc1.BillingPostalCode = '25846';
        acc1.Phone = '9876543210';
        acc1.Employer_Name__c = 'dax';
        acc1.PersonBirthdate = Date.newInstance(1997, 12, 9);
        acc1.ShippingPostalCode = '98652';
        acc1.PersonOtherPhone = '8965321470';
        acc1.Length_of_Employment_Years__c = 6.00;
        acc1.Length_of_Employment_Months__c = 8.00;
        acc1.Job_Title__c = 'Tester';
        insert acc1;
      
        Product__c prdObj = new Product__c(Name='Test',
                                           APR__c = 1.99,
                                           Product_Loan_Type__c='Battery Only',
                                           Installer_Account__c = accObj.Id,
                                           Product_Tier__c= '0',
                                           State_Code__c='CA',
                                           Long_Term_Facility__c = 'TCU',
                                           FNI_Min_Response_Code__c=2,
                                           Product_Display_Name__c = 'Display Name',
                                           Qualification_Message__c = 'Message');
        insert prdObj;
        
        Opportunity oppObj = new Opportunity(Name = 'OppName',
                                             AccountId=accObj.Id,
                                             StageName='Qualified',
                                             CloseDate=system.today(),
                                             SLF_Product__c = prdObj.ID,
                                             Install_State_Code__c = 'CA',
                                             Install_Postal_Code__c = '98652',
                                             Long_Term_Loan_Amount_Manual__c = 985200,
                                             Combined_Loan_Amount__c = 100250,
                                             Monthly_Payment_Escalated_Single_Loan__c = 1000);
        insert oppObj;
        
        State_Allocation__c saObj = new State_Allocation__c(Installer_Account__c=accObj.Id, 
                                                            State_Code__c='CA',
                                                            Facility_1__c=accObj1.Id,
                                                            Facility_1_Allocation__c = 10,
                                                            Facility_2_Allocation__c = 10,
                                                            Facility_3_Allocation__c = 10,
                                                            Facility_4_Allocation__c = 10,
                                                            Facility_5_Allocation__c = 10,
                                                            Facility_6_Allocation__c = 10,
                                                            Facility_7_Allocation__c = 10,
                                                            Facility_8_Allocation__c = 10,
                                                            Facility_9_Allocation__c = 20
                                                           );
        insert saObj;
        

        SLF_Credit__c slfCreditObj = new SLF_Credit__c();
        slfCreditObj.Opportunity__c = oppObj.id;
        slfCreditObj.Primary_Applicant__c = acc.id;
        slfCreditObj.Co_Applicant__c = acc.id;
        slfCreditObj.SLF_Product__c = prdObj.Id ;
        slfCreditObj.Total_Loan_Amount__c = 100;
        slfCreditObj.Status__c = 'New';
        slfCreditObj.LT_FNI_Reference_Number__c = '123';
        slfCreditObj.Application_ID__c = '123';
        insert slfCreditObj;
        
        Prequal__c prqlObj = new Prequal__c(Opportunity__c = oppObj.Id,
                                            Middle_Name__c = 'e',
                                            SSN__c = '123456789',
                                            DOB__c = system.today(),
                                            Mailing_Postal_Code__c = '25846',
                                            Annual_Income__c = 12300,
                                            Lending_Facility__c = 'CVX,TCU',
                                            Last_Name__c = 'Test',
                                            Employment_Years__c=5,
                                            Employment_Months__c = 1,
                                            Co_LastName__c = 'Test',
                                            Co_FirstName__c = 'Test',
                                            Co_Middle_Initial__c = 'w',
                                            Co_SSN__c = '123456758',
                                            Installer_Account__c = accObj.Id,
                                            Co_Date_of_Birth__c = System.today(),
                                            Co_Mailing_Street__c = 'Street',
                                            Co_Mailing_City__c = 'City',
                                            Co_Mailing_State__c = 'CA',
                                            Co_Mailing_Postal_Code__c = '45678',
                                            Co_Employer_Name__c = 'Employee',
                                            Co_Annual_Income__c = 200000,
                                            Co_Employment_Years__c = 1,
                                            Co_Employment_Months__c = 2,
                                            Requested_Loan_Amount__c = 2000,
                                            Term_mo__c = 24,
                                            APR__c = 1.99
                                            );
        
        insert prqlObj;
        System.assertNotEquals(null,prqlObj);
        
        Document doc = new Document();
        doc.Body = Blob.valueOf('Some Text');
        doc.ContentType = 'application/pdf';
        doc.DeveloperName = 'my_document';
        doc.IsPublic = true;
        doc.Name = 'Sunlight Logo';
        doc.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert doc;
        
        Test.StartTest();
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(accObj,accObj,oppObj,slfCreditObj,'12345678');
        
        prdObj.Product_Loan_Type__c = 'SolarPlus Roof';
        update prdObj;        
        prqlObj.Term_mo__c = null;
        prqlObj.APR__c = null;       
        update prqlObj;
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(accObj,accObj,oppObj,slfCreditObj,'12345678');
               
        accObj.Risk_Based_Pricing__c = true;
        accObj.Credit_Waterfall__c = true;
        update accObj;
        oppObj.SLF_Product__c = null;
        update oppObj;
        prdObj.Product_Loan_Type__c = 'Battery Only';
        update prdObj;
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(accObj,accObj,oppObj,slfCreditObj,'12345678');
        
        prdObj.Product_Loan_Type__c = 'Solar';
        update prdObj;
        prqlObj.Lending_Facility__c = 'CVX';
        update prqlObj;
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(accObj,accObj,oppObj,slfCreditObj,'12345678');
        
        prdObj.Product_Loan_Type__c = 'SolarPlus Roof';
        update prdObj;   
        
        oppObj.Monthly_Payment_Escalated_Single_Loan__c = null;
        update oppObj;
        try{
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(accObj,accObj,oppObj,slfCreditObj,'12345678');
        }catch(Exception ex){}
        
        try{
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(acc,acc1,oppObj,slfCreditObj,'12345678');
        }catch(Exception ex){}
        
        acc.Eligible_for_Last_Four_SSN__c = false;
        update acc;
        acc1.Eligible_for_Last_Four_SSN__c = false;
        update acc1;
        
         try{
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(acc,acc1,oppObj,slfCreditObj,'12345678');
        }catch(Exception ex){}
        
        acc.SSN__c = null;
        update acc;
        acc1.SSN__c = null;
        update acc1;
        
         try{
        CreditWaterFall.buildSoftPullFNIReq(prqlObj);
        CreditWaterFall.buildHardPullFNIReq(acc,acc1,oppObj,slfCreditObj,'12345678');
        }catch(Exception ex){}
        
        Test.StopTest();
    }
}