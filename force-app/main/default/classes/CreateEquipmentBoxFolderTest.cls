@isTest
public Class CreateEquipmentBoxFolderTest{
    public testMethod static void testCreateEquipmentBoxFolder(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();          
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);  
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);     
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);       
        insert trgLst;
        
         //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();  
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        //Insert opportunity record
        Opportunity oppObj = new Opportunity();
        oppObj.name = 'OppNew';
        oppObj.CloseDate = system.today();
        oppObj.StageName = 'New';
        oppObj.AccountId = acc.id;
        oppObj.Installer_Account__c = acc.id;
        oppObj.Install_State_Code__c = 'CA';
        oppObj.Install_Street__c ='Street';
        oppObj.Install_City__c ='InCity';
        oppObj.Combined_Loan_Amount__c = 10000;
        oppObj.Partner_Foreign_Key__c = 'TCU||1234';
        oppObj.Co_Applicant__c = acc.id;
        insert oppObj;              
        
        SLF_Credit__c credit = new SLF_Credit__c();
        credit.Opportunity__c = oppObj.id;
        credit.Primary_Applicant__c = acc.id;
        credit.Co_Applicant__c = acc.id;
        credit.Total_Loan_Amount__c = 100;
        credit.Status__c = 'New';
        credit.LT_FNI_Reference_Number__c = '123';
        credit.Application_ID__c = '123';
        insert credit;
        
        CreateEquipmentBoxFolder.FolderWrapper fldrObj = new CreateEquipmentBoxFolder.FolderWrapper(); 
        fldrObj.opportunityId = oppObj.Id;
        fldrObj.FolderName = 'folder';
        CreateEquipmentBoxFolder obj = new CreateEquipmentBoxFolder();
        CreateEquipmentBoxFolder.CreateNewFolder(new List<CreateEquipmentBoxFolder.FolderWrapper>{fldrObj});
                CreateEquipmentBoxFolder.CreateNewFolder(new List<CreateEquipmentBoxFolder.FolderWrapper>{null});


    }
}