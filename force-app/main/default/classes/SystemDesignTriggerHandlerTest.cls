/**
* Description: Test class for SystemDesignTrg trigger and SystemDesignTriggerHandler class. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         10/27/2017          Created
******************************************************************************************/
@isTest
public class SystemDesignTriggerHandlerTest {

    /**
    * Description: Test method to cover SystemDesignTriggerHandler funtionality.
    */
    public testMethod static void systemDesignTriggerTest()
    {
        test.starttest();
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
         
        List<SystemDesign_to_Opportunity_Sync__c> syncFieldsLst = new List<SystemDesign_to_Opportunity_Sync__c>();
        SystemDesign_to_Opportunity_Sync__c syncFields = new SystemDesign_to_Opportunity_Sync__c();
        syncFields.Name = 'Est. Annual Production (kWh)';
        syncFields.From_System_Design__c = 'Est_Annual_Production_kWh__c';
        syncFields.To_Opportunity__c = 'Est_Annual_Production_kWh__c';
        syncFieldsLst.add(syncFields);
        
        SystemDesign_to_Opportunity_Sync__c syncFields1 = new SystemDesign_to_Opportunity_Sync__c();
        syncFields1.Name = 'Ground Mount Type';
        syncFields1.From_System_Design__c = 'Inverter_Manufacturer__c';
        syncFields1.To_Opportunity__c = 'Inverter__c';
        syncFieldsLst.add(syncFields1);
        
        SystemDesign_to_Opportunity_Sync__c syncFields2 = new SystemDesign_to_Opportunity_Sync__c();
        syncFields2.Name = 'Inverter';
        syncFields2.From_System_Design__c = 'Ground_Mount_Type__c';
        syncFields2.To_Opportunity__c = 'Ground_Mount_Type__c';
        syncFieldsLst.add(syncFields2);
        
        SystemDesign_to_Opportunity_Sync__c syncFields3 = new SystemDesign_to_Opportunity_Sync__c();
        syncFields3.Name = 'Module';
        syncFields3.From_System_Design__c = 'Module_Manufacturer__c';
        syncFields3.To_Opportunity__c = 'Module__c';
        syncFieldsLst.add(syncFields3);
        
        SystemDesign_to_Opportunity_Sync__c syncFields4 = new SystemDesign_to_Opportunity_Sync__c();
        syncFields4.Name = 'Project Type';
        syncFields4.From_System_Design__c = 'Project_Type__c';
        syncFields4.To_Opportunity__c = 'Project_Type__c';
        syncFieldsLst.add(syncFields4);
        
        SystemDesign_to_Opportunity_Sync__c syncFields5 = new SystemDesign_to_Opportunity_Sync__c();
        syncFields5.Name = 'System Size STC (kW)';
        syncFields5.From_System_Design__c = 'System_Size_STC_kW__c';
        syncFields5.To_Opportunity__c = 'System_Size_kW__c';
        syncFieldsLst.add(syncFields5);
        
        insert syncFieldsLst;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Push_Endpoint__c = 'https://requestb.in';
        personAcc.FNI_Domain_Code__c = 'TCU';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        
        insert personAcc;
        
        //Insert SLF Product Record
        Product__c prod = new Product__c();
        prod.Installer_Account__c =personAcc.id;
        prod.Long_Term_Facility_Lookup__c =personAcc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 88;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c=87;
        prod.Internal_Use_Only__c = true;

        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 1;
        prod.Product_Tier__c = '0';
        insert prod;
        
        
        //Insert opportunity record
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = personAcc.id;
        personOpp.Installer_Account__c = personAcc.id;
        personOpp.Install_State_Code__c = 'CA'; 
        personOpp.Combined_Loan_Amount__c =10000;
        personOpp.Partner_Foreign_Key__c = 'TCU||12345';
        //opp.Approved_LT_Facility__c ='TCU';
        personOpp.SLF_Product__c = prod.id;
        
        insert personOpp;
        
        //Insert System Design Record
        System_Design__c sysDesignRec = new System_Design__c();
        sysDesignRec.System_Cost__c = 11;
        sysDesignRec.Opportunity__c = personOpp.Id;
        sysDesignRec.Est_Annual_Production_kWh__c = 10;
        sysDesignRec.Inverter_Count__c = 11;
        sysDesignRec.Inverter_Model__c = '12';
        sysDesignRec.Module_Count__c = 10;
        sysDesignRec.Module_Model__c = '11';
        insert sysDesignRec;

        Quote qte = new Quote();
        qte.name = 'test';
        qte.OpportunityId = personOpp.id;
        qte.System_Design__c = sysDesignRec.id;
        insert qte;
        
        Opportunity updateOpty = new Opportunity();
        updateOpty.Id = personOpp.Id;
        updateOpty.SyncedQuoteId = qte.Id;
        update updateOpty;
        
        update sysDesignRec;
        test.stoptest();    
    }
}