global class BatchBalanceTransactions 
                implements Database.Batchable<SObject>, 
                    Database.Stateful,
                    Schedulable
{

    map<String,String> typesMap = new map<String,String>
    {
        'Installer M0 Payment Paid' => 'Installer M0 Payment Scheduled',
        'Installer M1 Payment Paid' => 'Installer M1 Payment Scheduled',
        'Installer M2 Payment Paid' => 'Installer M2 Payment Scheduled',
        'Installer M0 Net Out Paid (Taken)' => 'Installer M0 Net Out Scheduled',
        'Incentive Payment Paid' => 'Incentive Payment Scheduled',
        'Short Term Membership Fee Paid' => 'Short Term Membership Fee Scheduled',
        'Long Term Membership Fee Paid' => 'Long Term Membership Fee Scheduled',
        'Installer M0 Net Out Paid (Taken)' => 'Installer M0 Net Out Scheduled'
    };

    class Txn
    {
        String type;
        Decimal amount;

        Txn (String type, Decimal amount)
        {
            this.type = type;
            this.amount = amount;
        }
    }

    Integer counter1 = 0;
    Integer counter2 = 0;
    Integer counter3 = 0;
    Integer counter4 = 0;
    Integer ufs = 0;
    Integer ufsProcessed = 0;
    Integer txnsWritten = 0;

//    Job__c job;

//********************************************************************************
    
    global BatchBalanceTransactions() 
    {
        //  nothing to do
    }

//********************************************************************************
    
    global Database.QueryLocator start (Database.BatchableContext BC) 
    {

        String query = 'SELECT ' + getFieldNamesCsv ('Underwriting_File__c') + 
                                            ', Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c, ' +
                                            ' Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c, ' +
                                            ' Opportunity__r.Installer_Account__c, ' +
                                            ' Opportunity__r.Installer_Account__r.Name, ' +
                                            ' (SELECT Id, Type__c, Amount__c FROM Underwriting_Transactions__r) ' +
                                            ' FROM Underwriting_File__c';  

        if (Test.isRunningTest()) query += ' LIMIT 10';

//    	this.job = new Job__c ();
//    	insert job;

        return Database.getQueryLocator(query);
    }

//********************************************************************************

    global void execute (SchedulableContext sc)
    {
        Database.executeBatch (new BatchBalanceTransactions(), 100);
    }

//********************************************************************************

    global void execute (Database.BatchableContext BC, List<SObject> scope) 
    {
        list<Underwriting_Transaction__c> uts = new list<Underwriting_Transaction__c>{};

        for (Underwriting_File__c uf : (list<Underwriting_File__c>) scope)
        {
            this.ufs++;

            set<String> schTxns = new set<String>{};

            for (Underwriting_Transaction__c ut : uf.Underwriting_Transactions__r)
            {
                if (ut.Type__c.contains('Scheduled')) 
                {
                    schTxns.add (ut.Type__c + String.valueOf(ut.Amount__c));
                    schTxns.add (ut.Type__c);
                }
            }

            System.debug ('AlanDebug ' + schTxns);

            ufsProcessed++;

            for (Underwriting_Transaction__c ut : uf.Underwriting_Transactions__r)
            {
                String newType = typesMap.get (ut.Type__c);

                System.debug ('AlanDebug ' + ut.Type__c);

                if (newType != null)
                {
                    //  1.  there is a matching scheduled type and amount, so do nothing
                    //  2.  there is a matching scheduled type, but a different amount, so write a scheduled HE transaction,
                    //      unless there is already a matching scheduled (Historical Exception) type and amount
                    //  3.  there is no matching scheduled type, so write a scheduled transaction

                    System.debug ('AlanDebug ' + schTxns);

                    if (schTxns.contains (newType + String.valueOf(ut.Amount__c)))  //  1.
                    {
                        this.counter1++;
                        continue;
                    }

                    if (schTxns.contains (newType))  //  2.
                    {
                        newType += ' (Historical Exception)';
                        this.counter2++;

                        if (schTxns.contains (newType))  //  a HE has already been written
                        {
                            this.counter3++;
                            continue;  
                        } 
                    }
                    else  //  3.
                    {
                        this.counter4++;
                    }

                    Underwriting_Transaction__c newUt = new Underwriting_Transaction__c ();
                    newUt.Underwriting_File__c = uf.Id;
                    newUt.Type__c = newType;
                    newUt.Amount__c = ut.Amount__c;
                    newUt.Test_Only__c = true;

                    uts.add (newUt);                        
                }
            }
        }
        insert uts;

        this.txnsWritten = this.txnsWritten + uts.size();
    }

//********************************************************************************
    
    global void finish (Database.BatchableContext BC) 
    {
/*
        this.job.Log__c =   'Total UFs = ' + ufs +
                            '\nUFs processed = ' + ufsProcessed +
                            '\nMatching scheduled type and amount = ' + this.counter1 + 
                            '\nMatching scheduled type, but a different amount = ' + this.counter2 + 
                            '\nMatching scheduled (Historical Exception) type and amount = ' + this.counter3 + 
                            '\nNo matching scheduled type = ' + this.counter4 +
                            '\nNumber of Transaction records written = ' + this.txnsWritten;
        update this.job;
*/
    }   

//********************************************************************************

    private static String getFieldNamesCsv (String objectName) 
    {
        list<String> fieldNames = getFieldNamesMap(objectName).values();
        
        String fields = '';

        if (fieldNames.size() == 0) return fields;
        
        for (String s : fieldNames)
        {
            fields += ', ' + s;
        }
        
        return fields.substring(2);
    }

//********************************************************************************
    
    private static map<String,String> getFieldNamesMap (String objectName)
    {
        map<String,String> fieldNamesMap = new map<String,String>{};

        SObjectType sot = Schema.getGlobalDescribe().get(objectName); 
                    
        String fields = '';

        if (sot <> null)
        {
        
            for (Schema.SObjectField sof : sot.getDescribe().fields.getMap().values())
            {
                String fieldName = sof.getDescribe().getName();
                fieldNamesMap.put(fieldName, fieldName);
            }
        }
                
        return fieldNamesMap;
    }
}