public class BoxUtil {  //reverted to production version for bugfix, see boxutilbackup012417 for new development version.  will revert after patch and carry across - pdm
    public class BoxUtilException extends Exception {}
    private static string UPLOAD_VERSION_URL = 'files/{0}/content';
    private static String SHARED_LINK_URL = 'folders/{0}/';
    @testVisible private static BoxApiConnection sharedAPI;
    private static boolean resetConnection = true;
    
    public class SiteDownloadWrapper
    {
        public Site_Download__c site_download {get; set;}
        public Attachment attachment {get; set;}
    }
        
        
     public static String uploadBoxFile(String boxFolderId, String fileName, String status, BLOB body)
     {         
         BoxApiConnection api = getBoxAPISession();
         String fid=null;
         try { 
         BoxFolder bf = new BoxFolder(api,boxFolderId);       
         
         BoxFile.Info info = bf.uploadFile(body,fileName);
         
         BoxFile f = new BoxFile(api,info.id);
         f.addAttributes('enterprise','tracking','{"status":"Submitted"}');
         fid = f.getId();
         } catch (Exception e) {
             // *** if the file name is in use we check if it is a new version, if not, 
             // *** we have already uploaded it in the past
             
             String m = e.getMessage();
       System.debug('***Debug: File name conflict: ' + e.getMessage());
             if (m.length() > 0 && m.indexOf('item_name_in_use') > 0) { 
                 String oldSHA1 = m.substring(m.indexOf('sha1')+7,m.indexOf('sha1')+47);
                 String newSHA1 = generateSHA1Digest(body);
                 // *** if the digests don't match we have to upload a new version
  
                 if (!oldSHA1.equals(newSHA1)) {
                     String fileId = m.substringAfter('"id":"').substringBefore('",');
                     //String fileId = (m.substring(m.indexOf('"id":')+6,(m.indexOf('"id":')+18)));//changed from 17
                     BoxUtil.uploadNewVersionOfFile(fileId,fileName,body);
                 }
                 return fid;
             } else {
               throw new BoxUtilException('cause => ' + e.getCause() + ' msg => ['  + e.getMessage() + ']');
             }
         } finally {
              setBoxTokens();
         }
         return fid;
     }
    
    
     /***
     ** Retrieves Child Folders for a Folder in Box
     ***/
     public static Map<String, String> getChildFoldersFromBox(String boxFolderID)
     {
        BoxApiConnection api = getBoxAPISession();

        Map<String, String> folderNameIdMap = new Map<String, String>();
        try {
            System.debug('***Debug: api: ' + api);
            BoxFolder bf = new BoxFolder(api,boxFolderID); 
    
            List<BoxItem.Info> info = bf.getChildren(null,null);
            
            for (BoxItem.Info i : info) {
                if (i.type == 'folder')
                    folderNameIdMap.put(i.name,i.id);  
            }
    } catch (Exception e) {
             throw new BoxUtilException('Check Connection. Unable to get child folders for ' + boxFolderID + ' ' + e.getMessage());
         } finally {
              setBoxTokens();
         }
        return folderNameIdMap;
     }
    
    

    
    
     public static String getFolderSharedLink(String boxFolderID) 
     {
       
        BoxApiConnection api = getBoxAPISession();
        BoxGenericJsonObject sharedLinkObject;
        
        try { 
        String url = api.getBaseUrl() + String.format(SHARED_LINK_URL, new String[] {boxFolderID});
        BoxApiRequest request = new BoxApiRequest(api, url, BoxApiRequest.METHOD_PUT);
        request.setBody('{"shared_link": {"access": "open"}}');
        request.setTimeout(api.timeout);
        request.addJsonContentTypeHeader();

        HttpResponse response = request.send();
        System.Debug('***** got Shared Link Response ' + response);
        if (response.getStatusCode() != 200) {
                throw new BoxUtilException('Unable to get Shared Link for folderID ' + boxFolderID + ' response => ' + response);
        }
        String responseBody = BoxApiRequest.getBoxResourceResponseBody(response, 'BoxFile.getFileInfo');
        BoxGenericJsonObject responseObject = new BoxGenericJsonObject(responseBody);
        sharedLinkObject =  new BoxGenericJsonObject(responseObject.getValue('shared_link'));
            
       } catch (Exception e) {
             throw new BoxUtilException(e.getMessage() + ' ' + e.getStackTraceString());
       } finally {
              setBoxTokens();
       }
        return sharedLinkObject.getValue('url');
    }
    
    
    
     public static String getBoxFolderId(Id sfRecordId)
     {
        String boxFolderId;
        List<box__FRUP__c> fl =  new List<box__FRUP__C>([SELECT box__Folder_ID__c, box__Record_Id__c FROM box__FRUP__c where box__Record_ID__c = :sfRecordId]);
        
        if (fl.size() == 0) 
            throw new BoxUtilException('Unable to find box folder for sf record id ' + sfRecordId);
        
        return fl[0].box__Folder_ID__c;
     }
    
    
     public static List<Underwriting_File__c> queryCategoryMappingFieldsFromUnderwriting(String oppId)
     {
     Map<string,SF_Category_Map__c> categoryFolderMappings = SF_Category_Map__c.getAll();
        
        // ** create the list of fields that need to be queried
        String fieldList = '';
        Map<String, Boolean> fieldProcessedMap = new Map<String, Boolean>();
        for (SF_Category_Map__c s : categoryFolderMappings.values() ) 
        {
            if (!String.isBlank(s.Folder_Id_Field_on_Underwriting__c) && !fieldProcessedMap.containsKey(s.Folder_Id_Field_on_Underwriting__c))
            {
               fieldList = fieldList + s.Folder_Id_Field_on_Underwriting__c + ',';
                fieldProcessedMap.put(s.Folder_Id_Field_on_Underwriting__c,true);
          }
            if (!String.isBlank(s.TS_Field_On_Underwriting__c) && !fieldProcessedMap.containsKey(s.TS_Field_On_Underwriting__c))
            {
               fieldList = fieldList + s.TS_Field_On_Underwriting__c  + ',';  
                fieldProcessedMap.put(s.TS_Field_On_Underwriting__c,true);
            }
            
        }
        fieldList = fieldList.SubString(0,fieldList.length()-1);
        // Added as part of 1930 by Adithya
        if(string.isNotBlank(fieldList)){
            fieldList='(SELECT id,Underwriting__c,'+fieldList+' FROM Box_Fields__r)';
        }
        List<Underwriting_File__c> ul = Database.query('Select ' + fieldList + ' from Underwriting_File__c where Opportunity__c = :oppId LIMIT 1');
        return ul;
      }
    
    public static String generateSHA1Digest(Blob b)
      {
          Blob b1 =  Crypto.generateDigest('SHA1', b);

      return EncodingUtil.convertToHex(b1);
      }
    
    
     // *** SS ADD TO Upload a new version of file
    public static BoxFile.Info uploadNewVersionOfFile(String fileId, String fileName, Blob fileBody) {
      if (String.isEmpty(fileName)) {
        throw new BoxResource.BoxResourceException('fileName must not be null or empty when calling BoxFolder.uploadFile.');
      }
      if (fileBody == null || fileBody.size() == 0) {
        throw new BoxResource.BoxResourceException('fileBody must not be null or empty when calling BoxFolder.uploadFile.');
      }
      BoxFile.Info info = null;
        BoxApiConnection api = getBoxAPISession();
        try {
            String url = api.getBaseUploadUrl() + String.format(UPLOAD_VERSION_URL, new String[] {fileId});
            BoxApiRequest request = new BoxApiRequest(api, url, BoxApiRequest.METHOD_POST);
            request.setTimeout(api.getTimeout());
            request.setMultipartFormBody(fileBody, 'tempname');
            
            HttpResponse response = request.send();
            String responseBody = BoxApiRequest.getBoxResourceResponseBody(response, 'BoxFolder.uploadFile');
            BoxGenericJsonObject responseObject = new BoxGenericJsonObject(responseBody);
            if (responseObject.getValue('total_count') != null) {
                list<String> fileEntries = BoxJsonObject.parseJsonObjectArray(responseObject.getValue('entries'));
                if (fileEntries.size() > 0) {
                    return new BoxFile.Info(fileEntries[0]);
                }
            }
        } catch (Exception e) {
             throw new BoxUtilException(e.getMessage());
         } finally {
              setBoxTokens();
         }
      return info;
    }

    public static String getFolderSharedLinkNew(String boxFolderID) 
    {
       /*String enterpriseId = '698406';
     String publicKeyId = 'f9x3kfn3';
       String privateKey = 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDQBxLEQuq+yF0GxLd0D5hveDBiSg5dRaUfHTSA40CsWbSEMnCRI7E/s5x3GAmjzwCUrZmcRcF1MBNf3P6zWUVJiG/t9isjbQ4jd+nxXPdxpG5gKyyB1/uF2i1qihKTR44gzAcr+YXJZ5xMzhYZG8kjTSQGsocAuYQVb5K08DoyMkJSqCBhX1UijZ/qVKwway+pqBMAJgOw9XyDIgKEV8Xe/7hM3iCiERzlCmSGi4SBBYPlTCg+FhoRqFfeffoTWNurYFvAyHq9ucpw82+pSXvdVNTgPgnC92xmfun2XFHzwu80ZPM3lURE2RCdT4y53X9R+KjvlPGQL4KjTmMWlTCrAgMBAAECggEASmwdZKVkAfkAfullkFn7+PUQqjlARlIiLq1uGSYz/vTgxeKdru8L/HQvZ9PrrvThik9JA0VZXqliZQtU+6jWRuZO1N0atcPQKRQ/V6AgzkE7U6Befd1dGJfjY5gd6R7SVB1p+2t7V4B/xGJ+OWFdZgm098hqz/GOtXSDEcRXrZj0STzS2MDG51phJ3c2p3LRBtQAj7XlO5PHTswc5BNIAggFGD92Br2uheIdc0R57t93KngK4SZg5uBU8dkYuDFr90S+rUCRzf4yCTpFXB5FPRGelK70mH7PKG9Xdnniethdky8f9KQYLQWpvSMKXk7w6tG3F6Sc2n379bTB6Bq4QQKBgQDqeieFWv6AlbB3LjAfwOJlL40tfY15dxoldd7qAG8pHPFbxt+A69mBRE/Haxxo5JA8L+YMKJF5H68J9c9TyoaPzfd8l8Wf2jOSgqbtmpGd/weeP2EGQBEOZIwHRVlOhVl51DOH3hOof2LHiELxnkaqeQT+QNBdLFZ1IJAGU6q+8QKBgQDjH2V18xmUG0ubctSfUVxeUfTn9YVsAj9pR540cXnaIPB5vSv0kdxx241O75tsuPDQXirDQh5BG+Nhq3i0dvZeuZETMuPQAfGdVAk4dg6Rd2qLfDmn6My734F2jfhH4jTekk1hcCEZm7BXxSM8H0Px8AFWgaeu7qi+VOYngyphWwKBgCuH6CRq40rt/2fw7EBI5/8PF++1GnnNcc44ltzsnbaNSAXY4w+lOak1DHaiXRDgNiLmZZgmQ/OvvShlOUt+EwNJosO/zZHCycZOeT0tcBFcEob/jzzA0inQ4upKpqzcNuNWUQbNZOroU2dl1b/TuZzWgOXPFJ/npllyZSq3h8CRAoGBAMr18pAttqzgm3cFbbYVWcq7yHREtn8nnwzwwBKzWSWxC9RuVUpP0kQ445Unj6ffuOsrS2GDw0+BoXdVaR8zs8RAZcFXV7c5FXX3c+ntreUWZrLRRyCia9pzx9hu0/FA1ugSq22oaxvcQgCdHXiLpAce58MrR/KOOWMksSucDhJDAoGBANiOQiurNqnzLSOu/G0QBNDNCHyHJ1jEqWDbs3M81eMzY7l8hcxeMb1lixTVZGOZRel5ad+qvKUQpXHSQ2MGFb1OqmSUmGfE1SL5nu9dexWvBgulMKA1rbEP+QJhTZS6AkZ9N0yOmsXYGzwZkVQnBQACCNBuGMGvI0SZLPDo+SB5';
       String clientId = 'hi8r9b23svujxcyohtisheghais5y2bq';
       String clientSecret = 'UpCipxQliOKoWfvnDkAzN5ZUulk0Mt9j';

     BoxJwtEncryptionPreferences preferences = new BoxJwtEncryptionPreferences();
       preferences.setPublicKeyId(publicKeyId);
       preferences.setPrivateKey(privateKey);
       //BoxPlatformApiConnection api = BoxPlatformApiConnection.getAppEnterpriseConnection(enterpriseId, clientId, clientSecret, preferences);
    
       BoxApiConnection api = new BoxApiConnection(clientId,clientSecret);*/
       BoxApiConnection api = getBoxAPISession();
       BoxSharedLink link = new BoxSharedLink(api, boxFolderID);
       setBoxTokens();
       return ''; //link.getSharedLink();
    }
  
    public static String getBoxToken()
    {
      SFSettings__c settings = SFSettings__c.getOrgDefaults();
        
        if (settings.Use_Non_Cached_Settings__c)  {
            List<Non_Cached_Settings__c> sl = new List<Non_Cached_Settings__c>([Select Value__c from Non_Cached_Settings__c where Name__c ='Box Token']);
            return (sl[0].Value__c);
        } else {
            return settings.BOX_TOKEN__c;
        }
    }
    
    public static void abortBatchCheck()
    {
      
      //List<Non_Cached_Settings__c> sl = new List<Non_Cached_Settings__c>([Select Value__c from Non_Cached_Settings__c where Name__c ='Abort Batch']);
    //if (sl.size() > 0 && sl[0].Value__c.equals('1'))
        //  throw new BoxUtilException('Aborting batch because Abort flag set');
    }
    
  @testVisible private static BoxApiConnection getBoxAPISession()
  {  
        //  start ticket SP-193: Amend Batch Upload to Box use JWT Auth
        //  BoxAPISession now returns BoxAuthentication.getConnection()

        if (Test.isRunningTest() == false)
        {
            return BoxAuthentication.getConnection(); 
        }

        SFSettings__c settings = SFSettings__c.getOrgDefaults();
          //BoxApiConnection c;
        if (settings.USE_BOX_DEVELOPER_TOKEN__c) {
               sharedAPI = new BoxApiConnection(getBoxToken());
        } else {
            if(resetConnection){
                sharedAPI = new BoxApiConnection('hi8r9b23svujxcyohtisheghais5y2bq',
            'UpCipxQliOKoWfvnDkAzN5ZUulk0Mt9j', settings.BOX_ACCESS_TOKEN__c, settings.BOX_REFRESH_TOKEN__c);
                resetConnection = false;
            }
        }

        if (sharedAPI == null)
            throw new BoxUtilException('Unable to create BOX Connection');
        return sharedAPI;
    }
    
    public static void checkLimits()
    {
        System.Debug('IN CHECK LIMITS heap size => ' + Limits.getHeapSize() + ' limit => ' + Limits.getLimitHeapSize());
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
      
        if (Limits.getHeapSize() > settings.Heap_Threshold__c) 
              throw new BoxUtilException('Heap size => ' + Limits.getHeapSize() + ' Heap Threshold ' + settings.Heap_Threshold__c  + ' Exceeded. Aborting operation!');
      
  }
    
    @testVisible private static void setBoxTokens(){
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        if (!settings.USE_BOX_DEVELOPER_TOKEN__c) {
            if(!resetConnection){}
            settings.BOX_ACCESS_TOKEN__c = sharedAPI.getAccessToken();
            settings.BOX_REFRESH_TOKEN__c = sharedAPI.getRefreshToken();
            Upsert settings;
            resetConnection = true;
        
        }
        
    }
      
}