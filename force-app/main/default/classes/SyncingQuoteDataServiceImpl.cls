/**
* Description: Pricing Calculation related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/
public with sharing class SyncingQuoteDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+SyncingQuoteDataServiceImpl.class);
        
        List<String> ErrorMessage = new List<String>();
        system.debug('### stsDtObj '+stsDtObj); 
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean syncingQuoteBean = new UnifiedBean();  
        IRestResponse iRestRes;
        
        try{
        
            if(null != reqData.projects[0]){
                
                set<Id> OppId = new set<Id>();
                OppId.add(reqData.projects[0].Id);
                syncingQuoteBean = SLFUtility.getUnifiedBean(OppId,'Orange',false);            
                iRestRes = (IRestResponse)syncingQuoteBean;
            }
        }catch(Exception ex){
            system.debug('### SyncingQuoteDataServiceImpl.transformOutput():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('SyncingQuoteDataServiceImpl.transformOutput()',ex.getLineNumber(),'transformOutput()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }    
        system.debug('### Exit from transformOutput() of '+SyncingQuoteDataServiceImpl.class);
        
        return iRestRes;
    }  
}