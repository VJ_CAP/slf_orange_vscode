public with sharing class BoxAuthentication 
{
    public static BoxPlatformApiConnection getConnection ()
    {
    	return getConnection ('Production');  
    }

    public class CustomException extends Exception {}

//	****************************************
 
     public static BoxPlatformApiConnection getConnection (String orgName)
    {
        Box_Credential__mdt creds;
        StaticResource boxPrivateKey;

        try
        {
            creds = [SELECT Id, DeveloperName, Username__c, User_ID__c, Public_Key_ID__c,
        				Client_ID__c, Client_Secret__c
        				FROM Box_Credential__mdt WHERE DeveloperName = :orgName LIMIT 1];
        }
        catch (Exception e)
        {
            throw new CustomException ('Unable to get Box credentials for Org: ' + orgName +
                                        ' error: ' + e.getMessage());
        }

        try
        {
            boxPrivateKey = [SELECT Id, Name, Body, BodyLength 
                                FROM StaticResource 
                                WHERE Name = 'Box_Private_Key' LIMIT 1];
        }
        catch (Exception e)
        {
            throw new CustomException ('Unable to get private key: Box_Private_Key' +
                                        ' error: ' + e.getMessage());
        }

        String privateKey = boxPrivateKey.Body.toString();

        BoxJwtEncryptionPreferences preferences = new BoxJwtEncryptionPreferences();

        preferences.setPublicKeyId (creds.Public_Key_ID__c);
        preferences.setPrivateKey (privateKey);

        BoxPlatformApiConnection api = null;

        try
        {
        	api = BoxPlatformApiConnection.getAppUserConnection (creds.User_ID__c, 
        													creds.Client_ID__c, 
        													creds.Client_Secret__c, 
        													preferences);
        }
        catch (Exception e) 
        {
            if (Test.isRunningTest()) return null;
            
            throw new CustomException ('Unable to establish BoxPlatformApiConnection' +
                                        ' error: ' + e.getMessage());
        }

        if (api == null)
        {
            throw new CustomException ('Unable to establish BoxPlatformApiConnection');
        }

        return api;
    }

//	****************************************

    public static String getFolderIdForName (BoxPlatformApiConnection api, String name)

    //  this method returns the Id of the folder with the name supplied, that is within the Box Root Folder for the current Org
    //  it throws an Exception if the root folder in Sunlight Settings is Production and the current Org is a sandbox

    {
		String rootFolderID = SFSettings__c.getOrgDefaults().Root_Folder_ID__c;
        Boolean isProduction = ![SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;

        try
        {
            BoxFolder oppFolder = new BoxFolder(api, rootFolderID);

            if (oppFolder.getFolderInfo().Name.endswith('Production') && !isProduction)
            {
                throw new CustomException ('Accessing Production Box data from a sandbox is not allowed');
            }

            for (BoxItem.Info i : oppFolder.getChildren())
            {
                if (i.Name == name && i.Type == 'Folder') return i.Id;  //  found
            }
        }
        catch (Exception e) 
        {
            throw new CustomException ('Unable to get folder Id for ' + name +
                                        ' error: ' + e.getMessage());
        }
        throw new CustomException ('Unable to get folder Id for ' + name);
    }
}