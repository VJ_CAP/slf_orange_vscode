/**
* Description: Pricing Calculation related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Raviteja           09/16/2017          Created
******************************************************************************************/
public with sharing class SyncQuoteServiceImpl extends RESTServiceBase{  
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back with data and response status code.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + SyncQuoteServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        SyncQuoteDataServiceImpl syncquoteDataSrvImpl = new SyncQuoteDataServiceImpl(); 
        IRestResponse iRestRes;
        try{ 
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            //CreateProjectRequestWrapper reqData = (CreateProjectRequestWrapper)JSON.deserialize(rw.reqDataStr, CreateProjectRequestWrapper.class);
            res.response = syncquoteDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
            system.debug('### SyncQuoteServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' SyncQuoteServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null)); 
            UnifiedBean unifiedBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214',null);
            errorWrapperLst.add(errorWrapper);
            unifiedBean.error  = errorWrapperLst ;
            //syncben.error.add(errorMsg);
            unifiedBean.returnCode = '214';
            iRestRes = (IRestResponse)unifiedBean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + SyncQuoteServiceImpl.class);
        return res.response ;
    }
}