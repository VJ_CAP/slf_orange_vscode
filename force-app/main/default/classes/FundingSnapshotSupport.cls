global class FundingSnapshotSupport 
{

    static set<String> fieldNamesSet;

    global FundingSnapshotSupport() 
    {
        //  nothing to do
    }

//  **************************************************************

    global class GenerateFundingSnapshotsInput 
    {
        @InvocableVariable(required=true) global Id accountId;
        @InvocableVariable global Integer maxNumberOfUFs; 
        @InvocableVariable global Date startDate; 
        @InvocableVariable global Date endDate; 
        @InvocableVariable global Boolean prioritizedOnly; 
        @InvocableVariable global Boolean homeImprovement; 

        global GenerateFundingSnapshotsInput () {}

        global GenerateFundingSnapshotsInput (Id accountId, Integer maxNumberOfUFs)
        {
            this.accountId = accountId;
            this.maxNumberOfUFs = maxNumberOfUFs;
        }
    }

//  **************************************************************

    global class GenerateFundingSnapshotsOutput
    {
        @InvocableVariable global Boolean success;
        @InvocableVariable global String errorMessage;
        @InvocableVariable global Funding_Snapshot__c FundingSnapshot;
    }

//  **************************************************************

    @InvocableMethod(label='Generate Funding Snapshots' 
                        description='Generates and returns Funding Snapshots for Accounts')
    public static list<GenerateFundingSnapshotsOutput> generateFundingSnapshots (list<GenerateFundingSnapshotsInput> inputs){
        system.debug('========'+inputs);
        fieldNamesSet = getFieldNamesMap ('Funding_Snapshot_Line_Item__c', true, true).keySet();

        list<GenerateFundingSnapshotsOutput> outputs = new list<GenerateFundingSnapshotsOutput>{};

        for (GenerateFundingSnapshotsInput i : inputs)
        {
            GenerateFundingSnapshotsOutput o = new GenerateFundingSnapshotsOutput ();

            Funding_Snapshot__c fs = generateFundingSnapshot (i);

            if (fs == null)
            {
                o.errorMessage = 'failed to create Funding Snapshot';
                o.success = false;
            }
            else
            {
                o.success = true;
                o.FundingSnapshot = fs;
            }
            outputs.add (o);
        }

        return outputs;
    }

//  **************************************************************
 
    public static Funding_Snapshot__c generateFundingSnapshot (GenerateFundingSnapshotsInput i)
    {
        System.debug('### i'+i);
        Account account;

        try
        {
            account = [SELECT Id, Name, Type, Facility_Membership_Fee__c,
                        (SELECT Id, Start_Date__c, End_Date__c, Amount__c 
                            FROM Installer_Incentives__r)
                        FROM Account WHERE Id = :i.accountId LIMIT 1]; 
        }
        catch (Exception e)
        {
            return null;
        }

        Integer maxNumberOfUFs = (i.maxNumberOfUFs == null ? 500 : i.maxNumberOfUFs);
        Date startDate = (i.startDate == null ? Date.newInstance(1970,1,1) : i.startDate);
        Date endDate = (i.endDate == null ? Date.newInstance(2099,1,1) : i.endDate);
        Boolean prioritizedOnly = (i.prioritizedOnly == true);
        Boolean homeImprovement = (i.homeImprovement == true);
        
        system.debug('Home Improvement..'+homeImprovement);
        
        Funding_Snapshot__c snapshot = new Funding_Snapshot__c (Account__c = i.accountId, 
                                                                Max_Number_Of_UFs__c = maxNumberOfUFs,
                                                                Start_Date__c = startDate,
                                                                End_Date__c = endDate,
                                                                Prioritized_Only__c = prioritizedOnly,
                                                                isHome__c = homeImprovement); 

        try
        {
            insert snapshot;
        }
        catch (Exception e)
        {
            return null;
        }

        list<Funding_Snapshot_Line_Item__c>  lineItems = buildLineItems (account, snapshot);

        insert lineItems;

        return snapshot;
    } 

//  **************************************************************

    private static list<Funding_Snapshot_Line_Item__c> buildLineItems (Account account, Funding_Snapshot__c snapshot)
    {
        list<Funding_Snapshot_Line_Item__c> lineItems = new list<Funding_Snapshot_Line_Item__c>{};

        Id accountId = account.Id;
        String query = '';
        set<Underwriting_File__c> ufs = new set<Underwriting_File__c>{};
        String isHome = 'Home';
        String startDate = String.valueOf(snapshot.Start_Date__c).left(10);
        String endDate = String.valueOf(snapshot.End_Date__c).left(10);
        String startDateTime = String.valueOf(snapshot.Start_Date__c).left(10) +'T00:00:01Z';
        String endDateTime = String.valueOf(snapshot.End_Date__c).left(10)+ 'T23:59:59Z';
        
        if(!snapshot.isHome__c)
        {
            String dateFilters='';
            
            if(account.Type == 'Facility')
            {
                 dateFilters = 'AND ((M1_Approval_Date__c >= ' + startDateTime + ' AND M1_Approval_Date__c <= ' + endDateTime + ') '+
                               ' OR (ST_Reapproved__c >= ' + startDateTime + ' AND ST_Reapproved__c <= ' + endDateTime + ') ' + 
                               ' OR (LT_Reapproved__c >= ' + startDateTime+ ' AND LT_Reapproved__c <= ' + endDateTime + ') )' ;
            }
            else if(account.Type == 'Partner')
            {
                 dateFilters = 'AND ((M0_Approval_Date__c >= ' + startDateTime + ' AND M0_Approval_Date__c <= ' + endDateTime + ') '+
                               ' OR (M1_Approval_Date__c >= ' + startDateTime + ' AND M1_Approval_Date__c <= ' + endDateTime + ') '+
                               ' OR (M2_Approval_Date__c >= ' + startDateTime + ' AND M2_Approval_Date__c <= ' + endDateTime + ') '+
                               ' OR (Installer_M0_Net_Out_Due_Date__c >= ' + startDate+ ' AND Installer_M0_Net_Out_Due_Date__c <= ' + endDate+ '))' ;
            }
            else
            {
                //Do Nothing
            }
        
                             
        //Srikanth - 05/29- Orange-219 - Added System Design sync fields                      

        query = 'SELECT ' + getCustomFieldNamesCsv ('Funding_Snapshot_Line_Item__c') + 
                                            ', (Select id,Name from Funding_Data__r) '+
                                            ', Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c, ' +
                                            ' Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c, ' +
                                            ' Installer_M0_Net_Out_Due_Date__c, ' +
                                            ' Opportunity__r.Installer_Account__c, ' +
                                            ' Opportunity__r.EDW_Originated__c, ' +
                                            ' Opportunity__r.OpportunityID18Char__c, ' +
                                            ' Opportunity__r.Inverter__c, ' +
                                            ' Opportunity__r.Battery__c, ' +
                                            ' Opportunity__r.Monthly_Payment_Escalated_Single_Loan__c, '+
                                            ' Opportunity__r.Final_Payment_Single_Loan__c, '+
                                            ' Opportunity__r.Module__c, ' +
                                            ' Opportunity__r.System_Size_kW__c, ' +
                                            ' Opportunity__r.Credit_exception_if_applicable__c, ' +
                                            ' Opportunity__r.Installer_Account__r.Name, ' +
                                            ' Opportunity__c, Opportunity__r.Sighten_UUID__c, ' +
                                            ' Opportunity__r.Account.SSN__c, ' +
                                            ' Opportunity__r.Co_Applicant__r.SSN__c, ' +
                                            ' (SELECT Id, Processed__c, Funding_Snapshot__r.Account__c FROM Funding_Snapshot_Line_Items__r), ' +
                                            '(Select id,Name from Box_Fields__r), '+
                                            //'(select id,Amount__c,Request_Date__c,Status__c,Approval_Date__c,Level__c from Draw_Requests__r where Approval_Date__c != null), '+
                                            '(select id,Amount__c,Request_Date__c,Status__c,SLF_Approved__c,Level__c from Draw_Requests__r where SLF_Approved__c != null), '+
                                            ' (SELECT Id, Type__c, Amount__c FROM Underwriting_Transactions__r) ' +
                                            ' FROM Underwriting_File__c' + 
                                            ' WHERE ' + 
                                            ' (Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c = :accountId OR ' + 
                                            ' Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c = :accountId OR ' + 
                                            ' Opportunity__r.Installer_Account__c = :accountId) ' + dateFilters;

       // set<Underwriting_File__c> ufs = new set<Underwriting_File__c>{};
        
        for (Underwriting_File__c uf : Database.query (query))
        {
            //  decide which UFs to consider for processing

            if (account.Type == 'Facility')
            {
                if ((snapshot.Prioritized_Only__c && uf.Funding_Prioritized__c) || !snapshot.Prioritized_Only__c)
                { 
                    ufs.add (uf);
                }                
            }

            if (account.Type == 'Partner')
            {
                 ufs.add (uf);
            }
        }
            if (account.Type == 'Facility') lineItems.addAll (processForFacility (account, snapshot, ufs));
            if (account.Type == 'Partner') lineItems.addAll (processForPartner (account, snapshot, ufs));
        }
        else{
        
            system.debug('Home Improvement Records...');
            
        
            query = 'SELECT ' + getCustomFieldNamesCsv ('Funding_Snapshot_Line_Item__c') + 
                                            ', (Select id,Name from Funding_Data__r), '+
                                            ' Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c, ' +
                                            ' Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c, ' +
                                            ' Installer_M0_Net_Out_Due_Date__c, ' +
                                            ' Opportunity__r.Installer_Account__c, ' +
                                            ' Opportunity__r.EDW_Originated__c, ' +
                                            ' Opportunity__r.OpportunityID18Char__c, ' +
                                            ' Opportunity__r.Inverter__c, ' +
                                            ' Opportunity__r.Battery__c, ' +
                                            ' Opportunity__r.Monthly_Payment_Escalated_Single_Loan__c, '+
                                            ' Opportunity__r.Final_Payment_Single_Loan__c, '+
                                            ' Opportunity__r.Module__c, ' +
                                            ' Opportunity__r.System_Size_kW__c, ' +
                                            ' Opportunity__r.Credit_exception_if_applicable__c, ' +
                                            ' Opportunity__r.Installer_Account__r.Name, ' +
                                            ' Opportunity__c, Opportunity__r.Sighten_UUID__c, ' +
                                            ' Opportunity__r.Account.SSN__c, ' +
                                            ' Opportunity__r.Co_Applicant__r.SSN__c, ' +
                                            ' (SELECT Id, Processed__c, Funding_Snapshot__r.Account__c FROM Funding_Snapshot_Line_Items__r), ' +
                                            '(Select id,Name from Box_Fields__r), '+
                                            //'(select id,Amount__c,Request_Date__c,Status__c,Approval_Date__c,Level__c from Draw_Requests__r where Approval_Date__c != null), '+
                                            '(select id,Amount__c,Request_Date__c,Status__c,SLF_Approved__c,Level__c from Draw_Requests__r where SLF_Approved__c != null), '+
                                            ' (SELECT Id, Type__c, Amount__c FROM Underwriting_Transactions__r) ' +
                                            ' FROM Underwriting_File__c' + 
                                            ' WHERE ' + 
                                            ' (Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c = :accountId OR ' + 
                                            ' Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c = :accountId OR ' + 
                                            ' Opportunity__r.Installer_Account__c = :accountId) AND Opportunity__r.Project_Category__c =: isHome AND ' +
                                            ' id IN (SELECT Underwriting__c FROM Draw_Requests__c WHERE SLF_Approved__c>= '+startDateTime+' and SLF_Approved__c<='+endDateTime+')';
            for (Underwriting_File__c uf : Database.query (query))
            {
                //  decide which UFs to consider for processing

                if (account.Type == 'Facility')
                {
                    if ((snapshot.Prioritized_Only__c && uf.Funding_Prioritized__c) || !snapshot.Prioritized_Only__c)
                    {    
                        ufs.add (uf);
                    }                
                }

            if (account.Type == 'Partner')
            {
                 ufs.add (uf);
            }
        }
            if (account.Type == 'Facility') lineItems.addAll (processForFacility (account, snapshot, ufs));
            if (account.Type == 'Partner') lineItems.addAll (processForPartner (account, snapshot, ufs));
        
        
        }
        return lineItems;
    }

//  **************************************************************

    private static list<Funding_Snapshot_Line_Item__c> processForFacility (Account account, 
                                                                            Funding_Snapshot__c snapshot,
                                                                            set<Underwriting_File__c> ufs)
    {
        list<Funding_Snapshot_Line_Item__c> lineItems = new list<Funding_Snapshot_Line_Item__c>{};
        Integer cnt = 0;

        Decimal maxNumberOfUFs = snapshot.Max_Number_Of_UFs__c;

        for (Underwriting_File__c uf : ufs)
        {
            if (cnt > maxNumberOfUFs) break;

            if (processedForFacility (account, uf) == false)
            {
                cnt++;

                Funding_Snapshot_Line_Item__c lineitem = new Funding_Snapshot_Line_Item__c (Funding_Snapshot__c = snapshot.Id,
                                                                                                Underwriting_File__c = uf.Id);

                //  copy values from UF as required for snapshot

                buildLineItemForFacility (account, snapshot, lineItem, uf); 
            
                lineItems.add (lineItem);       
            }           
        }
        return lineItems;
    }

//  **************************************************************

    private static list<Funding_Snapshot_Line_Item__c> processForPartner (Account account,
                                                                            Funding_Snapshot__c snapshot, 
                                                                            set<Underwriting_File__c> ufs)
    {
        list<Funding_Snapshot_Line_Item__c> lineItems = new list<Funding_Snapshot_Line_Item__c>{};
        Integer cnt = 0;

        Decimal maxNumberOfUFs = snapshot.Max_Number_Of_UFs__c;

        for (Underwriting_File__c uf : ufs)
        {
            if (cnt > maxNumberOfUFs) break;

            if (processedForPartner (account, uf) == false)
            {
                cnt++;

                Funding_Snapshot_Line_Item__c lineitem = new Funding_Snapshot_Line_Item__c (Funding_Snapshot__c = snapshot.Id,
                                                                                                Underwriting_File__c = uf.Id);

                //  copy values from UF as required for snapshot

                buildLineItemForPartner (account, snapshot, lineItem, uf);  
            
                lineItems.add (lineItem);       
            }           
        }
        return lineItems;
    }

//  **************************************************************

    private static Boolean processedForFacility (Account account, 
                                                    Underwriting_File__c uf)

    //  returns true if:
    
    //      this UF has already been processed for this Facility Account,
    //      if Facility is handling ST for this UF and Facility ST Funding Balance != 0
    //      if Facility is handling LT for this UF and Facility LT Funding Balance != 0

    {
        if (account.Type == 'Installer') return true;

        Boolean facilityHandlesShortTerm = (account.Id == uf.Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c);
        Boolean facilityHandlesLongTerm = (account.Id == uf.Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c);

        if (facilityHandlesShortTerm && Math.ABS(uf.Facility_ST_Funding_Balance__c) > 0.01) return false;

        if (facilityHandlesLongTerm && Math.ABS(uf.Facility_LT_Funding_Balance__c) > 0.01) return false;

        for (Funding_Snapshot_Line_Item__c fsli : uf.Funding_Snapshot_Line_Items__r)
        {
            if (fsli.Funding_Snapshot__r.Account__c == account.Id && fsli.Processed__c != null) 
            {
                return true;
            }
            else
            {
                System.debug ('###### not processed for Account ' + fsli);
            }
        }

        return false;
    }

//  **************************************************************

    private static Boolean processedForPartner (Account account, 
                                                    Underwriting_File__c uf)

    //  returns false if any of Installer M0 Payment Due, Installer M1 Payment Due and Installer M2 Payment Due all not zero
    //  returns false is Installer M0 Net Out Due is set and Installer_M0_Net_Out_Balance__c is not zero
    //  otherwise returns true
    
    {
        if (uf.Installer_M0_Net_Out_Due__c == true && Math.ABS(uf.Installer_M0_Net_Out_Balance__c) > 0.01) return false;
        if (Math.ABS(uf.Installer_M0_Payment_Due__c) > 0.01) return false;
        if (Math.ABS(uf.Installer_M1_Payment_Due__c) > 0.01) return false;
        if (Math.ABS(uf.Installer_M2_Payment_Due__c) > 0.01) return false;
        
        return true;
    }

//  **************************************************************

//  builds a line item specifically for a Partner Account

    private static void buildLineItemForPartner (Account account, 
                                                    Funding_Snapshot__c snapshot, 
                                                    Funding_Snapshot_Line_Item__c lineItem,
                                                    Underwriting_File__c uf)
    {
        copyFields (account, snapshot, lineItem, uf);

        lineItem.SSN__c = uf.Opportunity__r.Account.SSN__c;
        lineItem.CoBorrowerSSN__c = uf.Opportunity__r.Co_Applicant__r.SSN__c;

        Decimal totalIncentive = 0;

        for (Installer_Incentive__c ii : account.Installer_Incentives__r)
        {
            if (uf.Credit_Decision_Date__c != null &&
                    uf.M1_Approval_Date__c != null &&
                    uf.Credit_Decision_Date__c >= ii.Start_Date__c && 
                    uf.Credit_Decision_Date__c <= ii.End_Date__c)
            {
                totalIncentive += ii.Amount__c;
            }
        }

        if (totalIncentive > 0) 
        {
            lineItem.Net_Incentive_Required__c = totalIncentive - uf.Net_Incentive_Scheduled__c;
        }
    }

//  **************************************************************

//  builds a line item specifically for a Facility Account

    private static void buildLineItemForFacility (Account account, 
                                                    Funding_Snapshot__c snapshot, 
                                                    Funding_Snapshot_Line_Item__c lineItem,
                                                    Underwriting_File__c uf)
    {
        copyFields (account, snapshot, lineItem, uf);

        lineItem.SSN__c = uf.Opportunity__r.Account.SSN__c;
        lineItem.CoBorrowerSSN__c = uf.Opportunity__r.Co_Applicant__r.SSN__c;

        if (account.Id == uf.Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c)
        {
            lineItem.Facility_Membership_Fee__c = account.Facility_Membership_Fee__c;
            lineItem.Short_Term_Membership_Fee__c = account.Facility_Membership_Fee__c;
        }

        if (account.Id == uf.Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c)
        {
            lineItem.Facility_Membership_Fee__c = account.Facility_Membership_Fee__c;
            lineItem.Long_Term_Membership_Fee__c = account.Facility_Membership_Fee__c;
        }

        lineItem.ST_Reapproval__c = false;
        lineItem.LT_Reapproval__c = false;

        //  if the UF has been reapproved, the snapshot line item is as a result of a reapproval

        if (uf.ST_Reapproved__c != null) lineItem.ST_Reapproval__c = true;
        if (uf.LT_Reapproved__c != null) lineItem.LT_Reapproval__c = true;
    }

//  **************************************************************

//  this method attempts to copy all custom fields from the current Underwriting File to the new Funding Snapshot Line Item,
//  relying on matching field API names.
//  it ignores any errors, which can be as a result of there not being matching names, or the target being a formula.
//  if the current Facility is not handling for Short Term for an UF, all Short Term fields get a value of $0.00
//  if the current Facility is not handling for Long Term for an UF, all Long Term fields get a value of $0.00

    private static void copyFields (Account account, 
                                        Funding_Snapshot__c snapshot, 
                                        Funding_Snapshot_Line_Item__c lineItem,
                                        Underwriting_File__c uf)
    {
        set<String> stDependentFields = new set<String>
                                                {
                                                    'Short_Term_OID__c',
                                                    'ST_Amount_Financed__c',
                                                    'ST_Loan_Net__c',
                                                    'Facility_ST_Funding_Balance__c',
                                                    'ST_Facility_Fee__c',
                                                    'ST_Loan_Monthly_Payment__c',
                                                    'Facility_ST_Amount_Funded__c',
                                                    'Effective_ST_OID__c',
                                                    'ST_Loan_Term__c'
                                                };

        set<String> ltDependentFields = new set<String>
                                                {
                                                    'Long_Term_OID__c',
                                                    'LT_Amount_Financed__c',
                                                    'LT_Loan_Net__c',
                                                    'Facility_LT_Funding_Balance__c',
                                                    'LT_Facility_Fee__c',
                                                    'LT_Loan_Monthly_Payment__c',
                                                    'Facility_LT_Amount_Funded__c',
                                                    'Effective_LT_OID__c',
                                                    'LT_Loan_Term__c',
                                                    'APR__c'
                                                };

        Boolean stFacility = (uf.Opportunity__r.SLF_Product__r.Short_Term_Facility_Lookup__c == account.Id);
        Boolean ltFacility = (uf.Opportunity__r.SLF_Product__r.Long_Term_Facility_Lookup__c == account.Id);
        
        for (String s : fieldNamesSet)
        {
            try
            {
                if (account.Type == 'Facility' &&
                    ((stDependentFields.contains (s) && !stFacility) || (ltDependentFields.contains (s) && !ltFacility)))
                {
                    lineItem.put (s, 0);
                }
                else
                {
                    lineItem.put (s, uf.get (s));
                }
            }
            catch (Exception e) { }
        }

        lineItem.Opportunity_Id__c = uf.Opportunity__c;
        if(uf.Opportunity__r.EDW_Originated__c){
            lineItem.Sighten_UUID__c = uf.Opportunity__r.Sighten_UUID__c;
        }else{
            lineItem.Sighten_UUID__c = uf.Opportunity__r.OpportunityID18Char__c;
        }
        
        //Srikanth - 05/29 - Orange-529
        
        lineItem.Inverter__c = uf.Opportunity__r.Inverter__c;
        lineItem.Battery__c = uf.Opportunity__r.Battery__c;
        lineItem.Module__c = uf.Opportunity__r.Module__c;
        lineItem.System_Size_kW__c = uf.Opportunity__r.System_Size_kW__c;
        lineItem.Credit_exception_if_applicable__c = uf.Opportunity__r.Credit_exception_if_applicable__c;
        lineItem.Go_to_Payment__c = uf.Opportunity__r.Monthly_Payment_Escalated_Single_Loan__c;
        lineItem.Final_Payment_Amount__c = uf.Opportunity__r.Final_Payment_Single_Loan__c;
        
        if(!uf.Box_Fields__r.isEmpty()){
            lineItem.Box_Fields__c = uf.Box_Fields__r[0].id;
        }
        
        if(!uf.Funding_Data__r.isEmpty()){
            lineItem.Funding_Data__c = uf.Funding_Data__r[0].id;
        }
        
        // Added by Rajesh for Orange-1622
        if(!uf.Draw_Requests__r.isEmpty()){
            for(Draw_Requests__c draws :uf.Draw_Requests__r){
                if(draws.Level__c == '1'){
                    lineItem.Payment_1_Approval_Date__c = draws.SLF_Approved__c.date();
                    lineItem.Payment_1_Funding_Amount__c = draws.Amount__c;
                }else if(draws.Level__c == '2'){
                    lineItem.Payment_2_Approval_Date__c = draws.SLF_Approved__c.date();
                    lineItem.Payment_2_Funding_Amount__c = draws.Amount__c;
                }else if(draws.Level__c == '3'){
                    lineItem.Payment_3_Approval_Date__c = draws.SLF_Approved__c.date();
                    lineItem.Payment_3_Funding_Amount__c = draws.Amount__c;
                }
            }
        }
    }

    //********************************************************************************

    public static void processLineItemsForFacility (set<Id> snapshotIds)
    {
        list<Funding_Snapshot_Line_Item__c> lineItems = [SELECT Id, ST_Amount_Financed__c, LT_Amount_Financed__c, Underwriting_File__c,
                                                            Facility_ST_Funding_Balance__c,
                                                            Facility_LT_Funding_Balance__c,
                                                            ST_Loan_Net__c,
                                                            LT_Loan_Net__c,
                                                            Short_Term_Membership_Fee__c,
                                                            Long_Term_Membership_Fee__c,
                                                            Short_Term_Membership_Fee_Scheduled__c,
                                                            Long_Term_Membership_Fee_Scheduled__c
                                                            FROM Funding_Snapshot_Line_Item__c 
                                                            WHERE Funding_Snapshot__c IN :snapshotIds];

        list<Underwriting_Transaction__c> transactions = new list<Underwriting_Transaction__c>{};

        for (Funding_Snapshot_Line_Item__c li : lineItems)
        {
            if (li.ST_Loan_Net__c != 0)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Facility Short Term Funding';
                ut.Amount__c = li.Facility_ST_Funding_Balance__c;

                transactions.add (ut);
            }

            if (li.LT_Loan_Net__c != 0)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Facility Long Term Funding';
                ut.Amount__c = li.Facility_LT_Funding_Balance__c;

                transactions.add (ut);
            }

            if (li.Short_Term_Membership_Fee__c != null && 
                    li.Short_Term_Membership_Fee__c != 0 
                    && li.Short_Term_Membership_Fee_Scheduled__c == 0)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Short Term Membership Fee Scheduled';
                ut.Amount__c = -li.Short_Term_Membership_Fee__c;

                transactions.add (ut);

            }
            else if (li.Long_Term_Membership_Fee__c != null && 
                    li.Long_Term_Membership_Fee__c != 0 && 
                    li.Long_Term_Membership_Fee_Scheduled__c == 0) //changed to else if to not create duplicates - pdm
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Long Term Membership Fee Scheduled';
                ut.Amount__c = -li.Long_Term_Membership_Fee__c;

                transactions.add (ut);
            }
        }
        insert transactions;
    }

    //********************************************************************************

    public static void processLineItemsForPartner (set<Id> snapshotIds)
    {
        list<Funding_Snapshot_Line_Item__c> lineItems = [SELECT Id, ST_Amount_Financed__c, LT_Amount_Financed__c, Underwriting_File__c,
                                                            Facility_ST_Funding_Balance__c,
                                                            Facility_LT_Funding_Balance__c,
                                                            ST_Loan_Net__c,
                                                            LT_Loan_Net__c,
                                                            Installer_M0_Payment_Due__c,
                                                            Installer_M1_Payment_Due__c,
                                                            Installer_M2_Payment_Due__c,
                                                            Net_Incentive_Required__c,
                                                            Installer_M0_Net_Out_Due__c,
                                                            Installer_M0_Net_Out_Balance__c,
                                                            Installer_M0_Net_Out_Scheduled__c,
                                                            M0_Payback_Due__c
                                                            FROM Funding_Snapshot_Line_Item__c 
                                                            WHERE Funding_Snapshot__c IN :snapshotIds];

        list<Underwriting_Transaction__c> transactions = new list<Underwriting_Transaction__c>{};

        for (Funding_Snapshot_Line_Item__c li : lineItems)
        {
            if (li.Installer_M0_Payment_Due__c != 0)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Installer M0 Payment Scheduled';
                ut.Amount__c = li.Installer_M0_Payment_Due__c;

                transactions.add (ut);
            }

            if (li.Installer_M1_Payment_Due__c != 0)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Installer M1 Payment Scheduled';
                ut.Amount__c = li.Installer_M1_Payment_Due__c;

                transactions.add (ut);

                //  we paid M1, and now if we did a net out for M0, now we repay that as an M0 Payback

                if (li.M0_Payback_Due__c != 0)
                {
                    Underwriting_Transaction__c ut2 = new Underwriting_Transaction__c ();
                    ut2.Underwriting_File__c = li.Underwriting_File__c;
                    ut2.Funding_Snapshot_Line_Item__c = li.Id;
                    ut2.Type__c = 'Installer M0 Payback Scheduled';
                    ut2.Amount__c = li.M0_Payback_Due__c;

                    transactions.add (ut2);
                }
            }

            if (li.Installer_M2_Payment_Due__c != 0)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Installer M2 Payment Scheduled';
                ut.Amount__c = li.Installer_M2_Payment_Due__c;

                transactions.add (ut);
            }

            if (li.Net_Incentive_Required__c > 0)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Incentive Payment Scheduled';
                ut.Amount__c = li.Net_Incentive_Required__c;

                transactions.add (ut);
            }

            if (li.Installer_M0_Net_Out_Due__c == true && Math.ABS(li.Installer_M0_Net_Out_Balance__c) > 0.01)
            {
                Underwriting_Transaction__c ut = new Underwriting_Transaction__c ();
                ut.Underwriting_File__c = li.Underwriting_File__c;
                ut.Funding_Snapshot_Line_Item__c = li.Id;
                ut.Type__c = 'Installer M0 Net Out Scheduled';
                ut.Amount__c = li.Installer_M0_Net_Out_Balance__c;

                transactions.add (ut);
            }
        }
        insert transactions;
    }

//********************************************************************************

    public static void deleteLineItemsForSnapshot (set<Id> snapshotIds) 
    {
        delete [SELECT Id FROM Funding_Snapshot_Line_Item__c WHERE Funding_Snapshot__c IN :snapshotIds];
    }

//********************************************************************************

    public static void deleteTransactionsForSnapshot (set<Id> snapshotIds) 
    {
        set<Id> lineItemIds = new set<Id>{};

        for (Funding_Snapshot_Line_Item__c li : [SELECT Id FROM Funding_Snapshot_Line_Item__c WHERE Funding_Snapshot__c IN :snapshotIds])
        {
            lineItemIds.add (li.Id);
        }

        delete [SELECT Id FROM Underwriting_Transaction__c WHERE Funding_Snapshot_Line_Item__c IN :lineItemIds];
    }

//********************************************************************************

    private static String getCustomFieldNamesCsv (String objectName) 
    {
        list<String> fieldNames = getFieldNamesMap(objectName, true, true).values();

        set<String> excludeFields = new set<String>
            {
                'Funding_Snapshot__c',
                'LT_Loan_Agreement_Received__c', //Srikanth - 02/04/2019 - Added this field to exclude from population as Box.com fields moved out of UW
                'Underwriting_File__c',
                'Opportunity_Id__c',
                'Sighten_UUID__c',
                'Install_City__c',
                'Processed__c',
                'Net_Incentive_Required__c',
                'ST_Reapproval__c',
                'LT_Reapproval__c',
                'Net_Incentive_Due__c',
                'Short_Term_Membership_Fee__c',
                'Long_Term_Membership_Fee__c',
                'SSN__c',
                'CoBorrowerSSN__c',
                'Battery__c',
                'Go_to_Payment__c',
                'Final_Payment_Amount__c',
                'Inverter__c',
                'Module__c',
                'Credit_exception_if_applicable__c',
                'System_Size_kW__c',
                'Payment_1_Approval_Date__c',
                'Box_Fields__c',
                'Funding_Data__c',
                'Payment_1_Funding_Amount__c',
                'Payment_2_Approval_Date__c',
                'Payment_2_Funding_Amount__c',
                'Payment_3_Approval_Date__c',
                'Payment_3_Funding_Amount__c',
                'M2_Review_Complete_Date__c'
            };

        String fields = '';

        if (fieldNames.size() == 0) return fields;
        
        for (String s : fieldNames)
        {
            if (!excludeFields.contains(s)) fields += ', ' + s;
        }
        
        return fields.substring(2);
    }

//********************************************************************************
    
    private static map<String,String> getFieldNamesMap (String objectName, Boolean excludeFormulas, Boolean onlyCustom)
    {
        map<String,String> fieldNamesMap = new map<String,String>{};

        SObjectType sot = Schema.getGlobalDescribe().get(objectName); 
                    
        String fields = '';

        if (sot <> null)
        {
        
            for (Schema.SObjectField sof : sot.getDescribe().fields.getMap().values())
            {
                if (sof.getDescribe().isCalculated() && excludeFormulas) continue;

                String fieldName = sof.getDescribe().getName();

                if (!fieldName.endswith('__c') && onlyCustom) continue;

                fieldNamesMap.put(fieldName, fieldName);
            }
        }
                
        return fieldNamesMap;
    }

//********************************************************************************

}