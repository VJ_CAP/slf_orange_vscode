/**
* Description: Opportunity Id Search related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class QuickQuoteServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + QuickQuoteServiceImpl.class);
        IRestResponse iRestRes;
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean reqData ;
        try{
        if(string.isNotBlank(rw.reqDataStr))
        {
            reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        }else{
            reqData = new UnifiedBean();
        }
        
        UnifiedBean respData = new UnifiedBean();
        respData.products = new List<UnifiedBean.ProductWrapper>();
             List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
             
            system.debug('### reqData '+reqData);
            List<UnifiedBean.ProductWrapper> prodLst ;
            if(null != reqData.products)
            {
                prodLst = reqData.products;
            }else{
                prodLst = new List<UnifiedBean.ProductWrapper>();
            }
            UnifiedBean.ProductWrapper objProductWrapper ;
            if(!prodLst.isEmpty())
            {
                objProductWrapper = prodLst.get(0); 
            }else{
                objProductWrapper = new UnifiedBean.ProductWrapper();
            }
            
            //Added by Suresh as per Orange-1226 (Quickquote Calculator)
            
            if(null != objProductWrapper.alternatePaydown && objProductWrapper.productType != 'Solar Interest Only'){
                system.debug('### Entered alternatePaydown logic: '+objProductWrapper.alternatePaydown);
                Decimal loanAmount = Decimal.valueOf(objProductWrapper.loanAmount);
                Integer term = Integer.valueOf(objProductWrapper.term) ;
                Decimal alternateMonthlyPayment = LoanCalculator.PMT(loanAmount, term, objProductWrapper.apr);
                UnifiedBean.ProductWrapper prodRec = new UnifiedBean.ProductWrapper();
                prodRec.alternateMonthlyPayment = alternateMonthlyPayment.setScale(4);
                respData.products.add(prodRec);
                respData.returnCode = '200';
                
                system.debug('### respData: '+respData);
                
                res.response = (IRestResponse)respData;
                return res.response;
            }
            
            //End by Suresh as per Orange-1226 (Quickquote Calculator)
            
            string conditions = '';
            List<String> lstConditions = new List<String>();
            if (null != objProductWrapper.term) {
                lstConditions.add('Term_mo__c=' + objProductWrapper.term);
            }
            if (null != loggedInUsr[0].contact.AccountId) {
                lstConditions.add('Installer_Account__c =\'' + loggedInUsr[0].contact.AccountId+ '\'');
            }
            //Commented by Deepika as part of Orange - 3622
            /*if (null != objProductWrapper.isACH) {
                lstConditions.add('ACH__c=' + objProductWrapper.isACH);
            }*/
             
            if (null !=objProductWrapper.stateName && String.isNotBlank(objProductWrapper.stateName)) {
                lstConditions.add('State_Code__c=\'' + SLFUtility.getStateCode(objProductWrapper.stateName) + '\'');
            }
            
            if(objProductWrapper.projectCategory == 'Home'){ // Added for 1735
                //Modified by Deepika as part of Orange - 3622 - Start
                if(null != objProductWrapper.isACH && objProductWrapper.isACH) {
                    lstConditions.add('APR__c=' + objProductWrapper.apr);
                    if(objProductWrapper.productType == 'HIS') 
                        lstConditions.add('Promo_APR__c =' + objProductWrapper.promoAPR);
                }else if(null != objProductWrapper.isACH && !objProductWrapper.isACH){ //Added by Ravi as part of ORANGE-3622
                    lstConditions.add('NonACH_APR__c =' + objProductWrapper.apr);
                    if(objProductWrapper.productType == 'HIS')
                        lstConditions.add('NonACH_Promo_APR__c =' + objProductWrapper.promoAPR);
                }
                //Modified by Deepika as part of Orange - 3622 - end
                lstConditions.add('Promo_Term__c=' + objProductWrapper.promoTerm);
                lstConditions.add('Product_Loan_Type__c=\'' + objProductWrapper.productType + '\'');
                
                lstConditions.add('Promo_Fixed_Payment__c=' + objProductWrapper.promoFixedPayment);
                lstConditions.add('Promo_Percentage_Balance__c=' + objProductWrapper.promoBalance);
                lstConditions.add('Promo_Percentage_Payment__c=' + objProductWrapper.promoPercentPayment);
                lstConditions.add('Draw_Period__c=' + objProductWrapper.drawPeriod);
            }
            //Updated bu Adithya as part of ORANGE-2706 on 10/1/2019 Solar for Interest Only
            //Start
            if(objProductWrapper.projectCategory == 'Solar'){ 
                if(objProductWrapper.productType == 'Solar Interest Only')
                {
                    if(null != objProductWrapper.isACH && !objProductWrapper.isACH)
                    {
                        lstConditions.add('NonACH_APR__c =' + objProductWrapper.apr);
                    }
                    if(null != objProductWrapper.isACH && objProductWrapper.isACH) {
                        lstConditions.add('APR__c=' + objProductWrapper.apr);
                    }
                    lstConditions.add('Product_Loan_Type__c=\'' + objProductWrapper.productType + '\'');    
                }
            }
            //End
            //Default filter condition
            lstConditions.add('Internal_Use_Only__c=false');
            lstConditions.add('Is_Active__c=true');
            lstConditions.add('Loan_Type__c=\'Single\'');
            system.debug('###Conditions'+lstConditions);
            if(lstConditions!=null && lstConditions.size()>0){
                for (integer i = 0; i < lstConditions.size(); i++) {
                    if (i == lstConditions.size() - 1) {
                        conditions = conditions + lstConditions.get(i);
                    } else {
                        conditions = conditions + lstConditions.get(i) + ' and ';
                    }
                }
            }
            
            String queryStr = '';

            if(string.isNotBlank(conditions))
            { 
                queryStr = 'select Id,Name,Draw_Period__c,NonACH_APR__c,Promo_Fixed_Payment__c,Promo_Percentage_Balance__c,Promo_Percentage_Payment__c,Term_mo__c,APR__c,State_Code__c,Promo_Term__c,Promo_APR__c,NonACH_Promo_APR__c from Product__c where '+conditions;
            }
                                
            system.debug('### queryStr on Product: '+queryStr);
            try{
                List<Product__c>  prodResults  = Database.query(queryStr);
                
                if(!prodResults.isEmpty())
                {
                    //Added By Adithya as part of 1735
                    //Updated If condition By Adithya as part of ORANGE-2706 on 10/1/2019 for Solar Interest Only deals
                    if((objProductWrapper.projectCategory == 'Home' || (objProductWrapper.projectCategory == 'Solar' && objProductWrapper.productType == 'Solar Interest Only')) && null != objProductWrapper.loanAmount)
                    {
                         //pUpdated by Adithya as part of Orange-6921
                         if(objProductWrapper.productType == 'Solar Interest Only' ){ 
                            //Added by Sejal as part of Orange-6921 - Start
                            if(objProductWrapper.paydownPercentage == null)    {
                                DateTime itcCutOffDate = SFSettings__c.getOrgDefaults().ITC_CutOff_Date__c;
                                 if(Date.Today()>=itcCutOffDate)
                                    objProductWrapper.paydownPercentage = 26;
                                else
                                    objProductWrapper.paydownPercentage = 30;
                            }
                            //Added by Sejal as part of Orange-6921 - End 
                        }
                        UnifiedBean.ProductWrapper prodRec = new UnifiedBean.ProductWrapper();
                        
                        List<Payment_Schedule__c> newPaymentSchedules = QuotePaymentSchedulesGenerator.createPaymentSchedules(null, objProductWrapper.loanAmount,prodResults[0],objProductWrapper.productType,objProductWrapper.isACH,objProductWrapper.paydownPercentage);
                        
                        //Updated By Adithya as part of ORANGE-2706 on 10/1/2019 for Solar Interest Only deals
                        //Start
                        if(objProductWrapper.projectCategory == 'Home') 
                        {
                            prodRec.finalMonthlyPayment  = newPaymentSchedules[0].Amount__c;
                            prodRec.finalEscalatedMonthlyPayment = newPaymentSchedules[newPaymentSchedules.size()-1].Amount__c;
                            prodRec.monthlyPayment = newPaymentSchedules[0].Amount__c;
                            prodRec.escalatedMonthlyPayment = newPaymentSchedules[newPaymentSchedules.size()-1].Amount__c;
                        }else if(objProductWrapper.productType == 'Solar Interest Only')
                        {
                            prodRec.monthlyPayment = newPaymentSchedules[1].Amount__c;
                            prodRec.escalatedMonthlyPayment = newPaymentSchedules[newPaymentSchedules.size()-1].Amount__c;
                            
                            if(objProductWrapper.isACH)
                            {
                                prodRec.monthlyPayment = newPaymentSchedules[1].Amount__c;
                                prodRec.escalatedMonthlyPayment = newPaymentSchedules[2].Amount__c;
                            }else{
                                prodRec.monthlyPayment = newPaymentSchedules[4].Amount__c;
                                prodRec.escalatedMonthlyPayment = newPaymentSchedules[5].Amount__c;
                            }
                            
                            List<Payment_Schedule_Interest_Only__c> newPaymentSchedulesInterestOnly = QuotePaymentSchedulesIOGenerator.createPaymentSchedules(null, objProductWrapper.loanAmount,prodResults[0],objProductWrapper.productType,objProductWrapper.isACH,objProductWrapper.paydownPercentage);
                            
                            if(null != newPaymentSchedulesInterestOnly && !newPaymentSchedulesInterestOnly.isEmpty() && objProductWrapper.productType == 'Solar Interest Only')
                            {
                                if(objProductWrapper.isACH)
                                {
                                    prodRec.monthlyPaymentIO = newPaymentSchedulesInterestOnly[1].Amount__c;
                                    prodRec.escalatedMonthlyPaymentIO = newPaymentSchedulesInterestOnly[2].Amount__c;
                                }else{
                                    prodRec.monthlyPaymentIO = newPaymentSchedulesInterestOnly[4].Amount__c;
                                    prodRec.escalatedMonthlyPaymentIO = newPaymentSchedulesInterestOnly[5].Amount__c;
                                }
                            }
                            if(null != objProductWrapper.alternatePaydown)
                            {
                                Decimal loanAmount = Decimal.valueOf(objProductWrapper.loanAmount);
                                Integer term = Integer.valueOf(objProductWrapper.term) ;
                                List<Payment_Schedule_Interest_Only__c> alternatePaydownPaymentSchedulesInterestOnly = QuotePaymentSchedulesIOGenerator.createPaymentSchedules(null, objProductWrapper.loanAmount,prodResults[0],objProductWrapper.productType,objProductWrapper.isACH,objProductWrapper.alternatePaydown);
                                if(objProductWrapper.isACH)
                                {
                                    prodRec.alternateMonthlyPayment = alternatePaydownPaymentSchedulesInterestOnly[2].Amount__c;
                                }else{
                                    prodRec.alternateMonthlyPayment = alternatePaydownPaymentSchedulesInterestOnly[alternatePaydownPaymentSchedulesInterestOnly.size()-1].Amount__c;
                                }
                            }
                        }
                        //End
                        
                        if(null != prodResults[0].Promo_Term__c)
                        {
                            prodRec.paymentCountPromo = prodResults[0].Promo_Term__c;
                            prodRec.paymentCountStandard = prodResults[0].Term_mo__c - prodResults[0].Promo_Term__c;
                        }else{
                            prodRec.paymentCountStandard = prodResults[0].Term_mo__c;
                        }
                        //Modified by Deepika as part of Orange-3622 -Start
                        if(objProductWrapper.isACH!=null && objProductWrapper.isACH && null != prodResults[0].Promo_APR__c){
                            prodRec.promoAPR = prodResults[0].Promo_APR__c;
                            prodRec.apr = prodResults[0].APR__c;
                        }
                        else if(objProductWrapper.isACH!=null && !objProductWrapper.isACH && null != prodResults[0].NonACH_Promo_APR__c){
                            prodRec.promoAPR = prodResults[0].NonACH_Promo_APR__c;
                            prodRec.apr = prodResults[0].NonACH_APR__c;
                        }
                        //Modified by Deepika as part of Orange-3622 -End
                        
                        respData.products.add(prodRec); 
                        respData.returnCode = '200';
                        
                    }else if(prodResults.size() > 1)
                    {
                        respData.returnCode = '215';
                        List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                        UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                        errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('215',null);
                        errorLst.add(errorWrap);
                        respData.error = errorLst;
                    }else{
                        for(Product__c prodIterate : prodResults){
                            UnifiedBean.ProductWrapper prodRec = new UnifiedBean.ProductWrapper();
                            if(objProductWrapper.projectCategory != 'Home' || (objProductWrapper.projectCategory == 'Solar' && objProductWrapper.productType == 'Solar Interest Only')){
                                prodRec.id = prodIterate.Id;
                                prodRec.name = prodIterate.Name; 
                                if(prodIterate.Term_mo__c != null)
                                    prodRec.term =  prodIterate.Term_mo__c;
                                //Modified by Deepika as part of Orange-3622 -Start
                                if(objProductWrapper.isACH!=null && objProductWrapper.isACH && prodIterate.APR__c != null)
                                    prodRec.apr =  prodIterate.APR__c;            
                                else if(objProductWrapper.isACH!=null && !objProductWrapper.isACH && prodIterate.NonACH_APR__c != null)
                                    prodRec.apr =  prodIterate.NonACH_APR__c; 
                                //prodRec.isACH =  prodIterate.ACH__c;
                                prodRec.isACH = objProductWrapper.isACH;
                                //Modified by Deepika as part of Orange-3622 - end
                                prodRec.stateName =  SLFUtility.getStateName(prodIterate.State_Code__c);
                            }
                            respData.products.add(prodRec); 
                            respData.returnCode = '200';
                        }
                    }
                }else{
                    respData.returnCode = '216';
                    List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                    UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                    errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('216',null);
                    errorLst.add(errorWrap);
                    respData.error = errorLst;
                }
                system.debug('###respData ' + respData);
            } catch(Exception ex){
                respData.returnCode = '400';
                List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                errorWrap.errorMessage = 'Error occurred.'+ex.getMessage();
                errorLst.add(errorWrap);
                respData.error = errorLst;
            }
            
            res.response = (IRestResponse)respData;

        } catch(Exception err){
           system.debug('### QuickQuoteServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' QuickQuoteServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            
            UnifiedBean unifBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214',null);
            errorWrapperLst.add(errorWrapper);
            unifBean.error  = errorWrapperLst ;
            unifBean.returnCode = '214';
            iRestRes = (IRestResponse)unifBean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + QuickQuoteServiceImpl.class);
        return res.response ;
    }
    
    
}