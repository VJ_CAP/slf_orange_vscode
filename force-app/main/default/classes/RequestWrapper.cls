/**
* Description: All requests part of each operation is wrapped into a single RequestWrapper object, 
*              which constitutes  QueryString parameters, application name, object, operation etc..
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma         05/30/2017             Created
******************************************************************************************/

global class RequestWrapper{
    //RequestActionParam class has all request parameters like application id, sobject, action, operations
    public RequestActionParam reqAction {get; set;}
    
    //RequestData class has all records which are POSTed as part of request body.
    public IRestRequest reqData {get; set;} 
    
    //RequestData class has all records which are POSTed as part of request body.
    public string reqDataStr{get; set;} 
    
       
    //Request paramters which comes in as part of Query string in GET calls
    public Map<String, String> reqParamMap {get; set;}
    
    /**
    * Description: Default constructor to initialize
    */
    public RequestWrapper(){
        this.reqAction = new RequestActionParam();
      
        this.reqParamMap = new Map<String, String>();
       
    }
}