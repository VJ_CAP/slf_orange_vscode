/**
* Description: Opportunity Id Search related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class EquipmentDetailServiceImpl extends RESTServiceBase{  
    private String msg = '';
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + EquipmentDetailServiceImpl.class);
        IRestResponse iRestRes;
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean reqData ;
        if(string.isNotBlank(rw.reqDataStr))
        {
            reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        }else{
            reqData = new UnifiedBean();
        }
        UnifiedBean respData = new UnifiedBean();
        respData.equipmentManufacturers = new List<UnifiedBean.EquipmentWrapper>();
        
        try{
            system.debug('### reqData '+reqData);
            
            List<UnifiedBean.EquipmentWrapper> equipmentLst;
            if(null != reqData.equipmentManufacturers)
            {
                equipmentLst = reqData.equipmentManufacturers;
            }else{
                equipmentLst = new List<UnifiedBean.EquipmentWrapper>();
            }
            
            UnifiedBean.EquipmentWrapper objEquipmentWrapper;

            if(!equipmentLst.isEmpty())
            {
                objEquipmentWrapper = equipmentLst.get(0);
            }else{
                objEquipmentWrapper = new UnifiedBean.EquipmentWrapper();
            }               
            
            
            string conditions = '';
            List<String> lstConditions = new List<String>();
                        
            if (String.isNotBlank(objEquipmentWrapper.id)) {
                lstConditions.add('id=\'' + objEquipmentWrapper.id + '\'');
            }
            if (String.isNotBlank(objEquipmentWrapper.manufacturer)) {
                lstConditions.add('Manufacturer__c=\'' + objEquipmentWrapper.manufacturer + '\'');
            }
            if (String.isNotBlank(objEquipmentWrapper.manufacturerType)) {
                lstConditions.add('Manufacturer_Type__c=\'' + objEquipmentWrapper.manufacturerType + '\'');
            }
            if (null != objEquipmentWrapper.isActive) {
                lstConditions.add('Is_Active__c=' + objEquipmentWrapper.isActive);
            }
            if (String.isNotBlank(objEquipmentWrapper.activationDate)) {
                lstConditions.add('Activation_Date__c=' + objEquipmentWrapper.activationDate);
            }
            if (String.isNotBlank(objEquipmentWrapper.endDate)) {
                lstConditions.add('End_Date__c=' + objEquipmentWrapper.endDate);
            }
            
            if(lstConditions!=null && lstConditions.size()>0){
                for (integer i = 0; i < lstConditions.size(); i++) {
                    if (i == lstConditions.size() - 1) {
                        conditions = conditions + lstConditions.get(i);
                    } else {
                        conditions = conditions + lstConditions.get(i) + ' and ';
                    }
                }
            }
            
            String queryStr = '';
            if(string.isNotBlank(conditions))
            {
                queryStr = 'select id,Name,Is_Active__c,Manufacturer_Type__c,End_Date__c,Manufacturer__c,Activation_Date__c from Equipment_Manufacturer__c where '+conditions;
            }else{
                queryStr = 'select id,Name,Is_Active__c,Manufacturer_Type__c,End_Date__c,Manufacturer__c,Activation_Date__c from Equipment_Manufacturer__c';
            }           
           try{
                List<Equipment_Manufacturer__c>  equipResults  = Database.query(queryStr);
                
                if(!equipResults.isEmpty())
                {
                    for(Equipment_Manufacturer__c equipIterate : equipResults)
                    {
                        UnifiedBean.EquipmentWrapper equipmentRec = new UnifiedBean.EquipmentWrapper();
                        equipmentRec.id = equipIterate.Id;
                        equipmentRec.isActive = equipIterate.Is_Active__c;
                        equipmentRec.manufacturer =  equipIterate.Manufacturer__c;
                        equipmentRec.manufacturerType =  equipIterate.Manufacturer_Type__c;
                        equipmentRec.activationDate =  String.ValueOf(equipIterate.Activation_Date__c);
                        equipmentRec.endDate =  String.ValueOf(equipIterate.End_Date__c);
                        
                        respData.equipmentManufacturers.add(equipmentRec);
                    }
                    respData.returnCode = '200';
                }
                else{
                    respData.returnCode = '386';
                    List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                    UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                    errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('386', null);
                    errorLst.add(errorWrap);
                    respData.error = errorLst;
                }
            }catch(Exception ex){
                respData.returnCode = '400';
                List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('400', null);
                errorLst.add(errorWrap);
                respData.error = errorLst;
            }
            
            res.response = (IRestResponse)respData;

        }catch(Exception err){
            system.debug('### EquipmentDetailServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('EquipmentDetailServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null)); 
            
            UnifiedBean unifBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            unifBean.error  = errorWrapperLst ;
            unifBean.returnCode = '214';
            iRestRes = (IRestResponse)unifBean;
            res.response= iRestRes;
            
        }
        system.debug('### Exit from fetchData() of ' + EquipmentDetailServiceImpl.class);
        return res.response ;
    }
    
    
}