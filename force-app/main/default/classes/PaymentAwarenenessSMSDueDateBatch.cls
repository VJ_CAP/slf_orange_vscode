/**
*   Description: This Batch class is used to send SMS to applicant an Co-Applicannt.
*
*   Modification Log :
---------------------------------------------------------------------------
   Developer                          Date                Description
---------------------------------------------------------------------------
   Adithya Sarma Kousika              12/27/2019          Created
******************************************************************************************/
global class PaymentAwarenenessSMSDueDateBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful,Schedulable{
    
    /**
    * Description:    Start method to query all the expired credit records
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('### Entered into start() of '+ PaymentAwarenenessSMSDueDateBatch.class);  
        
        String longTermFacility = 'CRB';
        return Database.getQueryLocator('SELECT id,Opportunity__r.Primary_Applicant_Email__c,Opportunity__r.AccountId,Opportunity__r.Co_Applicant__c,Opportunity__r.Account.PersonMobilePhone,Opportunity__r.Co_Applicant__r.PersonMobilePhone,Opportunity__r.Account.FirstName,Opportunity__r.Account.LastName,Opportunity__r.Co_Applicant__r.FirstName,Opportunity__r.Co_Applicant__r.LastName,M1_Loan_Booked_Date__c,Opportunity__r.SLF_Product__r.Product_Loan_Type__c,Opportunity__r.SLF_Product__r.Long_Term_Facility__c,Opportunity__r.ACH_APR_Process_Required__c,Opportunity__r.Long_Term_Facility__c,Opportunity__r.Product_Loan_Type__c FROM Underwriting_File__c where Opportunity__r.SLF_Product__r.Long_Term_Facility__c =  \''+String.escapeSingleQuotes(longTermFacility)+'\' ' +' AND Opportunity__r.Account.SMS_Opt_in__c = true AND M1_Loan_Booked_Date__c = LAST_N_DAYS:50');
    }
    
    /**
    * Description:    Batch execute method to update all expired credits status to "Expired"
    */
    global void execute(Database.BatchableContext bc, List<Underwriting_File__c> uwList){
        system.debug('### Entered into execute() of '+ PaymentAwarenenessSMSDueDateBatch.class);    
        System.debug('****Underwriting List'+uwList);
        try{
            SFSettings__c settings = SFSettings__c.getOrgDefaults();
            List<Twilio_Templates__c> twlTemplate = [select Id,Reply_Text__c,Response_Text__c from Twilio_Templates__c where Reply_Text__c=: 'Payment Awareness'];
            
            String stringtemp='';
            
            if(!uwList.isEmpty()){     
                if(twlTemplate.size()>0){
                    stringtemp = twlTemplate[0].Response_Text__c;
                    system.debug('### stringtemp before'+ stringtemp);
                    
                }
                for(Underwriting_File__c  uwIterate : uwList)
                {
                    if(null != uwIterate.Opportunity__r.AccountId)
                    {
                        if(stringtemp.contains('[customer name]')) 
                        {
                            system.debug('### FirstName '+ uwIterate.Opportunity__r.Account.FirstName);
                            system.debug('### LastName '+ uwIterate.Opportunity__r.Account.LastName);
                            stringtemp = stringtemp.replace('[customer name]',uwIterate.Opportunity__r.Account.FirstName+' '+uwIterate.Opportunity__r.Account.LastName);
                        }
                            system.debug('### stringtemp after'+ stringtemp);
                            
                        Map<String,String> applicantParams = new Map<String,String>();
                        applicantParams = new Map<String,String> {
                        'To'   => '+1'+uwIterate.Opportunity__r.Account.PersonMobilePhone,
                        'From' => settings.Twilio_From_Phone_Number__c,
                        'Body' => stringtemp
                        };
                        if(!applicantParams.isEmpty())
                        {
                            TwilioUtil.invokeTwilioWSCallOut(applicantParams);
                        } 
                    }
                    if(null != uwIterate.Opportunity__r.Co_Applicant__c)
                    {
                        if(stringtemp.contains('[Customer Name]')) 
                            stringtemp = stringtemp.replace('[Customer Name]',uwIterate.Opportunity__r.Co_Applicant__r.FirstName+' '+uwIterate.Opportunity__r.Co_Applicant__r.LastName);
                        
                        
                        
                        Map<String,String> coApplicantParams = new Map<String,String>();
                        coApplicantParams = new Map<String,String> {
                        'To'   => '+1'+uwIterate.Opportunity__r.Co_Applicant__r.PersonMobilePhone,
                        'From' => settings.Twilio_From_Phone_Number__c,
                        'Body' => stringtemp
                        };
                        if(!coApplicantParams.isEmpty())
                        {
                            TwilioUtil.invokeTwilioWSCallOut(coApplicantParams);
                        } 
                    }
                }
                
            }
        }catch(Exception err){
            ErrorLogUtility.writeLog('PaymentAwarenenessSMSDueDateBatch.execute()',err.getLineNumber(),'execute()',err.getMessage() + '***' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));       
        }
        system.debug('### Exit from execute() of '+ PaymentAwarenenessSMSDueDateBatch.class);    
    }
    global void Finish(Database.BatchableContext bc){
        
    }

    /**
    * Description: Schedule execute method to schedule the batch class with size 1.
    */  
    global void execute(SchedulableContext sc) {
        PaymentAwarenenessSMSDueDateBatch paymentBatch = new PaymentAwarenenessSMSDueDateBatch();
        database.executebatch(paymentBatch,1);
    } 
}