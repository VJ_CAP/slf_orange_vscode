@isTest
public class SetFileMetadataTest {
    @isTest
    public static void createFileMetaDataTest(){
        String username='abc';
        String fileid='123';
        String action='Create';
        test.startTest();
        SetFileMetaData.createFileMetaData(username, fileid, action);
        action='Update';
        SetFileMetaData.createFileMetaData(username, fileid, action);
        action='delete';
        SetFileMetaData.createFileMetaData(username, fileid, action);
        system.assertEquals('delete',action);
        test.stopTest();
    }
}