@IsTest
public class jsonSightenResponse_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'  \"status_code\": 200,'+
		'  \"data\": {'+
		'    \"tmy_datatype\": \"TMY3\",'+
		'    \"date_updated\": \"2016-05-16T23:05:40.850821+00:00\",'+
		'    \"avg_monthly_utility_bill\": 105.7572,'+
		'    \"utility\": {'+
		'      \"link\": \"/api/solar/utility/a7507dcf-1c4a-42a4-9fb9-039d5cdce7ae\",'+
		'      \"natural_id\": \"LADWP - Los Angeles DWP\",'+
		'      \"uuid\": \"a7507dcf-1c4a-42a4-9fb9-039d5cdce7ae\"'+
		'    },'+
		'    \"heating_source\": \"GAS\",'+
		'    \"usage_months\": {'+
		'      \"1\": 721.5531089977,'+
		'      \"2\": 629.8054736516,'+
		'      \"3\": 635.9436862099,'+
		'      \"4\": 589.4425414394,'+
		'      \"5\": 574.0130712993,'+
		'      \"6\": 561.284030612,'+
		'      \"7\": 752.6085177992,'+
		'      \"8\": 729.8932638468,'+
		'      \"9\": 753.2405278828,'+
		'      \"10\": 695.8410307415,'+
		'      \"11\": 634.9512108856,'+
		'      \"12\": 707.0977581766'+
		'    },'+
		'    \"owned_by_organization\": {'+
		'      \"link\": \"\",'+
		'      \"natural_id\": \"Sunlight Financial\",'+
		'      \"uuid\": \"29b2fe4b-df92-45a4-adaa-8f35a39848ed\"'+
		'    },'+
		'    \"owned_by_user\": {'+
		'      \"link\": \"\",'+
		'      \"natural_id\": \"operations@sunlightfinancial.com\",'+
		'      \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\"'+
		'    },'+
		'    \"avg_monthly_usage\": 665.4728517952,'+
		'    \"avg_cost_of_power\": 0.15876702419581146,'+
		'    \"uuid\": \"9ee57ea9-9b13-4496-8713-f2eb1f20f037\",'+
		'    \"created_by\": {'+
		'      \"link\": \"\",'+
		'      \"natural_id\": \"operations@sunlightfinancial.com\",'+
		'      \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\"'+
		'    },'+
		'    \"address\": {'+
		'      \"link\": \"/api/solar/quotegen/address/a92ffaa7abcf6cd5e55aa90417027327\",'+
		'      \"natural_id\": \"1450 Hauser Blvd, Los Angeles, CA 90019\",'+
		'      \"uuid\": \"a92ffaa7abcf6cd5e55aa90417027327\"'+
		'    },'+
		'    \"modified_by\": {'+
		'      \"link\": \"\",'+
		'      \"natural_id\": \"operations@sunlightfinancial.com\",'+
		'      \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\"'+
		'    },'+
		'    \"utility_rate_schedule\": {'+
		'      \"link\": \"/api/solar/utility_rate_schedule/6c1016f5-8c33-4fc1-8bdb-6a4c4e9ef414\",'+
		'      \"natural_id\": \"R-1A\",'+
		'      \"uuid\": \"6c1016f5-8c33-4fc1-8bdb-6a4c4e9ef414\"'+
		'    },'+
		'    \"contacts\": {'+
		'      \"link\": \"/api/solar/quotegen/contact/?solarsite_id=3980\"'+
		'    },'+
		'    \"utility_territory\": {'+
		'      \"link\": \"/api/solar/utility_territory/d695890a-a4c0-4d2c-a64f-9003c8014272\",'+
		'      \"natural_id\": \"Zone 1\",'+
		'      \"uuid\": \"d695890a-a4c0-4d2c-a64f-9003c8014272\"'+
		'    },'+
		'    \"natural_id\": \"1450 Hauser Blvd, Los Angeles, CA\",'+
		'    \"tmy_station\": \"722885\",'+
		'    \"date_created\": \"2016-05-16T16:55:36.906410+00:00\"'+
		'  },'+
		'  \"messages\": {'+
		'    \"critical\": [],'+
		'    \"error\": [],'+
		'    \"info\": [],'+
		'    \"warning\": []'+
		'  }'+
		'}';
		jsonSightenResponse obj = jsonSightenResponse.parse(json);
		System.assert(obj != null);
	}
}