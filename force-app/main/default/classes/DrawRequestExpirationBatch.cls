/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkatesh        05/31/2019         Created
******************************************************************************************/
global class DrawRequestExpirationBatch  implements Database.Batchable<sobject>,Database.Stateful{
    private String query; //Query in the start method
    global List<Draw_Requests__c> lstDrawRequests = new List<Draw_Requests__c>();
    
    /**
* Description: Entry of batch and execute a SOQL.
*/
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('### Entered & Leaving into start() of '+ DrawRequestExpirationBatch.class);
        query=   'SELECT id,Approval_Date__c,Customer_Name__c,Draw_Methodology__c,Installer_Account_Name__c,Installer_Email__c,Primary_Applicant_Email__c,Request_Date__c,Status__c,Sales_Rep_Name__c,Underwriting__c,Underwriting__r.opportunity__r.Owner.ContactId  FROM Draw_Requests__c WHERE Status__c=\'Requested\' AND Request_Date__c!=null ';
        system.debug('### query String '+ query);
        return Database.getQueryLocator(query);
    }
    /**
* Description:
*/
    global void execute(Database.BatchableContext BC, List<Draw_Requests__c> scopeList){
        system.debug('### Entered into execute() of '+ DrawRequestExpirationBatch.class); 
        List<Draw_Requests__c> lstDrawRequestsToUpdt = new List<Draw_Requests__c>();
        Date currentDate=Date.Today();
        If(null!=System.Label.DrawRequestExpDays){
            currentDate=currentDate.addDays(Integer.ValueOf(System.Label.DrawRequestExpDays));
        }else{
             currentDate=currentDate.addDays(-10);
        }
        
        system.debug('### currentDate==>:'+currentDate);
        for(Draw_Requests__c objDrawReq:scopeList){
            Date reqDate = date.newinstance(objDrawReq.Request_Date__c.year(), objDrawReq.Request_Date__c.month(), objDrawReq.Request_Date__c.day());
            system.debug('### reqDate==>:'+reqDate);
             system.debug('### duration==>:'+ currentDate.daysBetween(reqDate));
            if(reqDate==currentDate){
                objDrawReq.Status__c='Expired';
                lstDrawRequestsToUpdt.add(objDrawReq); 
            }
            
        }
        if(!lstDrawRequestsToUpdt.isEmpty()){
            DataBase.update(lstDrawRequestsToUpdt);
            lstDrawRequests.addAll(lstDrawRequestsToUpdt);
        }
        
    }
    /**
* Description:
*/
    global void finish(Database.BatchableContext bc){
        system.debug('### lstDrawRequests.size '+lstDrawRequests.size());
        try{
            if(!lstDrawRequests.isEmpty()){
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                List<OrgWideEmailAddress> senderEmail = [select id,DisplayName, Address from OrgWideEmailAddress where DisplayName ='Sunlight Financial Support' LIMIT 1];
                List<EmailTemplate> lstEmailTemplt =New List<EmailTemplate>();
                lstEmailTemplt=[Select id, htmlValue, Body, subject,DeveloperName from EmailTemplate where DeveloperName='Draw_Request_Expire_Notification_Template'];
                System.debug('#### lstEmailTemplt==>:'+lstEmailTemplt);
                
                for(Draw_Requests__c objDrwRec : lstDrawRequests){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    
                    List<String> toAddresses = new List<String>();
                    
                    // Opportunity Owner Email
                    if(null != objDrwRec.Underwriting__r && null != objDrwRec.Underwriting__r.opportunity__r && String.isNotBlank(objDrwRec.Underwriting__r.opportunity__r.Owner.ContactId) && !lstEmailTemplt.isEmpty()){
                       // toAddresses.add(objDrwRec.Installer_Email__c);
                        mail.setToAddresses(toAddresses);
                        mail.setTargetObjectId(objDrwRec.Underwriting__r.opportunity__r.Owner.ContactId);
                        mail.setTemplateId(lstEmailTemplt[0].id);
                        mail.setWhatId(objDrwRec.id);
                        mail.setSaveAsActivity(false);
                        if(senderEmail.size()>0){
                            mail.setOrgWideEmailAddressId(senderEmail[0].id);
                        }
                        mails.add(mail);
                    }
                    
                    system.debug('### mail==>:'+mail); 
                }
                if(mails.size() > 0 ) {
                    system.debug('### mails==>:'+mails);
                    Messaging.sendEmail(mails);
                }
            }
            
        }catch(exception e){
            system.debug('### DrawRequestExpirationBatch.finish():'+e.getLineNumber()+':'+ e.getMessage() + '###' +e.getStackTraceString());
        }
    }
}