/**
* Description: Task related Trigger logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
   Rajesh                  01/03/2019          Created
******************************************************************************************/ 
public without sharing Class TaskTriggerHandler
{
   
    public void OnAfterInsert(Task[] newTasks){
    try{
            system.debug('### Entered into OnAfterInsert() of '+ TaskTriggerHandler.class);
            Set<id> uwIds = new Set<id>();
             Set<id> taskIds = new Set<id>();
            for(Task taskRec : newTasks)
            {
                if(taskRec.WhatId != null){
                    taskIds.add(taskRec.id);
                    Id myId = taskRec.WhatId;
                    String sObjName = myId.getSObjectType().getDescribe().getName();
                    system.debug('### sObjName-->:'+sObjName);
                    if(sObjName == 'Underwriting_File__c'){
                        uwIds.add(taskRec.WhatId);
                    }
                }
            }
            if(!uwIds.isEmpty()){
                List<Task> lstTasks = new List<Task>();
                List<Underwriting_File__c> lstUWrittingFiles = [SELECT Id,Name,M0_Ready_for_Review_Date_Time__c,(SELECT Id,Status,Type,Review_Date_Time_Complete__c FROM Tasks where Status = 'Open' and Type != 'Post-NTP Issue') FROM Underwriting_File__c WHERE Id in:uwIds];
                for(Underwriting_File__c objUW:lstUWrittingFiles){
                    
                    //objUW.Review_Status__c = null;
                    for(Task objTask:objUW.Tasks){
                        if(!taskIds.contains(objTask.id)){
                            if(objTask.Type == 'M0 Review')
                                objUW.M0_Ready_for_Review_Date_Time__c = null;
                            else if(objTask.Type == 'M1 Review')
                                objUW.M1_Ready_for_Review_Date_Time__c = null;
                             else if(objTask.Type == 'M2 Review')
                                objUW.M2_Ready_for_Review_Date_Time__c  = null;
                            else if(objTask.Type == 'CO Review')
                                objUW.CO_Ready_for_Review_Date_Time__c = null;
                            //Added As part of Orange - 3964 - Start
                            else if(objTask.Type == 'HIDR Review')
                                objUW.HIDR_Ready_for_Review_Date_Time__c = null;
                            else if(objTask.Type == 'HIF Review')
                                objUW.HIF_Ready_for_Review_Date_Time__c = null;
                            //Added As part of Orange - 3964 - End
                            objTask.Status = 'Complete - Stipped';
                            objTask.Review_Date_Time_Complete__c = System.now();
                            lstTasks.add(objTask);
                        }
                    }
                }
                
                if(!lstTasks.isEmpty()){
                    update lstTasks;
                }
                if(!lstUWrittingFiles.isEmpty()){
                    update lstUWrittingFiles;
                }            
            }
                
        }catch(Exception err){
           
            system.debug('### TaskTriggerHandler.OnAfterInsert():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('TaskTriggerHandler.OnAfterInsert()',err.getLineNumber(),'OnAfterInsert()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));            
        }
        system.debug('### Exit from OnAfterInsert() of '+ TaskTriggerHandler.class);
    }
    
    public void OnAfterupdate(Task[] newTasks,Map<id,Task> oldMap){
    try{
            system.debug('### Entered into OnAfterupdate() of '+ TaskTriggerHandler.class);
            Set<id> uwIds = new Set<id>();
             Set<id> taskIds = new Set<id>();
            for(Task taskRec : newTasks)
            {
                if(taskRec.WhatId != null){
                    taskIds.add(taskRec.id);
                    Id myId = taskRec.WhatId;
                    String sObjName = myId.getSObjectType().getDescribe().getName();
                    system.debug('### sObjName-->:'+sObjName);
                    if(sObjName == 'Underwriting_File__c' && taskRec.Status != oldMap.get(taskRec.id).Status && taskRec.Status != 'Complete - No Longer Eligible'){
                        uwIds.add(taskRec.WhatId);
                    }
                }
            }
            if(!uwIds.isEmpty()){
                List<Underwriting_File__c> lstUWrittingFiles = new List<Underwriting_File__c>();
                for(id objUW:uwIds){
                    Underwriting_File__c uwRec = new Underwriting_File__c(id = objUW);
                    lstUWrittingFiles.add(uwRec);
                   
                }
                
               
                if(!lstUWrittingFiles.isEmpty()){
                    update lstUWrittingFiles;
                }            
            }
                
        }catch(Exception err){
           
            system.debug('### TaskTriggerHandler.OnAfterupdate():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('TaskTriggerHandler.OnAfterupdate()',err.getLineNumber(),'OnAfterupdate()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));            
        }
        system.debug('### Exit from OnAfterupdate() of '+ TaskTriggerHandler.class);
    }      
}