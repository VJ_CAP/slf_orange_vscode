@isTest
public class OppToParentInstallerAccountBatchTest {
     
    public static testMethod void OppShareToParentInstallerAccountBatchTest(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
            
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst; 
        
        
        String RecTypeId= [select Id from RecordType where (Name='Business Account') and (SobjectType='Account')].Id;
        Account accPar= new Account();
        accPar.name = 'Parent Account';
        accPar.RecordTypeID=RecTypeId;
        accPar.Type='Partner';
        accPar.BillingStateCode = 'CA';
        accPar.FNI_Domain_Code__c = 'TCU12';
        accPar.Installer_Legal_Name__c='Bright Solar Planet';
        accPar.Solar_Enabled__c=true;
        insert accPar;
        
        Account accInst= new Account();
        accInst.name = 'Installer Account';
        accInst.RecordTypeID=RecTypeId;
        accInst.Type='Partner';
        accInst.BillingStateCode = 'CA';
        accInst.FNI_Domain_Code__c = 'TCU123';
        accInst.ParentId = accPar.Id;
        accInst.Installer_Legal_Name__c='Bright Solar Planet';
        accInst.Solar_Enabled__c=true;
        insert accInst;
        
        Account accAppl = new Account();
        accAppl.name = 'Applicant Account';
        //accAppl.RecordTypeID='Person Account';
        accAppl.Type='PartnerExecutive';
        accAppl.BillingStateCode = 'CA';
        accAppl.FNI_Domain_Code__c = 'TCU';
        accAppl.Installer_Legal_Name__c='Bright Solar Planet';
        accAppl.Solar_Enabled__c=true;
        insert accAppl;
        
        Account accCAppl = new Account();
        accCAppl.name = 'CoApplicant Account';
        // accCAppl.RecordTypeID='Personal Account';
        accCAppl.Type='Partner';
        accCAppl.Installer_Legal_Name__c='Bright Solar Planet';
        accCAppl.Solar_Enabled__c=true;
        //accCAppl.BillingStateCode = 'CA';
        //accCAppl.FNI_Domain_Code__c = 'TCU1';
        //acclist.add(accCAppl);
        insert accCAppl;
         
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
       /* Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;*/

         
         
        //UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = '');
        //insert r;
        Product__c prod = new Product__c();
        prod.Installer_Account__c =accInst.id;
        prod.Long_Term_Facility_Lookup__c =accInst.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.Internal_Use_Only__c = false;
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        prod.Product_Tier__c = '0';
        insert prod;
        
        
        Product_Proxy__c prodProxy = new Product_Proxy__c();
        prodProxy.ACH__c = true;
        prodProxy.APR__c = 2.99;
        prodProxy.Installer__c =accInst.id;
        prodProxy.Internal_Use_Only__c = false;
        prodProxy.Is_Active__c = true;
        prodProxy.SLF_ProductID__c = prod.Id;
        prodProxy.Name = 'testprod';
        prodProxy.State_Code__c ='CA';
        prodProxy.Term_mo__c = 120;          
        insert prodProxy;
        
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        insert trgConflag;
        
        TriggerFlags__c trgConflag1 = new TriggerFlags__c();
        trgConflag1.Name ='User';
        trgConflag1.isActive__c =true;
        insert trgConflag1; 
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = accPar.Id,Is_Default_Owner__c = true);
        insert con;  
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
                    
        // List<Opportunity> oppList = new List<Opportunity>();
        List<Opportunity> personOppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = accAppl.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Installer_Account__c = accInst.id;
        personOpp.Co_Applicant__c = accCAppl.id;
        personOpp.ForecastCategoryName= 'Pipeline';
        personOpp.SLF_Product__c = prod.id;
        insert personOpp;
        
        
        Test.startTest();
        Database.executeBatch(new OppShareToParentInstallerAccountBatch(null),50);
        Test.stopTest();
        
        
        
        
        
        Account accPar1= new Account();
        accPar1.name = 'Parent Account1';
        accPar1.RecordTypeID=RecTypeId;
        accPar1.Type='Partner';
        accPar1.BillingStateCode = 'CA';
        accPar1.FNI_Domain_Code__c = 'TCU122';
        accPar1.Installer_Legal_Name__c='Bright Solar Planet';
        accPar1.Solar_Enabled__c=true;
        insert accPar1;
        
        Account accInst1= new Account();
        accInst1.name = 'Installer Account1';
        accInst1.RecordTypeID=RecTypeId;
        accInst1.Type='Partner';
        accInst1.BillingStateCode = 'CA';
        accInst1.FNI_Domain_Code__c = 'TCU1223';
        accInst1.ParentId = accPar1.Id;
        accInst1.Installer_Legal_Name__c='Bright Solar Planet';
        accInst1.Solar_Enabled__c=true;
        insert accInst1;
        
        Account accAppl1 = new Account();
        accAppl1.name = 'Applicant Account1';
        //accAppl.RecordTypeID='Person Account';
        accAppl1.Type='PartnerExecutive';
        accAppl1.BillingStateCode = 'CA';
        accAppl1.FNI_Domain_Code__c = 'TCU21';
        accAppl1.Installer_Legal_Name__c='Bright Solar Planet';
        accAppl1.Solar_Enabled__c=true;
        insert accAppl1;
        
        Account accCAppl1 = new Account();
        accCAppl1.name = 'CoApplicant Account';
        // accCAppl.RecordTypeID='Personal Account';
        accCAppl1.Type='Partner';
        accCAppl1.Installer_Legal_Name__c='Bright Solar Planet';
        accCAppl1.Solar_Enabled__c=true;
        //accCAppl.BillingStateCode = 'CA';
        //accCAppl.FNI_Domain_Code__c = 'TCU1';
        //acclist.add(accCAppl);
        insert accCAppl1;
        
        //UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = '');
        //insert r;
        Product__c prod1 = new Product__c();
        prod1.Installer_Account__c =accInst.id;
        prod1.Long_Term_Facility_Lookup__c =accInst.id;
        prod1.Name='testprod';
        prod1.FNI_Min_Response_Code__c=9;
        prod1.Product_Display_Name__c='test';
        prod1.Qualification_Message__c ='testmsg';
        prod1.State_Code__c ='CA';
        prod1.ST_APR__c = 2.99;
        prod1.Long_Term_Facility__c = 'TCU';
        prod1.APR__c = 2.99;
        prod1.ACH__c = true;
        prod1.Internal_Use_Only__c = false;
        prod1.LT_Max_Loan_Amount__c =1000;
        prod1.Term_mo__c = 120;
        prod1.Product_Tier__c = '0';
        insert prod1;
        
        
        Product_Proxy__c prodProxy1 = new Product_Proxy__c();
        prodProxy1.ACH__c = true;
        prodProxy1.APR__c = 2.99;
        prodProxy1.Installer__c =accInst1.id;
        prodProxy1.Internal_Use_Only__c = false;
        prodProxy1.Is_Active__c = true;
        prodProxy1.SLF_ProductID__c = prod1.Id;
        prodProxy1.Name = 'testprod';
        prodProxy1.State_Code__c ='CA';
        prodProxy1.Term_mo__c = 120;          
        insert prodProxy1;
        
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = accPar1.Id,Is_Default_Owner__c = true);
        insert con1;   
        
        User portalUser1 = new User(alias = 'testc1', Email='testc1@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test1', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass1@mail.com',ContactId = con1.Id);
        insert portalUser1;
        
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = accAppl1.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Installer_Account__c = accInst1.id;
        personOpp.Co_Applicant__c = accCAppl1.id;
        personOpp.ForecastCategoryName= 'Pipeline';
        personOpp.SLF_Product__c = prod1.id;
        
        update personOpp;
        
                 //Insert opportunity record
         List<Opportunity> oppList = new List<Opportunity>();
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
                        opp.AccountId = accAppl .id;
            opp.Installer_Account__c = accAppl .id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            //insert opp;
            oppList.add(opp);
            
            insert oppList; 

        
         Underwriting_File__c undStpbt = new Underwriting_File__c();           
            undStpbt.Opportunity__c = opp.id;
            undStpbt.Approved_for_Payments__c = true;
            insert undStpbt;
            
    }
    
    
    public static testMethod void InstallerAccShareToParentAccountBatchtest(){

        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
         TriggerFlags__c trgConflag2 = new TriggerFlags__c();
        trgConflag2.Name ='Contact';
        trgConflag2.isActive__c =true;
        trgLst.add(trgConflag2);
        
        TriggerFlags__c trgConflag1 = new TriggerFlags__c();
        trgConflag1.Name ='User';
        trgConflag1.isActive__c =true;
        trgLst.add(trgConflag1);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
            
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst; 
        
        
        String RecTypeId= [select Id from RecordType where (Name='Business Account') and (SobjectType='Account')].Id;
        Account accPar= new Account();
        accPar.name = 'Parent Account';
        accPar.RecordTypeID=RecTypeId;
        accPar.Type='Partner';
        accPar.BillingStateCode = 'CA';
        accPar.FNI_Domain_Code__c = 'TCU12';
        accPar.Installer_Legal_Name__c='Bright Solar Planet';
        accPar.Solar_Enabled__c=true;
        insert accPar;
        
        Account accPar1 = new Account();
        accPar1.name = 'Parent Account1';
        accPar1.RecordTypeID=RecTypeId;
        accPar1.Type='Partner';
        accPar1.BillingStateCode = 'CA';
        accPar1.FNI_Domain_Code__c = 'TCU121';
        accPar1.Installer_Legal_Name__c='Bright Solar Planet';
        accPar1.Solar_Enabled__c=true;
        insert accPar1;
        
        
        Account accInst= new Account();
        accInst.name = 'Installer Account';
        accInst.RecordTypeID=RecTypeId;
        accInst.Type='Partner';
        accInst.BillingStateCode = 'CA';
        accInst.FNI_Domain_Code__c = 'TCU123';
        accInst.ParentId = accPar.Id;
        accInst.Installer_Legal_Name__c='Bright Solar Planet';
        accInst.Solar_Enabled__c=true;
        insert accInst;
        
       
        
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = accPar.Id,Is_Default_Owner__c = true);
        insert con;
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =accInst.id;
        prod.Long_Term_Facility_Lookup__c =accInst.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.Internal_Use_Only__c = false;
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        prod.Product_Tier__c = '0';
        insert prod;
        
        //Insert opportunity record
         List<Opportunity> oppList = new List<Opportunity>();
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
                        opp.AccountId = accInst.id;
            opp.Installer_Account__c = accInst.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            //opp.Partner_Foreign_Key__c = 'TCU||1423';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            //insert opp;
            oppList.add(opp);
            
            insert oppList; 

        
         Underwriting_File__c undStpbt = new Underwriting_File__c();           
            undStpbt.Opportunity__c = opp.id;
            undStpbt.Approved_for_Payments__c = true;
            insert undStpbt;
                    
        accInst.ParentId = accPar1.Id;
        update accInst;
        
        Test.startTest();
        Database.executeBatch(new InstallerAccShareToParentAccountBatch(null),50);
        Test.stopTest();

        
        
    } 
  
}