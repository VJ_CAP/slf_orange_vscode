@isTest
public class SLFRestDispatchTestTemp {    
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
        
    }
   /**
*
* Description: This method is used to cover RBPFNIFacilityDecisionDataServiceImpl and RBPFNIFacilityDecisionService 
*
*/   
    private testMethod static void RBPFNIFacilityDecisionServiceTest(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' and id = :loggedInUsr[0].contact.AccountId LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Installer_Account__c,Install_State_Code__c,Partner_Foreign_Key__c,Product_Loan_Type__c,SLF_Product__c from opportunity where AccountId =: acc.Id LIMIT 1]; 
        Product__c prod = [select Id,Name,Long_Term_Facility__c,Product_Tier__c,Is_Active__c,Term_mo__c,State_Code__c,Internal_Use_Only__c,APR__c,Installer_Account__c from Product__c where Name =: 'testprod' LIMIT 1];
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1];
        
        System.runAs(loggedInUsr[0])
        {
            Test.starttest();
            Prequal__c preq = new Prequal__c();
            preq.Opportunity__c = opp.Id;
            preq.Installer_Account__c = acc.id;
            preq.Pre_Qual_Status__c ='Auto Approved';
            insert preq;
            
            prod.Product_Tier__c ='0';
            update prod;
            
            Map<String, String> RBPFNIFacilityDecisionMap = new Map<String, String>();
            
            String RBPFNIFacilityDecision ;            
            
            RBPFNIFacilityDecision ='{"prequal":{"id":"'+preq.id+'"},"projects":[{"productType":"Solar","credits":[{"id":"'+cred.id+'","fniSoftResp":" <?xmlversion=\\"1.0\\"encoding=\\"UTF-8\\"standalone=\\"no\\"?> <soap:Envelope xmlns:soap=\\"http://schemas.xmlsoap.org/soap/envelope/\\"> <soap:Header/> <soap:Body> <ns3:RESPONSE xmlns=\\"http://tko.fni.com/application/transaction.xsd\\" xmlns:ns2=\\"http://tko.fni.com/application/request.xsd\\" xmlns:ns3=\\"http://tko.fni.com/application/response.xsd\\"> <ns3:StatusCode>SUCCESS</ns3:StatusCode> <ns3:StatusMessage/> <ns3:FNIReferenceNumber>20000007248832</ns3:FNIReferenceNumber> <ns3:Decision>A</ns3:Decision> <ns3:DecisionDateTime>2018-09-07T13:55:24-05:00</ns3:DecisionDateTime> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:LastName>DONALD</ns3:LastName> <ns3:TransactionID>6BF60E49-35E5-4103-B157-1A351FC12777</ns3:TransactionID> <ns3:LenderID>SUN</ns3:LenderID> <ns3:Strategy> <ns3:StipRsn1/> <ns3:StipRsn2/> <ns3:StipRsn3/> <ns3:StipRsn4/> <ns3:LineAssignmentAmt>10000000</ns3:LineAssignmentAmt> <ns3:DTI>1.7</ns3:DTI> <ns3:LowFico>793</ns3:LowFico> <ns3:DebtForMax>485</ns3:DebtForMax> </ns3:Strategy> <ns3:PrequalDecisions> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Decision>D</ns3:Decision> <ns3:Line>7000000</ns3:Line> <ns3:StipRsn/> <ns3:MaxPayment>24514</ns3:MaxPayment> <ns3:MaxDTI>750</ns3:MaxDTI> </ns3:Lender> </ns3:PrequalDecisions> <ns3:ApplicationDate>2018-09-07T13:55:24-05:00</ns3:ApplicationDate> <ns3:Applicant> <ns3:FirstName>YESHAR</ns3:FirstName> <ns3:MiddleInitial/> <ns3:LastName>DONALD</ns3:LastName> <ns3:SSN/> <ns3:DOB/> <ns3:FICO>0793</ns3:FICO> <ns3:FICODSC1>LENGTHOFTIMESINCEMOSTRECENTACCOUNTESTABLISHED</ns3:FICODSC1> <ns3:FICODSC2>PROPORTIONOFBALANCETOHIGHCREDITONBANKREVOLVINGORALLREVOLVINGACCOUNTS</ns3:FICODSC2> <ns3:FICODSC3>CURRENTBALANCESONREVOLVINGACCOUNTS</ns3:FICODSC3> <ns3:FICODSC4>LENGTHOFREVOLVINGACCOUNTHISTORY</ns3:FICODSC4> </ns3:Applicant> <ns3:RBP> <ns3:Lenders> <ns3:Lender> <ns3:Name>TCU</ns3:Name> <ns3:Tiers> <ns3:Tier> <ns3:TierID>1</ns3:TierID> <ns3:StipRsn/> <ns3:MaxDTI>500</ns3:MaxDTI> <ns3:MaxPayment>-485</ns3:MaxPayment> <ns3:Decision>A</ns3:Decision> </ns3:Tier> </ns3:Tiers> </ns3:Lender> </ns3:Lenders> <ns3:GlobalDecision>A</ns3:GlobalDecision> </ns3:RBP> </ns3:RESPONSE> </soap:Body> </soap:Envelope>"}]}]}';
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            RBPFNIFacilityDecision =  '{ "prequal" : { "id" : "'+preq.id+'" }, "projects" : [ { "productType" : "Solar", "credits" : [ { "id" :  "'+cred.id+'", "fniSoftResp" : "test Response" } ] } ] }'; 
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            RBPFNIFacilityDecision =  '{ "prequal" : { "id" : "'+preq.id+'" }, "projects" : [ { "productType" : "Solar", "credits" : [ { "id" :  "", "fniSoftResp" : "" } ] } ] }'; 
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            RBPFNIFacilityDecision =  '{ "prequal" : { "id" : "" }, "projects" : [ { "productType" : "SolarRoof", "credits" : [ { "id" : "'+cred.id+'", "fniSoftResp" : "" } ] } ] }'; 
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            
            RBPFNIFacilityDecision =  'Exceptiondata'; 
            MapWebServiceURI(RBPFNIFacilityDecisionMap,RBPFNIFacilityDecision,'rbpfnisoftpulldecision');
            Test.stoptest();
        }
    }
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}