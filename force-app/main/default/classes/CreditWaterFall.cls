public class CreditWaterFall{
    private static String action = 'PREQUAL';
    private static String partnerId = 'SALESFORCE';
    private static String applicantType = 'PRIM';
    private static String addressType = 'CURR';
    private static String employmentType = 'CURR';
    Static String isSandbox='';

   public Static String buildSoftPullFNIReq(Prequal__c prqlObjt){
        System.debug('### Entered into buildSoftPullFNIReq() of '+CreditWaterFall.class);
        Prequal__c prqlObj = [Select id, Installer_Name__c, Installer_Account__r.FNI_Domain_Code__c, First_Name__c,                     
                             Middle_Name__c,SSN__c,Last_Name__c,Credit__c,
                             Installer_Account__r.Risk_Based_Pricing__c,
                             DOB__c,Mailing_Street__c,Mailing_City__c,Mailing_State__c,Mailing_Postal_Code__c,
                             Annual_Income__c,Lending_Facility__c,Employer_Name__c,
                             Requested_Loan_Amount__c,Term_mo__c,APR__c, Opportunity__r.Product_Loan_Type__c,Opportunity__r.Monthly_Payment__c,Opportunity__r.Escalated_Monthly_Payment_calc__c,
                             Opportunity__r.SyncedQuote.Unofficial_HIS_PMT_Value__c, Opportunity__r.SyncedQuoteId,
                             Opportunity__r.SLF_Product__c,Opportunity__r.Project_Category__c,
                             Employment_Months__c,Employment_Years__c,Co_LastName__c,Co_FirstName__c,
                             Co_Middle_Initial__c,Co_Mailing_Street__c,Co_Mailing_Postal_Code__c,Co_Mailing_City__c,
                             Co_Mailing_State__c,Co_Employer_Name__c,Co_Employment_Months__c,Co_SSN__c,
                             Co_Employment_Years__c,Co_Annual_Income__c,Co_Date_of_Birth__c
                             from Prequal__c where id = :prqlObjt.Id];
        //set conditional parameters
        Boolean runningInASandbox = fniCalloutManual.runningInASandbox();
        
        String soapBody = '';
        try{   

            SLF_Boomi_API_Details__c FNIAPIDetails = new SLF_Boomi_API_Details__c ();
            
            // Description: Added code to read boomi and FNI API details from the custom settings
            Map<String,SLF_Boomi_API_Details__c> FNIAPIDetailsMap = SLF_Boomi_API_Details__c.getAll();
            
            //set conditional parameters
            if(fniCalloutManual.runningInASandbox()){                
                FNIAPIDetails = FNIAPIDetailsMap.get('FNI QA');
            }else{
                FNIAPIDetails = FNIAPIDetailsMap.get('FNI Prod');
            }
                
            soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:req="http://tko.fni.com/application/request.xsd" xmlns:tran="http://tko.fni.com/application/transaction.xsd"><soapenv:Header/><soapenv:Body><req:REQUEST><tran:TransactionControl><tran:UserName>' + 
                FNIAPIDetails.Username__c +
                '</tran:UserName><tran:Password>' +
                FNIAPIDetails.Password__c +
                '</tran:Password><tran:TransactionTimeStamp>' +
                System.now().year() + '-' +
                (System.now().month() < 10 ? '0': '') +
                System.now().month() + '-' +
                (System.now().day() < 10 ? '0': '') +
                System.now().day() + 'T' +
                System.now().time() +
                '</tran:TransactionTimeStamp><tran:Action>' +
                action +
                '</tran:Action><tran:ExternalReferenceNumber>' +
                prqlObj.Id + 
                '</tran:ExternalReferenceNumber><tran:PartnerId>' +
                partnerId +
                '</tran:PartnerId><tran:DomainName>' +
                prqlObj.Installer_Account__r.FNI_Domain_Code__c +
                '</tran:DomainName></tran:TransactionControl><req:RequestData><req:AppRequest><req:Applicants>';
            //applicant info, this would duplicate if we did multiple
            if(prqlObj.Last_Name__c != null){
                soapBody += '<req:Applicant><req:ApplicantType>' +
                    applicantType +
                    '</req:ApplicantType><req:Name><req:First>' +prqlObj.First_Name__c +'</req:First>';
                if(prqlObj.Middle_Name__c != null) 
                    soapBody += '<req:MI>' + prqlObj.Middle_Name__c.left(1) + '</req:MI>';
                soapBody += '<req:Last>' + prqlObj.Last_Name__c  + '</req:Last></req:Name>';
                if(prqlObj.SSN__c != null && prqlObj.SSN__c != ''){
                    String ssn = prqlObj.SSN__c.replaceAll('-', '');
                    if(ssn.length() == 9)
                        soapBody += '<req:SSN>' + ssn +'</req:SSN>';
                }   
                if(prqlObj.DOB__c != null) 
                    soapBody += '<req:DateOfBirth>' + prqlObj.DOB__c.year() + (prqlObj.DOB__c.month() < 10 ? '0': '') + prqlObj.DOB__c.month() + (prqlObj.DOB__c.day() < 10 ? '0': '') +    prqlObj.DOB__c.day() + '</req:DateOfBirth>';
                soapBody += '<req:Addresses><req:Address><req:AddressType>' +
                    addressType +
                    '</req:AddressType><req:AddressLine1>' +
                    prqlObj.Mailing_Street__c +
                    '</req:AddressLine1><req:City>' +
                    prqlObj.Mailing_City__c +
                    '</req:City><req:State>' +
                    prqlObj.Mailing_State__c +
                    '</req:State><req:PostalCode>';
                if(prqlObj.Mailing_Postal_Code__c != null) 
                    soapBody += prqlObj.Mailing_Postal_Code__c.left(5);
                soapBody += '</req:PostalCode></req:Address></req:Addresses>';
                            
                soapBody += '<req:Employers><req:Employer>';
                    soapBody += '<req:EmploymentType>' + employmentType + '</req:EmploymentType>';
                    soapBody += '<req:CompanyName>'+prqlObj.Employer_Name__c+'</req:CompanyName>';                    
                    soapBody += '<req:EmploymentStatus>FT</req:EmploymentStatus>';
                    if(prqlObj.Annual_Income__c != null) 
                        soapBody += '<req:YearlyIncome>' + prqlObj.Annual_Income__c + '</req:YearlyIncome>';
                soapBody += '</req:Employer></req:Employers>';
                
                soapBody += '</req:Applicant>';
            }
            //co applicant
            if(prqlObj.Co_LastName__c != null){
                soapBody += '<req:Applicant><req:ApplicantType>COAPP</req:ApplicantType>';
                soapBody += '<req:Name>';
                    soapBody += '<req:First>' +prqlObj.Co_FirstName__c +'</req:First>';
                    if(prqlObj.Co_Middle_Initial__c != null) 
                        soapBody += '<req:MI>' + prqlObj.Co_Middle_Initial__c.left(1) + '</req:MI>';
                    soapBody += '<req:Last>' + prqlObj.Co_LastName__c  + '</req:Last>';
                soapBody += '</req:Name>';
                
                if(prqlObj.Co_SSN__c != null && prqlObj.Co_SSN__c != ''){
                    String ssn = prqlObj.Co_SSN__c.replaceAll('-', '');
                    if(ssn.length() == 9)
                        soapBody += '<req:SSN>' + ssn +'</req:SSN>';
                }   
                if(prqlObj.Co_Date_of_Birth__c != null) 
                    soapBody += '<req:DateOfBirth>' + prqlObj.Co_Date_of_Birth__c.year() + (prqlObj.Co_Date_of_Birth__c.month() < 10 ? '0': '') + prqlObj.Co_Date_of_Birth__c.month() + (prqlObj.Co_Date_of_Birth__c.day() < 10 ? '0': '') +    prqlObj.Co_Date_of_Birth__c.day() + '</req:DateOfBirth>';
                soapBody += '<req:Addresses><req:Address><req:AddressType>' +
                    addressType +
                    '</req:AddressType><req:AddressLine1>' +
                    prqlObj.Co_Mailing_Street__c +
                    '</req:AddressLine1><req:City>' +
                    prqlObj.Co_Mailing_City__c +
                    '</req:City><req:State>' +
                    prqlObj.Co_Mailing_State__c +
                    '</req:State><req:PostalCode>';
                if(prqlObj.Co_Mailing_Postal_Code__c != null) 
                    soapBody += prqlObj.Co_Mailing_Postal_Code__c.left(5);
                soapBody += '</req:PostalCode></req:Address></req:Addresses>';
                            
                soapBody += '<req:Employers><req:Employer>';
                    soapBody += '<req:EmploymentType>' + employmentType + '</req:EmploymentType>';
                    soapBody += '<req:CompanyName>'+prqlObj.Co_Employer_Name__c+'</req:CompanyName>';
                    soapBody += '<req:EmploymentStatus>FT</req:EmploymentStatus>';
                    if(prqlObj.Co_Annual_Income__c != null) 
                        soapBody += '<req:YearlyIncome>' + prqlObj.Co_Annual_Income__c + '</req:YearlyIncome>';
                soapBody += '</req:Employer></req:Employers>';
                
                soapBody += '</req:Applicant>';
            }
            soapBody += '</req:Applicants>';
            
            soapBody += '<req:Loan>';
                  soapBody += '<req:AmountRequested>'+Math.roundToLong(prqlObj.Requested_Loan_Amount__c * 100)+'</req:AmountRequested>';
                if(prqlObj.Opportunity__r.SLF_Product__c != null){
                    if((prqlObj.Opportunity__r.Product_Loan_Type__c == 'HIS' || prqlObj.Opportunity__r.Product_Loan_Type__c == 'HIN') && prqlObj.Opportunity__r.SyncedQuoteId !=null && prqlObj.Opportunity__r.SyncedQuote.Unofficial_HIS_PMT_Value__c != null) //Updated by Ravi as part of 3666
                    {
                        soapBody += '<req:MonthlyPayment>'+ Math.roundToLong(prqlObj.Opportunity__r.SyncedQuote.Unofficial_HIS_PMT_Value__c * 100)+'</req:MonthlyPayment>';
                    }
                    else
                    {
                        soapBody += '<req:MonthlyPayment>'+Math.roundToLong(prqlObj.Opportunity__r.Escalated_Monthly_Payment_calc__c * 100)+'</req:MonthlyPayment>';
                    }
                }
                else{
                    soapBody += '<req:MonthlyPayment></req:MonthlyPayment>';
                }
                soapBody += '<req:CombinedAmount>'+Math.roundToLong(prqlObj.Requested_Loan_Amount__c * 100)+'</req:CombinedAmount>';
            if(prqlObj.Term_mo__c!=null){
                soapBody += '<req:Term>'+prqlObj.Term_mo__c+'</req:Term>';
            }else{
                soapBody += '<req:Term></req:Term>';
            }
            

            if(prqlObj.APR__c!=null){
                soapBody += '<req:APR>'+Math.roundToLong(prqlObj.APR__c * 100)+'</req:APR>';
            }else{
                soapBody += '<req:APR></req:APR>';
            }
            if(prqlObj.Opportunity__c != null ){
                if(null == prqlObj.Opportunity__r.Product_Loan_Type__c && prqlObj.Opportunity__r.Project_Category__c=='Solar' ){//Added By as part of ORANGE - 1738
                    soapBody += '<req:ProductType>LOA</req:ProductType>';
                }else if(null == prqlObj.Opportunity__r.Product_Loan_Type__c && prqlObj.Opportunity__r.Project_Category__c=='Home'){//Added as part of 1663
                     soapBody += '<req:ProductType></req:ProductType>';
                }else if(prqlObj.Opportunity__r.Product_Loan_Type__c == 'Battery Only'){
                    soapBody += '<req:ProductType>BAT</req:ProductType>';
                }else if(prqlObj.Opportunity__r.Product_Loan_Type__c == 'Solar' || prqlObj.Opportunity__r.Product_Loan_Type__c == 'Solar Interest Only'){
                    soapBody += '<req:ProductType>LOA</req:ProductType>';
                }else if(prqlObj.Opportunity__r.Product_Loan_Type__c == 'SolarPlus Roof'){
                    soapBody += '<req:ProductType>ROF</req:ProductType>';
                }else if(prqlObj.Opportunity__r.Product_Loan_Type__c == 'Integrated Solar Shingles'){//Added By Deepika on 24/10/2018
                    soapBody += '<req:ProductType>ISS</req:ProductType>';
                }else if(prqlObj.Opportunity__r.Product_Loan_Type__c == 'HII' || prqlObj.Opportunity__r.Product_Loan_Type__c == 'HIS'){//Added as part of 1663
                    soapBody += '<req:ProductType></req:ProductType>';
                }else if(prqlObj.Opportunity__r.Product_Loan_Type__c == 'HIN'){ //Added by Ravi as part of 3666
                    soapBody += '<req:ProductType>HIN</req:ProductType>';
                }else if(prqlObj.Opportunity__r.Product_Loan_Type__c == 'Non-PV Stand Alone Battery'){//Added as part of 2542
                    soapBody += '<req:ProductType>NPV</req:ProductType>';
                }
            }
            //RBP related code
            System.debug('###prqlObj.Installer_Account__r.Risk_Based_Pricing__c:::'+prqlObj.Installer_Account__r.Risk_Based_Pricing__c);
            if(prqlObj.Installer_Account__r.Risk_Based_Pricing__c)
                soapBody += '<req:RBP_Flag>1</req:RBP_Flag>';
            else
                soapBody += '<req:RBP_Flag>0</req:RBP_Flag>';
            soapBody += '</req:Loan>';
            System.debug('###soapBody :::'+soapBody);

            if(prqlObj.Lending_Facility__c != null && prqlObj.Lending_Facility__c != ''){ 
                List<String> lendersList = new List<String>();
                if(prqlObj.Lending_Facility__c.contains(','))
                    lendersList = prqlObj.Lending_Facility__c.split(',');
                else
                    lendersList.add(prqlObj.Lending_Facility__c);
                if(!lendersList.isEmpty() && (prqlObj.Credit__c== null || !prqlObj.Installer_Account__r.Risk_Based_Pricing__c)){
                    soapBody += '<req:Lenders>';  
                    for(Integer i=0; i<lendersList.size();i++){
                        soapBody += '<req:Lender'+(i+1)+'>'+lendersList[i]+'</req:Lender'+(i+1)+'>';
                    }          
                    soapBody += '</req:Lenders>';
                }                        
            }
            soapBody +='</req:AppRequest></req:RequestData></req:REQUEST></soapenv:Body></soapenv:Envelope>';
        
          }catch(Exception err){
            system.debug('### CreditWaterFall.buildSoftPullFNIReq():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' CreditWaterFall.buildSoftPullFNIReq()',err.getLineNumber(),'buildSoftPullFNIReq()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### soapBody '+soapBody);
        system.debug('### Exit from buildSoftPullFNIReq() of '+CreditWaterFall.class);        
        return soapBody;
     }
     //need to query Installer_Account__r.Risk_Based_Pricing__c, ProductTier__c from opportunity while calling this method
     public Static string buildHardPullFNIReq(Account acc1,Account acc2,Opportunity oppRec,SLF_Credit__c slfCredit,String creditExtRefNum){
        System.debug('### Entered into buildHardPullFNIReq() of '+CreditWaterFall.class);
        boolean eligibleFor4DigitSSN = false;
        List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,UserRole.Name,contact.Account.Eligible_for_Last_Four_SSN__c,contact.Account.Credit_Waterfall__c from User where Id =: userinfo.getUserId()]; 
        eligibleFor4DigitSSN = loggedInUsr[0].contact.Account.Eligible_for_Last_Four_SSN__c; //Added by Suresh on 27/04/2018 to handle SSN 4 Digit eligible installers 
        String soapBody = '';
        try{   

            SLF_Boomi_API_Details__c FNIAPIDetails ;
            
            // Description: Added code to read boomi and FNI API details from the custom settings
            Map<String,SLF_Boomi_API_Details__c> FNIAPIDetailsMap = SLF_Boomi_API_Details__c.getAll();
            
            //set conditional parameters
            if(!fniCalloutManual.runningInASandbox()){
                FNIAPIDetails = FNIAPIDetailsMap.get('FNI Prod');
            }else{
                FNIAPIDetails = FNIAPIDetailsMap.get('FNI QA');
            }
                
            soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:req="http://tko.fni.com/application/request.xsd" xmlns:tran="http://tko.fni.com/application/transaction.xsd">';
            soapBody += '<soapenv:Header/>';
            soapBody += '<soapenv:Body>';
            soapBody += '<req:REQUEST>';
            soapBody += '<tran:TransactionControl>';
            soapBody += '<tran:UserName>' +FNIAPIDetails.Username__c +'</tran:UserName>';
            soapBody += '<tran:Password>' +FNIAPIDetails.Password__c +'</tran:Password>';

            soapBody += '<tran:TransactionTimeStamp>' +
            System.now().year() + '-' +
            (System.now().month() < 10 ? '0': '') +System.now().month() + '-' +
            (System.now().day() < 10 ? '0': '') +
            System.now().day() + 'T' +
            System.now().time() +
            '</tran:TransactionTimeStamp>';
            soapBody += '<tran:Action>APPLY</tran:Action>';
           
            soapBody += '<tran:ExternalReferenceNumber>'+slfCredit.Id+'</tran:ExternalReferenceNumber>'; 
           
            soapBody += '<tran:PartnerId>SALESFORCE</tran:PartnerId>';
            soapBody += '<tran:DomainName>';
            soapBody += '</tran:DomainName>';
            soapBody += '</tran:TransactionControl>';
            soapBody += '<req:RequestData>';
            soapBody += '<req:AppRequest>';
            soapBody += '<req:Applicants>';
                
            if(null != acc1)   
            {               
                //applicant info, this would duplicate if we did multiple
                soapBody += '<req:Applicant>';
                    soapBody += '<req:ApplicantType>PRIM</req:ApplicantType>';
                    soapBody += '<req:Name>';
                        soapBody += '<req:First>'+acc1.FirstName +'</req:First>';
                        if(acc1.MiddleName != null) 
                            soapBody += '<req:MI>' + acc1.MiddleName.left(1) + '</req:MI>';
                        
                        soapBody += '<req:Last>' + acc1.LastName + '</req:Last>';
                    soapBody += '</req:Name>';
                
                    if(!eligibleFor4DigitSSN && acc1.SSN__c!=null){
                        soapBody += '<req:SSN>' + acc1.SSN__c.replaceAll('-', '') +'</req:SSN>';
                    }
                    else{
                        if(acc1.SSN__c!=null){
                            soapBody += '<req:SSN>' + acc1.SSN__c.replaceAll('-', '') +'</req:SSN>';
                        }
                        else if(acc1.Last_four_SSN__c!=null){
                            soapBody += '<req:SSN></req:SSN>';
                        }
                    }
                    
                    if(acc1.PersonBirthdate != null)
                        soapBody += '<req:DateOfBirth>' + acc1.PersonBirthdate.year() + (acc1.PersonBirthdate.month() < 10 ? '0': '') + acc1.PersonBirthdate.month() + (acc1.PersonBirthdate.day() < 10 ? '0': '') +  acc1.PersonBirthdate.day() + '</req:DateOfBirth>';
                    if(null!=acc1.Marital_Status__c)
                        soapBody += '<req:MaritalStatus>' +acc1.Marital_Status__c +'</req:MaritalStatus>';
                    else
                        soapBody += '<req:MaritalStatus>' +'Unmarried'+'</req:MaritalStatus>';
                    
                    if(acc1.Phone != null)
                        soapBody += '<req:PrimaryPhone>' +SubmitCreditDataServiceImpl.escapePhone(acc1.Phone) +'</req:PrimaryPhone>';
                    soapBody += '<req:Email>' +acc1.PersonEmail +'</req:Email>';
                    soapBody += '<req:Addresses>';
                        soapBody += '<req:Address>';
                            soapBody += '<req:AddressType>CURR</req:AddressType>';
                            soapBody += '<req:AddressLine1>' +acc1.ShippingStreet +'</req:AddressLine1>';
                            soapBody += '<req:City>' +acc1.ShippingCity +'</req:City>';
                            soapBody += '<req:State>' +acc1.ShippingStateCode +'</req:State>';
                            if(acc1.ShippingPostalCode != null)
                                soapBody += '<req:PostalCode>' +acc1.ShippingPostalCode.left(5) +'</req:PostalCode>';
                        soapBody += '</req:Address>';
                        soapBody += '<req:Address>';
                            soapBody += '<req:AddressType>MAIL</req:AddressType>';
                            soapBody += '<req:AddressLine1>' +acc1.BillingStreet +'</req:AddressLine1>';
                            soapBody += '<req:City>' +acc1.BillingCity +'</req:City>';
                            soapBody += '<req:State>' +acc1.BillingStateCode +'</req:State>';
                            if(acc1.BillingPostalCode != null)
                                soapBody += '<req:PostalCode>' +acc1.BillingPostalCode.left(5) +'</req:PostalCode>';
                        soapBody += '</req:Address>';
                        soapBody += '<req:Address>';
                            soapBody += '<req:AddressType>PROP</req:AddressType>';
                            soapBody += '<req:AddressLine1>' +oppRec.Install_Street__c +'</req:AddressLine1>';
                            soapBody += '<req:City>'+oppRec.Install_City__c +'</req:City>';
                            soapBody += '<req:State>'+oppRec.Install_State_Code__c +'</req:State>';
                            if(oppRec.Install_Postal_Code__c != null)
                                soapBody += '<req:PostalCode>' +oppRec.Install_Postal_Code__c.left(5) +'</req:PostalCode>';
                        soapBody += '</req:Address>'; 
                                
                    soapBody += '</req:Addresses>';
                    
                    String primAppId = SLFUtility.guidIdGenerator();

                    soapBody += '<req:Identification><req:IDNumber>' +creditExtRefNum +'</req:IDNumber></req:Identification>';
                  
                    soapBody += '<req:Employers>';
                        soapBody += '<req:Employer>';
                            soapBody += '<req:EmploymentType>CURR</req:EmploymentType>';
                            soapBody += '<req:YearlyIncome>' +acc1.Annual_Income__c +'</req:YearlyIncome>';
                            if(acc1.Employer_Name__c != null) 
                                soapBody += '<req:CompanyName>' + acc1.Employer_Name__c + '</req:CompanyName>';
                            
                            if(acc1.PersonOtherPhone != null) 
                                    soapBody += '<req:WorkPhone>' + SubmitCreditDataServiceImpl.escapePhone(acc1.PersonOtherPhone) + '</req:WorkPhone>';
                                
                            if(acc1.Length_of_Employment_Years__c != null){
                                Decimal empMonths = 0;
                                if(acc1.Length_of_Employment_Months__c != null) empMonths = acc1.Length_of_Employment_Months__c;
                                    soapBody += '<req:LengthAtEmployment>' + (acc1.Length_of_Employment_Years__c < 10 ? '0': '') + acc1.Length_of_Employment_Years__c + (empMonths < 10 ? '0': '') + empMonths + '</req:LengthAtEmployment>';    
                            } 
                            
                            if(acc1.Job_Title__c != null) 
                                soapBody += '<req:PositionDescription>' + acc1.Job_Title__c + '</req:PositionDescription>';
                            
                        soapBody += '</req:Employer>';
                    soapBody += '</req:Employers>';
                soapBody += '</req:Applicant>';
            
            }
            
            //add coapplicant here...
            if(acc2 != null){
                soapBody += '<req:Applicant><req:ApplicantType>COAPP</req:ApplicantType>';
                    soapBody += '<req:Name>';
                    soapBody += '<req:First>' +acc2.FirstName +'</req:First>';
                    
                    if(acc2.MiddleName != null) 
                        soapBody += '<req:MI>' + acc2.MiddleName.Left(1) + '</req:MI>';
                    
                        soapBody += '<req:Last>' + acc2.LastName + '</req:Last>';
                    soapBody += '</req:Name>';
                
                    if(!eligibleFor4DigitSSN && acc2.SSN__c!=null){
                        soapBody += '<req:SSN>' + acc2.SSN__c.replaceAll('-', '') +'</req:SSN>';
                    }
                    else{
                        if(acc2.SSN__c!=null){
                            soapBody += '<req:SSN>' + acc2.SSN__c.replaceAll('-', '') +'</req:SSN>';
                        }
                        else if(acc2.Last_four_SSN__c!=null){
                            soapBody += '<req:SSN></req:SSN>';
                        }
                    }
                        if(acc2.PersonBirthdate != null)
                            soapBody += '<req:DateOfBirth>' + acc2.PersonBirthdate.year() + (acc2.PersonBirthdate.month() < 10 ? '0': '') + acc2.PersonBirthdate.month() + (acc2.PersonBirthdate.day() < 10 ? '0': '') +  acc2.PersonBirthdate.day() + '</req:DateOfBirth>';
                        soapBody += '<req:MaritalStatus>' +acc2.Marital_Status__c +'</req:MaritalStatus>';
                        if(acc2.Phone != null)
                            soapBody += '<req:PrimaryPhone>' +SubmitCreditDataServiceImpl.escapePhone(acc2.Phone) +'</req:PrimaryPhone>';
                        soapBody += '<req:Email>' +acc2.PersonEmail +'</req:Email>';
                    soapBody += '<req:Addresses>';
                        soapBody += '<req:Address>';
                            soapBody += '<req:AddressType>CURR</req:AddressType>';
                            soapBody += '<req:AddressLine1>' +acc2.ShippingStreet +'</req:AddressLine1>';
                            soapBody += '<req:City>' +acc2.ShippingCity +'</req:City>';
                            soapBody += '<req:State>'+acc2.ShippingStateCode +'</req:State>';
                            if(acc2.ShippingPostalCode != null)
                                soapBody += '<req:PostalCode>' +acc2.ShippingPostalCode.left(5) +'</req:PostalCode>';
                        soapBody += '</req:Address>';
                        soapBody += '<req:Address>';
                            soapBody += '<req:AddressType>MAIL</req:AddressType>';
                            soapBody += '<req:AddressLine1>' +acc2.BillingStreet +'</req:AddressLine1>';
                            soapBody += '<req:City>' +acc2.BillingCity +'</req:City>';
                            soapBody += '<req:State>' +acc2.BillingStateCode +'</req:State>';
                            if(acc2.BillingPostalCode != null)
                                soapBody += '<req:PostalCode>' +acc2.BillingPostalCode.left(5) +'</req:PostalCode>';
                        soapBody += '</req:Address>';                
                        soapBody += '<req:Address>';
                            soapBody += '<req:AddressType>PROP</req:AddressType>';
                            soapBody += '<req:AddressLine1>' +oppRec.Install_Street__c +'</req:AddressLine1>';
                            soapBody += '<req:City>' +oppRec.Install_City__c +'</req:City>';
                            soapBody += '<req:State>' +oppRec.Install_State_Code__c +'</req:State>';
                            if(oppRec.Install_Postal_Code__c != null)
                                soapBody += '<req:PostalCode>' +oppRec.Install_Postal_Code__c.left(5) +'</req:PostalCode>';
                        soapBody += '</req:Address>';  
                    soapBody += '</req:Addresses>';
                    soapBody += '<req:Identification>';
                        soapBody += '<req:IDNumber>' +creditExtRefNum+'</req:IDNumber>';
                    soapBody += '</req:Identification>';
                    soapBody += '<req:Employers>';
                        soapBody += '<req:Employer>';
                            soapBody += '<req:EmploymentType>CURR</req:EmploymentType>';
                            soapBody += '<req:YearlyIncome>' +acc2.Annual_Income__c + '</req:YearlyIncome>';
                            
                            if(acc2.Employer_Name__c != null) 
                                soapBody += '<req:CompanyName>' + acc2.Employer_Name__c + '</req:CompanyName>';
                            
                            if(acc2.PersonOtherPhone != null) 
                                soapBody += '<req:WorkPhone>' + SubmitCreditDataServiceImpl.escapePhone(acc2.PersonOtherPhone) + '</req:WorkPhone>';
                            
                            if(acc2.Length_of_Employment_Years__c != null){
                                Decimal empCoMonths = 0;
                                if(acc2.Length_of_Employment_Months__c != null) empCoMonths = acc2.Length_of_Employment_Months__c;
                                    soapBody += '<req:LengthAtEmployment>' + (acc2.Length_of_Employment_Years__c < 10 ? '0': '') + acc2.Length_of_Employment_Years__c + (empCoMonths < 10 ? '0': '') + empCoMonths + '</req:LengthAtEmployment>';    
                            } 
                           if(acc2.Job_Title__c != null) 
                               soapBody += '<req:PositionDescription>' + acc2.Job_Title__c + '</req:PositionDescription>';
                       
                       soapBody += '</req:Employer>';
                   soapBody += '</req:Employers>';
               soapBody += '</req:Applicant>';   
             }

            
                soapBody += '</req:Applicants>';
                
                soapBody += '<req:Loan>';
                soapBody += '<req:AmountRequested>';
                if(oppRec.Long_Term_Loan_Amount_Manual__c != null)
                     soapBody += Math.roundToLong(oppRec.Long_Term_Loan_Amount_Manual__c * 100);
                 soapBody += '</req:AmountRequested>';
                 
                if(null != oppRec.Long_Term_Term__c)
                    soapBody += '<req:Term>' +oppRec.Long_Term_Term__c +'</req:Term>';
                else
                    soapBody += '<req:Term>'+0+'</req:Term>'; 
                
                if(null == oppRec.Product_Loan_Type__c ){//Added By as part of ORANGE - 1738
                    soapBody += '<req:ProductType>LOA</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'Battery Only'){
                    soapBody += '<req:ProductType>BAT</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'Solar' || oppRec.Product_Loan_Type__c == 'Solar Interest Only'){
                    soapBody += '<req:ProductType>LOA</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'SolarPlus Roof'){
                    soapBody += '<req:ProductType>ROF</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'Integrated Solar Shingles'){//Added By Deepika on 24/10/2018
                    soapBody += '<req:ProductType>ISS</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'HII'){
                    soapBody += '<req:ProductType>HII</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'HIS'){
                    soapBody += '<req:ProductType>HIS</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'HIN'){ //Added by Ravi as part of 3666
                    soapBody += '<req:ProductType>HIN</req:ProductType>';
                }else if(oppRec.Product_Loan_Type__c == 'Non-PV Stand Alone Battery'){//Added as part of 2542
                    soapBody += '<req:ProductType>NPV</req:ProductType>';
                }
                
                /*else if(oppRec.Product_Loan_Type__c == 'HII' || oppRec.Product_Loan_Type__c == 'HIS'){//Added By as part of ORANGE - 1663
                    soapBody += '<req:ProductType></req:ProductType>';
                }*/ //commented By Brahmeswar
                if(null != oppRec.Long_Term_Facility__c)
                    soapBody += '<req:LenderID>' +oppRec.Long_Term_Facility__c +'</req:LenderID>';
                else
                    soapBody += '<req:LenderID>'+'TCU'+'</req:LenderID>';  
                soapBody += '<req:APR>' ;
                
                if(oppRec.Long_Term_APR__c != null)
                    soapBody += Math.roundToLong(oppRec.Long_Term_APR__c * 100) ;
                else
                    soapBody += 499;
                    
                soapBody +='</req:APR>';
                soapBody += '<req:MonthlyPayment>';

                if((oppRec.Product_Loan_Type__c == 'HIS' || oppRec.Product_Loan_Type__c == 'HIN') && oppRec.SyncedQuoteId !=null && oppRec.SyncedQuote.Unofficial_HIS_PMT_Value__c != null) //Updated by Ravi as part of 3666
                {
                    soapBody += Math.roundToLong(oppRec.SyncedQuote.Unofficial_HIS_PMT_Value__c * 100);
                }
                else if(oppRec.Escalated_Monthly_Payment_calc__c != null) 
                {
                    soapBody += Math.roundToLong(oppRec.Escalated_Monthly_Payment_calc__c * 100);
                }
                soapBody += '</req:MonthlyPayment>';
                
            soapBody+= '<req:CombinedAmount>';   
                if(oppRec.Combined_Loan_Amount__c != null){     
                    soapBody += Math.roundToLong(oppRec.Combined_Loan_Amount__c * 100);     
                }
            soapBody += '</req:CombinedAmount>';
            //RBP related code
            if(oppRec.Installer_Account__r.Risk_Based_Pricing__c){
                if(oppRec.ProductTier__c == '0' || oppRec.ProductTier__c == null || oppRec.ProductTier__c == '')
                     soapBody += '<req:RBP_Tier></req:RBP_Tier>';
                else
                    soapBody += '<req:RBP_Tier>'+oppRec.ProductTier__c+'</req:RBP_Tier>';
            }
            //RBP related code-- Added as part of ORANGE-1663
            System.debug('###oppRec.Installer_Account__r.Risk_Based_Pricing__c:::'+oppRec.Installer_Account__r.Risk_Based_Pricing__c);
            if(oppRec.Installer_Account__r.Risk_Based_Pricing__c)
                soapBody += '<req:RBP_Flag>1</req:RBP_Flag>';
            else
                soapBody += '<req:RBP_Flag>0</req:RBP_Flag>';
                
            soapBody += '</req:Loan>';
            soapBody += '<req:LenderFields>';
            soapBody += '<req:LenderField1>' +oppRec.Installer_Account__r.FNI_Domain_Code__c +'</req:LenderField1>';
            soapBody += '<req:LenderField2>SLF not used</req:LenderField2>';
            soapBody += '<req:LenderField3>' +oppRec.Id +'</req:LenderField3>';
            soapBody += '<req:LenderField4>Quote</req:LenderField4>';
            soapBody += '<req:LenderField5>' +oppRec.SyncedQuoteId +'</req:LenderField5>';
            soapBody += '<req:LenderField6>' +oppRec.Install_Cost_Cash__c +'</req:LenderField6>';
            soapBody += '<req:LenderField7>' +oppRec.Initial_Payment__c +'</req:LenderField7>';
            soapBody += '<req:LenderField8>';
            soapBody += acc2 == null ? 'INDIVIDUAL' : 'JOINT'; //oppRec Applicant requests individual oppRec or Co-applicants request joint oppRec +
            soapBody += '</req:LenderField8>';
            soapBody += '</req:LenderFields>';
            soapBody += '</req:AppRequest>';
            soapBody += '</req:RequestData>';
            soapBody += '</req:REQUEST>';
            soapBody += '</soapenv:Body>';
            soapBody += '</soapenv:Envelope>';
            
          }catch(Exception err){
            system.debug('### CreditWaterFall.buildHardPullFNIReq():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' CreditWaterFall.buildHardPullFNIReq()',err.getLineNumber(),'buildHardPullFNIReq()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### soapBody '+soapBody);
        system.debug('### Exit from buildHardPullFNIReq() of '+CreditWaterFall.class);
        
        return SLFUtility.replaceSpecialChars(soapBody);
    }
}