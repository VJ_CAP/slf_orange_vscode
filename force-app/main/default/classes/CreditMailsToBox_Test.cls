@isTest
public class CreditMailsToBox_Test{   
    public static testMethod void uploadToBoxTest(){
        SLFUtilityTest.createCustomSettings();   
        
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        Case caseTest = new Case();
        caseTest.Status = 'New';
        caseTest.Origin = 'Email';
        insert caseTest;
        
        Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.Push_Endpoint__c ='www.test.com';
        acc.PersonEmail = 'slfaccount@slf.com';
        acc.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
        acc.Installer_Email__c = 'slfaccount@slf.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Subscribe_to_Push_Updates__c = true;
        acc.Subscribe_to_Event_Updates__c=true;
        acc.SMS_Opt_in__c=true;
        insert acc;
        
        Contact cObj = new Contact(LastName = 'Sunlight Financial',Email='test@gmail.com');
        insert cObj;
        
        Product__c prodct = new Product__c();
        prodct.ACH__c = false;
        prodct.APR__c = 4.99;
        prodct.bump__c = false;
        prodct.Installer_Account__c = acc.Id;
        prodct.Internal_Use_Only__c = false;
        prodct.Is_Active__c = true;
        prodct.Long_Term_Facility__c = 'TCU';
        prodct.LT_Max_Loan_Amount__c =  100000.0;
        prodct.Max_Loan_Amount__c = 100000.0;
        prodct.State_Code__c = 'CA';
        prodct.FNI_Min_Response_Code__c = 10;
        prodct.Product_Display_Name__c = 'Test';
        prodct.Qualification_Message__c = '123';
        prodct.Product_Tier__c = '0';
        insert prodct;
        
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Postal_Code__c = '94901';
        opp.Install_Street__c = '39 GLEN AVE';
        opp.Install_City__c = 'SAN RAFAEL';
        opp.SLF_Product__c = prodct.Id;
        opp.Combined_Loan_Amount__c = 10000;
        opp.Long_Term_Loan_Amount_Manual__c = 20000;
        opp.Co_Applicant__c = acc.Id;
        opp.Project_Category__c='Solar';
        insert opp;                   
        
        Underwriting_File__c uwf = new Underwriting_File__c(
            Send_Confirmation_Email__c = true,
            Opportunity__c = opp.Id,
            M1_Approval_Requested_Date__c=date.Today()
        );
        insert uwf;        
        
        Document  document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'Sunlight_Logo1';
        document.IsPublic = true;
        document.Name = 'Sunlight Logo';
        document.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert document;
        
        SLF_Credit__c slfCredit = new SLF_Credit__c();
        slfCredit.SLF_Product__c = prodct.Id;
        slfCredit.Total_Loan_Amount__c = opp.Combined_Loan_Amount__c;
        slfCredit.Long_Term_Loan_Amount__c  = opp.Long_Term_Loan_Amount_Manual__c;
        slfCredit.Primary_Contact__c = cObj.Id;
        if(null != opp.AccountId)
            slfCredit.Primary_Applicant__c = opp.AccountId;
        
        if(null != opp.Co_Applicant__c)
            slfCredit.Co_Applicant__c = opp.Co_Applicant__c;           
        slfCredit.Opportunity__c = opp.Id ;
        slfCredit.Status__c = 'Auto Approved';
        slfCredit.IsSyncing__c = true ;          
        slfCredit.Long_Term_Amount_Approved__c = 1000;
        slfCredit.Customer_has_authorized_credit_hard_pull__c = true;
        insert slfCredit;
        
        Test.startTest();    
        System.runAs(new User(Id = UserInfo.getUserId())){
            CreditMailsToBox  crobj = new CreditMailsToBox();
            List <SLF_Credit__c> crlst = [SELECT Id FROM SLF_Credit__c limit 10];
            Id crditId = crlst[0].id;
            EmailTemplate etObj = new EmailTemplate(DeveloperName = 'test', TemplateType= 'Text', Name = 'test',IsActive=true,folderId = UserInfo.getUserId()); 
            insert etObj;
            CreditMailsToBox.Input ipObj = new CreditMailsToBox.input(slfCredit.Id,etObj.DeveloperName);
            CreditMailsToBox.uploadToBox(new List<CreditMailsToBox.Input>{ipObj}); 
        }
        Test.stopTest();      
    }    
}