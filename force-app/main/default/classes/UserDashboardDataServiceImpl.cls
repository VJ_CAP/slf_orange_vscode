/**
* Description: Report related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkat               11/14/2018              Created
******************************************************************************************/
public without sharing class UserDashboardDataServiceImpl extends RestDataServiceBase{
    List<User> lstUsersData = new List<User>();
    Map<Id,List<User>> installerAccUserMap=new Map<Id,List<User>>();
    Static Map<Id,List<User>> userReporterMap =new  Map<Id,List<User>>();
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+UserDashboardDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj; 
        system.debug('### reqData '+reqData);
        
        UnifiedBean unifiedBean = new UnifiedBean(); 
        unifiedBean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            
            try
        {   
            
            String userId = ''; 
            
            if(reqData.userId != null && String.isNotBlank(reqData.userId))
            {              
                userId = reqData.userId;                
            }else{
                userId = UserInfo.getUserId();               
            }
            System.debug('#### userId==>:'+userId);
            if(String.isNotBlank(UserId)){
                
                
                
                List<User> loggedInUser = new List<User>();
                List<Account> childInstallersAccount = new List<Account>();
                loggedInUser = [Select id,Name,contact.Account.parentid,contact.Account.name,UserRoleId,UserRole.Name,contact.AccountId,contact.ReportsToId from User where id = :userId AND isActive=true];
                if(!loggedInUser.isEmpty() && loggedInUser[0] != null && loggedInUser[0].UserRoleId != null ){

                   generateUserWrappers(unifiedBean,loggedInUser[0]);                    
                    unifiedBean.returnCode = '200';
                    
                }
                else{
                    system.debug('#### in else ');
                    iolist = new list<DataValidator.InputObject>{};
                        errMsg = ErrorLogUtility.getErrorCodes('214', null);                    
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    unifiedBean.error.add(errorMsg);
                    unifiedBean.returnCode = '214';
                    iRestRes = (IRestResponse)unifiedBean;
                    return iRestRes;
                }
            }           
            
            //iRestRes = (IRestResponse)unifiedBean;
        }catch(Exception err){
            system.debug('Exception -->: '+err.getMessage());
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400', null);
            unifiedBean = new UnifiedBean(); 
            unifiedBean.error = new list<UnifiedBean.errorWrapper>();
            gerErrorMsg(unifiedBean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)unifiedBean;
            system.debug('### UserDashboardDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString()); 
            ErrorLogUtility.writeLog('UserDashboardDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            //return iRestRes;
        }    
        system.debug('### Exit from transformOutput() of '+UserDashboardDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
         system.debug('### unifiedBean '+JSON.serialize(unifiedBean));
        iRestRes = (IRestResponse)unifiedBean;
        return iRestRes;
    }  
    /******************************************************
     * @ Method     : 
     * @ Return Type:
     * @ Parameters :
*********************************************************/
    public void generateUserWrappers(UnifiedBean unifiedBean,user userRec){
            unifiedBean.installers = new List<UnifiedBean.InstallersWrapper>(); // Added as part of 2092
            UnifiedBean.InstallersWrapper instalWrp = new UnifiedBean.InstallersWrapper();
            
            UnifiedBean.InstallersWrapper tempInstalWrp = new UnifiedBean.InstallersWrapper();
            
            instalWrp.salesreps=new List<UnifiedBean.UserWrapper>();
            instalWrp.executives = new List<UnifiedBean.UserWrapper>();
            instalWrp.managers = new List<UnifiedBean.ManagerWrapper>();

            Set<Id> installerIdSet = new Set<Id>();
          // logged in user is executive
        if(userRec.UserRole.Name.contains('Executive') ){
            
               // Installer has parent 
            if(null!=userRec.contact.Account.parentid){
                installerIdSet.add(userRec.contact.AccountId);
                installerAccUserMap = getUserList('', installerIdSet);
            }else{
               installerIdSet.add(userRec.contact.AccountId);
               system.debug('### ParentId==>:'+userRec.contact.AccountId);
                for(Account installRec:[SELECT id,Name FROM Account WHERE ParentId = :userRec.contact.AccountId]){
                    system.debug('#### installerIdSet ==>:'+installerIdSet);
                    if(null!=installRec){
                        installerIdSet.add(installRec.id);
                    } 
                }
                system.debug('#### installerIdSet ==>:'+installerIdSet);
                installerAccUserMap = getUserList('', installerIdSet);
                system.debug('#### installerAccUserMap ==>:'+installerAccUserMap);
            }
            
            // user has reportees
            if(!installerAccUserMap.isEmpty()){
                System.debug('#### unifiedBean==>:'+instalWrp);
                for(Id objAcc:installerAccUserMap.keySet()){
                    instalWrp = new UnifiedBean.InstallersWrapper();
                    instalWrp.executives = new List<UnifiedBean.UserWrapper>();
                    instalWrp.managers = new List<UnifiedBean.ManagerWrapper>();
                    instalWrp.salesreps=new List<UnifiedBean.UserWrapper>();
                    instalWrp.name=installerAccUserMap.get(objAcc)[0].contact.Account.name;
                    instalWrp.installerId=installerAccUserMap.get(objAcc)[0].contact.AccountId;
                    if(installerAccUserMap.containsKey(userRec.contact.AccountId) && userRec.contact.AccountId == installerAccUserMap.get(objAcc)[0].contact.AccountId){
                        instalWrp.isChild=false;
                    }else{
                        instalWrp.isChild=true;
                    }
                    //instalWrp.executives.add(generateUserWrapper(userRec,'Executive'));
                    for(User objUser:installerAccUserMap.get(objAcc)){ 
                        
                        if(objUser.UserRole.Name.contains('Executive')){
                            instalWrp.executives.add(generateUserWrapper(objUser,'Executive'));
                        }
                        if(!objUser.UserRole.Name.contains('Executive') && objUser.contact.ReportsToId==null){
                            if(!userReporterMap.isEmpty() && userReporterMap.containsKey(objUser.contactId)){
                                // user has reportees 
                                UnifiedBean.ManagerWrapper mangWrap = new UnifiedBean.ManagerWrapper();
                                mangWrap.hasReportsTo  = new List<UnifiedBean.UserWrapper>();
                                //Added By Adithya
                                //Start
                                mangWrap.hasReportsTo.add( generateUserWrapper(objUser,(objUser.UserRole.Name.contains('User'))?'User':'Manager'));
                                //End
                                mangWrap.userId=objUser.id;
                                mangWrap.name=objUser.Name;
                                mangWrap.role=(objUser.UserRole.Name.contains('User'))?'User':'Manager';
                                instalWrp.managers.add(mangWrap);
                                
                                for(User objUserRec:userReporterMap.get(objUser.contactId)){
                                    mangWrap.hasReportsTo.add( generateUserWrapper(objUserRec,(objUserRec.UserRole.Name.contains('User'))?'User':(objUserRec.UserRole.Name.contains('Executive'))?'Executive':'Manager') );
                                }
                                system.debug('### Manager Wrapper ==>:'+mangWrap);
                            }else{
                                system.debug('#### objUser ==>'+objUser+'#### role name==>:'+objUser.UserRole.Name);
                                instalWrp.salesreps.add( generateUserWrapper(objUser,(objUser.UserRole.Name.contains('User'))?'User':'Manager') );
                            }
                        }   
                    }
                    if(instalWrp.isChild == false)
                        tempInstalWrp = instalWrp.clone();
                    
                    system.debug('#####tempInstalWrp#'+tempInstalWrp);
                    
                    if(instalWrp.isChild)
                        unifiedBean.installers.add(instalWrp);
                    
                }
                if(null != unifiedBean.installers && !unifiedBean.installers.isEmpty())
                {
                    if(null != tempInstalWrp)
                        unifiedBean.installers.add(0,tempInstalWrp);
                }else{
                    unifiedBean.installers.add(instalWrp);
                }
                 
            }/*else{
                // user not have reportees

                instalWrp.executives.add(generateUserWrapper(userRec,'Executive'));
            }*/
            // logged in user is not executive and is not reporting to any user
        }else if(!userRec.UserRole.Name.contains('Executive') && userRec.contact.ReportsToId==null){
            installerIdSet.add(userRec.contact.AccountId);
            installerAccUserMap = getUserList(userRec.contactId,installerIdSet );
            // user has reportees
            if(!installerAccUserMap.isEmpty()){
                UnifiedBean.ManagerWrapper mangWrap = new UnifiedBean.ManagerWrapper();
                mangWrap.hasReportsTo  = new List<UnifiedBean.UserWrapper>();
                mangWrap.userId=userRec.id;
                mangWrap.name=userRec.Name;
                mangWrap.role=(userRec.UserRole.Name.contains('User'))?'User':'Manager';
                instalWrp.managers.add(mangWrap);

                instalWrp.name=userRec.contact.Account.name;
                instalWrp.installerId=userRec.contact.AccountId;
                instalWrp.isChild=false;
                for(Id instlrId:installerAccUserMap.keySet()){
                    for(User objUser:installerAccUserMap.get(instlrId)){
                         mangWrap.hasReportsTo.add(generateUserWrapper(objUser,(objUser.UserRole.Name.contains('User'))?'User':(objUser.UserRole.Name.contains('Executive'))?'Executive':'Manager'));
                    }
                }
                
            }else{
                // user not have reportees
                 instalWrp.name=userRec.contact.Account.name;
                 instalWrp.installerId=userRec.contact.AccountId;
                 instalWrp.isChild=false;
                instalWrp.salesreps.add(generateUserWrapper(userRec,(userRec.UserRole.Name.contains('User'))?'User':'Manager'));
                
                System.debug('#### unifiedBean==>:'+instalWrp);
            }
            unifiedBean.installers.add(instalWrp);
        }else if( !userRec.UserRole.Name.contains('Executive') && userRec.contact.ReportsToId!=null){
            instalWrp.name=userRec.contact.Account.name;
            instalWrp.installerId=userRec.contact.AccountId;
            instalWrp.isChild=false;
            // logged in user is not executive and is reporting to some user
            instalWrp.salesreps.add(generateUserWrapper(userRec,(userRec.UserRole.Name.contains('User'))?'User':'Manager'));
            System.debug('#### unifiedBean==>:'+instalWrp);
            unifiedBean.installers.add(instalWrp);
        }

        //return instalWrp;
    }
    /******************************************************
     * @ Method     : 
     * @ Return Type:
     * @ Parameters :
*********************************************************/
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
        
    }
    
    /********************************************************************************
* Description: this method helps to get list of users based on logged in user / requested userId
* @param  reportsToId     
* @param installerId        
* return list of users based on logged in user / requested userId.
*********************************************************************************/
    public static Map<Id,List<User>> getUserList(string reportsToId, Set<Id> installerIdSet){
        Map<Id,List<User>> installerUserMap=new Map<Id,List<User>>();
        List<User> lstUsers = new List<User>();
        string userQuery= 'SELECT id,Name,contactId,UserRoleId,UserRole.Name,contact.AccountId,contact.ReportsToId,contact.Account.name FROM User WHERE ';
        
        if(string.isNotBlank(reportsToId) && installerIdSet.size()>0){
            userQuery+='contact.ReportsToId = :reportsToId AND  contact.AccountId IN:installerIdSet AND isActive=true';
        }
        if(installerIdSet.size()>0 && string.isBlank(reportsToId)){
            userQuery+='contact.AccountId IN:installerIdSet AND isActive=true';
        }
        userQuery+=' Order By contact.Account.name';
        system.debug('#### userQuery-->:'+userQuery);
        lstUsers =Database.Query(userQuery);
        if(lstUsers.size()>0){
            for(User userRec:lstUsers){
                List<User> tempLstUsers = new List<User>();
                tempLstUsers = installerUserMap.containsKey(userRec.contact.AccountId)?installerUserMap.get(userRec.contact.AccountId):tempLstUsers;
                 tempLstUsers.add(userRec);
                  installerUserMap.put(userRec.contact.AccountId,tempLstUsers);
                
            }
            for(User objUser:lstUsers){
                if(objUser.contact.ReportsToId!=null){
                    List<User> tempUserList = new List<User>();
                    tempUserList = userReporterMap.containsKey(objUser.contact.ReportsToId)?userReporterMap.get(objUser.contact.ReportsToId):tempUserList;
                    tempUserList.add(objUser);
                    userReporterMap.put(objUser.contact.ReportsToId,tempUserList);
                }
            }
        }
        
        return installerUserMap;
    }
    /******************************************************
     * @ Method     : 
     * @ Return Type:
     * @ Parameters :
*********************************************************/
    public static UnifiedBean.UserWrapper generateUserWrapper(User objUserRec,string userRole){
        UnifiedBean.UserWrapper userWrp= new UnifiedBean.UserWrapper();
        userWrp.userId=objUserRec.id;
        userWrp.name=objUserRec.Name;
        userWrp.role=userRole;
        
        return userWrp;
    }
}