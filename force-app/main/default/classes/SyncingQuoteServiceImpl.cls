/**
* Description: Pricing Calculation related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Raviteja           09/16/2017          Created
******************************************************************************************/
public with sharing class SyncingQuoteServiceImpl extends RESTServiceBase{  
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back with data and response status code.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + SyncingQuoteServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        SyncingQuoteDataServiceImpl syncingQuoteDataSrvImpl = new SyncingQuoteDataServiceImpl(); 
        try{ 
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            //CreateProjectRequestWrapper reqData = (CreateProjectRequestWrapper)JSON.deserialize(rw.reqDataStr, CreateProjectRequestWrapper.class);
            res.response = syncingQuoteDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
           system.debug('### SyncingQuoteServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' SyncingQuoteServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + SyncingQuoteServiceImpl.class);
        return res.response ;
    }
}