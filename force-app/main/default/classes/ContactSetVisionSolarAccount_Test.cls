@istest
public class ContactSetVisionSolarAccount_Test 
{
    public static testmethod void test () 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        Account a1 = new Account (Name = 'Test Account 1', FNI_Domain_Code__c = 'Test FNI',Installer_Legal_Name__c='Bright planet Solar',Solar_Enabled__c=true);
        insert a1;

        Contact c1 = new Contact (LastName = 'Test', AccountId = a1.Id);
        insert c1;
    }
}