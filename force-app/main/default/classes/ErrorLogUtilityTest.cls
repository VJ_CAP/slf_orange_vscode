/******************************************************************************************
    * Description: Test class to cover ErrorLogUtility
    *
    * Modification Log : 
    ---------------------------------------------------------------------------
    Developer               Date               Description
    ---------------------------------------------------------------------------
    Adithya              01/22/2017            Created
    ******************************************************************************************/
    @isTest
    public class ErrorLogUtilityTest { 
         private testMethod static void errorLogUtilityTestMethod(){    
    
            SLFUtilityTest.createCustomSettings();
            ErrorLogUtility.writeLog('test',1,'Test','Testing','null pointer exception',''); 
            
            List<SObject> accLst = new List<SObject>();
            Account acc = new Account();
            acc.name = 'Test Account';
            acc.M0_Required__c ='Communications';
            acc.M1_Required__c ='Communications';
            acc.M2_Required__c ='Communications';
            acc.BillingStateCode = 'CA';
            acc.Installer_Legal_Name__c='Bright Solar Planet';
            acc.Solar_Enabled__c = true; 
            insert acc;
            accLst.add(acc);
               
            List<Database.UndeleteResult> unDeleteResultSRS = new List<Database.UndeleteResult>{}; 
            unDeleteResultSRS = Database.unDelete(accLst,false);
            ErrorLogUtility.writeUnDeleteLog(unDeleteResultSRS);
            
            List<Database.DeleteResult> DeleteResultSRS = new List<Database.DeleteResult>{}; 
            DeleteResultSRS = Database.delete(accLst,false);
            ErrorLogUtility.writeDeleteLog(DeleteResultSRS );
            
            List<Database.UpsertResult> upsertResultSRS = new List<Database.UpsertResult>{}; 
            upsertResultSRS = Database.upsert(new List<Account>{acc},false);
            ErrorLogUtility.writeUpsertLog(upsertResultSRS);
            
            List<Database.SaveResult> insertSRS = new List<Database.SaveResult>{};  
            insertSRS= Database.insert(accLst,false);
            ErrorLogUtility.writeInsertLog(insertSRS); 
            ErrorLogUtility.writeUpdateLog(insertSRS);
            
            ErrorLogUtility.writeFutureLog('test',1,'Test','Testing','null pointer exception'); 
            
            //Covering Try Catch Block
            ErrorLogUtility.writeInsertLog(null);
            ErrorLogUtility.writeUpdateLog(null);        
            ErrorLogUtility.writeUpsertLog(null);
            ErrorLogUtility.writeDeleteLog(null);
            ErrorLogUtility.writeUnDeleteLog(null);
         }
       
    }