@IsTest
public class jsonSightenExtraQuoteInfoResponse_Test {
    
    static testMethod void testParse() {
         String json = '{'+
        '  \"messages\": {'+
        '    \"error\": [],'+
        '    \"info\": [],'+
        '    \"warning\": [],'+
        '    \"critical\": []'+
        '  },'+
        '  \"status_code\": 200,'+
        '  \"data\": [{'+
        '    \"results\": {'+
        '      \"monthly_payment_no_prepay\": 138.14,'+
        '      \"monthly_payment\": 96.40999999999991,'+
        '      \"ach_elected\": true,'+
        '      \"stub_payment_no_prepay\": 139.44'+
        '    }'+
        '  }]'+
        '}';
        jsonSightenExtraQuoteInfoResponse obj = jsonSightenExtraQuoteInfoResponse.parse(json);
        System.assert(obj != null);
        
    }
}