public with sharing class DataValidator 
{
    public class Input 
    {
        public list<InputObject> inputObjects;

        //  *************************

        public Input (list<InputObject> inputObjects)
        {
            this.inputObjects = inputObjects;
        }
    }

//  **************************************************************

    public class InputObject
    {
        public sObject obj;
        public String variant;

        //  *************************

        public InputObject (SObject obj, String variant)
        {
            this.obj = obj;
            this.variant = variant;
        }

        //  *************************

        public InputObject (SObject obj)
        {
            this.obj = obj;
            this.variant = null;
        }
    }

//  **************************************************************

    public class Output
    {
        public Boolean errors;
        public String errorMessage;
        public list<OutputObject> outputObjects;

        //  *************************

        public Output ()
        {
            this.errors = false;
            this.outputObjects = new list<OutputObject>{};
        }
    }

//  **************************************************************

    public class OutputObject
    {
        public String objectType;
        public String variant;
        public String errorMessage;
        public Boolean errors;
        public map<String,String> fieldErrorsMap;
        public map<String,Field_Validation__mdt> FVObjectMap;

        //  *************************

        public OutputObject (String objectType, String variant)
        {
            this.objectType = objectType;
            this.variant = variant;
            this.errors = false;
            this.fieldErrorsMap = new map<String,String>{};
            this.FVObjectMap = new map<String,Field_Validation__mdt>();
        }
    }

//  **************************************************************

    //  To validate a single object with a variant

    public static OutputObject validateObject (InputObject ipo)
    {
        list<InputObject> inputObjects = new list<InputObject>{ipo};
        return validateObjects (new Input (inputObjects)).outputObjects[0];
    }

//  **************************************************************

    //  To validate a single object with no variant

    public static OutputObject validateObject (SObject o)
    {
        list<InputObject> inputObjects = new list<InputObject>{new InputObject (o,null)};
        return validateObjects (new Input (inputObjects)).outputObjects[0];
    }

//  **************************************************************

    //  To validate a list of objects with no variants

    public static Output validateObjects (list<SObject> objects)
    {
        list<InputObject> inputObjects = new list<InputObject>{};

        for (SObject o : objects)
        {
            inputObjects.add (new InputObject (o, null));
        }
        return validateObjects (new Input (inputObjects));
    }

//  **************************************************************

    //  to validate a list of input objects with variants and return an Output

    public static Output validateObjects (list<InputObject> inputObjects)
    {
        return validateObjects (new Input (inputObjects));
    }

//  **************************************************************

    //  the most complex generic version of the method, called by the others, does all the work

    public static Output validateObjects (Input ip)
    {
        Output op = new Output ();

        set<String> objectTypes = new set<String>{};

        for (InputObject ipo : ip.inputObjects)
        {
            objectTypes.add (String.valueOf(ipo.obj.getSObjectType()));
        }

        map<String,Object_Validation__mdt> objectValidationsMap = new map<String,Object_Validation__mdt>{};

        for (Object_Validation__mdt ov : [SELECT Id, DeveloperName, Object_Type__c, Variant__c,
                                            (SELECT Id, DeveloperName, API_Name__c, Display_Name__c, Type__c, Other_Value__c, Error_Message__c,
                                                Maximum_Length__c, Required__c,
                                                Field_Validation_Rule__r.DeveloperName,
                                                 Field_Validation_Rule__r.Regex__c, Field_Validation_Rule__r.Error_Message__c
                                                FROM Field_Validations__r WHERE Active__c =: true)   //Srikanth - added Active__c condition to exclude inactive rules
                                            FROM Object_Validation__mdt
                                            WHERE Object_Type__c IN :objectTypes])
        {
            objectValidationsMap.put (ov.Object_Type__c + '|' + ov.Variant__c, ov);
        }

        //   now validate

        for (InputObject ipo : ip.inputObjects)
        {
            SObject o = ipo.obj;

            OutputObject objectOut = new OutputObject (String.valueOf(o.getSObjectType()), ipo.variant);

            Object_Validation__mdt ov = objectValidationsMap.get (objectOut.objectType + '|' + objectOut.variant);
            
            if (ov == null)
            {
                objectOut.errorMessage = 'Variant not found: ' + objectOut.objectType + '|' + objectOut.variant;
                objectOut.errors = true;
                op.errors = true;
                op.OutputObjects.add (objectOut);
                continue;
            }

            for (Field_Validation__mdt fv : ov.Field_Validations__r)
            {
                Boolean match;
                if (fv.Type__c == 'Picklist')
                {
                    validatePicklist (fv, o, objectOut);
                }

                if (fv.Type__c == 'MultiPicklist')
                {
                    validateMultiPicklist (fv, o, objectOut);
                }

                if (fv.Type__c == 'Text' || fv.Type__c == 'Number' || fv.Type__c == 'Date')
                {
                    validateText (fv, o, objectOut);
                }
            }
            op.OutputObjects.add (objectOut);

            if (objectOut.errors == true) op.errors = true;
        }
        return op;
    }

//  **************************************************************

    private static void validatePicklist (Field_Validation__mdt fv, SObject o, OutputObject objectOut)
    {
        Schema.DescribeSObjectResult dsor = o.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
        Schema.DescribeFieldResult dr = objectFields.get(fv.API_Name__c).getDescribe();

        Boolean nullAllowed = dr.isNillable();

        if (dr.getType() != Schema.DisplayType.Picklist)
        {
            objectOut.errors = true;
            //Dax Change
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'Field is not a picklist');
        }

        List<Schema.PicklistEntry> ple = dr.getPicklistValues();

        Boolean match = false;

        String fieldValue = (String) o.get(fv.API_Name__c);

        set<String> picklistValues = new set<String>{};

        for (Schema.PicklistEntry f : ple)
        {
            picklistValues.add (f.getValue());
        }

        if (picklistValues.contains (fieldValue)) match = true;

        if (fieldValue == null && nullAllowed) match = true;

        if (match == false)
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'Picklist value not allowed: (' + fieldValue + ')');
        }

        if (fieldValue == null && fv.Required__c == true) 
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'A non-null Picklist value is required');
        }
    }

//  **************************************************************

    private static void validateMultiPicklist (Field_Validation__mdt fv, SObject o, OutputObject objectOut)
    {
        Schema.DescribeSObjectResult dsor = o.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
        Schema.DescribeFieldResult dr = objectFields.get(fv.API_Name__c).getDescribe();

        if (dr.getType() != Schema.DisplayType.MultiPicklist)
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'Field is not a multi-picklist');
        }

        List<Schema.PicklistEntry> ple = dr.getPicklistValues();

        Boolean match = true;

        String fieldValue = (String) o.get(fv.API_Name__c);

        if (fieldValue != null)
        {
            list<String> fieldValues = fieldValue.split (';');

            set<String> picklistValues = new set<String>{};

            for (Schema.PicklistEntry f : ple)
            {
                picklistValues.add (f.getValue());
            }

            for (String s : fieldValues)
            {
                if (picklistValues.contains (s) == false)
                {
                    match = false;
                    break;
                }
            }
        }

        if (match == false)
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'One or more multi-picklist values not allowed: (' + fieldValue + ')');
        }

        if (fieldValue == null && fv.Required__c == true) 
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'A non-null multi-picklist value is required');
        }
    }

//  **************************************************************

    private static void validateText (Field_Validation__mdt fv, SObject o, OutputObject objectOut)
    {
        String fieldValue = null;

        if (fv.Type__c == 'Text')
        {
            fieldValue = (String) o.get(fv.API_Name__c);
        }

        if (fv.Type__c == 'Number')
        {
            fieldValue = String.valueOf ( (Decimal) o.get(fv.API_Name__c));
        }
        
        if(fv.Type__c == 'Date')
        {
            if(null != o.get(fv.API_Name__c))
            {
                Date d = (Date)o.get(fv.API_Name__c);
                fieldValue = DateTime.newInstance(d.year(),d.month(),d.day()).format('yyyy-MM-dd');
            }
        }
        
        String regex = fv.Field_Validation_Rule__r.Regex__c;
        if (fv.Field_Validation_Rule__r.DeveloperName == 'Other')
        {
            regex = fv.Other_Value__c;
        }

        String errorMessage = (fv.Error_Message__c != null ? fv.Error_Message__c : fv.Field_Validation_Rule__r.Error_Message__c) +
                                    ' (' + regex + ')';

        Boolean match = true;
        if (fieldValue != null && regex != null) match = Pattern.matches (regex, fieldValue);

        if (match == false)
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, errorMessage);
            objectOut.FVObjectMap.put(fv.Display_Name__c, fv);
        }

        if (fieldValue == null && fv.Required__c == true) 
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'A non-null value is required');
        }

        if (fv.Maximum_Length__c != null && fieldValue != null && fieldValue.length() > fv.Maximum_Length__c) 
        {
            objectOut.errors = true;
            objectOut.fieldErrorsMap.put (fv.Display_Name__c, 'Field value is too long, max: ' + fv.Maximum_Length__c);
        }
    }

//  **************************************************************

    public static Date validateDate (String inputDate)
    {
        if (inputDate.length() != 10) return null;

        list<String> parts = inputDate.split('-');

        if (parts.size() != 3) return null;

        Date d;

        try
        {
            Integer yyyy = Integer.valueOf(parts[0]);
            Integer mm = Integer.valueOf(parts[1]);
            Integer dd = Integer.valueOf(parts[2]);

            if (mm < 1 || mm > 12) return null;
            if (dd < 1 || dd > 31) return null;

            d = Date.newInstance (yyyy, mm, dd);
        }
        catch (Exception e)
        {
            return null;
        }
        return d;
    }
}