/**
* Description: Test class for UnderwritingFileTrg trigger and UnderwritingFileTriggerHandler class. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/27/2017          Created
******************************************************************************************/
@isTest
public class UnderwritingFileTriggerTest {
    /**
    * Description: Test method to cover UnderwritingFileTriggerHandler funtionality.
    */
    public testMethod static void underwritingFileTest()
    {        
        //get the person account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        insert trgAccflag;
        TriggerFlags__c trgAccflag1 = new TriggerFlags__c();
        trgAccflag1.Name ='Task';
        trgAccflag1.isActive__c =true;
        insert trgAccflag1;
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.Push_Endpoint__c ='www.test.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.M0_Split__c = 60;
        acc.M1_Split__c = 20;
        acc.M2_Split__c = 20;
        acc.Subscribe_to_Push_Updates__c = true;
        insert acc;
        
        //insert custom setting TriggerFlag records.
        //start
        SLFUtilityTest.createCustomSettings();
        //stop
        Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            insert prod;
        
        set<Id> opppIdSet = new set<Id>();
        list<Opportunity> oppLst = new List<Opportunity>();
        for(Integer i=0; i<6; i++) 
        {
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.SLF_Product__c = prod.id;
            opp.Install_State_Code__c = 'CA';
            oppLst.add(opp);
            
            
        }
        insert oppLst;
        
        System_Design__c objSdesign = new System_Design__c();
        objSdesign.Opportunity__c = oppLst[0].id;
        insert objSdesign;
        
        SLF_Credit__c credit = new SLF_Credit__c();
        credit.Opportunity__c = oppLst[0].id;
        insert credit;
        
        for(Opportunity oppIterate : oppLst) 
        {
            opppIdSet.add(oppIterate.Id);
        }

        //Insert Quote Record
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = oppLst[0].id;
        //quoteRec.SLF_Product__c = prod.Id ;
        //quoteRec.Accountid =acc.id;
        insert quoteRec;
        
        oppLst[0].SyncedQuoteId =  quoteRec.Id;
        update oppLst[0];
        
         //Insert System Design Record
        System_Design__c sysDesignRec = new System_Design__c();
        //sysDesignRec.Name = 'Test Sys';
        sysDesignRec.System_Cost__c = 11;
        sysDesignRec.Opportunity__c = oppLst[0].Id;
        sysDesignRec.Est_Annual_Production_kWh__c = 10;
        sysDesignRec.Inverter_Count__c = 11;
      //  sysDesignRec.Inverter_Make__c = '11';
        sysDesignRec.Inverter_Model__c = '12';
        //sysDesignRec.Inverter__c = 'TBD';
        sysDesignRec.Module_Count__c = 10;
        sysDesignRec.Module_Model__c = '11';
        //sysDesignRec.System_Size_STC_kW__c = 12;
        insert sysDesignRec;
        
        quoteRec.System_Design__c = sysDesignRec.Id;
        update quoteRec;
        
        Underwriting_File__c underWrFilRec0 = new Underwriting_File__c();
            underWrFilRec0.name = 'Underwriting File';
            underWrFilRec0.ACT_Review__c ='Yes';
            underWrFilRec0.Opportunity__c = oppLst[0].Id;
            underWrFilRec0.All_Installers_M2_Queue__c = 'Yes';
            underWrFilRec0.Title_Review_Complete_Date__c = system.today();
            underWrFilRec0.Install_Contract_Review_Complete__c = true;
            underWrFilRec0.Utility_Bill_Review_Complete__c = true;
            underWrFilRec0.LT_Loan_Agreement_Review_Complete__c = true;
            underWrFilRec0.Installation_Photos_Review_Complete__c = true;
            underWrFilRec0.Final_Design_Review_Complete__c = true;   
            underWrFilRec0.Funding_status__c = 'Under Review by Sunlight Financial';
            underWrFilRec0.Project_Status__c = 'M0';
            underWrFilRec0.ST_Reapproved__c = System.now();
            underWrFilRec0.LT_Reapproved__c = System.now();
            insert underWrFilRec0;
            UnderwritingFileTriggerHandler.isTrgExecuting = true;
            update underWrFilRec0;
        
        List<Underwriting_File__c> underWrFilInsertLst = new List<Underwriting_File__c>();
        test.startTest();
        
        
        
        //System.assertEquals(true, UnderwritingFileTriggerHandler.runOnce());
        
        for(Integer i=0; i<6; i++) 
        {
            Underwriting_File__c underWrFilRec = new Underwriting_File__c();
            underWrFilRec.name = 'Underwriting File'+i;
            
           // underWrFilRec.Box_Installation_Photos_TS__c = system.Today();
         //   underWrFilRec.Box_Approval_Letters_TS__c = system.Today();
         //   underWrFilRec.Box_Utility_Bills_TS__c = system.Today();
          //  underWrFilRec.Box_Payment_Election_Form_TS__c = system.Today();
            
            underWrFilRec.ACT_Review__c ='Yes';
            underWrFilRec.Opportunity__c = oppLst[0].Id;
            underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
            underWrFilRec.Title_Review_Complete_Date__c = system.today();
            underWrFilRec.Install_Contract_Review_Complete__c = true;
            underWrFilRec.Utility_Bill_Review_Complete__c = true;
            underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
            underWrFilRec.Installation_Photos_Review_Complete__c = true;
            //underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
            //underWrFilRec.Final_Design_Document_Received__c = system.today();
            underWrFilRec.Final_Design_Review_Complete__c = true;   
           
            underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
            underWrFilRec.Project_Status__c = 'M0';
            underWrFilRec.ST_Reapproved__c = System.now();
            underWrFilRec.LT_Reapproved__c = System.now();
            if(i == 0)
            {
                underWrFilRec.Project_Status__c = 'M0';
                //underWrFilRec.Box_Loan_Agreements_TS__c = system.Today();
                underWrFilRec.M0_Approval_Requested_Date__c = system.Today();               
            }
            if(i == 1)  {
                underWrFilRec.Project_Status__c = 'M1';
               // underWrFilRec.Box_Communications_TS__c = system.Today();
                underWrFilRec.M1_Stipulations__c=false;
                underWrFilRec.M1_Approval_Requested_Date__c = system.Today();
            }
            
            if(i == 2)
                underWrFilRec.Project_Status__c = 'M2';
               //  underWrFilRec.Box_Government_IDs_TS__c = system.Today();
                //underWrFilRec.M2_Approval_Requested_Date__c = system.Today();
                 
            if(i == 3)
                underWrFilRec.Project_Status__c = 'Project Completed';
             //   underWrFilRec.Box_Final_Designs_TS__c = system.Today();
            if(i == 4)
                underWrFilRec.Project_Status__c = 'Declined';
             if(i == 5)
             {
                underWrFilRec.Opportunity__c = oppLst[4].Id; 
                underWrFilRec.Project_Status__c = 'M0';
                
              
                //underWrFilRec.M0_Approval_Requested_Date__c = null;
                
             }
             //   underWrFilRec.Box_Install_Contracts_TS__c = system.Today();
            //if(i == 4)
           //     underWrFilRec.Project_Status__c = '';
            //    underWrFilRec.Box_Install_Contracts_TS__c = system.Today();
                
            underWrFilInsertLst.add(underWrFilRec);
        }
        
        insert underWrFilInsertLst;
        
        //underWrFilInsertLst[0].ST_Reapproved__c = system.Today()+2;
        underWrFilInsertLst[0].LT_Reapproved__c = system.Today()+2;
        underWrFilInsertLst[0].M1_Approval_Requested_Date__c = System.now().addDays(1);
        underWrFilInsertLst[0].ST_Reapproved__c = underWrFilInsertLst[0].ST_Reapproved__c.addDays(10);
        UnderwritingFileTriggerHandler.isTrgExecuting = true;
        update underWrFilInsertLst[0];
        
        List<Task>  lstT = new List<Task>();
        Map<Id, Underwriting_File__c> newUnderwritingMap = new Map<Id, Underwriting_File__c>();
        for(Underwriting_File__c underWrFilIterate : underWrFilInsertLst){
            newUnderwritingMap.put(underWrFilIterate.Id,underWrFilIterate);
            Task objT = new Task();
            objT.WhatId = underWrFilIterate.Id;
            objT.Subject = 'Open';
            objT.Type = 'M1 Review';
            objT.Status = 'Open';
            lstT.add(objT);
            Task objT1 = new Task();
            objT1.WhatId = underWrFilIterate.Id; 
            objT1.Subject = 'Open';
            objT1.Type = 'M0 Review';
            objT1.Status = 'Open';
            lstT.add(objT1);
            Task objT2 = new Task();
            objT2.WhatId = underWrFilIterate.Id;
            objT2.Subject = 'Open';
            objT2.Type = 'Kitting Review';
            objT2.Status = 'Open';
            lstT.add(objT2);
            Task objT3 = new Task();
            objT3.WhatId = underWrFilIterate.Id; 
            objT3.Subject = 'Open';
            objT3.Type = 'Inspection Review';
            objT3.Status = 'Open';
            lstT.add(objT3);
            Task objT4 = new Task();
            objT4.WhatId = underWrFilIterate.Id; 
            objT4.Subject = 'Open';
            objT4.Type = 'Permit Review';
            objT4.Status = 'Open';
            lstT.add(objT4);
        }
        if(!lstT.isEmpty()){
            insert lstT;
        }
        UnderwritingFileTriggerHandler underWritingHandler = new UnderwritingFileTriggerHandler(true);
        underWritingHandler.updateProjDetailOnUW(underWrFilInsertLst,opppIdSet);
        UnderwritingFileTriggerHandler.isTrgExecuting = true;
       // underWritingHandler.onAfterUpdate(newUnderwritingMap,newUnderwritingMap);
       // underWritingTrg.generateJSONBody(underWrFilUpdateLst);
        
        List<Underwriting_File__c> underWrFilUpdateLst = new List<Underwriting_File__c>();
        for(Integer i=0; i<5; i++)
        {
            
            Underwriting_File__c underWrFilRec = underWrFilInsertLst[i];
            // underWrFilRec.LT_Loan_Agreement_Received__c = system.today()+1;
            if(i == 0)
                underWrFilRec.Project_Status__c = 'Declined';
            
            if(i == 1)
                underWrFilRec.Project_Status__c = 'Project Completed';
            
            if(i == 2)
                underWrFilRec.Project_Status__c = 'M1';
            
            if(i == 3)
                underWrFilRec.Project_Status__c = 'M2';
            
            if(i == 4)
                underWrFilRec.Project_Status__c = 'M0';
            
            underWrFilRec.Funding_status__c = 'M1 Complete';
            underWrFilUpdateLst.add(underWrFilRec);
           
            
        }
        try {
            //insert lstT;
            update underWrFilUpdateLst;
        } catch(Exception e) {
            system.assertEquals(e.getMessage(), e.getMessage());        
        }   
        
        Underwriting_File__c underWrFilRec = new Underwriting_File__c();
        underWrFilRec.Opportunity__c = oppLst[0].Id;
        underWrFilRec.M0_Stipulations__c = True;
        UnderwritingFileTriggerHandler underWritingTrg = new UnderwritingFileTriggerHandler(true); 
        underWritingTrg.OnBeforeInsert(new List<Underwriting_File__c>{underWrFilRec});
        
        UnderwritingFileTriggerHandler.runonce();
        UnderwritingFileTriggerHandler.isTrgExecuting = false;
        UnderwritingFileTriggerHandler.runonce();
        
        test.stopTest();
    }   
}