@RestResource(urlMapping='/GetCredit/*')
global with sharing class RestControllerCredit {
    @httpGet
    global static String getCredit(){
        String opptyId = RestContext.request.params.get('opportunity');
        System.debug('***Debug: oppty id: ' + opptyId);
        System.debug('***Debug: all oppties: ' + [SELECT Id, SLF_Product__c, SyncedQuoteId, Customer_has_authorized_credit_hard_pull__c FROM Opportunity]);
        Opportunity oppty = [SELECT Id, SLF_Product__c, SyncedQuoteId, Customer_has_authorized_credit_hard_pull__c FROM Opportunity where Id = :opptyId];
        
        //todo, add validation of required fields
        if(oppty != null){
            String exceptionMsg = '';
            if(oppty.Customer_has_authorized_credit_hard_pull__c == false) exceptionMsg = 'Customer must authorize Credit Pull\n';
            if(oppty.SyncedQuoteId == null){
            	if(oppty.SLF_Product__c == null) exceptionMsg += 'A product or quote must be selected to run credit\n';    
            }else{
                if(oppty.SLF_Product__c == null) exceptionMsg += 'The quote selected does not have a valid Sunlight product associated, please check Sighten or contact Sunlight Financial\n';   
            }
                
            if(exceptionMsg == ''){
                 getCreditQueueable cGet = new getCreditQueueable(oppty.Id);
        		//Database.executeBatch(qGet,1);
                ID jobID = System.enqueueJob(cGet);
                
               //while(){
                    AsyncApexJob jobInfo = [SELECT Status,NumberOfErrors FROM AsyncApexJob WHERE Id=:jobID];
                //}
                
                return jobInfo.Status;
                //fniCalloutManual.buildHardCreditRequest(oppty.Id);
                //return [SELECT Id, Application_Status__c FROM Opportunity where Id = :opptyId];
            }else{           
                throw new restException(exceptionMsg);
            }
            
        }else{
          //  List<String> retString = new List<String>{'No Opportunity Found'};
         //   return retString;
            System.debug('***Debug: invalid id: ' + opptyId);
            
        }
        
        return null;
        
    }

}