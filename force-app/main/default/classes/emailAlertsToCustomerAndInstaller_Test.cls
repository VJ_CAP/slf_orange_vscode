@isTest
public class emailAlertsToCustomerAndInstaller_Test {
    public testMethod static void testtriggerEmail(){
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();          
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag); 
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);  
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        TriggerFlags__c trgAccflagCredit = new TriggerFlags__c();
        trgAccflagCredit .Name ='SLF_Credit__c';
        trgAccflagCredit .isActive__c =true;
        trgLst.add(trgAccflagCredit );
        
        TriggerFlags__c trgAccflagUser = new TriggerFlags__c();
        trgAccflagUser.Name ='User';
        trgAccflagUser.isActive__c =true;
        trgLst.add(trgAccflagUser);
        
        insert trgLst;
        
        Profile p=[SELECT id FROM Profile WHERE Name='Partner Community User for Salesforce1' LIMIT 1];
        User u=[SELECT id,name FROM User WHERE ProfileId=:p.id LIMIT 1];
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();  
        
         Account Acc = new Account();
            Acc.name = 'Test Account';
            Acc.Push_Endpoint__c = 'https://requestb.in';
            Acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            Acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            Acc.BillingStateCode = 'CA';
            Acc.Installer_Email__c = 'test@slf.com';
            Acc.FNI_Domain_Code__c = 'Solar';
            Acc.Installer_Legal_Name__c='Bright Solar Planet';
            Acc.Subscribe_to_Push_Updates__c = true;
            Acc.Domain_list__c = 'Test';
            Acc.Solar_Enabled__c = true; 
            Acc.ACH_true__c = 'X';
            Acc.Requires_ACH_Validation__c = True;
            Acc.Routing_Number__c = '012345678';
            Acc.LexisNexis_IDLookupResult__c = 'Passed';
            Acc.LexisNexis_IDQuestionsResult__c = 'Failed';
            Acc.Stip_Email_1__c = 'teststipemail1@gmail.com';
            acc.Stip_Email_2__c = 'teststipemail2@gmail.com';
            acc.Stip_Email_3__c = 'teststipemail3@gmail.com';
            acc.Operations_Email__c = 'testOpsemail@gmail.com';
            insert Acc;
      
       Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;
      
      map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
      
      User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
      
        //Insert Account record
        Account personAcc = new Account();
        personAcc.firstname = 'Financial';
        personAcc.lastname = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        //personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        //personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        //personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.PersonEmail = 'test@gmail.com';
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
          
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        personAcc.PersonEmail = 'test@gmail.com';
        update personAcc;
          
        Opportunity testOppty = new Opportunity();
        testOppty.AccountId = personAcc.Id;
        //testOppty.Co_Applicant__c = acc.Id;
        testOppty.Name = 'testOppty';
        testOppty.StageName = 'Closed Won';
        testOppty.CloseDate = Date.today();
        testOppty.Install_City__c = 'Oakland';
        testOppty.Install_State_Code__c = 'AL';
        testOppty.Install_Postal_Code__c = '94611';
        testOppty.Install_Street__c = '1900 Magellan Dr';
        testOppty.Installer_Account__c = acc.id;    
        testOppty.Sighten_Product_UUID__c = '1';
        testOppty.Sales_Representative_Email__c = 'test11@leancog.com';
        testOppty.Create_in_Sighten__c = true;
        testOppty.Combined_Loan_Amount__c = 240;
        testOppty.Email_Application_Request__c = true;
        testOppty.Application_Status__c = 'New';
        testOppty.Routing_Number_Validated__c = false;
        testOppty.isACH__c = true;
        testOppty.Dealer_Terms_Accepted__c = true;
        testOppty.Account_Numbers_Match__c = false;
        //testOppty.Primary_Applicant_Email__c ='Closed Won';
        testOppty.Change_Order_Status__c = 'Active';
        testOppty.Credit_Expiration_Date__c = system.today();
        testOppty.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Customer Opportunity Record Type').getRecordTypeId();
        insert testOppty;
        
        List<Underwriting_File__c> undFileList = new List<Underwriting_File__c>();
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = testOppty.id;
        undStpbt.Approved_for_Payments__c = true;      
        undFileList.add(undStpbt);  
        insert undFileList;
        undStpbt.Withdraw_Reason__c = 'Customer request';
        undStpbt.Withdraw_Reason_Detail__c = 'Test';
        update undStpbt;
        
        List<Opportunity> optyList=new List<Opportunity>();
        optyList.add(testOppty);
        
        Test.startTest();
        emailAlertsToCustomerAndInstaller sendEmail=new emailAlertsToCustomerAndInstaller();
        emailAlertsToCustomerAndInstaller.triggerEmail(optyList);
        
        Test.stopTest();
        
    }
    
}