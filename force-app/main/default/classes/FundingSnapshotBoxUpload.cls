/**
* Description: To generate and upload funding snapshot to Box.com
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh               01/03/2019            Created
******************************************************************************************/
global class FundingSnapshotBoxUpload implements Database.Batchable<sObject>,Schedulable,Database.Stateful,Database.AllowsCallouts {
    global String strCQuery = '';
    global String boxAccessTokan = '';
    global Map<String,String> mapMedMapping = new Map<String,String>();
    global Map<String,Boolean> mapDateMapping = new Map<String,Boolean>();
    global Map<id,Funding_Snapshot__c> accSnapMap = new Map<id,Funding_Snapshot__c>();
    
    global FundingSnapshotBoxUpload(Map<id,Funding_Snapshot__c> accSnapMap){
        
        List<Funding_Report_Mapping__mdt> fundingReportMapping = [Select id,DeveloperName,Section_Name__c,Section_Value__c,Column_Number__c, Datetime_Conversion__c from Funding_Report_Mapping__mdt where Section_Name__c != null and Section_Value__c != null order by Column_Number__c];
        for (Funding_Report_Mapping__mdt medCseMap : fundingReportMapping) {
                if(!strCQuery.contains(','+medCseMap.Section_Value__c)){
                    strCQuery = strCQuery + ',' + medCseMap.Section_Value__c;
                }
                mapMedMapping.put(medCseMap.Section_Name__c, medCseMap.Section_Value__c);
                mapDateMapping.put(medCseMap.Section_Name__c, medCseMap.Datetime_Conversion__c);
        }
        strCQuery = strCQuery.replaceFirst(',','');
        BoxPlatformApiConnection connection;

        try
        {
            connection = BoxAuthentication.getConnection();
            System.debug('#### test'+connection.accessToken);
            boxAccessTokan = connection.accessToken;
        }
        catch (Exception e)
        {
            
        }
        
        this.strCQuery = strCQuery;
        this.accSnapMap = accSnapMap;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
      ID facilityAccRectypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Facility').getRecordTypeId();
        return Database.getQueryLocator('select id, name,Facility_Picklist_Key__c from Account where RecordTypeId =: facilityAccRectypeid');
    }
    
    global void execute(Database.BatchableContext BC, List<Account> AccList){    
        system.debug('### Entered into execute() of '+ FundingSnapshotBoxUpload.class); 
        System.debug('accList'+accList);
        
        for(Account acc :accList){
            
            Funding_Snapshot__c snapshot = accSnapMap.get(acc.id);
            
            String generatedCSVFile = '';
            List<Funding_Snapshot_Line_Item__c> lineItems = new List<Funding_Snapshot_Line_Item__c>();
                String query = '';
                if(String.isNotBlank(strCQuery)){
                    query = 'SELECT '+ strCQuery +' from Funding_Snapshot_Line_Item__c where Funding_Snapshot__c =\'' + snapshot.id + '\'';  
                    System.debug('----query----'+query);
                    lineItems = database.query(query);
                }
                
             try{
                String fileRow = '';
                List<String> lstColHeaders = new List<String>();
                
                //if(!lineItems.isEmpty() && !mapMedMapping.isEmpty()){
                   if(!mapMedMapping.isEmpty()){ 
                    String fHeaderStr = '';
                    for(String fileHeader: mapMedMapping.keySet()){
                        fHeaderStr = fHeaderStr+','+fileHeader;
                        lstColHeaders.add(fileHeader);
                    }
                   fHeaderStr = fHeaderStr.replaceFirst(',', '');
                    generatedCSVFile  = generatedCSVFile + fHeaderStr + '\n';
                   
                    //}
                    System.debug('----lstColHeaders----'+lstColHeaders);
                    System.debug('----generatedCSVFile----'+generatedCSVFile);
                    for(Funding_Snapshot_Line_Item__c lineItem: lineItems) {
                        fileRow = '';
                        System.debug('----lineItem----'+lineItem);
                        
                            if(!lstColHeaders.isEmpty()){
                                
                                for(String colHeader : lstColHeaders){
                                    if(mapMedMapping.containsKey(colHeader) ){
                                        if(mapMedMapping.get(colHeader) != '' && mapMedMapping.get(colHeader) != null){
                                            String rowVal = '';
                                            System.debug('-@@---colHeader----'+colHeader);
                                            String mFHeader = mapMedMapping.get(colHeader);
                                            System.debug('---mFHeader---'+mFHeader);
                                            
                                            if(mFHeader.startsWithIgnoreCase('Underwriting_File__r.Opportunity__r.Account')){    
                                                //Added apostrophe for Account number and routing number as part of Orange-2036 - Start
                                                if((mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.Account.') == 'Routing_Number__c' || mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.Account.') == 'Account_Number__c') && (String)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').getSObject('Account').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.Account.')) != null){
                                                    //rowVal = '\''+(String)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').getSObject('Account').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.Account.'));
                                                    rowVal = '="'+(String)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').getSObject('Account').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.Account.'))+'"';
                                                }else{
                                                    rowVal = (String)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').getSObject('Account').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.Account.'));
                                                }
                                                //Added apostrophe for Account number and routing number as part of Orange-2036 - End
                                            }
                                            else if(mFHeader.startsWithIgnoreCase('Underwriting_File__r.Opportunity__r.SLF_Product__r')){
                                                if(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.SLF_Product__r.') == 'ACH__c'){
                                                    rowVal = String.valueOf((Boolean)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').getSObject('SLF_Product__r').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.SLF_Product__r.')));
                                                }else{
                                                    rowVal = (String)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').getSObject('SLF_Product__r').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.SLF_Product__r.'));
                                                }
                                            }else if(mFHeader.startsWithIgnoreCase('Underwriting_File__r.Opportunity__r')){
                                                if(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.') == 'isACH__c'){
                                                    rowVal = String.valueOf((Boolean)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.')));
                                                }
                                                else{                                               
                                                    rowVal = (String)lineItem.getSObject('Underwriting_File__r').getSObject('Opportunity__r').get(mFHeader.substringAfter('Underwriting_File__r.Opportunity__r.'));
                                                }
                                            }else if(mFHeader.startsWithIgnoreCase('Underwriting_File__r.')){
                                                rowVal = (String)lineItem.getSObject('Underwriting_File__r').get(mFHeader.substringAfter('Underwriting_File__r.'));
                                            }
                                            else if(mapDateMapping.get(colHeader)){
                                                if(colHeader.equalsIgnoreCase('UF Created Date Time') && lineItem.UF_Created_Date_Time__c != NULL)
                                                    rowVal = (lineItem.UF_Created_Date_Time__c).format('MM/dd/yyyy HH:mm','America/New_York');
                                                else if(colHeader.equalsIgnoreCase('M1 Approval Date') && lineItem.M1_Approval_Date__c != NULL)
                                                    rowVal = (lineItem.M1_Approval_Date__c).format('MM/dd/yyyy HH:mm','America/New_York');
                                                else if(colHeader.equalsIgnoreCase('Credit Decision Date') && lineItem.Credit_Decision_Date__c != NULL)
                                                    rowVal = (lineItem.Credit_Decision_Date__c).format();
                                                else if(colHeader.equalsIgnoreCase('BorrowerDOB') && lineItem.BorrowerDOB__c != NULL)
                                                    rowVal = (lineItem.BorrowerDOB__c).format();
                                                else{}                                                  
                                            }
                                            else{
                                                rowVal = ''+ lineItem.get(mFHeader);
                                            }
                                        
                                            System.debug('----%%--rowVal-----'+rowVal);
                                            if(!String.isBlank(rowVal))
                                                rowVal = rowVal.replaceAll(',',' ');
                                            fileRow = (String.isNotBlank(rowVal) && rowVal != 'null') ? (fileRow +','+rowVal) : (fileRow +',');
                                        }
                                        else{
                                                string rowVal = ' ';
                                                fileRow = (String.isNotBlank(rowVal) && rowVal != 'null') ? (fileRow +','+rowVal) : (fileRow +',');
            
                                        }
                                    }
                                }
                            }
                            
                            fileRow = fileRow.replaceFirst(',', '');
                            
                            generatedCSVFile = generatedCSVFile + fileRow + '\n';
                    }
                    
                }   
                System.debug('-@@---generatedCSVFile----'+generatedCSVFile);
                
                if(!String.isBlank(generatedCSVFile)){
                    String folderId = SFSettings__c.getOrgDefaults().Funding_Reports_Box_Folder_Id__c;
                    String filename = acc.Facility_Picklist_Key__c+' Funding '+Datetime.now().format('MMddyyyy')+'.csv';
                    uploadFileToBox(folderId,Blob.valueOf(generatedCSVFile),boxAccessTokan,filename);
                }
            }
            catch(Exception err){
             System.debug('--------Exception-------'+err.getMessage()+'----at Line #------------'+err.getLineNumber()); 
              system.debug('### FundingSnapshotBoxUpload.execute():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('FundingSnapshotBoxUpload.execute()',err.getLineNumber(),'execute()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            }
        }
        
        system.debug('### Exit into execute() of '+ FundingSnapshotBoxUpload.class);
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    global void execute(SchedulableContext sc) {
        FundingSnapshotBoxUpload batchObj = new FundingSnapshotBoxUpload(null); 
        database.executebatch(batchObj,1);
    }
    public static void uploadFileToBox(String strFolderId,Blob file,String token, String fileName)
    {
        system.debug('### Entered into uploadFileToBox() of ' + DownloadLoanDocs.class);
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+fileName+'";\nContent-Type: multipart/form-data;'+'\nnon-svg='+True;
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        HttpResponse res;
        String strFileId;
        
        while(headerEncoded.endsWith('='))
        {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        
        String bodyEncoded = EncodingUtil.base64Encode(file);
        
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
        
        if(last4Bytes.endsWith('==')) 
        {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        }
         
         else if(last4Bytes.endsWith('=')) 
         {
             last4Bytes = last4Bytes.substring(0,3) + 'N';
             bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
             footer = '\n' + footer;
             String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
             bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
         } 
         
         else 
         {
             footer = '\r\n' + footer;
             String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
             bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
         }
    
         String sUrl = 'https://upload.box.com/api/2.0/files/content?parent_id='+strFolderId;
         HttpRequest req = new HttpRequest();
         
         req.setHeader('Content-Type','multipart/form-data;non_svg='+True+';boundary='+boundary);
         
         req.setMethod('POST');
         req.setEndpoint(sUrl);
         req.setBodyAsBlob(bodyBlob);
         req.setTimeout(60000);
         req.setHeader('Authorization', 'Bearer '+token);
         req.setHeader('Content-Length',String.valueof(req.getBodyAsBlob().size()));
         
         Http http = new Http();
         
         system.debug('### size ###'+req.getBodyAsBlob().size());
         
         if(Test.isRunningTest()) {
             
             // Create a fake response
             res = new HttpResponse();
             res.setStatusCode(201);
             //dummy body
             String body = '{"entries":[{"id":"1231452"}]}';
             res.setBody(body);
         }
         else {           
             res = http.send(req);               
         }
        
        system.debug('### Upload status...'+res.getBody()+'###');        
        //DownloadLoanDocs.fileUploadJsonResponse ew = parseUploadFileResponse(res.getBody());
       // system.debug('### File id...'+ew.entries[0].id);
        
       // system.debug('### Exit from uploadFileToBox() of ' + DownloadLoanDocs.class);
        
        //return ew.entries[0].id;        
    }
}