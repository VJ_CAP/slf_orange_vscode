/**
* Description: User validation logic
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Raviteja Sajja          10/18/2017          Created
******************************************************************************************/

public class ValidateUserDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+ValidateUserDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### '+reqData);
        
        UnifiedBean unifiedBean = new UnifiedBean(); 
        unifiedBean.error = new list<UnifiedBean.errorWrapper>();
        unifiedBean.users = new List<UnifiedBean.UsersWrapper>();
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        string errMsg = '';
        IRestResponse iRestRes;
        
        
        try{
            Set<String> userEmails = new Set<String>();
            if(reqData.users != null)
            {
                for(UnifiedBean.UsersWrapper userIterate : reqData.users){
                    if(userIterate.email!= null || string.isNotBlank(userIterate.email)){
                        userEmails.add(userIterate.email);
                    }
                }
                if(!userEmails.isEmpty()){
                    List<User> usr = [Select id, email, name, Phone,contact.email, contact.AccountId, contact.phone, contact.Account.phone,contact.Account.Name from User where email IN : userEmails and IsActive = true order by CreatedDate DESC LIMIT 1];
                    
                    if(!usr.IsEmpty()){
                        
                        for(User userRec :usr)
                        {
                            UnifiedBean.UsersWrapper userWrap = new UnifiedBean.UsersWrapper();
                            userWrap.id = userRec.id;
                            userWrap.name = userRec.name;
                            userWrap.email = userRec.email;
                            
                            if(userRec.contact.Account.Name != null){
                                userWrap.installerName = userRec.contact.Account.Name;
                            }
                            if(null != userRec.phone){
                                userWrap.phone = userRec.phone;
                            }
                            
                            ConnectApi.Photo ph =  ConnectApi.UserProfiles.getPhoto(null, userRec.id);
                            
                            userWrap.photoURL = ph.fullEmailPhotoUrl;
                            unifiedBean.users.add(userWrap);
                        }
                        unifiedBean.returnCode = '200';
                        iRestRes = (IRestResponse)unifiedBean;
                        
                        system.debug('### iRestRes - '+iRestRes);
                    }else{
                        iolist = new list<DataValidator.InputObject>{};
                        errMsg = ErrorLogUtility.getErrorCodes('295',null);
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        unifiedBean.error.add(errorMsg);
                        unifiedBean.returnCode = '295';
                        iRestRes = (IRestResponse)unifiedBean;
                        system.debug('### iRestRes - '+iRestRes);
                        return iRestRes;
                    }
                }
            }else{
                iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('214',null);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                unifiedBean.error.add(errorMsg);
                unifiedBean.returnCode = '214';
                iRestRes = (IRestResponse)unifiedBean;
                system.debug('### iRestRes - '+iRestRes);
                return iRestRes;    
            }
            
        }catch(Exception err){
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedBean.error.add(errorMsg);
            unifiedBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedBean;
            system.debug('### ValidateUserDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('ValidateUserDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+ValidateUserDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
   
}