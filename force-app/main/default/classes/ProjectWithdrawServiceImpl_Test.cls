/******************************************************************************************
* Description: Test class to cover UpdateStipsDataServiceImpl Services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri             09/16/2019             Created
******************************************************************************************/
@isTest
public class ProjectWithdrawServiceImpl_Test{    
    
    @testsetup static void createtestdata(){
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c drawreqTrg = new TriggerFlags__c();
        drawreqTrg.Name ='Draw_Requests__c';
        drawreqTrg.isActive__c =true;
        trgLst.add(drawreqTrg);
        insert trgLst;
        SLFUtilityDataSetup.initData();
        
    }
    /**
*
* Description: This method is used to cover ProjectWithdrawServiceImpl  
*
*/
    private testMethod static void ProjectWithdrawServiceImplTest(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,hash_id__c from User where Email = 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,SLF_Product__c from opportunity where name = 'opName' LIMIT 1]; 
        system.debug('Opportunity Id'+opp);
        Product__c prod = [select Id,Name from Product__c where Name = 'testprod' LIMIT 1];
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1]; 
        
        
        System.runAs(loggedInUsr[0])
        {
            Test.starttest();
            Map<String, String> ProjectWithdrawMap = new Map<String, String>();
            
            String ProjectWithdraw ;
            ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'","externalId": "","hashId": "","projectWithDrawnReason" :"testgg"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            
            ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'","externalId": "'+opp.Partner_Foreign_Key__c+'","hashId": "","projectWithDrawnReason" :"testgg"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'","externalId": "'+opp.Partner_Foreign_Key__c+'","hashId": "'+opp.Hash_Id__c+'","projectWithDrawnReason" :"testgg"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            ProjectWithdraw= '{"projects": [{"id": "","externalId": "123","hashId": "","projectWithDrawnReason" :"testgg"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            ProjectWithdraw= '{"projects": [{"id": "","externalId": "","hashId": "'+opp.Hash_Id__c+'","projectWithDrawnReason" :"testgg"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            ProjectWithdraw= '{"projects": [{"id": "","externalId": "","hashId": "","projectWithDrawnReason" :"testgg"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            ProjectWithdraw= '{"projects": []}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');            
            ProjectWithdraw= '{"projects": ["qwrwr":"wete"]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw'); 
            
            opp.EDW_Originated__c=true;
            update opp;
            ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw');
            
            Test.stopTest();
            
        }  
    }
    private testMethod static void ProjectWithdrawServiceImplHome(){
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,hash_id__c from User where Email = 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,SLF_Product__c from opportunity where name = 'opName' LIMIT 1]; 
        system.debug('Opportunity Id'+opp);
        Product__c prod = [select Id,Name from Product__c where Name = 'testprod' LIMIT 1];
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1]; 
        // opp.Hash_Id__c = loggedInUsr[0].Hash_Id__c;
        opp.Project_Category__c = 'Home';       
        update opp;
        
    /*    Underwriting_File__c undRec=[Select id from Underwriting_File__c where Opportunity__c =:opp.id LIMIT 1];
        
        Draw_Requests__c drawReqRec = new Draw_Requests__c();
        drawReqRec.Amount__c = 1000;
        drawReqRec.Level__c = '1';
        drawReqRec.Underwriting__c = undRec.id;
        drawReqRec.Status__c = 'Approved';
        drawReqRec.Request_Date__c = System.Today();
        drawReqRec.Hash_Id__c = '7896541230';
        insert drawReqRec;*/
        System.runAs(loggedInUsr[0])
        {
            Test.starttest();
            Map<String, String> ProjectWithdrawMap = new Map<String, String>();
            
            String ProjectWithdraw ;
            
            ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw'); 
            
            ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'","projectWithDrawnReason" :"testgg"}]}';
            MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw'); 
 
            
        //    ProjectWithdraw= '{"projects": [{"id": "'+opp.id+'","projectWithDrawnReason" :"testgg","draws": [{"hashId":"7896541230",}]}]}';
        //    MapWebServiceURI(ProjectWithdrawMap,ProjectWithdraw,'projectwithdraw'); 
            
            
            Test.stopTest();
            
        }  
    }
    
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}