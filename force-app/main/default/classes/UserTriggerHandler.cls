/**
* Description: Trigger Business logic associated with User is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh R                01/03/2018           Created 
******************************************************************************************/
public with sharing Class UserTriggerHandler
{
    public static boolean isTrgExecuting = false;
    
    /**
    * Constructor to initialize 
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public UserTriggerHandler(boolean isExecuting){
        isTrgExecuting = isExecuting; 
    }
    
    /**
    * Description: On After Insert
    *
    * @param new User records.
    */
    public  void OnAfterInsert(User[] newUser){
        try{
            system.debug('### Entered into OnAfterInsert() of '+ UserTriggerHandler.class);
            List<user> lstUsersUpdate = new  List<user>();
            set<id> conList = new  set<id>();
            for (User u : newUser) {
                if(u.IsPortalEnabled && u.Hash_Id__c == null){
                    String hashid = SLFUtility.getHashId(u.id);
                    if(string.isNotBlank(hashid))
                    {
                        User userUpdate = new User();
                        userUpdate.id = u.id;
                        if(userUpdate.UserPreferencesShowEmailToExternalUsers == false){
                            userUpdate.UserPreferencesShowEmailToExternalUsers = true;
                        }
                        userUpdate.Hash_Id__c = hashid;
                        lstUsersUpdate.add(userUpdate);
                    }
                }
                if(u.contactId != null){
                    conList.add(u.contactId);
                }

            }
            try{
                if(lstUsersUpdate!=null && lstUsersUpdate.size()>0){
                    update lstUsersUpdate; 
                }
            }catch(Exception err){
              
            }
          
            if(!conList.isEmpty()){
                updateContact(conList);
            }
            
            system.debug('### Exit from OnAfterInsert() of '+ UserTriggerHandler.class);
        }catch(Exception err){
            system.debug('### UserTriggerHandler.OnAfterInsert():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('UserTriggerHandler.OnAfterInsert()',err.getLineNumber(),'OnAfterInsert()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));           
        }
    }
    
     /**
    * Description: 
    *
    * @param new User records.
    */
    public void OnBeforeInsert(User[] newUser){
        try
        {
            system.debug('### Entered into OnBeforeInsert() of '+ UserTriggerHandler.class);
            set<String> emailids = new set<string>();
            for(User usrRec :newUser){
                if(usrRec.Email != null){
                    emailids.add(usrRec.Email);
                }
                // Added as part of ORANGE - 143 by Venkat
                 if((usrRec.IsPortalEnabled && usrRec.isActive) && (String.isBlank(usrRec.Answer1__c) || String.isBlank(usrRec.Answer2__c) || String.isBlank(usrRec.Answer3__c) || String.isBlank(usrRec.Question1__c) || String.isBlank(usrRec.Question2__c) || String.isBlank(usrRec.Question3__c))){
                    usrRec.Ask_Questions__c=true;
                }else{
                    usrRec.Ask_Questions__c=false;
                }
            }
            if(!emailids.isEmpty()){
                List<User> conlist =[Select id,Email from User where Email in :emailids and isActive = true];
                system.debug('conlist'+conlist);
                if(!conlist.isEmpty()|| conlist.size()>0){
                    for(User usrRec :newUser){
                        usrRec.adderror('Provided Email id alredy exists.' );
                    }
                }
        }
            system.debug('### Exit from OnBeforeInsert() of '+ UserTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### UserTriggerHandler.OnBeforeInsert():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('UserTriggerHandler.OnBeforeInsert()',ex.getLineNumber(),'OnBeforeInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
     /**
    * Description: 
    *
    * @param new User records.
    */
    public void OnBeforeUpdate(User[] newUser){
        try
        {
            for(User usrRec :newUser){
                // Added as part of ORANGE - 143 by Venkat
                if((usrRec.IsPortalEnabled && usrRec.isActive) && (String.isBlank(usrRec.Answer1__c) || String.isBlank(usrRec.Answer2__c) || String.isBlank(usrRec.Answer3__c) || String.isBlank(usrRec.Question1__c) || String.isBlank(usrRec.Question2__c) || String.isBlank(usrRec.Question3__c))){
                    usrRec.Ask_Questions__c=true;
                }else{
                    usrRec.Ask_Questions__c=false;
                }
            }
        }catch(Exception ex)
        {
            system.debug('### UserTriggerHandler.OnBeforeUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('UserTriggerHandler.OnBeforeUpdate()',ex.getLineNumber(),'OnBeforeUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
     @future(callout=true)
    Public static void updateContact(Set<Id> contactId){
        try{
            List<Contact> conList = new List<Contact>();
            system.debug('### Entered into updateContact() of '+ UserTriggerHandler.class);
            Map<string,User> conidUserRec = new Map<string,User>(); //Added by Suresh to fix User Name in reset password email issue
            for(User objuser : [select id,contactid,name from User where contactid IN:contactId]){
                conidUserRec.put(objuser.contactid,objuser);
            }
            
            
            for (id conids : contactId) 
            {
                Contact con = new Contact(id=conids);
                con.Reset_Password_Flag__c =true;
                if(conidUserRec.get(conids)!= null)
                {
                    User objUser = conidUserRec.get(conids);
                    con.User_Hash_ID__c = SLFUtility.getHashId(objUser.id);
                }
                conList.add(con);
            }
            if(!conList.isEmpty())  
            {
                update conList;
            }   
        }catch(Exception ex)
        {
            system.debug('### UserTriggerHandler.updateContact():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('UserTriggerHandler.updateContact()',ex.getLineNumber(),'updateContact()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
}