/**
* Description: Base class which provides implementation of IRestDataService interface, so that actual Service implementation providing classes 
                    can override the virtual method what they want to provide implementation instead of providing implementation for all methods of interface.
*
*   Modification Log:
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public virtual class RestDataServiceBase implements IRestDataService{
    /**
    * Description: Convert input request wrapper (JSON) to salesforce sobject
    *
    * @param req            IRestRequest accepts object specific bean.
    * return null           sObject returns object specific bean based on the consumer.
    */
    public virtual IRestRequest transformInput(IDataObject req){
        return null;
    }
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public virtual IRestResponse transformOutput(IDataObject res){
        return null;
    }
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject Employer record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public virtual IRestResponse sObject2Bean(sObject sObj,Map<Id,List<Underwriting_File__c>> UnderwritingMap,Map<Id,List<dsfs__DocuSign_Status__c>> DocuSignMap,  String status, String msg){
        return null;
    }
}