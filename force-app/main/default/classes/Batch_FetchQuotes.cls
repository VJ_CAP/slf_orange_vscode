global class Batch_FetchQuotes implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {

    DateTime lastExecutionTime;
    DateTime curBatchStartTime;
    Double  batchNumber;
    Boolean errorEncountered;
    
    /**
     * This Batch fetches / upserts Quotes for all Opportunities 
     * where the 'Upload Comment' field is checked
     * 
     **/
    public Batch_FetchQuotes()
    {
        errorEncountered = false;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {

        // *** return all Opportunities where Upload_Comment__c = true
        String query = 'SELECT Id, Install_State_Code__c, Sighten_UUID__c FROM Opportunity where Upload_Comment__c = true and Sighten_UUID__c != null ORDER BY CreatedDate DESC';
        //String query = 'SELECT Id, Install_State_Code__c, Sighten_UUID__c FROM Opportunity where Upload_Comment__c = true and Sighten_UUID__c != null and Block_Draws__c=true ORDER BY CreatedDate DESC';        
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext sc)
    {
        Batch_FetchQuotes bf = new Batch_FetchQuotes();
        Database.executeBatch(bf,5);
    }
    
    global void execute(Database.BatchableContext BC,List<Opportunity> ol) 
    {
        try {
            Set<ID> oppIdSet = new set<ID>();
          
            for (Opportunity o : ol)
            {
                oppIdSet.add(o.Id);
            }
            
            // *** create lists to pass to createQuote
            List<Opportunity> updateOppList = new List<Opportunity>();
            List<Quote> upsertQuoteList = new List<Quote>();
            
            List<Quote> quoteList = new List<Quote> ([SELECT Id, OpportunityId, Opportunity.Install_State_Code__c, Sighten_Quote_Id__c FROM Quote WHERE OpportunityID in :oppIdSet]);
            List<Product__c> productsList = new List<Product__c> ([SELECT Id, Sighten_Product_UUID__c, State_Code__c, ACH__c, Installer_Account__c FROM Product__c WHERE Is_Active__c = true and Sighten_Product_UUID__c!='']); 
            for (Opportunity o : ol) {
                try
                {
                    String jsonQuoteResponse = sightenCallout.getSightenQuotesCallout(o.Sighten_UUID__c, o.Id);
                
                    if (!Test.IsRunningTest()) {
                        sightenUtil.createQuote(jsonQuoteResponse, o.Sighten_UUID__c, o, quoteList, productsList, true, updateOppList, upsertQuoteList);
                    }
                }
                catch(Exception ex)
                {
                    // Do nothing: so that it picks up the next record in the for loop
                    system.debug('### Batch_FetchQuotes.execute() Callout Failed : ' + ex.getMessage());
                 ErrorLogUtility.writeLog('Batch_FetchQuotes.execute():'+o.Id,ex.getLineNumber(),'execute()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));            
                }
            }
            update updateOppList;
            upsert upsertQuoteList;
            
        } catch (Exception e) {
            errorEncountered = true;
            throw new CustException(e);
        }
        
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        
        if (!Test.IsRunningTest()) {
            if (settings.CHAIN_BATCHES__c && !errorEncountered) {
                 Database.executeBatch(new Batch_FetchMilestones(), 10);
            }
        }
    }
}