@isTest
private class FundingSnapshotSupport_Test 
{
    
    @isTest static void test1 () 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Facility_Membership_Fee__c=1000,Solar_Enabled__c=true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a1.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30),
                                            Credit_Decision_Date__c = System.today());
        insert o1;
        
        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
        

        list<FundingSnapshotSupport.GenerateFundingSnapshotsInput> ilist = new list<FundingSnapshotSupport.GenerateFundingSnapshotsInput>{};

        FundingSnapshotSupport.GenerateFundingSnapshotsInput i1 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput(a3.Id,100);
        ilist.add (i1);

        FundingSnapshotSupport.GenerateFundingSnapshotsInput i2 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput(a1.Id,100);
        ilist.add (i2);

        list<FundingSnapshotSupport.GenerateFundingSnapshotsOutput> olist = FundingSnapshotSupport.generateFundingSnapshots(ilist);

        list<Funding_Snapshot__c> sns = [SELECT Id, Processed__c FROM Funding_Snapshot__c];

        sns[0].Processed__c = System.now();
        update sns[0];

        sns[0].Processed__c = null;
        update sns[0];

        sns[1].Processed__c = System.now();
        update sns[1];

        sns[1].Processed__c = null;
        update sns[1];
/*
        uf1.ST_Reapproved__c = System.now();
        uf1.LT_Reapproved__c = System.now();
        update uf1;

        uf1.ST_Amount_Financed__c = 2000;
        uf1.LT_Amount_Financed__c = 3000;
        update uf1;
*/
        delete sns;
        
        o1.Project_Category__c = 'Home';
        update o1;
          TriggerFlags__c stipTrgFlag = new TriggerFlags__c(Name='Draw_Requests__c',isActive__c=true);
        insert stipTrgFlag;
        List<Draw_Requests__c> DrawList = new List<Draw_Requests__c>();
        for(integer i=1;i<=3;i++){
            Draw_Requests__c objDraw = new Draw_Requests__c();
            objDraw.Level__c = String.valueOf(i);
            objDraw.Amount__c = 1000;
            objDraw.Request_Date__c = System.now();
            objDraw.Status__c = 'Requested';
            objDraw.Primary_Applicant_Email__c = 'test@gma.com';
            objDraw.Underwriting__c = uf1.id;
            DrawList.add(objDraw);
        }
            insert DrawList;
            for(Draw_Requests__c objDraw :DrawList){
            objDraw.Status__c = 'Approved';
            objDraw.SLF_Approved__c = system.now().addDays(2);
            }
            update DrawList;
            
            List<Funding_Data__c> funDataList = new List<Funding_Data__c>();
            Funding_Data__c funData = new Funding_Data__c();
            funData.Underwriting_ID__c = uf1.id;
            funData.C_O_CPF_APR__c = 1.5;
            funData.Cancelled_CPF_APR__c = 1.5;
            funData.Capital_Provider_Funded_APR__c = 1.5;
            funData.C_O_CPF_Term__c = 1;
            funData.Cancelled_CPF_Term__c =1;
            funData.Capital_Provider_Funded_Term__c = 1;
            funData.Draw_1_Pay_Amount__c = 1000;
            funData.Progress_Pay_Date__c = system.today();
            funDataList.add(funData);
            insert funDataList;
            
            funData.C_O_CPF_APR__c = 2.5;
            funData.C_O_CPF_Term__c =2;
            funData.Draw_1_Pay_Amount__c = 2000;
            funData.Progress_Pay_Date__c = system.today()+1;
            update funData;
            
        list<FundingSnapshotSupport.GenerateFundingSnapshotsInput> ilist1 = new list<FundingSnapshotSupport.GenerateFundingSnapshotsInput>{};

        FundingSnapshotSupport.GenerateFundingSnapshotsInput il1 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput();
        il1.accountId = a3.id;
        il1.maxNumberOfUFs = 500;
        il1.startDate = system.today().addDays(-21);
        il1.endDate = system.today().addDays(3);
        il1.homeImprovement = false;
        
        ilist1.add (il1);
        list<FundingSnapshotSupport.GenerateFundingSnapshotsOutput> olist1 = FundingSnapshotSupport.generateFundingSnapshots(ilist1);
    }   
    
    @isTest static void test2 () 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Facility_Membership_Fee__c=1000,Solar_Enabled__c=true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Product_Tier__c = '0',
                                            Short_Term_Facility_Lookup__c = a3.Id,
                                            Long_Term_Facility_Lookup__c = a3.Id);
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a3.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;
        

        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
        
        List<Funding_Data__c> funDataList = new List<Funding_Data__c>();
            Funding_Data__c funData = new Funding_Data__c();
            funData.Underwriting_ID__c = uf1.id;
            //funData.C_O_CPF_APR__c = 1.5;
            funData.Cancelled_CPF_APR__c = 1.5;
            funData.Capital_Provider_Funded_APR__c = 1.5;
            //funData.C_O_CPF_Term__c = 1;
            funData.Cancelled_CPF_Term__c =1;
            funData.Capital_Provider_Funded_Term__c = 1;
            funData.Draw_1_Pay_Amount__c = 1000;
            funData.Progress_Pay_Date__c = system.today();
            funDataList.add(funData);
            insert funDataList;
            
            //funData.C_O_CPF_APR__c = 2.5;
            //funData.C_O_CPF_Term__c =2;
            funData.Cancelled_CPF_APR__c = 2.5;
            funData.Cancelled_CPF_Term__c =2;
            
            update funData;

        list<FundingSnapshotSupport.GenerateFundingSnapshotsInput> ilist = new list<FundingSnapshotSupport.GenerateFundingSnapshotsInput>{};

        FundingSnapshotSupport.GenerateFundingSnapshotsInput i1 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput(a3.Id,100);
        ilist.add (i1);

        FundingSnapshotSupport.GenerateFundingSnapshotsInput i2 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput(a1.Id,100);
        ilist.add (i2);

        list<FundingSnapshotSupport.GenerateFundingSnapshotsOutput> olist = FundingSnapshotSupport.generateFundingSnapshots(ilist);

        list<Funding_Snapshot__c> sns = [SELECT Id, Processed__c FROM Funding_Snapshot__c];

        sns[0].Processed__c = System.now();
        update sns[0];

        sns[0].Processed__c = null;
        update sns[0];

        sns[1].Processed__c = System.now();
        update sns[1];

        sns[1].Processed__c = null;
        update sns[1];
/*
        uf1.ST_Reapproved__c = System.now();
        uf1.LT_Reapproved__c = System.now();
        update uf1;

        uf1.ST_Amount_Financed__c = 2000;
        uf1.LT_Amount_Financed__c = 3000;
        update uf1;
*/
        delete sns;
        
        o1.Project_Category__c = 'Home';
        update o1;
          TriggerFlags__c stipTrgFlag = new TriggerFlags__c(Name='Draw_Requests__c',isActive__c=true);
        insert stipTrgFlag;
        List<Draw_Requests__c> DrawList = new List<Draw_Requests__c>();
        for(integer i=1;i<=3;i++){
            Draw_Requests__c objDraw = new Draw_Requests__c();
            objDraw.Level__c = String.valueOf(i);
            objDraw.Amount__c = 1000;
            objDraw.Request_Date__c = System.now();
            objDraw.Status__c = 'Requested';
            objDraw.Primary_Applicant_Email__c = 'test@gma.com';
            objDraw.Underwriting__c = uf1.id;
            DrawList.add(objDraw);
        }
            insert DrawList;
            for(Draw_Requests__c objDraw :DrawList){
            objDraw.Status__c = 'Approved';
            objDraw.SLF_Approved__c = system.now().addDays(2);
            }
            update DrawList;
        list<FundingSnapshotSupport.GenerateFundingSnapshotsInput> ilist1 = new list<FundingSnapshotSupport.GenerateFundingSnapshotsInput>{};

        FundingSnapshotSupport.GenerateFundingSnapshotsInput il1 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput();
        il1.accountId = a3.id;
        il1.maxNumberOfUFs = 500;
        il1.startDate = system.today().addDays(-21);
        il1.endDate = system.today().addDays(3);
        il1.homeImprovement = false;
        
        ilist1.add (il1);
        list<FundingSnapshotSupport.GenerateFundingSnapshotsOutput> olist1 = FundingSnapshotSupport.generateFundingSnapshots(ilist1);
    } 
    @isTest static void test3 () 
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
        Account a1 = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40,Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a1;

        Installer_Incentive__c ii1 = new Installer_Incentive__c (   Installer_Account__c = a1.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert ii1;

        Account a2 = new Account (Name = 'Test customer',Installer_Legal_Name__c='Bright Solar Planet',Solar_Enabled__c=true);
        insert a2;

        Account a3 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility',Installer_Legal_Name__c='Bright Solar Planet',Facility_Membership_Fee__c=1000,Solar_Enabled__c=true);
        insert a3;

        Product__c p1 = new Product__c (Name = 'Test', Short_Term_Facility__c = 'TCU', Long_Term_Facility__c = 'TCU',
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = a1.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            Is_Active__c = true,
                                            Product_Tier__c = '0',
                                            Short_Term_Facility_Lookup__c = a3.Id,
                                            Long_Term_Facility_Lookup__c = a3.Id);
        insert p1;

        Opportunity o1 = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = a2.Id,
                                            Installer_Account__c = a3.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = p1.Id,
                                            StageName = 'Test', CloseDate = System.today().addDays(30));
        insert o1;
        

        Underwriting_File__c uf1 = new Underwriting_File__c (Opportunity__c = o1.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                                LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf1;
        
        List<Funding_Data__c> funDataList = new List<Funding_Data__c>();
            Funding_Data__c funData = new Funding_Data__c();
            funData.Underwriting_ID__c = uf1.id;
            funData.Capital_Provider_Funded_APR__c = 1.5;
            funData.Capital_Provider_Funded_Term__c = 1;
            funData.Draw_1_Pay_Amount__c = 1000;
            funData.Progress_Pay_Date__c = system.today();
            funDataList.add(funData);
            insert funDataList;
            
            funData.Capital_Provider_Funded_APR__c = 2.5;
            funData.Capital_Provider_Funded_Term__c = 2;
            update funData;

        list<FundingSnapshotSupport.GenerateFundingSnapshotsInput> ilist = new list<FundingSnapshotSupport.GenerateFundingSnapshotsInput>{};

        FundingSnapshotSupport.GenerateFundingSnapshotsInput i1 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput(a3.Id,100);
        ilist.add (i1);

        FundingSnapshotSupport.GenerateFundingSnapshotsInput i2 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput(a1.Id,100);
        ilist.add (i2);

        list<FundingSnapshotSupport.GenerateFundingSnapshotsOutput> olist = FundingSnapshotSupport.generateFundingSnapshots(ilist);

        list<Funding_Snapshot__c> sns = [SELECT Id, Processed__c FROM Funding_Snapshot__c];

        sns[0].Processed__c = System.now();
        update sns[0];

        sns[0].Processed__c = null;
        update sns[0];

        sns[1].Processed__c = System.now();
        update sns[1];

        sns[1].Processed__c = null;
        update sns[1];

        delete sns;
        
        o1.Project_Category__c = 'Home';
        update o1;
          TriggerFlags__c stipTrgFlag = new TriggerFlags__c(Name='Draw_Requests__c',isActive__c=true);
        insert stipTrgFlag;
        List<Draw_Requests__c> DrawList = new List<Draw_Requests__c>();
        for(integer i=1;i<=3;i++){
            Draw_Requests__c objDraw = new Draw_Requests__c();
            objDraw.Level__c = String.valueOf(i);
            objDraw.Amount__c = 1000;
            objDraw.Request_Date__c = System.now();
            objDraw.Status__c = 'Requested';
            objDraw.Primary_Applicant_Email__c = 'test@gma.com';
            objDraw.Underwriting__c = uf1.id;
            DrawList.add(objDraw);
        }
            insert DrawList;
            for(Draw_Requests__c objDraw :DrawList){
            objDraw.Status__c = 'Approved';
            objDraw.SLF_Approved__c = system.now().addDays(2);
            }
            update DrawList;
        list<FundingSnapshotSupport.GenerateFundingSnapshotsInput> ilist1 = new list<FundingSnapshotSupport.GenerateFundingSnapshotsInput>{};

        FundingSnapshotSupport.GenerateFundingSnapshotsInput il1 = new FundingSnapshotSupport.GenerateFundingSnapshotsInput();
        il1.accountId = a3.id;
        il1.maxNumberOfUFs = 500;
        il1.startDate = system.today().addDays(-21);
        il1.endDate = system.today().addDays(3);
        il1.homeImprovement = false;
        
        ilist1.add (il1);
        list<FundingSnapshotSupport.GenerateFundingSnapshotsOutput> olist1 = FundingSnapshotSupport.generateFundingSnapshots(ilist1);
    }  
}