/******************************************************************************************
* Description: Test class to cover GetDealerInfoDataServiceImpl Services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Bindu Sri             05/16/2019             Created
******************************************************************************************/
@isTest
public class GetDealerInfoDataServiceImpl_Test {
    /**
*
* Description: This method is used to cover GetDealerInfoDataServiceImpl
*
*/   
    private testMethod static void dealerInfoTest(){
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgAccflagDis = new TriggerFlags__c();
        trgAccflagDis.Name ='Disclosures__c';
        trgAccflagDis.isActive__c =true;
        trgLst.add(trgAccflagDis);
        
        
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        
        //Insert Profile
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        //For GetDealerInfoDataServiceImpl Inserting Account record 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.HI_Dealer_Code__c = '123456';
        acc.Home_Improvement_Enabled__c = true;
        insert acc;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;  
        
        List<User> userList = new List<User>();
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',Default_User__c = true,LoginCount__c = 1,
                                   EmailEncodingKey='UTF-8', LastName='test', Password__c = 'Test@123',LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, 
                                   UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        userList.add(portalUser);
        insert userList;
        
        List<Disclosures__c> disclsrObj = new List<Disclosures__c>();
        Disclosures__c disclr = new Disclosures__c();
        disclr.Type__c = 'Dealer';
        disclr.Active__c= true;
        disclr.Name ='Test';
        disclr.Disclosure_Text__c ='Test Disclousere Text';
        disclsrObj.add(disclr);
        insert disclsrObj;                                                 
        
        Map<String, String> userMap = new Map<String, String>();
        String dealerStr;
        
        System.runAs(userList[0])
        {
            Test.starttest();
            
            dealerStr = '{"dealercode":"123456"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,dealerStr,'getdealerinfo');
            
            dealerStr = '{"dealercode":"123"}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,dealerStr,'getdealerinfo');
            
            dealerStr = '{"dealercode":""}';
            SLFRestDispatchTest.MapWebServiceURI(userMap,dealerStr,'getdealerinfo');
            GetDealerInfoServiceImpl getDealerInfo =new GetDealerInfoServiceImpl();
            getDealerInfo.fetchData(null);
            
            Test.StopTest();
        }      
        
    }
}