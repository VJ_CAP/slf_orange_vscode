public class LeadTriggerHandler{
 
    public static void OnAfterInsert(Map<Id,Lead> newLeadMap){
        afterInsertUpdate(newLeadMap.values());
    }
    
    public static void OnAfterUpdate(Map<Id,Lead> newLeadMap, Map<Id,Lead> oldLeadMap){
        afterInsertUpdate(newLeadMap.values());
    }
    
    public static void OnBeforeInsert(List<Lead> newLeadList){
        beforeInsertUpdate(newLeadList,null);
    }
    
    public static void OnBeforeUpdate(Map<Id,Lead> newLeadMap, Map<Id,Lead> oldLeadMap){
        beforeInsertUpdate(newLeadMap.values(),oldLeadMap);
    }
    
    public static void beforeInsertUpdate(List<Lead> newLeadList, Map<id,Lead> oldMap){
        system.debug('### Entered into beforeInsertUpdate() of '+ LeadTriggerHandler.class);
        List<String> emailList = new List<String>();   
        //Query prequal table 
        List<Prequalification_Criterion__c> prequalList = [SELECT Installer_Account__c,Stip_Code__c,State_Code__c, Long_Term_Facility__c, FNIDecisionCode__c, FNI_Response_Code__c, Qualification_Message__c from Prequalification_Criterion__c];    
        boolean callFNIBool = false;
        for(Lead thisLead: newLeadList){
            //check if state is null but vision state code is not
            if(thisLead.Vision_State_Text__c != null){
                //update state
                thisLead.State = callFNITriggerStaticHelper.getState(thisLead.Vision_State_Text__c);
                thisLead.StateCode = thisLead.Vision_State_Text__c;
            }
            //check if this is a supported installer/state
            boolean isSupported = false;
            
            for(Prequalification_Criterion__c prequalCrit: prequalList){
            System.debug('### thisLead.Installer_Account__c:::'+thisLead.Installer_Account__c);
            System.debug('### prequalCrit.Installer_Account__c:::'+prequalCrit.Installer_Account__c);
                if(thisLead.Installer_Account__c == prequalCrit.Installer_Account__c && thisLead.StateCode == prequalCrit.State_Code__c){
                    isSupported = true;
                    break;
                }
            }
            if(isSupported){
                  if(oldMap != null){
                    Lead oldLead = oldMap.get(thisLead.Id);
                    callFNIBool = callFNITriggerStaticHelper.callFNI(
                                    thisLead.FirstName,
                                    thisLead.LastName, 
                                    thisLead.Street, 
                                    thisLead.City, 
                                    thisLead.StateCode, 
                                    thisLead.PostalCode, 
                                    thisLead.Customer_has_authorized_credit_soft_pull__c, 
                                    thisLead.FNI_Decision__c, 
                                    thisLead.DOB__c, 
                                    thisLead.Yearly_Income__c, 
                                    thisLead.SSN__c, 
                                    oldLead.FirstName,
                                    oldLead.LastName,
                                    oldLead.Street,
                                    oldLead.City,
                                    oldLead.StateCode,
                                    oldLead.PostalCode,
                                    oldLead.DOB__c,
                                    oldLead.Yearly_Income__c,
                                    oldLead.SSN__c,
                                    thisLead.Stip_Code__c);
                } else {
                    callFNIBool = callFNITriggerStaticHelper.callFNI(thisLead.FirstName,thisLead.LastName,thisLead.Street,thisLead.City,thisLead.StateCode,thisLead.PostalCode,thisLead.Customer_has_authorized_credit_soft_pull__c,thisLead.FNI_Decision__c,thisLead.DOB__c,thisLead.Yearly_Income__c,thisLead.SSN__c,null,null,null,null,null,null,null,null,null,thisLead.Stip_Code__c);   
                }
                if(callFNIBool){
                    thisLead.FNI_Decision__c = '0';
                    //thisLead.FNI_Decision__c = '';
                    thisLead.Prequal_Decision__c = 'Decision pending - please refresh page in a moment...';
                    callFNILeadTriggerStaticHelper.mapCalledFNI.clear();
                }    
            } else{
                thisLead.FNI_Decision__c = '99'; //Commented by Suresh on 20/04/2018
                //thisLead.FNI_Decision__c = 'E'; //Added by Suresh on 20/04/2018
                thisLead.Prequal_Decision__c = 'Unfortunately, at this time, the Sunlight Financial Prequalification feature is not available for products in the state you have selected.';
            }
            if(thisLead.Sales_Representative_Email__c != null)
            {
                emailList.add(thisLead.Sales_Representative_Email__c);
            }
        }
        
        if(emailList.size() > 0){
            Map<String, Id> contactMap = new Map<String, Id>();
            for(Contact contactResults : [Select Id, email from Contact where isPersonAccount = false AND email = :emailList]){
                contactMap.put(contactResults.email, contactResults.Id);
                System.debug('### Debug: contactResults: ' + contactResults);
            }
        
            for(Lead updateLead:newLeadList){
                updateLead.Sales_Representative__c = contactMap.get(updateLead.Sales_Representative_Email__c);
            }
        }
         system.debug('###Exit from beforeInsertUpdate() of '+ LeadTriggerHandler.class);
    }
    
    public static void afterInsertUpdate(List<Lead> newLeadList){
        system.debug('### Entered into afterInsertUpdate() of '+ LeadTriggerHandler.class);
        //Added by Suresh (Credit Waterfall Change)
        List<user> loggedInUsr = [select Id,contact.AccountId from User where Id =: userinfo.getUserId()];                

        //Query prequal table
        List<Prequalification_Criterion__c> prequalList = [SELECT Installer_Account__c,Stip_Code__c,State_Code__c, Long_Term_Facility__c, FNIDecisionCode__c, FNI_Response_Code__c, Qualification_Message__c from Prequalification_Criterion__c where Installer_Account__c=:loggedInUsr.get(0).contact.AccountId];
        List<Lead> fniCreditList = new List<Lead>();
        for(Lead thisLead: newLeadList){
            //no need to check recordtype since the credit auth is unique to that record type
            
            if(thisLead.FNI_Decision__c == '0'){
                fniCreditList.add(thisLead);
            }
        }
        
        for(Lead fniCredit : fniCreditList){
            //this is the non-bulkified portion - this call happens for each credit object, if FNI provides a bulk api, this can be editted
            if(!callFNILeadTriggerStaticHelper.mapCalledFNI.containsKey(fniCredit.id)){
                fniCalloutManual.buildRequestAsync(fniCredit.Id, JSON.serialize(prequalList)); 
                callFNILeadTriggerStaticHelper.mapCalledFNI.put(fniCredit.id,true);
            }
        }  
        system.debug('###Exit from afterInsertUpdate() of '+ LeadTriggerHandler.class);
    }
    
}