global without sharing class StipulationsSupport 
{
    //  NOTE: processStipulationsForUfs is called from trigger StipulationProcessAfterInsert
    
    public static String processStipulationsForUfs (set<Id> ufIds)
    {
        list<Case> casesToInsert = new list<Case>{};
            list<Stipulation__c> stipsToUpdate = new list<Stipulation__c>{};
                
                //  get UFs with embedded Stips not assigned to Case
                
                list<Underwriting_File__c> ufs = [SELECT Id, Name, Opportunity__r.OwnerId, Opportunity__r.Installer_Account__c,
                                                  Opportunity__r.AccountId,Opportunity__r.Account.Owner.Email,
                                                  Opportunity__r.Installer_Account__r.Stipulation__c,
                                                  Opportunity__r.Installer_Account__r.Stip_Email_1__c,
                                                  Opportunity__r.Installer_Account__r.Stip_Email_2__c,
                                                  Opportunity__r.Installer_Account__r.Stip_Email_3__c,
                                                  Opportunity__r.Installer_Account__r.Installer_Email__c,
                                                  Opportunity__r.Installer_Account__r.Operations_Email__c,
                                                  Opportunity__r.Account.PersonContactId,
                                                  Opportunity__r.Account.PersonEmail,
                                                  Opportunity__r.CoApplicant_Contact__r.Email,
                                                  Opportunity__r.Co_Applicant__c,  
                                                  Opportunity__r.Sales_Representative_Email__c,                                  
                                                  (SELECT Id, Installer_Only_Email__c, 
                                                   Stipulation_Data__r.Name,Status__c,
                                                   Stipulation_Data__r.Suppress_In_Portal__c,
                                                   Stipulation_Data__r.Email_Text__c,Underwriting__r.Opportunity__r.Account.Owner.Email,
                                                   ETC_Notes__c,Description__c,Upload_Document__c,
                                                   underwriting__r.opportunity__r.NonACH_APR__c,// added as part of Orange-2040
                                                   underwriting__r.opportunity__r.ACH_APR__c 
                                                   FROM Stipulations__r
                                                   WHERE Case__c = null)
                                                  FROM Underwriting_File__c
                                                  WHERE Id IN :ufIds
                                                 ];
        
        set<Id> oppIds = new set<Id>{};
            
            for (Underwriting_File__c uf : ufs) oppIds.add (uf.Opportunity__c);
        
        //  put parent Opps into a map
        
        map<Id,Opportunity> opportunitiesMap = new map<Id,Opportunity>([SELECT Id, Name, Upload_Comment__c, 
                                                                        Upload_Comment_Text__c, Upload_Comment_TS__c
                                                                        FROM Opportunity
                                                                        WHERE Id IN :oppIds]);
        
        //  process each UF:
        ID rectypeid = Schema.SObjectType.case.getRecordTypeInfosByName().get('Stipulation').getRecordTypeId();
        for (Underwriting_File__c uf : ufs)
        {
            if (uf.Stipulations__r.size() == 0) continue;
            
            Case c = new Case (Description = 'Stipulation for ' + uf.Name,
                               RecordTypeId = rectypeid,
                               Origin = 'Stipulation',
                               ContactId = uf.Opportunity__r.Account.PersonContactId,
                               OwnerId = uf.Opportunity__r.OwnerId,
                               AccountId = uf.Opportunity__r.AccountId,
                               Installer_Account__c = uf.Opportunity__r.Installer_Account__c,
                               Installer_Only_Stipulation_Email_Text__c = '',
                               Stipulation_Email_Text__c = '',
                               Stipulation_Status__c = '',
                               Upload_Document__c = false,
                               Installer_Only_Email_1__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_1__c,
                               Installer_Only_Email_2__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_2__c,
                               Installer_Only_Email_3__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_3__c,
                               Underwriting_File__c = uf.Id,
                               Installer_Email__c = uf.Opportunity__r.Installer_Account__r.Installer_Email__c,
                               Operations_Email__c = uf.Opportunity__r.Installer_Account__r.Operations_Email__c
                               ,Stip_Email_5__c = uf.Opportunity__r.CoApplicant_Contact__r.Email,
                               Co_Applicant_Name__c = uf.Opportunity__r.Co_Applicant__c
                              ); 
            if(!uf.Opportunity__r.Account.Owner.Email.contains('edwservice'))
            {
                c.Account_Owner_Email__c = uf.Opportunity__r.Account.Owner.Email;
            }
            
            //Added by Sejal as part of 8616 - Start
            If(uf.Opportunity__r.Sales_Representative_Email__c!=null)
                c.Sales_Representative_Email__c = uf.Opportunity__r.Sales_Representative_Email__c;
            //Added by Sejal as part of 8616 - End
            
            set<String> stipEmailTypes = new set<String>{'Send to Customer', 'Send to Installer and Customer'};
                
                if (stipEmailTypes.contains (uf.Opportunity__r.Installer_Account__r.Stipulation__c))
            {
                c.Stip_Email_1__c = uf.Opportunity__r.Account.PersonEmail;
                
                if (uf.Opportunity__r.Installer_Account__r.Stipulation__c == 'Send to Installer and Customer')
                {
                    c.Stip_Email_2__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_1__c;
                    c.Stip_Email_3__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_2__c;
                    c.Stip_Email_4__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_3__c;
                }
            }
            else
            {
                c.Stip_Email_1__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_1__c;
                c.Stip_Email_2__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_2__c;
                c.Stip_Email_3__c = uf.Opportunity__r.Installer_Account__r.Stip_Email_3__c;
            }
            
            casesToInsert.add(c);
        }
        
        //  insert the Cases
        
        insert casesToInsert;
        
        map<String,Case> UfToCaseMap = new map<String,Case>{};
            
            //  put the new Cases into a map
            
            for (Case c : casesToInsert)
        {
            UfToCaseMap.put (c.Description, c);
        }
        
        //  process the UFs again,  wiring the Stips to the appropriate Case, and update the parent Opp
        List<String> lstOppRecs = new List<String>();
        for (Underwriting_File__c uf : ufs)
        {
            Opportunity o = opportunitiesMap.get (uf.Opportunity__c);
            
            Case c = UfToCaseMap.get ('Stipulation for ' + uf.Name);
            
            buildEmailFields(c, o, uf.Stipulations__r);
            
            stipsToUpdate.addAll(uf.Stipulations__r);
        }
        
        //  update the Opps and the Stips
        //Added for Orange-1156 starts 08/21/2018
        for(Opportunity opp :opportunitiesMap.values()){
            if(null != opp.Upload_Comment_Text__c && string.isNotBlank(opp.Upload_Comment_Text__c))
                lstOppRecs.add(opp.id+':::'+opp.Upload_Comment_Text__c);
        }
        system.debug('### lstOppRecs'+ lstOppRecs);
        if(!lstOppRecs.isEmpty())
        {
            //Updated below logic By Adithya as per 2777
            //start
            if(System.isFuture() == false)
                updateComments(lstOppRecs);
            else
                updateCommentsNonFuture(lstOppRecs);
            //End
            
        }
        //Added for Orange-1156 ends
        // update opportunitiesMap.values();
        update stipsToUpdate;
        update casesToInsert;
        
        return null;
    }
    
    //  **************************************************************
    
    //  NOTE: buildEmailFields is also called from trigger StipulationProcessStatusChange
    //        with null in Opportunity parameter
    
    public static void buildEmailFields (Case c, Opportunity o, list<Stipulation__c> stips)
    {
        system.debug('### Entered into buildEmailFields() of '+ StipulationsSupport.class);
        for (Stipulation__c s : stips)
        {
            String emailText = '';
            if(!s.Underwriting__r.Opportunity__r.Account.Owner.Email.contains('edwservice'))
            {
                c.Account_Owner_Email__c = s.Underwriting__r.Opportunity__r.Account.Owner.Email;
            }
            // if (s.Stipulation_Data__r.Email_Text__c != null) emailText += s.Stipulation_Data__r.Email_Text__c;
            system.debug('emailText ***'+emailText );
            if (s.Stipulation_Data__r.Email_Text__c != null) 
                emailText += s.Stipulation_Data__r.Email_Text__c;
            
            // Added as part of ORANGE-2040 Sprint 12,2019 by kalyani - Start
            if(s.Stipulation_Data__r.Name == 'APR' && emailText !=null){
                emailText = emailText.replace('<ACH APR>',String.valueOf(s.underwriting__r.opportunity__r.ACH_APR__c.setScale(2))+'%'); //Added setScale as part of 14776 
                if(null != s.underwriting__r.opportunity__r.NonACH_APR__c)
                    emailText = emailText.replace('<NON ACH APR>',String.valueOf(s.underwriting__r.opportunity__r.NonACH_APR__c.setScale(2))+'%'); //Added setScale as part of 14776                
            }// Ended as part of ORANGE-2040  Sprint 12,2019 by kalyani - End
            
            if (s.ETC_Notes__c != null){
                emailText +=s.ETC_Notes__c;
            }
            //Added if condition as part of Orange-2713
            if(s.Stipulation_Data__r.Name != 'EQP' && s.Stipulation_Data__r.Name != 'INV'){
                if (s.Installer_Only_Email__c == true)
                {   
                    if(emailText !='')
                    {
                        c.Installer_Only_Stipulation_Email_Text__c += '*    ' + emailText + '\n\n';
                        c.Installer_Stipulation_Text_Populated__c = true;
                    }
                    
                }   
                else{
                    if(emailText !='')
                    {
                        c.Stipulation_Email_Text__c += '*    ' + emailText + '\n\n';
                        c.Stipulation_Text_Populated__c = true;
                    }
                }
            }
            if(s.Upload_Document__c && !c.Upload_Document__c){
                c.Upload_Document__c = true;
            }
            if(s.Installer_Only_Email__c && !c.Installer_Only_Email__c){
                c.Installer_Only_Email__c = true;
            }
            
            
            if (o != null)
            {
                if (o.Upload_Comment_Text__c == null) o.Upload_Comment_Text__c = '';
                //Added As part of Orange-62 By Brahmeswar
                if(o.Upload_Comment_Text__c != null && !String.isEmpty(o.Upload_Comment_Text__c) && o.Upload_Comment_Text__c !='' && emailText !='' && s.Stipulation_Data__r.Suppress_In_Portal__c==false) //Added By Kalyani on 26/08/2019 as part of Orange-8380
                {
                    o.Upload_Comment_Text__c +='********************'+'\n' +'('+s.Stipulation_Data__r.Name+')'+ emailText;
                    System.debug('upload commenttext'+'********************'+'\n' +'('+s.Stipulation_Data__r.Name+')'+ emailText);
                }
                else{
                    if(emailText !='' && s.Stipulation_Data__r.Suppress_In_Portal__c==false)  //Added By Kalyani on 26/08/2019 as part of Orange-8380
                        o.Upload_Comment_Text__c = '('+s.Stipulation_Data__r.Name+')'+ emailText;
                }
                
                
                o.Upload_Comment__c = true;
                o.Upload_Comment_TS__c = System.now();              
            }
            
            s.Status__c = 'In Progress';
            s.Case__c = c.Id;
            //added By Adithya as part of ORANGE-14701 on 11/25/2019
            c.Suppress_Completed_Email_Alert__c = s.Suppress_Completed_Email_Alert__c;
            //Added by Adithya to fix multiple email alerts issue
            //Start
            if(string.isBlank(c.Stipulation_Status__c))
            {
                c.Stipulation_Status__c  = s.Status__c;
            }else{
                if(!c.Stipulation_Status__c.contains(s.Status__c))
                {
                    c.Stipulation_Status__c = c.Stipulation_Status__c +';'+s.Status__c;
                }
            }
            //End
        }
        
        system.debug('### Exited from buildEmailFields() of '+ StipulationsSupport.class);
    }
    
    @future
    public static void updateComments(List<String> oppRecs){
        system.debug('###oppRecs '+oppRecs);
        List<Opportunity> lstOpp = new List<Opportunity>();
        for(String opp :oppRecs){
            String[] lstData = opp.split(':::');
            //Updated By 
            if(!lstData.isEmpty()){
                Opportunity oppObj = new  Opportunity(id = lstData[0],Upload_Comment_Text__c = lstData[1], Upload_Comment__c = true, Upload_Comment_TS__c = System.now() );
                lstOpp.add(oppObj);
            }
        }
        system.debug('###lstOpp '+lstOpp);
        update lstOpp;
    }
    /**
* Description: invoking below method in case of future method
*
* @param    Opportunity reocrd
*/
    public static void updateCommentsNonFuture(List<String> oppRecs){
        system.debug('###oppRecs '+oppRecs);
        List<Opportunity> lstOpp = new List<Opportunity>();
        for(String opp :oppRecs){
            String[] lstData = opp.split(':::');
            if(!lstData.isEmpty()){
                Opportunity oppObj = new  Opportunity(id = lstData[0],Upload_Comment_Text__c = lstData[1], Upload_Comment__c = true, Upload_Comment_TS__c = System.now() );
                lstOpp.add(oppObj);
            }
        }
        system.debug('###lstOpp '+lstOpp);
        update lstOpp;
    }
}