public class ApplicationIntegrationUtility {

    public static SLF_Boomi_API_Details__c objIntegrationDetails;
    
    public static integer updateRecordFromIntegration(Id opportunityId, String applicationId){
        return (ProcessApplicationIntegrationRequest(opportunityId, applicationId));
    }

    @future(callout=true)
    public static void updateRecordFromIntegrationFuture(Id opportunityId, String applicationId){
        ProcessApplicationIntegrationRequest(opportunityId, applicationId);
    }

    public static integer ProcessApplicationIntegrationRequest(Id opportunityId, String applicationId){
        
        Map<String,SLF_Boomi_API_Details__c> integrationDetailsMap = SLF_Boomi_API_Details__c.getAll();
        objIntegrationDetails = integrationDetailsMap.get('Application Integration');
       
        
        Blob headerValue = Blob.valueOf(SLFEncryption.DecryptData(objIntegrationDetails.Username__c) + ':' + SLFEncryption.DecryptData(objIntegrationDetails.Password__c));
        String applicationAuthHeader = 'Basic ' +EncodingUtil.base64Encode(headerValue);

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setTimeout(Integer.valueOf(objIntegrationDetails.Timeout__c));
        String endpoint = objIntegrationDetails.Endpoint_URL__c + applicationId;
        system.debug('### endpoint URL - '+endpoint);
        system.debug('### Auth Header: '+ applicationAuthHeader);
        request.setHeader('Authorization', applicationAuthHeader);
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        
        Integer code;
        code = 0;
        
        String responseBody = '';
        try{
            HttpResponse response = http.send(request);
            if(response.getStatusCode() != 200) { throw new custException('First API pass failed. Trying again.'); }
            responseBody = response.getBody();
            system.debug('### responseBody  - '+responseBody);
        }
        catch(Exception e){
            system.debug('### ApplicationIntegrationUtility.ProcessApplicationIntegrationRequest():'+e.getLineNumber()+':'+ e.getMessage() + '###' +e.getStackTraceString());
            
           ErrorLogUtility.writeLog('ApplicationIntegrationUtility.ProcessApplicationIntegrationRequest()',e.getLineNumber(),'ProcessApplicationIntegrationRequest()',e.getMessage() + '###' +e.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(e,null,null,null,null));   
            try{
                HttpResponse response = http.send(request);
                if(response.getStatusCode() != 200) { throw new custException('There was an error from the integration server; please Contact Sunlight Financial for help.'); }
                responseBody = response.getBody();
            }
            catch(Exception e2){
                system.debug('### ApplicationIntegrationUtility.ProcessApplicationIntegrationRequest():'+e2.getLineNumber()+':'+ e2.getMessage() + '###' +e2.getStackTraceString());
            
              ErrorLogUtility.writeLog('ApplicationIntegrationUtility.ProcessApplicationIntegrationRequest()',e2.getLineNumber(),'ProcessApplicationIntegrationRequest()',e2.getMessage() + '###' +e2.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(e2,null,null,null,null));   
                throw new custException('Could not connect to server; please Contact Sunlight Financial for help.');
            } 
        }
        
        try{
        
            ApplicationIntegrationData aid = ApplicationIntegrationData.parse(responseBody);
            String[] ficoDateStrings = aid.FICODate.split('-');
            String[] birthDateStrings = aid.PersonBirthdate.split('-');
            String[] coAppBirthDateStrings = aid.CoAppPersonBirthdate.split('-');
    
            Decimal employmentYears = null;
            Integer employmentMonths = null;
    
            if (aid.LengthOfEmployment !=null && aid.LengthOfEmployment.isNumeric()) 
            {
                employmentYears = Integer.valueOf(aid.LengthOfEmployment) / 365.0;
                employmentMonths = (12 * (employmentYears - employmentYears.intValue())).intValue();
            }
    
            Date ficoDate = null;
            Date birthDate = null;
            Date coAppBirthDate = null;

            if (ficoDateStrings.size() == 3)
            {
                ficoDate = Date.newInstance(Integer.valueOf(ficoDateStrings[0]), 
                                                Integer.valueOf(ficoDateStrings[1]), 
                                                Integer.valueOf(ficoDateStrings[2]));
            }
    
            if (birthDateStrings.size() == 3)
            {
                birthDate = Date.newInstance(Integer.valueOf(birthDateStrings[0]), 
                                                Integer.valueOf(birthDateStrings[1]), 
                                                Integer.valueOf(birthDateStrings[2]));
            }
    
            if (coAppBirthDateStrings.size() == 3)
            {
                coAppBirthDate = Date.newInstance(Integer.valueOf(coAppBirthDateStrings[0]), 
                                                    Integer.valueOf(coAppBirthDateStrings[1]), 
                                                    Integer.valueOf(coAppBirthDateStrings[2]));
            }


            String firstName = aid.FirstName;
            String lastName = aid.LastName;
    
            // Retrieve Accounts to update
            Opportunity opp = [SELECT Id, AccountId,EDW_Originated__c,FNI_Application_ID__c,FNI_Reference_Number__c, Co_Applicant__c, Account.FirstName, Account.LastName FROM Opportunity WHERE Id = :opportunityId];

            List<Account> accountsToUpdate = new List<Account>();
            // Last Name Check
            if(opp.Account.LastName == lastName)
            {
                accountsToUpdate.add(new Account(
                Id = opp.AccountId,
                DTI__c = aid.DTI,
                Mortgage_Balance__c = aid.MortgageBalance,
                Employer_Name__c = aid.EmployerName,
                Length_of_Employment_Months__c = employmentMonths,
                Length_of_Employment_Years__c = employmentYears,
                PersonEmail = aid.PersonEmail,
                Phone = aid.Phone,
                Annual_Income__c = aid.AnnualIncome,
                FICODate__c = ficoDate,
                FICO_Score__c = aid.FICOScore,
                PersonBirthdate = birthDate,
                SSN__c = aid.SSN
                ));

                if(opp.Co_Applicant__c != null){
                    accountsToUpdate.add(new Account(
                        Id = opp.Co_Applicant__c,
                        FICO_Score__c = aid.CoAppFICOScore,
                        PersonBirthdate = coAppBirthDate,
                        SSN__c = aid.CoAppSSN
                        ));
                }
            }
            else{
                code = 1;
            }

            if(accountsToUpdate.size() > 0){
                try {
                    update accountsToUpdate;

                } 
                catch(Exception e) {
                    code = 2;
                }
            }
            //Updated by Adithya as part of ORANGE-6923
            //start
            if(opp.FNI_Application_ID__c != opp.FNI_Reference_Number__c && opp.FNI_Reference_Number__c == applicationId && opp.EDW_Originated__c == false)
            {
                Opportunity oppRec = new Opportunity(id = opportunityId);
                oppRec.FNI_Application_ID__c = applicationId;
                update oppRec;
            }
            //End
        }catch(Exception err){
            system.debug('### ApplicationIntegrationUtility.ProcessApplicationIntegrationRequest():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('ApplicationIntegrationUtility.ProcessApplicationIntegrationRequest()',err.getLineNumber(),'ProcessApplicationIntegrationRequest()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        return( code );
    }
    
}