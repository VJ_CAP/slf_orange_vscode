@isTest(seeAllData=false)
public class ClosestMatchTest{    
    
    private testMethod static void ClosestMatchTest(){    
        Test.startTest();
        ClosestMatch.findClosestInteger(new List<Integer>{3,5,2},3,2);
        ClosestMatch.findClosestDecimal(new List<Decimal>{3,5,2},3,2);
        ClosestMatch.findClosestInteger(new List<Integer>{3,5,2},3,3);
        ClosestMatch.findClosestDecimal(new List<Decimal>{3,5,2},3,3);
        ClosestMatch.findClosestInteger(new List<Integer>{1,5,5},3,3);
        ClosestMatch.findClosestDecimal(new List<Decimal>{1,5,5},3,3);
        ClosestMatch.findClosestInteger(new List<Integer>{3,5,5},2,5);   
        ClosestMatch.findClosestDecimal(new List<Decimal>{1,5,5},1,9); 
        Test.stopTest();
    }
}