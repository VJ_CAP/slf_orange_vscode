/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkat               04/04/2019              Created
******************************************************************************************/
public class DualStepVerificationServiceImpl extends RESTServiceBase{
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse fetchData(RequestWrapper rw){
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        string errMsg = '';
        UnifiedBean respBean = new UnifiedBean();
        try{
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            respBean.users = new List<UnifiedBean.UsersWrapper>();
            if(reqData.users[0].selectedQuestionsAndAnswers!=null && !reqData.users[0].selectedQuestionsAndAnswers.isEmpty() && reqData.users[0].ignoreCount!=null){
                errMsg= ErrorLogUtility.getErrorCodes('214',null);
                gerErrorMsg(respBean,errMsg,'214');
                iRestRes = (IRestResponse)respBean;
                res.response= iRestRes;
                return iRestRes;
            }else{
                List<User> lstUser = [SELECT Id,email,userName,IsActive,Hash_Id__c,Ignore_Count__c,contactId,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c 
                                      FROM user WHERE Id=:UserInfo.getUserId() and IsActive=true];
                if(lstUser!=null && lstUser.size()>0){
                    User objUser = lstUser.get(0);
                    //Added by Sreekar as part of Orange-11105 - Start
                    User_Login_History__c objLoginHistory = new User_Login_History__c(); 
                    
                    if(reqData.users[0].ipAddress != null && reqData.users[0].version != null && reqData.users[0].source != null){
                        objLoginHistory.OwnerId = objUser.Id;
                        objLoginHistory.sourceIP__c = reqData.users[0].ipAddress;
                        objLoginHistory.version__c = reqData.users[0].version;
                        objLoginHistory.source__c = reqData.users[0].source;
                        objLoginHistory.Logged_In_User__c = objUser.Id;
                        insert objLoginHistory;

                    }
                    //Added by Sreekar as part of Orange-11105 - End
                    system.debug('###objLoginHistory'+objLoginHistory);
                    // save user selected question and answers in user object
                    if(reqData.users[0].selectedQuestionsAndAnswers!=null && !reqData.users[0].selectedQuestionsAndAnswers.isEmpty()){
                        if(String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[0].question) && String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[0].answer)){
                            objUser.Question1__c=reqData.users[0].selectedQuestionsAndAnswers[0].question;
                            objUser.Answer1__c=reqData.users[0].selectedQuestionsAndAnswers[0].answer;
                        }
                        if(String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[1].question) && String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[1].answer)){
                            objUser.Question2__c=reqData.users[0].selectedQuestionsAndAnswers[1].question;
                            objUser.Answer2__c=reqData.users[0].selectedQuestionsAndAnswers[1].answer;
                        }
                        if(String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[2].question) && String.isNotBlank(reqData.users[0].selectedQuestionsAndAnswers[2].answer)){
                            objUser.Question3__c=reqData.users[0].selectedQuestionsAndAnswers[2].question;
                            objUser.Answer3__c=reqData.users[0].selectedQuestionsAndAnswers[2].answer;
                        }
                        respBean.returnCode = '200';
                        respBean.message = ErrorLogUtility.getErrorCodes('200', null);
                        objUser.Ignore_Count__c=0;
                        
                        update objUser;
                        
                        generateResponse(respBean, objUser,reqData); 
                    }else if(reqData.users[0].ignoreCount!=null){
                        // update ignore count
                        objUser.Ignore_Count__c=reqData.users[0].ignoreCount;
                        update objUser;
                        
                        generateResponse(respBean, objUser, reqData);
                    }else{
                        generateResponse(respBean, objUser, reqData); 
                    }
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('295',null);
                    gerErrorMsg(respBean,errMsg,'295');
                }
            }
            
            iRestRes = (IRestResponse)respBean;
            // res.response= iRestRes;
            
        }catch(Exception err){
            
            system.debug('### DualStepVerificationServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('DualStepVerificationServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            errMsg= ErrorLogUtility.getErrorCodes('400',null);
            gerErrorMsg(respBean,errMsg,'400');
            iRestRes = (IRestResponse)respBean;
            res.response= iRestRes;
        }
        return iRestRes;
    }
    
    public void generateResponse(UnifiedBean respBean,User objUser, UnifiedBean reqData){
        User objUserRec = new User();
        objUserRec =[SELECT id,email,userName,IsActive,Hash_Id__c,Ignore_Count__c,contactId,contact.Reset_Password_Flag__c,Question_Counter__c,Answer1__c,Answer2__c,Answer3__c,Question1__c,Question2__c,Question3__c,Ask_Questions__c 
                     FROM user WHERE Id=:objUser.Id and IsActive=true];
        UnifiedBean.UsersWrapper objUserWrp = new UnifiedBean.UsersWrapper();
        objUserWrp.questions = new List<UnifiedBean.SelectedQuestionsWrapper>();
        UnifiedBean.SelectedQuestionsWrapper questionsWrp = new UnifiedBean.SelectedQuestionsWrapper();
        if(objUserRec.Ignore_Count__c!=null){
            objUserWrp.ignoreCount=Integer.valueOf(objUserRec.Ignore_Count__c);
        }else
            objUserWrp.ignoreCount=0;
        objUserWrp.askQuestions=objUserRec.Ask_Questions__c;
        objUserWrp.email=reqData.users[0].email;
        //objUserWrp.password=reqData.users[0].password;
        respBean.access_token=UserInfo.getSessionId();
        List<Security_Question__mdt> securityQuestionList = [SELECT id,Question__c FROM Security_Question__mdt];
        
        if(!securityQuestionList.isEmpty()){
            for(Security_Question__mdt objMtd:securityQuestionList){
                questionsWrp = new UnifiedBean.SelectedQuestionsWrapper();
                questionsWrp.question=objMtd.Question__c;
                objUserWrp.questions.add(questionsWrp);
            } 
            respBean.users.add(objUserWrp);
            respBean.returnCode = '200';
        }else{
            // Security questions not configured
        }
    }
    /**
* method
**/
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        List<UnifiedBean.ErrorWrapper> errorList = new List<UnifiedBean.ErrorWrapper>();
        UnifiedBean.ErrorWrapper errorWraper = new UnifiedBean.ErrorWrapper();
        errorWraper.errorMessage = sErrorMsg;
        errorList.add(errorWraper);
        objUnifiedBean.error = errorList;
        objUnifiedBean.returnCode = sReturnCode;
    }
}