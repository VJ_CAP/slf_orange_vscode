public class DrawRequestUtil {
    
    public static void recalculate(String OpptyId){ 
        system.debug('### Entered into DrawRequestUtil.recalculate() of '+DrawRequestUtil.class);
        
        try
        {
            List<Underwriting_file__c> lstUnderWriting = [select id,Approved_for_Payments__c, Amount_Remaining__c,Total_Amount_Drawn__c,opportunity__r.id,opportunity__r.Has_Open_Stip__c,opportunity__r.Block_Draws__c, opportunity__r.Draw_Window_Expiration__c, opportunity__r.Combined_Loan_Amount__c, opportunity__r.Max_Draw_Available__c,opportunity__r.Draw_Carry_Over__c, opportunity__r.Primary_Applicant_Email__c, opportunity__r.Co_Applicant__r.PersonEmail,opportunity__r.Max_Draw_Count__c,opportunity__r.All_HI_Required_Documents__c,Opportunity__r.Installer_Account__c,(select id,Level__c,Amount__c,Project_Complete_Certification__c,Status__c from Draw_Requests__r where Status__c = 'Approved' order by level__c) from underwriting_file__c where opportunity__r.id=:OpptyId];
            if(lstUnderWriting!=null && !lstUnderWriting.isEmpty()) 
            {
                List<String> lstDrawnLevels = new List<String>();
                Underwriting_file__c objUnderWriting = lstUnderWriting.get(0);
                double totalAmountDrawn = objUnderWriting.Total_Amount_Drawn__c!=null?objUnderWriting.Total_Amount_Drawn__c : 0;
                double approvedLoanAmount = objUnderWriting.opportunity__r.Combined_Loan_Amount__c!=null ? objUnderWriting.opportunity__r.Combined_Loan_Amount__c : 0;
                Double drawCarriedOver = objUnderWriting.opportunity__r.Draw_Carry_Over__c!=null ? objUnderWriting.opportunity__r.Draw_Carry_Over__c : 0;
                Decimal maxDrawCount = objUnderWriting.opportunity__r.Max_Draw_Count__c!=null?objUnderWriting.opportunity__r.Max_Draw_Count__c:0;
                Boolean isOpptyUpdated = false;
                
                Opportunity oppUpdate = new Opportunity();
                
                List<Draw_Requests__c> lstDrawRequests = objUnderWriting.Draw_Requests__r;
                
                //Check what all Drawn levels completed
                if(lstDrawRequests!=null && !lstDrawRequests.isEmpty()){
                    for(Draw_Requests__c objDrawReqs : lstDrawRequests)
                    {
                        lstDrawnLevels.add(objDrawReqs.level__c);                                    
                    }
                }
                
                //Query Draw Allocations for the installer and prepare a map(DrawLevel,DrawAllocation in %)
                Map<String,Decimal> allocations = new Map<String,Decimal>();
                List<Draw_Allocation__c> lstDrawAllocation = [select id,Draw_Allocation__c,Level__c,Account__c from Draw_Allocation__c where Account__c=:objUnderWriting.Opportunity__r.Installer_Account__c];
                if(lstDrawAllocation!=null && !lstDrawAllocation.isEmpty()){
                    for(Draw_Allocation__c objDrawAllocations : lstDrawAllocation){
                        allocations.put(objDrawAllocations.Level__c,objDrawAllocations.Draw_Allocation__c);
                    }
                }                       
                System.debug('### Draw Allocations.'+allocations);
                System.debug('### Draw Requests:'+lstDrawRequests);
                
                List<Draw_Requests__c> insrtDrawRequestList = new List<Draw_Requests__c>();
                
                if(lstDrawRequests.isEmpty())
                {
                    oppUpdate = new Opportunity();
                    oppUpdate.id=OpptyId;
                    System.debug('allocations.get(String.ValueOf(1))::'+allocations.get(String.ValueOf(1)));
                    System.debug('approvedLoanAmount::'+approvedLoanAmount);
                    oppUpdate.Draw_Carry_Over__c = allocations.get(String.ValueOf(1));
                    oppUpdate.Max_Draw_Available__c = approvedLoanAmount;
                    isOpptyUpdated = true;
                }
                
                Decimal pendingPercent = 0;
                for(Draw_Requests__c drawRequestIterate : lstDrawRequests)
                {
                    System.debug('### Into For Loop ');
                    Double requestedDrawAmount = drawRequestIterate.Amount__c;
                    Double totalAllowedDrawPercent = 0;
                    Double totalAllowedMaxDrawAmount = 0;
                    Decimal allocationPercentForNext = 0;
                    Decimal allocationPercent = allocations.get(String.ValueOf(drawRequestIterate.Level__c));
                   
                    Decimal actualPercent = (requestedDrawAmount/approvedLoanAmount)*100;
                    System.debug('requestedDrawAmount::'+requestedDrawAmount);
                    System.debug('approvedLoanAmount::'+approvedLoanAmount);
                    //Decimal pendingPercent =  allocationPercent - actualPercent;//Commented by Deepika on 4/4/2019 as part of ORANGE - 2172
                    pendingPercent =  pendingPercent + (allocationPercent - actualPercent);//Added by Deepika on 4/4/2019 as part of ORANGE - 2172
                    System.debug('actualPercent::'+actualPercent);
                    System.debug('allocationPercent::'+allocationPercent);
                    System.debug('pendingPercent:::'+pendingPercent);
                    if(Decimal.valueOf(drawRequestIterate.Level__c) >= maxDrawCount || !allocations.containsKey(String.valueOf(drawRequestIterate.Level__c))){
                        allocationPercentForNext = 0;
                    }
                    else{
                        allocationPercentForNext = allocations.get(String.ValueOf(Integer.ValueOf(drawRequestIterate.Level__c)+1));
                    }
                    System.debug('allocationPercentForNext:::'+allocationPercentForNext);
                    //totalAllowedDrawPercent = allocationPercentForNext+pendingPercent;
                    
                    
                    system.debug('### check point 2');
                    system.debug('### allocationPercent :' + allocationPercent);
                    system.debug('### allocationPercentForNext :' + allocationPercentForNext);
                    system.debug('### pendingPercent :' + pendingPercent);
                    system.debug('### drawCarriedOver : '+ drawCarriedOver);
                    system.debug('### approvedLoanAmount : ' + approvedLoanAmount);
                    system.debug('### allocations.get(String.ValueOf(drawRequestIterate.Level__c+1)):'+allocations.get(String.ValueOf(Integer.ValueOf(drawRequestIterate.Level__c)+1)));
                    
                    totalAllowedDrawPercent = allocationPercentForNext+pendingPercent;
                    System.debug('### Tracking - totalAllowedDrawPercent: '+totalAllowedDrawPercent);
                    totalAllowedMaxDrawAmount = (totalAllowedDrawPercent/100) * approvedLoanAmount;
                    System.debug('### Tracking - totalAllowedMaxDrawAmount: '+totalAllowedMaxDrawAmount);
                    
                    oppUpdate = new Opportunity();
                    oppUpdate.id=OpptyId;
                    oppUpdate.Max_Draw_Available__c = totalAllowedMaxDrawAmount;
                    oppUpdate.Draw_Carry_Over__c = totalAllowedDrawPercent;
                    isOpptyUpdated = true;
                    
                }
                if(isOpptyUpdated)
                {
                    system.debug('### oppUpdate :'+oppUpdate);
                    update oppUpdate;
                }
            }
        }                            
        catch(Exception err){
            system.debug('### DrawRequestUtil.recalculate():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' DrawRequestUtil.recalculate()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            
        }
    }    
}