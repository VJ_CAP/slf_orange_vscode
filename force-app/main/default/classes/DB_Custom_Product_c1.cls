@isTest private without sharing class DB_Custom_Product_c1
{
  @isTest (SeeAllData=true)
  private static void testTrigger()
  {
    CRMfusionDBR101.DB_Globals.triggersDisabled = true;
    sObject testData = CRMfusionDBR101.DB_TriggerHandler.createTestData( Product__c.getSObjectType() );
     if (testData == null) return;

        testData.put('Offer_Code__c', null);  //  added by pdm
    Test.startTest();
    
    //  try/catch added by AD
    
    try
    {
        insert testData;
    }
    catch (Exception e)
    {
        return;
    }
    
    update testData;
    CRMfusionDBR101.DB_Globals.generateCustomTriggerException = true;
    update testData;
    delete testData;
    Test.stopTest();
  }
}