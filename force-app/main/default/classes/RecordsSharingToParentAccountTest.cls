@isTest
private class RecordsSharingToParentAccountTest {

    private static testMethod void OpportunityShareToParentAccountTest() {
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        insert trgLst; 
        
        
        String RecTypeId= [select Id from RecordType where (Name='Business Account') and (SobjectType='Account')].Id;
        Account accPar= new Account();
        accPar.name = 'Parent Account';
        accPar.RecordTypeID=RecTypeId;
        accPar.Type='Partner';
        accPar.BillingStateCode = 'CA';
        accPar.FNI_Domain_Code__c = 'TCU12';
        accPar.Installer_Legal_Name__c='Bright Solar Planet';
        accPar.Solar_Enabled__c=true;
        insert accPar;
        
        Account accInst= new Account();
        accInst.name = 'Installer Account';
        accInst.RecordTypeID=RecTypeId;
        accInst.Type='Partner';
        accInst.BillingStateCode = 'CA';
        accInst.FNI_Domain_Code__c = 'TCU123';
        accInst.ParentId = accPar.Id;
        accInst.Installer_Legal_Name__c='Bright Solar Planet';
        accInst.Solar_Enabled__c=true;
        insert accInst;
        
        Account accAppl = new Account();
        accAppl.name = 'Applicant Account';
        //accAppl.RecordTypeID='Person Account';
        accAppl.Type='PartnerExecutive';
        accAppl.BillingStateCode = 'CA';
        accAppl.FNI_Domain_Code__c = 'TCU';
        accAppl.Installer_Legal_Name__c='Bright Solar Planet';
        accAppl.Solar_Enabled__c=true;
        insert accAppl;
        
        Account accCAppl = new Account();
        accCAppl.name = 'CoApplicant Account';
        // accCAppl.RecordTypeID='Personal Account';
        accCAppl.Type='Partner';
        accCAppl.Installer_Legal_Name__c='Bright Solar Planet';
        accCAppl.Solar_Enabled__c=true;
        //accCAppl.BillingStateCode = 'CA';
        //accCAppl.FNI_Domain_Code__c = 'TCU1';
        //acclist.add(accCAppl);
        insert accCAppl;
        
        //UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = '');
        //insert r;
        Product__c prod = new Product__c();
        prod.Installer_Account__c =accInst.id;
        prod.Long_Term_Facility_Lookup__c =accInst.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.Internal_Use_Only__c = false;
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        prod.Product_Tier__c = '0';
        insert prod;
        
        
        Product_Proxy__c prodProxy = new Product_Proxy__c();
        prodProxy.ACH__c = true;
        prodProxy.APR__c = 2.99;
        prodProxy.Installer__c =accInst.id;
        prodProxy.Internal_Use_Only__c = false;
        prodProxy.Is_Active__c = true;
        prodProxy.SLF_ProductID__c = prod.Id;
        prodProxy.Name = 'testprod';
        prodProxy.State_Code__c ='CA';
        prodProxy.Term_mo__c = 120;          
        insert prodProxy;
        
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        insert trgConflag;
        
        TriggerFlags__c trgConflag1 = new TriggerFlags__c();
        trgConflag1.Name ='User';
        trgConflag1.isActive__c =true;
        insert trgConflag1; 
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = accPar.Id,Is_Default_Owner__c = true);
        insert con;  
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
                    
        // List<Opportunity> oppList = new List<Opportunity>();
        List<Opportunity> personOppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = accAppl.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Installer_Account__c = accInst.id;
        personOpp.Co_Applicant__c = accCAppl.id;
        personOpp.ForecastCategoryName= 'Pipeline';
        personOpp.SLF_Product__c = prod.id;
        insert personOpp;
        
        Map<id,Opportunity> Triggernewmap  = new Map<id,Opportunity>();
        Triggernewmap.put(personOpp.id,personOpp);
        RecordsSharingToParentAccount opportunityshare = new RecordsSharingToParentAccount();
        opportunityshare.OpportunityShareToParentAccount(Triggernewmap,new Map<id,Opportunity>()); 
        /*******************************************************************/
        
        
        Account accPar1= new Account();
        accPar1.name = 'Parent Account1';
        accPar1.RecordTypeID=RecTypeId;
        accPar1.Type='Partner';
        accPar1.BillingStateCode = 'CA';
        accPar1.FNI_Domain_Code__c = 'TCU122';
        accPar1.Installer_Legal_Name__c='Bright Solar Planet';
        accPar1.Solar_Enabled__c=true;
        insert accPar1;
        
        Account accInst1= new Account();
        accInst1.name = 'Installer Account1';
        accInst1.RecordTypeID=RecTypeId;
        accInst1.Type='Partner';
        accInst1.BillingStateCode = 'CA';
        accInst1.FNI_Domain_Code__c = 'TCU1223';
        accInst1.ParentId = accPar1.Id;
        accInst1.Installer_Legal_Name__c='Bright Solar Planet';
        accInst1.Solar_Enabled__c=true;
        insert accInst1;
        
        Account accAppl1 = new Account();
        accAppl1.name = 'Applicant Account1';
        //accAppl.RecordTypeID='Person Account';
        accAppl1.Type='PartnerExecutive';
        accAppl1.BillingStateCode = 'CA';
        accAppl1.FNI_Domain_Code__c = 'TCU21';
        accAppl1.Installer_Legal_Name__c='Bright Solar Planet';
        accAppl1.Solar_Enabled__c=true;
        insert accAppl1;
        
        Account accCAppl1 = new Account();
        accCAppl1.name = 'CoApplicant Account';
        // accCAppl.RecordTypeID='Personal Account';
        accCAppl1.Type='Partner';
        accCAppl1.Installer_Legal_Name__c='Bright Solar Planet';
        accCAppl1.Solar_Enabled__c=true;
        //accCAppl.BillingStateCode = 'CA';
        //accCAppl.FNI_Domain_Code__c = 'TCU1';
        //acclist.add(accCAppl);
        insert accCAppl1;
        
        //UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = '');
        //insert r;
        Product__c prod1 = new Product__c();
        prod1.Installer_Account__c =accInst.id;
        prod1.Long_Term_Facility_Lookup__c =accInst.id;
        prod1.Name='testprod';
        prod1.FNI_Min_Response_Code__c=9;
        prod1.Product_Display_Name__c='test';
        prod1.Qualification_Message__c ='testmsg';
        prod1.State_Code__c ='CA';
        prod1.ST_APR__c = 2.99;
        prod1.Long_Term_Facility__c = 'TCU';
        prod1.APR__c = 2.99;
        prod1.ACH__c = true;
        prod1.Internal_Use_Only__c = false;
        prod1.LT_Max_Loan_Amount__c =1000;
        prod1.Term_mo__c = 120;
        prod1.Product_Tier__c = '0';
        insert prod1;
        
        
        Product_Proxy__c prodProxy1 = new Product_Proxy__c();
        prodProxy1.ACH__c = true;
        prodProxy1.APR__c = 2.99;
        prodProxy1.Installer__c =accInst1.id;
        prodProxy1.Internal_Use_Only__c = false;
        prodProxy1.Is_Active__c = true;
        prodProxy1.SLF_ProductID__c = prod1.Id;
        prodProxy1.Name = 'testprod';
        prodProxy1.State_Code__c ='CA';
        prodProxy1.Term_mo__c = 120;          
        insert prodProxy1;
        
        
        Contact con1 = new Contact(LastName ='testCon1',AccountId = accPar1.Id,Is_Default_Owner__c = true);
        insert con1;   
        
        User portalUser1 = new User(alias = 'testc1', Email='testc1@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test1', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass1@mail.com',ContactId = con1.Id);
        insert portalUser1;
        opportunity oppobj = [select id,CloseDate,StageName,AccountId,Install_State_Code__c,Installer_Account__c,Co_Applicant__c,ForecastCategoryName,
                            SLF_Product__c from opportunity where id=:personOpp.id];
        oppobj.name = 'personOpp';
        oppobj.CloseDate = system.today();
        oppobj.StageName = 'Qualified';
        oppobj.AccountId = accAppl1.id;
        oppobj.Install_State_Code__c = 'CA';
        oppobj.Installer_Account__c = accInst1.id;
        oppobj.Co_Applicant__c = accCAppl1.id;
        oppobj.ForecastCategoryName= 'Pipeline';
        oppobj.SLF_Product__c = prod1.id;
        
        update oppobj; 
        
        Map<id,Opportunity> Triggeroldmap  = new Map<id,Opportunity>();
        Triggeroldmap.put(oppobj.id,oppobj);
        opportunityshare.OpportunityShareToParentAccount(Triggernewmap,Triggeroldmap);
        
        
        
        
        
        
    }
    
    private static testMethod void AccountShareToParentAccountTest() {
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        insert trgLst; 
        
        
        String RecTypeId= [select Id from RecordType where (Name='Business Account') and (SobjectType='Account')].Id;
        Account accPar= new Account();
        accPar.name = 'Parent Account';
        accPar.RecordTypeID=RecTypeId;
        accPar.Type='Partner';
        accPar.BillingStateCode = 'CA';
        accPar.FNI_Domain_Code__c = 'TCU12';
        accPar.Installer_Legal_Name__c='Bright Solar Planet';
        accPar.Solar_Enabled__c=true;
        insert accPar;
        
        Account accPar1 = new Account();
        accPar1.name = 'Parent Account1';
        accPar1.RecordTypeID=RecTypeId;
        accPar1.Type='Partner';
        accPar1.BillingStateCode = 'CA';
        accPar1.FNI_Domain_Code__c = 'TCU121';
        accPar1.Installer_Legal_Name__c='Bright Solar Planet';
        accPar1.Solar_Enabled__c=true;
        insert accPar1;
        
        
        Account accInst= new Account();
        accInst.name = 'Installer Account';
        accInst.RecordTypeID=RecTypeId;
        accInst.Type='Partner';
        accInst.BillingStateCode = 'CA';
        accInst.FNI_Domain_Code__c = 'TCU123';
        accInst.ParentId = accPar.Id;
        accInst.Installer_Legal_Name__c='Bright Solar Planet';
        accInst.Solar_Enabled__c=true;
        insert accInst;
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        insert trgConflag;
        
        TriggerFlags__c trgConflag1 = new TriggerFlags__c();
        trgConflag1.Name ='User';
        trgConflag1.isActive__c =true;
        insert trgConflag1;
        
        Contact con = new Contact(LastName ='testCon',AccountId = accPar.Id,Is_Default_Owner__c = true);
        insert con;
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1'];
        
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass@mail.com',ContactId = con.Id);
        //insert portalUser;
        
        RecordsSharingToParentAccount accountShare = new RecordsSharingToParentAccount();
        Map<id,Account> Triggernewmap = new Map<id,Account>();
        Triggernewmap.put(accInst.id,accInst);
        accountShare.AccountShareToParentAccount(Triggernewmap,new Map<id,Account>());
        Account accInst1 = [select id,ParentId from Account where id=:accInst.id];
        accInst1.ParentId = accPar1.Id;
        update accInst1;
        Map<id,Account> Triggeroldmap = new Map<id,Account>();
        Triggeroldmap.put(accInst1.id,accInst1);
        accountShare.AccountShareToParentAccount(Triggernewmap,Triggeroldmap);
        
        
        
        

    }

}