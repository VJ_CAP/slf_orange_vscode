public class jsonSightenQuoteResponse {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public class Incentives_Z {
		public String quoteincentive_id {get;set;} 
		public String assigned_to {get;set;} 
		public Integer upfront_amount {get;set;} 
		public String incentive_type {get;set;} 
		public List<Integer> annual_amounts {get;set;} 

		public Incentives_Z(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'quoteincentive_id') {
							quoteincentive_id = parser.getText();
						} else if (text == 'assigned_to') {
							assigned_to = parser.getText();
						} else if (text == 'upfront_amount') {
							upfront_amount = parser.getIntegerValue();
						} else if (text == 'incentive_type') {
							incentive_type = parser.getText();
						} else if (text == 'annual_amounts') {
							annual_amounts = new List<Integer>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								annual_amounts.add(parser.getIntegerValue());
							}
						} else {
							System.debug(LoggingLevel.WARN, 'Incentives_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Modified_by {
		public String uuid {get;set;} 
		public String link {get;set;} 
		public String natural_id {get;set;} 

		public Modified_by(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'uuid') {
							uuid = parser.getText();
						} else if (text == 'link') {
							link = parser.getText();
						} else if (text == 'natural_id') {
							natural_id = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Modified_by consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Messages {
		public List<Info> info {get;set;} 
		public List<Name> error {get;set;} 
		public List<Name> critical {get;set;} 
		public List<Name> warning {get;set;} 

		public Messages(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'info') {
							info = new List<Info>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								info.add(new Info(parser));
							}
						} else if (text == 'error') {
							error = new List<Name>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								error.add(new Name(parser));
							}
						} else if (text == 'critical') {
							critical = new List<Name>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								critical.add(new Name(parser));
							}
						} else if (text == 'warning') {
							warning = new List<Name>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								warning.add(new Name(parser));
							}
						} else {
							System.debug(LoggingLevel.WARN, 'Messages consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public List<Data> data {get;set;} 
	public Messages messages {get;set;} 
	public String db_table {get;set;} 
	public Integer status_code {get;set;} 
	public Integer count {get;set;} 

	public jsonSightenQuoteResponse(JSONParser parser) {
		while (parser.nextToken() != JSONToken.END_OBJECT) {
            System.debug('***Debug: next token: ' + parser.getCurrentToken());
			if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != JSONToken.VALUE_NULL) {
					if (text == 'data') {
                        
                        
						data = new List<Data>();
                        
                        //if next token is start object, just add it
                        if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                            System.debug('***Debug: current token: (object)' + parser.getCurrentToken());
                            data.add(new Data(parser));
                        }else{                        
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                       //         System.debug('***Debug: current token: ' + parser.getCurrentToken());
                                data.add(new Data(parser));
                           }
                        }
					} else if (text == 'messages') {
						messages = new Messages(parser);
					} else if (text == 'db_table') {
						db_table = parser.getText();
					} else if (text == 'status_code') {
						status_code = parser.getIntegerValue();
					} else if (text == 'count') {
						count = parser.getIntegerValue();
					} else {
						System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Ineligible {
		public List<Name> name {get;set;} 
		public Name reasons {get;set;} 
		public Integer count {get;set;} 

		public Ineligible(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = new List<Name>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								name.add(new Name(parser));
							}
						} else if (text == 'reasons') {
							reasons = new Name(parser);
						} else if (text == 'count') {
							count = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Ineligible consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Data {
		public Double install_cost_cash {get;set;} 
		public Proposals proposals {get;set;} 
		public String status {get;set;} 
		public Proposals milestones {get;set;} 
		public String date_created {get;set;} 
		public Modified_by modified_by {get;set;} 
		public Modified_by system_Z {get;set;} // in json: system
		public Modified_by created_by {get;set;} 
		public Integer initial_payment {get;set;} 
		public String uuid {get;set;} 
		public Product_eligibility product_eligibility {get;set;} 
		public Modified_by product {get;set;} 
		public Proposals tasks {get;set;} 
		public Double avg_monthly_solar_bill {get;set;} 
		public String natural_id {get;set;} 
		public Double consumer_savings_usd {get;set;} 
		public Modified_by owned_by_organization {get;set;} 
		public List<Incentives_Z> incentives {get;set;} 
		public Integer contract_term {get;set;} 
		public String date_updated {get;set;} 
		public Double consumer_savings_pct {get;set;} 
		public Modified_by owned_by_user {get;set;} 
		public Double rate_contract {get;set;} 
		public Double amount_financed {get;set;} 
		public Integer rate_esc_pct {get;set;} 

		public Data(JSONParser parser) {
            System.debug('***Debug: parser.getCurrentToken(): ' + parser.getCurrentToken());
			while (parser.nextToken() != JSONToken.END_OBJECT && parser.getCurrentToken() != JSONToken.END_Array) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'install_cost_cash') {
                            install_cost_cash = parser.getDoubleValue();
                        } else if (text == 'proposals') {
                            proposals = new Proposals(parser);
                        } else if (text == 'status') {
                            status = parser.getText();
                        } else if (text == 'milestones') {
                            milestones = new Proposals(parser);
                        } else if (text == 'date_created') {
                            date_created = parser.getText();
                        } else if (text == 'modified_by') {
                            modified_by = new Modified_by(parser);
                        } else if (text == 'system') {
                            system_Z = new Modified_by(parser);
                        } else if (text == 'created_by') {
                            created_by = new Modified_by(parser);
                        } else if (text == 'initial_payment') {
                            initial_payment = parser.getIntegerValue();
                        } else if (text == 'uuid') {
                            uuid = parser.getText();
                        } else if (text == 'product_eligibility') {
                            product_eligibility = new Product_eligibility(parser);
                        } else if (text == 'product') {
                            product = new Modified_by(parser);
                        } else if (text == 'tasks') {
                            tasks = new Proposals(parser);
                        } else if (text == 'avg_monthly_solar_bill') {
                            avg_monthly_solar_bill = parser.getDoubleValue();
                        } else if (text == 'natural_id') {
                            natural_id = parser.getText();
                        } else if (text == 'consumer_savings_usd') {
                            consumer_savings_usd = parser.getDoubleValue();
                        } else if (text == 'owned_by_organization') {
                            owned_by_organization = new Modified_by(parser);
                        } else if (text == 'incentives') {
                            incentives = new List<Incentives_Z>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                incentives.add(new Incentives_Z(parser));
                            }
                        } else if (text == 'contract_term') {
                            contract_term = parser.getIntegerValue();
                        } else if (text == 'date_updated') {
                            date_updated = parser.getText();
                        } else if (text == 'consumer_savings_pct') {
                            consumer_savings_pct = parser.getDoubleValue();
                        } else if (text == 'owned_by_user') {
                            owned_by_user = new Modified_by(parser);
                        } else if (text == 'rate_contract') {
                            rate_contract = parser.getDoubleValue();
                        } else if (text == 'amount_financed') {
                            amount_financed = parser.getDoubleValue();
                        } else if (text == 'rate_esc_pct') {
                            rate_esc_pct = parser.getIntegerValue();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }else
                {
                    System.debug('***Debug: ' + parser.getCurrentToken());
                }
            }
		}
	}
	
	public class Proposals {
		public String link {get;set;} 

		public Proposals(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'link') {
							link = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Proposals consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Eligible {
		public List<String> name {get;set;} 
		public Integer count {get;set;} 

		public Eligible(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = new List<String>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								name.add(parser.getText());
							}
						} else if (text == 'count') {
							count = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Eligible consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Info {
		public String message {get;set;} 
		public String msg_code {get;set;} 
		public String timestamp {get;set;} 

		public Info(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'message') {
							message = parser.getText();
						} else if (text == 'msg_code') {
							msg_code = parser.getText();
						} else if (text == 'timestamp') {
							timestamp = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Info consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Pending {
		public List<Name> name {get;set;} 
		public Integer count {get;set;} 

		public Pending(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = new List<Name>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								name.add(new Name(parser));
							}
						} else if (text == 'count') {
							count = parser.getIntegerValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Pending consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Product_eligibility {
		public Eligible eligible {get;set;} 
		public String user {get;set;} 
		public Pending pending {get;set;} 
		public Ineligible ineligible {get;set;} 
		public Ineligible warning {get;set;} 

		public Product_eligibility(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'eligible') {
							eligible = new Eligible(parser);
						} else if (text == 'user') {
							user = parser.getText();
						} else if (text == 'pending') {
							pending = new Pending(parser);
						} else if (text == 'ineligible') {
							ineligible = new Ineligible(parser);
						} else if (text == 'warning') {
							warning = new Ineligible(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Product_eligibility consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Name {

		public Name(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Name consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static jsonSightenQuoteResponse parse(String json) {
		return new jsonSightenQuoteResponse(System.JSON.createParser(json));
	}
}