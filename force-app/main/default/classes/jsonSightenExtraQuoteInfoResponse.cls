public class jsonSightenExtraQuoteInfoResponse {

    //Commented by Ravi as part of ORANGE-5464 because the structure has changed as part of Sighten API deprecation
    /*public class Messages {
        public List<Error> error;
        public List<Error> info;
        public List<Error> warning;
        public List<Error> critical;
    }

    public Messages messages;
    public Integer status_code;
    public Data data;

    public class Results {
        public Double monthly_payment_no_prepay;
        public Double monthly_payment;
        public Boolean ach_elected;
        public Double stub_payment_no_prepay;
    }

    public class Error {
    }

    public class Data {
        public Results results;
    }*/
    //Commented by Ravi as part of ORANGE-5464 because the structure has changed as part of Sighten API deprecation
    
    public class Messages {
        public List<Info> info;
        public List<Info> warning;
        public List<Info> critical;
        public List<Info> error;
    }

    public Integer pages;
    public List<Data> data;
    public Integer status_code;
    public Integer count;
    public Messages messages;
    public Boolean has_next_page;

    public class Data {
        public Double monthly_payment;
        public Double monthly_payment_no_prepay;
        public Boolean ach_elected;
        public Double stub_payment_no_prepay;
    }

    public class Info {
    }
    
    public static jsonSightenExtraQuoteInfoResponse parse(String json) {
        return (jsonSightenExtraQuoteInfoResponse) System.JSON.deserialize(json, jsonSightenExtraQuoteInfoResponse.class);
    }
}