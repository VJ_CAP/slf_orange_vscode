@isTest(seeAllData=false)
public class FinOpsServiceImplV2_test {
@testsetup static void createtestdata(){
    insertUserData();  
}

public static void insertUserData(){
    List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
    
    TriggerFlags__c trgAccflag = new TriggerFlags__c();
    trgAccflag.Name ='Account';
    trgAccflag.isActive__c =true;
    trgLst.add(trgAccflag);
    
    TriggerFlags__c trgConflag = new TriggerFlags__c();
    trgConflag.Name ='Contact';
    trgConflag.isActive__c =true;
    trgLst.add(trgConflag);
    
    TriggerFlags__c trgUserflag = new TriggerFlags__c();
    trgUserflag.Name ='User';
    trgUserflag.isActive__c =true;
    trgLst.add(trgUserflag);
    
    insert trgLst;
    
    
    //get the Account record type
    Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
    Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
    
    Profile p=[SELECT id FROM Profile WHERE Name='Partner Community User for Salesforce1' LIMIT 1];
    User u=[SELECT id FROM User WHERE ProfileId=:p.id LIMIT 1];
    
    //For getInstallerinfoServiceImpl Inserting Portal Test User 
    Account acc = new Account();
    acc.name = 'Test Account';
    acc.Installer_Email__c = 'test@gmail.com';
    acc.Inspection_Enabled__c = true;
    acc.Kitting_Enabled__c = true;
    acc.Permit_Enabled__c = true;
    acc.Kitting_Split__c = 10;
    acc.Permit_Split__c = 10;
    acc.Inspection_Split__c =10 ;
    acc.M0_Split__c=20;
    acc.M1_Split__c=20;
    acc.M2_Split__c=30;
    acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
    acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
    acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
    acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
    acc.BillingStateCode = 'CA';
    acc.FNI_Domain_Code__c = 'TCU';
    acc.Installer_Legal_Name__c='Bright planet Solar';
    acc.Solar_Enabled__c = true;
    acc.Box_Folder_ID_Net_Out__c='85779699960';
    acc.Box_Folder_ID_Remittance__c='66828620442';
    acc.Box_Folder_ID_HI_Remittance__c='70700686068';
    insert acc;
    System.debug('acc details'+acc);
    
    
    Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
    insert con;  
    
    map<String, Id> roleMap = new map<String, Id>();
    for(UserRole role:[select Id, DeveloperName from UserRole]){
        roleMap.put(role.DeveloperName, role.Id);
    }
    User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                               EmailEncodingKey='UTF-8', LastName='test', 
                               LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                               //UserRoleId = r.id,
                               timezonesidkey='America/Los_Angeles', 
                               username='testclass@mail.com',ContactId = con.Id);
    insert portalUser;
    System.debug('***************'+portalUser.contact.Account.Box_Folder_ID_Remittance__c);
     
}

/**
*
* Description: This method is used to cover PaymentApprovalServiceImpl
*
*/
private testMethod static void finOpsReportTest(){
    List<user> loggedInUsr = [select Id,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.Box_Folder_ID_Net_Out__c,contact.Account.Box_Folder_ID_Remittance__c,contact.Account.Box_Folder_ID_HI_Remittance__c from User where Email =: 'testc@mail.com'];
    System.debug('loggedinuser details'+loggedInUsr);
    System.debug('loggedinuser details'+loggedInUsr[0].contact.Account.Box_Folder_ID_Net_Out__c);
try  
{
  BoxPlatformApiConnection api = BoxAuthentication.getConnection ();
}
catch (Exception e) {}

    Map<String, String> userMap = new Map<String, String>();
    System.runAs(loggedInUsr[0]){
        Test.starttest();
        
        String req;
        req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"Remittance","projectCategory":"Solar"}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"NetOut","projectCategory":"Solar","boxAccessToken ":api}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"NetOut"}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"Installer"}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        req = '{"startDate":"2018-12-28","endDate":"2019-02-10","reportType":"Remittance","projectCategory":"Home"}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        req = '{"startDate":"2018-12-28","endDate":"2019-01-10","reportType":"Remittance1"}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        //null scenario
        req = '{"startDate":"","endDate":"","reportType":"","projectCategory":""}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        
        // if(dEndDate < dStartDate)
        req = '{"startDate":"2019-03-10","endDate":"2019-01-10","reportType":"Remittance"}';
        
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
        // if(dStartDate.daysBetween(dEndDate) > 45)
        req = '{"startDate":"2019-01-10","endDate":"2019-03-10","reportType":"Remittance"}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,req,'finopsreportV2');
        
                  
        
        Test.StopTest();

    }
}

}