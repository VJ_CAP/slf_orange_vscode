/**
* Description: Fetch nonACHApr information logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer                Date                Description
---------------------------------------------------------------------------
Udaya Kiran            Jul/05/2019           Created
*****************************************************************************************/
public without sharing class GetNonACHAprInfoDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+GetNonACHAprInfoDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean aprInfoResp = new UnifiedBean(); 
        aprInfoResp.error = new list<UnifiedBean.errorWrapper>();
        aprInfoResp.projects  = new List<UnifiedBean.OpportunityWrapper>();
        aprInfoResp.disclosures = new List<UnifiedBean.DisclosureWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        string dynamicQuery = '';
        boolean isProductInactive = false ;//Added by Adithya as part of Orange-8382
        List<Product__c> productLst = new List<Product__c>();
        string installStateCode = '';
        string defProdType = 'Solar'; //do we need this?
        Boolean isOptyExist = true;
        String externalId;

        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try{ 
            List<Opportunity> OpportunityObj = new List<Opportunity>();
            
            UnifiedBean.OpportunityWrapper retWrap = new UnifiedBean.OpportunityWrapper();
            
            /*if(reqData.projects[0].projectCategory != null && reqData.projects[0].projectCategory == 'Home')
            {
                aprInfoResp.returnCode = '200';
                aprInfoResp.projects.add(reqData.projects[0]);
                iRestRes = (IRestResponse)aprInfoResp;
                system.debug('### iRestRes - '+iRestRes);
                return iRestRes;
            }*/
            
            //Defaulting product loan type to Solar if not in the request
            if(null == reqData.projects[0].productType){
                system.debug('### defaulting product loan type to Solar');
                reqData.projects[0].productType = 'Solar';
            }
            
            //Defaulting project category to Solar if not in the request
            if(null == reqData.projects[0].projectCategory){
                system.debug('### defaulting project category to Solar');
                reqData.projects[0].projectCategory = 'Solar';
            }
            
            //If project Id or hashId or externalId is present in the request
            if((reqData.projects[0].id != null && String.isNotBlank(reqData.projects[0].id)) || (reqData.projects[0].hashId != null && String.isNotBlank(reqData.projects[0].hashId)) || (reqData.projects[0].externalId != null && String.isNotBlank(reqData.projects[0].externalId))){
                
                //Orange-8382 Added 2 fields in the below query
                dynamicQuery = 'SELECT Id,StageName,ProductTier__c,Application_Status__c,Requested_Product__r.is_Active__c,Requested_Product__c,SLF_Product__r.is_Active__c,Credit_Expiration_Date__c,Product_Loan_Type__c,isACH__c,language__c,Install_State_Code__c,Project_Category__c,Installer_Account__c,SLF_Product__c,SLF_Product__r.Term_mo__c,SLF_Product__r.APR__c,SLF_Product__r.NonACH_APR__c, SLF_Product__r.Promo_Term__c, SLF_Product__r.Promo_Fixed_Payment__c, SLF_Product__r.Promo_Percentage_Balance__c, SLF_Product__r.Promo_Percentage_Payment__c,  SLF_Product__r.Draw_Period__c, SLF_Product__r.Promo_APR__c, SLF_Product__r.NonACH_Promo_APR__c, Promo_APR__c, Promo_Term__c, Promo_Fixed_Payment__c, Promo_Percentage_Balance__c, Promo_Percentage_Payment__c, Draw_Period__c FROM Opportunity WHERE';
                
                if(reqData.projects[0].id != null && String.isNotBlank(reqData.projects[0].id)){
                    dynamicQuery = dynamicQuery+' Id ='+'\''+reqData.projects[0].id+'\'';
                }else if(reqData.projects[0].hashId != null && String.isNotBlank(reqData.projects[0].hashId)){
                    dynamicQuery = dynamicQuery+' Hash_Id__c ='+'\''+reqData.projects[0].hashId+'\'';
                }else if(reqData.projects[0].externalId != null && String.isNotBlank(reqData.projects[0].externalId)){
                    //Updated by Ravi as part of ORANGE-8407 START
                    //dynamicQuery = dynamicQuery+' Partner_Foreign_Key__c ='+'\''+reqData.projects[0].externalId+'\'';
                    List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                    if(!loggedInUsr.isEmpty() && loggedInUsr.size()>0){
                        if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                            externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                            dynamicQuery = dynamicQuery+' Partner_Foreign_Key__c ='+'\''+externalId+'\'';
                        }else{
                            errMsg = ErrorLogUtility.getErrorCodes('202', null);
                            aprInfoResp.returnCode = '202';
                        }
                    }
                    //Updated by Ravi as part of ORANGE-8407 END
                }
                
                System.debug('### dynamicQuery - '+dynamicQuery);
                OpportunityObj = Database.query(dynamicQuery);
                system.debug('### Oppty List - '+OpportunityObj);
                string tierVal = '0';
                
                //If OpportunityObj list is not empty
                if(!OpportunityObj.isEmpty() && OpportunityObj.size()>0){//Orange-8382 - made it as 'else if'
                    
                    //Added by Adithya as part of Orange-8382
                    //Start
                    if(null != OpportunityObj[0].Credit_Expiration_Date__c && (OpportunityObj[0].Application_Status__c == 'Auto Approved' || OpportunityObj[0].Application_Status__c == 'Manually Approved') && ((null != OpportunityObj[0].SLF_Product__c && OpportunityObj[0].SLF_Product__r.is_Active__c == false) || (null != OpportunityObj[0].Requested_Product__c && OpportunityObj[0].Requested_Product__r.is_Active__c == false))  && OpportunityObj[0].Credit_Expiration_Date__c >= system.today())
                    {
                        isProductInactive = true;
                    }
                    //End
                    dynamicQuery = '';
                        
                    //If Term APR isACH is in the request 
                    if(null != reqData.projects[0].term && null != reqData.projects[0].apr && null != reqData.projects[0].isACH){
                        
                        //Logic to build the query for Solar scenario
                        if(OpportunityObj[0].Project_Category__c == 'Solar' || reqData.projects[0].projectCategory == 'Solar'){
                            
                            System.debug('### Executing Solar logic');
                            
                            dynamicQuery = 'select Id, Promo_Fixed_Payment__c,Promo_APR__c,Promo_Term__c,NonACH_Promo_APR__c, Promo_Percentage_Balance__c, ACH__c, Draw_Period__c, APR__c, NonACH_APR__c, Product_Loan_Type__c, Long_Term_Facility__c from Product__c where Product_Loan_Type__c ='+'\''+reqData.projects[0].productType+'\''+' AND Installer_Account__c = '+'\''+OpportunityObj[0].Installer_Account__c+'\''+' AND Term_mo__c = '+reqData.projects[0].term+' AND State_Code__c = \''+ OpportunityObj[0].Install_State_Code__c+'\'';
                            
                            dynamicQuery = null != OpportunityObj[0].ProductTier__c?dynamicQuery + ' AND Product_Tier__c = '+'\''+OpportunityObj[0].ProductTier__c+'\'': dynamicQuery +' AND Product_Tier__c = '+'\''+tierVal+'\'';
                            
                            //Added by Adithya as part of Orange-8382
                            //Start
                            if(isProductInactive)
                            {
                                 //do nothing
                            }else{
                                dynamicQuery = dynamicQuery + ' AND Is_Active__c = TRUE';
                            }                           
                            //End
                            if(reqData.projects[0].isACH)
                            {
                                dynamicQuery = dynamicQuery + ' AND APR__c = '+reqData.projects[0].apr;
                            }else{
                                dynamicQuery = dynamicQuery + ' AND NonACH_APR__c = '+reqData.projects[0].apr;
                            }
                        }
                        //Logic to build the query for Home scenario
                        else if(OpportunityObj[0].Project_Category__c == 'Home' || reqData.projects[0].projectCategory == 'Home'){
                            
                            System.debug('### Executing Home logic');
                            
                            dynamicQuery = 'select Id, Promo_Fixed_Payment__c, Promo_Percentage_Balance__c, Draw_Period__c, ACH__c, APR__c, NonACH_APR__c, Product_Loan_Type__c, Long_Term_Facility__c from Product__c where Product_Loan_Type__c ='+'\''+reqData.projects[0].productType+'\''+' AND Installer_Account__c = '+'\''+OpportunityObj[0].Installer_Account__c+'\''+' AND Term_mo__c = '+reqData.projects[0].term+' AND Promo_APR__c = '+reqData.projects[0].promoAPR+' AND Promo_Term__c = '+reqData.projects[0].promoTerm+' AND Promo_Percentage_Payment__c = '+reqData.projects[0].promoPercentPayment+' AND State_Code__c = \''+ OpportunityObj[0].Install_State_Code__c+'\'';
                            
                            dynamicQuery = null != OpportunityObj[0].ProductTier__c?dynamicQuery + ' AND Product_Tier__c = '+'\''+OpportunityObj[0].ProductTier__c+'\'': dynamicQuery +' AND Product_Tier__c = '+'\''+tierVal+'\'';
                            
                            //Added by Adithya as part of Orange-8382
                            //Start
                            if(isProductInactive)
                            {
                                //do nothing
                            }else{
                                dynamicQuery = dynamicQuery + ' AND Is_Active__c = TRUE';
                            }
                            //End
                            if(reqData.projects[0].isACH)
                            {
                                dynamicQuery = dynamicQuery + ' AND APR__c = '+reqData.projects[0].apr;
                                dynamicQuery = dynamicQuery + ' AND Promo_APR__c = '+reqData.projects[0].promoAPR;
                            }else{
                                dynamicQuery = dynamicQuery + ' AND NonACH_APR__c = '+reqData.projects[0].apr;
                                dynamicQuery = dynamicQuery + ' AND NonACH_Promo_APR__c = '+reqData.projects[0].promoAPR;
                            }
                            
                        }
                    
                        system.debug('### dynamicQuery - '+dynamicQuery);
                        
                        if(dynamicQuery != ''){
                            productLst = Database.query(dynamicQuery);
                            system.debug('### productLst - '+productLst);
                        }
                        
                        if(!productLst.isEmpty()){
                            retWrap.Id = OpportunityObj[0].Id;
                            retWrap.term = reqData.projects[0].term;
                            //retWrap.apr = reqData.projects[0].apr;
                            retWrap.apr = productLst[0].APR__c;
                            retWrap.isACH = reqData.projects[0].isACH;
                            retWrap.promoTerm = reqData.projects[0].promoTerm;
                            retWrap.PromoAPR = reqData.projects[0].PromoAPR;
                            retWrap.productType = reqData.projects[0].productType;
                            retWrap.projectCategory = reqData.projects[0].projectCategory;
                            retWrap.promoPercentPayment = reqData.projects[0].promoPercentPayment;
                            
                            retWrap.promoFixedPayment = productLst[0].Promo_Fixed_Payment__c;
                            retWrap.promoBalance = productLst[0].Promo_Percentage_Balance__c;
                            retWrap.drawPeriod = Integer.valueOf(productLst[0].Draw_Period__c);
                            retWrap.nonACHApr = productLst[0].NonACH_APR__c;
                            system.debug('nonACHApr - '+productLst[0].NonACH_APR__c);
                            
                            aprInfoResp.returnCode = '200';
                            aprInfoResp.projects.add(retWrap); 
                            
                        }else{
                            errMsg = ErrorLogUtility.getErrorCodes('214', null);
                            aprInfoResp.returnCode = '214';
                        }
                    }
                    //If Term APR isACH is NOT in the request 
                    else if(null != OpportunityObj[0].SLF_Product__c && null == reqData.projects[0].term && null == reqData.projects[0].apr && null == reqData.projects[0].isACH){
                        
                        retWrap.Id = OpportunityObj[0].Id;
                        retWrap.term = OpportunityObj[0].SLF_Product__r.Term_mo__c;
                        retWrap.isACH = OpportunityObj[0].isACH__c;
                        retWrap.promoTerm = integer.valueOf(OpportunityObj[0].SLF_Product__r.Promo_Term__c);
                        
                        if(OpportunityObj[0].isACH__c){
                            retWrap.PromoAPR = OpportunityObj[0].SLF_Product__r.Promo_APR__c;
                            retWrap.apr = OpportunityObj[0].SLF_Product__r.APR__c;
                        }else{
                            retWrap.PromoAPR = OpportunityObj[0].SLF_Product__r.NonACH_Promo_APR__c;
                            retWrap.apr = OpportunityObj[0].SLF_Product__r.NonACH_APR__c;
                        }
                        
                        retWrap.productType = OpportunityObj[0].Product_Loan_Type__c;
                        retWrap.projectCategory = OpportunityObj[0].Project_Category__c;
                        retWrap.promoPercentPayment = OpportunityObj[0].SLF_Product__r.Promo_Percentage_Payment__c;
                        
                        retWrap.promoFixedPayment = OpportunityObj[0].SLF_Product__r.Promo_Fixed_Payment__c;
                        retWrap.promoBalance = OpportunityObj[0].SLF_Product__r.Promo_Percentage_Balance__c;
                        retWrap.drawPeriod = Integer.valueOf(OpportunityObj[0].SLF_Product__r.Draw_Period__c);
                        retWrap.nonACHApr = OpportunityObj[0].SLF_Product__r.NonACH_APR__c;
                        system.debug('nonACHApr - '+OpportunityObj[0].SLF_Product__r.NonACH_APR__c);
                        
                        aprInfoResp.returnCode = '200';
                        aprInfoResp.projects.add(retWrap);
                        
                    }
                    //If there is no product assigned to the opportunity and no Term APR isACH is NOT in the request 
                    else{
                        errMsg = ErrorLogUtility.getErrorCodes('214', null);
                        aprInfoResp.returnCode = '214';
                    }
                    
                }else{
                    /*errMsg = ErrorLogUtility.getErrorCodes('202', null);
                    aprInfoResp.returnCode = '202';
                    System.debug('### entered into if case - Error 202 invalid project id');*/
                    isOptyExist = false;
                }
            }
            
            //If project Id is NOT in the request but Term APR isACH is in the request 
            if(((null == reqData.projects[0].id || String.isBlank(reqData.projects[0].id)) && (null == reqData.projects[0].hashId || String.isBlank(reqData.projects[0].hashId)) && (null == reqData.projects[0].externalId || String.isBlank(reqData.projects[0].externalId))) || !isOptyExist){
                
                User loggedInUsr = [select Id, contact.AccountId from User where Id =: userinfo.getUserId()]; 
                
                if(String.isNotBlank(reqData.projects[0].installStateName))
                    installStateCode = SLFUtility.getStateCode(reqData.projects[0].installStateName);
                
                system.debug('### installStateCode - '+installStateCode);
                
                //If Term APR isACH is in the request 
                if(null != reqData.projects[0].term && null != reqData.projects[0].apr && null != reqData.projects[0].isACH){
                    
                    //Logic to build the query for Solar scenario 
                    if(reqData.projects[0].projectCategory == 'Solar'){
                        
                        System.debug('### Executing Solar logic');
                        
                        dynamicQuery = 'select Id, Promo_Fixed_Payment__c,Promo_Term__c,Promo_APR__c,NonACH_Promo_APR__c, Promo_Percentage_Balance__c, ACH__c, Draw_Period__c, APR__c, NonACH_APR__c, Product_Loan_Type__c, Long_Term_Facility__c from Product__c where Product_Tier__c = \'0\' AND Installer_Account__c = '+'\''+loggedInUsr.contact.AccountId+'\''+' AND Term_mo__c = '+reqData.projects[0].term+' AND Is_Active__c = TRUE'+' AND State_Code__c = \''+installStateCode+'\''+ ' AND Product_Loan_Type__c = \''+reqData.projects[0].productType+'\'';
                        
                        //dynamicQuery = dynamicQuery + ' AND APR__c = '+reqData.projects[0].apr;
                        
                        if(reqData.projects[0].isACH)
                        {
                            dynamicQuery = dynamicQuery + ' AND APR__c = '+reqData.projects[0].apr;
                        }else{
                            dynamicQuery = dynamicQuery + ' AND NonACH_APR__c = '+reqData.projects[0].apr;
                        }
                    }
                    
                    //Logic to build the query for Home scenario
                    else if(reqData.projects[0].projectCategory == 'Home'){
                        
                        System.debug('### Executing Home logic');
                        
                        dynamicQuery = 'select Id, Promo_Fixed_Payment__c, Promo_Percentage_Balance__c, Draw_Period__c, ACH__c, APR__c, NonACH_APR__c, Product_Loan_Type__c, Long_Term_Facility__c from Product__c where Product_Tier__c = \'0\' AND Product_Loan_Type__c ='+'\''+reqData.projects[0].productType+'\''+' AND Installer_Account__c = '+'\''+loggedInUsr.contact.AccountId+'\''+' AND Term_mo__c = '+reqData.projects[0].term+' AND Promo_APR__c = '+reqData.projects[0].promoAPR+' AND Promo_Term__c = '+reqData.projects[0].promoTerm+' AND Promo_Percentage_Payment__c = '+reqData.projects[0].promoPercentPayment+' AND Is_Active__c = TRUE'+' AND State_Code__c = \''+installStateCode+'\'';
                        
                        if(reqData.projects[0].isACH)
                        {
                            dynamicQuery = dynamicQuery + ' AND APR__c = '+reqData.projects[0].apr;
                            dynamicQuery = dynamicQuery + ' AND Promo_APR__c = '+reqData.projects[0].promoAPR;
                        }else{
                            dynamicQuery = dynamicQuery + ' AND NonACH_APR__c = '+reqData.projects[0].apr;
                            dynamicQuery = dynamicQuery + ' AND NonACH_Promo_APR__c = '+reqData.projects[0].promoAPR;
                        }
                        
                    }
                
                    system.debug('### dynamicQuery - '+dynamicQuery);
                    
                    if(dynamicQuery != ''){
                        productLst = Database.query(dynamicQuery);
                        system.debug('### productLst - '+productLst);
                    }
                    
                    if(!productLst.isEmpty()){
                        retWrap.term = reqData.projects[0].term;
                        //retWrap.apr = reqData.projects[0].apr;
                        retWrap.apr = productLst[0].APR__c;
                        retWrap.isACH = reqData.projects[0].isACH;
                        retWrap.promoTerm = reqData.projects[0].promoTerm;
                        retWrap.PromoAPR = reqData.projects[0].PromoAPR;
                        retWrap.productType = reqData.projects[0].productType;
                        retWrap.projectCategory = reqData.projects[0].projectCategory;
                        retWrap.promoPercentPayment = reqData.projects[0].promoPercentPayment;
                        
                        retWrap.promoFixedPayment = productLst[0].Promo_Fixed_Payment__c;
                        retWrap.promoBalance = productLst[0].Promo_Percentage_Balance__c;
                        retWrap.drawPeriod = Integer.valueOf(productLst[0].Draw_Period__c);
                        retWrap.nonACHApr = productLst[0].NonACH_APR__c;
                        system.debug('nonACHApr - '+productLst[0].NonACH_APR__c);
                        
                        aprInfoResp.returnCode = '200';
                        aprInfoResp.projects.add(retWrap); 
                        
                    }else{
                        errMsg = ErrorLogUtility.getErrorCodes('214', null);
                        aprInfoResp.returnCode = '214';
                    }
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('214', null);
                    aprInfoResp.returnCode = '214';
                }
            }
            
            /*else if((null == reqData.projects[0].id || String.isBlank(reqData.projects[0].id)) && (null == reqData.projects[0].hashId || String.isBlank(reqData.projects[0].hashId)) && (null == reqData.projects[0].externalId || String.isBlank(reqData.projects[0].externalId)) && null != reqData.projects[0].term && reqData.projects[0].term > 0 && null != reqData.projects[0].apr && reqData.projects[0].apr > 0 && null != reqData.projects[0].isACH){
                errMsg = ErrorLogUtility.getErrorCodes('214', null);
                aprInfoResp.returnCode = '214';
            }*/
            
            if(string.isNotBlank(errMsg)){
                iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                aprInfoResp.error.add(errorMsg);
                iRestRes = (IRestResponse)aprInfoResp;
                system.debug('### iRestRes - '+iRestRes);
                return iRestRes;
            }
            else{
                iRestRes = (IRestResponse)aprInfoResp;
                system.debug('### iRestRes - '+iRestRes);
                return iRestRes;
            }
        }catch(Exception err){
            errMsg = ErrorLogUtility.getErrorCodes('400', null);
            iRestRes = (IRestResponse)aprInfoResp;
            system.debug('### GetNonACHAprInfoDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('GetNonACHAprInfoDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            system.debug('### Exit from transformOutput() of '+GetNonACHAprInfoDataServiceImpl.class);
            system.debug('### iRestRes '+iRestRes);
            return iRestRes;
        }
    }
}