/**
* Description: Trigger Business logic associated with Contact is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Rajesh               11/29/2017              Created 
******************************************************************************************/
public with sharing Class ContactTriggerHandler
{
    public static boolean isTrgExecuting = false;
    
    /**
* Constructor to initialize
*
* @param isExecuting    Has value of which context it is executing.
*/
    public ContactTriggerHandler(boolean isExecuting){
        isTrgExecuting = isExecuting;
    }
    
    
    /**
* Description: 
*
* @param new Credit records.
*/
    public void OnBeforeInsert(Contact[] newContact){
        try
        {
            system.debug('### Entered into OnBeforeInsert() of '+ ContactTriggerHandler.class);
            //Start-Merged the ContactSetVisionSolarAccount code by sreenivas
            String fni = (Test.isRunningTest() ? 'Test FNI' : 'VisionSolar');
            if(String.isNotBlank(fni)){
                List<Account> acctVSolarList = new List<Account>();
                acctVSolarList = [SELECT Id FROM Account WHERE FNI_Domain_Code__c = :fni LIMIT 1]; 
                //end
                 //Start-Merged the ContactSetVisionSolarAccount code by Sreenivas
                if(!acctVSolarList.isEmpty()){
                    for(Contact con : newContact){
                        if(con.ConnectionReceived.ConnectionName == 'Vision Solar')
                        {
                            con.AccountId = acctVSolarList[0].Id;
                            //accIds.add(conIterate.AccountId);
                        } //end
                    }
                }
            }
            UpdateIsDefaultOwner(newContact,null);
            system.debug('### Exit from OnBeforeInsert() of '+ ContactTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### ContactTriggerHandler.OnBeforeInsert():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('ContactTriggerHandler.OnBeforeInsert()',ex.getLineNumber(),'OnBeforeInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    
    /**
* Description: OnBeforeUpdate
* 
* @param new Credit records.
*/
    public void OnBeforeUpdate(Contact[] newContact,Map<Id,Contact> oldConMap){
        try
        {
            system.debug('### Entered into OnBeforeUpdate() of '+ ContactTriggerHandler.class);
            UpdateIsDefaultOwner(newContact,oldConMap);
            system.debug('### Exit from OnBeforeUpdate() of '+ ContactTriggerHandler.class);
        }catch(Exception ex)
        {
            system.debug('### ContactTriggerHandler.OnBeforeUpdate():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('ContactTriggerHandler.OnBeforeUpdate()',ex.getLineNumber(),'OnBeforeUpdate()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    
    public void UpdateIsDefaultOwner(Contact[] newContact,Map<Id,Contact> oldConMap){
        try
        {
            system.debug('### Entered into UpdateIsDefaultOwner() of '+ ContactTriggerHandler.class);
            Set<id> accIds = new Set<id>();
            Map<id,Contact> conMap = new Map<id,Contact>();
            
            for(Contact conIterate : newContact){
                system.debug('### conIterate '+conIterate);
                if(oldConMap != null){
                    if(conIterate.Is_Default_Owner__c == true && conIterate.Is_Default_Owner__c != oldConMap.get(conIterate.Id).Is_Default_Owner__c){
                        accIds.add(conIterate.Accountid);
                    }
                }else{
                    if(conIterate.Is_Default_Owner__c == true){
                        accIds.add(conIterate.Accountid);
                    }
                   
                }
            }  
            system.debug('### accIds '+accIds);
            if(!accIds.isEmpty()){
                List<Contact> conLst = [Select id,Name,Is_Default_Owner__c,Accountid from Contact Where Is_Default_Owner__c =: true AND  AccountId IN: accIds];
                system.debug('### conLst '+conLst);
                for(Contact conRec :conLst){
                    conRec.Is_Default_Owner__c = false;
                    conMap.put(conRec.id,conRec);                  
                }
            }
            if(!conMap.isEmpty()){
                update conMap.values();
            }
            system.debug('### Exit from UpdateIsDefaultOwner() of '+ ContactTriggerHandler.class);
        }
        catch(Exception ex)
        {
            system.debug('### ContactTriggerHandler.UpdateIsDefaultOwner():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('ContactTriggerHandler.UpdateIsDefaultOwner()',ex.getLineNumber(),'UpdateIsDefaultOwner()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    public static void OnAfterUpdate(Map<Id,Contact> newContactMap, Map<Id,Contact> oldContactMap){
        OpportunityShareToReportToUser recordshareHandler = new OpportunityShareToReportToUser();
        recordshareHandler.OportunityShareOnContactChange(newContactMap,oldContactMap);
    }
}