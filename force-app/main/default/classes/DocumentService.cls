@RestResource(urlMapping='/Document/v1/*')
global with sharing class DocumentService {

    @HttpPost
	global static String uploadFile(){
        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
        Id parentId = req.params.get('parentid');
        String fileName = req.params.get('filename');
        String fileType = req.params.get('filetype');
        String updateFile = req.params.get('update');
        Blob body = req.requestBody;
        
        // *** get box folder id
        String boxFolderId;
        List<box__FRUP__c> fl =  new List<box__FRUP__C>([SELECT box__Folder_ID__c, box__Record_Id__c FROM box__FRUP__c where box__Record_ID__c = :parentId]);
        
        if (fl.size() == 0) {
             // here we have to create a new folder
       
        } 
        
        boxFolderId = fl[0].box__Folder_Id__c;
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        BoxApiConnection api = new BoxApiConnection(BoxUtil.getBoxToken());
        BoxFolder bf = new BoxFolder(api,boxFolderId);
            
        BoxFile.Info info = bf.uploadFile(body,fileName + ' [ Submitted ]');
        
        BoxFile f = new BoxFile(api,info.id);
        f.addAttributes('enterprise','tracking','{"status":"Submitted"}');
        // f.updateAttributes('enterprise','tracking','[{ "op": "replace", "path":"/status", "value": "Reviewed"}]');
        
       /* Task act = new Task(
            WhoId    = '00QQ000000BSENx',
            Subject         = 'Testing29'
        );
		insert act;	
       */
		
        
        return f.getId();
       
    }
}