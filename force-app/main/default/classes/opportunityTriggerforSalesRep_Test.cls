@isTest
public class opportunityTriggerforSalesRep_Test {
     @testsetup static void createtestdata(){
         
        //SLFUtilityDataSetup.initData();        
    }
static testMethod void opportunityTriggerforSalesRepTest() {

       
    
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        insert trgLst;
    
    //Insert Account record
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Push_Endpoint__c ='http://mockbin.org';
        acc.Endpoint_Header__c ='https://connect.boomi.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c =acc.id;
        opp.Sales_Representative_Email__c = 'test@test.com';
        opp.Install_State_Code__c = 'CA';       
        opp.Install_Postal_Code__c = '84092';       
        opp.Hash_Id__c = 'test';
     try {
          insert opp;
        }
        catch (DmlException ex) {
            System.debug('Error::'+ex.getDmlMessage(0));
       }
    
    }
}