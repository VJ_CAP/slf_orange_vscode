/*
*
* Odie.Tang 12/14/2018
*/
public class LoanCalculator {
    
    public class PaymentSchedule{
        public PaymentSchedule(Decimal payment, Integer months){
            this.payment = payment.setScale(2, system.RoundingMode.HALF_UP);
            this.Months = months;
            system.debug('Payment'+this.Payment);
            system.debug('Months - '+Months);
        }
        public Decimal Payment{get; private set;}
        public Integer Months{get; private set;}
        public string ACHType{get; set;}//Added By Adithya as part of 2706
    }
    
    public static List<PaymentSchedule> getPaymentSchedules(
            Decimal loanAmount, Integer term, Decimal APR, Integer drawPeroid,
            Integer promoPeriod, Decimal promoAPR, Decimal promoPercentPayment, Decimal promoPercentBalance, 
            Decimal promoFixedPayment){
        
        system.debug('drawPeroid - '+drawPeroid);
        system.debug('promoFixedPayment - '+promoFixedPayment);
        system.debug('promoPercentBalance - '+promoPercentBalance);
        system.debug('promoPercentPayment - '+promoPercentPayment);
        
        //if(drawPeroid > 0){ //Updated by Ravi as part of 3666
        if(null != drawPeroid){
            
            return getPaymentSchedulesDrawPeriod(loanAmount, term, APR, drawPeroid);
        }
        //else if(promoFixedPayment > 0){ //Updated by Ravi as part of 3666
        else if(null != promoFixedPayment){
            
            return getPaymentSchedulesPromoFixedPayment(loanAmount, term, APR, promoPeriod, promoAPR, promoFixedPayment);
        }
        //else if(promoPercentBalance > 0){ //Updated by Ravi as part of 3666
        else if(null != promoPercentBalance){
            
            return getPaymentSchedulesPromoPercentBalance(loanAmount, term, APR, promoPeriod, promoAPR, promoPercentBalance);
        }
        //else if(promoPercentPayment > 0){ //Updated by Ravi as part of 3666
        else if(null != promoPercentPayment){
            
            return getPaymentSchedulesPromoPercentLoanAmount(loanAmount, term, APR, promoPeriod, promoAPR, promoPercentPayment);
        }else{
            
            return getPaymentSchedulesPmt(loanAmount, term, APR);
        }
        
    }
    
    public static List<PaymentSchedule> getPaymentSchedulesInterestOnly(
        Decimal loanAmount,Decimal deferredInterest, Integer term,Integer promoTerm, Decimal promoAPR, Decimal APR,Decimal paydownPercentage,String ACHType){
        
        List<PaymentSchedule> ret = new List<PaymentSchedule>();
        //Schedule 1 - No payment
        ret.add(new PaymentSchedule(0, 1));

        system.debug('ret - '+ret);
                
        //Schedule 2 - Interest Only payment
        //Double aprVal = Double.valueOf(APR/12);
        system.debug('APR - '+APR);
        system.debug('APR/12 - '+(APR/12));
        system.debug('loanAmount - '+loanAmount);
        Decimal CurrentIntrest = (loanAmount * (APR/12));
        system.debug('CurrentIntrest - '+CurrentIntrest);
        ret.add(new PaymentSchedule(CurrentIntrest,17));

        //Schedule 3 - Amortized payment - 0% / 30% prepayment
        deferredInterest = CurrentIntrest;
        Integer termVal;
        if(null != promoTerm)
        {
            //Schedule 3 - Amortized payment - 30% prepayment
            Decimal payDown = loanAmount * paydownPercentage;
            termVal = term - promoTerm; 
            //loanAmount = (loanAmount - payDown) +deferredInterest + CurrentIntrest;
            loanAmount = (loanAmount - payDown) +deferredInterest;
            //if(payDown >= (CurrentIntrest + deferredInterest))
            if(payDown >= (deferredInterest))
            {
                ret.addAll(getPaymentSchedulesPmt(loanAmount, termVal, APR));
            }else{
                Decimal payment = amortizedPayment(loanAmount, deferredInterest, termVal, APR);
                ret.add(new PaymentSchedule(payment, termVal));
            }
        }else{
            //Schedule 3 - Amortized payment - 0% prepayment
            termVal = term-18; 
            Decimal payment = amortizedPayment(loanAmount, deferredInterest, termVal, APR);
            ret.add(new PaymentSchedule(payment, termVal));
        }
        //Updating with ACH Type values
        //start
        if(!ret.isEmpty())
        {
            for(PaymentSchedule paymentIterate : ret)
            {
                paymentIterate.ACHType = ACHType;
            }
        }
        //End
        return ret;
    }
    
    public static Decimal PMT(Decimal balance, Integer term, Double apr){
        
        if(apr == 0){ //Added by Ravi as part of 3666
            return balance/term;
        }else{
            Decimal t = Decimal.valueOf(Math.pow(1 + apr, term));
            return balance*apr*t/(t-1);
        }
        
        //return balance*apr*t/(t-1);
    }
    
    public static List<PaymentSchedule> getPaymentSchedulesPmt(Decimal loanAmount, Integer term, Decimal APR){
        
        List<PaymentSchedule> ret = new List<PaymentSchedule>();
        
        Decimal payment = PMT(loanAmount, term, Double.valueOf(APR/12));
        
        ret.add(new PaymentSchedule(payment, term));
        
        return ret;
    }
    
    public static List<PaymentSchedule> getPaymentSchedulesPromoPercentBalance(
        Decimal loanAmount, Integer term, Decimal APR, Integer promoTerm, Decimal promoTermAPR, Decimal promoPaymentPercent){
            
        List<PaymentSchedule> ret = new List<PaymentSchedule>();
        
        Decimal balance = loanAmount, deferredInterest = 0;
        
        for(Integer i = 1; i <= promoTerm; i++){
            
            deferredInterest += balance * promoTermAPR / 12;
            
            Decimal payment = balance * promoPaymentPercent;
            
            ret.add(new PaymentSchedule(payment, 1));
            
            balance -= payment;
        }
        
        Integer monthsPostPromo = term - promoTerm;
        
        Decimal paymentPostPromo = amortizedPayment(balance, deferredInterest, monthsPostPromo, APR);
        
        ret.add(new PaymentSchedule(paymentPostPromo, monthsPostPromo));
        
        return ret;
    }
    
    public static List<PaymentSchedule> getPaymentSchedulesPromoPercentLoanAmount(
        Decimal loanAmount, Integer term, Decimal APR, Integer promoTerm, Decimal promoTermAPR, Decimal promoPaymentPercent){
        
        Decimal promoFixedPayment = loanAmount * promoPaymentPercent;
        
        return getPaymentSchedulesPromoFixedPayment(loanAmount, term, APR, promoTerm, promoTermAPR, promoFixedPayment);
    }
    
        
    public static List<PaymentSchedule> getPaymentSchedulesPromoFixedPayment(
        Decimal loanAmount, Integer term, Decimal APR, Integer promoTerm, Decimal promoTermAPR, Decimal promoPayment){
            
        List<PaymentSchedule> ret = new List<PaymentSchedule>();
        
        ret.add(new PaymentSchedule(promoPayment, promoTerm));
        
        Decimal paymentPostPromo = getAmortizedPaymentPostPromo(loanAmount, term, APR, promoTerm, promoTermAPR, promoPayment);
        
        Integer monthsPostPromo = term - promoTerm;
        
        ret.add(new PaymentSchedule(paymentPostPromo, monthsPostPromo));
        
        return ret;
    }
    
        
    public static List<PaymentSchedule> getPaymentSchedulesDrawPeriod(
        Decimal loanAmount, Integer term, Decimal APR, Integer drawPeriod){
        
        List<PaymentSchedule> ret = new List<PaymentSchedule>();
        
        Decimal paymentDrawPayment = loanAmount * APR / 12;
        
        ret.add(new PaymentSchedule(paymentDrawPayment, drawPeriod));
        
        Integer monthsPostDrawPeriod = term - drawPeriod;
        
        Decimal paymentPostDrawPeriod = PMT(loanAmount, monthsPostDrawPeriod, Double.valueOf(APR/12));
        
        ret.add(new PaymentSchedule(paymentPostDrawPeriod, monthsPostDrawPeriod));
        
        return ret;         
    }

    public static Decimal getAmortizedPaymentPostPromo(
        Decimal loanAmount, Integer term, Decimal APR, Integer promoTerm, Decimal promoTermAPR, Decimal promoPayment){
        
        Decimal balance = loanAmount, deferredInterest = 0;
        
        for(Integer i = 1; i <= promoTerm; i++){
            
            deferredInterest += balance * promoTermAPR / 12;
            
            balance -= promoPayment;
        }
        
        return amortizedPayment(balance, deferredInterest, term - promoTerm, APR);
    }
    
    /*
        Derivative Process: a heuristic search algorithm that minizes the seeks of the goal.
        
        
        Decimal PMT;
        Decimal interest;
        
        Decimal x = 0;
        
        x >= PMT(balance, terms, APR) + deferredInterest/terms;
        x <= PMT(balance+deferredInterest, terms, APR);
        
        
        (x - interest)*(n-1) + delta = deferredInterest;
        
        delta = deferredInterest - (x - interest)*(n-1);
        
        balance = balance - (x - interest - deferredInterest + (x - interest)*(n-1));
        
        balance = balance + interest*n + deferredInterest - x*n;
        
        x = PMT(balance, terms - n, MPR);
        
        t = (1 + r)^(terms - dmonths);
        
        c = r * t /(t - 1);
        
        x = balance * c = (balance + interest*n + deferredInterest - x*n)*c;
        
        x = (balance + interest*n + deferredInterest)*c/(1 + n*c);
    */
    public static Decimal amortizedPayment(Decimal balance, Decimal deferredInterest, Integer term, Decimal APR){
        
        Double r = APR/12;
        Decimal interest = balance*r;
        
        Decimal minPayment = PMT(balance, term, r) + deferredInterest/term;
        Decimal maxPayment = PMT(balance + deferredInterest, term, r);
        
        Integer min = (Integer)Math.ceil(deferredInterest/(maxPayment-interest));
        Integer max = (Integer)Math.ceil(deferredInterest/(minPayment-interest));
        
        for(Integer i = max; i >= min; i--){
            
            Decimal payment = amortizedPayment(balance, deferredInterest, term, i, r);
            
            if((payment - interest)*(i-1) < deferredInterest 
               && (payment - interest)*i >= deferredInterest)
                return payment;
        }
        
        return null;
    }
    private static Decimal amortizedPayment(Decimal balance, Decimal deferredInterest, Integer term, Integer dmonths, Double r){
        
        Decimal t = Decimal.valueOf(Math.pow(1 + r, term - dmonths));
        
        Decimal c = r*t/(t-1);
        
        return ((balance + deferredInterest + balance*r*dmonths)*c/(1 + dmonths*c)).setScale(2, system.RoundingMode.HALF_UP);
    }
    
}