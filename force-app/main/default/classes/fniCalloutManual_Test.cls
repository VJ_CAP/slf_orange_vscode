/**
* Description: Test class for fniCalloutManual. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Paul Mackinaw           4/12/2016           Created
    Adithya Sarma K         06/27/2017          Updated and re- modified with dinamic Id's
******************************************************************************************/
@isTest
public class fniCalloutManual_Test {
   
    static{
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
        
         //Insert Custom Endpoints Custom Setting Record
         List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
           SLF_Boomi_API_Details__c endpoint3 = new SLF_Boomi_API_Details__c();
            endpoint3.Name = 'FNI QA';
            endpoint3.Username__c =  'salesforceuser';
            endpoint3.Password__c = 'RC8N1CreKnz4rm2kMTCj';
            endpoint.add(endpoint3);
            
            SLF_Boomi_API_Details__c endpoint4 = new SLF_Boomi_API_Details__c();
            endpoint4.Name = 'FNI Prod';
            endpoint4.Username__c =  'salesforceuser';
            endpoint4.Password__c = 'AmtPHqSlK9sB6NXjSyq4';
            endpoint.add(endpoint4);
            insert endpoint;
    }
    /*
        Description: This class is to cover fni_Credit_Extension Class and FniCalloutManual
    */
    private testMethod static void testFNICallout(){
        
        //get the person account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.BillingStreet='Teststreet';
        acc.BillingCity='testcity';
        acc.BillingState='Alaska';
        acc.BillingPostalCode='12445';
        acc.ShippingStreet='Teststreet';
        acc.ShippingCity='testcity';
        acc.ShippingState='Alaska';
        acc.ShippingPostalCode='12345';
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;
        acc.Phone='9876543210';
        insert acc;

        Contact con = new Contact(LastName = 'Contact Last Name', AccountId = acc.id);
        insert con;

        User portalUser = new User();
        portalUser.ProfileID = [Select Id From Profile Where Name='Partner Community User for Salesforce1'].id;
        portalUser.EmailEncodingKey = 'ISO-8859-1';
        portalUser.LanguageLocaleKey = 'en_US';
        portalUser.TimeZoneSidKey = 'America/New_York';
        portalUser.LocaleSidKey = 'en_US';
        portalUser.FirstName = 'first';
        portalUser.LastName = 'last';
        portalUser.Username = 'testSLF@appirio.com';   
        portalUser.CommunityNickname = 'testUser123';
        portalUser.Alias = 't1';
        portalUser.Email = 'no@email.com';
        portalUser.IsActive = true;
        portalUser.ContactId = con.Id;
        insert portalUser;
        
        AccountShare accShare = new AccountShare(); //a new empty AccountShare object
        accShare.userorgroupid = portalUser.Id;
        accShare.accountid = con.accountid;
        accShare.accountaccesslevel = 'Edit';
        accShare.OpportunityAccessLevel = 'None';
        accShare.CaseAccessLevel = 'None';
        insert accShare;
        
        
        System.RunAs(portalUser) {
             //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            insert opp;
            
            
            // Create prequal table
            Prequalification_Criterion__c testPrequal = new Prequalification_Criterion__c();
            testPrequal.Installer_Account__c = acc.Id;
            testPrequal.State_Code__c = 'CA';
            testPrequal.FNI_Response_Code__c = 1;
            testPrequal.FNIDecisionCode__c = 'A';
            testPrequal.Long_Term_Facility__c = 'TCU';
            testPrequal.Stip_Code__c = '';
            testPrequal.Qualification_Message__c = 'test';
            insert testPrequal;
            
            // Create Lead record
            Lead testLeadNew = new Lead();
            testLeadNew.FirstName = 'TestFN';
            testLeadNew.LastName = 'TestLN';
            testLeadNew.Street = '123 Test St';
            testLeadNew.FNI_Decision__c = '1';
            testLeadNew.StateCode = 'CA';
            testLeadNew.City = 'Oakland';
            testLeadNew.Vision_State_Text__c= 'CA';
            testLeadNew.PostalCode = '94611';
            testLeadNew.Company = 'testco';
            testLeadNew.OwnerId = portalUser.Id;
            testLeadNew.Customer_has_authorized_credit_soft_pull__c = true;
            testLeadNew.SSN__c ='666-67-3548';
            testLeadNew.LeadSource='Inbound';
            testLeadNew.Lead_Source_Detail__c='Call';
            
            insert testLeadNew;
            
        
        
        
            
            
        //Query prequal table
            List<Prequalification_Criterion__c> prequalList = [SELECT Installer_Account__c, State_Code__c, FNI_Response_Code__c, Qualification_Message__c from Prequalification_Criterion__c];

            System.assertEquals('success', fniCalloutManual.buildRequest(testLeadNew.Id, JSON.serialize(prequalList)));
            List<Lead> updatedLeadNew = new List<Lead>([Select Id, FNI_Decision__c from Lead where Id = :testLeadNew.Id]);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(updatedLeadNew[0]);
            
            fni_Lead_Extension testExtension = new fni_Lead_Extension(sc);
            //testExtension.callFNI();
            testExtension.creditConsent();

            //Create credit without trigger.old
            Credit__c testCredit = new Credit__c();
            testCredit.First_Name__c = 'TestFN';
            testCredit.Last_Name__c = 'TestLN';
            testCredit.Street__c = '123 Test St';
            testCredit.City__c = 'Oakland';
            testCredit.State_Code__c = 'CA';
            testCredit.Postal_Code__c = '94611';
            testCredit.Customer_has_authorized_credit_soft_pull__c = true;
            insert testCredit;
            
            System.assertEquals('success', fniCalloutManual.buildCreditRequest(testCredit.Id, JSON.serialize(prequalList)));
            List<Credit__c> updatedCredit = new List<Credit__c>([Select Id, FNI_Decision__c from Credit__c where Id = :testCredit.Id]);
            ApexPages.StandardController sc2 = new ApexPages.StandardController(updatedCredit[0]);
            fni_Credit_Extension testExtension2 = new fni_Credit_Extension(sc2);
            testExtension2.creditConsent(); 
            fniCalloutManual.buildRequestCallout(testLeadNew.id,'test');
            }
    }
    
     
    /*
        Description: This class is to cover buildHardCreditRequest method in FniCalloutManual
    */
    
    public testMethod static void buildHardCreditRequestTest(){
        
         //get the person account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
       
        //Insert Account record
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.BillingStreet='Teststreet';
        acc.BillingCity='testcity';
        acc.BillingState='Alaska';
        acc.BillingPostalCode='12445';
        acc.ShippingStreet='Teststreet';
        acc.ShippingCity='testcity';
        acc.ShippingState='Alaska';
        acc.ShippingPostalCode='12345';
        acc.Phone='1234567894';
        acc.Length_of_Employment_Years__c=12;
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;  
        
        acc = [select Id from Account where Id=: acc.Id];
        acc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update acc;
        
        Date todayDate = system.today();
        acc.PersonBirthdate=  Date.newInstance(todayDate.year()-19, todayDate.month(), todayDate.day());
        acc.SSN__c = '999-99-9999';
        acc.ShippingPostalCode='75252';
        acc.BillingState='Texas';
        acc.BillingPostalCode='75252';
        acc.ShippingStateCode = 'TX';
        acc.PersonOtherPhone = '1111111111';
        update acc;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.ST_APR__c = 88;
        prod.State_Code__c ='AK';
        prod.APR__c=87;
        prod.Internal_Use_Only__c = true;
        prod.Long_Term_Facility__c='TCU';
        prod.Product_Loan_Type__c='Solar';
        prod.Product_Tier__c = '0';
        insert prod;
        system.debug('***********'+prod.ST_APR__c);
        system.debug('****Long_Term_Facility__c*******'+prod.Long_Term_Facility__c);
        
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.Install_Postal_Code__c = '12345';
        opp.Short_Term_Loan_Amount_Manual__c = 5;
        opp.AccountId = acc.id;
        opp.Customer_has_authorized_credit_hard_pull__c=true;
        opp.SLF_Product__c=prod.id;
        opp.Install_Postal_Code__c = 'AK';
        opp.Install_State_Code__c = 'AK';
        opp.Co_Applicant__c = acc.id;
        opp.FNI_Long_Term_Decision__c = 'D';
        opp.Installer_Account__c =acc.id;
        opp.Combined_Loan_Amount__c = 10000;       
        insert opp;
    
        fniCalloutManual.buildHardCreditRequest(opp.id);
        //Test.setMock(WebServiceMock.class, new fniCallMock());
        fniCalloutManual.buildHardCreditRequestCallout(opp,acc,acc,true);
       
        //fniCalloutManual.buildHardCreditRequestCallout(opp,acc,acc,false);
        
        try
        {
            acc.Phone = null;
            update acc;
            opp.FNI_Long_Term_Decision__c = 'P';
            update  opp;
        
            fniCalloutManual.buildHardCreditRequest(opp.id);
        }catch(exception e){
            system.debug('********message**'+e.getMessage());
        }   

        try
        {
            acc.Phone = '111111111';
            acc.PersonOtherPhone = '111111111';
            update acc;
            opp.FNI_Long_Term_Decision__c = 'A';
            update  opp;
            
            fniCalloutManual.buildHardCreditRequest(opp.id);
        }catch(exception e){
            system.debug('********message**'+e.getMessage());
        } 
         try
        {
            acc.Phone = '111111111';
            acc.PersonOtherPhone = '111111111';
            update acc;
            opp.FNI_Long_Term_Decision__c = 'P';
            update  opp;
            
            fniCalloutManual.buildHardCreditRequest(opp.id);
        }catch(exception e){
            system.debug('********message**'+e.getMessage());
        } 
         try
        {
            acc.Phone = '111111111';
            acc.PersonOtherPhone = '111111111';
            update acc;
            opp.FNI_Long_Term_Decision__c = 'D';
            update  opp;
            
            fniCalloutManual.buildHardCreditRequest(opp.id);
        }catch(exception e){
            system.debug('********message**'+e.getMessage());
        } 
            
    }
    
    /*
        Description: This class is to cover buildHardCreditRequestCallout method in FniCalloutManual
    */
    
    public testMethod static void buildHardCreditRequestCalloutTest(){
        
    //get the person account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
         //Insert Account record
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.BillingStreet='Teststreet';
        acc.BillingCity='testcity';
        acc.BillingState='Alaska';
        acc.BillingPostalCode='12445';
        acc.ShippingStreet='Teststreet';
        acc.ShippingCity='testcity';
        acc.ShippingState='Alaska';
        acc.ShippingPostalCode='12345';
        acc.Phone='1234567894';
        acc.Length_of_Employment_Years__c=12;
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;
        insert acc;
      
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;  
        
        acc = [select Id from Account where Id=: acc.Id];
        acc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update acc;
        
        Date todayDate = system.today();
        acc.PersonBirthdate=  Date.newInstance(todayDate.year()-19, todayDate.month(), todayDate.day());
        acc.SSN__c = '999-99-9999';
        acc.ShippingPostalCode='75252';
        acc.BillingState='Texas';
        acc.BillingPostalCode='75252';
        acc.ShippingStateCode = 'TX';
        acc.PersonOtherPhone = '1111111111';
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        update acc;
        
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.State_Code__c ='AK';
        prod.ST_APR__c = 88;
        prod.APR__c=87;
        prod.Internal_Use_Only__c = true;
        prod.Long_Term_Facility__c='TCU';
        prod.Product_Tier__c = '0';
        insert prod;
        system.debug('***********'+prod.ST_APR__c);
        system.debug('****Long_Term_Facility__c*******'+prod.Long_Term_Facility__c);
        
        Document doc = new Document();
        doc.Body = Blob.valueOf('Some Text');
        doc.ContentType = 'application/pdf';
        doc.DeveloperName = 'my_document';
        doc.IsPublic = true;
        doc.Name = 'Sunlight Logo';
        doc.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert doc;
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.Install_Postal_Code__c = '12345';
        opp.Short_Term_Loan_Amount_Manual__c = 5;
        opp.AccountId = acc.id;
        opp.Customer_has_authorized_credit_hard_pull__c=true;
        opp.SLF_Product__c=prod.id;
        opp.Install_Postal_Code__c = 'AK';
        opp.Co_Applicant__c = acc.id;
        opp.FNI_Long_Term_Decision__c = 'D';
        opp.Long_Term_Loan_Amount_Manual__c=100;
        opp.Install_State_Code__c = 'AK';
        opp.Installer_Account__c =acc.id;
        insert opp;
        prod.APR__c=87;
        update prod;
        opp.FNI_Long_Term_Decision__c = 'P';
        update  opp;
        //Test.setMock(WebServiceMock.class, new fniCallMock());
        //fniCalloutManual.objBoomiAPIDetails  = boomiapis;
        fniCalloutManual.buildHardCreditRequestCallout(opp,acc,acc,false);
    
    }
    
    private static Boolean runningInASandbox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    public testMethod static void getFNIResponseNodeTest(){
        String soapBody = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns3:RESPONSE xmlns="http://tko.fni.com/application/transaction.xsd" xmlns:ns2="http://tko.fni.com/application/request.xsd" xmlns:ns3="http://tko.fni.com/application/response.xsd"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage></ns3:StatusMessage><ns3:FNIReferenceNumber>20000004676571</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-04-20T07:53:06-05:00</ns3:DecisionDateTime><ns3:FirstName>DARYL</ns3:FirstName><ns3:LastName>MANSON</ns3:LastName><ns3:TransactionID>4A64DF75-AEFD-4F00-931E-7E22F917F056</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2></ns3:StipRsn2><ns3:StipRsn3></ns3:StipRsn3><ns3:StipRsn4></ns3:StipRsn4><ns3:DTI>0</ns3:DTI><ns3:LowFico>819</ns3:LowFico><ns3:DebtForMax>0</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>D</ns3:Decision><ns3:Line></ns3:Line><ns3:StipRsn>D30</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>D</ns3:Decision><ns3:Line></ns3:Line><ns3:StipRsn>D30</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>'; 
        
        Test.startTest();
        fniCalloutManual.getFNIResponseNode(soapBody);
        Test.stopTest();
        
    }
    public testMethod static void getFNIResponseNodeErrorTest(){
        String soapBody = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns3:RESPONSE xmlns="http://tko.fni.com/application/transaction.xsd" xmlns:ns2="http://tko.fni.com/application/request.xsd" xmlns:ns3="http://tko.fni.com/application/response.xsd"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage></ns3:StatusMessage><ns3:FNIReferenceNumber>20000004676571</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-04-20T07:53:06-05:00</ns3:DecisionDateTime><ns3:FirstName>DARYL</ns3:FirstName><s333:UUUU></SSSS><ns3:LastName>MANSON</ns3:LastName><ns3:TransactionID>4A64DF75-AEFD-4F00-931E-7E22F917F056</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2></ns3:StipRsn2><ns3:StipRsn3></ns3:StipRsn3><ns3:StipRsn4></ns3:StipRsn4><ns3:DTI>0</ns3:DTI><ns3:LowFico>819</ns3:LowFico><ns3:DebtForMax>0</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>D</ns3:Decision><ns3:Line></ns3:Line><ns3:StipRsn>D30</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>D</ns3:Decision><ns3:Line></ns3:Line><ns3:StipRsn>D30</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>'; 
        
        Test.startTest();
        fniCalloutManual.getErrorMsg(soapBody);
        Test.stopTest();
        
    }
     public testMethod static void parseFNIResponseXMLTest(){
        String soapBody = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns3:RESPONSE xmlns="http://tko.fni.com/application/transaction.xsd" xmlns:ns2="http://tko.fni.com/application/request.xsd" xmlns:ns3="http://tko.fni.com/application/response.xsd"><ns3:StatusCode>SUCCESS</ns3:StatusCode><ns3:StatusMessage></ns3:StatusMessage><ns3:FNIReferenceNumber>20000004676571</ns3:FNIReferenceNumber><ns3:Decision>A</ns3:Decision><ns3:DecisionDateTime>2018-04-20T07:53:06-05:00</ns3:DecisionDateTime><ns3:FirstName>DARYL</ns3:FirstName><ns3:LastName>MANSON</ns3:LastName><ns3:TransactionID>4A64DF75-AEFD-4F00-931E-7E22F917F056</ns3:TransactionID><ns3:LenderID>SUN</ns3:LenderID><ns3:Strategy><ns3:StipRsn1>NULL</ns3:StipRsn1><ns3:StipRsn2></ns3:StipRsn2><ns3:StipRsn3></ns3:StipRsn3><ns3:StipRsn4></ns3:StipRsn4><ns3:DTI>0</ns3:DTI><ns3:LowFico>819</ns3:LowFico><ns3:DebtForMax>0</ns3:DebtForMax></ns3:Strategy><ns3:PrequalDecisions><ns3:Lender><ns3:Name>CRB</ns3:Name><ns3:Decision>D</ns3:Decision><ns3:Line></ns3:Line><ns3:StipRsn>D30</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender><ns3:Lender><ns3:Name>TCU</ns3:Name><ns3:Decision>D</ns3:Decision><ns3:Line></ns3:Line><ns3:StipRsn>D30</ns3:StipRsn><ns3:MaxPayment>0</ns3:MaxPayment><ns3:MaxDTI>550</ns3:MaxDTI></ns3:Lender></ns3:PrequalDecisions></ns3:RESPONSE></soap:Body></soap:Envelope>'; 
        
        Test.startTest();
        DOM.XMLNode prequalDecNode = fniCalloutManual.getFNIResponseNode(soapBody);
        fniCalloutManual.parseFNIResponseXML(prequalDecNode);
        Test.stopTest();
        
    }
   public testMethod static void getPrequalDecisionTest(){
      string qualMsgListSerialized = '{"qualMsgListSerialized": [{"id": "","stateCode": "1233","sDecision": "testDecision","lenderName": "testname","stipRsn": "123444"}]';
      
    }
}