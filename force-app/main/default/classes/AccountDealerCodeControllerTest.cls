@isTest
public Class AccountDealerCodeControllerTest{
    public testMethod static void testAccountDealer(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();          
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);  
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);            
        insert trgLst;
        
         //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();  
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        PageReference pageRef = Page.GenerateDealerCode;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(acc.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        AccountDealerCodeController adcCntrl = new AccountDealerCodeController(sc);
        adcCntrl.getDealerCode();
        //Handling negative scenario 
        //start
        acc =  new Account();
        ApexPages.StandardController sc1 = new ApexPages.StandardController(acc);
        AccountDealerCodeController adcCntrl1 = new AccountDealerCodeController(sc1);
        adcCntrl1.getDealerCode();
        //End
        //adcCntrl.getindex();  
    }
}