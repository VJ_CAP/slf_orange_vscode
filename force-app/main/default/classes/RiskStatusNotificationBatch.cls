/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkatesh        03/01/2019         Created
******************************************************************************************/
global class RiskStatusNotificationBatch implements Database.Batchable<sobject>,Database.Stateful{
    private String query; //Query in the start method
    global Map<Underwriting_File__c,String> riskstatusUnderWrtMap = new Map<Underwriting_File__c,String>();
    
    
    /**
    * Description: Entry of batch and execute a SOQL.
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('### Entered into start() of '+ RiskStatusNotificationBatch.class);
        
        Date queryDate = Date.Today();
        Date currentDate = Date.Today();
        //Datetime queryDateTime=system.now().addDays(60);This is Commented by Adithya because this variable is not being used any where in the code.
        queryDate = queryDate.addDays(30);
        
        query = 'SELECT id,M0_Approval_Date__c,M1_Approval_Date__c,Opportunity__r.Project_Category__c,Opportunity__r.Account.Name,M0_Approval_Requested_Date__c,M2_Approval_Date__c,Opportunity__c,Opportunity__r.Account.id,Opportunity__r.Co_Applicant__r.id,Opportunity__r.Credit_Expiration_Date__c,Opportunity__r.Installer_Account__c,Opportunity__r.Installer_Account__r.Operations_Email__c,Opportunity__r.Installer_Account__r.Stip_Email_1__c,Opportunity__r.Installer_Account__r.Stip_Email_2__c,Opportunity__r.Installer_Account__r.Stip_Email_3__c,Opportunity__r.Installer_Account__r.Installer_Email__c from Underwriting_File__c WHERE Opportunity__r.EDW_Originated__c = false AND Opportunity__r.StageName !=\'Archive\' AND Project_Status__c != \'Declined\' AND Project_Status__c!=\'Project Withdrawn\' AND Project_Status__c !=\'Project Completed\' AND (Opportunity__r.Credit_Expiration_Date__c != null AND Opportunity__r.Credit_Expiration_Date__c <=: queryDate AND Opportunity__r.Credit_Expiration_Date__c >: currentDate)';
        
        
        //AND M0_Approval_Date__c!=null AND M1_Approval_Date__c=null AND M0_Approval_Requested_Date__c!=null  AND Opportunity__r.Credit_Expiration_Date__c <=:queryDate) OR (M1_Approval_Date__c!=null AND M2_Approval_Date__c=null))';
        
        system.debug('### query String '+ query);
        
        system.debug('### Exit from start() of '+RiskStatusNotificationBatch.class);
        return Database.getQueryLocator(query);
    }
    
    /**
    * Description:
    */
    global void execute(Database.BatchableContext BC, List<Underwriting_File__c> scopeList)
    {
        system.debug('### Entered into execute() of '+RiskStatusNotificationBatch.class);
        for(Underwriting_File__c objUnderWrt : scopeList){
            String riskStatus ='';
            Date currentDate=Date.today();
            
            if(objUnderWrt.Opportunity__r.Project_Category__c == 'Solar')
            {
                if(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c != null && objUnderWrt.M0_Approval_Date__c != null && objUnderWrt.M1_Approval_Date__c == null && objUnderWrt.M0_Approval_Requested_Date__c != null ){
                
                    system.debug('#### objUnderWrt==>:'+objUnderWrt.Opportunity__c+' #### days diff==>:'+currentDate.daysBetween(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c));
                
                    // credit expiration date less than 10
                    if(currentDate.daysBetween(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c) == 10 ){
                        riskStatus='Credit Exp 10';
                    }else if(currentDate.daysBetween(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c) == 30 ){
                        // credit expiration date less than 30
                        riskStatus='Credit Exp 30';
                    }
                
                }else if(objUnderWrt.M1_Approval_Date__c != null && objUnderWrt.M2_Approval_Date__c == null&& currentDate.daysBetween(date.newinstance(objUnderWrt.M1_Approval_Date__c.year(), objUnderWrt.M1_Approval_Date__c.month(), objUnderWrt.M1_Approval_Date__c.day())) == 60 ){

                    system.debug('#### objUnderWrt==>:'+objUnderWrt.Opportunity__c+' #### days diff==>:'+currentDate.daysBetween(date.newinstance(objUnderWrt.M1_Approval_Date__c.year(), objUnderWrt.M1_Approval_Date__c.month(), objUnderWrt.M1_Approval_Date__c.day())));
                    
                    riskStatus='PTO Exp';
                }
            }else if(objUnderWrt.Opportunity__r.Project_Category__c == 'Home')
            {
                if(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c != null){
                    system.debug('#### objUnderWrt==>:'+objUnderWrt.Opportunity__c+' #### days diff==>:'+currentDate.daysBetween(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c));
                
                    // credit expiration date less than 10
                    if(currentDate.daysBetween(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c) == 10 ){
                        riskStatus='Credit Exp 10';
                    }else if(currentDate.daysBetween(objUnderWrt.Opportunity__r.Credit_Expiration_Date__c) == 30 ){
                        // credit expiration date less than 30
                        riskStatus='Credit Exp 30';
                    }
                }
            }
            if(String.isNotBlank(riskStatus)){
                riskstatusUnderWrtMap.put(objUnderWrt,riskStatus);
            } 
        }
        system.debug('### Exit from execute() of '+RiskStatusNotificationBatch.class);
    }
    /**
* Description:
*/
    global void finish(Database.BatchableContext bc){
        system.debug('### Entered into finish() of '+RiskStatusNotificationBatch.class);
        try{
            system.debug('### riskstatusUnderWrtMap.size '+riskstatusUnderWrtMap.size());
            if(!riskstatusUnderWrtMap.isEmpty()){
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                List<OrgWideEmailAddress> senderEmail = [select id,DisplayName, Address from OrgWideEmailAddress where DisplayName ='Sunlight Financial Support' LIMIT 1];
                Map<String,EmailTemplate> emailTempltMap =New Map<String,EmailTemplate>();
                for(EmailTemplate et:[Select id, htmlValue, Body, subject,DeveloperName from EmailTemplate where DeveloperName  IN ('Risk_Status_Notification','Risk_Status_Notification_PTO','Risk_Status_Notification_Home')]){
                    if(null!=et){
                        emailTempltMap.put(et.DeveloperName,et);
                    } 
                }
                System.debug('#### emailTempltMap==>:'+emailTempltMap.keySet());
                
                
                for(Underwriting_File__c underWrtIterate : riskstatusUnderWrtMap.keySet()){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    
                    List<String> toAddresses = new List<String>();
                    
                    // Installer Email
                    if(String.isNotBlank(underWrtIterate.Opportunity__r.Installer_Account__r.Installer_Email__c))
                        toAddresses.add(underWrtIterate.Opportunity__r.Installer_Account__r.Installer_Email__c);
                    
                    // Operation Email
                    if(String.isNotBlank(underWrtIterate.Opportunity__r.Installer_Account__r.Operations_Email__c))
                        toAddresses.add(underWrtIterate.Opportunity__r.Installer_Account__r.Operations_Email__c);
                    
                    // Stipulation Email 1
                    if(String.isNotBlank(underWrtIterate.Opportunity__r.Installer_Account__r.Stip_Email_1__c))
                        toAddresses.add(underWrtIterate.Opportunity__r.Installer_Account__r.Stip_Email_1__c);
                    
                    // Stipulation Email 2
                    if(String.isNotBlank(underWrtIterate.Opportunity__r.Installer_Account__r.Stip_Email_2__c))
                        toAddresses.add(underWrtIterate.Opportunity__r.Installer_Account__r.Stip_Email_2__c);
                    
                    // Stipulation Email 3
                    if(String.isNotBlank(underWrtIterate.Opportunity__r.Installer_Account__r.Stip_Email_3__c))
                        toAddresses.add(underWrtIterate.Opportunity__r.Installer_Account__r.Stip_Email_3__c);
                    
                    if(!toAddresses.isEmpty() && !emailTempltMap.isEmpty() && (emailTempltMap.containsKey('Risk_Status_Notification') || emailTempltMap.containsKey('Risk_Status_Notification_PTO') || emailTempltMap.containsKey('Risk_Status_Notification_Home')))
                    {
                        
                        mail.setToAddresses(toAddresses);
                        String htmlBody = '';
                        String subjectStr='';
                        
                        if(riskstatusUnderWrtMap.get(underWrtIterate).contains('Credit Exp 30')){
                            
                            if(underWrtIterate.Opportunity__r.Project_Category__c == 'Solar')
                                htmlBody = emailTempltMap.get('Risk_Status_Notification').HtmlValue;
                            else if(underWrtIterate.Opportunity__r.Project_Category__c == 'Home')
                                htmlBody = emailTempltMap.get('Risk_Status_Notification_Home').HtmlValue;
                            
                            subjectStr=' credit expires in 30 days';
                            htmlBody = htmlBody.replace('{!Account.AccountNumber}', '30');
                            
                        }else if(riskstatusUnderWrtMap.get(underWrtIterate).contains('Credit Exp 10')){
                            
                            if(underWrtIterate.Opportunity__r.Project_Category__c == 'Solar')
                                htmlBody = emailTempltMap.get('Risk_Status_Notification').HtmlValue;
                            else if(underWrtIterate.Opportunity__r.Project_Category__c == 'Home')
                                htmlBody = emailTempltMap.get('Risk_Status_Notification_Home').HtmlValue;
                            
                            subjectStr=' credit expires in 10 days';
                            htmlBody = htmlBody.replace('{!Account.AccountNumber}', '10');
                            
                        }else if(riskstatusUnderWrtMap.get(underWrtIterate).contains('PTO Exp') && underWrtIterate.Opportunity__r.Project_Category__c == 'Solar'){
                            htmlBody = emailTempltMap.get('Risk_Status_Notification_PTO').HtmlValue;
                            subjectStr=' project needs PTO approval'; 
                        }
                        
                        htmlBody = htmlBody.replace('{!$Label.Sunlight_Logo}',System.Label.Sunlight_Logo);
                        htmlBody = htmlBody.replace('{!Account.Name}', underWrtIterate.Opportunity__r.Account.Name);
                        mail.setSubject(underWrtIterate.Opportunity__r.Account.Name+'\'s'+subjectStr);
                        mail.setSaveAsActivity(false);
                        mail.setHtmlBody(htmlBody);
                        if(senderEmail.size()>0){
                            mail.setOrgWideEmailAddressId(senderEmail[0].id);
                        }
                        mails.add(mail);
                    }
                    
                    system.debug('### mail==>:'+mail); 
                }
                if(mails.size() > 0 ) {
                    system.debug('### mails==>:'+mails);
                    Messaging.sendEmail(mails);
                }
            }
        }catch(exception e){
            system.debug('### RiskStatusNotificationBatch.finish():'+e.getLineNumber()+':'+ e.getMessage() + '###' +e.getStackTraceString());
            
            ErrorLogUtility.writeLog('RiskStatusNotificationBatch.finish()',e.getLineNumber(),'finish()',e.getMessage() + '###' +e.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(e,null,null,null,null));   
        }
        system.debug('### Exit from finish() of '+RiskStatusNotificationBatch.class);
    }
}