/**
* Description: synccredit integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class SyncCreditServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + SyncCreditServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        SyncCreditDataServiceImpl syncCreditDataSrvImpl = new SyncCreditDataServiceImpl(); 
        IRestResponse iRestRes;
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            res.response = syncCreditDataSrvImpl.transformOutput(reqData);
        }catch(Exception err){
            system.debug('### SyncCreditServiceImpl .fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           
            ErrorLogUtility.writeLog(' SyncCreditServiceImpl .fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));  
            UnifiedBean syncbean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            syncbean.error  = errorWrapperLst ;
            //syncbean.error.add(errorMsg);
            syncbean.returnCode = '214';
            iRestRes = (IRestResponse)syncbean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + SyncCreditServiceImpl.class);
        return res.response ;
    }
    
    
}