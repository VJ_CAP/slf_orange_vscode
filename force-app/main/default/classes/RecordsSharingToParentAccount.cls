/**
* Description: Account and Opportunity related Trigger logic.
*              Sharing Installer Account,Applicant,Co-Applicant,opportunity and product to parent of Installer Account     
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Brahmeswar Gajula      03/16/2018          Created
******************************************************************************************/


public class RecordsSharingToParentAccount {
    
        Map<string,String> GroupNameandId = new Map<string,String>();
        
        public void PrepareRoleGroupMap(){                               
            For(Group objGp : [SELECT Id, Name, RelatedId, DeveloperName, Type, Email, OwnerId, DoesSendEmailToMembers 
                                       FROM Group where type='Role' AND DeveloperName like '%PartnerExecutive%']){
             GroupNameandId.Put(objGp.DeveloperName.toLowerCase(),objGp.id);
            } 
        }
        
        public void AccountShareToParentAccount(Map<id,Account> Triggernewmap ,Map<id,Account> Triggeroldmap){
            String BusinessRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            List<RecordType> PersonAccountRecordtype = [SELECT Id,Name, SobjectType,IsPersonType FROM RecordType WHERE SobjectType='Account' AND IsPersonType=True];
            String PersonRecordTypeId ='';
            if(PersonAccountRecordtype != null && !PersonAccountRecordtype.isEmpty()){
                PersonRecordTypeId = PersonAccountRecordtype[0].id;
            }
            Map<string,string> AccidAndName = new Map<String,String>();
           List<AccountShare> lstAccshare = new List<AccountShare>();
            if(!Triggernewmap.isEmpty() && Triggeroldmap.isEmpty()){//Insert
               AccountShare Accshare = new AccountShare();
               PrepareRoleGroupMap();
                for( Account objacc : [select id,Parentid,parent.Name,Name,recordtypeid from Account where id IN:Triggernewmap.keyset()]){
                    if(objacc.parentid != null && objacc.recordtypeid == BusinessRecordTypeId){//Business Accout needs to be share with parent
                        String groupNameval = String.valueOf(objacc.parent.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';//For testing purpose
                       if(GroupNameandId.get(groupNameval.toLowerCase())!= null){
                           Accshare = new AccountShare();
                            Accshare = PrepareAccountShareRecords(objacc.id,groupNameval);
                            if(Accshare!= null)
                            lstAccshare.add(Accshare);
                       }                     
                    }
                    //Srikanth 04/02 - Added to grant access to child account's executive
                    if(objacc.Name != null && objacc.recordtypeid == BusinessRecordTypeId){//Business Accout needs to be share with Executive at same account
                        String groupNameval = String.valueOf(objacc.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';//Removing specila charectors from string
                       if(GroupNameandId.get(groupNameval.toLowerCase())!= null){
                           Accshare = new AccountShare();
                            Accshare = PrepareAccountShareRecords(objacc.id,groupNameval);
                            if(Accshare!= null)
                            lstAccshare.add(Accshare);
                       }                     
                    }
                }
            }
            if(!Triggernewmap.isEmpty() && !Triggeroldmap.isEmpty()){//Update
                PrepareRoleGroupMap();
                Set<string> Accids = new set<String>();
                for( Account objacc : [select id,Parentid,parent.Name,recordtypeid from Account where id IN:Triggernewmap.keyset()]){
                    if( Triggernewmap.get(objacc.id).parentid != Triggeroldmap.get(objacc.id).parentid){
                        if(objacc.parentid != null && objacc.recordtypeid == BusinessRecordTypeId){//Business Accout needs to be share with parent
                            AccountShare Accshare = new AccountShare();
                            String groupNameval = String.valueOf(objacc.parent.Name).replaceAll('(?i)[^a-z0-9_]', '')+'PartnerExecutive';//Removing specila charectors from string
                           if(GroupNameandId.get(groupNameval.toLowerCase())!= null){
                                Accshare = PrepareAccountShareRecords(objacc.id,groupNameval);
                                if(Accshare!= null)
                                lstAccshare.add(Accshare);
                           }
                        }
                    }
                    //Added as part of Orange-1109
                    if( Triggernewmap.get(objacc.id).ownerid != Triggeroldmap.get(objacc.id).ownerid){
                        if(PersonRecordTypeId != '' && objacc.recordtypeid == PersonRecordTypeId){
                            Accids.add(objacc.id);
                        }
                    }
                    if(Accids != null && Accids.size()>0){
                        Map<id,Opportunity> OpportunityMap = new Map<id,Opportunity>([select id,Accountid from Opportunity where Accountid IN:Accids]);
                        if(OpportunityMap != null && OpportunityMap.size()>0){
                            OpportunityShareToParentAccount(OpportunityMap,new Map<id,Opportunity>());
                        }
                    }
                    
                    
                }
            }
            system.debug('### lstAccshare--'+lstAccshare);
            if(lstAccshare != null && lstAccshare.size()>0){
                try{
                    insert lstAccshare;
                    }catch(Exception ex){
                     system.debug('### RecordsSharingToParentAccount.AccountShareToParentAccount():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
                        ErrorLogUtility.writeLog('RecordsSharingToParentAccount.AccountShareToParentAccount()',ex.getLineNumber(),'insertData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
                }
                
            }
                
                
        }
        
        public void OpportunityShareToParentAccount(Map<id,Opportunity> Triggernewmap ,Map<id,Opportunity> Triggeroldmap){
            List<Product__Share> lstProductShare = new List<Product__Share>();
            List<AccountShare> lstApplicantAndCoApplicantShare = new List<AccountShare>();
            if(!Triggernewmap.isEmpty() && Triggeroldmap.isEmpty()){//Insert 
                OpportunityShare Oppshare;
                AccountShare AccShare;
                PrepareRoleGroupMap();
                for(Opportunity opp :[select id,Installer_Account__c, Installer_Account__r.Name, SLF_Product__c,Accountid,Co_Applicant__c,Installer_Account__r.Parentid,Installer_Account__r.Parent.Name from Opportunity where id IN:Triggernewmap.keyset()]){
                    if(string.isNotBlank(opp.Installer_Account__c) && string.isNotBlank(opp.Installer_Account__r.Parentid)){
                        String groupNameval = '';
                        String Nameval = String.valueOf(opp.Installer_Account__r.Parent.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';//Removing specila charectors from string
                        String firstChar =Nameval.substring(0,1);
                        if(firstChar.isNumeric()){
                            groupNameval = 'x'+String.valueOf(opp.Installer_Account__r.Parent.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }else{
                            groupNameval = String.valueOf(opp.Installer_Account__r.Parent.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }
                        if(opp.Accountid != null){
                            AccShare = PrepareAccountShareRecords(opp.Accountid,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare);
                        }
                        if(opp.Co_Applicant__c != null){ 
                            AccShare = PrepareAccountShareRecords(opp.Co_Applicant__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare); 
                        }
                        if(opp.Installer_Account__c != null){ 
                            AccShare = PrepareAccountShareRecords(opp.Installer_Account__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare);
                        }
                        Product__Share ObjprodShare;
                        if(GroupNameandId.get(groupNameval.toLowerCase())!= null){
                            if(opp.SLF_Product__c != null){
                                ObjprodShare = prepareProductShare(opp.SLF_Product__c,groupNameval);
                                if(ObjprodShare != null)
                                    lstProductShare.add(ObjprodShare);
                            }
                        }
                    }
                    
                    //Srikanth 04/02 - Added to grant access to child account's executive
                    
                    if(string.isNotBlank(opp.Installer_Account__c)){
                        String groupNameval = '';
                        String Nameval = String.valueOf(opp.Installer_Account__r.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';//Removing specila charectors from string
                            
                        String firstChar =Nameval.substring(0,1);
                        if(firstChar.isNumeric()){
                            groupNameval = 'x'+String.valueOf(opp.Installer_Account__r.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }else{
                            groupNameval = String.valueOf(opp.Installer_Account__r.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }
                       
                        if(opp.Accountid != null){
                            AccShare = PrepareAccountShareRecords(opp.Accountid,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare);
                        }
                        if(opp.Co_Applicant__c != null){ 
                            AccShare = PrepareAccountShareRecords(opp.Co_Applicant__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare); 
                           
                        }
                        if(opp.Installer_Account__c != null){ 
                            AccShare = PrepareAccountShareRecords(opp.Installer_Account__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare);
                        }
                        Product__Share ObjprodShare;
                        if(GroupNameandId.get(groupNameval.toLowerCase())!= null){
                            if(opp.SLF_Product__c != null){
                                ObjprodShare = prepareProductShare(opp.SLF_Product__c,groupNameval);
                                if(ObjprodShare != null)
                                    lstProductShare.add(ObjprodShare);
                            }
                        }
                    }
                }
            }
            if(!Triggernewmap.isEmpty() && !Triggeroldmap.isEmpty()){//Update
                OpportunityShare Oppshare;
                PrepareRoleGroupMap();
                Boolean ownerChanged = false;
                Set<string> IncAccids = new set<string>();
                Map<ID,Account> IncAccMap;
                for(Opportunity oppobj :Triggernewmap.values()){
                    IncAccids.add(oppobj.Installer_Account__c);
                }
                if(!IncAccids.isEmpty()){
                    IncAccMap = new Map<ID,Account>([select id,Name,Parentid,Parent.Name from Account where id IN:IncAccids]);
                }
                for(Opportunity opp :Triggernewmap.values()){
                    if(Triggernewmap.get(opp.id).ownerid != Triggeroldmap.get(opp.id).ownerid)
                        ownerChanged = true;
                    if(string.isNotBlank(opp.Installer_Account__c) && string.isNotBlank(IncAccMap.get(opp.Installer_Account__c).Parentid)){
                        boolean InstallerAccCheck = false;
                        if(Triggernewmap.get(opp.id).Installer_Account__c != null && (ownerChanged ||(Triggernewmap.get(opp.id).Installer_Account__c != Triggeroldmap.get(opp.id).Installer_Account__c))){
                           InstallerAccCheck = true; 
                        }
                        String groupNameval = '';
                        String Nameval = String.valueOf(IncAccMap.get(opp.Installer_Account__c).Parent.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';//Removing specila charectors from string
                            
                        String firstChar =Nameval.substring(0,1);
                        if(firstChar.isNumeric()){
                            groupNameval = 'x'+String.valueOf(IncAccMap.get(opp.Installer_Account__c).Parent.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }else{
                            groupNameval = String.valueOf(IncAccMap.get(opp.Installer_Account__c).Parent.Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }
                        if(opp.Accountid != null && (ownerChanged || InstallerAccCheck || Triggernewmap.get(opp.id).Accountid != Triggeroldmap.get(opp.id).Accountid)){
                            AccountShare AccShare;
                            if(opp.Accountid != null)
                                AccShare = PrepareAccountShareRecords(opp.Accountid,groupNameval);
                            if(AccShare != null) 
                                lstApplicantAndCoApplicantShare.add(AccShare);
                        }
                        if(opp.Co_Applicant__c != null && (ownerChanged || InstallerAccCheck || Triggernewmap.get(opp.id).Co_Applicant__c != Triggeroldmap.get(opp.id).Co_Applicant__c)){ 
                            AccountShare AccShare;
                            if(opp.Co_Applicant__c != null)
                                AccShare = PrepareAccountShareRecords(opp.Co_Applicant__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare); 
                        }
                        if(opp.Installer_Account__c != null && (ownerChanged || InstallerAccCheck || Triggernewmap.get(opp.id).Installer_Account__c != Triggeroldmap.get(opp.id).Installer_Account__c)){ 
                            AccountShare AccShare;
                            if(opp.Installer_Account__c != null)
                                AccShare = PrepareAccountShareRecords(opp.Installer_Account__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare); 
                        }
                       if(Triggernewmap.get(opp.id).SLF_Product__c != null && (ownerChanged || InstallerAccCheck || Triggernewmap.get(opp.id).SLF_Product__c != Triggeroldmap.get(opp.id).SLF_Product__c)){
                            Product__Share  ObjprodShare;
                            if(opp.SLF_Product__c != null)
                                ObjprodShare= prepareProductShare(opp.SLF_Product__c,groupNameval);
                            if(ObjprodShare != null)
                                lstProductShare.add(ObjprodShare);
                       }
                    }
                    //Srikanth 04/02 - Added to grant access to child account's executive
                    
                    if(string.isNotBlank(IncAccMap.get(opp.Installer_Account__c).Name)){
                        boolean InstallerAccCheck = false;
                        if(Triggernewmap.get(opp.id).Installer_Account__c != null && (ownerChanged || Triggernewmap.get(opp.id).Installer_Account__c != Triggeroldmap.get(opp.id).Installer_Account__c)){
                           InstallerAccCheck = true; 
                        }
                        String groupNameval = '';
                        String Nameval = String.valueOf(IncAccMap.get(opp.Installer_Account__c).Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';//Removing specila charectors from string
                            
                        String firstChar =Nameval.substring(0,1);
                        if(firstChar.isNumeric()){
                            groupNameval = 'x'+String.valueOf(IncAccMap.get(opp.Installer_Account__c).Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }else{
                            groupNameval = String.valueOf(IncAccMap.get(opp.Installer_Account__c).Name).replaceAll('(?i)[^a-z0-9_]','')+'PartnerExecutive';
                        }
                        if(InstallerAccCheck || (opp.Accountid != null && (ownerChanged ||Triggernewmap.get(opp.id).Accountid != Triggeroldmap.get(opp.id).Accountid))){
                            AccountShare AccShare;
                            if(opp.Accountid != null)
                                AccShare = PrepareAccountShareRecords(opp.Accountid,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare);
                        }
                        if(InstallerAccCheck ||(opp.Co_Applicant__c != null && (ownerChanged || Triggernewmap.get(opp.id).Co_Applicant__c != Triggeroldmap.get(opp.id).Co_Applicant__c))){ 
                            AccountShare AccShare;
                            if(opp.Co_Applicant__c != null)
                                AccShare = PrepareAccountShareRecords(opp.Co_Applicant__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare); 
                        }
                        if(InstallerAccCheck ||(opp.Installer_Account__c != null &&(ownerChanged || Triggernewmap.get(opp.id).Installer_Account__c != Triggeroldmap.get(opp.id).Installer_Account__c))){ 
                            AccountShare AccShare;
                            if(opp.Installer_Account__c != null)
                                AccShare = PrepareAccountShareRecords(opp.Installer_Account__c,groupNameval);
                            if(AccShare != null)
                                lstApplicantAndCoApplicantShare.add(AccShare);
                        }
                       if(InstallerAccCheck || (Triggernewmap.get(opp.id).SLF_Product__c != null && (Triggernewmap.get(opp.id).SLF_Product__c != Triggeroldmap.get(opp.id).SLF_Product__c || ownerChanged))){
                            Product__Share  ObjprodShare;
                            if(opp.SLF_Product__c != null)
                                ObjprodShare= prepareProductShare(opp.SLF_Product__c,groupNameval);
                            if(ObjprodShare != null)
                                lstProductShare.add(ObjprodShare);
                       }
                    }
                }
            }
            
            try{
                if(lstApplicantAndCoApplicantShare != null && lstApplicantAndCoApplicantShare.size()>0)
                    insert lstApplicantAndCoApplicantShare;
                if(lstProductShare != null && lstProductShare.size()>0)
                    insert lstProductShare;
            }catch(Exception ex){
                 system.debug('### RecordsSharingToParentAccount.OpportunityShareToParentAccount():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
                    ErrorLogUtility.writeLog('RecordsSharingToParentAccount.OpportunityShareToParentAccount()',ex.getLineNumber(),'insertData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
            }
        }
        
        public AccountShare prepareAccountShareRecords(ID Accountid,string groupNameval){
            if(GroupNameandId.get(groupNameval.toLowerCase()) != null){
                AccountShare AccShare = new AccountShare();
                AccShare.AccountAccessLevel = 'Edit';
                AccShare.UserOrGroupId = GroupNameandId.get(groupNameval.toLowerCase());
                AccShare.OpportunityAccessLevel ='Edit';
                AccShare.AccountId = Accountid;
                return AccShare;
            }else{ 
                return null;
            }
        }
        public Product__Share prepareProductShare(ID Productid,String groupNameval){
            if(GroupNameandId.get(groupNameval.toLowerCase()) !=null){
                product__Share  ObjprodShare = new Product__Share();
                ObjprodShare.ParentId = Productid;
                ObjprodShare.UserOrGroupId = GroupNameandId.get(groupNameval.toLowerCase());
                ObjprodShare.AccessLevel = 'Edit';
                return ObjprodShare;
            
            }else{
                return null;   
            }
        }
        
        public void keepManualSharedRecordsOnOwnerChangeOfAccount(Map<id,Account> newMapValues,Map<id,Account> oldMapValues){
            
             List<AccountShare> lstAccShare = new List<AccountShare>();
             Set<String> AccIds = new set<String>();
             for(Account objacc : newMapValues.values()){
                 if(newMapValues.get(objacc.id).ownerid != oldMapValues.get(objacc.id).ownerid)
                    AccIds.add(objacc.id);
                 
             }
             if(AccIds != null && AccIds.size()>0){
                 for(AccountShare objAccSh: [SELECT Id, AccountId, UserOrGroupId, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel,
                                            ContactAccessLevel, RowCause FROM AccountShare where RowCause='Manual' AND ID IN:AccIds]){
                          lstAccShare.add(new AccountShare(AccountId=objAccSh.AccountId,
                                                          UserOrGroupId=objAccSh.UserOrGroupId,
                                                          OpportunityAccessLevel=objAccSh.OpportunityAccessLevel));                      
                 }
             }
            if(!lstAccShare.isEmpty()){
                insert lstAccShare;
            }
        }
        
        
        
        
}