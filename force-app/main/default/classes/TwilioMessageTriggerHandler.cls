/**
* Description: Trigger Business logic associated with Twilio Message is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma K         01/09/2019          Created 
******************************************************************************************/

global without sharing Class TwilioMessageTriggerHandler
{
    public static boolean isTrgExecuting = true;
    
    /**
    * Constructor to initialize
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public TwilioMessageTriggerHandler(boolean isExecuting){
    }
    
    public static boolean runOnce(){
        if(isTrgExecuting){
            isTrgExecuting=false;
            
            return true;
        }else{
            return isTrgExecuting;
        }
    }
    
    /**
    * Description: On After Insert
    *
    * @param new TwilioSF__Message__c records.
    */
    public Static void OnAfterInsert(TwilioSF__Message__c[] newTwilioMsg){
        try{
            system.debug('### Entered into OnAfterInsert() of '+ TwilioMessageTriggerHandler.class);
            set<string> fromPhoneNumSet = new set<string>();
            String decision = '';
            for(TwilioSF__Message__c TwilioMsgIterate : newTwilioMsg){
                if(null != TwilioMsgIterate.TwilioSF__From_Number__c && TwilioMsgIterate.TwilioSF__Direction__c == 'inbound')
                {
                    string phoneNum = TwilioMsgIterate.TwilioSF__From_Number__c;
                    System.debug('###phoneNum :'+phoneNum);
                    if(phoneNum.contains('+1'))
                    {
                        phoneNum = phoneNum.remove('+1');
                    }
                    System.debug('###after :'+phoneNum);
                    fromPhoneNumSet.add(phoneNum);
                    phoneNum = '(' + phoneNum.substring(0, 3) + ') ' + 
                    phoneNum.substring(3, 6) + '-' + phoneNum.substring(6, 10);
                    fromPhoneNumSet.add(phoneNum);
                    decision = TwilioMsgIterate.TwilioSF__Body__c.toLowerCase();
                }
            }
            System.debug('###fromPhoneNumSet :'+fromPhoneNumSet);
            Map<Id,Draw_Requests__c> applicantDrawConMap = new Map<Id,Draw_Requests__c>();
            if(!fromPhoneNumSet.isEmpty())
            {
                Boolean sendTwilioMessage = false;
                Map<string,Twilio_Templates__c> twilioMessageMap = new Map<string,Twilio_Templates__c>();
                List<Twilio_Templates__c> twlTemplate = [select Id,Reply_Text__c,Message_Type__c,Response_Text__c from Twilio_Templates__c];
                for(Twilio_Templates__c twlTemplateIterate : twlTemplate)
                {
                    twilioMessageMap.put(twlTemplateIterate.Reply_Text__c.toLowerCase(),twlTemplateIterate);
                }
                if(!twlTemplate.isEmpty())
                {
                    if(twilioMessageMap.containsKey(decision))
                    {
                        Twilio_Templates__c messageType = twilioMessageMap.get(decision);
                        if(messageType.Message_Type__c == 'Static')
                        {
                            sendTwilioMessage = true;
                            
                        }else if(messageType.Message_Type__c == 'OptIn')
                        {
                            //Updated this SOQL with Project_Category__c to get rid of Non-selective query against large object type (more than 200000 rows)
                            //Added by Adithya on 9/13/2019
                            List<Underwriting_File__c> UWRecLst = [select Id,opportunity__r.Account.Id,opportunity__r.Project_Category__c,opportunity__r.Account.SMS_Opt_in__c from Underwriting_File__c WHERE Opportunity__r.Primary_Contact__r.Phone IN: fromPhoneNumSet ];
                            
                            Map<Id,Account> accMap = new Map<Id,Account>();
                            if(!UWRecLst.isEmpty()){
                                //If Customer Sends start then set SMS_Opt_in__c to false on applicant.
                                for(Underwriting_File__c uwIterate : UWRecLst)
                                {
                                    Account accRec = new Account(id = uwIterate.opportunity__r.Account.Id);
                                    accRec.SMS_Opt_in__c = true;
                                    accMap.put(accRec.Id,accRec);
                                }
                                if(!accMap.isEmpty())
                                    update accMap.Values();
                            }
                            sendTwilioMessage = true;
                            
                        }else if(messageType.Message_Type__c == 'OptOut')
                        {
                            //Updated this SOQL with Project_Category__c to get rid of Non-selective query against large object type (more than 200000 rows)
                            //Added by Adithya on 9/13/2019
                            List<Underwriting_File__c> UWRecLst = [select Id,opportunity__r.Account.Id,opportunity__r.Account.SMS_Opt_in__c from Underwriting_File__c WHERE opportunity__r.Account.SMS_Opt_in__c =: true AND Opportunity__r.Primary_Contact__r.Phone IN: fromPhoneNumSet ];
                            
                            Map<Id,Account> accMap = new Map<Id,Account>();
                            if(!UWRecLst.isEmpty()){
                                //If Customer Sends STOP then set SMS_Opt_in__c to false on applicant.
                                for(Underwriting_File__c uwIterate : UWRecLst)
                                {
                                    Account accRec = new Account(id = uwIterate.opportunity__r.Account.Id);
                                    accRec.SMS_Opt_in__c = false;
                                    accMap.put(accRec.Id,accRec);
                                }
                                if(!accMap.isEmpty())
                                    update accMap.Values();
                            }
                            sendTwilioMessage = true;
                        }else{
                            List<Draw_Requests__c> drawRequestLst = [select id,Primary_Applicant_Email__c,Total_Amount_Drawn__c,Maximum_loan_amount__c,Combined_Loan_Amount__c,
                            Remaining_amount_for_additional_payment__c,Installer_Account_Name__c,Sales_Rep_Name__c,Customer_Name__c,
                            Installer_Email__c,Operations_Email__c,Opp_Hash_Id__c,Underwriting__r.opportunity__r.Installer_Account__r.Installer_Email__c,
                            Underwriting__r.opportunity__r.Installer_Account__r.Operations_Email__c,Underwriting__r.opportunity__r.Hash_Id__c,Underwriting__r.opportunity__c,Hash_Id__c,
                            Underwriting__r.Total_Amount_Drawn__c,Underwriting__r.opportunity__r.Account.Name,
                            Underwriting__r.Opportunity__r.Owner.Name,Underwriting__r.opportunity__r.Combined_Loan_Amount__c,
                            Underwriting__r.opportunity__r.Installer_Account__r.Name,
                            Underwriting__r.Opportunity__r.Primary_Contact__r.Phone,Underwriting__r.Opportunity__r.Primary_Applicant_Email__c,
                            Underwriting__r.Opportunity__r.CoApplicant_Contact__c,Underwriting__r.Amount_Remaining__c,
                            Underwriting__r.Opportunity__r.Primary_Contact__c,Status__c,Amount__c,Approval_Date__c,Decline_Date__c,Draw_Methodology__c,Level__c,
                            Project_Complete_Certification__c,Reminder_Sent__c,Request_Date__c,Requester__c,Text_Approval_Sent__c,Underwriting__c,
                            Underwriting__r.opportunity__r.Account.SMS_Opt_in__c FROM Draw_Requests__c WHERE Underwriting__r.Opportunity__r.Primary_Contact__r.Phone IN: fromPhoneNumSet 
                            AND Underwriting__r.opportunity__r.Account.SMS_Opt_in__c = true AND Status__c = 'Requested'];
                            
                            system.debug('### drawRequestLst '+ drawRequestLst);
                            //Added by Suresh (Added if condition) to fix Orange-2047
                            if(drawRequestLst!=null && !drawRequestLst.isEmpty()){
                                Set<Id> drawRequestIds = new Set<Id>();
                                for(Draw_Requests__c drawReqIterate : drawRequestLst){
                                    drawRequestIds.add(drawReqIterate.Id);
                                }
                                if(!drawRequestIds.isEmpty())
                                {
                                    applicantDrawConMap.put(drawRequestLst[0].Underwriting__r.Opportunity__r.Primary_Contact__c,drawRequestLst[0]);
                                    if(drawRequestIds.size() == 1)
                                    {
                                        List<Draw_Requests__c> updateDrawRequestList = new List<Draw_Requests__c>();
                                        if(decision == 'Yes' && drawRequestLst[0].Status__c == 'Requested' && drawRequestLst[0].Underwriting__r.Opportunity__r.Account.SMS_Opt_in__c)
                                        {
                                            drawRequestLst[0].Status__c = 'Approved';
                                            drawRequestLst[0].Approval_Date__c = system.NOW();
                                            drawRequestLst[0].Draw_Methodology__c = 'Short-code text';
                                            //update drawRequestLst[0];
                                            updateDrawRequestList.add(drawRequestLst[0]);
                                            //update updateDrawRequestList;
                                            DrawRequestUtil.recalculate(drawRequestLst[0].Underwriting__r.opportunity__c);
                                        }
                                        
                                        else if(decision == 'No' && drawRequestLst[0].Status__c == 'Requested' && drawRequestLst[0].Underwriting__r.Opportunity__r.Account.SMS_Opt_in__c)
                                        {
                                            drawRequestLst[0].Status__c = 'Declined';
                                            drawRequestLst[0].Approval_Date__c = null;
                                            drawRequestLst[0].Decline_Date__c = system.NOW();
                                            drawRequestLst[0].Draw_Methodology__c = 'Short-code text';
                                            //update drawRequestLst[0];
                                            updateDrawRequestList.add(drawRequestLst[0]);
                                            //update updateDrawRequestList;
                                            DrawRequestUtil.recalculate(drawRequestLst[0].Underwriting__r.opportunity__c);
                                            
                                        }else{
                                            decision = 'help';
                                            sendTwilioMessage = true;
                                        }
                                        System.debug('###updateDrawRequestList :'+updateDrawRequestList);
                                        if(updateDrawRequestList!=null && !updateDrawRequestList.isEmpty()){
                                            update updateDrawRequestList;
                                        }
                                    }else{
                                        //If More than One Draw records found 
                                        ErrorLogUtility.writeLog('TwilioMessageTriggerHandler.OnAfterInsert()',null,'OnAfterInsert()','Too many DrawRequest found, hence could not approve Records IDs:'+drawRequestIds+ '','Error','');
                                        decision = 'Error';
                                        sendTwilioMessage = true;
                                    }
                                    
                                }
                            }else{
                                //If No Draw records found 
                                decision = 'NoDrawRequest';
                                sendTwilioMessage = true;
                                ErrorLogUtility.writeLog('TwilioMessageTriggerHandler.OnAfterInsert()',null,'OnAfterInsert()','No DrawRequest found, hence could not proceed further with Phone Number:'+newTwilioMsg[0].TwilioSF__From_Number__c+ '','Error','');
                            }
                        }
                    }else{
                        decision = 'help';
                        sendTwilioMessage = true;
                    }
                }
                 
                if(sendTwilioMessage && string.isNotBlank(decision))
                {
                    Twilio_Templates__c messageType = twilioMessageMap.get(decision.toLowerCase());
                    Map<String,String> params = new Map<String,String>();
                    SFSettings__c settings = SFSettings__c.getOrgDefaults();
                    params = new Map<String,String> {
                    'To'   => newTwilioMsg[0].TwilioSF__From_Number__c,
                    'From' => settings.Twilio_From_Phone_Number__c,
                    'Body' => messageType.Response_Text__c
                    };
                    
                    if(!params.isEmpty())
                    {
                         TwilioUtil.invokeTwilioWS(params);
                    }
                }
            }
        }catch(Exception err){
            system.debug('### TwilioMessageTriggerHandler.OnAfterInsert():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('TwilioMessageTriggerHandler.OnAfterInsert()',err.getLineNumber(),'OnAfterInsert()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));         
        }
        system.debug('### Exit from OnAfterInsert() of '+ TwilioMessageTriggerHandler.class);        
    }    
    
}