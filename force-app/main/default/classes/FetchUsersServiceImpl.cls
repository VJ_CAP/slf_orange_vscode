/**
* Description: Fetch Users related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh                  03/02/2018            Created
******************************************************************************************/
public without sharing class FetchUsersServiceImpl extends RESTServiceBase{  
    private String msg = '';
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + FetchUsersServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        UnifiedBean reqData ;
        try{
            if(string.isNotBlank(rw.reqDataStr))
            {
                reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            }else{
                reqData = new UnifiedBean();
            }
            system.debug('### reqData '+reqData);
            UnifiedBean respData = new UnifiedBean(); 
            respData.users = new List<UnifiedBean.UsersWrapper>();
            respData.error = new list<UnifiedBean.errorWrapper>();
            
            string errMsg = '';
            list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            Savepoint sp = Database.setSavepoint();
            try
            {  
                integer  count;
                Integer offset;
                Integer noOfRecs = reqData.numberOfRecords;
                if(reqData.pageNumber == 1){
                    offset =0;
                }else{
                    if(reqData.pageNumber != null && reqData.numberOfRecords != null)//Added 'if' condition by Deepika on 28-12-2018
                        offset= (reqData.pageNumber-1)*reqData.numberOfRecords;
                }
                
                System.debug('### offset '+offset);
                if(offset>2000){
                    iolist = new list<DataValidator.InputObject>{};
                    errMsg = ErrorLogUtility.getErrorCodes('387', null);
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    respData.error.add(errorMsg);
                    respData.returnCode = '387';
                    iRestRes = (IRestResponse)respData;
                    res.response= iRestRes;
                    return res.response;
                }
                
                string conditions = '';
                List<String> lstConditions = new List<String>();
                
                if (reqData.isActive!= null) {
                    lstConditions.add('isActive=' + reqData.isActive);
                }
                if (null !=reqData.searchKey && String.isNotBlank(reqData.searchKey)) {
                    String SearchValue = '%'+String.escapeSingleQuotes(reqData.searchKey)+'%';//Added 'escapeSingleQuotes' by Deepika on 28-12-2018
                    lstConditions.add('(Email LIKE \'' + SearchValue+ '\' or Name LIKE \'' + SearchValue + '\' or FirstName LIKE \'' + SearchValue + '\' or LastName LIKE \'' + SearchValue + '\' or Title LIKE \'' + SearchValue + '\')');
                }
                
                //Added by Adithya as part of ORANGE-638
                System.debug('######'+reqData.titles);
                if(null != reqData.titles && !reqData.titles.isEmpty())
                {
                    list<string> reqTitles = reqData.titles;
                    reqTitles.add('');
                    lstConditions.add('Title IN: reqTitles ');
                }
                system.debug('============='+reqData.profile);
                //Added by Ravi as part of ORANGE-4651
                if(null != reqData.profile && !reqData.profile.isEmpty())
                {
                    list<string> reqProfiles = reqData.profile;
                    reqProfiles.add('');
                    lstConditions.add('Profile.Name IN: reqProfiles ');
                }
                 system.debug('====lstConditions========='+lstConditions);
                //Default filter condition
                lstConditions.add('UserType=\'PowerPartner\'');
                lstConditions.add('Id IN: usrIds');
                
                if(lstConditions!=null && lstConditions.size()>0){
                    for (integer i = 0; i < lstConditions.size(); i++) {
                        if (i == lstConditions.size() - 1) {
                            conditions = conditions + lstConditions.get(i);
                        } else {
                            conditions = conditions + lstConditions.get(i) + ' and ';
                        }
                    }
                }
                // 06/05/2019 - Srikanth - Orange-4651 -> exclude OpsUser from return list by looking at Default_User__c flag on User object
                conditions = (conditions != NULL && conditions != '')?conditions+' and Default_User__c  = false':' Default_User__c  = false';
                
                Set<Id> usrIds = new Set<Id>();
                Set<id> installerAccIds = new Set<id>();
                ID LoggedinUseridval = UserInfo.getUserId();
                List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c,contact.Account.ParentId,contact.Account.FNI_Domain_Code__c,userrole.DeveloperName  from User where Id =: userinfo.getUserId()];
                installerAccIds.add(loggedInUsr[0].contact.AccountId);
                if(reqData.includeHierarchy != null && reqData.includeHierarchy){
                    if(loggedInUsr[0].contact.Account.ParentId != null && loggedInUsr[0].userrole.DeveloperName.contains('PartnerExecutive'))
                    {
                        //to Get Parent Execs only

                        List<User> usrLst = [Select Id, Name from User where Contact.AccountId = :loggedInUsr[0].contact.Account.ParentId and UserRole.DeveloperName like '%PartnerExecutive%'];
                        
                        for(User usr : usrLst){
                            usrIds.add(usr.id);
                        }
                    }
                    
                    //Added Excecutive check by Rajesh
                    if(loggedInUsr[0].contact.Account.ParentId == null && loggedInUsr[0].userrole.DeveloperName.contains('PartnerExecutive'))
                    { 
                        List<Account> childInstallerAccs = [Select id,Name,ParentId FROM ACCOUNT where ParentId =: loggedInUsr[0].contact.Accountid];
                        if(!childInstallerAccs.isEmpty()){
                            for(Account acc :childInstallerAccs){
                                installerAccIds.add(acc.id);
                            }
                        }
                    }
                }
                System.debug('### installerAccIds '+installerAccIds);
                List<User> usrLst = [Select Id, Name from User where Contact.AccountId in :installerAccIds];
                for(User usr : usrLst){
                    usrIds.add(usr.id);
                }
                String queryStr = '';
                String countQuery = '';
                
               //Srikanth - 06/05/2019 - Orange-4651 -> 'conditions' will never be blank and else condition can be removed.
               if(string.isNotBlank(conditions))
                {
                    countQuery = 'SELECT count() FROM User where '+conditions;
                    queryStr = 'SELECT Id, Username, LastName,Title, FirstName, MiddleName,LastModifiedDate, Name, Email, Contact.ReportsToid,Contact.ReportsTo.Name,UserRole.Name,Phone, Alias, IsActive, UserRoleId, UserType, ContactId, AccountId, PortalRole, IsPortalEnabled, LastViewedDate,LastLoginDate, Hash_Id__c FROM User where '+conditions+ ' order by LastName ASC,Title DESC NULLS LAST LIMIT :noOfRecs OFFSET :offset';
                }else{
                    countQuery= 'SELECT count() FROM User where Default_User__c = false';
                    queryStr = 'SELECT Id, Username, LastName,Title, FirstName,LastModifiedDate, MiddleName,Contact.ReportsToid,Contact.ReportsTo.Name, Name, Email,UserRole.Name, Phone, Alias, IsActive, UserRoleId, UserType, ContactId, AccountId, PortalRole, IsPortalEnabled, LastViewedDate,LastLoginDate, Hash_Id__c FROM User where Default_User__c = false order by LastName ASC,Title DESC NULLS LAST LIMIT :noOfRecs OFFSET :offset';
                }  
                List<User>  userList  = Database.query(queryStr);
                count = database.countQuery(countQuery); 
               Set<Id> userIds = new Set<Id>();
                Map<Id,UserLogin> mapUserLogin = new Map<Id,UserLogin>();
                Date lastLoginDate;
                // Added this part of 5256
                if(!userList.isEmpty()){
                    for(User userRecs :userList){
                        userIds.add(userRecs.Id);
                        system.debug('serRecs.Id============='+userRecs.Id);
                        if(lastLoginDate == null && userRecs.lastLoginDate != null ){
                            lastLoginDate= userRecs.lastLoginDate.date();
                        }else if(lastLoginDate != null && lastLoginDate < userRecs.LastLoginDate && userRecs.lastLoginDate != null ){
                            lastLoginDate= userRecs.LastLoginDate.date();
                        }
                    }
                    if(lastLoginDate != null)
                       lastLoginDate =  lastLoginDate.addDays(-1);
                }
                system.debug('lastLoginDate========='+lastLoginDate+'===='+userIds);
                if(!userIds.isEmpty()){
                    system.debug('lastLoginDate ============='+lastLoginDate );
                    for(UserLogin uLogin:[SELECT id, userid, isfrozen FROM UserLogin WHERE userid in:userIds  order by lastmodifieddate ASC]){
                        mapUserLogin.put(uLogin.UserId,uLogin);
                    }
                }
                    // Added this part of 5256 End  
                  system.debug('mapUserLogin============'+mapUserLogin);                    
                if(!userList.isEmpty()){
                    for(User userRecs :userList){
                        UnifiedBean.UsersWrapper userWrap = new UnifiedBean.UsersWrapper();
                        system.debug('mapUserLogin.get(userRecs.Id)============'+mapUserLogin.get(userRecs.Id)); 
                        userWrap.isFrozen = (mapUserLogin != null && mapUserLogin.get(userRecs.Id) != null?mapUserLogin.get(userRecs.Id).isFrozen:false);  //    // Added this part of 5256 Start
                        userWrap.id = userRecs.id;
                        userWrap.userName = userRecs.userName;
                        userWrap.hashId = userRecs.Hash_Id__c;
                        userWrap.firstName = userRecs.FirstName;
                        userWrap.lastName = userRecs.LastName;
                        userWrap.email = userRecs.Email;
                        userWrap.phone = userRecs.Phone;
                        userWrap.title = userRecs.Title;
                        userWrap.lastLogin = userRecs.LastLoginDate!=null ? String.valueOf(userRecs.LastLoginDate) : 'NA';
                        userWrap.isActive = userRecs.isActive;
                        userWrap.contactid = userRecs.Contactid;
                        userWrap.reportsTo = userRecs.Contact.ReportsTo.Name;
                        respData.users.add(userWrap);
                    }
                    respData.returnCode = '200';
                    respData.totalNumberOfRecords = count;
                    respData.pageNumber = reqData.pageNumber;
                    respData.numberOfRecords = reqData.numberOfRecords;
                }else{
                    respData.returnCode = '388';
                    List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                    UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                    errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('388', null);
                    errorLst.add(errorWrap);
                    respData.error = errorLst;
                }
            }catch(Exception err){
                Database.rollback(sp);
                 respData.returnCode = '400';
                List<UnifiedBean.errorWrapper> errorLst = new List<UnifiedBean.errorWrapper>();
                UnifiedBean.errorWrapper errorWrap = new UnifiedBean.errorWrapper();
                errorWrap.errorMessage = ErrorLogUtility.getErrorCodes('400', null);
                errorLst.add(errorWrap);
                respData.error = errorLst;
                
                system.debug('### FetchUsersServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
                
                ErrorLogUtility.writeLog('FetchUsersServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            } 
            res.response = (IRestResponse)respData;
        }catch(Exception err){
           
            system.debug('### FetchUsersServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' FetchUsersServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            
            UnifiedBean unifiedBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            unifiedBean.error  = errorWrapperLst ;
            unifiedBean.returnCode = '214';
            iRestRes = (IRestResponse)unifiedBean;
            res.response= iRestRes;
        }
       
        system.debug('### Exit from fetchData() of ' + FetchUsersServiceImpl.class);
        
        return res.response ;
    }
}