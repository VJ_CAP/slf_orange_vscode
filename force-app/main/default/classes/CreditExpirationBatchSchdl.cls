/**
*   Description: This Batch Class updates Credit status to 'Expired' when FNI_Credit_Expiration__c = Yesterday.
*
*   Modification Log :
---------------------------------------------------------------------------
   Developer               Date                Description
---------------------------------------------------------------------------
    Deepika              12/20/2018               Created
******************************************************************************************/
global class CreditExpirationBatchSchdl implements Database.Batchable<sObject>,Schedulable {
    global String query;
    
    /**
    * Description:    Start method to query all the expired credit records
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        //query = 'Select Id, Name, Status__c, Opportunity__c, M1_Approval_Requested__c from SLF_Credit__c where M1_Approval_Requested__c = false AND FNI_Credit_Expiration__c != null AND FNI_Credit_Expiration__c >= YESTERDAY AND Opportunity__c != null AND Opportunity__r.StageName != \'Archive\' AND Status__c != \'Expired\' limit 2000';
        query = 'Select Id, Name, Status__c, Opportunity__c, M1_Approval_Requested__c from SLF_Credit__c where M1_Approval_Requested__c = false AND FNI_Credit_Expiration__c != null AND FNI_Credit_Expiration__c = YESTERDAY AND Opportunity__c != null AND Opportunity__r.StageName != \'Archive\' AND Status__c != \'Expired\' limit 2000';
        return Database.getQueryLocator(query);
    }
    
    /**
    * Description:    Batch execute method to update all expired credits status to "Expired"
    */
    global void execute(Database.BatchableContext BC, List<SLF_Credit__c> creditList){
        system.debug('### Entered into execute() of '+ CreditExpirationBatchSchdl.class); 
        system.debug('### creditList : '+ creditList);
        Set<Id> opportunityIds = new Set<Id>();
        if(!creditList.isEmpty()){
        
            for(SLF_Credit__c crdObj : creditList){
                opportunityIds.add(crdObj.Opportunity__c);
                crdObj.Status__c = 'Expired';
            }

            //Added as part of ORANGE-8958 Start
            system.debug('### opportunityIds - '+opportunityIds);
            List<Underwriting_File__c> underWritingList = [SELECT id,Approved_for_Payments__c FROM Underwriting_File__c WHERE OpportunityID18Char__c IN :opportunityIds  AND Opportunity__r.Project_Category__c = 'Home' and Approved_for_Payments__c = true];
            for(Underwriting_File__c udwObj : underWritingList){
                udwObj.Approved_for_Payments__c = false;
                //underWritingList.add(udwObj);   
            }
            if(null != underWritingList && !underWritingList.isEmpty()){
                update underWritingList;
            }
            //Added as part of ORANGE-8958 End
        }
        
        if(!creditList.isEmpty()){
            try{
                if(Test.isRunningTest()) {
                    creditList.add(new SLF_Credit__c());
                }
                Database.SaveResult[] saveRsltList = Database.update(creditList,false); 
                
                List<Error_Log__c> errorLogList = new List<Error_Log__c>();
                // Iterate through each returned result of update
                for(Integer i=0;i<saveRsltList.size();i++){
                    // If update failed, create Error log records to capture each record Id and its error.
                    if (!saveRsltList[i].isSuccess()) {
                        String errorString = '';
                        for(Database.Error err : saveRsltList[i].getErrors()) {
                            if(errorString != '')
                                errorString = errorString + ', ' + err.getMessage();
                            else
                                errorString = errorString + err.getMessage();
                        }
                        Error_Log__c errorLog = new Error_Log__c();
                        errorLog.Functionality__c= 'CreditExpirationBatchSchdl.execute()';
                        errorLog.Method_Name__c= 'execute()';  
                        errorLog.RecordIds__c = creditList[i].Id;
                        if(errorString.length() > 32767){
                            errorLog.Message__c = errorString.left(32760) + '...';   
                        }
                        else{
                            errorLog.Message__c = errorString;
                        }
                        errorLog.Type__c = 'Error';                       
                        errorLogList.add(errorLog);
                    }
                }
                if(!errorLogList.isEmpty())
                    insert errorLogList;
            }Catch(Exception ex){
                ErrorLogUtility.writeLog('CreditExpirationBatchSchdl.execute()',ex.getLineNumber(),'execute()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));   
            }
        }
        system.debug('### Exit from execute() of '+ CreditExpirationBatchSchdl.class);
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    /**
    * Description: Schedule execute method to schedule the batch class with size 1.
    */
    global void execute(SchedulableContext sc) {
        CreditExpirationBatchSchdl batchObj = new CreditExpirationBatchSchdl(); 
        database.executebatch(batchObj,1);
    }
}