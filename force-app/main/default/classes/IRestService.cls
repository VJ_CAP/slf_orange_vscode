/**
* Description: REST interface with all the operation (like Create, Update, Read, Delete), can be performed on sObjects is declared.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public interface IRestService{
    //Read and respond with custom JSON output over POST 
	IRestResponse fetchData(RequestWrapper rw);
	IRestResponse deleteData(RequestWrapper rw);

}