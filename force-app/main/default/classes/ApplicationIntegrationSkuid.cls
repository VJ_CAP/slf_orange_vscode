global class ApplicationIntegrationSkuid
{
    global class Input
    {
        @InvocableVariable(required=true) global Id oppId;

        global Input()
        {
            //  nothing to do
        }

        global Input(Id oppId)
        {
            this.oppId = oppId;
        }
    }

    class CustomException extends Exception
    {

    }

    //  ***********************************************

    global class Output
    {
        @InvocableVariable global Boolean success;
        @InvocableVariable global String errorMessage;
    }

    //  ***********************************************

    @InvocableMethod(label='FNI Web Service Pull' description='Pull FNI Data for TCU Opportunities')
    public static list<Output> ApplicationIntegrationSkuid(list<Input> inputs)
    {
        list<Output> outputs = new list<Output>();                
        set<String> setOfOpps = new set<String>();

        for (Input i : inputs)
        {
            setOfOpps.add(i.oppId);
        }

        map<Integer,String> errorsMap = new map<Integer,String>
        {
            0 => '',
            1 => 'The Last name provided does not match the returned value for the FNI Application.',
            2 => 'Unable to Update Opportunity!',
            9 => 'The opportunity provided does not have a value for FNI Application Id.'
        };

        for (Opportunity opp : [SELECT Id, FNI_Application_Id__c FROM Opportunity WHERE Id =:setOfOpps])
        {        
            Output o = new Output();    
            o.success = true;
            outputs.add (o);

            if(opp.FNI_Application_Id__c == null)
            {
                o.success = false;
                o.errorMessage = errorsMap.get (9); 
                continue;
            }
 
            Integer code = ApplicationIntegrationUtility.updateRecordFromIntegration(opp.Id, opp.FNI_Application_Id__c);

            o.success = (code == 0);
            o.errorMessage = errorsMap.get (code);   

            if (o.success == false) throw new CustomException (o.errorMessage);                 
        }      
        return outputs;
    }
}