/**
* Description: Credit Extension Request Logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh.R                11/22/2018          Created
******************************************************************************************/
public with sharing class CreditExtensionRequestServiceImpl extends RESTServiceBase{  
   
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + CreditExtensionRequestServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean responseBean = new UnifiedBean();
        responseBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        system.debug('### reqData '+reqData);
        responseBean.error = new list<UnifiedBean.errorWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(reqData.projects != null && reqData.projects[0].credits != null)
            {
               
                //Changed to SOQL query because FNI call should happen as Credit driven insted of opportunity driven
                list<SLF_Credit__c> credit = [select Id,Name,Primary_Applicant__c,Co_Applicant__c, LT_FNI_Reference_Number__c from SLF_Credit__c where id =: reqData.projects[0].credits[0].id AND IsSyncing__c = true AND FNI_Credit_Expiration__c > today];
                if(credit.isEmpty())
                {
                    iolist = new list<DataValidator.InputObject>{};
                    errMsg = ErrorLogUtility.getErrorCodes('340',null);
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    responseBean.error.add(errorMsg);
                    responseBean.returnCode = '340';
                    iRestRes = (IRestResponse)responseBean;
                    res.response = iRestRes;
                }
                else
                {
                    Account acc = new Account(id = credit[0].Primary_Applicant__c);
                    acc.Credit_Extension_Status__c = 'Requested';
                    acc.Extension_Requested_Date_Time__c = system.Now();
                    update acc;
                    
                    UnifiedBean.CreditsWrapper creditIterate = new UnifiedBean.CreditsWrapper();
                    creditIterate.fniLTReq = buildFNIExtnBody(credit[0].Id, credit[0].LT_FNI_Reference_Number__c);
                    creditIterate.id = credit[0].id;
                    projectWrap.credits.add(creditIterate);
                    
                    responseBean.projects.add(projectWrap);
                    responseBean.returnCode = '200';
                    //String soapLTBody = 
                    
                    /*
                    
                    responseBean.message = 'You have successfully submitted your request for credit extension.';
                    */
                    iRestRes = (IRestResponse)responseBean;
                    res.response = iRestRes;
                    system.debug('###..response'+res.response);   
                }
                
            }
            
        }catch(Exception err){
            system.debug('### CreditExtensionRequestServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' CreditExtensionRequestServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            responseBean.error.add(errorMsg);
            responseBean.returnCode = '400';
            iRestRes = (IRestResponse)responseBean;
            res.response = iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + CreditExtensionRequestServiceImpl.class);
        
        return responseBean;
        
    }
    
    public static Boolean runningInASandbox() {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        return (!settings.IS_PRODUCTION__c);
    }

    public static string buildFNIExtnBody(Id slfCreditId, String FNI_REFNUM){
        String soapBody = '';
        try{
            SLF_Boomi_API_Details__c FNIAPIDetails ;

            // Description: Added code to read boomi and FNI API details from the custom settings
            Map<String,SLF_Boomi_API_Details__c> FNIAPIDetailsMap = SLF_Boomi_API_Details__c.getAll();

            if(!runningInASandbox()){
                FNIAPIDetails = FNIAPIDetailsMap.get('FNI Prod');
            }else{
                FNIAPIDetails = FNIAPIDetailsMap.get('FNI QA');
            }

            soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:req="http://tko.fni.com/application/request.xsd" xmlns:tran="http://tko.fni.com/application/transaction.xsd">';
            soapBody += '<soapenv:Header/>';
            soapBody += '<soapenv:Body>';
            soapBody += '<req:REQUEST>';
            soapBody += '<tran:TransactionControl>';
            soapBody += '<tran:UserName>' +FNIAPIDetails.Username__c +'</tran:UserName>';
            soapBody += '<tran:Password>' +FNIAPIDetails.Password__c +'</tran:Password>';

            soapBody += '<tran:TransactionTimeStamp>' +
            System.now().year() + '-' +
            (System.now().month() < 10 ? '0': '') +System.now().month() + '-' +
            (System.now().day() < 10 ? '0': '') +
            System.now().day() + 'T' +
            System.now().time() +
            '</tran:TransactionTimeStamp>';
            soapBody += '<tran:Action>EXTEND</tran:Action>';
            soapBody += '<tran:ExternalReferenceNumber>'+slfCreditId+'</tran:ExternalReferenceNumber>'; 

            soapBody += '<tran:PartnerId>SALESFORCE</tran:PartnerId>';
            soapBody += '<tran:FNIReferenceNumber>'+FNI_REFNUM+'</tran:FNIReferenceNumber>';
            soapBody += '<tran:DomainName>';
            soapBody += '</tran:DomainName>';
            soapBody += '</tran:TransactionControl>';

            soapBody += '</req:REQUEST>';
            soapBody += '</soapenv:Body>';
            soapBody += '</soapenv:Envelope>';
            system.debug('### soapBody '+soapBody);

        } catch(Exception err){
            system.debug('### SubmitCreditDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' SubmitCreditDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### soapBody '+soapBody);
        system.debug('### Exit from transformOutput() of '+SubmitCreditDataServiceImpl.class);
        
        return soapBody;
    }    
}