/**
* Description: User and Contact Creation logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Brahmeswar             06/08/2018           Created
******************************************************************************/

public without sharing class UpdateStipsDataServiceImpl extends RestDataServiceBase{
   /*
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+UpdateStipsDataServiceImpl.class);
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        UnifiedBean createstipbean = new UnifiedBean();
        createstipbean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        Savepoint sp = Database.setSavepoint();
        try
        {
            List<Stipulation__c> Stipobjlst;
            if(reqData.projects != null){
                if(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId) && String.isBlank(reqData.projects[0].hashId)){
                    errMsg = ErrorLogUtility.getErrorCodes('203',null);
                    createstipbean.returnCode = '203';
                }else if(String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId)){
                    errMsg = ErrorLogUtility.getErrorCodes('201',null);
                    createstipbean.returnCode = '201';
                }else{
                        system.debug('### reqData.projects[0].id : ' + reqData.projects[0].id);
                        system.debug('### reqData.projects[0].externalId : ' + reqData.projects[0].externalId);
                        system.debug('### reqData.projects[0].hashId : ' + reqData.projects[0].hashId);
                        if(String.isNotBlank(reqData.projects[0].id))
                        {
                            Stipobjlst= [Select id,fileName__c,Underwriting__c,Underwriting__r.Opportunity__c,Stipulation_Data__c,
                                            Stipulation_Data__r.Folder_Name__c,Status__c,FileUploadTimeStamp__c from Stipulation__c 
                                            where status__c != 'Completed' AND 
                                            Underwriting__r.Opportunity__c=:reqData.projects[0].id
                                            AND Stipulation_Data__r.Folder_Name__c =:reqData.projects[0].stips[0].folderName];        
                        }
                        else if(String.isNotBlank(reqData.projects[0].externalId))
                        {
                            Stipobjlst= [Select id,fileName__c,Underwriting__c,Underwriting__r.Opportunity__c,Stipulation_Data__c,
                                            Stipulation_Data__r.Folder_Name__c,Status__c,FileUploadTimeStamp__c from Stipulation__c 
                                            where status__c != 'Completed' AND 
                                            Underwriting__r.Opportunity__r.Partner_Foreign_Key__c=:reqData.projects[0].externalId 
                                            AND Stipulation_Data__r.Folder_Name__c =:reqData.projects[0].stips[0].folderName];        
                        }
                        else if(String.isNotBlank(reqData.projects[0].hashId))
                        {
                            Stipobjlst= [Select id,fileName__c,Underwriting__c,Underwriting__r.Opportunity__c,Stipulation_Data__c,
                                            Stipulation_Data__r.Folder_Name__c,Status__c,FileUploadTimeStamp__c from Stipulation__c 
                                            where status__c != 'Completed' AND Underwriting__r.Opportunity__r.Hash_Id__c=:reqData.projects[0].hashId 
                                            AND Stipulation_Data__r.Folder_Name__c =:reqData.projects[0].stips[0].folderName];        
                        }
                        
                }
            
                System.debug('###Stipobjlst--'+Stipobjlst);
            }
            Set<id> stipIds = new Set<id>();
            if(string.isBlank(errMsg)){ 
                Map<id,Stipulation__c> stipMap = new Map<id,Stipulation__c>();
                if(Stipobjlst != null && !Stipobjlst.isEmpty()){

                    List<Stipulation__c> stiplstToUpdate = new List<Stipulation__c>();
                    for( Stipulation__c Stipobj :  Stipobjlst){
                        stipIds.add(Stipobj.Underwriting__c);
                        Stipobj.FileUploadTimeStamp__c = system.now();
                        Stipobj.fileName__c = reqData.projects[0].stips[0].fileName;
                        Stipobj.Status__c = 'Documents Received';
                        Stipobj.Document_URL__c = reqData.projects[0].stips[0].documentURL;
                        stiplstToUpdate.add(Stipobj);
                    }
                    System.debug('###stiplstToUpdate--'+stiplstToUpdate);
                    if(stiplstToUpdate != null && stiplstToUpdate.size()>0){
                        update stiplstToUpdate;
                    }
                    if(stipIds != null && !stipIds.isEmpty()){
                        List<Stipulation__c> stipList = [select id,Name,Status__c,Description__c,Completed_Date__c,ETC_Notes__c,Stipulation_Data__c,Stipulation_Data__r.Name,Stipulation_Data__r.Upload_Instruction__c,Folder_link__c,Stipulation_Data__r.Installer_Only_Email__c,Stipulation_Data__r.Display_Folder_Text__c,Stipulation_Data__r.Folder_Name__c,fileName__c,FileUploadTimeStamp__c from Stipulation__c where Underwriting__c IN :stipIds];
                        for(Stipulation__c stipulationIterate : stipList)
                        {
                            stipMap.put(stipulationIterate.id,stipulationIterate);
                        }
                    }
                }
                
                if(Stipobjlst != null && Stipobjlst.size()>0){
                    createstipbean = SLFUtility.getUnifiedBean(new set<Id>{Stipobjlst[0].Underwriting__r.Opportunity__c},'Orange',false);
                }
                else
                {
                    createstipbean = SLFUtility.getUnifiedBean(new set<Id>{ID.valueOf(reqData.projects[0].id)},'Orange',false);
                }
                
                List<UnifiedBean.OpportunityWrapper> projectWrapperLst = createstipbean.projects;
                UnifiedBean.OpportunityWrapper projectWrapper = projectWrapperLst.get(0);
                list<UnifiedBean.StipsAndStatusWrapper> stripList = projectWrapper.stips;
                for(UnifiedBean.StipsAndStatusWrapper stipWrap :stripList){
                    Stipulation__c stipRec = stipMap.get(stipWrap.id);
                    stipWrap.folderName = stipRec.Stipulation_Data__r.Folder_Name__c;//Added by Suresh on 09/08/2018 (Orange-89)
                    stipWrap.fileName = stipRec.fileName__c;//Added by Suresh on 09/08/2018 (Orange-89)
                    if(stipRec.FileUploadTimeStamp__c != null)
                        stipWrap.modifiedAt = SLFUtility.getDateFormat(stipRec.FileUploadTimeStamp__c.format());//Modified by deepika on 03/09/2018 (Orange-1219)          
                }

                createstipbean.message = 'Success! Your file has been uploaded.';
                createstipbean.returnCode = '200';
                iRestRes = (IRestResponse)createstipbean;
            }
            else{
                Database.rollback(sp);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                createstipbean.error.add(errorMsg);
                iRestRes = (IRestResponse)createstipbean;
            }
        }catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            gerErrorMsg(createstipbean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)createstipbean;
            system.debug('### UpdateStipsDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('UpdateStipsDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from transformOutput() of '+CreateApplicantDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
}