global class Batch_FetchSite implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {

    DateTime lastExecutionTime;
    DateTime curBatchStartTime;
    Double  batchNumber;
    
    public Batch_FetchSite()
    {
        BoxUtil.checkLimits();
        BoxUtil.abortBatchCheck();
        
        // **** load last execution batch time and set current batch start time
		SFSettings__c settings = SFSettings__c.getOrgDefaults();
        lastExecutionTime = settings.Site_Batch_Execution_TS__c;
        batchNumber = settings.Batch_Number__c;
        if (lastExecutionTime == null) {
            Date dt = date.parse('01/01/2000');
            lastExecutionTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
        }
        
        curBatchStartTime = System.Now();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
	{

        /* Currently we are processing all opportunities, since we do not have a way to tell which 
         * sites have changed. Eventually, we should be able to filter only those opportunities that
         * need to be modified.
         * 
         */
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        String limitClause = '';
        if (settings.MAX_ROWS_IN_BATCH__c != null && String.valueOf(settings.MAX_ROWS_IN_BATCH__c).length() > 0) {
            limitClause = ' LIMIT ' + Integer.valueOf(settings.MAX_ROWS_IN_BATCH__c);
        }
        
         String query = 'SELECT Sighten_UUID__c FROM Opportunity where Sighten_UUID__c != null ORDER BY CreatedDate DESC' + limitClause; 
        
        
         // check if Debug Opportunities have been set
        
        if (settings.Use_Debug_Opportunities__c) { 
            //List<String> oppIds = settings.Debug_Opportunities__c.split(',');
            List<String> strOppIds = settings.Debug_Opportunities__c.split(',');
			List<Id> oppIds = new List<Id>();
            for (String s : strOppIds) oppIds.add(s);
            
            if (oppIds.size() > 0) {	
                query = 'Select Id, Sighten_UUID__c from Opportunity where Id in :oppIds';
            } 
        }
                
        return Database.getQueryLocator(query);
	}

    global void execute(SchedulableContext sc)
    {
        Batch_FetchSite bf = new Batch_FetchSite();
        Database.executeBatch(bf,1);
 	}
    
 	global void execute(Database.BatchableContext BC,List<Opportunity> ol) 
    {
		
       
        List<Site_Download__c> logList = new List<Site_Download__c>();
        List<Attachment> attList = new List<Attachment>();
        
    	Site_Download__c l = null;
        for (Opportunity o : ol) {
            BoxUtil.abortBatchCheck();
            try {
			 BoxUtil.checkLimits();
             // ** SS CHANGE
             BoxUtil.SiteDownloadWrapper w = sightenCallout.downloadSiteFromSighten(o, lastExecutionTime);
             l = w.site_download;
             if (w.attachment != null)
                 attList.add(w.attachment);
                
             l.Batch_Number__c = batchNumber;
             batchNumber = batchNumber + 1;
             //l = sightenCallOut.getDocsBySiteFromSighten(o, lastExecutionTime);
             l.Notes__c = '';
             l.Processed__c = false;
             l.Processing_Error_Details__c = null;
             l.Success__c = true;
             logList.add(l);
            } catch (Exception e) {
                l = new Site_Download__c();
               	l.Batch_Number__c = batchNumber;
                batchNumber = batchNumber + 1;
                l.Opportunity__c  = o.Id;
             	l.Site_UUID__c = o.Sighten_UUID__c;
                l.Success__c = false;
                l.Processed__c = false;
				l.Notes__c = e.getMessage().Left(255);
                l.Processing_Error_Details__c  = e.getLineNumber() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + e.getStackTraceString();
                logList.add(l);
            }
            
            SFSettings__c settings = SFSettings__c.getOrgDefaults();
            if (settings.Delay_Between_Sighten_Calls__c > 0)
            	sightenCallout.sleep(Integer.valueOf(settings.Delay_Between_Sighten_Calls__c));
        }
        System.Debug('******* UPSERTING logList ' + logList);
        upsert logList Site_UUID__c;
        
        // *** now to process the attachments
        Map<String, Id> uuidSiteMap = new Map<String, Id>();
        for (Site_Download__c s : logList)
            uuidSiteMap.put(s.Site_UUID__C,s.Id);
        
        // *** delete all existing attachments
        List<Attachment> delList = new List<Attachment>([Select Id from Attachment where parentId in :uuidSiteMap.values()]);
        delete delList;
        
        // *** the parentId was set in the name field
        for (Attachment a : attList) {
            a.parentId = uuidSiteMap.get(a.name);
        	a.name = a.name + ' - ' + System.Now();
        }
        insert attList;
        
        
        
        update ol;
        
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        
        if (!Test.IsRunningTest()) {
            if (settings.CHAIN_BATCHES__c) {
       			 Database.executeBatch(new Batch_ProcessSite(), 1);
            }
        }
    }
}