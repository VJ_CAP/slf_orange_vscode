@isTest(seeAllData=false)
public class TestBoxUtil {

    @testSetup static void setUpData()
    {
        //get Custom setting data.
        //SLFUtilityTest.createCustomSettings(); 
        
        TestSightenCallout.setUpData();
    }
    
    public static testMethod void testMethods()
    {
        List<StaticResource> rl = new List<StaticResource>([Select Id, Body from StaticResource where Name = 'sf_settings']);
        BoxUtil.generateSHA1Digest(rl[0].Body);

        BoxUtil.queryCategoryMappingFieldsFromUnderwriting(null);
        
        try {
        BoxUtil.getBoxFolderId(null);    
        
        } catch(Exception e) {}
       
        try {
        BoxUtil.getFolderSharedLink(null);
        } catch (Exception e) {}
        
        try {
        BoxUtil.getFolderSharedLinkNew(null);
        } catch (Exception e) {}
        
        BoxUtil.sharedAPI = new BoxApiConnection('111');
        BoxUtil.setBoxTokens();
            
        BoxUtil.getBoxAPISession();
        
        BoxUtil.getBoxToken();
        
        try {
            BoxUtil.getChildFoldersFromBox('abc');  
        } catch (Exception e) {}
        
         try {
            BoxUtil.uploadBoxFile('abc','def','ghi',rl[0].body);  
        } catch (Exception e) {}

        BoxUtil.SiteDownloadWrapper w1 = new BoxUtil.SiteDownloadWrapper ();  
    }
    
    
    public static testMethod void test()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock(); 
        mock.setStaticResource('sighten_site_create_response');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        List<StaticResource> rl = new List<StaticResource>([Select Id, Body from StaticResource where Name = 'sf_settings']);
       
        
         Test.startTest(); 
         BoxUtil.uploadNewVersionOfFile('123','test.txt',rl[0].Body);
         Test.stopTest(); 
    }
/*    
    public static testMethod void testBoxLoginCode()
    {
        PageReference p = Page.BoxLogin;
        //p.getParameters().put('id',ol[0].Id);
        Test.setCurrentPage(p);
        
        Test.startTest();
        BoxController c = new BoxController();
        Test.stopTest();
        
    }
    
    public static testMethod void testBoxUploadCode()
    {
        Test.startTest();
        BoxUtil.getCategoryFolders();
        List<Opportunity> ol = new List<Opportunity>([Select Id, AccountId from Opportunity]);
        System.Assert(ol.size() > 0, 'Opportunities not created in test');
        
        BoxUtil.setBoxFolderID(ol[0].Id, '1234567');
        
        
        BoxUtil.getBoxAPISession();
       
        
//        BoxUploadController.getChildFolderId(ol[0].Id,'oppName','folderName',false);
//        BoxUploadController.getChildFolderId(ol[0].Id,'oppName','folderName',true);
        BoxUtil.getChildFolderIDToken(ol[0].Id,'oppName','folderName',false);
        BoxUtil.getChildFolderIDToolkit(ol[0].Id,'oppName','folderName',false);
        BoxUtil.getOppFolderId(ol[0].Id);
        BoxUtil.getSubFolderID('parent','folder');
        
        BoxUploadController.getBoxToken();
        BoxUploadController.updateStoredBoxToken('acessToken','refreshToken',1000);
        BoxUploadController.getRefreshedBoxToken();
        BoxUploadController.getRefreshedAPIConnection();
        
        PageReference p = Page.UploadFileToBox;
        p.getParameters().put('id',ol[0].Id);
        Test.setCurrentPage(p);
        
        BoxUploadController c = new BoxUploadController();
        c.getUploadFolderList();
        c.getAuthUrl();
        c.getPageUrl();
        c.getHasToken();
        c.refreshToken();
        
        BoxUploadController.createFolder(ol[0].accountId);
        
        
        sightenUtil.createFRUPEntries('12334555',ol[0].Id);
        
        
        Test.stopTest();
    }
    
    public static testMethod void testUpdateUnderwitingFile()
    {
        List<Opportunity> ol = new List<Opportunity>([Select Id, AccountId from Opportunity]);
        System.Assert(ol.size() > 0, 'Opportunities not created in test');


        StaticResourceCalloutMock mock = new StaticResourceCalloutMock(); 
        mock.setStaticResource('box_sharedlink_response_json');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        
        BoxUtil.getBoxAPISession();
        BoxUploadController.updateUnderwritingfile(ol[0].Id, 'Unknown', '1234567', 'acccesstoken');
        
        Test.stopTest();
    }
    
    public static testMethod void testBoxController()
    {
        
        PageReference p = Page.BoxLogin;
        Test.setCurrentPage(p);
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock(); 
        mock.setStaticResource('oauth_response_json');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        
        BoxController.validateCode('code','http://www.url.com');
        BoxController c = new BoxController();

        Test.stopTest();    
        
        c.getAuthUrl();
        c.getHasToken();
        c.getPageUrl();
        c.redirectOnCallback();
        c.refreshToken();
        
    }
*/
}