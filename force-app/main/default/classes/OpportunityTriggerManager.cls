public class OpportunityTriggerManager extends DefaultTriggerHandler
{

    // Default Constructor
    public OpportunityTriggerManager() {}   

    //  ****************************************************************** 

    public override void beforeInsert(List<SObject> newSoList)
    {
    	//  empty
    }

    //  ****************************************************************** 

    public override void beforeUpdate( map<id,Sobject> oldSoMap,map<id,SObject> newSoMap)
    {
    	//  empty
    }

    //  ****************************************************************** 

    public override void afterInsert(map<id,SObject> newSoMap)
    {
    	OpportunitySharingSupport.afterInsert (newSoMap);
    }

    //  ****************************************************************** 

    public override void afterUpdate(map<id,SObject> oldSoMap, map<id,SObject> newSoMap)
    {
    	OpportunitySharingSupport.afterUpdate (oldSoMap, newSoMap);
    }

    //  ****************************************************************** 

    public override void beforeDelete (map<id,SObject> oldSoMap)
    {
    	//  empty
    }

    //  ******************************************************************      
}