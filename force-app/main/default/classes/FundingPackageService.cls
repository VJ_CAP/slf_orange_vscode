/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkat               03/28/2019              Created
******************************************************************************************/
global class FundingPackageService {
 
  
     global class ResponseWrapper {
          webservice  Id opportunityId;
          webservice  Boolean isRequiredDocMising;
          webservice  String missingFolders;
          webservice  Boolean ignoreRequiredFolder;    
    }

    // webservice method
    webservice static String getFundingPackageData(Id opportunityId,Boolean ignoreRequiredFolder){
        String response='';
        
        ResponseWrapper responseWrp = new ResponseWrapper();
        try{
            Map<string,M1_Automation__c> m1AutomationData = M1_Automation__c.getAll();
            System.debug('#### m1AutomationData==>:'+m1AutomationData); 
            
            M1AutomationWrapper automationWrp = new M1AutomationWrapper(); 
            M1AutomationWrapper.FolderWrapper folderWrapper = new M1AutomationWrapper.FolderWrapper();
            M1AutomationWrapper.DestinationFileWrapper destWrapper = new M1AutomationWrapper.DestinationFileWrapper();
             automationWrp.folders = new List<M1AutomationWrapper.FolderWrapper>();  
            Set<String> tempSet= new Set<String>();
            String boxAccessTokan = '';

            if(opportunityId!=null){
                 if(!Test.isRunningTest())
                {
                    BoxPlatformApiConnection connection;
                    connection = BoxAuthentication.getConnection();
                    boxAccessTokan = connection.accessToken;
                }
                // Get the project,Applicant and installer details 
                List<Opportunity> lstOpp = [SELECT Id,OpportunityID18Char__c,EDW_Originated__c,Long_Term_Facility__c,Account.FirstName,Account.middleName,Account.lastName,Install_Street__c,Installer_Account__r.Name 
                                            FROM Opportunity WHERE Id=:opportunityId LIMIT 1];          
                if(lstOpp.Size()>0){
                    set<String> requiredFolders=new set<String>();
                    // generate customsetting data
                    Map<String,List<M1_Automation__c>> fcilityNFolderMap = new Map<String,List<M1_Automation__c>>();
                    for (M1_Automation__c objCustVal : m1AutomationData.Values()){
                        if(lstOpp[0].Long_Term_Facility__c == objCustVal.Facility__c && lstOpp[0].EDW_Originated__c == objCustVal.EDW_Originated__c){
                            List<M1_Automation__c> tempList = new List<M1_Automation__c> ();
                            tempList = fcilityNFolderMap.containsKey(objCustVal.Folder_Name__c)?fcilityNFolderMap.get(objCustVal.Folder_Name__c):tempList;
                            tempList.add(objCustVal);
                            fcilityNFolderMap.put(objCustVal.Folder_Name__c,tempList);
                            if(objCustVal.isRequired__c){
                                requiredFolders.add(objCustVal.Folder_Name__c);
                            }
                        }  
                    }
                    if(!fcilityNFolderMap.isEmpty()){
                        // check for required folder is exist on box
                        if(!ignoreRequiredFolder)
                            tempSet = validateRequiredFolders(requiredFolders,opportunityId);
                        
                        if(!tempSet.isEmpty()){
                            List<String> tempList = new List<String>();
                            tempList.addAll(tempSet);
                            responseWrp.missingFolders = String.Join(tempList,', ');
                            responseWrp.isRequiredDocMising=true;
                            responseWrp.opportunityId=opportunityId;
                            //lstRespose.add(responseWrp);
                            response=Json.Serialize(responseWrp);
                            return response;
                            
                        }else{
                            responseWrp.missingFolders = '';
                            responseWrp.isRequiredDocMising=false;
                            responseWrp.opportunityId=opportunityId;
                            for (String strKey : fcilityNFolderMap.keySet()){
                                folderWrapper = new M1AutomationWrapper.FolderWrapper();
                                if(fcilityNFolderMap.get(strKey)[0].Data_Room__c!=null)
                                    automationWrp.dataRoom=String.ValueOf(fcilityNFolderMap.get(strKey)[0].Data_Room__c);
                                folderWrapper.folderName =strKey;
                                folderWrapper.isRequired=fcilityNFolderMap.get(strKey)[0].isRequired__c;
                                //folderWrapper.skipMarkers =fcilityNFolderMap.get(strKey)[0].Skip_Markers__c;
                                for(M1_Automation__c  mark1 : fcilityNFolderMap.get(strKey))
                                {
                                    folderWrapper.skipMarkers = mark1.Skip_Markers__c;
                                    //first instance of skipMarkers = false then set that as the value in JSON
                                    if(!mark1.Skip_Markers__c)
                                        break;
                                    
                                }
                                
                                if(fcilityNFolderMap.get(strKey)[0].File_Filter__c!=null)
                                    folderWrapper.fileFilter =fcilityNFolderMap.get(strKey)[0].File_Filter__c;
                                folderWrapper.destinationFiles = new List<M1AutomationWrapper.DestinationFileWrapper>();
                                for(M1_Automation__c objVal:fcilityNFolderMap.get(strKey)){
                                    destWrapper = new M1AutomationWrapper.DestinationFileWrapper();
                                    // replace static values with actual values
                                    if(objVal.Dest_File_Name__c!=null){
                                        String tempString = objVal.Dest_File_Name__c;
                                        tempString=tempString.replace('<OpportunityID>',''+lstOpp[0].OpportunityID18Char__c);
                                        tempString=tempString.replace('<FirstName>',''+lstOpp[0].Account.FirstName);
                                        tempString=tempString.replace('<MiddleName>',(lstOpp[0].Account.middleName!=null)?lstOpp[0].Account.middleName:'');
                                        tempString=tempString.replace('<LastName>',''+lstOpp[0].Account.lastName);
                                        tempString=tempString.replace('<InstallerName>',''+lstOpp[0].Installer_Account__r.Name);
                                        tempString=tempString.replace('<CustomerStreetAddress>',''+lstOpp[0].Install_Street__c);
                                        destWrapper.destFileName= tempString;
                                    }
                                    
                                    if(String.isNotBlank(objVal.Start_Marker__c))
                                        destWrapper.startMarker=objVal.Start_Marker__c;
                                    
                                    if(String.isNotBlank(objVal.End_Marker__c))
                                        destWrapper.endMarker=objVal.End_Marker__c;
                                    
                                    folderWrapper.destinationFiles.add(destWrapper);
                                }
                                if(folderWrapper!=null)
                                automationWrp.folders.add(folderWrapper);
                            }
                        }
                    }
                    DateTime currentDate = system.now();
                    
                    System.debug('#### Date==>:'+currentDate);
                    //Get Box Folder Id
                    List<box__FRUP__c> lstBoxFrup = [SELECT Id,box__Folder_ID__c,box__Lead_Dest_Folder_ID__c,box__Object_Name__c,box__Record_ID__c 
                                                     FROM box__FRUP__c WHERE box__Record_ID__c=:lstOpp[0].Id LIMIT 1];
                    
                    automationWrp.projectID=lstOpp[0].Id;
                    automationWrp.boxToken=SLFEncryption.encryptDataAES128(boxAccessTokan);
                    String sfURL=System.URL.getSalesforceBaseUrl().toExternalForm();
                    if(sfURL.contains('skuid.')){
                    sfURL = sfURL.substringBetween('skuid.', 'visual');
                    sfURL ='https://'+sfURL+'salesforce.com';
                    }
                    automationWrp.updateURL= sfURL + '/';
                    if(lstBoxFrup.size()>0){
                        automationWrp.folderID=lstBoxFrup[0].box__Folder_ID__c;
                    }
                    
                    automationWrp.customerFirstName=lstOpp[0].Account.FirstName;
                    automationWrp.customerLastName=lstOpp[0].Account.lastName;
                    automationWrp.installerName=lstOpp[0].Installer_Account__r.Name;
                    automationWrp.customerStreetAddress=lstOpp[0].Install_Street__c;
                    automationWrp.dayFolder=currentDate.format('EEEE')+' - '+currentDate.Format('MM-dd-yyyy');  
                }   
                if(automationWrp!=null){
                    sendFundingPackageReq(json.serialize(automationWrp));
                }                       
            }
            
        }catch(exception err){
            System.debug('Exception ==>:'+err.getMessage()+'### at Line==>:'+err.getLineNumber());
            ErrorLogUtility.writeLog(' FundingPackageService.getFundingPackageData()',err.getLineNumber(),'getFundingPackageData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
        }
          response=Json.serialize(responseWrp);
          return response;
    }

  
    /********************************************************
*@ Method       : validateRequiredFolders
*@ Description  : 
*@ Parameters   : 
*@ Return Type  : void
**********************************************************/
    
    public static Set<String> validateRequiredFolders( Set<String> lstOfFolderNames,Id oppId){
        
        String fieldnames='';
        Set<String> missingList = new Set<String>();
        Box_Fields__c boxFieldRec;
        Map<String,String> fldrNmeUndrWrtFldMap = new Map<String,String>();
        
        if(!lstOfFolderNames.isEmpty()){
        for (SF_Category_Map__c categs : SF_Category_Map__c.getAll().values()){
            
            if(categs.Folder_Name__c != null){
                fldrNmeUndrWrtFldMap.put(categs.Folder_Name__c.toUpperCase(),categs.TS_Field_On_Underwriting__c);
            }
            
            if(categs.Folder_Id_Field_on_Underwriting__c != null){
                if(!fieldnames.contains(categs.Folder_Id_Field_on_Underwriting__c)){
                    if (fieldnames == '') {
                        fieldnames = categs.Folder_Id_Field_on_Underwriting__c;
                    } else {
                        fieldnames += ',' + categs.Folder_Id_Field_on_Underwriting__c;
                    }
                }
            }
            if(categs.TS_Field_On_Underwriting__c != null){
                if(!fieldnames.contains(categs.TS_Field_On_Underwriting__c)){
                    if (fieldnames == '') {
                        fieldnames = categs.TS_Field_On_Underwriting__c;
                    } else {
                        fieldnames += ',' + categs.TS_Field_On_Underwriting__c;
                    }
                }
            }
            if(categs.Last_Run_TS_Field_on_Underwriting_File__c != null){
                if(!fieldnames.contains(categs.Last_Run_TS_Field_on_Underwriting_File__c)){
                    if (fieldnames == '') {
                        fieldnames = categs.Last_Run_TS_Field_on_Underwriting_File__c;
                    } else {
                        fieldnames += ',' + categs.Last_Run_TS_Field_on_Underwriting_File__c;
                    }
                }
            }
        }
        
        if(String.isNotBlank(fieldnames)){
            String BoxFieldsqueryString = 'SELECT id,Name,Box_Approval_Letters_TS__c,Box_Final_Designs_TS__c,Box_Installation_Photos_TS__c,Box_Utility_Bills_TS__c,Box_Government_IDs_TS__c,Box_Install_Contracts_TS__c,Box_Loan_Agreements_TS__c,Box_Payment_Election_Form_TS__c,'+fieldnames+' from Box_Fields__c where Underwriting__r.Opportunity__c =\'' + oppId+ '\'';
            List<Box_Fields__c> boxFieldLst = database.query(BoxFieldsqueryString);
            if(!boxFieldLst.isEmpty() && !fldrNmeUndrWrtFldMap.isEmpty()){

                for(String doc: lstOfFolderNames){   
                    if(fldrNmeUndrWrtFldMap.containsKey(doc.toUpperCase())){
                        if(boxFieldLst[0].get(fldrNmeUndrWrtFldMap.get(doc.toUpperCase()))== null)
                        {
                            missingList.add(doc);
                        }   
                    }  
                }
            }  
        }
        }
        return missingList;
        
    }
    /********************************************************
*@ Method       : sendFundingPackageReq
*@ Description  : 
*@ Parameters   : 
*@ Return Type  : void
**********************************************************/
    
    public static void sendFundingPackageReq( String reqJson)
    {
        try
        {   
            SFSettings__c settings = SFSettings__c.getOrgDefaults(); 
            if(settings.Funding_Package_End_Point__c!=null){
                
                //send http post call to external system.
                Http ObjHttp = new Http();
                HttpRequest httpReq = new HttpRequest();
                httpReq.setTimeout(120000);
                httpReq.setEndpoint(settings.Funding_Package_End_Point__c);
                httpReq.setHeader('content-type', 'application/json');
                httpReq.setHeader('SFAccessToken', 'Bearer '+UserInfo.getSessionId());
                httpReq.setMethod('POST');
                system.debug('#### reqJson:'+reqJson);
                httpReq.setBody(reqJson);
                HttpResponse httpRes = new HttpResponse();
                Integer stsCode;
                if(!Test.isRunningTest())
                {
                    system.debug('#### httpReq-->:'+httpReq);
                    httpRes = ObjHttp.send(httpReq);
                    stsCode = httpRes.getStatusCode();
                    system.debug('### stsCode-->:'+stsCode);
                    system.debug('### stsCode-->:'+httpRes.getStatus());
                    //system.debug('### response:'+ httpRes.getResponseBody());
                }else{
                    
                    stsCode = 201;
                }
                
            }
            
        }catch(Exception ex)
        {
            system.debug('### FundingPackageService.sendFundingPackageReq():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('FundingPackageService.sendFundingPackageReq()',ex.getLineNumber(),'sendFundingPackageReq()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        } 
    }
}