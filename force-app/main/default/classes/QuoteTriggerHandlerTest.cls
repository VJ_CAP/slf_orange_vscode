/**
* Description: Test class for QuoteTrg trigger and QuoteTriggerHandler class. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         09/26/2017          Created
******************************************************************************************/
@isTest
public class QuoteTriggerHandlerTest {

    /**
    * Description: Test method to cover QuoteTriggerHandler funtionality.
    */
    public testMethod static void QuoteTriggerTest()
    {
    //StipulationTriggerHandler class code coverage        
    
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();
                
        //get the person account record type
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        Case caseTest = new Case();
        caseTest.Status = 'New';
        caseTest.Origin = 'Email';
        insert caseTest;
        
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
         
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        insert trgLst;
        
        //test.startTest();
        
        Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.Push_Endpoint__c ='www.test.com';
        acc.PersonEmail = 'slfaccount@slf.com';
        acc.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
        acc.Installer_Email__c = 'slfaccount@slf.com';
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Push_Endpoint__c = 'www.google.com';
        acc.Subscribe_to_Push_Updates__c = true;
        insert acc;
        
         /*
            
            //Insert SLF Product Record
            //List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '1';
            insert prod;
            //prodList.add(prod);    */
          
            List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            prod.Min_Loan_Amount__c = 500;
            prodList.add(prod);
           
            
            //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
            //Insert opportunity record
            //List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            insert personOpp;
            //oppList.add(personOpp);
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            insert opp;
            //oppList.add(opp);
            
         
            
                        
        Account businessAcc = new Account();
        businessAcc.Name = 'Test Account';
        businessAcc.Push_Endpoint__c ='www.test.com';
        businessAcc.Installer_Legal_Name__c='Bright Solar Planet';
        businessAcc.Solar_Enabled__c = true; 
        insert businessAcc;
                      
        contact con = new contact();
        con.AccountId = businessAcc.Id;
        con.FirstName = 'test';
        con.LastName = 'class';
        insert con;
        
        
                                
        test.startTest();
        
        System_Design__c sysDes = new System_Design__c();
        sysDes.System_Size_Ac__c =5.00;
        sysDes.Battery_Capacity__c =5000.00;
        //sysDes.Name = 'Test Sys';
            sysDes.System_Cost__c = 11;
            sysDes.Opportunity__c = opp.Id;
            sysDes.Est_Annual_Production_kWh__c = 10;
            sysDes.Inverter_Count__c = 11;
            //sysDes.Inverter_Make__c = '11';
            sysDes.Inverter_Model__c = '12';
            //sysDes.Inverter__c = 'TBD';
            sysDes.Module_Count__c = 10;
            sysDes.Module_Model__c = '11';
            //sysDes.System_Size_STC_kW__c = 12;
        insert sysDes;
        
          //Insert Quote Record
        List<Quote> qlist = new List<Quote>();
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Combined_Loan_Amount__c = 70000;
        quoteRec.Opportunityid = opp.id;
        quoteRec.SLF_Product__c = prod.Id ;
        quoteRec.Monthly_Payment__c = 1000;
        quoteRec.System_Design__c = sysDes.id;
        quoteRec.isACH__c = true;
        quoteRec.ACH_Monthly_Payment__c = 100;
        quoteRec.ACH_Monthly_Payment_without_Prepay__c = 100;
        quoteRec.ACH_FinalMonthlyPaymentWOPrepay__c = 100;  
        quoteRec.ACH_Final_Monthly_Payment__c = 100;
        quoteRec.Combined_Loan_Amount__c = 1000;
        qlist.add(quoteRec);
        //quoteRec.Accountid =acc.id;
        //insert quoteRec;
        
        
        Quote quoteRecOne = new Quote();
        quoteRecOne.Name ='Test quote';
        quoteRecOne.Opportunityid = opp.id;
        quoteRecOne.SLF_Product__c = prod.Id ;
        quoteRecOne.Monthly_Payment__c = 1000;
        quoteRecOne.System_Design__c = sysDes.id;
        //quoteRec.Accountid =acc.id;
        quoteRecOne.isACH__c = false;
        quoteRecOne.NonACH_Monthly_Payment__c = 100;
        quoteRecOne.NonACH_Monthly_Payment_WO_Prepay__c = 100;
        quoteRecOne.NonACH_FinalMonthlyPaymentWOPrepay__c = 100;
        quoteRecOne.NonACH_Final_Monthly_Payment__c = 100;
        qlist.add(quoteRecOne);
        insert qlist;
        
        
        //Insert Credit Record
            //List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.id;
            credit.Primary_Applicant__c = acc.id;
            credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = prod.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'Auto Approved';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            credit.Installer_Email__c = 'tes123@gmail.com';
            credit.IsSyncing__c = true;
            credit.Long_Term_Amount_Approved__c = 2000;
            insert credit;
            //creditList.add(credit);
            //insert creditList;
            
        List<SystemDesign_to_Opportunity_Sync__c> syoppList = new List<SystemDesign_to_Opportunity_Sync__c>();
        SystemDesign_to_Opportunity_Sync__c syopp = new SystemDesign_to_Opportunity_Sync__c();
        syopp.Name = 'Inverter';
        syopp.From_System_Design__c = 'Inverter_Manufacturer__c';
        syopp.To_Opportunity__c ='Inverter__c';   
        syoppList.add(syopp);   
        
        SystemDesign_to_Opportunity_Sync__c syopp1 = new SystemDesign_to_Opportunity_Sync__c();
        syopp1.Name = 'Battery';
        syopp1.From_System_Design__c = 'Battery_Manufacturer__c';
        syopp1.To_Opportunity__c ='Battery__c';   
        syoppList.add(syopp1);
        
        SystemDesign_to_Opportunity_Sync__c syopp2 = new SystemDesign_to_Opportunity_Sync__c();
        syopp2.Name = 'Module';
        syopp2.From_System_Design__c = 'Module_Manufacturer__c';
        syopp2.To_Opportunity__c ='Module__c';   
        syoppList.add(syopp2);
          
        insert syoppList;
        
        //opp.SyncedQuoteId = qlist[0].Id;
         opp.Synced_Quote_Id__c =qlist[0].Id;
        update opp;
       
        QuoteTriggerHandler.runonce();
        QuoteTriggerHandler.isTrgExecuting = false;
       // QuoteTriggerHandler.runonce(); 
    
    test.stoptest();
    
    }

    
}