public class SendConfirmationEmailTriggerHandler {
  
    public static void HandleAfterInsertUpdate(List<Underwriting_File__c> uwfList)
    {
        Set<Id> uwfIds = new Set<Id>();

        System.debug(uwfList);

        for (Underwriting_File__c uwf : uwfList)
        {
            if(uwf.Send_Confirmation_Email__c)
            {
                uwfIds.add(uwf.Id);
            }
        }

        if(uwfIds.size() > 0)
        {
            List<EmailTemplate> emailTemplates = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName IN (
                                                    'CRB_CRB_v2', 'CRB_Short_term_Only_v2', 'CRB_Single_Loan_v2', 'CRB_TCU_v2', 
                                                    'TCU_CRB_v2', 'TCU_Short_term_Only_v2', 'TCU_single_loan_v2', 'TCU_TCU_v2')];
            Map<String, Id> emailTemplateByDevNameMap = new Map<String, Id>();
            for(EmailTemplate et : emailTemplates){
                emailTemplateByDevNameMap.put(et.DeveloperName, et.Id);
            }

            List<Underwriting_File__c> uwfNeedingCheck = [SELECT Id, Opportunity__c, 
                                                                Opportunity__r.SLF_Product__r.Short_Term_Facility__c, 
                                                                Opportunity__r.SLF_Product__r.Long_Term_Facility__c, 
                                                                Opportunity__r.SLF_Product__r.Loan_Type__c
                                                           FROM Underwriting_File__c WHERE Id IN :uwfIds];
            Map<Id, Id> oppIdToTemplateIdMap = new Map<Id, Id>();
            for(Underwriting_File__c uwf : uwfNeedingCheck){
                if(uwf.Opportunity__r.SLF_Product__r.Short_Term_Facility__c == 'CRB' && 
                   uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Same As Cash'){
                    // CBR Short-Term
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('CRB_Short_term_Only_v2'));
                } else if(uwf.Opportunity__r.SLF_Product__r.Long_Term_Facility__c == 'CRB' && 
                          uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Single'){
                    // CRB Single
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('CRB_Single_Loan_v2'));
                } else if(uwf.Opportunity__r.SLF_Product__r.Long_Term_Facility__c == 'CRB' && 
                          uwf.Opportunity__r.SLF_Product__r.Short_Term_Facility__c == 'CRB' && 
                          uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Combo'){
                    // CRB/CRB
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('CRB_CRB_v2'));
                } else if (uwf.Opportunity__r.SLF_Product__r.Long_Term_Facility__c == 'TCU' &&
                           uwf.Opportunity__r.SLF_Product__r.Short_Term_Facility__c == 'TCU' &&
                           uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Combo'){
                    // TCU/TCU
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('TCU_TCU_v2'));
                } else if(uwf.Opportunity__r.SLF_Product__r.Long_Term_Facility__c == 'TCU' &&
                          uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Single'){
                    // TCU Single
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('TCU_single_loan_v2'));
                } else if(uwf.Opportunity__r.SLF_Product__r.Short_Term_Facility__c == 'CRB' &&
                          uwf.Opportunity__r.SLF_Product__r.Long_Term_Facility__c == 'TCU' &&
                          uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Combo'){
                    // CRB/TCU
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('CRB_TCU_v2'));
                } else if(uwf.Opportunity__r.SLF_Product__r.Short_Term_Facility__c == 'TCU' && 
                          uwf.Opportunity__r.SLF_Product__r.Long_Term_Facility__c == 'CRB' && 
                          uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Combo'){
                    // TCU/CRB
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('TCU_CRB_v2'));
                } else if(uwf.Opportunity__r.SLF_Product__r.Short_Term_Facility__c == 'TCU' && 
                          uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == 'Same as Cash'){
                    // TCU Short-Term
                    oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get('TCU_Short_term_Only_v2'));
                }
            }

            if(oppIdToTemplateIdMap.size() > 0){
                List<Attachment> attachList = new List<Attachment>();
                for(Id oppId : oppIdToTemplateIdMap.keySet()){
                    //SendOpportunityAndEmailTemplateToConga.SendConfirmationEmailToConga(UserInfo.getOrganizationId().left(15), UserInfo.getSessionId(), oppId, oppIdToTemplateIdMap.get(oppId));
                    Contact dummyContact = [SELECT Id, Email FROM Contact WHERE Email <> null LIMIT 1];

                    Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                    msg.setTemplateId(oppIdToTemplateIdMap.get(oppId));
                    msg.setTargetObjectId(dummyContact.Id);
                    msg.setSaveAsActivity(false);
                    msg.setWhatId(oppId);
                    msg.setToAddresses(new List<String> { 'noreply@email.com' });

                    Savepoint sp = Database.setSavepoint();
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                    Database.rollback(sp);

                    //string body = msg.getPlainTextBody().replace('\n', '\r\n');
                    string body = msg.getHTMLBody();
                    body = '<html>' + body + '</html>';
                    //body = body.substringBefore('<style') + body.substringAfter('</style>');
                    //body = body.substringBefore('<img') + body.substringAfter('</img>');
                    system.debug('**SZ: line breaks = ' + body.indexOf('\r\n'));
                    system.debug('**SZ: body = ' + body);

                    attachList.add(new Attachment(
                        Name = 'Confirmation_Email_' + (Datetime.now().format('yyyyMMdd_hhmmss')) + '.html',
                        ContentType = 'text/html',
                        Body = Blob.valueOf(body),
                        ParentId = oppId
                    ));
                }

                if(attachList.size() > 0){
                    insert attachList;
                }

                Set<Id> attachmentIds = new Set<Id>();
                for(Attachment att : attachList){
                    attachmentIds.add(att.Id);
                }
                SendAttachmentsToBox (attachmentIds);
            }
        }
    }

    @future(callout=true)
    public static void SendAttachmentsToBox (Set<Id> attachmentIds)
    {
        List<Attachment> attachmentList = [SELECT Id, Name, ParentId, Body FROM Attachment WHERE Id IN :attachmentIds];

        set<Id> oppIds = new set<Id>{};

        for (Attachment a : attachmentList) oppIds.add (a.ParentId);

        map<Id,String> oppToFolderIdsMap = new map<Id,String>{};

        //  map the opp Id to the box folder id 

        for (box__FRUP__c f : [SELECT Id, box__Record_ID__c, box__Folder_ID__c
                                FROM box__FRUP__c
                                WHERE box__Object_Name__c = 'Opportunity'
                                AND box__Record_ID__c IN :oppIds])
        {
            oppToFolderIdsMap.put (f.box__Record_ID__c, f.box__Folder_ID__c); 
        }

        if (Test.isRunningTest()) return;

        BoxPlatformApiConnection api = BoxAuthentication.getConnection();

        for (Attachment att : attachmentList)
        {
            //  get the folder Id for the Opp owning the attachment

            String oppFolderId = oppToFolderIdsMap.get (att.ParentId);

            if (oppFolderId == null) continue;  //  no box folder for opp - error condition

            BoxFolder oppFolder = new BoxFolder(api, oppFolderId);

            //  look for the Email folder inder the Opp folder

            String emailFolderId = null;
            for (BoxItem.Info i : oppFolder.getChildren())
            {
                if (i.Name == 'Emails')
                {
                    emailFolderId = i.Id;
                    break;
                }
            }

            //  if no email folder then create it

            if (emailfolderId == null)
            {
                BoxItem.Info i = oppFolder.createFolder('Emails');
                emailFolderId = i.Id;
            }

            //  connect to the email folder

            BoxFolder emailFolder = new BoxFolder(api, emailfolderId);

            Document d = new Document();

            d.Name = att.Name;
            d.Body = att.Body;

            //  upload the document to the email folder

            BoxFile.Info fileInfo = emailFolder.uploadFile(d, d.Name);
        }
    }

}