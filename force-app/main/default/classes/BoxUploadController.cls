public class BoxUploadController 
{
        @TestVisible private String access_token;
        @TestVisible private String refresh_token;
        @TestVisible private Boolean isCallback;

        BoxApiConnection apiConnection;
                
        // private final static String oppRootFolderID = '7788344505'; // 8842774230';   // this is the root folder 
                
        public Id oppId {get; set;}
        public String oppName {get; set;}
        public User usr {get; set;}

        public String diags {get;set;}

        public String tree {get;set;}

        public String accessToken {get;set;}

        public BoxPlatformApiConnection platformApiConnection {get;set;}

        DisplayFile df1 = new Displayfile();
        DisplayFile df2 = new Displayfile('a','b','c');
        DisplayFile df3 = new Displayfile();
        DisplayFile df4 = new Displayfile('a','b','c');
        DisplayFile df5 = new Displayfile();
        DisplayFile df6 = new Displayfile('a','b','c');
        DisplayFile df7 = new Displayfile('a','b','c');
        DisplayFile df8 = new Displayfile('a','b','c');
        DisplayFile df9 = new Displayfile('a','b','c');

        DisplayFolder dfd1 = new DisplayFolder();
        DisplayFolder dfd2 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
        DisplayFolder dfd3 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
        DisplayFolder dfd4 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
        DisplayFolder dfd5 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
        DisplayFolder dfd6 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
        DisplayFolder dfd7 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
        DisplayFolder dfd8 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
        DisplayFolder dfd9 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    
   
//  *********************************************************************
        
    public class DisplayFolder
    {
        public String folderName {get;set;}
        public String folderStatus {get;set;}
        public list<DisplayFile> files {get;set;}

        public DisplayFolder () {}

        public DisplayFolder (String folderName, String folderStatus, list<DisplayFile> files)
        {
            this.folderName = folderName;
            this.folderStatus = folderStatus;
            this.files = files;
        }
    }

//  *********************************************************************

    public class DisplayFile
    {
        public String fileName {get;set;}
        public String createdAt {get;set;}
        public String createdBy {get;set;}

        public DisplayFile () {}

        public DisplayFile (String fileName, String createdAt, String createdBy)
        {
            this.fileName = fileName;
            this.createdAt = createdAt;
            this.createdBy = createdBy;
        }
    }
   
//  *********************************************************************
       
        /**
        * Generic constructor
        */
        public BoxUploadController() {
            
            oppId = ApexPages.currentPage().getParameters().get('id');

            if (oppId == null) throw new CustException('Please pass in an Opportunity Id.');

            Opportunity opp;

            try
            {
                opp = [SELECT Id, Name FROM Opportunity WHERE Id = :oppId LIMIT 1];
            }
            catch (Exception e)
            {
                throw new CustException('Not found Opportunity Id ' + oppId + 
                                        ' Exception: ' + e.getMessage() + 
                                        'User Id = ' + userinfo.getuserId());
            }

            oppName = opp.Name;
            
            usr=[Select Id,Name,Email from User where Id=:userinfo.getuserId()];

            this.platformApiConnection = BoxAuthentication.getConnection();

            if (this.platformApiConnection != null)
            {
                this.accessToken = this.platformApiConnection.accessToken;
            }

            this.diags = '';
            this.tree = '';
        }

//  *********************************************************************

   @RemoteAction
    public static String displayTree (String accessToken, String oppId)
    {
        String folderId;

        BoxApiConnection apiConnection = new BoxApiConnection (accessToken);

        //  get folder id for Opportunity, from FRUP table

        try
        {

            folderId = [SELECT Id, box__Folder_ID__c 
                            FROM box__FRUP__c 
                            WHERE box__Object_Name__c = 'Opportunity' 
                            AND box__Record_ID__c LIKE :oppId LIMIT 1].box__Folder_ID__c  ;    
        }
        catch (Exception e)
        {
            return 'Opportunity folder not found ' + e.getMessage();
        }

        //  get Opportunity and associated UF(s)

        Opportunity opp = [SELECT Id, Name,
                        (SELECT Id,
                            LT_Loan_Agreement_Review_Complete__c,
                            ST_Loan_Agreement_Review_Complete__c,
                            Final_Design_Review_Complete__c,
                            Confirm_ACH_Information__c,
                            Installation_Photos_Review_Complete__c,
                            Confirmation_Call_Initiated__c,
                            Welcome_Call_Initiated__c,
                            PTO_Review_Complete__c,
                            Install_Contract_Review_Complete__c,
                            Primary_Borrower_ID_Type__c,
                            Utility_Bill_Review_Complete__c
                            FROM Underwriting_Files__r ORDER By CreatedDate DESC LIMIT 1)
                        FROM Opportunity WHERE Id = :oppId LIMIT 1];

        Underwriting_File__c uf = new Underwriting_File__c ();

        //  save the newest UF for the Opportunity

        if (opp.Underwriting_Files__r.size() > 0)
        {
            uf = opp.Underwriting_Files__r[0];
        }

        //  get the Opportuntiy folder from Box

        BoxFolder oppFolder = new BoxFolder(apiConnection, folderId);

        list<DisplayFolder> displayFolders = new list<DisplayFolder>{};

        Integer docsLimit = 40;
        Integer docsCount = 0;
        Integer folderCount = 0;
        Boolean limitReached = false;

        //  build the tree

        try
        {

            for (BoxItem.Info i : oppFolder.getChildren())
            {
                folderCount++;
                DisplayFolder dfd = new DisplayFolder ();
                dfd.folderName = i.Name;
                dfd.files = new list<DisplayFile>{};
                displayFolders.add (dfd);

                BoxFolder f = new BoxFolder(apiConnection, i.Id);

                for (BoxItem.Info j : f.getChildren())
                {
                    if (docsCount++ < docsLimit)
                    {
                        DisplayFile df = new DisplayFile ();
                        df.fileName = j.name;
                        BoxFile file = new BoxFile(apiConnection, j.Id); 
                        BoxFile.Info info = file.getFileInfo();
                        String meta = file.getFileMetadata('Uploaded By');
                        df.createdAt = info.createdAt.format();
                        df.createdBy = meta;
                        dfd.files.add (df);
                    }
                    else
                    {
                        limitReached = true;
                    }
                }
            }
        }
        catch (Exception e) {}

        //  build the folder status map from the UF

        map<String, String> folderStatusMap = new map<String, String>{};

        //  Loan Agreements, Final Designs, Payment Election Form, Installation Photos, Unknown, 
        //  Communications, Approval Letters, Install Contracts, Government IDs, Utility Bills

        String folderStatus;

        String inReview = '(In Review)';
        String reviewComplete = '(Review Complete)';

        folderStatusMap.put ('Unknown', '');
        
        if (uf != null)
        {
            folderStatus = (uf.LT_Loan_Agreement_Review_Complete__c || uf.ST_Loan_Agreement_Review_Complete__c) ? reviewComplete : inReview;
            folderStatusMap.put ('Loan Agreements', folderStatus);

            folderStatus = uf.Final_Design_Review_Complete__c ? reviewComplete : inReview;
            folderStatusMap.put ('Final Designs', folderStatus);

            folderStatus = uf.Confirm_ACH_Information__c != null ? reviewComplete : inReview;
            folderStatusMap.put ('Payment Election Form', folderStatus);

            folderStatus = uf.Installation_Photos_Review_Complete__c ? reviewComplete : inReview;
            folderStatusMap.put ('Installation Photos', folderStatus);

            folderStatus = (uf.Confirmation_Call_Initiated__c != null || uf.Welcome_Call_Initiated__c != null) ? reviewComplete : inReview;
            folderStatusMap.put ('Communications', folderStatus);

            folderStatus = uf.PTO_Review_Complete__c ? reviewComplete : inReview;
            folderStatusMap.put ('Approval Letters', folderStatus);

            folderStatus = uf.Install_Contract_Review_Complete__c ? reviewComplete : inReview;
            folderStatusMap.put ('Install Contracts', folderStatus);

            folderStatus = uf.Primary_Borrower_ID_Type__c != null ? reviewComplete : inReview;
            folderStatusMap.put ('Government IDs', folderStatus);

            folderStatus = uf.Utility_Bill_Review_Complete__c ? reviewComplete : inReview;
            folderStatusMap.put ('Utility Bills', folderStatus);
        }

        //  build the output HTML from the tree

        String outputHtml = '<ul>';

        if (Test.isRunningTest())
        { 
            DisplayFile df1 = new Displayfile('a','b','c');
            DisplayFile df2 = new Displayfile('a','b','c');

            DisplayFolder dfd1 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2}); 
            DisplayFolder dfd2 = new DisplayFolder('d','e',new list<DisplayFile>{df1,df2});    

            displayFolders = new list<DisplayFolder>{dfd1,dfd2};   
        }

        for (DisplayFolder x : displayFolders)
        {
            String displayFolderStatus = folderStatusMap.get(x.folderName);

            if (displayFolderStatus ==  null) displayFolderStatus = '';

            outputHtml += '<li><b>' + x.folderName + '</b> ' + displayFolderStatus + ' ' + '</li><ul>';

            for (DisplayFile y : x.files)
            {
                outputHtml += '<li><span style=color:blue;>' + y.fileName + '</span> created at ' + y.createdAt + ' by ' + y.createdBy + '</li>';
            }
            outputHtml += '</ul>';
        }
        outputHtml += '</ul>';

        if (limitReached) outputHtml+= '<br/>Note: only the first ' + docsLimit + ' documents have been displayed';

        if (folderCount == 0) outputHtml+= '<br/>No documents found';

        return outputHtml;
    }

//  *********************************************************************
/*        
        @RemoteAction
        public static String getBoxToken()
        {
            return BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
        }
        
        @future
        public static void updateStoredBoxToken(String accessToken, String refreshToken, Long expiry)
        {
            BoxCCredentials__c creds = BoxCCredentials__c.getValues(SETTING_NAME);
            creds.Access_Token__c = accessToken;
            creds.Refresh_Token__c = refreshToken;
            creds.Expires_In__c = expiry;
            creds.Expire_Date__c = System.Now().addSeconds((Integer)expiry);
            update creds;
        }

        @RemoteAction
        public static String getRefreshedBoxToken()
        {
            return null;

            //throw new CustException('exception here');
            String client_id = BoxCCredentials__c.getValues(SETTING_NAME).Client_Id__c;
            String client_secret = BoxCCredentials__c.getValues(SETTING_NAME).Client_Secret__c;
            String access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
            String refresh_token = BoxCCredentials__c.getValues(SETTING_NAME).Refresh_Token__c;
              
            try {
            
            if (!Test.isRunningTest())
            {
                BoxApiConnection api = new BoxApiConnection(client_id, client_secret, access_token, refresh_token);
                api.refresh();
            
                
                BoxCCredentials__c creds = BoxCCredentials__c.getValues(SETTING_NAME);
                creds.Access_Token__c = api.getAccessToken();
                creds.Refresh_Token__c = api.getRefreshToken();
                creds.Expires_In__c = api.getExpires();
                creds.Expire_Date__c = System.Now().addSeconds((Integer)api.getExpires());
                update creds;
            }
            
    
            } catch (Exception e)
            {
                throw new CustException('Unable to authenticate with Box. Please contact Sunlight Support');   
            }
            
            return BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
        }
        
        
        @RemoteAction
        public static BoxApiConnection getRefreshedAPIConnection()
        {
            //throw new CustException('exception here');
            String client_id = BoxCCredentials__c.getValues(SETTING_NAME).Client_Id__c;
            String client_secret = BoxCCredentials__c.getValues(SETTING_NAME).Client_Secret__c;
            String access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
            String refresh_token = BoxCCredentials__c.getValues(SETTING_NAME).Refresh_Token__c;
            
            BoxApiConnection api = null;
            try {
            
                if (!Test.isRunningTest()) {
                    api = new BoxApiConnection(client_id, client_secret, access_token, refresh_token);
                    api.refresh();
    
                    updateStoredBoxToken(api.getAccessToken(), api.getRefreshToken(), api.getExpires());
                }
            } catch (Exception e)
            {
                throw new CustException('Unable to authenticate with Box. Please contact Sunlight Support');   
            }
            
            //return BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
            //return api.getAccessToken();
            return api;
        }
 */       
        @RemoteAction
        public static void ensureFRUPEntryExists(String oppFolderId, Id oppId)
        {
    
            //** SS Uncomment this if you want to create frup entries after creating an opportunity folder
            sightenUtil.createFRUPEntries(oppFolderId, oppId);
        }
        
        //** SS MAIN This is the main method which is called from UploadFileToBox to create a folder
        //           This returns a string in the form <oppFolderId>:<folderFolderId>
        
        @RemoteAction
        public static String getChildFolderId(Id oppId, String oppName, String folderName, String accessToken)
        {
            // *** check if a folder exists for the opportunity
            String oppFolderId;
           
            // *** queries the FRUP table for the folder ID
            oppFolderId = BoxUploadUtil.getBoxFolderId(oppId);
            if (oppFolderId == null)
            {
                // *** create the opportunity folder
                // SS MAIN - replace getChildFolderIdToolkit with getChildFolderId in 2 calls below
                // ***       if we can't use Toolkit
                String oppRootFolderID = SFSettings__c.getOrgDefaults().Root_Folder_ID__c;
                oppFolderId = BoxUploadUtil.getChildFolderId(oppId, oppRootFolderID, oppName, accessToken);
                
                //BoxUploadUtil.setBoxFolderID(oppId, oppFolderId);
                
                // *** we probably need to enter the record into FRUP
            }
            
            System.Assert(oppFolderId != null, 'Opportunity Folder should not be null here');
            //oppFolderId = '7788344505';
            return oppFolderId + ':' + BoxUploadUtil.getChildFolderId(oppId, oppFolderId,folderName, accessToken);
        }
        
        @RemoteAction
        public static String getOppFolderID(Id oppId)
        {
            return BoxUploadUtil.getOppFolderID(oppId);
            
        }
        
        @RemoteAction
        public static String getSubFolderId(String parentBoxFolderID, String folderName)
        {
            
            return BoxUploadUtil.getSubFolderID(parentBoxFolderId, folderName);
        }
            
        public class createFolderResult
        {
            public String token {get; set;}
            public String folderId {get; set;}
        }
        
           /**
         * This routine updates the shared link and TS fields on
         * underwriting file object on an Opportunity
         **/
        @RemoteAction
        public static void updateUnderwritingFile(Id oppId,  String folderName, String folderId, String token)
        {
            String sharedLink = 'test';

            if (Test.isRunningTest() == false) sharedLink = BoxUploadUtil.getFolderSharedLinkPortal(folderId, token);

            List<Underwriting_File__c> fl = new list<Underwriting_File__c>{};

            if (Test.isRunningTest() == false) 
                fl = BoxUploadUtil.queryCategoryMappingFieldsFromUnderwriting(oppId);

            Underwriting_File__c ufile;
            Box_Fields__c BoxFieldRec;// Added as part of 1930 by Adithya
            if (fl.size() == 0)
            {
                ufile = new Underwriting_File__c();
                ufile.Opportunity__c = oppId;
                insert ufile; // Added as part of 1930 by Adithya
                
                // Added as part of 1930 by Adithya
                BoxFieldRec = new Box_Fields__c();
                BoxFieldRec.Underwriting__c = ufile.Id;
            }else {
                BoxFieldRec = fl[0].Box_Fields__r[0];// Added as part of 1930 by Adithya
            }
            
            Map<string,SF_Category_Map__c> categoryFolderMappings = SF_Category_Map__c.getAll();
            String ufileIDFieldName = null;
            String ufileTSFieldName = null;
            for (String category : categoryFolderMappings.keySet())
            {
                String childFolderName = categoryFolderMappings.get(category).Folder_Name__c;
                if (childFolderName.equals(folderName)) 
                {
                    ufileIDFieldName = categoryFolderMappings.get(category).Folder_Id_Field_on_Underwriting__c ;
                    ufileTSFieldName = categoryFolderMappings.get(category).TS_Field_On_Underwriting__c;  
                    break;
                }
            }
    
            if (ufileIDFieldName == null && Test.isRunningTest() == false)
                throw new CustException('Unable to find underwriting folder id field for folder : ' + folderName + '. Please contact Sunlight Support.');
    
            if (ufileTSFieldName == null && Test.isRunningTest() == false)
                throw new CustException('Unable to find timestamp field for folder : ' + folderName + '. Please contact Sunlight Support.');
            
            if (Test.isRunningTest() == false) 
            {
                // Added as part of 1930 by Adithya
                BoxFieldRec.put(ufileIDFieldName,sharedLink);
                BoxFieldRec.put(ufileTSFieldName,System.Now());
                upsert BoxFieldRec;
            }
        }
        
       /* @RemoteAction
        public static createFolderResult refreshTokenAndCreateFolder(Id oppId, String oppName, String folderName)
        {
            createFolderResult res = new createFolderResult();
            res.token = getRefreshedBoxToken();
            res.folderId = getChildFolderId(oppId, oppName, folderName);
            return res;
        }*/
        
        
        public String uploadFolder { get; set;}
        
        
        public List<SelectOption> getUploadFolderList()
        {
            List<String> fList = BoxUploadUtil.getCategoryFolders();
            List<SelectOption> folderList = new List<SelectOption>();
            folderList.add(new SelectOption('--- No Document Type selected ---','--- No Document Type selected ---'));
            for (String f : fList)
            {
                folderList.add(new SelectOption(f,f));
            }
            return folderList;
        }
        
        /** The JSON result from a successful oauth call */
        public class OAuthResult {
            /** The access token */
            public String access_token {get; set;}
            
            /** The refresh token */
            public String refresh_token {get; set;}
            
            public Long expires_in {get; set;}
        }
        
 /*   
        /**
        * Gets the authroization URL
        *
        * @return The authorization url
        */
/*        public String getAuthUrl() {
            Map<String, String> urlParams = new Map<String, String> {
                'client_id' => BoxCCredentials__c.getValues(SETTING_NAME).Client_Id__c,
                'redirect_uri' => getPageUrl(),
                'response_type' => 'code'
            };
    
            PageReference ref = new PageReference(AUTHORIZE_URL);
            ref.getParameters().putAll(urlParams);
    
            return ref.getUrl();
        }
 */       
        /**
        * Gets the page url
        * 
        * @return The page url
        */
        @testVisible
        public String getPageUrl() {
            String host = ApexPages.currentPage().getHeaders().get('Host');
            String path = ApexPages.currentPage().getUrl().split('\\?').get(0);
            
            return 'https://' + host + path;
        }
        
        /**
        * If the access token is set
        * 
        * @return If the access token is set
        */
        public Boolean getHasToken() {
            return (this.access_token != null);
        }
        
    /*    
        
        public PageReference refreshToken()
        {
            String client_id = BoxCCredentials__c.getValues(SETTING_NAME).Client_Id__c;
            String client_secret = BoxCCredentials__c.getValues(SETTING_NAME).Client_Secret__c;
            String access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
            String refresh_token = BoxCCredentials__c.getValues(SETTING_NAME).Refresh_Token__c;
            
            try {
            BoxApiConnection api = new BoxApiConnection(client_id, client_secret, access_token, refresh_token);
            api.refresh();
            BoxCCredentials__c creds = BoxCCredentials__c.getValues(SETTING_NAME);
            creds.Access_Token__c = api.getAccessToken();
            creds.Refresh_Token__c = api.getRefreshToken();
            creds.Expires_In__c = api.getExpires();
            creds.Expire_Date__c = System.Now().addSeconds((Integer)api.getExpires());
            update creds;
            } catch (Exception e)
            {
                BoxCCredentials__c creds = BoxCCredentials__c.getValues(SETTING_NAME);
                creds.Access_Token__c = null;
                creds.Refresh_Token__c = null;
                update creds;
                PageReference pg =  new PageReference('/apex/BoxLogin');
                pg.setRedirect(true);
                return pg;
            }
            
            return null;
        }
    */    
/*        
        
        
        
        
        
        public PageReference createFolder()
        {
            //String access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
            
            //BoxApiConnection api = new BoxApiConnection(access_token);
            BoxUploadUtil.getChildFolderId('7788344505','f1');
            //BoxFolder bf = new BoxFolder(api,'f1'); 
            //Boxfolder.Info f =  bf.createFolder(folderName);
            return null;
        }
        
*/    
        
    
        /*** 
         * try to refresh, if not, clear sttings and redirect back to login
         ***/
        
    
        
        
        private class ParentFolder {
            public String id;
            
            public ParentFolder(String id) {
                this.id = id;
            }
        }
        
        private class Folder {
            public String name;
            ParentFolder parent;
            
            public Folder(String name, String parentId) {
                this.name = name;
                this.parent = new ParentFolder(parentId);
            }
        }

        ParentFolder pf2 = new ParentFolder ('x');
        Folder f1 = new Folder ('x','x');
        
        /**
        * Static method to create the folder inside of the box account
        * 
        * @param accountId The account id to create the folder for
        */
/*        @Future(callout = true)
        public static void createFolder(Id accountId) {
            if (BoxCCredentials__c.getValues(SETTING_NAME) == null) {
                return;
            }
    
            String access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
            Folder folder_info = new Folder(accountId, '0');
            
            HttpRequest request=new HttpRequest();
            request.setEndpoint(FOLDER_URL); 
            request.setMethod('POST');
            request.setHeader('Authorization', 'Bearer ' + access_token);
            String body = JSON.serialize(folder_info);
            System.debug(body);
            request.setBody(body);
            
            if (!Test.isRunningTest()) {
                Http p = new Http();
                HttpResponse response = p.send(request);
            }
        }

        public PageReference refresh ()
        {

            String client_id = BoxCCredentials__c.getValues(SETTING_NAME).Client_Id__c;
            String client_secret = BoxCCredentials__c.getValues(SETTING_NAME).Client_Secret__c;
            String access_token = BoxCCredentials__c.getValues(SETTING_NAME).Access_Token__c;
            String refresh_token = BoxCCredentials__c.getValues(SETTING_NAME).Refresh_Token__c;


            if (this.apiConnection != null)
            {
              updateStoredBoxToken (apiConnection.getAccessToken(), apiConnection.getRefreshToken(), apiConnection.getExpires());
            } 
            return null;        
        }
*/    
}