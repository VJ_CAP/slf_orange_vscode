/**
* Description: Test class for UnderwritingFileTrg trigger and StipulationTriggerHandler class. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/27/2017          Created
******************************************************************************************/
@isTest
public class StipulationTriggerHandlerTest {
     @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
        
    }
    /**
    * Description: Test method to cover StipulationTriggerHandler funtionality.
    */
    private testMethod static void stipulationTriggerTest(){
    List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
    
     test.startTest();
        
        //insert custom setting TriggerFlag records.
        //start
        SLFUtilityTest.createCustomSettings();
        //stop
        
        Case caseTest = new Case();
        caseTest.Status = 'New';
        caseTest.Origin = 'Email';
        insert caseTest;
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Push_Endpoint__c ='www.test.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Solar_Enabled__c = true; 
        acc.Push_Endpoint__c = 'www.google.com';
        acc.Subscribe_to_Push_Updates__c = True;   
        insert acc;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;
        
        TriggerFlags__c trgflag = new TriggerFlags__c();
        trgflag.Name ='Opportunity';
        trgflag.isActive__c =true;
        insert trgflag;
        
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.EDW_Originated__c = True;
        insert opp;
        
        Underwriting_File__c uwFile = new Underwriting_File__c();
        uwFile.name = 'Underwriting File';
        uwFile.ACT_Review__c ='Yes';
        uwFile.Opportunity__c = opp.Id;
        uwFile.Stage_Status__c='Open';
        insert uwFile;
        
        uwFile.Project_Status__c  ='M1';
        uwFile.CO_Ready_for_Review_Date_Time__c =System.Today();
        update uwFile;
        
         list<Task> tsklst = new list<Task>();
        Task ts = new Task();
        ts.Whoid = con.id;
        ts.whatId = uwFile.id;
        ts.Subject = 'Call';
        ts.Priority = 'Normal';
        ts.Status = 'Open';
        ts.Type = 'M2 Review';
        //ts.Owner = loggedInUsr[0].id;
        tsklst.add(ts);
        Task ts1 = new Task();
        ts1.Whoid = con.id;
        ts1.whatId = uwFile.id;
        ts1.Subject = 'Call';
        ts1.Priority = 'Normal';
        ts1.Status = 'Open';
        ts1.Type = 'M0 Review';
        tsklst.add(ts1);
        Task ts2 = new Task();
        ts2.Whoid = con.id;
        ts2.whatId = uwFile.id;
        ts2.Subject = 'Call';
        ts2.Priority = 'Normal';
        ts2.Status = 'Open';
        ts2.Type = 'M1 Review';
        tsklst.add(ts2);
        Task ts3 = new Task();
        ts3.Whoid = con.id;
        ts3.whatId = uwFile.id;
        ts3.Subject = 'Call';
        ts3.Priority = 'Normal';
        ts3.Status = 'Open';
        ts3.Type = 'Permit Review';
        tsklst.add(ts3);
        Task ts4 = new Task();
        ts4.Whoid = con.id;
        ts4.whatId = uwFile.id;
        ts4.Subject = 'Call';
        ts4.Priority = 'Normal';
        ts4.Status = 'Open';
        ts4.Type = 'Kitting Review';
        tsklst.add(ts4);
        insert tsklst; 
        
        
        Stipulation_Data__c sData  = new Stipulation_Data__c();
        sData.Name = 'SD Test';
        sData.Description__c = 'Test';
        sData.Installer_Only_Email__c = true;
        sData.Review_Classification__c = 'SLS II';
        insert sData;        
        
        
        List<Stipulation__c> stipInsertLst = new List<Stipulation__c>();
        for(Integer i=0; i<5; i++) 
        {
            Stipulation__c stipTest  = new Stipulation__c();
            stipTest.Status__c = 'New';
            stipTest.Completed_Date__c = system.today();
            stipTest.Stipulation_Data__c  = sData.id;
            stipTest.Underwriting__c =uwFile.id;
            stipTest.Case__c = caseTest.id;
            stipTest.ETC_Notes__c = 'test';
            stipInsertLst.add(stipTest);
        }
        insert stipInsertLst;
        
        List<Stipulation__c> stipUpdateLst = new List<Stipulation__c>();
        for(Integer i=0; i<5; i++)
        {
            Stipulation__c stipTest = stipInsertLst[i];
            stipTest.Status__c = 'in progress';
            stipTest.Completed_Date__c = system.today();
            stipTest.ETC_Notes__c = '123Test';
            if(i == 2)
            {
                stipTest.Completed_Date__c = null;
                stipTest.ETC_Notes__c = '';
                stipTest.Status__c = '';
            }  
            if(i == 2)
            {
                stipTest.Completed_Date__c = system.today();
                stipTest.ETC_Notes__c = 'wrwr122';
                stipTest.Status__c = 'Completed';
            }  
             if(i == 3)
            {
                stipTest.Completed_Date__c = system.today();
                stipTest.ETC_Notes__c = '';
                stipTest.Status__c = 'Under Review';
            }
            if(i == 4)
            {
                stipTest.Completed_Date__c = system.today();
                stipTest.ETC_Notes__c = '';
                stipTest.Ready_for_Review_Date_Time__c = null;
                stipTest.Status__c = 'Documents Received';
                
            }
            stipUpdateLst.add(stipTest);
        }
        sData.Description__c = 'Test1';
        sData.Installer_Only_Email__c = false;
        update sData;
        update stipUpdateLst;
        
        StipulationTriggerHandler stpObj = new StipulationTriggerHandler(true);
        StipulationTriggerHandler.isTrgExecuting = true;
        StipulationTriggerHandler.runOnce();
        StipulationTriggerHandler.isTrgExecuting = false;
        StipulationTriggerHandler.runOnce();
        stpObj.OnBeforeInsert(stipUpdateLst);
        
        Case cas = new Case(Status ='New', Priority = 'Medium', Origin = 'Email', Accountid = acc.Id); 
        insert cas;
        stipUpdateLst[0].case__c = cas.Id;
        update stipUpdateLst[0];
        
        stpObj.caseCompletedStatusonAccount(new Set<Id>{stipUpdateLst[0].Id,stipUpdateLst[1].Id}, new Set<Id>{acc.Id}, new Map<String,String>{stipUpdateLst[0].Id=>acc.Id});

        
        test.stopTest();   
    }
    
}