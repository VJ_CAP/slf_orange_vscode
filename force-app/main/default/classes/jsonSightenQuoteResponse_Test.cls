@IsTest
public class jsonSightenQuoteResponse_Test {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{'+
		'  \"data\": ['+
		'    {'+
		'      \"install_cost_cash\": 3.95,'+
		'      \"proposals\": {'+
		'        \"link\": \"\"'+
		'      },'+
		'      \"status\": \"QUOT\",'+
		'      \"milestones\": {'+
		'        \"link\": \"/api/solar/milestone/?quote_id=11181\"'+
		'      },'+
		'      \"date_created\": \"2016-05-20T00:34:51.568704+00:00\",'+
		'      \"modified_by\": {'+
		'        \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"operations@sunlightfinancial.com\"'+
		'      },'+
		'      \"system\": {'+
		'        \"uuid\": \"aa1ce53c-67ad-4caf-9eb3-b66ac2f65f2f\",'+
		'        \"link\": \"/api/solar/quotegen/system/aa1ce53c-67ad-4caf-9eb3-b66ac2f65f2f\",'+
		'        \"natural_id\": \"System 1\"'+
		'      },'+
		'      \"created_by\": {'+
		'        \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"operations@sunlightfinancial.com\"'+
		'      },'+
		'      \"initial_payment\": 0,'+
		'      \"uuid\": \"8e1eb481-a1f7-4a18-bb14-d1d02e1bbeef\",'+
		'      \"product_eligibility\": {'+
		'        \"eligible\": {'+
		'          \"name\": ['+
		'            \"enVision 20 6.99%\"'+
		'          ],'+
		'          \"count\": 1'+
		'        },'+
		'        \"user\": \"operations@sunlightfinancial.com\",'+
		'        \"pending\": {'+
		'          \"name\": [],'+
		'          \"count\": 0'+
		'        },'+
		'        \"ineligible\": {'+
		'          \"name\": [],'+
		'          \"reasons\": {},'+
		'          \"count\": 0'+
		'        },'+
		'        \"warning\": {'+
		'          \"name\": [],'+
		'          \"reasons\": {},'+
		'          \"count\": 0'+
		'        }'+
		'      },'+
		'      \"product\": {'+
		'        \"uuid\": \"65a9da63-15a8-4436-8fab-94e90637f616\",'+
		'        \"link\": \"/api/solar/product/65a9da63-15a8-4436-8fab-94e90637f616\",'+
		'        \"natural_id\": \"enVision 20 6.99%\"'+
		'      },'+
		'      \"tasks\": {'+
		'        \"link\": \"/api/solar/task/?quote_id=11181\"'+
		'      },'+
		'      \"avg_monthly_solar_bill\": 122.30352257739561,'+
		'      \"natural_id\": \"API REED//enVision 20 6.99%\",'+
		'      \"consumer_savings_usd\": -37.24731297776524,'+
		'      \"owned_by_organization\": {'+
		'        \"uuid\": \"29b2fe4b-df92-45a4-adaa-8f35a39848ed\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"Sunlight Financial\"'+
		'      },'+
		'      \"incentives\": ['+
		'        {'+
		'          \"quoteincentive_id\": null,'+
		'          \"assigned_to\": \"S\",'+
		'          \"upfront_amount\": 0,'+
		'          \"incentive_type\": \"RBT\"'+
		'        },'+
		'        {'+
		'          \"quoteincentive_id\": null,'+
		'          \"assigned_to\": \"S\",'+
		'          \"annual_amounts\": ['+
		'            0'+
		'          ],'+
		'          \"upfront_amount\": 0,'+
		'          \"incentive_type\": \"PTC\"'+
		'        },'+
		'        {'+
		'          \"quoteincentive_id\": null,'+
		'          \"assigned_to\": \"S\",'+
		'          \"upfront_amount\": 0,'+
		'          \"incentive_type\": \"ITC\"'+
		'        }'+
		'      ],'+
		'      \"contract_term\": 20,'+
		'      \"date_updated\": \"2016-05-20T00:34:51.568744+00:00\",'+
		'      \"consumer_savings_pct\": -35.21964969393419,'+
		'      \"owned_by_user\": {'+
		'        \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"operations@sunlightfinancial.com\"'+
		'      },'+
		'      \"rate_contract\": 0.186060125624841,'+
		'      \"amount_financed\": 22119.99999999997,'+
		'      \"rate_esc_pct\": 0'+
		'    },'+
		'    {'+
		'      \"install_cost_cash\": 4,'+
		'      \"proposals\": {'+
		'        \"link\": \"\"'+
		'      },'+
		'      \"status\": \"QUOT\",'+
		'      \"milestones\": {'+
		'        \"link\": \"/api/solar/milestone/?quote_id=11180\"'+
		'      },'+
		'      \"date_created\": \"2016-05-20T00:34:32.696405+00:00\",'+
		'      \"modified_by\": {'+
		'        \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"operations@sunlightfinancial.com\"'+
		'      },'+
		'      \"system\": {'+
		'        \"uuid\": \"aa1ce53c-67ad-4caf-9eb3-b66ac2f65f2f\",'+
		'        \"link\": \"/api/solar/quotegen/system/aa1ce53c-67ad-4caf-9eb3-b66ac2f65f2f\",'+
		'        \"natural_id\": \"System 1\"'+
		'      },'+
		'      \"created_by\": {'+
		'        \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"operations@sunlightfinancial.com\"'+
		'      },'+
		'      \"initial_payment\": 0,'+
		'      \"uuid\": \"99419f7f-2b83-4c67-bd90-ddc915636ee9\",'+
		'      \"product_eligibility\": {'+
		'        \"eligible\": {'+
		'          \"name\": ['+
		'            \"enVision 12 2.99%\"'+
		'          ],'+
		'          \"count\": 1'+
		'        },'+
		'        \"user\": \"operations@sunlightfinancial.com\",'+
		'        \"pending\": {'+
		'          \"name\": [],'+
		'          \"count\": 0'+
		'        },'+
		'        \"ineligible\": {'+
		'          \"name\": [],'+
		'          \"reasons\": {},'+
		'          \"count\": 0'+
		'        },'+
		'        \"warning\": {'+
		'          \"name\": [],'+
		'          \"reasons\": {},'+
		'          \"count\": 0'+
		'        }'+
		'      },'+
		'      \"product\": {'+
		'        \"uuid\": \"66cd5479-9e37-4a18-b0a1-c7eb9975c5a0\",'+
		'        \"link\": \"/api/solar/product/66cd5479-9e37-4a18-b0a1-c7eb9975c5a0\",'+
		'        \"natural_id\": \"enVision 12 2.99%\"'+
		'      },'+
		'      \"tasks\": {'+
		'        \"link\": \"/api/solar/task/?quote_id=11180\"'+
		'      },'+
		'      \"avg_monthly_solar_bill\": 129.73983506731724,'+
		'      \"natural_id\": \"API REED//enVision 12 2.99%\",'+
		'      \"consumer_savings_usd\": -44.68268273030262,'+
		'      \"owned_by_organization\": {'+
		'        \"uuid\": \"29b2fe4b-df92-45a4-adaa-8f35a39848ed\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"Sunlight Financial\"'+
		'      },'+
		'      \"incentives\": ['+
		'        {'+
		'          \"quoteincentive_id\": null,'+
		'          \"assigned_to\": \"S\",'+
		'          \"upfront_amount\": 0,'+
		'          \"incentive_type\": \"RBT\"'+
		'        },'+
		'        {'+
		'          \"quoteincentive_id\": null,'+
		'          \"assigned_to\": \"S\",'+
		'          \"annual_amounts\": ['+
		'            0'+
		'          ],'+
		'          \"upfront_amount\": 0,'+
		'          \"incentive_type\": \"PTC\"'+
		'        },'+
		'        {'+
		'          \"quoteincentive_id\": null,'+
		'          \"assigned_to\": \"S\",'+
		'          \"upfront_amount\": 0,'+
		'          \"incentive_type\": \"ITC\"'+
		'        }'+
		'      ],'+
		'      \"contract_term\": 12,'+
		'      \"date_updated\": \"2016-05-20T00:34:32.696441+00:00\",'+
		'      \"consumer_savings_pct\": -42.25025397364597,'+
		'      \"owned_by_user\": {'+
		'        \"uuid\": \"ae5116c5-ce0d-4f9f-93e8-399e2fc23854\",'+
		'        \"link\": \"\",'+
		'        \"natural_id\": \"operations@sunlightfinancial.com\"'+
		'      },'+
		'      \"rate_contract\": 0.197372974240341,'+
		'      \"amount_financed\": 22400.000000000047,'+
		'      \"rate_esc_pct\": 0'+
		'    }'+
		'  ],'+
		'  \"messages\": {'+
		'    \"info\": ['+
		'      {'+
		'        \"message\": \"Successfully fetched Quote instance(s)\",'+
		'        \"msg_code\": \"3MM6A10NVC\",'+
		'        \"timestamp\": \"2016-05-19 19:35:12.368211\"'+
		'      }'+
		'    ],'+
		'    \"error\": [],'+
		'    \"critical\": [],'+
		'    \"warning\": []'+
		'  },'+
		'  \"db_table\": \"solar_quote\",'+
		'  \"status_code\": 200,'+
		'  \"count\": 2'+
		'}';
		jsonSightenQuoteResponse r = jsonSightenQuoteResponse.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Incentives_Z objIncentives_Z = new jsonSightenQuoteResponse.Incentives_Z(System.JSON.createParser(json));
		System.assert(objIncentives_Z != null);
		System.assert(objIncentives_Z.quoteincentive_id == null);
		System.assert(objIncentives_Z.assigned_to == null);
		System.assert(objIncentives_Z.upfront_amount == null);
		System.assert(objIncentives_Z.incentive_type == null);
		System.assert(objIncentives_Z.annual_amounts == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Modified_by objModified_by = new jsonSightenQuoteResponse.Modified_by(System.JSON.createParser(json));
		System.assert(objModified_by != null);
		System.assert(objModified_by.uuid == null);
		System.assert(objModified_by.link == null);
		System.assert(objModified_by.natural_id == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Messages objMessages = new jsonSightenQuoteResponse.Messages(System.JSON.createParser(json));
		System.assert(objMessages != null);
		System.assert(objMessages.info == null);
		System.assert(objMessages.error == null);
		System.assert(objMessages.critical == null);
		System.assert(objMessages.warning == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse objRoot = new jsonSightenQuoteResponse(System.JSON.createParser(json));
		System.assert(objRoot != null);
		System.assert(objRoot.data == null);
		System.assert(objRoot.messages == null);
		System.assert(objRoot.db_table == null);
		System.assert(objRoot.status_code == null);
		System.assert(objRoot.count == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Ineligible objIneligible = new jsonSightenQuoteResponse.Ineligible(System.JSON.createParser(json));
		System.assert(objIneligible != null);
		System.assert(objIneligible.name == null);
		System.assert(objIneligible.reasons == null);
		System.assert(objIneligible.count == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Data objData = new jsonSightenQuoteResponse.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.install_cost_cash == null);
		System.assert(objData.proposals == null);
		System.assert(objData.status == null);
		System.assert(objData.milestones == null);
		System.assert(objData.date_created == null);
		System.assert(objData.modified_by == null);
		System.assert(objData.system_Z == null);
		System.assert(objData.created_by == null);
		System.assert(objData.initial_payment == null);
		System.assert(objData.uuid == null);
		System.assert(objData.product_eligibility == null);
		System.assert(objData.product == null);
		System.assert(objData.tasks == null);
		System.assert(objData.avg_monthly_solar_bill == null);
		System.assert(objData.natural_id == null);
		System.assert(objData.consumer_savings_usd == null);
		System.assert(objData.owned_by_organization == null);
		System.assert(objData.incentives == null);
		System.assert(objData.contract_term == null);
		System.assert(objData.date_updated == null);
		System.assert(objData.consumer_savings_pct == null);
		System.assert(objData.owned_by_user == null);
		System.assert(objData.rate_contract == null);
		System.assert(objData.amount_financed == null);
		System.assert(objData.rate_esc_pct == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Proposals objProposals = new jsonSightenQuoteResponse.Proposals(System.JSON.createParser(json));
		System.assert(objProposals != null);
		System.assert(objProposals.link == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Eligible objEligible = new jsonSightenQuoteResponse.Eligible(System.JSON.createParser(json));
		System.assert(objEligible != null);
		System.assert(objEligible.name == null);
		System.assert(objEligible.count == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Info objInfo = new jsonSightenQuoteResponse.Info(System.JSON.createParser(json));
		System.assert(objInfo != null);
		System.assert(objInfo.message == null);
		System.assert(objInfo.msg_code == null);
		System.assert(objInfo.timestamp == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Pending objPending = new jsonSightenQuoteResponse.Pending(System.JSON.createParser(json));
		System.assert(objPending != null);
		System.assert(objPending.name == null);
		System.assert(objPending.count == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Product_eligibility objProduct_eligibility = new jsonSightenQuoteResponse.Product_eligibility(System.JSON.createParser(json));
		System.assert(objProduct_eligibility != null);
		System.assert(objProduct_eligibility.eligible == null);
		System.assert(objProduct_eligibility.user == null);
		System.assert(objProduct_eligibility.pending == null);
		System.assert(objProduct_eligibility.ineligible == null);
		System.assert(objProduct_eligibility.warning == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse.Name objName = new jsonSightenQuoteResponse.Name(System.JSON.createParser(json));
		System.assert(objName != null);
	}
}