global class ResendAutomatedLoanConfirmationEmail 
{
    global class SendEmailConfirmationInput 
    {
        @InvocableVariable(required=true) global Id uwfId;

        global SendEmailConfirmationInput () {}

        global SendEmailConfirmationInput (Id uwfId) 
        {
            this.uwfId = uwfId;
        }
    }

//  **************************************************

    global class SendEmailConfirmationOutput 
    {
        @InvocableVariable global Boolean success;
        @InvocableVariable global String errorMessage;
    }

//  **************************************************

    @InvocableMethod(label='Resend Email Confirmation')
    global static list<SendEmailConfirmationOutput> sendEmailConfirmation (list<SendEmailConfirmationInput> inputs)
    {
    list<SendEmailConfirmationOutput> outputs = new list<SendEmailConfirmationOutput>{};

        for (SendEmailConfirmationInput i : inputs)
        {
            SendEmailConfirmationOutput o = new SendEmailConfirmationOutput();
            String result = sendEmailConfirmation (i.uwfId);

            if (result == null)
            {
                o.success = true;
            }
            else
            {
                o.success = false;
                o.errorMessage = result;
            }
            outputs.add (o);
        }
        return outputs;
    }

//  **************************************************

    global static String sendEmailConfirmation (Id uwfId)
    {
        Underwriting_File__c uwf;

        try
        {
            uwf = [SELECT Id, Opportunity__c, 
                                Opportunity__r.SLF_Product__r.Short_Term_Facility__c,Opportunity__r.SyncedQuoteId,
                                Opportunity__r.isACH__c,Opportunity__r.ACH_APR_Process_Required__c,Confirm_ACH_Information__c,
                                Opportunity__r.SLF_Product__r.Long_Term_Facility__c, 
                                Opportunity__r.SLF_Product__r.Loan_Type__c,
                                Opportunity__r.Product_Loan_Type__c
                                FROM Underwriting_File__c WHERE Id = :uwfId LIMIT 1];
                if(uwf != null){ // part of 2040 
                    uwf.Send_Confirmation_Email__c = true;
                    update uwf;
                }
        }
        catch( Exception e )
        {
            return 'Unable to get Underwriting File: ' + e.getMessage();
        }
        Map<String,Underwriting_Confirmation_Emails__c> confirmemailsMap = Underwriting_Confirmation_Emails__c.getAll();
        
        set<string> emailTemplateSet = new set<string>();
        for(string templateIterate : confirmemailsMap.keyset()){
            emailTemplateSet.add(confirmemailsMap.get(templateIterate).Email_Template_Name__c);
        }
        
        system.debug('### confirmemailsMap - '+confirmemailsMap.keySet());
        List<EmailTemplate> emailTemplates = [SELECT Id, DeveloperName, TemplateType, Subject FROM EmailTemplate WHERE DeveloperName IN :emailTemplateSet];
        
        Map<String, Id> emailTemplateByDevNameMap = new Map<String, Id>();
        System.debug('### emailTemplates'+emailTemplates);

        for(EmailTemplate et : emailTemplates)
        {
            emailTemplateByDevNameMap.put(et.DeveloperName, et.Id);
        }
        
        Map<Id, Id> oppIdToTemplateIdMap = new Map<Id, Id>();
        
        for (Underwriting_Confirmation_Emails__c confirmemails : confirmemailsMap.values())
        {
            if(uwf.Opportunity__r.SLF_Product__r.Long_Term_Facility__c == confirmemails.Long_Term_Facility__c && 
                uwf.Opportunity__r.SLF_Product__r.Short_Term_Facility__c == confirmemails.Short_Term_Facility__c && 
                uwf.Opportunity__r.SLF_Product__r.Loan_Type__c == confirmemails.Loan_Type__c &&
                uwf.Opportunity__r.Product_Loan_Type__c == confirmemails.Product_Loan_Type__c)
            {
                System.debug('### Email_Template_Name__c'+confirmemails.Email_Template_Name__c);
                oppIdToTemplateIdMap.put(uwf.Opportunity__c, emailTemplateByDevNameMap.get(confirmemails.Email_Template_Name__c));
            }
        }

        if (oppIdToTemplateIdMap.size() > 0)
        {
            List<Attachment> attachList = new List<Attachment>();
            //Updated By Adithya on 11/25/2019 (i.e Removed SOQL from for loop) 
            Contact dummyContact = [SELECT Id, Email FROM Contact WHERE Name = 'Sunlight Financial' AND Email <> null LIMIT 1];
            for (Id oppId : oppIdToTemplateIdMap.keySet())
            {
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                system.debug('oppIdToTemplateIdMap.get(oppId)======'+oppIdToTemplateIdMap.get(oppId));
                msg.setTemplateId(oppIdToTemplateIdMap.get(oppId));
                msg.setTargetObjectId(dummyContact.Id);
                msg.setSaveAsActivity(false);
                //msg.setWhatId(oppId);
                msg.setWhatId(uwfId);
                //msg.setToAddresses(new List<String> { 'noreply@email.com' });

                Savepoint sp = Database.setSavepoint();
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                Database.rollback(sp);

                //string body = msg.getPlainTextBody().replace('\n', '\r\n');
               string body= '';
                //string body = msg.getPlainTextBody().replace('\n', '\r\n');
                if(emailTemplates[0].TemplateType != 'Text'){
                    body = msg.getHTMLBody();
                    body = '<html>' + body + '</html>';
                    system.debug('**SZ: line breaks = ' + body.indexOf('\r\n'));
                }else{
                    body = msg.getPlainTextBody();
                }
                system.debug('**SZ: body = ' + body);
                System.debug('msg.getSubject()::'+msg.getSubject());

                attachList.add(new Attachment(
                    Name = 'Confirmation_Email_' + (Datetime.now().format('yyyyMMdd_hhmmss')) + '.html',
                    ContentType = 'text/html',
                    Body = Blob.valueOf(body),
                    ParentId = oppId
                ));
            }

            if (attachList.size() > 0)
            {
                insert attachList;
                //Updated by Adithya on 11/25/2019
                //start
                Set<Id> attachmentIds = new Set<Id>();
                for(Attachment att : attachList)
                {
                    attachmentIds.add(att.Id);
                }
                if(!attachmentIds.isEmpty())
                    SendAttachmentsToBox (attachmentIds,false);
                //End
            }

            
        }
        return null;
    }

//  **************************************************

    @future(callout=true)
    public static void sendAttachmentsToBox (Set<Id> attachmentIds, boolean addIdtoName)
    {
        List<Attachment> attachmentList = [SELECT Id, Name, ParentId, Body FROM Attachment WHERE Id IN :attachmentIds];

        set<Id> oppIds = new set<Id>{};

        for (Attachment a : attachmentList) oppIds.add (a.ParentId);

        map<Id,String> oppToFolderIdsMap = new map<Id,String>{};

        //  map the opp Id to the box folder id 

        for (box__FRUP__c f : [SELECT Id, box__Record_ID__c, box__Folder_ID__c
                                FROM box__FRUP__c
                                WHERE box__Object_Name__c = 'Opportunity'
                                AND box__Record_ID__c IN :oppIds])
        {
            oppToFolderIdsMap.put (f.box__Record_ID__c, f.box__Folder_ID__c); 
        }

        if (Test.isRunningTest()) return;

        BoxPlatformApiConnection api = BoxAuthentication.getConnection();

        for (Attachment att : attachmentList)
        {
            //  get the folder Id for the Opp owning the attachment

            String oppFolderId = oppToFolderIdsMap.get (att.ParentId);

            if (oppFolderId == null) continue;  //  no box folder for opp - error condition

            BoxFolder oppFolder = new BoxFolder(api, oppFolderId);

            //  look for the Email folder inder the Opp folder

            String emailFolderId = null;
            String paymentElectionFId = null;


            for (BoxItem.Info i : oppFolder.getChildren())
            {
                 // Added if condition as part of 2040 Sprint 12 - by kalyani -Start
                if(i.Name=='Payment Election Form')
                {
                    paymentElectionFId = i.Id;
                }// Added if condition as part of 2040 Sprint 12 - by kalyani -End
   
                 else if(i.Name == 'Emails')
                {
                    emailFolderId = i.Id;
                 //   break;
                }
            }
            
            //Added by Adithya to fix ORANGE-14429 on 11/21/2019 
            //Start
            BoxFolder emailFolder;
            if(att.Name.contains('PAYMENT ELECTION NOTICE'))
            {
                if(paymentElectionFId == null)
                {
                    BoxItem.Info i = oppFolder.createFolder('Payment Election Form');
                    paymentElectionFId = i.Id;   
                    emailFolder = new BoxFolder(api, paymentElectionFId);
                }else{
                    emailFolder = new BoxFolder(api, paymentElectionFId);   
                }
            }else if(emailfolderId == null){
                BoxItem.Info i = oppFolder.createFolder('Emails');
                emailFolderId = i.Id;
                emailFolder = new BoxFolder(api, emailfolderId);
            }else if(null != emailfolderId){
                emailFolder = new BoxFolder(api, emailfolderId);
            }
            //End
            
            
            //  if no email folder then create it
            //  Added if condition as part of 2040 Sprint 12 - by kalyani -Start
            /*  
            BoxFolder emailFolder;
            if(att.Name.contains('PAYMENT ELECTION NOTICE') && paymentElectionFId == null)
            {
                BoxItem.Info i = oppFolder.createFolder('Payment Election Form');
                paymentElectionFId = i.Id;   
                emailFolder = new BoxFolder(api, paymentElectionFId);
            }
            else if(emailfolderId == null){    
                BoxItem.Info i = oppFolder.createFolder('Emails');
                emailFolderId = i.Id;
                emailFolder = new BoxFolder(api, emailfolderId); 
            }
            */
            //  Added if condition as part of 2040 Sprint 12 - by kalyani -End
            //  connect to the email folder

            //BoxFolder emailFolder = new BoxFolder(api, emailfolderId);

            Document d = new Document();

            /*if(addIdtoName)
            {
                d.Name = att.id+'_'+att.Name;
            }
            else
            {*/
                d.Name = att.Name;
            //}
            d.Body = att.Body;

            //  upload the document to the email folder
            system.debug('### d.Name: '+ d.Name);
            if(null != emailFolder)
                BoxFile.Info fileInfo = emailFolder.uploadFile(d, d.Name);
        }
    }
}