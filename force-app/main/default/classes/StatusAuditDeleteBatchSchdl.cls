/**
* Description: To delete Status Audit Records
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Deepika               06/04/2019             Created
******************************************************************************************/

global class StatusAuditDeleteBatchSchdl implements Database.Batchable<sobject>,Database.AllowsCallouts,Database.Stateful,Schedulable {
    private String query; //Query in the start method
    global Map<Id,Status_Audit__c> statusAuditMap = new Map<Id,Status_Audit__c>();    
    
    /**
    * Description: Entry of batch and execute a SOQL.
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('### Entered into start() of '+ StatusAuditDeleteBatchSchdl.class);        
        this.query = 'select isProcessed__c,JSON_Body__c,Opportunity__c,Opportunity__r.Installer_Account__c,Opportunity__r.Installer_Account__r.Name,Opportunity__r.Installer_Account__r.Push_Endpoint__c,Opportunity__r.Installer_Account__r.Subscribe_to_Push_Updates__c,Opportunity__r.Installer_Account__r.Endpoint_Header__c,Name,createdDate from Status_Audit__c where CreatedDate = N_DAYS_AGO:7';
        system.debug('### query String '+ query);
        system.debug('### Entered into end() of '+ StatusAuditDeleteBatchSchdl.class);
        return Database.getQueryLocator(query);
    }
    
    /**
    * Description: Delete Status Audit Records
    */
    global void execute(Database.BatchableContext BC, List<Status_Audit__c> stausAuditList)
    {
        system.debug('### Entered into execute() of Batch class - '+ StatusAuditDeleteBatchSchdl.class);
        system.debug('### stausAuditList '+ stausAuditList);
        try
        {  
            if(!stausAuditList.isEmpty())
                SLFUtility.deleteData(stausAuditList); 
        }
        catch(Exception ex)
        {
            system.debug('### StatusAuditDeleteBatchSchdl.execute():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('StatusAuditDeleteBatchSchdl.execute()',ex.getLineNumber(),'execute()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }   
        system.debug('### Exit from execute() of Batch class - '+ StatusAuditDeleteBatchSchdl.class);
    }
    
    /**
    * Description:
    */
    global void finish(Database.BatchableContext bc){        
    }
    
    /**
    * Description: Execute Batch class 
    */
    global void execute(SchedulableContext sc) {
        System.debug('### Entered into execute() of Schedule Class - '+ StatusAuditDeleteBatchSchdl.class);
        StatusAuditDeleteBatchSchdl deleteBatch = new StatusAuditDeleteBatchSchdl();
        Database.executeBatch(deleteBatch);
        System.debug('### Exit from execute() of Schedule Class - '+ StatusAuditDeleteBatchSchdl.class);
    }
}