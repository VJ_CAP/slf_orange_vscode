/**
* Description: User validation logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suresh Kumar            10/26/2018          Created
******************************************************************************************/
public with sharing class DrawRequestServiceImpl extends RESTServiceBase{  
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back with data and response status code.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + DrawRequestServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        DrawRequestDataServiceImpl submitRatingDataSrvImpl = new DrawRequestDataServiceImpl(); 

        try{
            system.debug('### rw.reqDataStr***'+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            res.response = submitRatingDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
           system.debug('### DrawRequestServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' DrawRequestServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of ' + DrawRequestServiceImpl.class);
        return res.response ;
    }
}