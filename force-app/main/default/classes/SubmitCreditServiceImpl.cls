/**
* Description: submitcredit integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class SubmitCreditServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + SubmitCreditServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        SubmitCreditDataServiceImpl submitCreditDataSrvImpl = new SubmitCreditDataServiceImpl(); 
        IRestResponse iRestRes;
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            res.response = submitCreditDataSrvImpl.transformOutput(reqData);
        }catch(Exception err){
            system.debug('### SubmitCreditServiceImpl .fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           
            ErrorLogUtility.writeLog(' SubmitCreditServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));
            UnifiedBean creditbean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            creditbean.error  = errorWrapperLst ;
            //creditbean.error.add(errorMsg);
            creditbean.returnCode = '214';
            iRestRes = (IRestResponse)creditbean;
            res.response= iRestRes;        
        }
        system.debug('### Exit from fetchData() of ' + SubmitCreditServiceImpl.class);
        return res.response ;
    }
    
    
}