@isTest
private class StipulationAPREmailBatch_Test{  
    @testSetup static void setup() {
        // Creating Trigger flags.
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =false;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =false;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =false;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =false;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =false;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgFundingDataflag = new TriggerFlags__c();
        trgFundingDataflag.Name ='Funding_Data__c';
        trgFundingDataflag.isActive__c =false;
        trgLst.add(trgFundingDataflag);
        insert trgLst; 
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName(); 
        
        //Insert Profile
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        //For StipulationAPREmailBatch Inserting Account record 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.HI_Dealer_Code__c = '123456';
        acc.Home_Improvement_Enabled__c = true;
        insert acc;
        
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con;  
        //Insert SLF Product Record
        List<Product__c> prodList = new List<Product__c>();
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.NonACH_APR__c = 3.24;
        prod.Internal_Use_Only__c = false;
        // prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 180;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        //insert prod;
        prodList.add(prod);
        insert prodList;
        
        //Insert opportunity record
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = acc.id;
        personOpp.isACH__c = true;
        personOpp.Project_Category__c = 'Solar';
        personOpp.Installer_Account__c = acc.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Install_Street__c ='Street';
        personOpp.Install_City__c ='InCity';
        personOpp.Combined_Loan_Amount__c =10000;
        personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
        personOpp.ACH_APR_Process_Required__c = True;
        personOpp.hash_id__c = '6767bnbhgj8878_';
        personOpp.ACH_APR__c= 2.99;
        personOpp.NonACH_APR__c= 3.23;
       
        personOpp.SLF_Product__c = prod.id;            
        //insert personOpp;
        oppList.add(personOpp);
        insert oppList;
        
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = oppList[0].id;
        undStpbt.Approved_for_Payments__c = true;
        undStpbt.M1_Review_Complete_Date__c = null;
        insert undStpbt;
        
        Case c = new Case();
        c.Installer_Account__c = acc.Id;
        c.Stipulation_Email_Text__c = 'Action needed!';        
        insert c;
        
        Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'APR', Email_Text__c = 'Test data', POS_Message__c = 'test',Review_Classification__c='SLS II');
        insert sd;
        
        List<Stipulation__c> stpList = new List<Stipulation__c>();
       
        Stipulation__c stp = new Stipulation__c();
        stp.Status__c ='In Progress';
        stp.Stipulation_Data__c = sd.id;
        stp.Underwriting__c =undStpbt.id;
        stp.Case__c = c.id;
        insert stp;
        Datetime DaysAgo = Datetime.now().addDays(-10);
        Test.setCreatedDate(stp.Id, DaysAgo);
        Stipulation__c myStip = [SELECT Id, CreatedDate FROM Stipulation__c  limit 1];
        //System.assertEquals(myStip.CreatedDate, DateTime.newInstance(2019,07,17)); 
        
    } 
    private testMethod static void stipAPREmailTest(){
        Test.startTest();
         Database.executeBatch(new StipulationAPREmailBatch());
        Test.stopTest();
    }
}