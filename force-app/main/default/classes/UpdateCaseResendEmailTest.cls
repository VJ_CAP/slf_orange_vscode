@isTest
public class UpdateCaseResendEmailTest {
    
    public testMethod static void resendEmailTest(){
        
        Test.startTest();
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();          
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);  
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag); 
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();  
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        //Insert opportunity record
        Opportunity oppObj = new Opportunity();
        oppObj.name = 'OppNew';
        oppObj.CloseDate = system.today();
        oppObj.StageName = 'New';
        oppObj.AccountId = acc.id;
        oppObj.Installer_Account__c = acc.id;
        oppObj.Install_State_Code__c = 'CA';
        oppObj.Install_Street__c ='Street';
        oppObj.Install_City__c ='InCity';
        oppObj.Combined_Loan_Amount__c = 10000;
        oppObj.Partner_Foreign_Key__c = 'TCU||1234';
        oppObj.Co_Applicant__c = acc.id;
        insert oppObj;  
        
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = oppObj.id;
        undStpbt.Approved_for_Payments__c = true;
        insert undStpbt;
        
        Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', Email_Text__c = 'Test data', POS_Message__c = 'test', Review_Classification__c = 'SLS II');
        insert sd;
        
        Stipulation__c stp = new Stipulation__c();
        stp.Status__c ='New';
        stp.Stipulation_Data__c = sd.id;
        
        stp.Underwriting__c =undStpbt.id;
        insert stp;
        
        UpdateCaseResendEmail.Input obj=new UpdateCaseResendEmail.Input();
        UpdateCaseResendEmail.Input inpObj=new UpdateCaseResendEmail.Input(stp.id);
        inpObj.stipId=stp.id;
        List<UpdateCaseResendEmail.Input> StipList=new List<UpdateCaseResendEmail.Input>();
        StipList.add(inpObj);
        List<UpdateCaseResendEmail.Output> result=UpdateCaseResendEmail.updateCase(StipList);
        System.debug(result);
        
        Test.stopTest();
        
        
    }
}