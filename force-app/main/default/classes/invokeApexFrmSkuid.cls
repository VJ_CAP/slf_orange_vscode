global class invokeApexFrmSkuid {
    
    global class invokeApexFrmSkuidResult {    
    @InvocableVariable(required=true) global Id inputId;
    @InvocableVariable global String error;
  }
    
    global class SendEmailConfirmationOutput 
    {
        @InvocableVariable global String Message;
    }
    
@InvocableMethod(label='send errorMsg')
    global static List<SendEmailConfirmationOutput> sendEmail(list<Id> inputId)
    {
        list<SendEmailConfirmationOutput> outputs = new list<SendEmailConfirmationOutput>{};
        system.debug('--inputId--'+inputId);
        SendEmailConfirmationOutput o = new SendEmailConfirmationOutput();
        o.Message = 'There is an error';
        system.debug('--error--'+o.Message );
        outputs.add(o);
        return outputs;
    }
}