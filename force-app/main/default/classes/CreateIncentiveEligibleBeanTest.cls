@isTest(seeAllData=false)
public class CreateIncentiveEligibleBeanTest{    
    
    private testMethod static void CreateIncentiveEligibleBeanTest(){
        CreateIncentiveEligibleBean crtIncElgBeanObj = new CreateIncentiveEligibleBean();
        crtIncElgBeanObj.returnCode = '200';
        crtIncElgBeanObj.quoteName = 'Quote';
        crtIncElgBeanObj.product = 'Product';
        crtIncElgBeanObj.amortizationTable = new List<String>{'amortizationTable'};
        crtIncElgBeanObj.finalMonthlyPayment = 200;
        crtIncElgBeanObj.finalEscalatedMonthlyPayment = 100;
        crtIncElgBeanObj.message = 'Hi';
        crtIncElgBeanObj.monthlyPayment = 200;
        crtIncElgBeanObj.prepaymentAmortizationTable = new List<String>{'prepaymentAmortizationTable'};
        crtIncElgBeanObj.escalatedMonthlyPayment  = 200;
        CreateIncentiveEligibleBean.ErrorWrapper errorWrprObj = new CreateIncentiveEligibleBean.ErrorWrapper();
        errorWrprObj.errorMessage = 'Error';
        crtIncElgBeanObj.error  = new List<CreateIncentiveEligibleBean.ErrorWrapper>{errorWrprObj};
    }
}