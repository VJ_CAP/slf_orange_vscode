/******************************************************************************************
* Description: The Class CreateTaskForUnderWrittingCTRL to do unin test 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Veereandranath Jalla   11/26/2018          Created
******************************************************************************************/
@isTest
public with sharing class CreateTaskForUnderWrittingCTRLTest {
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
    }
    private testMethod static void BuildHardfullReqServiceTest(){
        Underwriting_File__c undStpbt = [select Id,Project_Status__c From Underwriting_File__c where Opportunity__r.Project_Category__c='Solar'  limit 1];
        undStpbt.Project_Status__c = 'M0';
        update undStpbt;
        
        Underwriting_File__c objUWFile;
        Task taskObj = new Task(WhatId = undStpbt.Id, Subject = 'Call',Priority = 'High',Status='Completed',type = 'M1 Review');
        insert taskObj;
        
        List<CreateTaskForUnderWrittingCTRL.Input> lstInput = new List<CreateTaskForUnderWrittingCTRL.Input>();        
        CreateTaskForUnderWrittingCTRL.Input globalInput = new CreateTaskForUnderWrittingCTRL.Input(undStpbt.id);
        globalInput.strUnderWrittingId = undStpbt.Id;
        lstInput.add(globalInput);
        List<CreateTaskForUnderWrittingCTRL.Output> lstOutput = new List<CreateTaskForUnderWrittingCTRL.Output>();
        CreateTaskForUnderWrittingCTRL.Output ouput = new CreateTaskForUnderWrittingCTRL.Output();
        lstOutput = CreateTaskForUnderWrittingCTRL.InsertTask(lstInput);
        for(CreateTaskForUnderWrittingCTRL.Output objouput:lstOutput){
            system.assertnotequals(null,objouput.message);
        }
        
        undStpbt.Project_Status__c = 'M1';
        update undStpbt;
        List<CreateTaskForUnderWrittingCTRL.Input> lstInput2 = new List<CreateTaskForUnderWrittingCTRL.Input>();        
        CreateTaskForUnderWrittingCTRL.Input globalInput2 = new CreateTaskForUnderWrittingCTRL.Input();
        globalInput2.strUnderWrittingId = undStpbt.Id;
        lstInput2.add(globalInput2);
        List<CreateTaskForUnderWrittingCTRL.Output> lstOutput2 = new List<CreateTaskForUnderWrittingCTRL.Output>();
        CreateTaskForUnderWrittingCTRL.Output ouput2 = new CreateTaskForUnderWrittingCTRL.Output();
        objUWFile = [select Id,(select Id,OwnerId,Owner.Name from Tasks) 
                     from Underwriting_File__c where Id =:undStpbt.id limit 1];
        delete objUWFile.Tasks;
        lstOutput2 = CreateTaskForUnderWrittingCTRL.InsertTask(lstInput2);
        for(CreateTaskForUnderWrittingCTRL.Output objouput:lstOutput2){
            system.assertnotequals(null,objouput.message);
        }
        // permit
        undStpbt.Project_Status__c = 'Permit';
        update undStpbt;
        List<CreateTaskForUnderWrittingCTRL.Input> lstInput4 = new List<CreateTaskForUnderWrittingCTRL.Input>();        
        CreateTaskForUnderWrittingCTRL.Input globalInput4 = new CreateTaskForUnderWrittingCTRL.Input();
        globalInput4.strUnderWrittingId = undStpbt.Id;
        lstInput4.add(globalInput4);
        List<CreateTaskForUnderWrittingCTRL.Output> lstOutput4 = new List<CreateTaskForUnderWrittingCTRL.Output>();
        CreateTaskForUnderWrittingCTRL.Output ouput4 = new CreateTaskForUnderWrittingCTRL.Output();
        objUWFile = [select Id,(select Id,OwnerId,Owner.Name from Tasks) 
                     from Underwriting_File__c where Id =:undStpbt.id limit 1];
        delete objUWFile.Tasks;
        lstOutput4 = CreateTaskForUnderWrittingCTRL.InsertTask(lstInput4);
        for(CreateTaskForUnderWrittingCTRL.Output objouput:lstOutput4){
            system.assertnotequals(null,objouput.message);
        }
        // permit end
        // kitting
        objUWFile = [select Id,(select Id,OwnerId,Owner.Name from Tasks) 
                     from Underwriting_File__c where Id =:undStpbt.id limit 1];
        delete objUWFile.Tasks;
        
        undStpbt.Project_Status__c = 'Kitting';
        update undStpbt;
        List<CreateTaskForUnderWrittingCTRL.Input> lstInput5 = new List<CreateTaskForUnderWrittingCTRL.Input>();        
        CreateTaskForUnderWrittingCTRL.Input globalInput5 = new CreateTaskForUnderWrittingCTRL.Input();
        globalInput5.strUnderWrittingId = undStpbt.Id;
        lstInput5.add(globalInput5);
        List<CreateTaskForUnderWrittingCTRL.Output> lstOutput5 = new List<CreateTaskForUnderWrittingCTRL.Output>();
        CreateTaskForUnderWrittingCTRL.Output ouput5 = new CreateTaskForUnderWrittingCTRL.Output();
        objUWFile = [select Id,(select Id,OwnerId,Owner.Name from Tasks) 
                     from Underwriting_File__c where Id =:undStpbt.id limit 1];
        delete objUWFile.Tasks;
        lstOutput5 = CreateTaskForUnderWrittingCTRL.InsertTask(lstInput5);
        for(CreateTaskForUnderWrittingCTRL.Output objouput:lstOutput5){
            system.assertnotequals(null,objouput.message);
        }
        // kitting end
        // Inspection
        objUWFile = [select Id,(select Id,OwnerId,Owner.Name from Tasks) 
                     from Underwriting_File__c where Id =:undStpbt.id limit 1];
        delete objUWFile.Tasks;
        
        undStpbt.Project_Status__c = 'Inspection';
        update undStpbt;
        List<CreateTaskForUnderWrittingCTRL.Input> lstInput6 = new List<CreateTaskForUnderWrittingCTRL.Input>();        
        CreateTaskForUnderWrittingCTRL.Input globalInput6 = new CreateTaskForUnderWrittingCTRL.Input();
        globalInput6.strUnderWrittingId = undStpbt.Id;
        lstInput6.add(globalInput6);
        List<CreateTaskForUnderWrittingCTRL.Output> lstOutput6 = new List<CreateTaskForUnderWrittingCTRL.Output>();
        CreateTaskForUnderWrittingCTRL.Output ouput6 = new CreateTaskForUnderWrittingCTRL.Output();
        objUWFile = [select Id,(select Id,OwnerId,Owner.Name from Tasks) 
                     from Underwriting_File__c where Id =:undStpbt.id limit 1];
        delete objUWFile.Tasks;
        lstOutput6 = CreateTaskForUnderWrittingCTRL.InsertTask(lstInput6);
        for(CreateTaskForUnderWrittingCTRL.Output objouput:lstOutput6){
            system.assertnotequals(null,objouput.message);
        }
        // Inspection end
        Task objTask = new Task();
        objTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Underwriting Task').getRecordTypeId();
        objTask.WhatId = undStpbt.id;//strUnWrId;
        objTask.OwnerId = UserInfo.getUserId();
        objTask.Status = 'Completed';
        objTask.Subject = undStpbt.Project_Status__c + 'Review' ;
        objTask.ActivityDate = system.today();
        objTask.Start_Date_Time__c = system.now();
        objTask.type = 'M1 Review';
        objTask.IsReminderSet = true;
        insert objTask;
        List<CreateTaskForUnderWrittingCTRL.Input> lstInput3 = new List<CreateTaskForUnderWrittingCTRL.Input>();        
        CreateTaskForUnderWrittingCTRL.Input globalInput3 = new CreateTaskForUnderWrittingCTRL.Input(undStpbt.Id);
        globalInput3.strUnderWrittingId = undStpbt.Id;
        lstInput3.add(globalInput3);
        List<CreateTaskForUnderWrittingCTRL.Output> lstOutput3 = new List<CreateTaskForUnderWrittingCTRL.Output>();
        CreateTaskForUnderWrittingCTRL.Output ouput3 = new CreateTaskForUnderWrittingCTRL.Output();
        
        // convering exception block
        try{
            objUWFile = [select Id,(select Id,OwnerId,Owner.Name from Tasks) from Underwriting_File__c where Id =:undStpbt.id limit 1];
                        System.debug('before delter'+objUWFile.Tasks);

            delete objUWFile.Tasks;
            System.debug('after delter'+objUWFile.Tasks);
            lstOutput3 = CreateTaskForUnderWrittingCTRL.InsertTask(lstInput3);
        }Catch(Exception ex){
            system.debug('exception');
        }
    }
}