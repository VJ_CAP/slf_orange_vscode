/**
* Description: StatusBean class to marshall and unmarshall request and response.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public class ValidateDocumentUploadDataObject implements IRestRequest, IRestResponse,IDataObject {
   
    public String documentFolderType {get; set;}
    public String opportunityId {get; set;} 
    public String externalId {get; set;}
    public String fileName {get; set;}
    public String sharedLink{get;set;} 
    public OutputWrapper output {get; set;}
    
    public class OutputWrapper 
    {
        public Boolean success;
        public String errorMessage;
        public String accessToken;
        public String documentFolderId;
        public String fileName;
        public boolean isFileNameAlreadyExist;
        public String existFileId;
        public String loggedInUserName;
        public String opportunityFolderId;
        public boolean isFolderAlreadyExist;     //Srikanth - to know if the subfolder (like Loan Agreements) got created or already existed
        public String sharedLink;     //Srikanth - added to update Underwriting File record in DownloadLoanDocs class
        public Id uwId;     //Srikanth - added to update Underwriting File record in DownloadLoanDocs class
        public String returnCode;
    }
    
    
}