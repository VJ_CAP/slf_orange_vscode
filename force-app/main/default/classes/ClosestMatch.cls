public class ClosestMatch
{
    public static Integer findClosestInteger(Integer[] arr, Integer n, Integer target) 
    { 
        if (target <= arr[0]) 
        {
            return arr[0]; 
        }
        if (target >= arr[n - 1]) 
        {
            return arr[n - 1]; 
        }
        
        Integer i = 0;
        Integer j = n; 
        Integer mid = 0; 
        while (i < j) 
        { 
            mid = (i + j) / 2; 
            system.debug('### mid:' + mid);
            if (arr.get(mid) == target) 
            {
                system.debug('### line 23 arr[mid] :' + arr[mid]);
                return arr[mid]; 
            }
            if (target < arr[mid]) 
            {
                if (mid > 0 && target > arr[mid - 1]) 
                {
                    system.debug('### line 30 mid :' + mid);
                    return getClosestInteger(arr[mid - 1], arr[mid], target); 
                }
                j = mid;
                system.debug('### line 34 mid :' +mid); 
            } 
            else { 
                if (mid < n - 1 && target < arr[mid + 1]) 
                {
                    system.debug('### line 39 mid :' + mid);
                    return getClosestInteger(arr[mid],  arr[mid + 1], target); 
                }
                i = mid + 1;
                system.debug('### line 43 mid :' + mid); 
            } 
        }   
        return arr.get(mid); 
    } 

    private static Integer getClosestInteger(Integer val1, Integer val2, Integer target) 
    { 
        if (target - val1 >= val2 - target) 
        {
            return val2; 
        }
        else
        {
            return val1; 
        }
    }

    public static Decimal findClosestDecimal(Decimal[] arr, Integer n, Decimal target) 
    { 
        if (target <= arr[0]) 
        {
            return arr[0]; 
        }
        if (target >= arr[n - 1]) 
        {
            return arr[n - 1]; 
        }
        
        Integer i = 0;
        Integer j = n; 
        Integer mid = 0; 
        while (i < j) 
        { 
            mid = (i + j) / 2; 
            if (arr[mid] == target) 
            {
                return arr[mid]; 
            }
            if (target < arr[mid]) 
            {
                if (mid > 0 && target > arr[mid - 1]) 
                {
                    return getClosestDecimal(arr[mid - 1], arr[mid], target); 
                }
                j = mid; 
            } 
            else { 
                if (mid < n - 1 && target < arr[mid + 1]) 
                {
                    return getClosestDecimal(arr[mid],  arr[mid + 1], target); 
                }
                i = mid + 1; 
            } 
        }   
        return arr[mid]; 
    }

    private static Decimal getClosestDecimal(Decimal val1, Decimal val2, Decimal target) 
    { 
        if (target - val1 >= val2 - target) 
        {
            return val2; 
        }
        else
        {
            return val1; 
        }
    }
}