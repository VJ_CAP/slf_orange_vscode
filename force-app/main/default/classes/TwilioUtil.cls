/**
* Description:  This Class is used to Call Twilio 
*
*   Modification Log : 
---------------------------------------------------------------------------
Developer                    Date                Description
---------------------------------------------------------------------------
Adithya                      01/15/2019          Created 
******************************************************************************************/
public without sharing class TwilioUtil 
{
    /**
    * Description: modify Twlio Body
    * 
    * @param newContact     new Draw_Requests__c records List.
    * @param oldConMap      Old Draw_Requests__c records Map.
    */
    public static  void callTwilioWS(String jsonapplicantDrawConMap,string response){
        system.debug('### Entered into callTwilioWS() of '+ TwilioUtil.class);
        try{
            List<Twilio_Templates__c> twlTemplate = [select Id,Reply_Text__c,Response_Text__c from Twilio_Templates__c where Reply_Text__c=: response];
            String stringtemp='';
            
            map<id, Draw_Requests__c> applicantDrawConMap = (Map<id, Draw_Requests__c>) JSON.deserialize(jsonapplicantDrawConMap,Map<id, Draw_Requests__c>.class);
            
            Map<String,String> params = new Map<String,String>();
            SFSettings__c settings = SFSettings__c.getOrgDefaults();
            if(!twlTemplate.isEmpty())
            {
                stringtemp = twlTemplate[0].Response_Text__c;
                system.debug('### stringtemp before'+ stringtemp);
                for(Id idIterate : applicantDrawConMap.keyset())
                {
                    Draw_Requests__c drowReqRec = applicantDrawConMap.get(idIterate);
                    if(stringtemp != null && stringtemp != ''){
                        system.debug('### drowReqRec '+drowReqRec.Primary_Applicant_Email__c);
                        if(stringtemp.contains('[email address]') && null != drowReqRec.Primary_Applicant_Email__c) 
                            stringtemp = stringtemp.replace('[email address]',drowReqRec.Primary_Applicant_Email__c);
                        
                        if(stringtemp.contains('[Contractor/Installer Name]')) 
                            stringtemp = stringtemp.replace('[Contractor/Installer Name]',drowReqRec.Installer_Account_Name__c);
                        
                        if(stringtemp.contains('X,XXX,XX'))
                        {
                            Decimal input = drowReqRec.Combined_Loan_Amount__c;
                            stringtemp = stringtemp.replace('X,XXX,XX',input.format().contains('.')?input.format():(input.format()+'.00'));
                        }
                        if(stringtemp.contains('Y,YYY,YY'))
                        {
                            system.debug('*drowReqRec.Maximum_loan_amount__c***'+drowReqRec.Maximum_loan_amount__c);
                            Decimal input = drowReqRec.Maximum_loan_amount__c;
                            
                            stringtemp = stringtemp.replace('Y,YYY,YY',input.format().contains('.')?input.format():(input.format()+'.00'));
                        }                           
                        
                        if(stringtemp.contains('Z,ZZZ,ZZ'))
                        {
                            Decimal input = drowReqRec.Remaining_amount_for_additional_payment__c;
                            stringtemp = stringtemp.replace('Z,ZZZ,ZZ',input.format().contains('.')?input.format():(input.format()+'.00'));
                        }
                       
                        if(stringtemp.contains('0,000,00'))
                        {
                            if(null != drowReqRec.Total_Amount_Drawn__c)
                            {
                                Decimal input = drowReqRec.Total_Amount_Drawn__c;
                                stringtemp = stringtemp.replace('0,000,00',input.format().contains('.')?input.format():(input.format()+'.00'));
                            }else
                            {
                                stringtemp = stringtemp.replace('0,000,00','0.0');
                            }
                        }
                            
                        system.debug('### Stringtemp after '+ stringtemp);
                    }
                    params = new Map<String,String> {
                    'To'   => '+1'+drowReqRec.Primary_Contact_Number__c,
                    'From' => settings.Twilio_From_Phone_Number__c,
                    'Body' => stringtemp
                    };
                }
            }
            system.debug('### params '+ params);
            
            if(!params.isEmpty())
            {
                invokeTwilioWS(params);
            }
            
        }catch(Exception ex)
        {
            system.debug('### TwilioUtil.callTwilioWS():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('TwilioUtil.callTwilioWS()',ex.getLineNumber(),'callTwilioWS()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from callTwilioWS() of '+ TwilioUtil.class);
    }
    
    
    /**
    * Description: Send SMS to Twilio
    * 
    * @param params     Twilio Perameters Map<String,String>.
    */
    @future(callout=true)
    public static  void invokeTwilioWS(Map<String,String>  params){
        system.debug('### Entered into callTwilioWS() of '+ TwilioUtil.class);
        try{
            if(!params.isEmpty())
            {
                SFSettings__c settings = SFSettings__c.getOrgDefaults();
                String account = settings.Twilio_Account_SID__c;
                String token = settings.Twilio_Auth_Token__c;
                TwilioRestClient client = new TwilioRestClient(account, token);
                system.debug('### params '+ params);
                TwilioMessage message = client.getAccount().getMessages().create(params);
            }
        }catch(Exception ex)
        {
            system.debug('### TwilioUtil.invokeTwilioWS():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('TwilioUtil.invokeTwilioWS()',ex.getLineNumber(),'invokeTwilioWS()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from callTwilioWS() of '+ TwilioUtil.class);
    }
    
    /**
    * Description: Send SMS to Twilio
    * 
    * @param params     Twilio Perameters Map<String,String>.
    */
    public static  void invokeTwilioWSCallOut(Map<String,String>  params){
        system.debug('### Entered into callTwilioWS() of '+ TwilioUtil.class);
        try{
            if(!params.isEmpty())
            {
                SFSettings__c settings = SFSettings__c.getOrgDefaults();
                String account = settings.Twilio_Account_SID__c;
                String token = settings.Twilio_Auth_Token__c;
                TwilioRestClient client = new TwilioRestClient(account, token);
                system.debug('### params '+ params);
                TwilioMessage message = client.getAccount().getMessages().create(params);
            }
        }catch(Exception ex)
        {
            system.debug('### TwilioUtil.invokeTwilioWS():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            
            ErrorLogUtility.writeLog('TwilioUtil.invokeTwilioWS()',ex.getLineNumber(),'invokeTwilioWS()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
        system.debug('### Exit from callTwilioWS() of '+ TwilioUtil.class);
    }
    
    //added by Adithya as part of ORANGE-2641 on 11/5/2019
    //Start
    
    /**
    * Description: Send SMS to Twilio
    * 
    * @param customerNumber   +1XXXXXXXXXX.
    */
    @future(callout=true)
    public static void invokeTwilioHttpCallOut(id accId,string customerNumber){
        SFSettings__c settings = SFSettings__c.getOrgDefaults();
        String account = settings.Twilio_Account_SID__c;
        String token = settings.Twilio_Auth_Token__c;
        System.debug('**account***'+account);
        System.debug('**token***'+token);
        Blob headerValue = Blob.valueOf(account + ':' + token);
        System.debug('**headerValue***'+headerValue);
        System.debug('**EncodingUtil***'+EncodingUtil.base64Encode(headerValue));
        String applicationAuthHeader = 'Basic ' +EncodingUtil.base64Encode(headerValue);
        System.debug('**applicationAuthHeader***'+applicationAuthHeader);

        Http restHttp = new Http();
        HttpRequest restReq = new HttpRequest();
        restReq.setEndpoint('https://lookups.twilio.com/v1/PhoneNumbers/'+customerNumber+'?Type=carrier&Type=caller-name');
        restReq.setMethod('GET');
        restReq.setHeader('Content-Type', 'application/json');
        restReq.setHeader('Authorization', applicationAuthHeader);
        HttpResponse res = restHttp.send(restReq);
        System.debug('*****'+res.getbody());
        String responseBody = '';
        responseBody = res.getBody();
        System.debug('**responseBody****'+responseBody);
        TwilioUtil twilioRespData = (TwilioUtil)JSON.deserialize(responseBody, TwilioUtil.class);
        System.debug('**twilioRespData****'+twilioRespData);
        Account accRec = new Account(id=accId);
        if(null != twilioRespData)
        {
            if(null != twilioRespData.caller_name)
            {
                if(null != twilioRespData.caller_name.caller_name)
                {
                    accRec.Phone_Caller_Name__c = twilioRespData.caller_name.caller_name;
                    System.debug('**caller_name***'+twilioRespData.caller_name.caller_name);
                }
            }
            
            if(null != twilioRespData.carrier)
            {
                if(null != twilioRespData.carrier.type)
                {
                    accRec.Phone_Type__c = twilioRespData.carrier.type;
                    System.debug('**type***'+twilioRespData.carrier.type);
                    if(twilioRespData.carrier.type != 'mobile')
                        accRec.SMS_Opt_in__c = false;
                    
                }
            }
        }
        update accRec;
    }
    
    public String national_format{get;set;}
    public carrier carrier{get;set;}
    public String phone_number{get;set;}
    public String add_ons{get;set;}
    public String country_code{get;set;}
    public String url{get;set;}
    public callerName caller_name{get;set;}
    public class carrier{
        public String mobile_country_code{get;set;}
        public String mobile_network_code{get;set;}
        public String name{get;set;}
        public String error_code{get;set;}
        public String type{get;set;}
    }
    public class callerName{
        public String error_code{get;set;}
        public String caller_type{get;set;}
        public String caller_name{get;set;}
    }
    //End
}