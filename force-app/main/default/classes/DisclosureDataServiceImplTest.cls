/******************************************************************************************
* Description: Test class to cover DisclosureDataServiceImpl Class
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Sejal Bhatt         06/11/2011          Updated
******************************************************************************************/
@isTest
public class DisclosureDataServiceImplTest {    
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
        
    }
 
 
  /**
*
* Description: This method is used to cover DisclosureServiceImpl and DisclosureDataServiceImpl 
*
*/   
    private testMethod static void disclosureTest(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account instlrAccObj = [select id, Credit_Waterfall__c, Risk_Based_Pricing__c from Account 
                                                where id=:loggedInUsr[0].contact.AccountId];
        instlrAccObj.Credit_Waterfall__c = true;
        instlrAccObj.Risk_Based_Pricing__c = true;
        update instlrAccObj;
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        
        Product__c prod = [select Id,Name,Product_Tier__c,Term_mo__c,APR__c from Product__c where Name =: 'testprod' LIMIT 1];
          prod.Installer_Account__c = acc.id;
            prod.State_Code__c = 'CA';
            prod.Is_Active__c = true;
            prod.Product_Tier__c = '1';
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            prod.Term_mo__c = 120;
            update prod;
        
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        opp.Hash_Id__c = loggedInUsr[0].Hash_Id__c;
        opp.isACH__c = true;
        opp.SLF_Product__c = prod.id;
        opp.Install_State_Code__c = 'CA';
        update opp;
        

        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1];
        
        
        //
        String disclosure ;
        Map<String, String> disclosureMap = new Map<String, String>();
        SLF_Error_Codes__c errCodObj = new SLF_Error_Codes__c(Name = '202',Error_Message__c = 'Either Id or externalId or hashId is not valid and does not match with our system.');
        insert errCodObj;
        
        
        
        System.runAs(loggedInUsr[0])
        {
            Test.starttest();
            Disclosures__c disclr = new Disclosures__c();
            disclr.Type__c = 'RBP';
            disclr.Active__c= true;
            disclr.Name ='Test';
            disclr.Disclosure_Text__c ='You are applying for a loan with an Annual Percentage Rate between X.XX% and Y.YY% and a term between AA years and BB years. The terms you may qualify for are based on your credit worthiness.';
            insert disclr;
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
          
            
            //
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            //
            disclosure=  '{ "projects": [ {"id": "'+acc.id+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            //
            disclosure=  '{ "projects": [ {"externalId": "'+opp.Reference_Key__c+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ {"externalId": "'+opp.id+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'","externalId": "'+opp.id+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ {"externalId": "233||wer", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ {"hashId":"'+opp.Hash_Id__c+'","disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ {"id": "","externalId": "","hashId":"","disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');          
            
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'","disclosures":[ { "type":"RBPetewer" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');            
            
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'", "disclosures":[ { "type":"" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'", "disclosures":[ {"ertr" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ { "id": "'+opp.id+'" } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": "wer" }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            disclosure=  '{ "projects": [ { 45er} ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');   
          
            Test.stoptest();
        }
        disclosure=  '{ "projects": [ {"externalId": "233||wer", "disclosures":[ { "type":"RBP" } ] } ] }';
        MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
    }
    
    
    private testMethod static void disclosureTest1(){
        
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        Account instlrAccObj = [select id, Credit_Waterfall__c, Risk_Based_Pricing__c from Account 
                                                where id=:loggedInUsr[0].contact.AccountId];
        instlrAccObj.Credit_Waterfall__c = true;
        instlrAccObj.Risk_Based_Pricing__c = true;
        update instlrAccObj;
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        
        Product__c prod = [select Id,Name,Product_Tier__c,Term_mo__c,APR__c from Product__c where Name =: 'testprod' LIMIT 1];
        prod.Installer_Account__c = acc.id;
            prod.State_Code__c = 'CA';
            prod.Is_Active__c = true;
            prod.Product_Tier__c = '1';
            prod.ACH__c = true;
            prod.Product_Loan_Type__c = 'HII';
            prod.Internal_Use_Only__c = false;
            prod.Term_mo__c = 120;
            update prod;
        
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,AccountId,Reference_Key__c,
                           SLF_Product__c,Install_State_Code__c,ACH__c,Product_Loan_Type__c
                           from opportunity where AccountId =: acc.Id LIMIT 1]; 
        opp.isACH__c = true;
        opp.SLF_Product__c = prod.id;
        opp.Install_State_Code__c = 'CA';
        update opp;
       
        
        SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1];
       
        opp.Hash_Id__c = loggedInUsr[0].Hash_Id__c;
        opp.Project_Category__c = 'Home';
        update opp;
       
        //
        String disclosure ;
        Map<String, String> disclosureMap = new Map<String, String>();
        SLF_Error_Codes__c errCodObj = new SLF_Error_Codes__c(Name = '202',Error_Message__c = 'Either Id or externalId or hashId is not valid and does not match with our system.');
        insert errCodObj;
        
        
        
        System.runAs(loggedInUsr[0])
        {
            Test.starttest();
             Disclosures__c disclr = new Disclosures__c();
            disclr.Type__c = 'RBP';
            disclr.Active__c= true;
            disclr.Name ='Test';
            disclr.Disclosure_Text__c ='AA year loan with a X.XX% Annual Percentage Rate<br />BB year loan with a Y.YY% Annual Percentage Rate';
            insert disclr;
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
            
            
            
            //
            disclosure=  '{ "projects": [ {"id": "'+opp.id+'", "disclosures":[ { "type":"RBP" } ] } ] }';
            MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
             Test.stoptest();
        }
        disclosure=  '{ "projects": [ {"externalId": "233||wer", "disclosures":[ { "type":"RBP" } ] } ] }';
        MapWebServiceURI(disclosureMap,disclosure,'getdisclosure');
    }
      /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}