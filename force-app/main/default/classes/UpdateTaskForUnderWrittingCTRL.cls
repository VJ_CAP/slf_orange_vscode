/* Author : Veereandranath Jalla
* Date   : 11/14/2018
* Description: This class is used to create a task and invoked from Skuid page
*
*/
/*-------------------------------------------------------------------------------
*  Initial Version Written By           Date Modified By      Description
*  Veereandranath Jalla                 11/14/2018            Initial version
*/ 
global class UpdateTaskForUnderWrittingCTRL {
    global class Input {
        @InvocableVariable(required=true) global Id strUnderWrittingId;
        global Input() {
            //  nothing to do
        }
        global Input(Id strUnderWrittingId) {
            this.strUnderWrittingId = (String.valueOf(strUnderWrittingId)).substring(0,15);
        }
    }
    global class Output {   
        @InvocableVariable global String Message;
    }
    public class TaskException extends Exception{}
    
        
    @InvocableMethod(label='UpdateTaskStatus')
    public static List<Output> UpdateTaskStatus(List<Input> lstInput){
        List<Output> lstOutPuts = new List<Output>();  
        Output objOutput = new Output();    
        Set<Id> UnderWrittingIds = new Set<Id>();
        List<Task> taskList = new List<Task>();
        List<Stipulation__c> stipList = new List<Stipulation__c>();
        Opportunity opp = new Opportunity();
        
        for(input objI:lstInput){
            UnderWrittingIds.add(objI.strUnderWrittingId);
        }
        
        Underwriting_File__c objUWFile = [select Id,Project_Status__c,Opportunity__c,Account_Name__c,M0_Ready_for_Review_Date_Time__c,
                                          M0_Ready_for_Review_Age__c,M1_Ready_for_Review_Date_Time__c,M1_Ready_for_Review_Age__c,
                                          (select id,Name,Description__c,Status__c,Completed_Date_Time__c,Suppress_Completed_Email_Alert__c from Stipulations__r where Status__c != 'Completed' and (Stipulation_Data__r.Name = 'APR' or Stipulation_Data__r.Name = 'ACH')),
                                          (select Id,OwnerId,Owner.Name from Tasks where whatid in:UnderWrittingIds and status = 'Open'),
                                         Opportunity__r.isACH__c, Confirm_ACH_Information__c
                                          from Underwriting_File__c where Id in:UnderWrittingIds limit 1];
    

    
        
        if(objUWFile != null){
            //Added as a part of Orange-2040 - Start
                      if(!objUWFile.Stipulations__r.isEmpty() && objUWFile.Project_Status__c == 'M1') // Added '&&' condition as part of 14705  
                       {
                  for(Stipulation__c stip:objUWFile.Stipulations__r )    { 
                         stip.Status__c = 'Completed';
                         stip.Suppress_Completed_Email_Alert__c = true; //Added by Adithya as part of 14701.
                         objOutput.Message = 'Auto-closed due to M1 approval';
                         //lstOutPuts.add(objOutput);
                        stipList.add(stip);
                        }
                       } 
                   /*if(objUWFile.Opportunity__r.isACH__c == true && objUWFile.Confirm_ACH_Information__c == 'ACH data Not Confirmed')
                       objUWFile.Opportunity__r.isACH__c = false;
                   else if(objUWFile.Opportunity__r.isACH__c == false && objUWFile.Confirm_ACH_Information__c == 'ACH data Confirmed')
                       objUWFile.Opportunity__r.isACH__c = true;*/
                     
                   opp = objUWFile.Opportunity__r;
            //Added as a part of Orange-2040 - End
            if(!objUWFile.Tasks.isEmpty()){
                
                for(Task objTask :objUWFile.Tasks){
                    objTask.Review_Date_Time_Complete__c = system.now();
                    objTask.Status = 'Complete - Approved';
                    taskList.add(objTask);
                }
            }
             
            try{
                if(!stipList.isEmpty())
                    update stipList;
                if(!taskList.isEmpty())
                    update taskList;
                update opp;
              
                lstOutPuts.add(objOutput);
                return lstOutPuts;
            }Catch(Exception ex){
                objOutput.Message = ex.getMessage();
                lstOutPuts.add(objOutput);
                return lstOutPuts;
            }
        }
        return  null;        
    }
}