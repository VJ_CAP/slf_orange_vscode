public without sharing class CreditApprovalAttachmentController {

    public SLF_Credit__c crdtObj{get;set;}
    public Id creditObjId;
    
    public CreditApprovalAttachmentController(){
        System.debug('ApexPages.currentPage():'+ApexPages.currentPage());
        if(ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().get('Id') != null){
            creditObjId = ApexPages.currentPage().getParameters().get('id');
            System.debug('creditObjId :'+creditObjId);
            crdtObj = new SLF_Credit__c ();
            crdtObj = [Select id, Opportunity__c ,Primary_Applicant_FICO__c,Co_Applicant_FICO__c,Credit_Submission_Date__c,Co_Key_Factor_1__c,Co_Key_Factor_2__c,Co_Key_Factor_3__c,
            Co_Key_Factor_4__c,Key_Factor_1__c,Key_Factor_2__c,Key_Factor_3__c,Key_Factor_4__c from SLF_Credit__c where id = :creditObjId limit 1];
            
        }
    }
    @future(callout=true)
    public static void sendAttachment(Id creditId, Id oppId){
        System.debug('creditId:'+creditId);
        System.debug('oppId:'+oppId);
        
        Opportunity oppObj = new Opportunity();
        oppObj = [Select id, Co_Applicant__c, AccountId from Opportunity where id =:oppId];
        
        List<Attachment> attachList = new List<Attachment>();
        if(oppObj.AccountId != null){
            pagereference appPdf = Page.CreditApprovalAttachmentPage;
            appPdf.getParameters().put('Id',creditId);
            
            if(appPdf != null){
                Blob bodyApp;
                if(Test.isRunningTest()) { 
                    bodyApp = blob.valueOf('Unit.Test');
                } else {
                    bodyApp = appPdf.getContentAsPDF();
                }
                
                attachList.add(new Attachment(
                    Name = 'Pricing Disclosure_Applicant ' + (Datetime.now().format('yyyyMMdd_hhmmss')) + '.pdf',
                    ContentType = 'text/html',
                    Body = bodyApp,
                    ParentId = oppId 
                ));
            }
        }
        
        if(oppObj.Co_Applicant__c != null){
            pagereference coAppPdf = Page.CreditApprovalAttachmentPageCoApp;               
            coAppPdf.getParameters().put('Id',creditId);
            
            if(coAppPdf != null){
                Blob bodyCoApp;
                if(Test.isRunningTest()) { 
                    bodyCoApp = blob.valueOf('Unit.Test');
                } else {
                    bodyCoApp = coAppPdf.getContentAsPDF();
                }
                
                attachList.add(new Attachment(
                    Name = 'Pricing Disclosure_Co Applicant ' + (Datetime.now().format('yyyyMMdd_hhmmss')) + '.pdf',
                    ContentType = 'text/html',
                    Body = bodyCoApp,
                    ParentId = oppId 
                ));
            }
        }    
        
        if (attachList.size() > 0){
            insert attachList;
        }        
    }
}