public with sharing class UploadPaymentsController 
{
	class Row
	{
		Integer num;
		Date transactionDate;
		Decimal total;
		Decimal m0;
		Decimal m1;
		Decimal m2;
		Decimal incentive;
		Decimal m0Payback;
		Decimal m0Netout;
		Decimal cancellation;
		String id;

		Row (Integer num)
		{
			this.num = num;
		}
	}

	class Stats
	{
		public String transactionType {get;set;}
		public Integer transactions {get;set;}
		public Decimal totalAmount {get;set;}

		Stats (String transactionType)
		{			
			this.transactionType = transactionType;
			this.transactions = 0;
			this.totalAmount = 0;
		}
	}

//  ***************************

	list<Underwriting_Transaction__c> transactions;
	Reconciliations_Management__c rm;

	public map<String,Stats> transactionTypeStatsMap {get;set;}
	public list<Stats> transactionTypeStats {get;set;}
	public String debug {get;set;}


	Date rowDate = Date.newInstance(2000,1,1);

	public Date latestDateProcessed {get;set;}
	public list<String> errors {get;set;}
	public Blob inputFileContent {get;set;}
	public String inputFileName {get;set;}	
	public Boolean noErrors {get;set;}
	public Boolean saveDisabled {get;set;}
	public Boolean validateDisabled {get;set;}

//  ***************************

	public UploadPaymentsController () 
	{
		this.inputFileContent = null;

		this.rm = Reconciliations_Management__c.getOrgDefaults();
		this.latestDateProcessed = rm.Latest_Date_Processed__c;

		if (this.latestDateProcessed == null)
		{
			this.latestDateProcessed = this.rowDate;
		}

		this.noErrors = true;
		this.saveDisabled = true; 
		this.validateDisabled = false; 
	}

//  ***************************

	public PageReference validate ()
	{
		validatePayments ();
		return null;
	}

//  ***************************

	public PageReference save ()
	{
		savePayments ();
		return null;
	}

//  ***************************

	public void validatePayments ()

	//  this method processes the CSV data in inputFileContent, validates each row, 
	//  and generates either error messages or Underwriting_Transaction__c records.

	{
		//  reset transactions and errors

		if (this.inputFileContent == null)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Choose a file to upload'));
			return;
		}

		this.transactions = new list<Underwriting_Transaction__c>{};
		this.errors = new list<String>{};

		this.transactionTypeStatsMap = new map<String,Stats>{};

		this.transactionTypeStatsMap.put('Installer M0 Payment Paid', new Stats ('M0'));
		this.transactionTypeStatsMap.put('Installer M1 Payment Paid', new Stats ('M1'));
		this.transactionTypeStatsMap.put('Installer M2 Payment Paid', new Stats ('M2'));
		this.transactionTypeStatsMap.put('Incentive Payment Paid', new Stats ('Incentive'));
		this.transactionTypeStatsMap.put('Installer M0 Payback Paid', new Stats ('M0 Payback'));
		this.transactionTypeStatsMap.put('Installer M0 Net Out Paid (Taken)', new Stats ('M0 Net Out'));

		//  split the input CSV file into rows

		list<String> inputRows;

		try
		{
			inputRows = this.inputFileContent.toString().split('\n');
		}
		catch (Exception e)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Upload file must be in CSV format'));
			return;
		}

		//  build a list of Row objects from each row in the file

		list<Row> rows = new list<Row>{};

		Integer num = 1;

		for (String row : inputRows)
		{
			Row r = new Row (num++);
			row = row.trim();
			list<String> roughColumns = row.split(',');

			list<String> columns = new list<String>{};

			//  fudge to handle commas within currency fields in double quotes

			String save;
			for (String s : roughColumns)
			{
				if (s.startswith('"') && !s.endswith('"')) 
				{
					save = s;
				}
				else
				if (s.endswith('"') && !s.startswith('"')) 
				{
					save = save + s;
					columns.add (save);
				}
				else
				{
					columns.add (s);
				}
			}

			//  must have 10 columns

			if (columns.size() < 10)
			{
				this.errors.add ('Row ' + r.num + ': not enough columns');
				continue;
			}

			//  populate the Row object with good values or null

			r.transactionDate = getDate (columns[0]);
			r.total = getDecimal (columns[1]);
			r.m0 = getDecimal (columns[2]);
			r.m1 = getDecimal (columns[3]);
			r.m2 = getDecimal (columns[4]);
			r.incentive = getDecimal (columns[5]);
			r.m0Payback = getDecimal (columns[6]);
			r.m0Netout = getDecimal (columns[7]);
			r.cancellation = getDecimal (columns[8]);
			r.id = columns[9];

			if (r.transactionDate == null)
			{
				this.errors.add ('Row ' + r.num + ': invalid or missing date: ' + columns[0]);
				continue;
			}

			if (r.m0 == null && columns[2].length() > 0)
			{
				this.errors.add ('Row ' + r.num + ': M0 invalid amount : ' + columns[2]);
				continue;
			}

			if (r.m1 == null && columns[3].length() > 0)
			{
				this.errors.add ('Row ' + r.num + ': M1 invalid amount : ' + columns[3]);
				continue;
			}

			if (r.m2 == null && columns[4].length() > 0)
			{
				this.errors.add ('Row ' + r.num + ': M2 invalid amount : ' + columns[4]);
				continue;
			}

			if (r.incentive == null && columns[5].length() > 0)
			{
				this.errors.add ('Row ' + r.num + ': Incentive invalid amount : ' + columns[5]);
				continue;
			}

			if (r.m0Payback == null && columns[6].length() > 0)
			{
				this.errors.add ('Row ' + r.num + ': M0 Payback invalid amount : ' + columns[6]);
				continue;
			}

			if (r.m0Netout == null && columns[7].length() > 0)
			{
				this.errors.add ('Row ' + r.num + ': M0 Net Out invalid amount : ' + columns[7]);
				continue;
			}

			if (r.transactionDate <= this.rm.Latest_Date_Processed__c)
			{
				this.errors.add ('Row ' + r.num + ': transactions have already been processed to : ' + this.rm.Latest_Date_Processed__c.format());
				continue;
			}

			if (r.transactionDate > this.rowDate) this.rowDate = r.transactionDate;

			rows.add (r);
		}

		set<String> ids = new set<String>{};

		for (Row r : rows)
		{
			if (r.Id.startswith('006') && r.Id.length() == 15)  // normalize to 18-character form of Id
			{
				try
				{
					Id x = r.Id;
					r.Id = x;
				}
				catch (Exception e) {}
			}
			ids.add (r.id);
		}

		//  build a map of UFs keyed by Sighten_UUID__c referenced in the Rows

		map<String,Underwriting_File__c> ufsMap = new map<String,Underwriting_File__c>{};

		for (Underwriting_File__c uf : [SELECT Id, Opportunity__r.Sighten_UUID__c
										FROM Underwriting_File__c
										WHERE Opportunity__r.Sighten_UUID__c IN :ids])
		{
			ufsMap.put (uf.Opportunity__r.Sighten_UUID__c, uf);
		}

		for (Underwriting_File__c uf : [SELECT Id, Opportunity__c
										FROM Underwriting_File__c
										WHERE Opportunity__c IN :ids])
		{
			ufsMap.put (uf.Opportunity__c, uf);
		}

		//  generate transactions for each Row

		for (Row r : rows)
		{
			Underwriting_File__c uf = ufsMap.get (r.id);

			if (uf == null)
			{
				this.errors.add ('Row ' + r.num + ': Underwriting File not found for Id ' + r.id);
				continue;
			}

			if (r.m0 != null)
			{
				this.transactions.add ( buildTransaction (r, uf, 'Installer M0 Payment Paid', r.m0));
			}

			if (r.m1 != null)
			{
				this.transactions.add ( buildTransaction (r, uf, 'Installer M1 Payment Paid', r.m1));
			}

			if (r.m2 != null)
			{
				this.transactions.add ( buildTransaction (r, uf, 'Installer M2 Payment Paid', r.m2));
			}

			if (r.incentive != null)
			{
				this.transactions.add ( buildTransaction (r, uf, 'Incentive Payment Paid', r.incentive));
			}

			if (r.m0Payback != null)
			{
				this.transactions.add ( buildTransaction (r, uf, 'Installer M0 Payback Paid', r.m0Payback));
			}

			if (r.m0Netout != null)
			{
				this.transactions.add ( buildTransaction (r, uf, 'Installer M0 Net Out Paid (Taken)', r.m0Netout));
			}
		}

		this.noErrors = (this.errors.isEmpty());
		this.saveDisabled = !this.noErrors;

		if (this.noErrors)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File has no errors; ' + 
																				this.transactions.size() +
																				' payment transactions will be uploaded - click Upload Payments'));
		}

		this.transactionTypeStats = transactionTypeStatsMap.values();

		System.debug(rows);
		System.debug(this.errors);
		System.debug (this.transactions);
	}

//  ***************************

	private void savePayments ()
	{
		insert this.transactions;

		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, this.transactions.size() +
																				' payment transactions were uploaded OK'));

		this.rm.Latest_Date_Processed__c = this.rowDate;
		update this.rm;

		this.latestDateProcessed = rm.Latest_Date_Processed__c;

		this.inputFileContent = null;
		this.saveDisabled = true;
		this.validateDisabled = false;
		this.transactions = new list<Underwriting_Transaction__c>{};
		this.errors = new list<String>{};
		this.transactionTypeStats = new list<Stats>{};
	}

//  ***************************

	private Underwriting_Transaction__c buildTransaction (Row r, Underwriting_File__c uf, String transactionType, Decimal amount)

	// helper method builds an Underwriting_Transaction__c and adds it to the Stats

	{
		Underwriting_Transaction__c t = new Underwriting_Transaction__c ();
		t.Underwriting_File__c = uf.Id;
		t.Type__c = transactionType;
		t.Amount__c = amount;
		t.Transaction_Date__c = r.transactionDate;
		t.Test_Only__c = true;

		Stats s = this.transactionTypeStatsMap.get(transactionType);
		if (s != null)
		{
			s.transactions++;
			s.totalAmount += t.Amount__c;
		}
		return t;
	}

//  ***************************

	private Date getDate (String value)

	//  helper method validates a text date in the format mm/dd/yy or mm/dd/yyyy and if OK returns a Date object instance
	//  if not OK returns null

	{
		if (value == null || value.length() == 0) return null;
		list<String> parts = value.split('/');
		if (parts.size() != 3) return null;
		if (parts[2].length() == 2) parts[2] = '20' + parts[2];

		Date d = null;

		try
		{
			d = Date.newInstance (Integer.valueOf(parts[2]),Integer.valueOf(parts[0]),Integer.valueOf(parts[1]));
		}
		catch (Exception e) {}

		return d;
	}

//  ***************************

	private Decimal getDecimal (String value)

	//  helper method validates a text currency in the format "$nn,nnn.mm" and if OK returns a Decimal object instance
	//  if not OK returns null

	{
		if (value == null || value.length() == 0) return null;
		Decimal d = null;
		try
		{
			String s = value.remove('$').remove('"').trim();
			if (s.contains('('))
			{
				s = '-' + s.remove('(').remove(')');
			}
			d = Decimal.valueOf (s);
		}
		catch (Exception e) {}

		return d;
	}
}