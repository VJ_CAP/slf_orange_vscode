public class TCUMemberShipCtrl {
    
    @AuraEnabled
    public static List<Underwriting_File__c> getMemberShipData(Id fundingSnapShotId){
        List<Funding_Snapshot_Line_Item__c> funLineItems = [select id, Underwriting_File__c from Funding_Snapshot_Line_Item__c where Funding_Snapshot__c =: fundingSnapShotId];
        Set<Id> uwIds = new Set<Id>();        
        for(Funding_Snapshot_Line_Item__c fsli: funLineItems)   {
            uwIds.add(fsli.Underwriting_File__c);
        } 	
        List<Underwriting_File__c> uwFiles = [select id,BorrowerStreet__c,BorrowerCity__c,BorrowerState__c,BorrowerZip__c,CoBorrowerStreet__c,
                                              CoBorrowerCity__c,CoBorrowerState__c,CoBorrowerZip__c,CoBorrower_Citizenship__c,OpportunityID18Char__c,
                                              Opportunity__r.Account.FirstName,Opportunity__r.Account.MiddleName,Opportunity__r.Account.LastName,
                                              Opportunity__r.Account.PersonHomePhone,Opportunity__r.Account.PersonOtherPhone,Opportunity__r.Account.PersonMobilePhone,
                                              Opportunity__r.Account.PersonEmail,Opportunity__r.Account.ID_Type__c,Opportunity__r.Account.ID_Number__c,
                                              Opportunity__r.Account.ID_Issuer__c,Opportunity__r.Account.ID_Issue_Date__c,Opportunity__r.Account.ID_Expiration_Date__c,
                                              Opportunity__r.Account.Maiden_Name__c,Opportunity__r.Account.PersonBirthdate,Opportunity__r.Account.SSN__c,Opportunity__r.Account.Citizenship__c,
                                              Opportunity__r.Account.Employer_Name__c,Opportunity__r.Account.Job_Title__c,Opportunity__r.Account.PersonMailingStreet,
                                              Opportunity__r.Account.PersonMailingCity,Opportunity__r.Account.PersonMailingState,Opportunity__r.Account.PersonMailingPostalCode,
                                              Opportunity__r.Co_Applicant__r.FirstName,Opportunity__r.Co_Applicant__r.MiddleName,Opportunity__r.Co_Applicant__r.LastName,
                                              Opportunity__r.Co_Applicant__r.PersonHomePhone,Opportunity__r.Co_Applicant__r.PersonOtherPhone,Opportunity__r.Co_Applicant__r.PersonMobilePhone,
                                              Opportunity__r.Co_Applicant__r.PersonEmail,Opportunity__r.Co_Applicant__r.ID_Type__c,Opportunity__r.Co_Applicant__r.ID_Number__c,
                                              Opportunity__r.Co_Applicant__r.ID_Issuer__c,Opportunity__r.Co_Applicant__r.ID_Issue_Date__c,Opportunity__r.Co_Applicant__r.ID_Expiration_Date__c,
                                              Opportunity__r.Co_Applicant__r.PersonBirthdate,Opportunity__r.Co_Applicant__r.Maiden_Name__c,Opportunity__r.Co_Applicant__r.SSN__c,
                                              Opportunity__r.Co_Applicant__r.Employer_Name__c,Opportunity__r.Co_Applicant__r.Job_Title__c,Opportunity__r.Sighten_UUID__c
                                              from Underwriting_File__c where Id in: uwIds ORDER BY OpportunityID18Char__c];
        
        for(Underwriting_File__c uwFile: uwFiles)	{
            if(uwFile.Opportunity__r.AccountId !=null)
                uwFile.Opportunity__r.Account.SSN__c = 'XXX-XX-'+uwFile.Opportunity__r.Account.SSN__c.right(4);
            if(uwFile.Opportunity__r.Co_Applicant__r != null)
                uwFile.Opportunity__r.Co_Applicant__r.SSN__c = 'XXX-XX-'+uwFile.Opportunity__r.Co_Applicant__r.SSN__c.right(4);
        }
        
        return uwFiles;
    }
    
}