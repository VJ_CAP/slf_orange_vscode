/**
* Description: Pricing Calculation related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/
public with sharing class PricingServiceImpl extends RESTServiceBase{  
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back with data and response status code.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + PricingServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        PricingDataServiceImpl pricingDataSrvImpl = new PricingDataServiceImpl(); 
         IRestResponse iRestRes;
        try{ 
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            //CreateProjectRequestWrapper reqData = (CreateProjectRequestWrapper)JSON.deserialize(rw.reqDataStr, CreateProjectRequestWrapper.class);
            res.response = pricingDataSrvImpl.transformOutput(reqData);

        }catch(Exception err){
            system.debug('### PricingServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' PricingServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            UnifiedBean pricingbean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214',null);
            errorWrapperLst.add(errorWrapper);
            pricingbean.error  = errorWrapperLst ;
            pricingbean.returnCode = '214';
            iRestRes = (IRestResponse)pricingbean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + PricingServiceImpl.class);
        return res.response ;
    }
}