global with sharing class BatchRebuildOpportunityShares 
                            implements Database.Batchable<SObject>,
                                        Database.Stateful,
                                        Schedulable                                       
{
    class Parameters
    {
        String objectType = 'Campaign';
        Boolean updateDatabase = true;
        String oneId = null;
    }

    Boolean testMode = Test.isRunningTest();
    Parameters params = new Parameters();
//    Job__c job;

    Integer sharesInserted = 0;
 
//****************************************************************** 

    global BatchRebuildOpportunityShares ()
    {
        // JobSupport.submitJob('BatchRebuildPartnerShares', '');
        // JobSupport.submitJob('BatchRebuildPartnerShares', '{"objectType":"Account","updateDatabase":false}');
    }

//****************************************************************** 

    global void execute (SchedulableContext sc)
    {
		Database.executeBatch (new BatchRebuildOpportunityShares(), 50);
    }

//******************************************************************

    global Database.QueryLocator start (Database.BatchableContext bc)
    {       
/*
        this.job = JobSupport.getJobForId (bc.getJobId());
        JobSupport.addLineToJobLog (this.job, 'Started', true);
        update this.job;

        if (this.job.Parameters__c != null)
        {
            this.params = (Parameters) JSON.deserialize (this.job.Parameters__c, Parameters.class);
        }

        String oneId = this.params.oneId;

        System.debug(this.params);

        JobSupport.addLineToJobLog (this.job, 'Parameters: ' + this.params, false);

        String objectType = this.params.objectType;
*/
        String query = 'SELECT Id, Name, ' +
                                'AccountId, ' +
                                'Installer_Account__c, Co_Applicant__c FROM Opportunity' +                 
                			(this.testmode ? ' LIMIT 10' : '');
                
        Database.Querylocator ql = Database.getQueryLocator (query); 
        
        return ql;
    }
    
//******************************************************************

    global void execute (Database.BatchableContext bc, list<SObject> objects) 
    {       
/*
        Batch__c batch = new Batch__c (Job__c = this.job.Id);        
        JobSupport.addLineToBatchLog (batch, 'Started', true);

        insert batch;

        JobSupport.setCurrentBatch (batch);

        Savepoint sp = Database.setSavepoint();
*/
        map<Id,SObject> objectsMap = new map<Id,SObject>{};

        for (SObject o : objects)
        {
        	objectsMap.put (o.Id, o);
        }

        OpportunitySharingSupport.afterInsert (objectsMap);
/*       
        if (this.params.updateDatabase == false)  Database.rollback(sp);

        insert JobSupport.getBatchLogs();

        this.sharesInserted += JobSupport.getBatchLogs().size();

        JobSupport.addLineToBatchLog (batch, 'Finished', true);

        update batch;
*/
    }

//******************************************************************
    
    global void finish (Database.BatchableContext bc)
    {
/*
        JobSupport.addLineToJobLog (this.job, 'Batch Log records = ' + this.sharesInserted, false);    
        JobSupport.addLineToJobLog (this.job, 'Finished', true);    
        update this.job;        
*/
    }   

//******************************************************************

}