/**
* Description: Payment Approval Validation logic
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suresh Kumar            12/19/2018          Created
******************************************************************************************/
public with sharing class PaymentApprovalServiceImpl extends RESTServiceBase{  
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back with data and response status code.
    */
    public override IRestResponse fetchData(RequestWrapper rw){
        system.debug('### Entered into DrawRequestUtil.fetchData() of '+PaymentApprovalServiceImpl.class);
        //UnifiedBean reqData = (UnifiedBean)stsDtObj;
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        UnifiedBean unifiedRespBean = new UnifiedBean(); 
        unifiedRespBean.error = new list<UnifiedBean.errorWrapper>();
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            string errMsg = '';
        IRestResponse iRestRes;
        Savepoint sp = Database.setSavepoint();
        try{
            List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
            List<Opportunity> opprLst = new List<Opportunity>();
            //sInstallerId = loggedInUsr.get(0).contact.AccountId;
            //sLoggedInUser = loggedInUsr.get(0).Id;
            
            if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
            {                 
                errMsg = ErrorLogUtility.getErrorCodes('203',null);
                unifiedRespBean.returnCode = '203';                
            } 
            else if(
                (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
            {
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                unifiedRespBean.returnCode = '201';
            }
            else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
            {
                opprLst = [SELECT Id,Installer_Account__c FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                
                if (opprLst==null || opprLst.size() == 0)
                {
                    unifiedRespBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            }
            else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
            {
                String externalId;
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                    }else{
                        unifiedRespBean.returnCode = '400';
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                    }
                }
                if(String.isBlank(errMsg)){
                    opprLst = [select id,StageName,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        unifiedRespBean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                }
            }
            else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
            {
                opprLst = [select id,StageName,Hash_Id__c from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                if (opprLst==null || opprLst.size() == 0) {
                    unifiedRespBean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            }
            
            if(string.isNotBlank(errMsg))
            {
                //Database.rollback(sp);
                iolist = new list<DataValidator.InputObject>{};
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                errMsg = errorMsg.errorMessage;
                unifiedRespBean.error.add(errorMsg);
                iRestRes = (IRestResponse)unifiedRespBean;
                return iRestRes;
            }
            
            else{
                if(reqData.projects[0].draws!= null){
                    Draw_Requests__c drawReqUpdate = new Draw_Requests__c();
                    List<Draw_Requests__c> updateDrawRequestList = new List<Draw_Requests__c>();
                    
                    set<string> drawReqHashIdSet = new set<string>();
                    System.debug('******Project Related Draws*****'+reqData.projects[0].draws);
                    for(UnifiedBean.DrawRequestWrapper drawRequestIterate : reqData.projects[0].draws)
                    {
                        if(string.isNotBlank(drawRequestIterate.hashId))
                        {
                            drawReqHashIdSet.add(drawRequestIterate.hashId);    
                        }
                        
                    }
                    System.debug('****Draw Request Id Set***'+drawReqHashIdSet);
                    //SOQL to fetch Draw Request records based up on hash ID 
                    List<Draw_Requests__c> drawReqLst = [select id,Primary_Applicant_Email__c,SMS_Opt_in__c,Total_Amount_Drawn__c,Maximum_loan_amount__c,
                            Remaining_amount_for_additional_payment__c,Installer_Account_Name__c,Sales_Rep_Name__c,Customer_Name__c,
                            Installer_Email__c,Operations_Email__c,Opp_Hash_Id__c,Underwriting__r.opportunity__r.Installer_Account__r.Installer_Email__c,Primary_Contact__c,
                            Underwriting__r.opportunity__r.Installer_Account__r.Operations_Email__c,Underwriting__r.opportunity__r.Hash_Id__c,Underwriting__r.opportunity__c,Hash_Id__c,
                            Underwriting__r.Total_Amount_Drawn__c,Underwriting__r.opportunity__r.Account.Name,
                            Underwriting__r.Opportunity__r.Owner.Name,Underwriting__r.opportunity__r.Combined_Loan_Amount__c,
                            Underwriting__r.opportunity__r.Installer_Account__r.Name,
                            Underwriting__r.Opportunity__r.Primary_Contact__r.Phone,Underwriting__r.Opportunity__r.Primary_Applicant_Email__c,
                            Underwriting__r.Opportunity__r.CoApplicant_Contact__c,Underwriting__r.Amount_Remaining__c,
                            Underwriting__r.Opportunity__r.Primary_Contact__c,Status__c,Amount__c,Approval_Date__c,Decline_Date__c,Draw_Methodology__c,Level__c,Primary_Contact_Number__c,
                            Project_Complete_Certification__c,Reminder_Sent__c,Request_Date__c,Requester__c,Text_Approval_Sent__c,Underwriting__c,
                            Underwriting__r.opportunity__r.Account.SMS_Opt_in__c FROM Draw_Requests__c where Hash_Id__c IN: drawReqHashIdSet];
                    
                   // set<id> UWIdSet = new set<id>();
                    System.debug('***Draw Request List***'+drawReqLst);
                    Map<string,Draw_Requests__c> drawReqMap = new Map<string,Draw_Requests__c>();//key as hash Id and value as Draw Request
                    if(!drawReqLst.isEmpty())
                    {
                        for(Draw_Requests__c drawRequestIterate : drawReqLst){
                            drawReqMap.put(drawRequestIterate.Hash_Id__c,drawRequestIterate);
                            //UWIdSet.add(drawRequestIterate.Underwriting__c);
                        }
                    }
                    /*
                    Decimal sumDrawReq = 0.0;
                    //Ravi: To update the Draw Request with Total Amount Drawn and Remaining Amount For Additional Payment as part of ORANGE-3386
                    List<AggregateResult> drawReqGroupedResults = [select sum(Amount__c) from Draw_Requests__c where Status__c =: 'Approved' AND Underwriting__c IN: UWIdSet];
                    if(!drawReqGroupedResults.isEmpty())
                    {
                        for (AggregateResult agrResultIterate : drawReqGroupedResults)  {
                            sumDrawReq =  (Decimal)agrResultIterate.get('expr0');
                        }
                    }
                    if(null != sumDrawReq)
                        sumDrawReq = 0.0;
                    
                    */
                    for(UnifiedBean.DrawRequestWrapper drawRequestIterate : reqData.projects[0].draws)
                    {
                        if(drawRequestIterate.hashId==null || drawRequestIterate.hashId==''){
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                        if(null != drawReqMap && !drawReqMap.isEmpty() && drawReqMap.containsKey(drawRequestIterate.hashId)){
                            System.debug('### Processing payment request: ');
                            
                            Draw_Requests__c drawReqRec = drawReqMap.get(drawRequestIterate.hashId);
                            
                            // Added as part of ORANGE - 3986 - start
                            if(null != drawReqRec.Status__c && drawReqRec.Status__c == 'Expired'){
                                
                                //Calling Twiliow on Draw request expired status
                                //start
                                if(drawReqRec.SMS_Opt_in__c == true )
                                {
                                    Map<Id,Draw_Requests__c> drawConMap = new Map<Id,Draw_Requests__c>();
                                    drawConMap.put(drawReqRec.Primary_Contact__c,drawReqRec);
                                    String jsonDrawRecMap = JSON.serialize(drawConMap);
                                    TwilioUtil.callTwilioWS(jsonDrawRecMap,'Expired');
                                }
                                //End
                                getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('333',null),'333');
                                iRestRes = (IRestResponse)unifiedRespBean;
                                return iRestRes;
                            }
                            // Added as part of ORANGE - 3986 - End
                            if(drawReqRec.Status__c!=null && drawReqRec.Status__c != 'Requested'){
                                getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('378',null),'378');
                                iRestRes = (IRestResponse)unifiedRespBean;
                                return iRestRes;
                            }
                            
                            drawReqUpdate.id = drawReqRec.id;
                            drawReqUpdate.Status__c = drawRequestIterate.status;
                            drawReqUpdate.Draw_Methodology__c = drawRequestIterate.drawMethodology;
                            if(drawRequestIterate.status=='Approved'){
                                drawReqUpdate.Approval_Date__c = System.now();
                                unifiedRespBean.returnCode = '200';
                                unifiedRespBean.message = 'Thank you for authorizing payment.';
                                //drawReqUpdate.Total_Amount_Drawn__c = drawReqRec.Underwriting__r.Amount_Remaining__c;
                                //drawReqUpdate.Remaining_amount_for_additional_payment__c =  drawReqRec.Maximum_loan_amount__c - drawReqUpdate.Total_Amount_Drawn__c;
                               /*
                                system.debug('sumDrawReq '+sumDrawReq);
                                system.debug('Amount__c '+drawReqRec.Amount__c);
                                
                                if(null != sumDrawReq)
                                    drawReqUpdate.Total_Amount_Drawn__c = sumDrawReq + drawReqRec.Amount__c;
                                else
                                    drawReqUpdate.Total_Amount_Drawn__c = drawReqRec.Amount__c;
                                
                                drawReqUpdate.Remaining_amount_for_additional_payment__c = drawReqRec.Maximum_loan_amount__c - drawReqUpdate.Total_Amount_Drawn__c;
                                */    
                            }else if(drawRequestIterate.status=='Declined'){
                                drawReqUpdate.Decline_Date__c = drawRequestIterate.declineDate;
                                //drawReqUpdate.Project_Complete_Certification__c = false; //added by Ravi as part of ORANGE-8959
                                unifiedRespBean.returnCode = '200';
                                unifiedRespBean.message = 'At this time, you have confirmed you do not wish to authorize payment.';
                            }
                            
                            updateDrawRequestList.add(drawReqUpdate);
                            //system.debug('Draw Request ID: '+drawReqUpdate.id);
                            //system.debug('Success Message: '+unifiedRespBean.message);
                        }
                        else{
                            getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('371',null),'371');
                            iRestRes = (IRestResponse)unifiedRespBean;
                            return iRestRes;
                        }
                    }
                    if(updateDrawRequestList!=null && !updateDrawRequestList.isEmpty()){
                        update updateDrawRequestList;
                        
                        /*
                        system.debug('### Updated Draw Request record: ');
                        List<Draw_Requests__c> updateDrawRequestList1 = new List<Draw_Requests__c>();
                        
                        List<Draw_Requests__c> lstDrawReqRec1 = [select id,Status__c,Underwriting__r.Total_Amount_Drawn__c,Underwriting__r.opportunity__r.Combined_Loan_Amount__c,Underwriting__r.Amount_Remaining__c,Underwriting__r.opportunity__r.Installer_Account__r.Name,Underwriting__r.Opportunity__r.Owner.Name,Underwriting__r.opportunity__r.Account.Name,Underwriting__r.opportunity__r.Installer_Account__r.Installer_Email__c,Underwriting__r.opportunity__r.Installer_Account__r.Operations_Email__c,Underwriting__r.opportunity__r.Hash_Id__c from Draw_Requests__c where Hash_Id__c=:reqData.projects[0].draws[0].hashId];
                        if(lstDrawReqRec1!=null && !lstDrawReqRec1.isEmpty() && lstDrawReqRec1.get(0).id != null){
                            
                            System.debug('### Tracking-1');
                            drawReqUpdate.Total_Amount_Drawn__c = lstDrawReqRec1[0].Underwriting__r.Total_Amount_Drawn__c;
                            drawReqUpdate.Maximum_loan_amount__c = lstDrawReqRec1[0].Underwriting__r.opportunity__r.Combined_Loan_Amount__c;
                            drawReqUpdate.Remaining_amount_for_additional_payment__c = lstDrawReqRec1[0].Underwriting__r.Amount_Remaining__c;
                            drawReqUpdate.Installer_Account_Name__c = lstDrawReqRec1[0].Underwriting__r.opportunity__r.Installer_Account__r.Name;
                            drawReqUpdate.Sales_Rep_Name__c = lstDrawReqRec1[0].Underwriting__r.Opportunity__r.Owner.Name;
                            drawReqUpdate.Customer_Name__c = lstDrawReqRec1[0].Underwriting__r.opportunity__r.Account.Name;
                            drawReqUpdate.Installer_Email__c = lstDrawReqRec1[0].Underwriting__r.opportunity__r.Installer_Account__r.Installer_Email__c;
                            drawReqUpdate.Operations_Email__c = lstDrawReqRec1[0].Underwriting__r.opportunity__r.Installer_Account__r.Operations_Email__c;
                            drawReqUpdate.Opp_Hash_Id__c = lstDrawReqRec1[0].Underwriting__r.opportunity__r.Hash_Id__c;
                            
                            updateDrawRequestList1.add(drawReqUpdate);
                        }
                        update updateDrawRequestList1;
                        */
                    }
                    //need to update oppty records as well
                    DrawRequestUtil.recalculate(opprLst.get(0).id);
                }
                else{
                    getErrorMsg(unifiedRespBean,ErrorLogUtility.getErrorCodes('214',null),'214');
                    iRestRes = (IRestResponse)unifiedRespBean;
                    return iRestRes;
                }
            }
            
        }Catch(Exception err){
            Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedRespBean.error.add(errorMsg);
            unifiedRespBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedRespBean;
            system.debug('### PaymentApprovalServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('PaymentApprovalServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of '+PaymentApprovalServiceImpl.class);
        iRestRes = (IRestResponse)unifiedRespBean;
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
    public void getErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        iolist = new list<DataValidator.InputObject>{};
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
}