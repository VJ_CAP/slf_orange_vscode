/**
* Description: 
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Rajesh               05/02/2019              Created
******************************************************************************************/
global class CreateEquipmentBoxFolder{
    
    global class FolderWrapper {
        @InvocableVariable (required=true) global Id opportunityId;
        @InvocableVariable  global String FolderName;
        
    }
    
    @InvocableMethod(label='CreateEquipmentBoxFolder')
    global static List<FolderWrapper> CreateNewFolder(List<FolderWrapper> fundingWrap){
        List<FolderWrapper> responseWrap = new List<FolderWrapper>();
        FolderWrapper responseWrp = new FolderWrapper();
        try{
            
            

           
            String boxAccessTokan = '';
                    
            Id oppId;
            String FolderName = '';
            
            for(FolderWrapper objI:fundingWrap){
                oppId=objI.opportunityId;
                FolderName = objI.FolderName;
            }
            CreateEquipmentfolder(oppId,FolderName);
            /*
            if(fundingWrap!=null && !fundingWrap.isEmpty()){
                BoxPlatformApiConnection connection;
                connection = BoxAuthentication.getConnection();
                System.debug('#### test'+connection.accessToken);
                boxAccessTokan = connection.accessToken;
                String opportunityFolderId = '';
                Box.Toolkit boxToolkit = new Box.Toolkit();
                opportunityFolderId = boxToolkit.createFolderForRecordId (oppId, null, true);
                //ValidateDocumentUploadDataServiceImpl validdateDocUpload = new ValidateDocumentUploadDataServiceImpl();
                responseWrp.FolderName = ValidateDocumentUploadDataServiceImpl.getChildFolderId (opportunityFolderId,FolderName, boxAccessTokan);
                responseWrap.add(responseWrp);

            }*/
        }
                
               
            
        catch(exception err){
            System.debug('Exception ==>:'+err.getMessage()+'### at Line==>:'+err.getLineNumber());
            ErrorLogUtility.writeLog(' CreateEquipmentBoxFolder.CreateNewFolder()',err.getLineNumber(),'CreateNewFolder()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));    
        }
        return responseWrap;
    }
    
    @future(callout=true)
    
    Public static void CreateEquipmentfolder(String oppId,String FolderName){
    ValidateDocumentUploadDataObject validateDocUploadObj = new ValidateDocumentUploadDataObject();
            validateDocUploadObj.documentFolderType  = FolderName;
            validateDocUploadObj.opportunityId = oppId;
            validateDocUploadObj.externalId  = '';
            //validateDocUploadObj.fileName = 'Sunlight Financial '+d.dsfs__Opportunity__r.Applicant_First_Name__c+' '+d.dsfs__Opportunity__r.Applicant_Last_Name__c+' '+d.dsfs__Opportunity__r.Long_Term_Term__c/12+' '+d.dsfs__Opportunity__r.Long_Term_APR__c+' Loan Documents.pdf';
            ValidateDocumentUploadDataServiceImpl validdateDocUpload = new ValidateDocumentUploadDataServiceImpl();

            ValidateDocumentUploadDataObject parseValidateDocUpload = (ValidateDocumentUploadDataObject)validdateDocUpload.transformOutput(validateDocUploadObj);
            ValidateDocumentUploadDataObject.OutputWrapper  op = parseValidateDocUpload.output; 
    }
}