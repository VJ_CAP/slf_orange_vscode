public class jsonSightenResponse {

	public class Utility {
		public String link;
		public String natural_id;
		public String uuid;
	}
    

	public class Messages {
		public List<Error> critical;
		public List<Error> error;
		public List<Error> info;
		public List<Error> warning;
	}

	public class Contacts {
		public String link;
	}

	public Integer status_code;
	public Data data;
	public Messages messages;
	
    public class Error {
		public String timestamp;
		public String message;
		public String error_code;
		public String resolution;
		public String msg_code;
		public String reason;
	}
    
	public class Usage_months {
		public Double one	;
		public Double two;
		public Double three;
		public Double four;
		public Double five;
		public Double six;
		public Double seven;
		public Double eight;
		public Double nine;
		public Double ten;
		public Double eleven;
		public Double twelve;
	}

	public class Data {
		public String tmy_datatype;
		public String date_updated;
		public Double avg_monthly_utility_bill;
		public Utility utility;
		public String heating_source;
		public Usage_months usage_months;
		public Utility owned_by_organization;
		public Utility owned_by_user;
		public Double avg_monthly_usage;
		public Double avg_cost_of_power;
		public String uuid;
		public Utility created_by;
		public Utility address;
		public Utility modified_by;
		public Utility utility_rate_schedule;
		public Contacts contacts;
		public Utility utility_territory;
		public String natural_id;
		public String tmy_station;
		public String date_created;
	}


	
	public static jsonSightenResponse parse(String json) {
		return (jsonSightenResponse) System.JSON.deserialize(json, jsonSightenResponse.class);
	}
}