@isTest
public class SendConfirmationEmailTriggerHandlerTest {
  private static Boolean runAllTests = true;

    @testSetup static void createTestData(){
        
        insert new SFSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), Root_Folder_ID__c='0');

        Account acct = new Account(
            Name = 'Installer Test Account'
        );
        insert acct;

        Contact c = new Contact(
            FirstName = 'Test',
            LastName = 'Contact',
            Email = 'test@email.com'
        );
        insert c;

        Product__c slf = new Product__c(
            Short_Term_Facility__c = '',
            Long_Term_Facility__c = '',
            Installer_Account__c = acct.Id,
            State_Code__c = 'TX',
            FNI_Min_Response_Code__c = 5,
            Product_Display_Name__c = 'Test Product',
            Qualification_Message__c = 'Test Qualification Message'
        );
        insert slf;

        Opportunity opp = new Opportunity(
            SLF_Product__c = slf.Id,
            Name = 'Test Opportunity',
            StageName = 'Qualified',
            CloseDate = Date.today().addDays(-3),
            Install_State_Code__c = 'TX',
            Installer_Account__c = acct.Id
        );
        insert opp;

        Underwriting_File__c uwf = new Underwriting_File__c(
            Send_Confirmation_Email__c = true,
            Opportunity__c = opp.Id
        );
        insert uwf;
    }

    @isTest static void testCRB_SameAsCash(){
        //if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Short_Term_Facility__c FROM Product__c LIMIT 1];
        slf.Short_Term_Facility__c = 'CRB';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];

        ApexPages.currentPage().getParameters().put('Id', uwf.Opportunity__c);

        //AutomatedLoanConfirmationEmail.sendEmailConfirmation ();

        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

        Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE 'Confirmation_Email%' LIMIT 1];
        system.assertNotEquals(null, att);

        Test.stopTest();
    }

    @isTest static void testCRB_Single(){
        if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Long_Term_Facility__c FROM Product__c LIMIT 1];
        slf.Long_Term_Facility__c = 'CRB';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];
        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

//      Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE '%Confirmation_Email%' LIMIT 1];
//      system.assertNotEquals(null, att);

        Test.stopTest();
    }

    @isTest static void testCRB_Combo(){
        if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Short_Term_Facility__c, Long_Term_Facility__c FROM Product__c LIMIT 1];
        slf.Long_Term_Facility__c = 'CRB';
        slf.Short_Term_Facility__c = 'CRB';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];
        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

//      Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE 'Confirmation_Email%' LIMIT 1];
//      system.assertNotEquals(null, att);

        Test.stopTest();
    }

    @isTest static void testTCU_Combo(){
        if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Short_Term_Facility__c, Long_Term_Facility__c FROM Product__c LIMIT 1];
        slf.Long_Term_Facility__c = 'TCU';
        slf.Short_Term_Facility__c = 'TCU';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];
        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

//      Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE '%Confirmation_Email%' LIMIT 1];
//      system.assertNotEquals(null, att);

        Test.stopTest();
    }

    @isTest static void testTCU_Single(){
        if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Long_Term_Facility__c FROM Product__c LIMIT 1];
        slf.Long_Term_Facility__c = 'TCU';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];
        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

//      Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE '%Confirmation_Email%' LIMIT 1];
//      system.assertNotEquals(null, att);

        Test.stopTest();
    }

    @isTest static void testCRB_TCU(){
        if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Short_Term_Facility__c, Long_Term_Facility__c FROM Product__c LIMIT 1];
        slf.Long_Term_Facility__c = 'TCU';
        slf.Short_Term_Facility__c = 'CRB';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];
        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

//      Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE '%Confirmation_Email%' LIMIT 1];
//      system.assertNotEquals(null, att);

        Test.stopTest();
    }

    @isTest static void testTCU_CRB(){
        if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Short_Term_Facility__c, Long_Term_Facility__c FROM Product__c LIMIT 1];
        slf.Long_Term_Facility__c = 'CRB';
        slf.Short_Term_Facility__c = 'TCU';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];
        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

//      Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE '%Confirmation_Email%' LIMIT 1];
//      system.assertNotEquals(null, att);

        Test.stopTest();
    }

    @isTest static void testTCU_SameAsCash(){
        if(!runAllTests) { return; }

        Product__c slf = [SELECT Id, Short_Term_Facility__c, Loan_Type__c FROM Product__c LIMIT 1];
        slf.Short_Term_Facility__c = 'TCU';
        update slf;

        Test.startTest();

        Underwriting_File__c uwf = [SELECT Id, Opportunity__c, Send_Confirmation_Email__c FROM Underwriting_File__c LIMIT 1];
        SendConfirmationEmailTriggerHandler.HandleAfterInsertUpdate (new list<Underwriting_File__c>{uwf});

//      Attachment att = [SELECT Id, Name FROM Attachment WHERE ParentId = :uwf.Opportunity__c AND Name LIKE '%Confirmation_Email%' LIMIT 1];
//      system.assertNotEquals(null, att);

        Test.stopTest();
    }

}