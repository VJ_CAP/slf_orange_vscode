/**
* Description: SubmitChangeOrder related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           10/17/2017          Created
******************************************************************************************/
public class  SubmitChangeOrderDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject changOrDtObj){
        system.debug('### Entered into transformOutput() of '+SubmitChangeOrderDataServiceImpl.class);
        
             
        UnifiedBean reqData = (UnifiedBean)changOrDtObj;
        system.debug('### reqData '+reqData);
        
        UnifiedBean changeorderbean = new UnifiedBean(); 
        changeorderbean.projects = new List<UnifiedBean.OpportunityWrapper>();
        changeorderbean.error = new list<UnifiedBean.errorWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        projectWrap.applicants = new List<UnifiedBean.ApplicantWrapper>();
        projectWrap.stips = new List<UnifiedBean.StipsAndStatusWrapper>();
        projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
        projectWrap.systemDesigns = new List<UnifiedBean.SystemDesignWrapper>();
        projectWrap.quotes = new List<UnifiedBean.QuotesWrapper>();
        
        Savepoint sp = Database.setSavepoint();
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {
            Set<String> oppidSet = new Set<String>();
            opportunity oppRec = new opportunity();
            String opportunityId = '';
            String externalId;
            List<user> loggedInUsr = [select Id,contactid,contact.AccountId,contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,UserRole.Name from User where Id =: userinfo.getUserId()];
            List<UnifiedBean.OpportunityWrapper> reqProjectsLst = reqData.projects;
            system.debug('@@@@reqProjects: '+reqData.projects);
            if(reqData.projects == NULL ||(String.isBlank(reqData.projects[0].id) && String.isBlank(reqData.projects[0].externalId)&&String.isBlank(reqData.projects[0].hashId)))
            {                 
                errMsg = ErrorLogUtility.getErrorCodes('203',null);
                changeorderbean.returnCode = '203';
            } 
            else if(
                (String.isNotBlank(reqData.projects[0].id) && String.isNotBlank(reqData.projects[0].externalId) && String.isNotBlank(reqData.projects[0].hashId))
                ||(String.isNotBlank(reqData.projects[0].id) && (String.isNotBlank(reqData.projects[0].externalId) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].externalId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].hashId)))
                ||(String.isNotBlank(reqData.projects[0].hashId) && (String.isNotBlank(reqData.projects[0].id) || String.isNotBlank(reqData.projects[0].externalId))))
            {
                errMsg = ErrorLogUtility.getErrorCodes('201',null);
                changeorderbean.returnCode = '201';
            }else if(reqData.projects[0].id !=null && String.isNotBlank(reqData.projects[0].id))
            {
                List<Opportunity> opprLst = [SELECT Id FROM Opportunity WHERE Id = :reqData.projects[0].id LIMIT 1];
                system.debug('@@@@oppId: '+reqData.projects[0].id+'@oppLst: '+opprLst);
                if (opprLst==null || opprLst.size() == 0)
                {
                    changeorderbean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
                else{
                    opportunityId = opprLst.get(0).Id;
                }
            }
            else if(reqData.projects[0].externalId !=null && String.isNotBlank(reqData.projects[0].externalId))
            {
                if(!loggedInUsr.isEmpty())
                {
                    if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                        externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+reqData.projects[0].externalId;
                    }else{
                        changeorderbean.returnCode = '400';
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                    }
                    
                }
                if(String.isBlank(errMsg))
                {
                    List<opportunity> opprLst = [select id,Partner_Foreign_Key__c from opportunity where Partner_Foreign_Key__c=:externalId limit 1];
                    if (opprLst==null || opprLst.size() == 0)
                    {
                        changeorderbean.returnCode = '202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                    else{
                        opportunityId = opprLst.get(0).id;
                    }
                }
            }else if(reqData.projects[0].hashId !=null && String.isNotBlank(reqData.projects[0].hashId))
            {
                List<opportunity> opprLst = [select id,Hash_Id__c from opportunity where Hash_Id__c=:reqData.projects[0].hashId limit 1];
                system.debug('@@@@Hash Id from request:'+reqData.projects[0].hashId);
                if (opprLst!=null && opprLst.size() > 0)
                {
                    opportunityId = opprLst.get(0).id;
                }else {
                    changeorderbean.returnCode = '202';
                    errMsg = ErrorLogUtility.getErrorCodes('202',null);
                }
            } 
            if(String.isBlank(errMsg)  && String.isNotBlank(opportunityId))
            {
                {
                    // updated query as part of 1930 
                    List<Opportunity> oppLst = [Select id,Name,AccountId,Project_Category__c,SLF_Product__c,isChangeOrderCancelled__c, StageName,Installer_Account__c,CloseDate,Partner_Foreign_Key__c,Short_Term_APR__c,Short_Term_Term__c,Change_Order_Status__c,Co_applicant_present_on_loan_agreement__c,EDW_Originated__c,Co_Applicant_Approved_for_Credit__c,LT_Loan_Name_s_Matches_Credit_Bureau__c,LT_Loan_Name_s_Signature_is_Present__c,Short_Term_Facility__c,Monthly_Payment__c,Long_Term_Term__c,Monthly_Payment_calc__c,Long_Term_Facility__c,Long_Term_APR__c,Combined_Loan_Amount__c,Install_Cost_Cash__c,Initial_Payment__c,Customer_has_authorized_credit_hard_pull__c,RecordTypeId,Install_Postal_Code__c,Install_Street__c,Install_City__c,utilityName__c,Install_State_Code__c,Co_Applicant__c,SyncedQuoteId,Short_Term_Loan_Amount_Manual__c,Long_Term_Loan_Amount_Manual__c,Application_Status__c,
                    
                    (Select id,Name,System_Design__c,FinalMonthlyPaymentwithoutPrepay__c,QuoteNumber,SLF_Product__c,SLF_Product__r.Name,Upfront_Payment__c,isSyncing,Final_Payment_Single_Loan__c,OpportunityId,Monthly_Payment__c,Monthly_Payment_without_Prepay__c,Final_Monthly_Payment__c,CreatedDate,Combined_Loan_Amount__c from Quotes)
                    
                    ,(Select id,Name,Battery_Capacity__c,Battery_Count__c,BatteryMake__c,rmIdLocalContractorAndManufacturer__c,Battery_Model__c,Est_Annual_Production_kWh__c,Tilt__c,Inverter_Count__c,InverterMake__c,Inverter_Model__c,Module_Count__c,Module_Model__c,Opportunity__c,System__c,ModuleMake__c,Azimuth__c,System_Size_Ac__c,Ground_Mount_Manufacturer__c,Ground_Mount_Type__c,taxLiabilityPercentageCap__c,System_Cost_Minus_Incentives__c,isLowIncome__c,locallyManufacturedComponents__c,Rec_Retained__c,System_Payments__c from System_Designs__r)
                    
                    ,(select Id,Name,SLF_Product__c,SLF_Product__r.Name,Primary_Applicant__c,Primary_First_Name__c,Primary_Last_Name__c,Co_FirstName__c,Co_LastName__c,Credit_Decision_Date__c,Co_Applicant__c,FNI_Error_Message__c,Opportunity__c,IsSyncing__c,Status__c,Long_Term_Amount_Approved__c from Credits__r)
                    //,(select Id,Name,M1_Approval_Date__c, Project_Status__c,Opportunity__c,Funding_status__c,ST_Loan_Agreement_Review_Complete__c,Final_Design_Review_Complete__c,Project_Status_Detail__c,Confirm_ACH_Information__c,Installation_Photos_Review_Complete__c,LT_Loan_Agreement_Received__c,Confirmation_Call_Initiated__c,Welcome_Call_Initiated__c,PTO_Review_Complete__c,Install_Contract_Review_Complete__c,Final_Design_Document_Received__c,PTO_Received__c,Primary_Borrower_ID_Type__c,Utility_Bill_Review_Complete__c,Change_Order_Identified__c,LT_Loan_Amount_Verified_FNI__c,LT_Loan_Name_s_Verified_FNI__c,LT_Loan_Address_Verified_FNI__c,LT_Loan_Agreement_Review_Complete__c,Contract_Address_Matches_Loan_Address__c,Printed_Name_Matches_Loan_Agreement__c,Title_Name_Matches_Loan_Agreement__c,Title_Review_Complete__c from Underwriting_Files__r order by CreatedDate Desc)
                    ,(SELECT Id,Name,M1_Approval_Date__c, Project_Status__c,Opportunity__c,Funding_status__c,ST_Loan_Agreement_Review_Complete__c,Final_Design_Review_Complete__c,Project_Status_Detail__c,Confirm_ACH_Information__c,Installation_Photos_Review_Complete__c,Confirmation_Call_Initiated__c,Welcome_Call_Initiated__c,PTO_Review_Complete__c,Install_Contract_Review_Complete__c,Primary_Borrower_ID_Type__c,Utility_Bill_Review_Complete__c,Change_Order_Identified__c,LT_Loan_Amount_Verified_FNI__c,LT_Loan_Name_s_Verified_FNI__c,LT_Loan_Address_Verified_FNI__c,LT_Loan_Agreement_Review_Complete__c,Contract_Address_Matches_Loan_Address__c,Printed_Name_Matches_Loan_Agreement__c,Title_Name_Matches_Loan_Agreement__c,Title_Review_Complete__c FROM Underwriting_Files__r order by CreatedDate Desc)
                    ,Account.id,Change_Order_Number__c from Opportunity where id=: opportunityId];
                    if(!oppLst.isEmpty())
                    {
                        oppRec = oppLst[0];
                        system.debug('@@@@@oppty Record:'+oppRec);
                        Id oppId = oppRec.Id;
                        Opportunity oppt = new Opportunity();
                        
                        String oppquery = buildSqlQuery('Opportunity',oppId);
                        System.debug('### oppquery '+oppquery);
                        oppt = database.query(oppquery);
                        
                        List<Underwriting_File__c> underwrite = new List<Underwriting_File__c>();
                        String underwritngQuery = buildSqlQuery('Underwriting_File__c',oppId);
                        System.debug('### underwritngQuery '+underwritngQuery);
                        underwrite = database.query(underwritngQuery);
                        
                        // ends here - get all fields
                        if(oppRec.EDW_Originated__c == true){
                            errMsg = ErrorLogUtility.getErrorCodes('218',new List<String>{String.valueOf(oppRec.id)}); 
                            changeorderbean.returnCode = '218';
                        }
                        else if(oppRec.StageName != 'Closed Won')
                        {                                
                            errMsg = ErrorLogUtility.getErrorCodes('310',null);
                            changeorderbean.returnCode = '310';
                        }
                        else if(oppRec.Underwriting_Files__r != NULL && oppRec.Underwriting_Files__r.size() > 0 && oppRec.Underwriting_Files__r[0].M1_Approval_Date__c != NULL)
                        {
                            errMsg = ErrorLogUtility.getErrorCodes('312',null);
                            changeorderbean.returnCode = '312';
                        }else if(oppRec.Underwriting_Files__r != null && oppRec.Underwriting_Files__r[0].Project_Status__c != null && oppRec.Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                            changeorderbean.returnCode = '366';
                            errMsg = ErrorLogUtility.getErrorCodes('366',null);
                        }else{
                            Map<String,String> OwnercheckMap = SLFUtility.checkOpportunityOwner(oppRec,loggedInUsr[0]);
                            if(OwnercheckMap != null){
                                errMsg = OwnercheckMap.values().get(0);
                                List<String> errList = new List<String>();
                                errList.addAll(OwnercheckMap.keyset());
                                changeorderbean.returnCode = errList[0];
                            }
                            else{
                                oppRec.StageName = 'Change Order Pending';
                                oppRec.Change_Order_Status__c = 'Pending'; 
                                oppRec.isChangeOrderCancelled__c = false; //Srikanth - 03/15 - reset this flag so that Opportunty stage update workflows rule work as is.
                                oppRec.isChangeOrderSubmitted__c = true; //Added by suresh to fix change  order issue
                                oppRec.Probability = 95;
                                oppRec.CloseDate = Date.Today();
                                oppRec.ForecastCategoryName = 'Commit';
                                oppRec.Synced_Quote_Id__c = oppRec.SyncedQuoteId;
                                //Added by Suresh as per Orange-1606
                                oppRec.Draw_Carry_Over__c=0;
                                oppRec.Max_Draw_Available__c=0;
                                if(reqData.projects[0].reasonForCO !=null && String.isNotBlank(reqData.projects[0].reasonForCO)){
                                    oppRec.Reason_for_CO__c = reqData.projects[0].reasonForCO;
                                }
                                //End-Added by Suresh as per Orange-1606
                                //Need to store Synced Credit Id as well, field was already created Synced_Credit_Id__c
                                if(null != oppRec.Credits__r) 
                                {
                                    for(SLF_Credit__c creditIterate : oppRec.Credits__r)
                                    {
                                        if(creditIterate.IsSyncing__c){
                                            oppRec.Synced_Credit_Id__c = creditIterate.id;
                                        }
                                    }
                                }
                                if(oppRec.Change_Order_Number__c == 0 || null == oppRec.Change_Order_Number__c)
                                    oppRec.Change_Order_Number__c = 1;
                                else if(oppRec.Change_Order_Number__c > 0 )
                                    oppRec.Change_Order_Number__c = oppRec.Change_Order_Number__c + 1;
                                
                                update oppRec;
                                if(oppRec.Underwriting_Files__r != NULL && oppRec.Underwriting_Files__r.size() > 0 && oppRec.StageName == 'Change Order Pending')
                                {
                                    Underwriting_File__c underwritingRec = new Underwriting_File__c(id = oppRec.Underwriting_Files__r[0].id);
                                    underwritingRec.Project_Status_Detail__c = 'Change Order Requested';
                                    underwritingRec.Change_Order__c = false;  //Srikanth - 04/02 - Orange-315 - There is time-based workflow field update that clears this flag but multiple change orders with in an hour wouldn't clear this flag and causes issues.
                                    underwritingRec.CO_Requested_Date__c = System.now();
                                    //Srikanth - 06/11 - Orange-1877 - CO fields to be cleared when change order is initiated
                                    underwritingRec.CO_Approval_Requested_Date__c = NULL;
                                    underwritingRec.CO_Approval_Date__c = NULL;
                                    underwritingRec.CO_Ready_for_Review_Date_Time__c = NULL;
                                    underwritingRec.CO_Review_Complete__c = false;
                                    underwritingRec.CO_Approver__c = NULL;
                                    //Added as part of Orange - 3964 - Start
                                    if(oppRec.Project_Category__c == 'Home'){
                                        underwritingRec.HIDR_Approval_Date_Time__c = null;
                                        underwritingRec.HIDR_Approver__c = null;
                                        //underwritingRec.Approved_for_Payments__c = False; commented as a part of Orange - 11174
                                        underwritingRec.Loan_doc_reflects_Product_Details__c = False;
                                        underwritingRec.Loan_document_signed__c = False;
                                        underwritingRec.Loan_doc_not_signed_at_rep_email__c = False;
                                    }
                                    //Added as part of Orange - 3964 - End
                                    update underwritingRec;
                                }
                                
                                /*
                                Quote
                                Credit
                                Underwriting
                                System Design
                                //Credit Opinion
                                */

                                Opportunity newClnOpp = oppt.clone(false, true);
                                newClnOpp.StageName = 'Archive';
                                newClnOpp.Partner_Foreign_Key__c = '';
                                newClnOpp.Hash_Id__c = '';
                                newClnOpp.Probability = 5;
                                newClnOpp.ForecastCategoryName = 'Pipeline';
                                newClnOpp.Opportunity__c = oppRec.Id;  
                                newClnOpp.SyncedQuoteId = null;
                                newClnOpp.FNI_Application_ID__c=null;
                                newClnOpp.FNI_Reference_Number__c = null; //Srikanth - 09/12 - Orange-861
                                upsert newClnOpp;
                                //Clone Underwriting_File__c from old Opportunity and insert new Underwriting_File__c records to new Opportunity
                                //start
                                
                               // List<Underwriting_File__c> underwrite = new List<Underwriting_File__c>();
                            
                               // String underwritngQuery = buildSqlQuery('Underwriting_File__c',oppId);
                               // System.debug('### underwritngQuery '+underwritngQuery);
                               // underwrite = database.query(underwritngQuery);
                                List<Underwriting_File__c> underwritingLst = new List<Underwriting_File__c>();
                                if(null != underwrite) 
                                {
                                    for(Underwriting_File__c underwritingIterate : underwrite)
                                    {  
                                        Underwriting_File__c newUF = underwritingIterate.clone(false,true);
                                        newUF.Opportunity__c = newClnOpp.Id;
                                        underwritingLst.add(newUF);
                                    }
                                }
                                if(!underwritingLst.isEmpty())
                                    insert underwritingLst;
                                //end
                                 //Clone System_Design__c from old Opportunity and insert new System_Design__c records to new Opportunity
                                //start
                                List<System_Design__c> sysDesigns = new List<System_Design__c>();
                            
                                String sysdesinQuery = buildSqlQuery('System_Design__c',oppId);
                                System.debug('### sysdesinQuery ' +sysdesinQuery);
                                sysDesigns = database.query(sysdesinQuery);
                                
                                List<System_Design__c> sysDsignLst = new List<System_Design__c>();
                                Map<id,System_Design__c> systemdesignsMap = new Map<id,System_Design__c>();
                                if(null != sysDesigns) 
                                {
                                    for(System_Design__c sysDsignIterate : sysDesigns)
                                    {  
                                        System_Design__c newSysDsign = sysDsignIterate.clone(false,true);
                                        newSysDsign.Opportunity__c = newClnOpp.Id;
                                        systemdesignsMap.put(sysDsignIterate.id,newSysDsign);
                                        sysDsignLst.add(newSysDsign);
                                    }
                                }
                                if(!sysDsignLst.isEmpty())
                                    insert sysDsignLst;
                                //end
                                //Clone Quote from old Opportunity and insert new Quote records to new Opportunity
                                //start
                                List<Quote> quotes = new List<Quote>();
                            
                                String quoteQuery = buildSqlQuery('Quote',oppId);
                                quotes = database.query(quoteQuery);
                                
                                List<Quote> quoteLst = new List<Quote>();
                                Map<Id,Quote> quoteSyncMap = new Map<Id,Quote> ();
                                if(null != quotes) 
                                {
                                    for(Quote quoteIterate : quotes)
                                    {  
                                        
                                        Quote newUF = quoteIterate.clone(false,true);
                                        if(quoteIterate.System_Design__c != null)
                                            newUF.System_Design__c =systemdesignsMap.get(quoteIterate.System_Design__c).id;
                                        newUF.OpportunityId = newClnOpp.Id;
                                        quoteLst.add(newUF);
                                        if(quoteIterate.isSyncing)
                                        {
                                            quoteSyncMap.put(newClnOpp.Id,newUF);
                                        }
                                    }
                                }
                                if(!quoteLst.isEmpty())
                                    insert quoteLst;
                                
                                //sync Quote record to Opportunity
                                if(!quoteSyncMap.isEmpty())
                                {
                                    newClnOpp.SyncedQuoteId = quoteSyncMap.get(newClnOpp.Id).Id;
                                    update newClnOpp;
                                }
                                
                                //end
                                //Clone SLF_Credit__c from old Opportunity and insert new SLF_Credit__c records to new Opportunity
                                //start
                                List<SLF_Credit__c> credits = new List<SLF_Credit__c>();
                                String creditQuery = buildSqlQuery('SLF_Credit__c',oppId);
                                System.debug('### creditQuery '+creditQuery);
                                credits = database.query(creditQuery);
                                
                                List<SLF_Credit__c> creditLst = new List<SLF_Credit__c>();
                                if(null != credits) 
                                {
                                    for(SLF_Credit__c creditIterate : credits)
                                    {  
                                        SLF_Credit__c newSLFCredit = creditIterate.clone(false,true);
                                        newSLFCredit.Opportunity__c = newClnOpp.Id;
                                        newSLFCredit.Hash_Id__c = '';
                                        creditLst.add(newSLFCredit);
                                    }
                                }
                                if(!creditLst.isEmpty())
                                    insert creditLst;
                                //end
                               
                                changeorderbean = SLFUtility.getUnifiedBean(new set<Id>{oppRec.Id},'Orange',false);
                                changeorderbean.returnCode = '200';                            
                                //Added as part of the ticket 1877
                                /*if(newClnOpp.Combined_Loan_Amount__c != oppRec.Combined_Loan_Amount__c)
                                {
                                    changeorderbean.quoteAmountChanged = false;
                                }
                                else
                                {
                                    changeorderbean.quoteAmountChanged = true;
                                }*/
                                //Added as part of the ticket 1877
                            }
                        }
                    }else{
                        changeorderbean.returnCode='202';
                        errMsg = ErrorLogUtility.getErrorCodes('202',null);
                    }
                }
                    
            }
            if(string.isNotBlank(errMsg))
            {
                Database.rollback(sp);
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                changeorderbean.error.add(errorMsg);
            }
            
        }catch(Exception err){
            Database.rollback(sp);
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            changeorderbean.returnCode = '400';  
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            changeorderbean.error.add(errorMsg);
                     
            system.debug('### SubmitChangeOrderDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('SubmitChangeOrderDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        iRestRes = (IRestResponse)changeorderbean; 
        system.debug('### Exit from transformOutput() of '+SubmitChangeOrderDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    /*
    ****This Method Used to build Query Dynamically
    */
    public string buildSqlQuery(String objName,Id oppId){
        system.debug('### Entered into buildSqlQuery() of ' + SubmitChangeOrderDataServiceImpl.class);
        String fieldnames = '';
        String sql;
        try{
            Map < String, Schema.SObjectType > m = Schema.getGlobalDescribe();
            Schema.SObjectType s = m.get(objName);
            Schema.DescribeSObjectResult r = s.getDescribe();
            
            //get fields
            Map < String, Schema.SObjectField > fields = r.fields.getMap();
            for (string field: fields.keySet()) 
            {
                list<String> updateFields = new list<String>();
                
                    if (fieldnames == '') {
                        fieldnames = field;
                    } else {
                        fieldnames += ',' + field;
                    }
            }
            
            if(objName =='Opportunity'){
                sql = 'SELECT ' + fieldnames + ' FROM ' + objName + ' WHERE Id=\'' + oppId + '\'';
            }else if(objName =='Quote'){
                sql = 'SELECT ' + fieldnames + ' FROM ' + objName + ' WHERE OpportunityId=\'' + oppId + '\'';
            }else{
             sql = 'SELECT ' + fieldnames + ' FROM ' + objName + ' WHERE Opportunity__c=\'' + oppId + '\'';
            }
            System.debug('### sql '+sql);
        }catch(Exception err){
           system.debug('### SubmitChangeOrderDataServiceImpl.buildSqlQuery():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           ErrorLogUtility.writeLog(' SubmitChangeOrderDataServiceImpl.buildSqlQuery()',err.getLineNumber(),'buildSqlQuery()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }system.debug('### Exit from buildSqlQuery() of '+SubmitChangeOrderDataServiceImpl.class);
        return sql;
    }
}