/**
* Description: StatusDataObject class to marshall and unmarshall request and response for Opportunity object.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/
public class StatusDataObject implements IDataObject
{   
    
     //List of customerIds with cama separated values come's in as request
    public Map<string,string> reqIdMap  {get; set;}
    
    public  Map<string,Opportunity> oppMap;
    public  Map<string,List<SLF_Credit__c>> creditMap;
    public  Map<string,List<Underwriting_File__c>> UnderwritingMap ;
    public  Map<Id,List<dsfs__DocuSign_Status__c>> DocuSignMap ;
    public StatusDataObject(){
        oppMap = new Map<string,Opportunity> ();
        UnderwritingMap = new Map<string,List<Underwriting_File__c>>(); 
        DocuSignMap = new Map<Id,List<dsfs__DocuSign_Status__c>>();
        creditMap = new  Map<string,List<SLF_Credit__c>>();
    }
    
}