@IsTest
public class jsonSightenQuoteResponse2_Test {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{'+
		'	\"data\": {'+
		'		\"avg_monthly_solar_bill\": 100.98198817067849,'+
		'		\"install_cost_cash\": 4.01,'+
		'		\"uuid\": \"0258a70c-c2bc-4dc3-994f-87a578830ad5\",'+
		'		\"status\": \"QUOT\",'+
		'		\"incentives\": ['+
		'			{'+
		'				\"override\": false,'+
		'				\"date_created\": \"2016-10-26T21:09:00.294753+00:00\",'+
		'				\"incentive_type\": \"ITC\",'+
		'				\"created_by\": {'+
		'					\"link\": \"\",'+
		'					\"uuid\": \"29e705c4-5666-4766-a028-42173c6d22ec\",'+
		'					\"natural_id\": \"sunlightvision@yopmail.com\"'+
		'				},'+
		'				\"annual_amounts\": ['+
		'					6015'+
		'				],'+
		'				\"uuid\": \"bd17930b-b4eb-4168-b268-877b46641d14\",'+
		'				\"date_updated\": \"2016-10-26T21:09:00.298825+00:00\",'+
		'				\"owned_by_user\": {'+
		'					\"link\": \"\",'+
		'					\"uuid\": \"29e705c4-5666-4766-a028-42173c6d22ec\",'+
		'					\"natural_id\": \"sunlightvision@yopmail.com\"'+
		'				},'+
		'				\"upfront_amount\": 6015,'+
		'				\"natural_id\": \"QuoteIncentive object\",'+
		'				\"owned_by_organization\": {'+
		'					\"link\": \"\",'+
		'					\"uuid\": \"e3043913-0b57-4f2c-8f7e-b8b367c0e925\",'+
		'					\"natural_id\": \"Vision Solar\"'+
		'				},'+
		'				\"assigned_to\": \"S\",'+
		'				\"modified_by\": {'+
		'					\"link\": \"\",'+
		'					\"uuid\": \"29e705c4-5666-4766-a028-42173c6d22ec\",'+
		'					\"natural_id\": \"sunlightvision@yopmail.com\"'+
		'				}'+
		'			}'+
		'		],'+
		'		\"proposals\": {'+
		'			\"link\": \"\"'+
		'		},'+
		'		\"owned_by_user\": {'+
		'			\"link\": \"\",'+
		'			\"uuid\": \"29e705c4-5666-4766-a028-42173c6d22ec\",'+
		'			\"natural_id\": \"sunlightvision@yopmail.com\"'+
		'		},'+
		'		\"product\": {'+
		'			\"link\": \"/api/solar/product/65a9da63-15a8-4436-8fab-94e90637f616\",'+
		'			\"uuid\": \"65a9da63-15a8-4436-8fab-94e90637f616\",'+
		'			\"natural_id\": \"enVision 20 6.99%\"'+
		'		},'+
		'		\"date_created\": \"2016-10-26T21:08:59.043634+00:00\",'+
		'		\"owned_by_organization\": {'+
		'			\"link\": \"\",'+
		'			\"uuid\": \"e3043913-0b57-4f2c-8f7e-b8b367c0e925\",'+
		'			\"natural_id\": \"Vision Solar\"'+
		'		},'+
		'		\"modified_by\": {'+
		'			\"link\": \"\",'+
		'			\"uuid\": \"29e705c4-5666-4766-a028-42173c6d22ec\",'+
		'			\"natural_id\": \"sunlightvision@yopmail.com\"'+
		'		},'+
		'    		\"product_eligibility\": {'+
		'			\"eligible\": ['+
		'				{'+
		'					\"reasons\": {},'+
		'					\"name\": \"enVision 20 6.99%\",'+
		'					\"qual_def_id\": 3,'+
		'					\"message\": null,'+
		'					\"uuid\": \"65a9da63-15a8-4436-8fab-94e90637f616\"'+
		'				}'+
		'			],'+
		'			\"ineligible\": [],'+
		'			\"user\": \"sunlightvision@yopmail.com\",'+
		'			\"pending\": [],'+
		'			\"warning\": []'+
		'		},'+
		'		\"natural_id\": \"Pricing APItestLN//enVision 20 6.99%\",'+
		'		\"tasks\": {'+
		'			\"link\": \"/api/solar/task/?quote_id=50922\"'+
		'		},'+
		'		\"system\": {'+
		'			\"link\": \"/api/solar/quotegen/system/29e38332-f64e-44ca-a097-7676a475e4f1\",'+
		'			\"uuid\": \"29e38332-f64e-44ca-a097-7676a475e4f1\",'+
		'			\"natural_id\": \"System 29e38332\"'+
		'		},'+
		'		\"consumer_savings_usd\": -5.224794951972815,'+
		'		\"rate_esc_pct\": 0,'+
		'		\"consumer_savings_pct\": -4.94036839760672,'+
		'		\"date_updated\": \"2016-10-26T21:09:00.279586+00:00\",'+
		'		\"contract_term\": 20,'+
		'		\"amount_financed\": 19050,'+
		'		\"rate_contract\": 0.26928530178847593,'+
		'		\"milestones\": {'+
		'			\"link\": \"/api/solar/milestone/?quote_id=50922\"'+
		'		},'+
		'		\"created_by\": {'+
		'			\"link\": \"\",'+
		'			\"uuid\": \"29e705c4-5666-4766-a028-42173c6d22ec\",'+
		'			\"natural_id\": \"sunlightvision@yopmail.com\"'+
		'		},'+
		'		\"initial_payment\": 1000'+
		'	},'+
		'	\"status_code\": 200,'+
		'	\"messages\": {'+
		'		\"critical\": [],'+
		'		\"info\": [],'+
		'		\"warning\": [],'+
		'		\"error\": []'+
		'	}'+
		'}';
		jsonSightenQuoteResponse2 r = jsonSightenQuoteResponse2.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Incentives objIncentives = new jsonSightenQuoteResponse2.Incentives(System.JSON.createParser(json));
		System.assert(objIncentives != null);
		System.assert(objIncentives.override_Z == null);
		System.assert(objIncentives.date_created == null);
		System.assert(objIncentives.incentive_type == null);
		System.assert(objIncentives.created_by == null);
		System.assert(objIncentives.annual_amounts == null);
		System.assert(objIncentives.uuid == null);
		System.assert(objIncentives.date_updated == null);
		System.assert(objIncentives.owned_by_user == null);
		System.assert(objIncentives.upfront_amount == null);
		System.assert(objIncentives.natural_id == null);
		System.assert(objIncentives.owned_by_organization == null);
		System.assert(objIncentives.assigned_to == null);
		System.assert(objIncentives.modified_by == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Messages objMessages = new jsonSightenQuoteResponse2.Messages(System.JSON.createParser(json));
		System.assert(objMessages != null);
		System.assert(objMessages.critical == null);
		System.assert(objMessages.info == null);
		System.assert(objMessages.warning == null);
		System.assert(objMessages.error == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2 objRoot = new jsonSightenQuoteResponse2(System.JSON.createParser(json));
		System.assert(objRoot != null);
		System.assert(objRoot.data == null);
		System.assert(objRoot.status_code == null);
		System.assert(objRoot.messages == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Reasons objReasons = new jsonSightenQuoteResponse2.Reasons(System.JSON.createParser(json));
		System.assert(objReasons != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Data objData = new jsonSightenQuoteResponse2.Data(System.JSON.createParser(json));
		System.assert(objData != null);
		System.assert(objData.avg_monthly_solar_bill == null);
		System.assert(objData.install_cost_cash == null);
		System.assert(objData.uuid == null);
		System.assert(objData.status == null);
		System.assert(objData.incentives == null);
		System.assert(objData.proposals == null);
		System.assert(objData.owned_by_user == null);
		System.assert(objData.product == null);
		System.assert(objData.date_created == null);
		System.assert(objData.owned_by_organization == null);
		System.assert(objData.modified_by == null);
		System.assert(objData.product_eligibility == null);
		System.assert(objData.natural_id == null);
		System.assert(objData.tasks == null);
		System.assert(objData.system_Z == null);
		System.assert(objData.consumer_savings_usd == null);
		System.assert(objData.rate_esc_pct == null);
		System.assert(objData.consumer_savings_pct == null);
		System.assert(objData.date_updated == null);
		System.assert(objData.contract_term == null);
		System.assert(objData.amount_financed == null);
		System.assert(objData.rate_contract == null);
		System.assert(objData.milestones == null);
		System.assert(objData.created_by == null);
		System.assert(objData.initial_payment == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Created_by objCreated_by = new jsonSightenQuoteResponse2.Created_by(System.JSON.createParser(json));
		System.assert(objCreated_by != null);
		System.assert(objCreated_by.link == null);
		System.assert(objCreated_by.uuid == null);
		System.assert(objCreated_by.natural_id == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Proposals objProposals = new jsonSightenQuoteResponse2.Proposals(System.JSON.createParser(json));
		System.assert(objProposals != null);
		System.assert(objProposals.link == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Eligible objEligible = new jsonSightenQuoteResponse2.Eligible(System.JSON.createParser(json));
		System.assert(objEligible != null);
		System.assert(objEligible.reasons == null);
		System.assert(objEligible.name == null);
		System.assert(objEligible.qual_def_id == null);
		System.assert(objEligible.message == null);
		System.assert(objEligible.uuid == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		jsonSightenQuoteResponse2.Product_eligibility objProduct_eligibility = new jsonSightenQuoteResponse2.Product_eligibility(System.JSON.createParser(json));
		System.assert(objProduct_eligibility != null);
		System.assert(objProduct_eligibility.eligible == null);
		System.assert(objProduct_eligibility.ineligible == null);
		System.assert(objProduct_eligibility.user == null);
		System.assert(objProduct_eligibility.pending == null);
		System.assert(objProduct_eligibility.warning == null);
	}
}