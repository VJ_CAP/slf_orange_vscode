global without sharing class CreditMailsToBox 
{
     global class Input {
        @InvocableVariable(required=true) global String templateName;
        @InvocableVariable(required=true) global Id crditId;
        global Input() {
            //  nothing to do
        }
        global Input(Id crditId, String templateName) {
            this.crditId = (String.valueOf(crditId)).substring(0,15);
            this.templateName = templateName;
        }
    }
    @InvocableMethod(label='Upload Credit Email To Box')
    global static void uploadToBox(List<Input> Inputs)
    {
        List<SObject> SObjList = new List<SObject>();
        
        Input ip = Inputs[0];
        
        Id sObjId = ip.crditId;
        Schema.SObjectType token = sObjId.getSObjectType();
        Schema.DescribeSObjectResult dr = token.getDescribe();
        
         system.debug('****token***'+token);
        system.debug('****sObjectName***'+dr);
        
        //Updated By Adithya on 8/23/2019 as part of ORANGE-6897
        //Start
        string strQuery = '';
        Id oppId = null; 
        system.debug('****sObjectName*22**'+dr.getName());
        if (dr.getName() == 'Draw_Requests__c')
        {
            strQuery = 'select Id,Underwriting__r.Opportunity__c from '+dr.getName()+' where Id =: sObjId';   
        }else{
            strQuery = 'select Id,Opportunity__c from '+dr.getName()+' where Id =: sObjId';  
        }
        
        system.debug('****strQuery***'+strQuery);
        SObjList = database.query(strQuery);
        system.debug('****SObjList***'+SObjList);
        if(!SObjList.isEmpty())
        {
            SObject objRec = SObjList[0];
            if(dr.getName() == 'Draw_Requests__c')
                oppId = (Id)objRec.getSobject('Underwriting__r').get('Opportunity__c');
            else
                oppId = (Id)objRec.get('Opportunity__c');
        }
        //End
        
        List<EmailTemplate> emailTemplate = new List<EmailTemplate>();
        emailTemplate = [SELECT Id, DeveloperName, Subject, TemplateType FROM EmailTemplate WHERE DeveloperName = :ip.templateName];
        List<Contact> conList = new List<Contact>();
        conList = [SELECT Id, Email FROM Contact WHERE Name = 'Sunlight Financial' AND Email <> null LIMIT 1];
        System.debug('SObjList::'+SObjList);
        System.debug('emailTemplate::'+emailTemplate);
        System.debug('conList::'+conList);
        System.debug('conList::'+conList[0].email);
        if(emailTemplate.size() > 0 && !SObjList.isEmpty() && !conList.isEmpty() && oppId != null)
        {
            List<Attachment> attachList = new List<Attachment>();
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg.setTemplateId(emailTemplate[0].Id);
            msg.setTargetObjectId(conList[0].Id);
            msg.setSaveAsActivity(false);
            msg.setTreatTargetObjectAsRecipient(true);
            msg.setWhatId(SObjList[0].Id);

            Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
            Database.rollback(sp);
            
            string body= '';
            //string body = msg.getPlainTextBody().replace('\n', '\r\n');
            if(emailTemplate[0].TemplateType != 'Text'){
                body = msg.getHTMLBody();
                body = '<html>' + body + '</html>';
                system.debug('**SZ: line breaks = ' + body.indexOf('\r\n'));
            }else{
                body = msg.getPlainTextBody();
            }
            system.debug('**SZ: body = ' + body);
            System.debug('msg.getSubject()::'+msg.getSubject());
            
            attachList.add(new Attachment(
                Name = msg.getSubject() +' ' + (Datetime.now().format('yyyyMMdd_hhmmss')) + '.html',
                ContentType = 'text/html',
                Body = Blob.valueOf(body),
                ParentId = oppId
            ));

            if (attachList.size() > 0)
            {
                insert attachList;
            }

            Set<Id> attachmentIds = new Set<Id>();
            for(Attachment att : attachList)
            {
                attachmentIds.add(att.Id);
            }
            AutomatedLoanConfirmationEmail.SendAttachmentsToBox (attachmentIds, true);
            
            if(ip.templateName == 'Credit_Approval_Applicant'){               
                CreditApprovalAttachmentController.sendAttachment(SObjList[0].Id,(Id)SObjList[0].get('Opportunity__c'));            
            }
            
        }
    }
}