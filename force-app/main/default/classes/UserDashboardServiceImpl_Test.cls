/******************************************************************************************
* Description: Test class to cover all REST services 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Venkatesh            11/16/2017          Created
******************************************************************************************/
@isTest
public class UserDashboardServiceImpl_Test {
    @testsetup static void createtestdata(){
        // SLFUtilityDataSetup.initData();
        insertUserData();
    }
    private static void insertUserData(){
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();   
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        insert trgLst;
                   
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        
        
        List<Contact> conList=new List<Contact>();
        
        Contact conExec = new Contact(LastName ='conExec',AccountId = acc.Id);
        insert conExec;
        Contact conManager = new Contact(LastName ='conManager',AccountId = acc.Id);
         insert conManager;
        Contact conUser = new Contact(LastName ='conUser',AccountId = acc.Id,ReportsToId=conManager.id);
        insert conUser;

        
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
            system.debug('#### roleMap-->:'+role.DeveloperName+'### role id-->:'+ role.Id);
        }
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        List<user> lstUser = new List<user>();
        User portalexecUser = new User(alias = 'testc', 
                                   Email='testexecutive@mail.com', 
                                   IsActive = true,
                                   Hash_Id__c='111',
                                   EmailEncodingKey='UTF-8', 
                                   LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', 
                                   ProfileId = p.Id, 
                                   //UserRoleId = roleMap.get('Test Account Partner Executive'),
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testexecutive@mail.com',
                                   ContactId = conExec.Id);
                                   if(roleMap.containsKey(portalexecUser.UserRoleId) ){
                                       portalexecUser.UserRoleId = roleMap.get('Test Account Partner Executive');
                                   }
        lstUser.add(portalexecUser);
        system.debug('#### portalexecUser-->:'+portalexecUser);
        User portalMangUser = new User(alias = 'testc', 
                                   Email='testManager@mail.com', 
                                   IsActive = true,
                                   Hash_Id__c='222',
                                   EmailEncodingKey='UTF-8', 
                                   LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', 
                                   ProfileId = p.Id, 
                                   //UserRoleId = roleMap.get('Test Account Partner Manager'),
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testManager@mail.com',
                                   ContactId = conManager.Id);
                                   /*if(roleMap.containsKey(portalexecUser.UserRoleId) ){
                                       portalMangUser.UserRoleId = roleMap.get('Test Account Partner Manager');
                                   }*/
        lstUser.add(portalMangUser);
        system.debug('#### portalMangUser-->:'+portalMangUser);
        User portalUser = new User(alias = 'testc', 
                                   Email='testUser@mail.com', 
                                   IsActive = true,
                                   Hash_Id__c='333',
                                   EmailEncodingKey='UTF-8', 
                                   LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', 
                                   ProfileId = p.Id, 
                                   //UserRoleId = roleMap.get('Test Account Partner User'),
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testPartnerUser@mail.com',
                                   ContactId = conUser.Id);
                                   /*if(roleMap.containsKey(portalexecUser.UserRoleId) ){
                                       portalUser.UserRoleId = roleMap.get('Test Account Partner User');
                                   }*/
        lstUser.add(portalUser);
        system.debug('#### portalUser-->:'+portalUser);
        user adminUser=[SELECT id FROM User WHERE id=:UserInfo.getUserId()];
        system.runAs(adminUser){
            insert lstUser;
        }
        
        
        
        system.debug('#### roleMap-->:'+roleMap);
    }
   private testMethod static void UserDashboardExceTest(){

        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testexecutive@mail.com'];
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
        String userDashBrd;
        Test.starttest();
        userDashBrd = '{"userID":""}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,userDashBrd,'getUserDashboardDetails');

        Test.StopTest();

        }
    }
    private testMethod static void UserDashboardMangTest(){

        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testManager@mail.com'];
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
        String userDashBrd;
        Test.starttest();
        userDashBrd = '{"userID":""}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,userDashBrd,'getUserDashboardDetails');

        Test.StopTest();

        }
    }
    private testMethod static void UserDashboardUserTest(){

        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testUser@mail.com'];
        Map<String, String> userMap = new Map<String, String>();
        System.runAs(loggedInUsr[0])
        {
        String userDashBrd;
        Test.starttest();
        userDashBrd = '{"userID":""}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,userDashBrd,'getUserDashboardDetails');
        userDashBrd = '{"userID":"12356"}';
        SLFRestDispatchTest.MapWebServiceURI(userMap,userDashBrd,'getUserDashboardDetails');
        Test.StopTest();

        }
    }

    private testMethod static void UserDashboardNegtiveTest(){

        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId, LoginCount__c from User where Email =: 'testUser@mail.com'];
        UserDashboardServiceImpl userDashBrdSerImpl = new UserDashboardServiceImpl();
        UserDashboardDataServiceImpl userDashBrdData = new UserDashboardDataServiceImpl();
        System.runAs(loggedInUsr[0])
        {
        
        Test.starttest();
            userDashBrdSerImpl.fetchData(null);
            userDashBrdData.transformOutput(null);
        Test.StopTest();

        }
    }
}