@isTest(seeAllData=false)
public class SalesOperationReportingServiceImplTest{    
    
    private testMethod static void reportTest(){
       
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='Contact';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        TriggerFlags__c trgflag5 = new TriggerFlags__c();
        trgflag5.Name ='User';
        trgflag5.isActive__c =true;
        trgrList.add(trgflag5);
        TriggerFlags__c trgflag6 = new TriggerFlags__c();
        trgflag6.Name ='Underwriting_File__c';
        trgflag6.isActive__c =true;
        trgrList.add(trgflag6);
        TriggerFlags__c trgflag7 = new TriggerFlags__c();
        trgflag7.Name ='Funding_Data__c';
        trgflag7.isActive__c =true;
        trgrList.add(trgflag7);
        
        insert trgrList;
        
        SF_Category_Map__c sfmap = new SF_Category_Map__c();
        sfmap.Name = 'Loan Agreement';
        sfmap.Folder_Name__c = 'Loan Agreements';
        sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
        sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
        sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'LT_Loan_Agreement_Received__c';
        insert sfmap;
        
        SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                         Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                         TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
        insert cm1;
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account150';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';        
        accObj.Type = 'Facility'; 
        accObj.Prequal_Enabled__c = true; 
        accObj.Website = 'www.google.com';  
        accObj.Solar_Enabled__c = true;     
        accObj.Kitting_Ever_Enabled__c = true;
        accObj.Inspection_Ever_Enabled__c = true;
        accObj.Permit_Ever_Enabled__c = true;
        insert accObj;
        
        List<Contact> conList = new List<Contact>();
        Contact con = new Contact(LastName ='testCon',AccountId = accObj.Id);
        conList.add(con);
        Contact con1 = new Contact(LastName ='testCon1',AccountId = accObj.Id);
        conList.add(con1);
        Contact con2 = new Contact(LastName ='testCon2',AccountId = accObj.Id,ReportsToId = con1.Id);
        conList.add(con2);
        insert conList;  

        String reportAPIRefactor;
        Map<String, String> createincenMap = new Map<String, String>();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];

        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        List<User> userList = new List<User>();
       
            User portalUser2 = new User(alias = 'testcccc', Email='testcccc@mail.com', IsActive = true,Hash_Id__c='123',
            EmailEncodingKey='UTF-8', LastName='test', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            UserRoleId = roleMap.get('Top Role'),
            timezonesidkey='America/Los_Angeles', 
            username='testclassss@mail.com',ContactId = con1.Id);            
            userList.add(portalUser2);
                       
            
            User uObj = [select id from User where id = :UserInfo.getUserID()];
            System.runAs(uObj){
                if(!userList.isEmpty()){
                    insert userList;
                    System.debug('userList::'+userList);
                }
            }
            Test.startTest();      
           
            
            System.runAs(portalUser2){
               List<Opportunity> oppList = new List<Opportunity>();
               Opportunity oppObj = new Opportunity(Name = 'OppName',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Qualified',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj); 
                
                Opportunity oppObj2 = new Opportunity(Name = 'OppName',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Qualified',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Project_Category__c = 'Home',
                                             Install_City__c='California');
                oppList.add(oppObj2);      
                insert oppList;                
                
                String startDateStr = String.valueOf(System.today().year())+'-'+String.valueOf(System.today().month())+'-'+String.valueOf(System.today().day());
                String endDateStr = String.valueOf(System.today().year())+'-'+String.valueOf(System.today().month())+'-'+String.valueOf(System.today().day());
                
                System.debug('startDateStr ::'+startDateStr);
                System.debug('endDateStr ::'+endDateStr);
                
                reportAPIRefactor = '{"startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","includeHierarchy": false,"type": "sales"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');
               
                reportAPIRefactor = '{"startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","includeHierarchy": true,"type": "Operations"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');
                
                reportAPIRefactor = '{"userId":"'+oppObj.Id+'","startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","includeHierarchy": false,"type": "sales"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');
                
                reportAPIRefactor = 'tyyuuuu';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');
                
                reportAPIRefactor = '{"startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","includeHierarchy": true,"type": "Operations","projectCategory":"Home"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');    
                                
            }
                       
            Test.stopTest();               
    }
    private testMethod static void reportTest2(){
       
        List<TriggerFlags__c> trgrList = new List<TriggerFlags__c>();
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        trgrList.add(trgflag1);
        TriggerFlags__c trgflag2 = new TriggerFlags__c();
        trgflag2.Name ='Opportunity';
        trgflag2.isActive__c =true;
        trgrList.add(trgflag2);
        TriggerFlags__c trgflag4 = new TriggerFlags__c();
        trgflag4.Name ='Contact';
        trgflag4.isActive__c =true;
        trgrList.add(trgflag4);
        TriggerFlags__c trgflag5 = new TriggerFlags__c();
        trgflag5.Name ='User';
        trgflag5.isActive__c =true;
        trgrList.add(trgflag5);
        TriggerFlags__c trgflag6 = new TriggerFlags__c();
        trgflag6.Name ='Underwriting_File__c';
        trgflag6.isActive__c =true;
        trgrList.add(trgflag6);
        TriggerFlags__c trgflag7 = new TriggerFlags__c();
        trgflag7.Name ='Funding_Data__c';
        trgflag7.isActive__c =true;
        trgrList.add(trgflag7);
        insert trgrList;
        
        SF_Category_Map__c sfmap = new SF_Category_Map__c();
        sfmap.Name = 'Loan Agreement';
        sfmap.Folder_Name__c = 'Loan Agreements';
        sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
        sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
        sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'LT_Loan_Agreement_Received__c';
        insert sfmap;
        
        SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                         Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                         TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
        insert cm1;
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account150';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CVX';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';        
        accObj.Type = 'Facility'; 
        accObj.Prequal_Enabled__c = true; 
        accObj.Website = 'www.google.com';  
        accObj.Solar_Enabled__c = true;     
        accObj.Kitting_Ever_Enabled__c = true;
        accObj.Inspection_Ever_Enabled__c = true;
        accObj.Permit_Ever_Enabled__c = true;
        insert accObj;
        
        List<Contact> conList = new List<Contact>();
        Contact con = new Contact(LastName ='testCon',AccountId = accObj.Id);
        conList.add(con);
        Contact con1 = new Contact(LastName ='testCon1',AccountId = accObj.Id);
        conList.add(con1);
        Contact con2 = new Contact(LastName ='testCon2',AccountId = accObj.Id,ReportsToId = con1.Id);
        conList.add(con2);
        insert conList;  

        String reportAPIRefactor;
        Map<String, String> createincenMap = new Map<String, String>();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];

        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        
        List<User> userList = new List<User>();       
            
        User portalUser3 = new User(alias = 'testccc', Email='testccc@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        timezonesidkey='America/Los_Angeles', 
        username='testclasssss@mail.com',ContactId = con2.Id);            
        userList.add(portalUser3);
        
        User uObj = [select id from User where id = :UserInfo.getUserID()];
        System.runAs(uObj){
            if(!userList.isEmpty()){
                insert userList;
                System.debug('userList::'+userList);
            }
        }
        Test.startTest();                         
        
        System.runAs(portalUser3){
                List<Opportunity> oppList = new List<Opportunity>();
                Opportunity oppObj1 = new Opportunity(Name = 'OppName1',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='New',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj1);
                Opportunity oppObj2 = new Opportunity(Name = 'OppName2',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='New',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj2);
                                
                Opportunity oppObj4 = new Opportunity(Name = 'OppName4',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='New',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj4); 
                Opportunity oppObj5 = new Opportunity(Name = 'OppName5',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Credit Application Sent',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj5); 
                Opportunity oppObj6 = new Opportunity(Name = 'OppName6',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Credit Pending Review',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj6); 
                Opportunity oppObj7 = new Opportunity(Name = 'OppName7',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Credit Approved',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj7);
                Opportunity oppObj8 = new Opportunity(Name = 'OppName8',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Loan Agreement Sent',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj8);
                Opportunity oppObj9 = new Opportunity(Name = 'OppName9',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Closed Won',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California');
                oppList.add(oppObj9);
                
                Opportunity oppObj10 = new Opportunity(Name = 'OppName10',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Closed Won',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California',
                                             Project_Category__c = 'Home');
                oppList.add(oppObj10);
                
                Opportunity oppObj11 = new Opportunity(Name = 'OppName11',
                                             AccountId=accObj.Id,
                                             Co_Applicant__c = accObj.Id,
                                             StageName='Closed Won',
                                             CloseDate=system.today(),
                                             Installer_Account__c = accObj.Id, 
                                             Install_State_Code__c = 'CA',
                                             Synced_Quote_Id__c = 'qId',
                                             Desync_Bypass__c = true,
                                             Install_Street__c='40 CORTE ALTA',
                                             Install_Postal_Code__c='94949',
                                             Install_City__c='California',
                                             Project_Category__c = 'Home');
                oppList.add(oppObj11);
                
                insert oppList;
                
                List<Underwriting_File__c> uwList = new List<Underwriting_File__c>();
                for(Opportunity opp:oppList){
                    if(opp.Name == 'OppName2'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Withdraw_Reason__c = 'Credit Expired',Declined_Withdrawn_Date__c = system.today(),Opportunity__c = opp.Id,Project_Status__c = 'Project Withdrawn');
                        uwList.add(uwObj);
                    }
                    if(opp.Name == 'OppName1'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status_Detail__c  = 'M2 - In Review');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName8'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status_Detail__c = 'M0 - Documents Needed');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName4'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status_Detail__c = 'M0 - In Review');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName5'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status__c = 'M2 Payment Pending');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName6'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status_Detail__c = 'M1 - Documents Needed');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName7'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status_Detail__c = 'M1 - In Review');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName9'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status_Detail__c = 'M2 - Documents Needed');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName10'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status__c = 'Documents Needed');
                        uwList.add(uwObj);
                    }
                    else if(opp.Name == 'OppName11'){
                        Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id,Project_Status__c = 'Loan Agreement Review');
                        uwList.add(uwObj);
                    }
                    else{
                         Underwriting_File__c uwObj = new Underwriting_File__c(Opportunity__c = opp.Id);
                         uwList.add(uwObj);
                    }
                }
                insert uwList;
                
                List<Underwriting_File__c> undrWrtngList = new List<Underwriting_File__c>();
                undrWrtngList = [Select id, createdDate from Underwriting_File__c];
                System.debug('undrWrtngList:::'+undrWrtngList);
                
                Prequal__c preqlObj = new Prequal__c(Opportunity__c = oppObj1.Id,Pre_Qual_Status__c='Auto Approved');
                insert preqlObj;
                
                                
                DateTime dt = System.now();
                if(!undrWrtngList.isEmpty()){
                    System.debug('undrWrtngList[0].createdDate::'+undrWrtngList[0].createdDate);
                    dt = undrWrtngList[0].createdDate;
                }    
                
                String endDateStr = String.valueOf(dt.year())+'-'+String.valueOf(dt.addMonths(1).month())+'-'+String.valueOf(dt.day());               
                String startDateStr = String.valueOf(dt.year())+'-'+String.valueOf(dt.addMonths(-2).month())+'-'+String.valueOf(dt.day());
                
                System.debug('startDateStr ::'+startDateStr);
                System.debug('endDateStr ::'+endDateStr);
                
                reportAPIRefactor = '{"startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","includeHierarchy": true,"type": "sales"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');
               
                reportAPIRefactor = '{"startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","type": "Operations"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');
                
                reportAPIRefactor = '{"userId":"'+oppObj1.Id+'","startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","includeHierarchy": true,"type": "sales"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');
                
                reportAPIRefactor = 'tyyuuuu';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');  
                
                reportAPIRefactor = '{"startDate": "'+ startDateStr +'","endDate": "'+ endDateStr +'","includeHierarchy": true,"type": "Operations","projectCategory":"Home"}';
                SLFRestDispatchTest.MapWebServiceURI(createincenMap,reportAPIRefactor,'getReportDetails');             
            }
        Test.stopTest();               
    }
}