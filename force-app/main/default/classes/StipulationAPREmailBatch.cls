/**
* Description: To send an email on every tuesday, if APR created is still open after creation of 5 days.
*
* Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Suma                28/06/2019              Created.
---------------------------------------------------------------------------

******************************************************************************************/
global class StipulationAPREmailBatch implements Database.Batchable<sobject>,Schedulable{
    private String query; //Query in the start method
    /**
* Description: Entry of batch and execute a SOQL for APR records.
*/
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('### Entered into start() of '+ StipulationAPREmailBatch.class);
        String status = 'Completed'; String sData = 'APR';  datetime crDate = system.today().addDays(-5); 
        
        return Database.getQueryLocator('SELECT id,Case__r.Opportunity_Hash_Id__c,Case__r.Co_Applicant_info__c,Case__r.Applicant_Co_Applicant_Name__c, Case__r.Stipulation_Email_Text__c, Case__r.Installer_Display_Name__c,Underwriting__r.Opportunity__r.account.name, Underwriting__r.Opportunity__r.Primary_Applicant_Email__c,status__c,Underwriting__r.Opportunity__r.ACH_APR__c, Underwriting__r.Opportunity__r.NonACH_APR__c From Stipulation__c WHERE CreatedDate <='+ crDate.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'') +' AND Underwriting__r.Opportunity__r.ACH_APR_Process_Required__c = true AND Underwriting__r.M1_Review_Complete_Date__c = NULL AND Status__c != \''+String.escapeSingleQuotes(status)+'\' ' +'AND Stipulation_Data__r.name = '+'\''+String.escapeSingleQuotes(sData)+'\' ' ); 
    }
    /**
* Description:
*/
    global void execute(Database.BatchableContext BC, List<Stipulation__c > scopeList){
        system.debug('Inside Execute method--'+scopeList);
        List<Messaging.SingleEmailMessage> lstEmails= new List<Messaging.SingleEmailMessage>();
        EmailTemplate emailTemplateRec = [SELECT Id,Name,HtmlValue,Subject,body FROM EmailTemplate WHERE DeveloperName ='Friendly_Reminder_Customer_Installer_Stipulation_Email_Upload'];         
        String emailBody = emailTemplateRec.HtmlValue; 
        try{  
            if(!scopeList.isEmpty()){
                
                for(Stipulation__c stip: scopeList){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    //     toAddress.add(stip.Underwriting__r.Opportunity__r.Primary_Applicant_Email__c); //commented for testing purpose  - 14/11/2019
                    List<string> toAddress = new List<string>();
                    toAddress.add('kalyani.konakalla@capgemini.com'); 
                    system.debug('toAddress:::'+toAddress);                
                    if(!toAddress.isEmpty())
                        mail.setToAddresses(toAddress);
                    mail.setSubject(emailTemplateRec.Subject); 
                    
                    emailBody = emailBody.replace('{!$Label.Credit_Application_link}',system.label.Credit_Application_link);
                    if(stip.Case__r.Opportunity_Hash_Id__c != null)
                        emailBody = emailBody.replace('{!URLENCODE(Case.Opportunity_Hash_Id__c)}', EncodingUtil.urlEncode(stip.Case__r.Opportunity_Hash_Id__c,'UTF-8'));
                    emailBody = emailBody.replace('{!Contact.Name}', stip.Underwriting__r.Opportunity__r.Account.Name);                
                    emailBody = emailBody.replace('{!Case.Installer_Display_Name__c}',stip.Case__r.Installer_Display_Name__c);
                    if(stip.Case__r.Stipulation_Email_Text__c!=null)
                    emailBody = emailBody.replace('{!Case.Stipulation_Email_Text__c}',stip.Case__r.Stipulation_Email_Text__c);  
                    system.debug('EmailBody==='+emailBody);   
                    if(stip.Underwriting__r.Opportunity__r.ACH_APR__c!=null) //Added null condition-by kalyani - 14/11/2019
                        emailBody = emailBody.replace('<ACH APR>', String.valueOf(stip.Underwriting__r.Opportunity__r.ACH_APR__c.setScale(2))+'%');  //Added setScale as part of 14776
                    if(stip.Underwriting__r.Opportunity__r.NonACH_APR__c!=null)//Added null condition-by kalyani - 14/11/2019
                        emailBody = emailBody.replace('<NON ACH APR>', String.valueOf(stip.Underwriting__r.Opportunity__r.NonACH_APR__c.setScale(2))+'%');  //Added setScale as part of 14776
                    if(stip.case__r.Co_Applicant_info__c != null && !String.isEmpty(stip.case__r.Co_Applicant_info__c))
                        emailBody = emailbody.replace('{!Case.Co_Applicant_info__c}',stip.case__r.Co_Applicant_info__c);              
                    else
                        emailBody = emailbody.replace('{!Case.Co_Applicant_info__c}', '');
                    
                    emailBody = emailBody.replace('<![CDATA[','');
                    emailBody = emailBody.replace(']]>','');
                    System.debug('email body'+emailbody);
                    
                    mail.setHtmlBody(emailBody);
                    lstEmails.add(mail); 
                }
            }
            if(!lstEmails.isEmpty()){
                Messaging.sendEmail(lstEmails,false);  
            }
        }catch(Exception err){
            ErrorLogUtility.writeLog('StipulationAPREmailBatch.execute()',err.getLineNumber(),'execute()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));       
        }
        
    }
    global void Finish(Database.BatchableContext bc){
    }
    //Scduling the batch
    global void execute(SchedulableContext sc) {
        StipulationAPREmailBatch stipBatch = new StipulationAPREmailBatch();
        database.executebatch(stipBatch);
    }
}