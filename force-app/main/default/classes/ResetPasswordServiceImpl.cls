/**
* Description: Applicant API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Suresh Kumar           06/12/2017           Created
******************************************************************************************/
public without sharing class ResetPasswordServiceImpl extends RESTServiceBase {
    private String msg = '';
    
    /**
* Description:    
*          
*
* @param reqAP      Get all POST request action parameters
* @param reqData    Get all POST request data
*
* return res        Send response back with data and response status code
*/
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('Entered into fetchData() of ' + ResetPasswordServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            UnifiedBean respBean = new UnifiedBean();
        
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            if(reqData.users != null)
            {
                if(null == reqData.users[0].hashId || String.isEmpty(reqData.users[0].hashId)){
                    errMsg = ErrorLogUtility.getErrorCodes('229',null);
                    gerErrorMsg(respBean,errMsg,'229');
                }
                
                if(null == reqData.users[0].newPassword || String.isEmpty(reqData.users[0].newPassword)){
                    errMsg = ErrorLogUtility.getErrorCodes('391',null);
                    gerErrorMsg(respBean,errMsg,'391');
                }
                
                if(string.isNotBlank(errMsg))
                {                   
                    iRestRes = (IRestResponse)respBean;
                    res.response= iRestRes;
                    return res.response;
                }
                
                if(null != reqData.users[0].hashId && String.isNotEmpty(reqData.users[0].hashId) && null != reqData.users[0].newPassword && String.isNotEmpty(reqData.users[0].newPassword)){
                    try{
                        List<User> lstUser = [select id,IsActive,Hash_Id__c,contactId,contact.Reset_Password_Flag__c from user where Hash_Id__c=:reqData.users[0].hashId and IsActive=true];
                        if(lstUser!=null && lstUser.size()>0){
                            User objUser = lstUser.get(0);
                            
                            try{
                                system.setPassword(objUser.Id, reqData.users[0].newPassword);
                                System.debug('### Password has been reset successfully. ');
                                Contact con = new Contact();
                                con.id = objUser.contactId;
                                con.Reset_Password_Flag__c = false;
                                update con;
                                
                                respBean.returnCode = '200';
                                respBean.message = 'Success! Password has been changed.';
                            }
                            catch(Exception exe){
                                String error = exe.getMessage();
                                if(error.contains('invalid repeated password')){
                                    errMsg = ErrorLogUtility.getErrorCodes('392',null);
                                    gerErrorMsg(respBean,errMsg,'392');
                                }
                                else{
                                    errMsg = ErrorLogUtility.getErrorCodes('400',null);
                                    gerErrorMsg(respBean,errMsg,'400');
                                }
                            }
                        }
                        else{
                            errMsg = ErrorLogUtility.getErrorCodes('400',null);
                            gerErrorMsg(respBean,errMsg,'400');
                        }
                        iRestRes = (IRestResponse)respBean;
                        res.response= iRestRes;
                    }catch(Exception exc){
                        System.debug('### Error occured: '+exc.getMessage());
                        errMsg = ErrorLogUtility.getErrorCodes('400',null);
                        gerErrorMsg(respBean,errMsg,'400');
                        iRestRes = (IRestResponse)respBean;
                        res.response= iRestRes;
                    }
                }   
            }else{
                errMsg= ErrorLogUtility.getErrorCodes('400',null);
                gerErrorMsg(respBean,errMsg,'400');
                iRestRes = (IRestResponse)respBean;
                res.response= iRestRes;
            }
        }catch(Exception err){
            
            system.debug('### ResetPasswordServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('ResetPasswordServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            errMsg= ErrorLogUtility.getErrorCodes('214',null);
            gerErrorMsg(respBean,errMsg,'214');
            iRestRes = (IRestResponse)respBean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + ResetPasswordServiceImpl.class);
        return res.response ;
    }
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        List<UnifiedBean.ErrorWrapper> errorList = new List<UnifiedBean.ErrorWrapper>();
        UnifiedBean.ErrorWrapper errorWraper = new UnifiedBean.ErrorWrapper();
        errorWraper.errorMessage = sErrorMsg;
        errorList.add(errorWraper);
        objUnifiedBean.error = errorList;
        objUnifiedBean.returnCode = sReturnCode;
    }
}