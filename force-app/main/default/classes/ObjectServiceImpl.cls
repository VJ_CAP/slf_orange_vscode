/**
* Description: Object service related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/
public with sharing class ObjectServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back in bean(JSON) format.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ObjectServiceImpl.class);
        ResponseWrapper res = new ResponseWrapper();
        ObjectDataServiceImpl objDataSrvImpl = new ObjectDataServiceImpl(); 
        try{
        system.debug('### rw.reqDataStr '+ rw.reqDataStr);
            ObjectRequestWrapper reqData = (ObjectRequestWrapper)JSON.deserialize(rw.reqDataStr, ObjectRequestWrapper.class);
            system.debug('### reqData' + reqData);
            res.response = (IRestResponse)objDataSrvImpl.transformOutput(reqData);

        }catch(Exception ex){
            system.debug('### ObjectServiceImpl.fetchData():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('ObjectServiceImpl.fetchData()',ex.getLineNumber(),'fetchData()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }
        system.debug('### Exit from fetchData() of ' + ObjectServiceImpl.class);
        return res.response ;
    }
}