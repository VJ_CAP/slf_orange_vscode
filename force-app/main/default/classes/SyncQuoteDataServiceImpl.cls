/**
* Description: Sync Quote related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh R                10/04/2017          Created
******************************************************************************************/

public class SyncQuoteDataServiceImpl extends RestDataServiceBase{
    
    /**
    * Description: Convert out response from salesforce sobject to wrapper (JSON)
    *
    * @param sObj           Convert the sobject record to bean.
    * @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
    * @param msg            Each record specific message, if success/fail.
    * return null           IRestResponse returns object specific bean based on the consumer.
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+SyncQuoteDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### reqData '+reqData);
        
        UnifiedBean respData = new UnifiedBean(); 
        respData.projects = new List<UnifiedBean.OpportunityWrapper>();
        //respData = reqData ;
        respData.error = new list<UnifiedBean.errorWrapper>();
        
        UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
        projectWrap.applicants = new List<UnifiedBean.ApplicantWrapper>();
        projectWrap.stips = new List<UnifiedBean.StipsAndStatusWrapper>();
        projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
        projectWrap.systemDesigns = new List<UnifiedBean.SystemDesignWrapper>();
        projectWrap.quotes = new List<UnifiedBean.QuotesWrapper>();
        Savepoint sp = Database.setSavepoint();
        IRestResponse iRestRes;
        string errMsg = '';
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try
        {   
        
            if(null != reqData.projects)
            {
                List<user> loggedInUsr = [select Id,contactid,contact.AccountId,contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,UserRole.Name from User where id =: userinfo.getUserId()];
                if(null != reqData.projects[0].quotes)
                {
                    List<UnifiedBean.QuotesWrapper> quoteRecLst = reqData.projects[0].quotes;
                    Map<string,Quote> quoteMap = new Map<string,Quote>();
                    List<Quote> quotelst = new List<Quote>();
                    set<Id> oppidSet = new set<Id>();
                    set<string> quoteSet = new set<string>();
                    set<Id> sdSet = new set<Id>();
                    List<System_Design__c> sdList = new List<System_Design__c>();
                    for(UnifiedBean.QuotesWrapper quoteIterate : quoteRecLst)
                    {
                        if(string.isNotBlank(quoteIterate.id))
                            quoteSet.add(quoteIterate.Id);
                    }
                     system.debug('### quoteSet '+quoteSet);
                    if(quoteSet.Size()>1)
                    {
                       errMsg = ErrorLogUtility.getErrorCodes('227',null);
                       iolist = new list<DataValidator.InputObject>{};
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        errMsg = errorMsg.errorMessage;
                        respData.error.add(errorMsg);
                        respData.returnCode = '227';
                        iRestRes = (IRestResponse)respData;
                        return iRestRes;
                    }
                    else if(!quoteSet.isEmpty())
                    {
                        quotelst = [Select id,Standard_Monthly_Payment_Max_Amount__c,Promo_Monthly_Payment_Max__c,Total_of_Payments_Max_Amount__c,Total_of_Payments_Loan_Amount__c,ACH_Monthly_Payment__c,Final_Monthly_Payment__c,ACH_Monthly_Payment_without_Prepay__c,ACH_Monthly_Payment_Interest_Only__c,NonACH_Monthly_Payment_IO__c,ACH_Escalated_Monthly_Payment_IO__c,NonACH_Escalated_Monthly_Payment_IO__c,ACH_Final_Monthly_Payment__c,ACH_FinalMonthlyPaymentWOPrepay__c,NonACH_Monthly_Payment__c,NonACH_Monthly_Payment_WO_Prepay__c,NonACH_FinalMonthlyPaymentWOPrepay__c,NonACH_Final_Monthly_Payment__c,isACH__c,Opportunityid, System_Design__c,SLF_Product__r.Expiration_Date__c,SLF_Product__r.Product_Loan_Type__c,SLF_Product__c,Non_Solar_Amount__c,Combined_Loan_Amount__c,opportunity.Project_Category__c,opportunity.Installer_Account__c,opportunity.StageName,Opportunity.Change_Order_Status__c from Quote where id IN: quoteSet];
                        
                        
                         
                        
                        system.debug('### quotelst '+quotelst);
                        
                        if(!quotelst.isEmpty())
                        {
                            for(Quote quoteIterate : quotelst)
                            {
                                if(quoteIterate.SLF_Product__r.Expiration_Date__c != null && quoteIterate.SLF_Product__r.Expiration_Date__c < system.today()){
                                    errMsg = ErrorLogUtility.getErrorCodes('362',null);
                                    iolist = new list<DataValidator.InputObject>{};
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    respData.error.add(errorMsg);
                                    respData.returnCode = '362';
                                    iRestRes = (IRestResponse)respData;
                                    return iRestRes;
                                }
                                string quoteId = string.ValueOf(quoteIterate.Id);
                                if(quoteId.length()>15)
                                    quoteId = quoteId.subString(0,15);
                                    
                                quoteMap.put(quoteId,quoteIterate);
                                oppidSet.add(quoteIterate.Opportunityid);
                            }
                        }else{
                            errMsg = ErrorLogUtility.getErrorCodes('228',null);
                            iolist = new list<DataValidator.InputObject>{};
                            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                            respData.error.add(errorMsg);
                            respData.returnCode = '228';
                            iRestRes = (IRestResponse)respData;
                            return iRestRes;
                        }
                    }else{
                        errMsg = ErrorLogUtility.getErrorCodes('228',null);
                        iolist = new list<DataValidator.InputObject>{};
                        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                        errMsg = errorMsg.errorMessage;
                        respData.error.add(errorMsg);
                        respData.returnCode = '228';
                        iRestRes = (IRestResponse)respData;
                        return iRestRes;
                    }
                    List<Opportunity> updateOppLst = new List<Opportunity>();
                    if(!quoteMap.isEmpty())
                    {
                        for(UnifiedBean.QuotesWrapper quoteIterate : quoteRecLst)
                        {
                            if(string.isNotBlank(quoteIterate.id))
                            {
                                if(quoteIterate.id.length()>15)
                                    quoteIterate.id = quoteIterate.id.subString(0,15);
                                
                                if(quoteMap.containsKey(quoteIterate.Id))
                                {
                                    Quote quoteRec = quoteMap.get(quoteIterate.Id);
                                    Opportunity oppRec = new Opportunity(id = quoteRec.Opportunityid);
                                    oppRec.SyncedQuoteid = quoteRec.id;
                                    
                                    system.debug('quoteRec======='+quoteRec);                                    
                                    //Because of the recursive updates on Opportunity and Quote ,For Opportunity to Quote sync settings the below values are populating with wrong values
                                    oppRec.Combined_Loan_Amount__c = quoteRec.Combined_Loan_Amount__c;                                  
                                    oppRec.ACH_Monthly_Payment__c = quoteRec.ACH_Monthly_Payment__c != null?quoteRec.ACH_Monthly_Payment__c:oppRec.ACH_Monthly_Payment__c;
                                    
                                    oppRec.Total_of_Payments_Loan_Amount__c = quoteRec.Total_of_Payments_Loan_Amount__c != null?quoteRec.Total_of_Payments_Loan_Amount__c:oppRec.Total_of_Payments_Loan_Amount__c;
                                    
                                    oppRec.ACH_Monthly_Payment_without_Prepay__c = quoteRec.ACH_Monthly_Payment_without_Prepay__c != null?quoteRec.ACH_Monthly_Payment_without_Prepay__c:oppRec.ACH_Monthly_Payment_without_Prepay__c;
                                    
                                    oppRec.Final_Monthly_Payment__c = quoteRec.Final_Monthly_Payment__c != null?quoteRec.Final_Monthly_Payment__c:oppRec.Final_Monthly_Payment__c;
                                    
                                    oppRec.ACH_Final_Monthly_Payment__c = quoteRec.ACH_Final_Monthly_Payment__c != null?quoteRec.ACH_Final_Monthly_Payment__c:oppRec.ACH_Final_Monthly_Payment__c;
                                    
                                    oppRec.ACH_FinalMonthlyPaymentwithoutPrepay__c = quoteRec.ACH_FinalMonthlyPaymentWOPrepay__c != null?quoteRec.ACH_FinalMonthlyPaymentWOPrepay__c:oppRec.ACH_FinalMonthlyPaymentwithoutPrepay__c;
                                    
                                    oppRec.NonACH_Monthly_Payment__c = quoteRec.NonACH_Monthly_Payment__c != null?quoteRec.NonACH_Monthly_Payment__c:oppRec.NonACH_Monthly_Payment__c;
                                    
                                    oppRec.NonACH_Monthly_Payment_without_Prepay__c = quoteRec.NonACH_Monthly_Payment_WO_Prepay__c != null?quoteRec.NonACH_Monthly_Payment_WO_Prepay__c:oppRec.NonACH_Monthly_Payment_without_Prepay__c;
                                    
                                    oppRec.NonACH_FinalMonthlyPaymentwithoutPrepay__c = quoteRec.NonACH_FinalMonthlyPaymentWOPrepay__c != null?quoteRec.NonACH_FinalMonthlyPaymentWOPrepay__c:oppRec.NonACH_FinalMonthlyPaymentwithoutPrepay__c;
                                    
                                    oppRec.NonACH_Final_Monthly_Payment__c = quoteRec.NonACH_Final_Monthly_Payment__c != null?quoteRec.NonACH_Final_Monthly_Payment__c :oppRec.NonACH_Final_Monthly_Payment__c ;
                                    //Updates by Adithya as part of 2706
                                    //Start
                                    if(quoteRec.SLF_Product__r.Product_Loan_Type__c == 'Solar Interest Only')
                                    {
                                        
                                        oppRec.ACH_Monthly_Payment_Interest_Only__c = quoteRec.ACH_Monthly_Payment_Interest_Only__c != null?quoteRec.ACH_Monthly_Payment_Interest_Only__c :oppRec.ACH_Monthly_Payment_Interest_Only__c ;
                                        
                                        oppRec.NonACH_Monthly_Payment_IO__c = quoteRec.NonACH_Monthly_Payment_IO__c != null?quoteRec.NonACH_Monthly_Payment_IO__c :oppRec.NonACH_Monthly_Payment_IO__c ;
                                        
                                        oppRec.ACH_Escalated_Monthly_Payment_IO__c = quoteRec.ACH_Escalated_Monthly_Payment_IO__c != null?quoteRec.ACH_Escalated_Monthly_Payment_IO__c :oppRec.ACH_Escalated_Monthly_Payment_IO__c ;
                                        
                                        oppRec.NonACH_Escalated_Monthly_Payment_IO__c = quoteRec.NonACH_Escalated_Monthly_Payment_IO__c != null?quoteRec.NonACH_Escalated_Monthly_Payment_IO__c :oppRec.NonACH_Escalated_Monthly_Payment_IO__c ;
                                        //End
                                    }else{
                                        oppRec.ACH_Monthly_Payment_Interest_Only__c = null;
                                        oppRec.NonACH_Monthly_Payment_IO__c = null;
                                        oppRec.ACH_Escalated_Monthly_Payment_IO__c = null;
                                        oppRec.NonACH_Escalated_Monthly_Payment_IO__c = null;
                                    }
                                    //End
                                    
                                    oppRec.isACH__c = quoteRec.isACH__c; //Added by Ravi as part of 3622
                                    //Condition added as part of 9015
                                    if(quoteRec.Opportunity.StageName !='Closed Won' && quoteRec.Opportunity.Change_Order_Status__c != 'Active'){
                                        oppRec.Signed_Document_Quote_Reference__c = quoteRec.id;
                                    }
                                    oppRec.Standard_Monthly_Payment_Max_Amount__c = quoteRec.Standard_Monthly_Payment_Max_Amount__c;
                                    oppRec.Promo_Monthly_Payment_Max_Amount__c = quoteRec.Promo_Monthly_Payment_Max__c;
                                    oppRec.Total_of_Payments_Max_Amount__c = quoteRec.Total_of_Payments_Max_Amount__c;
                                    oppRec.Total_of_Payments_Loan_Amount__c = quoteRec.Total_of_Payments_Loan_Amount__c;
                                    //Added by Rajesh for Orange-292 Starts
                                    oppRec.SLF_Product__c = quoteRec.SLF_Product__c;
                                    if(quoteRec.Non_Solar_Amount__c != null)
                                        oppRec.Non_Solar_Amount__c = quoteRec.Non_Solar_Amount__c;
                                    if(quoteRec.SLF_Product__r.Product_Loan_Type__c != 'SolarPlus Roof' && quoteRec.SLF_Product__r.Product_Loan_Type__c != 'Integrated Solar Shingles')// Added 'Integrated Solar Shingles' condition by Deepika on 29/10/2018 Orange-1040
                                        oppRec.Non_Solar_Amount__c = null;
                                    //Orange-292 Ends
                                    sdSet.add(quoteRec.System_Design__c);
                                    
                                    //Added by Suresh as per Orange-1606 (Re calculate MaxDrawAvilable and CarryOverPercentage values as per new loan amount)
                                    List<Double> lstValues = null; //Populates data when Project_Category__c='Home'
                                    boolean hasDrawRequests = false;
                                    if(quoteRec.opportunity.Project_Category__c=='Home'){
                                        System.debug('### Tracking1 - oppRecid'+oppRec.id);
                                        oppRec.Max_Draw_Available__c = quoteRec.Combined_Loan_Amount__c;//Udaya Kiran 9015
                                       /* List<Underwriting_file__c> lstUnderWriting = [select id,(select id,Level__c,Amount__c from Draw_Requests__r order by Level__c desc)  from underwriting_file__c where Opportunity__r.id=:oppRec.id];
                                        if(lstUnderWriting!=null && !lstUnderWriting.isEmpty()){
                                            List<Draw_Requests__c> lstDrawRequests = lstUnderWriting.get(0).Draw_Requests__r;
                                            System.debug('### Tracking - Draw Requests: '+lstDrawRequests);
                                            if(lstDrawRequests!=null && !lstDrawRequests.isEmpty()){
                                                hasDrawRequests = true;
                                                lstValues = recalculateDrawAmounts(String.valueOf(quoteRec.opportunity.Installer_Account__c),double.valueOf(quoteRec.Combined_Loan_Amount__c),lstUnderWriting);
                                                
                                                if(lstValues!=null && !lstValues.isEmpty()){
                                                    oppRec.Max_Draw_Available__c = lstValues.get(0);
                                                    oppRec.Draw_Carry_Over__c = lstValues.get(1);
                                                    //oppRec.Combined_Loan_Amount__c = quote.Combined_Loan_Amount__c;
                                                }
                                            }
                                        }*/
                                    }
                                    //End
                                    if(quoteRec.opportunity.Project_Category__c=='Home' && quoteRec.opportunity.StageName=='Closed Won' && !hasDrawRequests){
                                        System.debug('### Tracking - Sync quote: '+hasDrawRequests);
                                        //if((quoteRec.opportunity.StageName=='Closed Won' && !hasDrawRequests)){
                                            updateOppLst.add(oppRec);
                                            System.debug('### Tracking - Sync quote: '+updateOppLst);
                                        //}
                                    }
                                    //End
                                    else{
                                        updateOppLst.add(oppRec);
                                    }
                                    //updateOppLst.add(oppRec);                                    
                                }else{
                                    errMsg = ErrorLogUtility.getErrorCodes('228',null);
                                    iolist = new list<DataValidator.InputObject>{};
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    errMsg = errorMsg.errorMessage;
                                    respData.error.add(errorMsg);
                                    respData.returnCode = '228';
                                    iRestRes = (IRestResponse)respData;
                                    return iRestRes;
                                }
                                
                            }else{
                                errMsg = ErrorLogUtility.getErrorCodes('228',null);
                                iolist = new list<DataValidator.InputObject>{};
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                errMsg = errorMsg.errorMessage;
                                respData.error.add(errorMsg);
                                respData.returnCode = '228';
                                iRestRes = (IRestResponse)respData;
                                return iRestRes;
                            }
                        }
                        
                        if(string.isBlank(errMsg))
                        {
                            List<Opportunity> oppLst = new List<Opportunity>();
                            if(!oppIdSet.isEmpty())
                                                            
                                oppLst =[Select id,Name,AccountId,SLF_Product__c,StageName,Installer_Account__c,Project_Category__c,(select Id,Opportunity__c,Project_Status__c,Project_Status_Detail__c,Withdraw_Reason__c,Declined_Withdrawn_Date__c from Underwriting_Files__r) from Opportunity where id IN :oppIdSet];
                            //List<user> loggedInUsr = [select Id,contactid,contact.AccountId,contact.Account.FNI_Domain_Code__c,UserRole.DeveloperName,UserRole.Name from User where id =: userinfo.getUserId()];
                             Map<String,String> OwnercheckMap = SLFUtility.checkOpportunityOwner(oppLst[0],loggedInUsr[0]);
                            if(OwnercheckMap != null){
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = OwnercheckMap.values().get(0);
                                List<String> errList = new List<String>();
                                errList.addAll(OwnercheckMap.keyset());
                                respData.returnCode = errList[0];
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                respData.error.add(errorMsg);
                                iRestRes = (IRestResponse)respData;
                                return iRestRes;
                                
                            }
                            else if(oppLst[0].Underwriting_Files__r != null && oppLst[0].Underwriting_Files__r[0].Project_Status__c != null && oppLst[0].Underwriting_Files__r[0].Project_Status__c == 'Project Withdrawn'){
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = ErrorLogUtility.getErrorCodes('366',null);
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                respData.error.add(errorMsg);
                                respData.returnCode = '366';
                                iRestRes = (IRestResponse)respData;
                                return iRestRes;
                            }
                            //Modified below else if by Suresh and added new else if condition to support Orange-1606
                            //else if(!updateOppLst.isEmpty()&& oppLst[0].StageName != 'Closed Won')
                            if(updateOppLst.isEmpty()&& (oppLst[0].Project_Category__c=='Home')){
                                //No code here
                            }
                            else if(!updateOppLst.isEmpty()&& ((oppLst[0].Project_Category__c=='Solar' && oppLst[0].StageName != 'Closed Won') || (oppLst[0].Project_Category__c=='Home')))
                            {
                                try{
                                    system.debug('updateOppLst==========='+updateOppLst);
                                    upsert updateOppLst;
                                    
                                    //Updated by Adithya to fix ORANGE-8943
                                    //update quoteMap.Values();
                                    
                                    //Added by Rajesh for Orange -292 Starts
                                    Boolean syncCheckFlag = false;
                                    List<SLF_Credit__c> CreditList = new List<SLF_Credit__c>();
                                    Set<id> desyncOppIds = new Set<id>();                               
                                    List<Credit_Opinion__c> crdOpinoinLst = [select Id,Name,Credit__c,Quote__c,Status__c,Credit__r.Status__c,Credit__r.IsSyncing__c,Credit__r.LastModifiedDate,Credit__r.Opportunity__c from Credit_Opinion__c where Quote__c IN: quoteMap.keySet() order by Credit__r.LastModifiedDate desc];   
                                for(Credit_Opinion__c creditOp :crdOpinoinLst){
                                    SLF_Credit__c CreditRec = new SLF_Credit__c();  
                                    if((creditOp.status__c == 'Auto Approved' || creditOp.status__c == 'Manual Approved') && (creditOp.Credit__r.Status__c =='Auto Approved' || creditOp.Credit__r.Status__c =='Manual Approved') && !syncCheckFlag){
                                        CreditRec.id = creditOp.Credit__c;
                                        CreditRec.IsSyncing__c = true;
                                        syncCheckFlag=true;
                                        CreditList.add(CreditRec);
                                    }
                                    if(creditOp.status__c == 'N/A'){
                                        if(creditOp.Credit__r.IsSyncing__c){
                                            CreditRec.id = creditOp.Credit__c;
                                            CreditRec.IsSyncing__c = false;
                                            desyncOppIds.add(creditOp.Credit__r.Opportunity__c);
                                            CreditList.add(CreditRec);
                                        }
                                    }
                                }   
                                 if(!desyncOppIds.isEmpty() && desyncOppIds!= null){
                                    SyncDesyncUtilities.DesyncCreditFields(desyncOppIds);
                                }
                                if(!CreditList.isEmpty()){
                                    update CreditList;
                                }
                                                               
                                    //Orange -292 Ends
                                    DescribeSObjectResult describeResult = System_Design__c.getSObjectType().getDescribe();
                                    List<String> fieldNames = new List<String>(describeResult.fields.getMap().keySet());
                                    String query =  ' SELECT ' + String.join( fieldNames, ',' ) +  ' FROM ' +     describeResult.getName()+ ' Where id IN :sdSet';      
                                    system.debug('### query string '+query);
                                    sdList = Database.query(query);
                                    system.debug('### sdList '+sdList);
                                    
                                    SystemDesignTriggerHandler sd = new SystemDesignTriggerHandler();
                                    sd.syncSystemdesignToOpportunity(sdList);
                                    
                                }catch(Exception err){
                                    Database.rollback(sp);
                                    system.debug('### into Account catch '+err.getMessage());
                                     iolist = new list<DataValidator.InputObject>{};
                                    errMsg = ErrorLogUtility.getErrorCodes('400',null);
                                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                    //respData.error  = errorMsg;
                                    respData.error.add(errorMsg);
                                    respData.returnCode = '400';
                                    iRestRes = (IRestResponse)respData;
                                    return iRestRes;
                                }   
                                respData = SLFUtility.getUnifiedBean(oppIdSet,'Orange',false);
                                respData.returnCode = '200';
                                iRestRes = (IRestResponse)respData;
                            }else{
                                iolist = new list<DataValidator.InputObject>{};
                                errMsg = ErrorLogUtility.getErrorCodes('273',null);
                                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                                respData.error.add(errorMsg);
                                respData.returnCode = '273';
                                iRestRes = (IRestResponse)respData;
                                return iRestRes;
                            }
                            
                            respData = SLFUtility.getUnifiedBean(oppidSet,'Orange',false);
                            respData.returnCode = '200';
                            iRestRes = (IRestResponse)respData;
                        }   
                    }
                    
                }else{
                    errMsg = 'Please provide quote section to proceed further';
                }   
            }
            
            if(string.isNotBlank(errMsg))
            {
                iolist = new list<DataValidator.InputObject>{};
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                errMsg = errorMsg.errorMessage;
                respData.error.add(errorMsg);
                respData.returnCode = '400';
            }
            iRestRes = (IRestResponse)respData;
        }catch(Exception err){
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            respData.error.add(errorMsg);
            respData.returnCode = '400';
            iRestRes = (IRestResponse)respData;
            
            system.debug('### SyncQuoteDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('SyncQuoteDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+SyncQuoteDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
    /*
    * Related to Home Improvement logic
    * Re calculates Max Avialable Amount and Carryover percentage values as per the new loan amount
    * *//*
    public List<Double> recalculateDrawAmounts(String sInstallerId,Double totalLoanAmount,List<Underwriting_File__c> lstUnderwriting){
        system.debug('### Entered into recalculateDrawAmounts() of '+SyncQuoteDataServiceImpl.class);
        Double maxWithDrawAmnt = 0;
        Double carryOverPerc = 0;
        List<Double> lstValues = new List<Double>();
        try{
            Map<String,Decimal> allocations = new Map<String,Decimal>();
            List<Draw_Allocation__c> lstDrawAllocation = [select id,Draw_Allocation__c,Level__c,Account__c from Draw_Allocation__c where Account__c=:sInstallerId];
            if(lstDrawAllocation!=null && !lstDrawAllocation.isEmpty()){
                for(Draw_Allocation__c objDrawAllocations : lstDrawAllocation){
                    allocations.put(objDrawAllocations.Level__c,objDrawAllocations.Draw_Allocation__c);
                }
            }
            System.debug('Tracking - allocations: '+allocations);
            
            //List<Underwriting_File__c> lstUnderwriting = [select id,(select id,Level__c,Amount__c from Draw_Requests__r order by Level__c desc)  from underwriting_file__c where Opportunity__r.id=:sOpprId];
            if(lstUnderwriting!=null && !lstUnderwriting.isEmpty()){
                Underwriting_File__c objUnderwriting = lstUnderwriting.get(0);
                List<Draw_Requests__c> lstDrawRequests = objUnderwriting.Draw_Requests__r;
                System.debug('Tracking - lstDrawRequests: '+lstDrawRequests);
                if(lstDrawRequests!=null && !lstDrawRequests.isEmpty()){
                    Draw_Requests__c objDrawReq = lstDrawRequests.get(0);
                    
                    //double remainingBalAmt = (totalLoanAmount - objDrawReq.Amount__c);
                    //System.debug('Tracking - remainingBalAmt: '+remainingBalAmt);
                    double nextAllocationPercent = allocations.get(String.valueOf(((Integer.valueOf(objDrawReq.Level__c)+1))));
                    System.debug('Tracking - nextAllocationPercent: '+nextAllocationPercent);
                    double nextWithdrawnAmount = ((nextAllocationPercent/100) * (totalLoanAmount));
                    System.debug('Tracking - nextWithdrawnAmount: '+nextWithdrawnAmount);
                    
                    //maxWithDrawAmnt = (remainingBalAmt + nextWithdrawnAmount);
                    //System.debug('Tracking - maxWithDrawAmnt: '+maxWithDrawAmnt);
                    
                    //CarryOver percent calculation
                    double previousAllocationPercent = allocations.get(objDrawReq.Level__c);
                    System.debug('Tracking - previousAllocationPercent: '+previousAllocationPercent);
                    
                    double RevisedAllocatedAmount  = (previousAllocationPercent/100)*(totalLoanAmount);
                    System.debug('Tracking - RevisedAllocatedAmount: '+RevisedAllocatedAmount);
                    
                    double eligibleAllocatedAmount = totalLoanAmount - RevisedAllocatedAmount;
                    System.debug('Tracking - eligibleAllocatedAmount: '+eligibleAllocatedAmount);
                    
                    double balEligibleAmount = (eligibleAllocatedAmount-objDrawReq.Amount__c);
                    System.debug('Tracking - balEligibleAmount: '+balEligibleAmount);
                    
                    maxWithDrawAmnt = balEligibleAmount+nextWithdrawnAmount;
                    
                    carryOverPerc = ((balEligibleAmount/totalLoanAmount) * 100);
                    //carryOverPerc = (totalAllowedDrawPercent - (requestedDrawAmount/totalLoanAmount * 100));
                }
            }
            System.debug('Tracking- maxWithDrawAmnt: '+maxWithDrawAmnt);
            System.debug('Tracking- carryOverPerc: '+carryOverPerc);
            lstValues.add(maxWithDrawAmnt);
            lstValues.add(carryOverPerc);
        }catch(Exception ex){
            System.debug('Error: '+ex.getMessage());
        }
        system.debug('### Exited from recalculateDrawAmounts() of '+SyncQuoteDataServiceImpl.class);
        return lstValues;
    }*/
}