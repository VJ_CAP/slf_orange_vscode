/**
* Description: Test class for CreditTrg trigger and CreditTriggerHandler class. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         09/26/2017          Created
******************************************************************************************/
@isTest
public class BoxFieldsTriggerHandlerTest {
    
    /**
    * Description: Test method to cover StipulationTriggerHandler funtionality.
    */
    public testMethod static void BoxFieldsTriggerTest()
    {
        //get Custom setting data.
        SLFUtilityTest.createCustomSettings();   
        TriggerFlags__c trgBoxflag = new TriggerFlags__c();
        trgBoxflag.Name ='Box_Fields__c';
        trgBoxflag.isActive__c =true;
        insert trgBoxflag;
        
        //get the person account record type
        Schema.DescribeSObjectResult accResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> accRecTypeInfoMap = accResult.getRecordTypeInfosByName();
        
        
        Account acc = new Account();
        acc.LastName = 'Test Account';
        acc.Push_Endpoint__c ='www.test.com';
        acc.PersonEmail = 'slfaccount@slf.com';
        acc.RecordTypeId = accRecTypeInfoMap.get('Person Account').getRecordTypeId();
        acc.Installer_Email__c = 'slfaccount@slf.com';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Push_Endpoint__c = 'https://www.google.com/';
        insert acc;
        
        Product__c prodct = new Product__c();
        //prodct.ACH__c = false;
        prodct.APR__c = 4.99;
        prodct.bump__c = false;
        prodct.Installer_Account__c = acc.Id;
        prodct.Internal_Use_Only__c = false;
        prodct.Is_Active__c = true;
        prodct.Long_Term_Facility__c = 'TCU';
        prodct.LT_Max_Loan_Amount__c =  100000.0;
        prodct.Max_Loan_Amount__c = 100000.0;
        prodct.State_Code__c = 'CA';
        prodct.FNI_Min_Response_Code__c = 10;
        prodct.Product_Display_Name__c = 'Test';
        prodct.Qualification_Message__c = '123';
        prodct.Product_Tier__c = '0';
        insert prodct;
        
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = acc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Postal_Code__c = '94901';
        opp.Install_Street__c = '39 GLEN AVE';
        opp.Install_City__c = 'SAN RAFAEL';
        opp.SLF_Product__c = prodct.Id;
        opp.Combined_Loan_Amount__c = 10000;
        opp.Long_Term_Loan_Amount_Manual__c = 20000;
        opp.Co_Applicant__c = acc.Id;
        insert opp;
        
        Underwriting_File__c uwf = new Underwriting_File__c(
            Send_Confirmation_Email__c = true,
            Opportunity__c = opp.Id,
            Project_Status__c = 'Pending Loan Docs',
            M1_Approval_Requested_Date__c = Date.Today()
        );
        insert uwf;
         
        List<Stipulation_Data__c> stpDataList = new List<Stipulation_Data__c>();
        Stipulation_Data__c sData  = new Stipulation_Data__c();
        sData.Name = 'LAA';
        sData.Description__c = 'LAA';
        sData.Installer_Only_Email__c = true;
        sData.Review_Classification__c='SLS I';
        stpDataList.add(sData);
        
        Stipulation_Data__c sData1 = new Stipulation_Data__c();
        sData1.Name = 'HIA';
        sData1.Description__c = 'HIA';
        sData1.Installer_Only_Email__c = true;
        sData1.Review_Classification__c='SLS II';
        stpDataList.add(sData1);
        insert stpDataList; 
        
        List<Stipulation__c> stpList = new List<Stipulation__c>();
        Stipulation__c stipTest1  = new Stipulation__c();
        stipTest1.Status__c = 'Documents Received';
        stipTest1.Review_Start_Date_Time__c = null;
        stipTest1.Stipulation_Data__c  = sData.id;
        stipTest1.Underwriting__c =uwf.id;
        stipTest1.ETC_Notes__c = 'test';
        stipTest1.Status__c = 'New';
        stpList.add(stipTest1);
        
        Stipulation__c stipTest2  = new Stipulation__c();
        stipTest2.Status__c = 'Documents Received';
        stipTest2.Review_Start_Date_Time__c = null;
        stipTest2.Stipulation_Data__c  = sData1.id;
        stipTest2.Underwriting__c =uwf.id;
        stipTest2.ETC_Notes__c = 'test';
        stipTest2.Status__c = 'New';
        stpList.add(stipTest2);
        insert stpList;
        
        Box_Fields__c boxObj  = new Box_Fields__c(Underwriting__c = uwf.ID);
        insert boxObj;
        
        boxObj.Latest_LT_Loan_Agreement_Date_Time__c = system.now();
        boxObj.Latest_Install_Contract_Date_Time__c = system.now();
        update boxObj;
        
        BoxFieldsTriggerHandler.runOnce();
        BoxFieldsTriggerHandler.runOnce();
    }
}