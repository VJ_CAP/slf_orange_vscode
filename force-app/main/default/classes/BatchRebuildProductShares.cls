global with sharing class BatchRebuildProductShares implements Database.Batchable<SObject>,Database.Stateful,Schedulable                                       
{
    class Parameters
    {
        String objectType = 'Campaign';
        Boolean updateDatabase = true;
        String oneId = null;
    }
    
    Boolean testMode = Test.isRunningTest();
    Parameters params = new Parameters();
    Integer sharesInserted = 0;
    
    //****************************************************************** 
    
    global BatchRebuildProductShares ()
    {   
    }
    
    //****************************************************************** 
    
    global void execute (SchedulableContext sc)
    {
        Database.executeBatch (new BatchRebuildProductShares(), 50);
    }
    
    //******************************************************************
    
    global Database.QueryLocator start (Database.BatchableContext bc)
    { 
        String query = 'SELECT Id, Installer_Account__c FROM Product__c where Is_Active__c=true' +                 
            (this.testmode ? ' LIMIT 10' : '');
        
        Database.Querylocator ql = Database.getQueryLocator (query); 
        
        return ql;
    }
    
    //******************************************************************
    
    global void execute (Database.BatchableContext bc, list<SObject> objects) 
    { 
        map<Id,SObject> objectsMap = new map<Id,SObject>{};
            
            for (SObject o : objects)
        {
            objectsMap.put (o.Id, o);
        }
        
        ProductSharingSupport.afterInsert (objectsMap);
    }
    
    //******************************************************************
    
    global void finish (Database.BatchableContext bc)
    {
    }   
    
    //******************************************************************
    
}