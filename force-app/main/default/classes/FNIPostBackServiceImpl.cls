/**
* Description: FNI PostBack integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           07/10/2017          Created
******************************************************************************************/
public with sharing class FNIPostBackServiceImpl extends RESTServiceBase{  
    private String msg = '';
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + FNIPostBackServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        FNIPostBackDataServiceImpl FNIPostBackDataSrvImpl = new FNIPostBackDataServiceImpl(); 
        IRestResponse iRestRes;
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData'+reqData);
            res.response = FNIPostBackDataSrvImpl.transformOutput(reqData);
        }catch(Exception err){
            system.debug('### FNIPostBackServiceImpl .fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
           
            ErrorLogUtility.writeLog(' FNIPostBackServiceImpl .fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));  
            UnifiedBean unifBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            unifBean.error  = errorWrapperLst ;
            unifBean.returnCode = '214';
            iRestRes = (IRestResponse)unifBean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + FNIPostBackServiceImpl.class);
        return res.response ;
    }
    
    
}