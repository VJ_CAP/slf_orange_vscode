/**
* Description: Fetch User information logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Brahmeswar            12/12/2018           Created
*****************************************************************************************/
public without sharing class GetProjectTypeDataServiceImpl extends RestDataServiceBase{ 
    
    
    
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+GetProjectTypeDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj; 
        system.debug('### reqData '+reqData);
        
        UnifiedBean unifiedBean = new UnifiedBean(); 
        unifiedBean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            
            unifiedBean.projectTypes = new List<UnifiedBean.ProjectTypeWrapper>();
        
        try
        {   
            
            
            if(reqData.projectTypes != null && reqData.projectTypes[0].isActive!=null )
            {     
                string projectCategory ='';     
                if(reqData.projectTypes[0].projectCategory!=null && String.isNotBlank(reqData.projectTypes[0].projectCategory)){
                    projectCategory=reqData.projectTypes[0].projectCategory;
                }else{
                    projectCategory='Solar';
                }
                
                List<Project_Type__c> projectTypeList = new List<Project_Type__c>();
                projectTypeList = [SELECT id,Name,is_Active__c,Sorting_the_Records__c,Type__c FROM Project_Type__c WHERE is_Active__c=:reqData.projectTypes[0].isActive AND Type__c=:projectCategory order By Sorting_the_Records__c ASC];
                if(!projectTypeList.isEmpty()){
                    for(Project_Type__c objProjectType:projectTypeList){
                        UnifiedBean.ProjectTypeWrapper objProjTypeWrp = new UnifiedBean.ProjectTypeWrapper();
                        objProjTypeWrp.isActive=objProjectType.is_Active__c;
                        objProjTypeWrp.name=objProjectType.Name;
                        objProjTypeWrp.id=objProjectType.id;
                        objProjTypeWrp.projectCategory =objProjectType.Type__c;
                        unifiedBean.projectTypes.add(objProjTypeWrp);
                    }
                }else{
                    // no records found
                    system.debug('#### in else no records found');
                    iolist = new list<DataValidator.InputObject>{};
                        errMsg = ErrorLogUtility.getErrorCodes('299', null);                    
                    UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                    unifiedBean.error.add(errorMsg);
                    unifiedBean.returnCode = '299';
                    iRestRes = (IRestResponse)unifiedBean;
                    return iRestRes;
                }           
            }else{
                system.debug('#### in else ');
                iolist = new list<DataValidator.InputObject>{};
                    errMsg = ErrorLogUtility.getErrorCodes('214', null);                    
                UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
                unifiedBean.error.add(errorMsg);
                unifiedBean.returnCode = '214';
                iRestRes = (IRestResponse)unifiedBean;
                return iRestRes;
            }
            
            
            //iRestRes = (IRestResponse)unifiedBean;
        }catch(Exception err){
            system.debug('Exception -->: '+err.getMessage());
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400', null);
            unifiedBean = new UnifiedBean(); 
            unifiedBean.error = new list<UnifiedBean.errorWrapper>();
            gerErrorMsg(unifiedBean,errMsg,'400',iolist);
            iRestRes = (IRestResponse)unifiedBean;
            system.debug('### GetProjectTypeDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString()); 
            ErrorLogUtility.writeLog('GetProjectTypeDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            //return iRestRes;
        }    
        system.debug('### Exit from transformOutput() of '+GetProjectTypeDataServiceImpl.class);
        system.debug('### iRestRes '+iRestRes);
        iRestRes = (IRestResponse)unifiedBean;
        return iRestRes;
    }  
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
        
    }
    
    
}