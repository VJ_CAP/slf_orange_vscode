public Class SLFTestUtility{
    public static Account createAccount(String RecordType, Boolean insertBool,Boolean personAcc){
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();  
        Account acc = new Account();
        if(personAcc){
            acc.FirstName = 'Somnath';
            acc.LastName = 'Dey';
        } else {
        acc.name = 'Test Account';
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        }
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        if(recTypeInfoMap.containsKey(RecordType) && recTypeInfoMap.get(RecordType) != null)
            acc.RecordTypeId = recTypeInfoMap.get(RecordType).getRecordTypeId();
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        if(insertBool)
            insert acc;
        return acc;
    }
    public static Contact createContact(Account accObj, Boolean insertBool){
        Contact con = new Contact(LastName ='testCon',AccountId = accObj.Id);
        if(insertBool)
            insert con;
        return con;
    }
    public static User createPortalUser(Profile p,Contact con, Boolean insertBool){
          map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
       User portalUser = new User();
        portalUser.alias = 'testc';
        portalUser.Email='testc@mail.com';
        portalUser.IsActive = true;
        portalUser.Hash_Id__c='123';
        portalUser.EmailEncodingKey='UTF-8';
        portalUser.LastName='test';
        portalUser.LanguageLocaleKey='en_US'; 
        portalUser.LocaleSidKey='en_US'; 
        portalUser.ProfileId = p.Id;
        portalUser.UserRoleId = roleMap.get('Top Role');
        //UserRoleId = r.id,
        portalUser.timezonesidkey='America/Los_Angeles'; 
        portalUser.username='testclass@mail.com';
        portalUser.ContactId = con.Id;
        if(insertBool)
            insert portalUser;
        return portalUser;
    }
    public static List<Draw_Allocation__c> createDrawAllocation(Account accObj, Boolean insertBool){
       List<Draw_Allocation__c> lstDrawAllocInst = new List<Draw_Allocation__c>();
        Draw_Allocation__c drawAlloc = new Draw_Allocation__c();
        drawAlloc.Account__c = accObj.id;
        drawAlloc.Level__c = '1';
        drawAlloc.Draw_Allocation__c = 50;
        lstDrawAllocInst.add(drawAlloc);
        
        Draw_Allocation__c drawAlloc1 = new Draw_Allocation__c();
        drawAlloc1.Account__c = accObj.id;
        drawAlloc1.Level__c = '2';
        drawAlloc1.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc1);
        
        Draw_Allocation__c drawAlloc2 = new Draw_Allocation__c();
        drawAlloc2.Account__c = accObj.id;
        drawAlloc2.Level__c = '3';
        drawAlloc2.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc2);
        
        if(insertBool)
            insert lstDrawAllocInst;
        return lstDrawAllocInst;
    }
    public static Document createDocument(String folderName,Boolean insertBool){
         Document doc1 = new Document();
            doc1.Body = Blob.valueOf('Some Text');
            doc1.ContentType = 'application/pdf';
            doc1.DeveloperName = 'my_document';
            doc1.IsPublic = true;
            doc1.Name = 'Sunlight Logo';
            doc1.FolderId = [select id from folder where name =:folderName].id;
        if(insertBool)
            insert doc1;
        return doc1;
    }
    public static List<Product__c> createProduct(Account accObj,Boolean insertBool){
        List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =accObj.id;
            prod.Long_Term_Facility_Lookup__c =accObj.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            //insert prod;
            prodList.add(prod);
            
            //Inserting duplicate SLF Product Records
            Product__c prod1 = new Product__c();
            prod1.Installer_Account__c =accObj.id;
            prod1.Long_Term_Facility_Lookup__c =accObj.id;
            prod1.Name='testprod';
            prod1.FNI_Min_Response_Code__c=9;
            prod1.Product_Display_Name__c='test';
            prod1.Qualification_Message__c ='testmsg';
            prod1.Product_Loan_Type__c='Solar';
            prod1.State_Code__c ='CA';
            prod1.ST_APR__c = 2.99;
            prod1.Long_Term_Facility__c = 'TCU';
            prod1.APR__c = 2.99;
            prod1.ACH__c = true;
            prod1.Internal_Use_Only__c = true;
            prod1.LT_Max_Loan_Amount__c =1000;
            prod1.Term_mo__c = 144;
            prod1.Product_Tier__c = '0';
            //insert prod1;
            prodList.add(prod1);
            
            //Inserting duplicate SLF Product Records
            Product__c prod2 = new Product__c();
            prod2.Installer_Account__c =accObj.id;
            prod2.Long_Term_Facility_Lookup__c =accObj.id;
            prod2.Name='testprod';
            prod2.FNI_Min_Response_Code__c=9;
            prod2.Product_Display_Name__c='test';
            prod2.Product_Loan_Type__c='Battery Only';
            prod2.Qualification_Message__c ='testmsg';
            prod2.State_Code__c ='CA';
            prod2.ST_APR__c = 2.99;
            prod2.Long_Term_Facility__c = 'TCU';
            prod2.APR__c = 2.99;
            prod2.ACH__c = true;
            prod2.Internal_Use_Only__c = true;
            prod2.LT_Max_Loan_Amount__c =1000;
            prod2.Term_mo__c = 144;
            prod2.Product_Tier__c = '0';
            //insert prod2;
            prodList.add(prod2);
            if(insertBool)
                 insert prodList;
            return prodList;
    }
    public static List<Product_Proxy__c> createProductProxy(Account accObj,Product__c prod,Boolean insertBool){
        List<Product_Proxy__c> prdctPrxyLst = new List<Product_Proxy__c>();
            Product_Proxy__c prodProxy = new Product_Proxy__c();
            prodProxy.ACH__c = true;
            prodProxy.APR__c = 2.99;
            prodProxy.Installer__c =accObj.id;
            prodProxy.Internal_Use_Only__c = false;
            prodProxy.Is_Active__c = true;
            prodProxy.SLF_ProductID__c = prod.Id;
            prodProxy.Name = 'testprod';
            prodProxy.State_Code__c ='CA';
            prodProxy.Term_mo__c = 120;
            //insert prodProxy;
            prdctPrxyLst.add(prodProxy);
            
            Product_Proxy__c prodProxy1 = new Product_Proxy__c();
            prodProxy1.ACH__c = true;
            prodProxy1.APR__c = 2.99;
            prodProxy1.Installer__c =accObj.id;
            prodProxy1.Internal_Use_Only__c = false;
            prodProxy1.Is_Active__c = true;
            prodProxy1.SLF_ProductID__c = prod.Id;
            prodProxy1.Name = 'testprod';
            prodProxy1.State_Code__c ='CA';
            prodProxy1.Term_mo__c = 144;
            //insert prodProxy1;
            prdctPrxyLst.add(prodProxy1);
            
            Product_Proxy__c prodProxy2 = new Product_Proxy__c();
            prodProxy2.ACH__c = true;
            prodProxy2.APR__c = 2.99;
            prodProxy2.Installer__c =accObj.id;
            prodProxy2.Internal_Use_Only__c = false;
            prodProxy2.Is_Active__c = true;
            prodProxy2.SLF_ProductID__c = prod.Id;
            prodProxy2.Name = 'testprod';
            prodProxy2.State_Code__c ='CA';
            prodProxy2.Term_mo__c = 144;
            //insert prodProxy2;
            prdctPrxyLst.add(prodProxy2);
            
            Product_Proxy__c prodProxy3 = new Product_Proxy__c();
            prodProxy3.ACH__c = true;
            prodProxy3.APR__c = 5.99;
            prodProxy3.Installer__c =accObj.id;
            prodProxy3.Internal_Use_Only__c = false;
            prodProxy3.Is_Active__c = true;
            prodProxy3.SLF_ProductID__c = prod.Id;
            prodProxy3.Name = 'testprod';
            prodProxy3.State_Code__c ='NY';
            prodProxy3.Term_mo__c = 120;
            //insert prodProxy3;
            prdctPrxyLst.add(prodProxy3);
            if(insertBool)
                insert prdctPrxyLst;
            return prdctPrxyLst;
    }
    public static List<Opportunity> createOpportunity(Account accObj,Account personAcc,Product__c prod,Boolean insertBool){
        List<Opportunity> oppList = new List<Opportunity>();
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'personOpp';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = accObj.id;
            personOpp.Install_State_Code__c = 'CA';
            personOpp.Install_Street__c ='Street';
            personOpp.Install_City__c ='InCity';
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;            
            //insert personOpp;
            oppList.add(personOpp);
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = accObj.id;
            opp.Installer_Account__c = accObj.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id; 
            opp.Block_Draws__c = false;
            opp.Has_Open_Stip__c = 'False';
            opp.All_HI_Required_Documents__c=true;
            opp.Max_Draw_Count__c=3;
            //insert opp;
            oppList.add(opp);
            if(insertBool)
                insert oppList;
            return oppList;
    }
    public static Underwriting_File__c createUnderwritingFile(Opportunity opp,Boolean insertBool){
            Underwriting_File__c undStpbt = new Underwriting_File__c();
            undStpbt.Opportunity__c = opp.id;
            undStpbt.Approved_for_Payments__c = true;
            if(insertBool)
                insert undStpbt;
            return undStpbt;
    }
    public static Stipulation_Data__c createStipulationData(Boolean insertBool){
            Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', Email_Text__c = 'Test data', POS_Message__c = 'test');
            if(insertBool)
                insert sd;
            return sd;
    }
     public static Stipulation__c createStipulation(Stipulation_Data__c sd,Underwriting_File__c undStpbt,Boolean insertBool){
         Stipulation__c stp = new Stipulation__c();
            stp.Status__c ='New';
            stp.Stipulation_Data__c = sd.id;
            stp.Underwriting__c =undStpbt.id;
         if(insertBool)
            insert stp;
         return stp;
     }
    public static List<Equipment_Manufacturer__c> createEquipmentManufacturer(Boolean insertBool){
        List<Equipment_Manufacturer__c> eqList = new List<Equipment_Manufacturer__c>();
            Equipment_Manufacturer__c equipRec1 = new Equipment_Manufacturer__c();
            equipRec1.End_Date__c = system.today()-1;
            equipRec1.Activation_Date__c =system.today()-6;
            equipRec1.Manufacturer_Type__c ='Mounting';
            equipRec1.Manufacturer__c ='Unirac';
            //insert equipRec1;
            eqList.add(equipRec1);
            
            Equipment_Manufacturer__c equipRec2 = new Equipment_Manufacturer__c();
            equipRec2.Activation_Date__c =system.today();
            equipRec2.End_Date__c =system.today()+6;
            equipRec2.Manufacturer_Type__c ='Mounting';
            equipRec2.Manufacturer__c ='Unirac';
            //insert equipRec2;
            eqList.add(equipRec2);
            if(insertBool)
                insert eqList;
            return eqList;
    }
    public static System_Design__c createSystemDesign(Opportunity personOpp,Boolean insertBool) {
        System_Design__c sysDesignRec = new System_Design__c();
            //sysDesignRec.Name = 'Test Sys';
            sysDesignRec.System_Cost__c = 11;
            sysDesignRec.Opportunity__c = personOpp.Id;
            sysDesignRec.Est_Annual_Production_kWh__c = 10;
            sysDesignRec.Inverter_Count__c = 11;
            //  sysDesignRec.Inverter_Make__c = '11';
            sysDesignRec.Inverter_Model__c = '12';
            //sysDesignRec.Inverter__c = 'TBD';
            sysDesignRec.Module_Count__c = 10;
            sysDesignRec.Module_Model__c = '11';
            //sysDesignRec.System_Size_STC_kW__c = 12;
            if(insertBool)
                insert sysDesignRec;
            return sysDesignRec;
    }
    public static List<Quote> createQuote(Opportunity opp,System_Design__c sysDesignRec,Product__c prod,Boolean insertBool){
        List<Quote> quoteList = new List<Quote>();
            Quote quoteRec = new Quote();
            quoteRec.Name ='Test quote';
            quoteRec.Opportunityid = opp.id;//opp.Id
            quoteRec.SLF_Product__c = prod.Id ;
            quoteRec.System_Design__c= sysDesignRec.id;
            //quoteRec.Accountid =acc.id;
            //insert quoteRec; 
            quoteList.add(quoteRec); 
            
            Quote quoteRec1 = new Quote();
            quoteRec1.Name ='Test quote';
            quoteRec1.Opportunityid = opp.Id;
            quoteRec1.SLF_Product__c = prod.Id ;
            quoteRec1.System_Design__c= sysDesignRec.id;
            //quoteRec.Accountid =acc.id;
            //insert quoteRec; 
            quoteList.add(quoteRec1);            
                       
            
            Quote quoteRec2 = new Quote();
            quoteRec2.Name ='Test quote';
            quoteRec2.Opportunityid = opp.id;
            quoteRec2.SLF_Product__c = prod.Id ;
            quoteRec2.System_Design__c= sysDesignRec.id;
            //insert quoteRec2; 
            quoteList.add(quoteRec2);
            if(insertBool)
                insert quoteList;
            return quoteList;
    }
    public static List<SLF_Credit__c> createSLFCredit(Account acc,Opportunity opp,Product__c prod,Boolean insertBool){
        List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.id;
            credit.Primary_Applicant__c = acc.id;
            credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = prod.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'New';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            //insert credit;
            creditList.add(credit);
            
            //Insert Credit Record
            SLF_Credit__c credit2 = new SLF_Credit__c();
            credit2.Opportunity__c = opp.id;
            credit2.Primary_Applicant__c = acc.id;
            credit2.Co_Applicant__c = acc.id;
            credit2.SLF_Product__c = prod.Id ;
            credit2.Total_Loan_Amount__c = 100;
            credit2.Status__c = 'New';
            credit2.LT_FNI_Reference_Number__c = '123';
            credit2.Application_ID__c = '123';
            //insert credit2;
            creditList.add(credit2);
            
            //Insert Credit Record
            SLF_Credit__c salescredit = new SLF_Credit__c();
            salescredit.Opportunity__c = opp.id;
            salescredit.Primary_Applicant__c = acc.id;
            salescredit.Co_Applicant__c = acc.id;
            salescredit.SLF_Product__c = prod.Id ;
            salescredit.Total_Loan_Amount__c = 100;
            salescredit.Status__c = 'New';
            salescredit.LT_FNI_Reference_Number__c = '123';
            salescredit.Application_ID__c = '123';
            //insert salescredit;
            creditList.add(salescredit);
            if(insertBool)
                insert creditList;
            return creditList;
    }
    public static Default_Product__c createDefaultProduct(Account acc,Product__c prod,Boolean insertBool){
        Default_Product__c defautProd = new Default_Product__c();
            defautProd.Account__c = acc.id;
            defautProd.SLF_Product__c = prod.id;
            if(insertBool)
                insert defautProd;
            return defautProd;
    }
    /*
    public static Product_Proxy__c createProducProxy(Account acc,Boolean insertBool){
        Product_Proxy__c proxprod = new Product_Proxy__c();
            proxprod.Installer__c = acc.id;
            proxprod.State_Code__c = 'CA';
            proxprod.Term_mo__c = 120;
            proxprod.Is_Active__c = true;               
            proxprod.ACH__c = true;
            proxprod.APR__c = 2.99 ;
            if(insertBool)
                insert proxprod;
            return proxprod;
    }
    */
    public static SFSettings__c createSFSettings(Boolean insertBool){
        SFSettings__c slfencrypt = new SFSettings__c();
            slfencrypt.Encrypt_Private_Key__c = 'SLFOrange';
            slfencrypt.Name = 'PrivateKey';
            if(insertBool)
                insert slfencrypt;
            return slfencrypt;

    }
    public static New_York_City_Zip_Codes__c createNewYorkCityZipCodes(Boolean insertBool){
        New_York_City_Zip_Codes__c newyorkzcode = new New_York_City_Zip_Codes__c();
            newyorkzcode.County__c = 'New York';
            newyorkzcode.Name = '10000';
            if(insertBool)
                insert newyorkzcode;
            return newyorkzcode;
    }
    public static List<ExceptionList__c> createExceptionList(Boolean insertBool){
         List<ExceptionList__c> expLst = new List<ExceptionList__c>();
            ExceptionList__c excobj = new ExceptionList__c();
            excobj.API_Name__c = 'Opportunity__c';
            excobj.Object_Name__c = 'Opportunity';
            excobj.Name = 'Opportunity__c';
            expLst.add(excobj);
            
            ExceptionList__c excobj1 = new ExceptionList__c();
            excobj1.API_Name__c = 'Partner_Foreign_Key__c';
            excobj1.Object_Name__c = 'Opportunity';
            excobj1.Name = 'Partner_Foreign_Key__c';
            expLst.add(excobj1);
            
            ExceptionList__c excobj2 = new ExceptionList__c();
            excobj2.API_Name__c = 'Install_Contract_System_Cost__c';
            excobj2.Object_Name__c = 'Underwriting_File__c';
            excobj2.Name = 'Install Contract System Cost';
            expLst.add(excobj2);
            
            ExceptionList__c excobj3 = new ExceptionList__c();
            excobj3.API_Name__c = 'LT_Amount_Financed__c';
            excobj3.Object_Name__c = 'Underwriting_File__c';
            excobj3.Name = 'LT Amount Financed';
            expLst.add(excobj3);
            if(insertBool)
                insert expLst;
            return expLst;
    }
    public static List<SLF_Boomi_API_Details__c> createSLFBoomiAPIDetails(Boolean insertBool){
        List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
            SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
            endpoint1.Name = 'Pricing';
            endpoint1.Endpoint_URL__c =  'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
            endpoint.add(endpoint1);
            SLF_Boomi_API_Details__c endpoint2 = new SLF_Boomi_API_Details__c();
            endpoint2.Name = 'BoomiToAWSPricing';
            endpoint2.Endpoint_URL__c =  'https://test.connect.boomi.com/ws/rest/v1/dev/pricing/callaws/';
            endpoint.add(endpoint2);
            
            SLF_Boomi_API_Details__c endpoint3 = new SLF_Boomi_API_Details__c();
            endpoint3.Name = 'FNI QA';
            endpoint3.Username__c =  'salesforceuser';
            endpoint3.Password__c = 'RC8N1CreKnz4rm2kMTCj';
            endpoint.add(endpoint3);
            
            SLF_Boomi_API_Details__c endpoint4 = new SLF_Boomi_API_Details__c();
            endpoint4.Name = 'FNI Prod';
            endpoint4.Username__c =  'salesforceuser';
            endpoint4.Password__c = 'AmtPHqSlK9sB6NXjSyq4';
            endpoint.add(endpoint4);
            
            
            SLF_Boomi_API_Details__c apidetails = new SLF_Boomi_API_Details__c();
            apidetails.Name ='Boomi QA';
            apidetails.Username__c ='sunlightfinancial-WSHA3Z.N29SCE';
            apidetails.Password__c ='2458a5de-d689-4708-a4fb-d3594c067610';
            apidetails.Endpoint_URL__c ='https://test.connect.boomi.com/ws/soap12/prequal';
            apidetails.viaBoomi__c =true;
            endpoint.add(apidetails);
            if(insertBool)
                insert endpoint;
            return endpoint;
    }   
}