public class parseResponse {

    public List<TextCustomFields> textCustomFields;

    public class TextCustomFields {
        public String fieldId;
        public String name;
        public String show;
        public String required;
        public String value;
    }

    
    public static parseResponse parse(String json) {
        return (parseResponse) System.JSON.deserialize(json, parseResponse.class);
    }
}