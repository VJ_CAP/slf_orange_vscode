global class deleteAccountsShare implements Database.Batchable<sObject>{
    String strQuery = '';
    Set<string> RoleNames = new Set<string>();
    Set<String> RoleIds = new Set<string>();
    string Recordtypeval = '';
    set<string> Groupids = new set<string>();
    global deleteAccountsShare(String Accid){
        //RecordType objrecordtype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Opportunity' AND Name='Customer Opportunity Record Type' limit 1];
        RecordType objrecordtype = [SELECT id,Name, SobjectType,IsPersonType FROM RecordType WHERE SobjectType='Account' AND IsPersonType=True]; 
        system.debug('***objrecordtype--'+objrecordtype);
        Recordtypeval = objrecordtype.id;
        system.debug('****Recordtypeval---'+Recordtypeval);
        Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        system.debug('***AccRecordTypeId--'+AccRecordTypeId);
        list<Account> lstAcc;
        if(Accid != null && Accid != '')
            lstAcc = [Select id,Name from Account where id=:Accid And recordtypeid=:AccRecordTypeId and type='Partner' and Name!='Service'];
        else
           lstAcc = [Select id,Name from Account where recordtypeid=:AccRecordTypeId and type='Partner' and Name!='Service']; 
        system.debug('***lstAcc---'+lstAcc);
        if(lstAcc != null && lstAcc.size()>0){
            system.debug('***lstAcc---'+lstAcc.size());
            for(Account accobj : lstAcc){
                system.debug('*** Account Name ----'+accobj.Name);
                string RoleName = accobj.Name.trim()+' ' +'Partner User';
                system.debug('*** Role Name ----'+RoleName);
                RoleNames.add(RoleName); 
            }
        }
        system.debug('*** Role Names ----'+RoleNames);
        if(RoleNames != null && RoleNames.size()>0){
           for(UserRole Roleobj :  [select Id,Name from UserRole where Name IN:RoleNames]){
               system.debug('***Role Name and ID----'+Roleobj.Name +'----'+Roleobj.id);
                RoleIds.add(Roleobj.id);
           }
        }
        system.debug('*** RoleIds ----'+RoleIds);
        if(RoleIds != null && RoleIds.size()>0){
            for(Group grp :[SELECT Id, RelatedId, DeveloperName,Type FROM Group WHERE RelatedId IN:RoleIds and type ='Role']){
                 Groupids.add(grp.id);
                 system.debug('*** Group Name and ID ----'+grp.DeveloperName+'---'+grp.id);
            }
        }
        system.debug('*** Groupids ----'+Groupids);
        if(Groupids != null && Groupids.size()>0 && Recordtypeval != null && Recordtypeval !='')
            strQuery = 'select id,RowCause,AccountId from AccountShare where UserOrGroupId IN:Groupids and RowCause != \'Owner\'';
        system.debug('***strQuery ---'+strQuery);
        
        
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
     system.debug('***strQuery in start --'+strQuery);
        return Database.getQueryLocator(strQuery);
    
    }
    global void execute(Database.BatchableContext BC,List<AccountShare> lstofAccShare)
    {
        system.debug('****lstofAccShare---'+lstofAccShare);
        
            if(lstofAccShare != null && lstofAccShare.size()>0){
                 system.debug('****lstofAccShare size---'+lstofAccShare.size());
                Database.DeleteResult[] drList = Database.delete(lstofAccShare,false);
                system.debug('****drList status---'+drList); 
            }
        
    }
    global void finish(Database.BatchableContext BC)

    {
        System.debug('******* AccountShare Record Deletion is completed -----');
    }
    
    
}