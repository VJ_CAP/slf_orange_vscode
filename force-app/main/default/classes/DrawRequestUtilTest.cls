@isTest
public class DrawRequestUtilTest{

     // SLFUtilityDataSetup.initData();

    @testsetup static void createtestdata(){
    
      Test.startTest();
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();          
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);  
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
                TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        TriggerFlags__c trgDrawflag = new TriggerFlags__c();
        trgDrawflag.Name ='Draw_Requests__c';
        trgDrawflag.isActive__c =true;
        trgLst.add(trgDrawflag);
        
        TriggerFlags__c trgDrawAllocflag = new TriggerFlags__c();
        trgDrawAllocflag.Name ='Draw_Allocation__c';
        trgDrawAllocflag.isActive__c =true;
        trgLst.add(trgDrawAllocflag);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();  
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        //User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        Account acc = new Account();
        acc.name = 'TestAccount';
        acc.Installer_Email__c = 'testc2@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        insert acc;
        System.debug('account**'+acc);
        //Insert opportunity record
        Opportunity oppObj = new Opportunity();
        oppObj.name = 'OppNew';
        oppObj.CloseDate = system.today();
        oppObj.StageName = 'New';
        oppObj.AccountId = acc.id;
        oppObj.Installer_Account__c = acc.id;
        oppObj.Install_State_Code__c = 'CA';
        oppObj.Install_Street__c ='Street';
        oppObj.Install_City__c ='InCity';
        oppObj.Combined_Loan_Amount__c = 10000;
        oppObj.Partner_Foreign_Key__c = 'TCU||1234';
        oppObj.Co_Applicant__c = acc.id;
        insert oppObj;
        
        Underwriting_File__c uwrec = new Underwriting_File__c();           
        uwrec.Opportunity__c = oppObj.id;
        uwrec.Approved_for_Payments__c = true;
        insert uwrec;
            
        /*Draw_Requests__c drawreqs = new Draw_Requests__c();
        drawreqs.status_c = 'approved';
        insert drawreqs;*/
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc1', Email='testc1@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testc1@mail.com',ContactId = con.Id);
        insert portalUser;
        
        //List<Draw_Requests__c> lstDrawRequest = new List<Draw_Requests__c>();
        Draw_Requests__c drawreq = new Draw_Requests__c();
        drawreq.Underwriting__c = uwrec.id;
        drawreq.Status__c = 'Approved';
        //lstDrawRequest.add(drawreq)
        //insert lstDrawRequest;
        insert drawreq;
        
        List<Draw_Allocation__c> lstDrawAllocation = new List<Draw_Allocation__c>();
        Draw_Allocation__c drawAlloc = new Draw_Allocation__c();
        drawAlloc.Account__c = acc.id;
        drawAlloc.Level__c = '1';
        drawAlloc.Draw_Allocation__c = 50;
        lstDrawAllocation.add(drawAlloc);
        
        
        
        insert lstDrawAllocation;
        
        Test.stopTest();
        
        }
        private testmethod static void recalculate(){
                Account acc = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1]; 

        //Account acc=[Select id from Account where Name='Test_acc'];
        List<user> loggedInUsr = [select Id,Hash_Id__c,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc1@mail.com'];
        Opportunity opty=[Select id from Opportunity where AccountId =:acc.id];
         DrawRequestUtil.recalculate(opty.id);
         system.debug('&&&opty list&&&' +opty);
            System.runAs(loggedInUsr[0])
        {
        
        DrawRequestUtil.recalculate(opty.id);
        
        }             
        
       }
       }