/**
* Description: To generate and upload funding snapshot to Box.com
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh               01/03/2019            Created
******************************************************************************************/
global class CreateFundingSnapshotRecords implements Database.Batchable<sObject>,Schedulable,Database.Stateful,Database.AllowsCallouts {
   global Map<id,Funding_Snapshot__c> accSnapMap = new Map<id,Funding_Snapshot__c>();
   private Date endDate;
    private Date StartDate;
    global CreateFundingSnapshotRecords(){
        this.endDate = Date.today();
        this.StartDate = Date.today().addDays(-21);
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
      ID facilityAccRectypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Facility').getRecordTypeId();
        return Database.getQueryLocator('select id, name,Facility_Picklist_Key__c from Account where RecordTypeId =: facilityAccRectypeid');
    }
    
    global void execute(Database.BatchableContext BC, List<Account> AccList){    
        system.debug('### Entered into execute() of '+ CreateFundingSnapshotRecords.class); 
        System.debug('accList'+accList);
        
        for(Account acc :accList){
            list<FundingSnapshotSupport.GenerateFundingSnapshotsInput> inputs = new list<FundingSnapshotSupport.GenerateFundingSnapshotsInput>();
            FundingSnapshotSupport.GenerateFundingSnapshotsInput inp = new FundingSnapshotSupport.GenerateFundingSnapshotsInput();
            inp.accountId = acc.id;
            inp.endDate = endDate;
            inp.maxNumberOfUFs = 500;
            inp.homeImprovement = false;
            inp.prioritizedOnly = false;
            inp.startDate = startDate;
            inputs.add(inp);
            System.debug('inp'+inp);
            Set<id> FundingSnapshotIds = new Set<id>();
            Funding_Snapshot__c snapshot = new Funding_Snapshot__c();
            list<FundingSnapshotSupport.GenerateFundingSnapshotsOutput> snapshots = FundingSnapshotSupport.generateFundingSnapshots(inputs);
            for(FundingSnapshotSupport.GenerateFundingSnapshotsOutput op :snapshots){
                snapshot = op.FundingSnapshot;
            }
            accSnapMap.put(acc.id,snapshot);
        }
        
        system.debug('### Exit into execute() of '+ CreateFundingSnapshotRecords.class);
    }
    
    global void finish(Database.BatchableContext BC){
        FundingSnapshotBoxUpload batchObj = new FundingSnapshotBoxUpload(accSnapMap); 
        database.executebatch(batchObj,1);
    }
    
    global void execute(SchedulableContext sc) {
        CreateFundingSnapshotRecords batchObj = new CreateFundingSnapshotRecords(); 
        database.executebatch(batchObj,1);
    }
    
}