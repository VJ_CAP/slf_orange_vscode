@isTest
public class LoadBalanceUtilitiesTest{
    private testMethod static void testloadBalancing(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();          
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);  
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='State_Allocation__c';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);            
        insert trgLst;
        
        
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();
        Account accObj = new Account();
        accObj.name = 'Test Account';
        accObj.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        accObj.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        accObj.BillingStateCode = 'CA';
        accObj.FNI_Domain_Code__c = 'CRB';
        accObj.Push_Endpoint__c ='www.test.com';
        accObj.Installer_Legal_Name__c='Bright planet Solar';
        accObj.Solar_Enabled__c = true;        
        accObj.Type = 'Facility';
        insert accObj;
        
        State_Allocation__c saObj1 = new State_Allocation__c(Installer_Account__c=accObj.Id, 
                                                            State_Code__c='AZ',
                                                            Facility_1__c=accObj.Id,
                                                            Facility_1_Allocation__c = 25,
                                                            Facility_2_Allocation__c = 25,
                                                            Facility_3_Allocation__c = 25,
                                                            Facility_4_Allocation__c = 25,
                                                            Facility_1_5_Year_Allocation__c = 50,
                                                            Facility_1_10_Year_Allocation__c = 50,
                                                            Facility_1_12_Year_Allocation__c = 50,
                                                            Facility_1_15_Year_Allocation__c = 50,
                                                            Facility_1_20_Year_Allocation__c = 50,
                                                            Facility_1_25_Year_Allocation__c = 50
                                                            );
        try{
            insert saObj1;
        }
        catch(DMLException de){
            
            boolean result=true;
            System.assert(result);
            if(de.getMessage().contains('5'))
                System.assert(de.getMessage().contains('Allocation must sum up to 100%. Please adjust allocation across the Facilities for 5 Year.'));
            
        }
        
        State_Allocation__c saObj = new State_Allocation__c(Installer_Account__c=accObj.Id, 
                                                            State_Code__c='CA',
                                                            Facility_1__c=accObj.Id,
                                                            Facility_1_Allocation__c = 25,
                                                            Facility_2_Allocation__c = 25,
                                                            Facility_3_Allocation__c = 25,
                                                            Facility_4_Allocation__c = 25,
                                                            Facility_1_10_Year_Allocation__c = 100
                                                            );
        insert saObj;
     
        System.assertNotEquals(null,saObj);
        
        
        
        saObj.Facility_1_5_Year_Allocation__c = 50;
        saObj.Facility_1_10_Year_Allocation__c = 50;
        saObj.Facility_1_12_Year_Allocation__c = 50;
        saObj.Facility_1_15_Year_Allocation__c = 50;
        saObj.Facility_1_20_Year_Allocation__c = 50;
        saObj.Facility_1_25_Year_Allocation__c = 50;
         try{
            update saObj;
        }
        catch(DMLException de){
            
            boolean result=true;
            System.assert(result);
            if(de.getMessage().contains('5'))
                System.assert(de.getMessage().contains('Allocation must sum up to 100%. Please adjust allocation across the Facilities for 5 Year.'));
            
        }

        //To cover try catch block
        StateAllocationTriggerHandler stAllocObj = new StateAllocationTriggerHandler(true);
        stAllocObj.OnBeforeInsert(null);
        stAllocObj.OnBeforeUpdate(null,null);
        
        Test.StartTest();
        LoadBalanceUtilities.getFacility(accObj.Id,'CA',new Map<String,String>{'CRB'=>'A'},120);
        LoadBalanceUtilities.getFacility(accObj.Id,'CA',new Map<String,String>{'CRB'=>'A','CVX'=>'A'},120);        
        LoadBalanceUtilities.getFacility(accObj.Id,'CA',new Map<String,String>{'CRB'=>'P','CVX'=>'P'},120);
        LoadBalanceUtilities.getFacility(accObj.Id,'CA',new Map<String,String>{'CRB'=>'D','CVX'=>'D'},120);
        Test.StopTest();
    }
}