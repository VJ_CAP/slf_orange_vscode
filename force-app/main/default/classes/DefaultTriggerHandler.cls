/**
* Default no-op implementation of TriggerHandler. Each sObject's TriggerHandler must extend it to selectively overide the methods.  
*
* @Author Salesforce
* @Date 19/11/2014
*/
public virtual without sharing class DefaultTriggerHandler implements TriggerHandler {
    
    /**
    * This method is called prior to execution of a BEFORE trigger. 
    * Use this to cache any data required into maps prior to execution of the trigger.
    * 
    * 
    * @param void
    */
    public virtual void bulkBefore(){}
    
    /**
    * This method is called prior to execution of an AFTER trigger. 
    * Use this to cache any data required into maps prior to execution of the trigger.                                                      
    * 
    * 
    * @param void
    */
    
    public virtual void bulkAfter(){            
        
    }
   
    /**
    * Purpose: This method is called iteratively for each record to be inserted during 
    * a BEFORE trigger. Never execute any SOQL/SOSL operations in iterative method.
    * 
    * 
    * @param so - List<sObject>
    */
   
    public virtual void beforeInsert(List<SObject> so){}
    
    /**
    * This method is called iteratively for each record to be updated during 
    * a BEFORE trigger. Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSoMap - Map<Id,SObject>
    * @param newSoMap - Map<Id,SObject>                                                          
    */
   
    public virtual void beforeUpdate(map<id,SObject> oldSoMap, map<id,SObject> newSoMap){}
    
    /**
    * This method is called iteratively for each record to be deleted during 
    * a BEFORE trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSoMap - Map<Id,SObject>
    *                                                         
    */
    
    public virtual void beforeDelete(map<id,SObject> oldSoMap){}
    
    /**
    *
    * This method is called iteratively for each record inserted during an AFTER
    * trigger. Always put field validation in the 'After' methods in case another trigger
    * has modified any values. The record is 'read only' by this point.  
    * Never execute any SOQL/SOSL operations in iterative method.
    *
    * @param newSoMap - Map<Id,SObject>
    */
    
    public virtual void afterInsert(map<id,SObject> newSoMap){}
 
    /**
    *
    * This method is called iteratively for each record updated during an 
    * AFTER trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSoMap - Map<Id,SObject>
    * @param newSoMap - Map<Id,SObject>  
    */
    

   
    public virtual void afterUpdate(map<id,SObject> oldSoMap, map<id,SObject> newSoMap){
        
    }
    
    /**
    * This method is called iteratively for each record deleted during an 
    * AFTER trigger.  Never execute any SOQL/SOSL operations in iterative method.
    * 
    * @param oldSoMap - Map<Id,SObject>
    */ 
     
    public virtual void afterDelete(map<id,SObject> oldSoMap){}
    
    /**
    *
    * This method is called once any record is Undeleted.                                                 
    *
    * @param newSoMap - Map<Id,SObject>
    *
    */
    
    public virtual void afterUndelete(map<id,SObject> newSoMap){}
    
    /**
    * This method is called once all records have been processed by the trigger.
    * Use this method to accomplish any final operations such as creation or updates of other records.   
    *
    *                                           
    */
    
    public virtual void andFinally(){
                      
    }
}