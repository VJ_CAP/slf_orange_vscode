/**
* Description: Applicant API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suresh Kumar           06/12/2017           Created
******************************************************************************************/
public with sharing class ForgotPasswordServiceImpl extends RESTServiceBase {
private String msg = '';
    
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + ForgotPasswordServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        IRestResponse iRestRes;
        string errMsg = '';
        UnifiedBean respBean = new UnifiedBean();
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);

            system.debug('### reqData '+reqData);
            if(reqData.users != null)
            {
                if((null != reqData.users[0].userName && String.isNotEmpty(reqData.users[0].userName))||(null != reqData.users[0].email && String.isNotEmpty(reqData.users[0].email)))
                {
                    try{
                        List<User> lstUser = new List<User>();
                        if(null != reqData.users[0].userName && String.isNotEmpty(reqData.users[0].userName)){
                            lstUser = [select id,IsActive,Email,contactId,contact.Reset_Password_Flag__c from user where username=:reqData.users[0].userName and IsActive=true];
                        }else if(null != reqData.users[0].email && String.isNotEmpty(reqData.users[0].email)){
                            lstUser = [select id,IsActive,Email,contactId,contact.Reset_Password_Flag__c from user where email=:reqData.users[0].email and IsActive=true];
                        }
                        if(lstUser!=null && lstUser.size()>0){
                            User objUser = lstUser.get(0);
                            String sRecipientEmail = objUser.Email;
                            List<EmailTemplate> emailTemplatLst = [SELECT Id, DeveloperName,HtmlValue,Body FROM EmailTemplate WHERE DeveloperName =: 'Forgot_Password_Link'];
                            
                            Map<String, EmailTemplate> emailTemplateMap = new Map<String, EmailTemplate>();
                            for(EmailTemplate templateIterate : emailTemplatLst)
                            {
                                emailTemplateMap.put(templateIterate.DeveloperName, templateIterate);
                            }
                            
                            EmailTemplate emailTemplateRec = emailTemplateMap.get('Forgot_Password_Link');
                            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                            msg.setTemplateId(emailTemplateRec.Id);
                            msg.setTargetObjectId(objUser.Id);
                            msg.setSaveAsActivity(false);
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                            
                            Contact con = new Contact();
                            con.id = objUser.contactId;
                            con.Reset_Password_Flag__c = true;
                            update con;
                            
                            respBean.returnCode = '200';
                           // respBean.message = 'You have been sent an e-mail with instructions on how to reset your password. Please check your inbox.';
                           // Changed by Veera part of 4484
                            respBean.message = 'The user has been sent an e-mail with instructions on how to reset their password. Please have them check their inbox.';
                            iRestRes = (IRestResponse)respBean;
                            res.response= iRestRes;
                        }
                        else{
                            System.debug('### No user found in the system ');
                            respBean.returnCode = '400';
                            respBean.message = ErrorLogUtility.getErrorCodes('400', null);
                            
                            iRestRes = (IRestResponse)respBean;
                            res.response= iRestRes;
                        }
                    }catch(Exception exc){
                        System.debug('### Error occured: '+exc.getMessage());
                        errMsg= ErrorLogUtility.getErrorCodes('400', null);
                        gerErrorMsg(respBean,errMsg,'400');
                        iRestRes = (IRestResponse)respBean;
                        res.response= iRestRes;
                    }
                }
            }
            else{
                errMsg= ErrorLogUtility.getErrorCodes('400', null);
                gerErrorMsg(respBean,errMsg,'400');
                iRestRes = (IRestResponse)respBean;
                res.response= iRestRes;
            }
        }catch(Exception err){
            system.debug('### ForgotPasswordServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('ForgotPasswordServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            
            errMsg= ErrorLogUtility.getErrorCodes('214', null);
            gerErrorMsg(respBean,errMsg,'214');
            iRestRes = (IRestResponse)respBean;
            res.response= iRestRes;
        }
        system.debug('### Exit from fetchData() of ' + ForgotPasswordServiceImpl.class);
        return res.response ;
    }
    
    public void gerErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode){
        List<UnifiedBean.ErrorWrapper> errorList = new List<UnifiedBean.ErrorWrapper>();
        UnifiedBean.ErrorWrapper errorWraper = new UnifiedBean.ErrorWrapper();
        errorWraper.errorMessage = sErrorMsg;
        errorList.add(errorWraper);
        objUnifiedBean.error = errorList;
        objUnifiedBean.returnCode = sReturnCode;
    }
}