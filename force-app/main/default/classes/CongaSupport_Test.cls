@isTest
private class CongaSupport_Test 
{
    
    @isTest static void test1 () 
    {
        
        //insert custom setting TriggerFlag records.
        //start
        SLFUtilityTest.createCustomSettings();
        //stop
        test.starttest();
        APXTConga4__Conga_Template__c conTemp = new APXTConga4__Conga_Template__c (APXTConga4__Name__c = 'Combo Loan Cover Page');
        insert conTemp;

        RecordType recTyp = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'PersonAccount' LIMIT 1];

        Account acc = new Account (Name = 'Test installer', Type = 'Partner', M0_Split__c = 40, FNI_Domain_Code__c = 'TCU',Solar_Enabled__c=true);
        insert acc;

        Installer_Incentive__c insInc = new Installer_Incentive__c (   Installer_Account__c = acc.Id,
                                                                    Start_Date__c = System.today().addDays(-200),
                                                                    End_Date__c = System.today().addDays(-100),
                                                                    Amount__c = 100);
        insert insInc;

        Account acc1 = new Account (Lastname = 'Test customer', RecordTypeId = recTyp.Id, FNI_Domain_Code__c = 'CVX');
        insert acc1;

        Contact con = new Contact (LastName = 'Test Contact 2');
        insert con;

        Account acc2 = new Account (Name = 'TCU', Facility_Picklist_Key__c = 'TCU', Type = 'Facility', FNI_Domain_Code__c = 'CRB',Solar_Enabled__c=true);
        insert acc2;
        
        List<Product__c>  lstprod1 = new list<Product__c>();
        List<Product__c>  lstprod2 = new list<Product__c>();
        List<Product__c>  lstprod3 = new list<Product__c>();
       
        
        
        //This is for Loan Type = Same As Cash
        Product__c proda = new Product__c (Name = 'Testv', Short_Term_Facility__c = 'TCU',
                                            Long_Term_Facility__c = '',
                                            Long_Term_Facility_Lookup__c = NULL,
                                            Short_Term_Facility_Lookup__c = acc2.Id,
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = acc.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            APR__c = 0.0,
                                            Term_mo__c = 0.0,
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        //insert proda;
        lstprod1.add(proda);
        //This is for Loan Type = Single
         Product__c prodb = new Product__c (Name = 'Testprodb', Short_Term_Facility__c = '',
                                            Long_Term_Facility__c = 'TCU',
                                            Long_Term_Facility_Lookup__c = acc2.Id,
                                            Short_Term_Facility_Lookup__c = NULL,
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = acc.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            APR__c = 0.0,
                                            Term_mo__c = 0.0,
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        //insert prodb;
        lstprod1.add(prodb);
        //This is for Loan Type = Combo
         Product__c prodc = new Product__c (Name = 'Testprodc', 
                                            Short_Term_Facility__c = 'TCU',
                                            Long_Term_Facility__c = 'CRB',
                                            Long_Term_Facility_Lookup__c = acc.Id,
                                            Short_Term_Facility_Lookup__c = acc2.Id,
                                            FNI_Min_Response_Code__c = 1, 
                                            Installer_Account__c = acc.Id, 
                                            Qualification_Message__c = 'test', 
                                            State_Code__c = 'CA', 
                                            Product_Display_Name__c = 'Test',
                                            APR__c = 0.0,
                                            Term_mo__c = 0.0,
                                            Is_Active__c = true,
                                            Product_Tier__c = '0');
        //insert prodc;
        lstprod1.add(prodc);
        
        if(lstprod1 !=  null && lstprod1.size()>0)    
        insert lstprod1;
                        
        //inserting opportunity
        Opportunity oppProd = new Opportunity (Name = 'Test Opp 1', 
                                            AccountId = acc.Id,
                                            long_term_loan_amount_manual__c = null,
                                            Monthly_Payment_Escalated_Single_Loan__c = 0.0,
                                            Final_Monthly_Payment__c = 0.0,
                                            Combined_Loan_Amount__c = 100.0,
                                            Installer_Account__c = acc.Id,
                                            Monthly_Payment__c = 100.0,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = proda.Id,
                                            Application_Status__c = 'Auto Approved',
                                            Approved_LT_Facility__c = 'TCU',
                                            Approved_ST_Facility__c = 'TCU',
                                            Short_Term_Loan_Amount_Manual__c = 0.0,
                                            StageName = 'Test', 
                                            Primary_Contact__c = con.Id,
                                            Long_Term_Amount_Approved__c = 100.0,
                                            CloseDate = System.today().addDays(30));
        insert oppProd;
        
        
        
        Opportunity oppProd1 = new Opportunity (Name = 'Test Opp 2', 
                                            AccountId = acc1.Id,
                                            long_term_loan_amount_manual__c=0.0,
                                            Monthly_Payment_Escalated_Single_Loan__c = 0.0,
                                            Monthly_Payment__c = 120.0,
                                            Installer_Account__c = acc.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = prodb.Id,
                                            Application_Status__c = 'Auto Approved',
                                            Approved_LT_Facility__c = 'TCU',
                                            Approved_ST_Facility__c = 'TCU',
                                            Short_Term_Loan_Amount_Manual__c = 0.0,
                                            Long_Term_Amount_Approved__c = 0.0,
                                            Combined_Loan_Amount__c = null,
                                            StageName = 'Test', 
                                            Primary_Contact__c = con.Id,
                                            CloseDate = System.today().addDays(30));
        insert oppProd1;
        
        
        Opportunity oppProd2 = new Opportunity (Name = 'Test Opp 3', 
                                            AccountId = acc2.Id,
                                            long_term_loan_amount_manual__c = 0.0,
                                            Monthly_Payment_Escalated_Single_Loan__c = 0.0,
                                            Monthly_Payment__c = 120.0,
                                            Installer_Account__c = acc.Id,
                                            Install_State_Code__c = 'CA',
                                            SLF_Product__c = prodc.Id,
                                            Application_Status__c = 'Auto Approved',
                                            Approved_LT_Facility__c = 'TCU',
                                            Approved_ST_Facility__c = 'TCU',
                                            Short_Term_Loan_Amount_Manual__c = 0.0,
                                            Long_Term_Amount_Approved__c = null,
                                            Combined_Loan_Amount__c = null,
                                            StageName = 'Test', 
                                            Primary_Contact__c = con.Id,
                                            CloseDate = System.today().addDays(30));
        insert oppProd2;
        
        
                

        Underwriting_File__c uf = new Underwriting_File__c (Opportunity__c = oppProd.Id, 
                                                                ST_Amount_Financed__c = 1000,
                                                               LT_Amount_Financed__c = 2000,
                                                                M0_Approval_Date__c = System.today(),
                                                                M1_Approval_Date__c = System.today(),
                                                                M2_Approval_Date__c = System.today());
        insert uf;

        //ApexPages.StandardController sc1 = new ApexPages.StandardController (oppProd);

        LoanDocsServiceImpl cs1 = new LoanDocsServiceImpl ();

        //cs1.go ();
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd.Id);
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd1.Id);
       // cs1.getCongaTemplatesForOpportunity (oppProd2.Id);
        
        Document doc1 = new Document();
        doc1.Body = Blob.valueOf('Some Text');
        doc1.ContentType = 'application/pdf';
        doc1.DeveloperName = 'my_document';
        doc1.IsPublic = true;
        doc1.Name = 'Sunlight Logo';
        doc1.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert doc1;
     
        
        proda.apr__c = 12;
        prodb.apr__c = 12;
        prodc.apr__c = 12;
        lstprod2.add(proda);
        lstprod2.add(prodb);
        lstprod2.add(prodc);
        /*update proda;
        update prodb;
        update prodc; */
        
        if(lstprod2 != null && lstprod2.size()>0)
        update lstprod2;
        test.stopTest();      
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd.Id);
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd1.Id);
      //  LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd2.Id);
        
        oppProd.short_term_loan_amount_manual__c = 10;
        oppProd.long_term_loan_amount_manual__c = 100;
        
        oppProd1.short_term_loan_amount_manual__c = 10;
        oppProd1.long_term_loan_amount_manual__c = 100;
        
        oppProd2.short_term_loan_amount_manual__c = 10;
        oppProd2.long_term_loan_amount_manual__c = 100;
        List<Opportunity> lstoppPrd = new List<Opportunity>();
        lstoppPrd.add(oppProd);
        lstoppPrd.add(oppProd1);
        lstoppPrd.add(oppProd2);
        
        if(lstoppPrd != null && lstoppPrd.size()>0)
        update lstoppPrd;
        
        /*update oppProd;
        update oppProd1;
        update oppProd1;*/
        
        //---------------
        
        
        proda.Short_Term_Facility_Lookup__c = acc2.id;
        //proda.Long_Term_Facility_Lookup__c = acc.id;
        
        //prodb.Short_Term_Facility_Lookup__c = acc.id;
        prodb.Long_Term_Facility_Lookup__c = acc.id;
        
        prodc.Short_Term_Facility_Lookup__c = acc.id;
        prodc.Long_Term_Facility_Lookup__c = acc.id;
        list<Product__c> listprod4 = new List<Product__c>();
        /*update proda;
        update prodb;
        update prodc;*/
        listprod4.add(proda);
        listprod4.add(prodb);
        listprod4.add(prodc);
        update listprod4;
        
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd.Id);
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd1.Id);
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd2.Id);
        
        //-------------------------
        
        proda.Short_Term_Facility_Lookup__c = acc.id;
        proda.Long_Term_Facility_Lookup__c = NULL;
        
        prodb.Short_Term_Facility_Lookup__c = NULL;
        prodb.Long_Term_Facility_Lookup__c = acc1.id;
        
        prodc.Short_Term_Facility_Lookup__c = acc2.id;
        prodc.Long_Term_Facility_Lookup__c = acc2.id;
        
        /*update proda;
        update prodb;
        update prodc;*/
        lstprod3.add(proda);
        lstprod3.add(prodb);
        lstprod3.add(prodc);
        
        update lstprod3;
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd.Id);
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd1.Id);
      //  cs1.getCongaTemplatesForOpportunity (oppProd2.Id); 
        
        //----------------------
        
        prodc.Short_Term_Facility_Lookup__c = acc.id;
        prodc.Long_Term_Facility_Lookup__c = acc2.id;
        
        update prodc;
        
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd2.Id);
        
        //----------------------
        
        prodc.Short_Term_Facility_Lookup__c = acc1.id;
        prodc.Long_Term_Facility_Lookup__c = acc1.id;
        
        update prodc;
        
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd2.Id);
        
        //-----------------------

        prodb.Long_Term_Facility_Lookup__c = acc2.id;
        
        prodc.Short_Term_Facility_Lookup__c = acc2.id;
        prodc.Long_Term_Facility_Lookup__c = acc.id;

        update prodb;
        update prodc;
        
        LoanDocsServiceImpl.getCongaTemplatesForOpportunity (oppProd1.Id);
       // cs1.getCongaTemplatesForOpportunity (oppProd2.Id);
        
    }   
}