/**
* Description: Get Co-Signer Loan Docs Logic
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Rajesh                03/12/2019              Created
******************************************************************************************/
public class CoSignerLoanDocsServiceImpl extends RESTServiceBase{  
    /**
* Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
*          
* @param rw    Get all POST request data.
*
* return res   Send response back with data and response status code.
*/
    public override IRestResponse fetchData(RequestWrapper rw){
        system.debug('### Entered into CoSignerLoanDocs.fetchData() of '+CoSignerLoanDocsServiceImpl.class);
        //UnifiedBean reqData = (UnifiedBean)stsDtObj;
        UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
        
        UnifiedBean unifiedRespBean = new UnifiedBean();
        unifiedRespBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        unifiedRespBean.error = new list<UnifiedBean.errorWrapper>();
        
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            string errMsg = '';
        IRestResponse iRestRes;
        //Savepoint sp = Database.setSavepoint();
         String resEnvelopeId = '';
         SLF_Boomi_API_Details__c c = SLF_Boomi_API_Details__c.getValues('DocusignAPI'); 
         resEnvelopeId = reqData.projects[0].envelopeId;
         try{
        if(String.isNotBlank(resEnvelopeId))
        {
            List<dsfs__DocuSign_Recipient_Status__c> objList = [Select id,name,dsfs__DocuSign_Recipient_Email__c,dsfs__DocuSign_Routing_Order__c,dsfs__Parent_Status_Record__r.dsfs__Opportunity__r.Co_Applicant__c from dsfs__DocuSign_Recipient_Status__c where dsfs__Envelope_Id__c = :resEnvelopeId AND dsfs__DocuSign_Routing_Order__c = 2 AND dsfs__Recipient_Status__c!= 'Completed'];
            Map<String,String> wordMap= new Map<String,String>{'á'=>'a', 'é'=>'e' ,'í'=>'i', 'ó'=>'o', 'ú'=>'u', 'ü'=>'u', 'ñ'=>'n'};
            if(!objList.isEmpty()){
                HttpRequest httpRequest2 = new HttpRequest();  
                httpRequest2.setMethod('POST');   
                httpRequest2.setHeader('X-DocuSign-Authentication','<DocuSignCredentials><Username>'+c.username__c+'</Username><Password>'+c.password__c+'</Password><IntegratorKey>'+c.APIKey__c+'</IntegratorKey></DocuSignCredentials>');
                httpRequest2.setHeader('Content-Type', 'application/json');        
                httpRequest2.setEndpoint(c.Endpoint_URL__c+resEnvelopeId+'/views/recipient');  
        
                InpersonReqWrpr inpersonReq = new InpersonReqWrpr();
                //inpersonReq.userName = objList[0].Name;
                String applicantName = objList[0].Name;
                if(applicantName.contains('á') || applicantName.contains('é') || applicantName.contains('í') || applicantName.contains('ó') || applicantName.contains('ú') || applicantName.contains('ü') || applicantName.contains('ñ') ){
                    for(String key :wordMap.keySet()){
                        applicantName = applicantName.replace(key,wordMap.get(key));
                    }
                }
                inpersonReq.userName = applicantName;
                inpersonReq.email = objList[0].dsfs__DocuSign_Recipient_Email__c;
                inpersonReq.recipientId = '2';
                inpersonReq.clientUserId = objList[0].dsfs__Parent_Status_Record__r.dsfs__Opportunity__r.Co_Applicant__c;
                inpersonReq.authenticationMethod = 'email';
                //inpersonReq.returnUrl = Label.In_Person;
                inpersonReq.returnUrl = Label.In_Person+'?event=cancel&event=decline&event=exception&event=fax_pending&event=id_check_failed&event=session_timeout&event=signing_complete&event=ttl_expired&event=viewing_complete';
                httpRequest2.setBody(JSON.serialize(inpersonReq));
                System.debug('### httpRequest2'+httpRequest2);
                Http http2 = new Http();   
                HttpResponse httpResponse2 = http2.send(httpRequest2);  
                if (httpResponse2.getStatusCode() == 201) {  
                    System.debug('httpResponse2.getBody():'+httpResponse2.getBody());
                    if(httpResponse2 != null && String.isNotBlank(httpResponse2.getBody())){
                        InpersonResWrpr response2 = (InpersonResWrpr)System.JSON.deserialize(httpResponse2.getBody(), InpersonResWrpr.class);
                        UnifiedBean.OpportunityWrapper projwrap = new UnifiedBean.OpportunityWrapper();
                         projwrap.envelopeURL =  response2.url;
                         unifiedRespBean.projects.add(projwrap);
                         unifiedRespBean.returnCode = '200';
                    }
                    
                } else {  
                    System.debug(' httpResponse2 ' + httpResponse2.getBody() );  
                    throw new CalloutException( httpResponse2.getBody() );  
                }
            }
        }  
           
         }           
        Catch(Exception err){
            //Database.rollback(sp);
            iolist = new list<DataValidator.InputObject>{};
            errMsg = ErrorLogUtility.getErrorCodes('400',null);
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            unifiedRespBean.error.add(errorMsg);
            unifiedRespBean.returnCode = '400';
            iRestRes = (IRestResponse)unifiedRespBean;
            system.debug('### CoSignerLoanDocsServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('CoSignerLoanDocsServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }
        system.debug('### Exit from fetchData() of '+CoSignerLoanDocsServiceImpl.class);
        iRestRes = (IRestResponse)unifiedRespBean;
        system.debug('### iRestRes '+iRestRes);
        return iRestRes;
    }
    
   
    public class InpersonReqWrpr{
        public String userName;
        public String email;
        public String recipientId;
        public String clientUserId;
        public String authenticationMethod;
        public String returnUrl;
    }
     public class InpersonResWrpr{
        public String url;       
    }
}