/**
* Description: AmortizationScheduleServiceImpl API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Brahmeswar            04/03/2019             Created
******************************************************************************************/
public with sharing class LoandocsAmortizationServiceImpl extends RESTServiceBase{  
    private String msg = '';
    /**
    * Description:    
    *          
    *
    * @param reqAP      Get all POST request action parameters
    * @param reqData    Get all POST request data
    *
    * return res        Send response back with data and response status code
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + LoandocsAmortizationServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        LoandocsAmortizationDataServiceImpl DataSrvImpl = new LoandocsAmortizationDataServiceImpl(); 
        IRestResponse iRestRes;
        
        try{
            system.debug('### rw.reqDataStr '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            system.debug('### reqData '+reqData);
            
            res.response = DataSrvImpl.transformOutput(reqData);
            system.debug('response in LoandocsAmortizationServiceImpl ---'+res.response);

        }catch(Exception err){
           
            system.debug('### LoandocsAmortizationServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' LoandocsAmortizationServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
            
            UnifiedBean unifiedBean = new UnifiedBean(); 
            List<UnifiedBean.errorWrapper> errorWrapperLst = new List<UnifiedBean.errorWrapper>();
            UnifiedBean.errorWrapper errorWrapper = new UnifiedBean.errorWrapper();
            errorWrapper.errorMessage = ErrorLogUtility.getErrorCodes('214', null);
            errorWrapperLst.add(errorWrapper);
            unifiedBean.error  = errorWrapperLst ;
            //unifiedBean.error.add(errorMsg);
            unifiedBean.returnCode = '214';
            iRestRes = (IRestResponse)unifiedBean;
            res.response= iRestRes;
        }
       
        system.debug('### Exit from fetchData() of ' + LoandocsAmortizationServiceImpl.class);
        
        return res.response;
    }
}