@isTest
public class ProjectDetailsDataServiceImplTest {    
    
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();
        //Insert test records for Home
    }
    
    /*
* Description: This method is used to cover ProjectDetailsServiceImpl AND ProjectDetailsDataServiceImpl
*
*/
    private testMethod static void projectdetailsTest(){
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        
        acc.Inspection_Required_Documents_Solar__c = '  Inspection Document';
        acc.Permit_Required_Documents_Solar__c = 'Permit Application';
        acc.Kitting_Required_Documents_Solar__c = ' Kitting Invoice';
        update acc;
        
        Opportunity opp = [select Id,Name,Hash_Id__c from opportunity where Installer_Account__c =: acc.Id LIMIT 1];
        opp.ACH_APR_Process_Required__c = true;
        opp.All_HI_Required_Documents__c = true;
        update opp;
        
        Note noteRec = new Note();
        noteRec.body = 'test';
        noteRec.title = 'test';
        noteRec.ParentId = opp.Id;
        insert noteRec;
        
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = opp.id;
        undStpbt.Approved_for_Payments__c = true;
        insert undStpbt;
        
        
        Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', Email_Text__c = 'Test data', POS_Message__c = 'test', Review_Classification__c = 'SLS II');
        insert sd;
        
        Stipulation__c stp = new Stipulation__c();
        stp.Status__c ='New';
        stp.Stipulation_Data__c = sd.id;
        stp.Underwriting__c =undStpbt.id;
            insert stp; 
        
        
        box__FRUP__c frup1 = new box__FRUP__c (box__Object_Name__c = 'Opportunity',box__Folder_ID__c = opp.Id,box__Record_ID__c = opp.Id);
        insert frup1; 
        
        Document doc = [Select id,name,folderid from Document where name = 'Sunlight Logo'];
        /*Document doc1 = new Document();
doc1.Body = Blob.valueOf('Some Text');
doc1.ContentType = 'application/pdf';
doc1.DeveloperName = 'my_document';
doc1.IsPublic = true;
doc1.Name = 'Sunlight Logo';
doc1.FolderId = [select id from folder where name = 'Shared Documents'].id;
insert doc1;*/
        
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            Map<String, String> projectdocsMap = new Map<String, String>();
            // Userd to Cover StatusServiceImpl class
            SF_Category_Map__c sfmap = new SF_Category_Map__c();
            sfmap.Name = 'Loan Agreement';
            sfmap.Folder_Name__c = 'Loan Agreements';
            sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
            sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
            sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'LT_Loan_Agreement_Received__c';
            insert sfmap;
            
            SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                             Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                             TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
            insert cm1;
            
            string projectJsonreq = '{"projects":[{"id":"'+opp.id+'"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"externalId":"'+opp.id+'"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"hashId":"'+opp.Hash_Id__c+'","isStipSuppressed":"false"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"id":""}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"id":"12erfdtu","externalId":"345ty6r"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"id":"12erfdtu","externalId":""}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            //To cover exception block
            ProjectDetailsDataServiceImpl projectDataSrvImpl = new ProjectDetailsDataServiceImpl();
            projectDataSrvImpl.transformOutput(null); 
            
            ProjectDetailsServiceImpl projectSrvImpl = new ProjectDetailsServiceImpl();
            projectSrvImpl.fetchData(null);
            
            Test.stopTest();
        }
        
    }
    
    private testMethod static void homeProjectdetailsTest(){
        
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com'];
        
        //Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        System.runAs(loggedInUsr[0])
        {
            Test.startTest();
            
            Account acc = new Account();
            acc.name = 'Test Account';
            acc.Installer_Email__c = 'test@gmail.com';
            acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            acc.BillingStateCode = 'CA';
            acc.FNI_Domain_Code__c = 'TCU';
            acc.Installer_Legal_Name__c='Bright planet Solar';
            acc.Solar_Enabled__c = true;
            acc.FNI_Domain_Code__c = 'BrightPlanet';
            acc.Hot_Water_Heater_eligible__c=true;
            acc.Hot_Water_heater_eligible_products__c='Battery only';
            
            insert acc;
            /*Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1];
acc.FNI_Domain_Code__c = 'BrightPlanet';
update acc;*/
            
            Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
            insert con; 
            //Insert SLF Product Record
            List<Product__c> prodList = new List<Product__c>();
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='HII';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 12.99;
            //prod.Long_Term_Facility__c = 'CRB';
            prod.APR__c = 12.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            //prod.Product_Tier__c ='1';
            prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 180;
            prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            insert prod; 
            
            //Insert Person Account record
            Account personAcc = new Account();
            personAcc.Name = 'SLFTest1';
            personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
            personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
            personAcc.BillingStateCode = 'CA';
            personAcc.Installer_Legal_Name__c='Bright planet Solar';
            personAcc.Solar_Enabled__c = false;
            //personAcc.OwnerId = portalUser.Id;
            insert personAcc; 
            
            Opportunity oppHome = new Opportunity();
            oppHome.name = 'OppHome';
            oppHome.CloseDate = system.today()+1;
            oppHome.StageName = 'New';
            oppHome.AccountId = acc.id;
            oppHome.Installer_Account__c = acc.id;
            oppHome.Install_State_Code__c = 'CA';
            oppHome.Install_Street__c ='Street';
            oppHome.Install_City__c ='InCity';
            oppHome.Combined_Loan_Amount__c = 10000;
            oppHome.Project_Category__c = 'Home';            
            oppHome.Partner_Foreign_Key__c = 'BrightPlanet||BPS24475';
            oppHome.Approved_LT_Facility__c = 'CRB';
            oppHome.SLF_Product__c = prod.id;
            //oppHome.Opportunity__c = personOpp.Id;
            oppHome.Co_Applicant__c = acc.id;
            oppHome.All_HI_Required_Documents__c = true;
            insert oppHome;
            //Opportunity opp = [select Id,Name,Hash_Id__c from opportunity where Installer_Account__c =: acc.Id and name='OppHome' LIMIT 1];
            
            Underwriting_File__c underWrFilRec = new Underwriting_File__c();
            underWrFilRec.name = 'Underwriting FileOne';
            underWrFilRec.ACT_Review__c ='Yes';
            underWrFilRec.Opportunity__c = oppHome.Id;
            underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
            underWrFilRec.Title_Review_Complete_Date__c = system.today();
            underWrFilRec.Install_Contract_Review_Complete__c = true;
            underWrFilRec.Utility_Bill_Review_Complete__c = true;
            underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
            underWrFilRec.Installation_Photos_Review_Complete__c = true;
            //underWrFilRec.LT_Loan_Agreement_Received__c = system.today();
            //underWrFilRec.Final_Design_Document_Received__c = system.today();
            underWrFilRec.Final_Design_Review_Complete__c = true;   
            //underWrFilRec.PTO_Received__c = system.today();
            underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
            
            insert underWrFilRec;   
            
            
            Note noteRec = new Note();
            noteRec.body = 'test';
            noteRec.title = 'test';
            noteRec.ParentId = oppHome.Id;
            insert noteRec;
            
            box__FRUP__c frup1 = new box__FRUP__c (box__Object_Name__c = 'Opportunity',box__Folder_ID__c = oppHome.Id,box__Record_ID__c = oppHome.Id);
            insert frup1;  
                        Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', 
                                                              Email_Text__c = 'Test data', 
                                                              POS_Message__c = 'test',
                                                             Folder_Name__c='Government IDs',
                                                             Review_Classification__c = 'SLS I');
                        insert sd;
            
            Stipulation__c stp = new Stipulation__c();
            stp.Status__c ='Completed';
            stp.Stipulation_Data__c = sd.id;
            stp.Underwriting__c =underWrFilRec.id;
            insert stp;
            
            
            Map<String, String> projectdocsMap = new Map<String, String>();
            // Userd to Cover StatusServiceImpl class
            SF_Category_Map__c sfmap = new SF_Category_Map__c();
            sfmap.Name = 'Loan Agreement';
            sfmap.Folder_Name__c = 'Loan Agreements';
            sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
            sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
            sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'LT_Loan_Agreement_Received__c';
            insert sfmap;
            
            SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                             Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                             TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
            insert cm1;
            
            string projectJsonreq = '{"projects":[{"id":"'+oppHome.id+'"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"externalId":"'+oppHome.id+'"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"hashId":"'+oppHome.Hash_Id__c+'"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"id":""}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"id":"12erfdtu","externalId":"345ty6r"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            projectJsonreq = '{"projects":[{"id":"12erfdtu","externalId":""}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            prod.Product_Loan_Type__c = 'HIS';
            update prod;
            projectJsonreq = '{"projects":[{"id":"'+oppHome.id+'"}]}';            
            MapWebServiceURI(projectdocsMap,projectJsonreq,'getprojectdetails');
            
            ProjectDetailsDataServiceImpl.compareValues('test1;test2','test1');
            ProjectDetailsDataServiceImpl.compareValues('test1;test2','test3');
            
            //To cover exception block
            ProjectDetailsDataServiceImpl projectDataSrvImpl = new ProjectDetailsDataServiceImpl();
            projectDataSrvImpl.transformOutput(null); 
            
            ProjectDetailsServiceImpl projectSrvImpl = new ProjectDetailsServiceImpl();
            projectSrvImpl.fetchData(null);
            
            
            Test.stopTest();
        }
    }
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
}