public class BusinessDayCalculation{  
    public static date calculateWorkingDays(Date startDate){       
        Set<Date> holidaysSet = new Set<Date>();  
        for(National_Holidays__mdt currHoliday : [Select Date__c from National_Holidays__mdt]){  
            holidaysSet.add(currHoliday.Date__c);  
        } 
        Date startDateFor4Days = startDate.addDays(4);
        Date enddate = startDate;
        Date finalDate = startDate;
        for(integer i=0; i< 4; i++){ 
            startDate = startDate.addDays(1);
            finalDate = finalDate.addDays(1);
            if((holidaysSet.contains(startDate))){
                finalDate = finalDate.addDays(1);
            }
            if(finalDate > startDateFor4Days && (holidaysSet.contains(finalDate))){
                finalDate = finalDate.addDays(1);
            }
        }
        system.debug('================'+finalDate); 
        return finalDate;  
    }
}