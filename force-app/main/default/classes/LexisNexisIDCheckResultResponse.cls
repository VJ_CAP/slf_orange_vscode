public class LexisNexisIDCheckResultResponse{
    public cls_signers[] signers;
    public cls_agents[] agents;
    public cls_editors[] editors;
    public cls_intermediaries[] intermediaries;
    public cls_carbonCopies[] carbonCopies;
    public cls_certifiedDeliveries[] certifiedDeliveries;
    public cls_inPersonSigners[] inPersonSigners;
    public cls_seals[] seals;
    public String recipientCount;   
    public String currentRoutingOrder; 
    public class cls_signers {
        public String creationReason;   
        public String isBulkRecipient;  
        public String name; 
        public String email;    
        public String recipientId;  
        public String recipientIdGuid;  
        public String requireIdLookup;  
        public String idCheckConfigurationName; 
        public String userId;   
        public String routingOrder;
        public String roleName; 
        public String status;   
        public String signedDateTime;   
        public String deliveredDateTime;    
        public cls_recipientAuthenticationStatus recipientAuthenticationStatus;
    }
    public class cls_recipientAuthenticationStatus {
        public cls_idLookupResult idLookupResult;
        public cls_idQuestionsResult idQuestionsResult;
    }
    public class cls_idLookupResult {
        public String status;   
        public String eventTimestamp;   
    }
    public class cls_idQuestionsResult {
        public String status;   
        public String eventTimestamp;   
    }
    class cls_agents {
    }
    class cls_editors {
    }
    class cls_intermediaries {
    }
    class cls_carbonCopies {
    }
    class cls_certifiedDeliveries {
    }
    class cls_inPersonSigners {
    }
    class cls_seals {
    }
    public static LexisNexisIDCheckResultResponse parse(String json){
        return (LexisNexisIDCheckResultResponse) System.JSON.deserialize(json, LexisNexisIDCheckResultResponse.class);
    }
}