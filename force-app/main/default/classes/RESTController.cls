@RestResource(urlMapping='/GetQuotes/*')
global with sharing class RESTController {
    @httpGet
    global static /*List<Quote>*/ String getQuotes(){
        String opptyId = RestContext.request.params.get('opportunity');
        try{
            Opportunity oppty = [SELECT Id, 
                                 Sighten_UUID__c, 
                                 Install_State_Code__c 
                                 FROM Opportunity 
                                 WHERE Id = :opptyId];
            if(oppty != null){
                getSightenQuotes qGet = new getSightenQuotes(oppty.Sighten_UUID__c, oppty.Id, oppty.Install_State_Code__c);
        		//Database.executeBatch(qGet,1);
                ID jobID = System.enqueueJob(qGet);
                
               //while(){
                    AsyncApexJob jobInfo = [SELECT Status,NumberOfErrors FROM AsyncApexJob WHERE Id=:jobID];
                //}
                
                return jobInfo.Status;

                //return 'Quote Pull Request Submitted';
                //sightenCallout.getSightenQuotes(oppty.Sighten_UUID__c, oppty.Id, oppty.Install_State_Code__c);
                //return [SELECT Id, Name, Quote_Short_Name__c, Term_yrs__c, Monthly_Payment__c, Credit_Approved__c FROM Quote where OpportunityId = :opptyId];
                
            }else{
              //  List<String> retString = new List<String>{'No Opportunity Found'};
             //   return retString;
                System.debug('***Debug: invalid id: ' + opptyId);
                
            }
        }
        catch(Exception e){
            return null;
        }
        
       return null; 
        
    }

}