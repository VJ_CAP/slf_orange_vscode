/**
* Description: SP-152 Status API: Schedule class to execute StatusAuditBatch class to do bulk Rest call's to boomi from Status_Audit__c object..
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/28/2017          Created
******************************************************************************************/

global class StatusAuditBatchSh implements Schedulable{
    global void execute(SchedulableContext sc){
        Batch_Jobs__c cnf = Batch_Jobs__c.getValues('Status Audit');
        Datetime prevRunDt = null;
        Datetime currRunDt = null;
        if(cnf!=null && cnf.Active__c){
            prevRunDt = (null==cnf.Previous_run_time__c? (Datetime.now()).addDays(-1) : cnf.Previous_run_time__c);
            currRunDt = (null==cnf.Current_run_time__c? Datetime.now() : cnf.Current_run_time__c);
            cnf.Previous_run_time__c = Datetime.now();
            cnf.Batch_Job_Id__c = sc.getTriggerId();
            upsert cnf;
            
            Database.executebatch(new StatusAuditBatch(prevRunDt, currRunDt),25);
        }
    }
}