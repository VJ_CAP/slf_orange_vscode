public class GraphController {
   
    @auraEnabled
    public static Map<string, integer> getChartMap(){
        List<Opportunity> lstOpp = new List<Opportunity>();
        Map<string, integer> mp = new Map<string, integer>();        
        mp.put('Jan 2020', 45);
        mp.put('Jan 2020', 33);
        mp.put('Feb 2020', 50);
        mp.put('Mar 2020', 35);      
        mp.put('Apr 2020', 50);
        mp.put('May 2020', 50);
        mp.put('June 2020', 50);
        mp.put('July 2020', 50);
        return mp;
    }
}