/**
*   Description: This Batch class is used to send email to applicant and Co-Applicannt.
*
*   Modification Log :
---------------------------------------------------------------------------
   Developer                          Date                Description
---------------------------------------------------------------------------
   Bindu Sri                        12/27/2019              Created
******************************************************************************************/
global class PaymentAwarenenessDueDateBatch implements Database.Batchable<sObject>, Schedulable{
    /**
    * Description:    Start method to query all the underwriting records
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        String longtermfacility = 'CRB';
        return Database.getQueryLocator('SELECT id,Opportunity__r.Account.PersonEmail,Opportunity__r.Primary_Applicant_Email__c,M1_Loan_Booked_Date__c,Opportunity__r.SLF_Product__r.Product_Loan_Type__c,Opportunity__r.SLF_Product__r.Long_Term_Facility__c,Opportunity__r.ACH_APR_Process_Required__c,Opportunity__r.Long_Term_Facility__c,Opportunity__r.Product_Loan_Type__c FROM Underwriting_File__c where Opportunity__r.SLF_Product__r.Long_Term_Facility__c =  \''+String.escapeSingleQuotes(longtermfacility)+'\' ' +' AND M1_Loan_Booked_Date__c = LAST_N_DAYS:50');
    }
    /**
    * Description:    Batch execute method to send email to person account email based on ACH APR Required Validation
    */
    global void execute(Database.BatchableContext bc, List<Underwriting_File__c> undList){
        System.debug('****Underwriting List'+undList);
        List<Messaging.SingleEmailMessage> lstEmails= new List<Messaging.SingleEmailMessage>();
        List<EmailTemplate> emailTemplateList = [SELECT Id,TemplateType,Name,HtmlValue,Subject,body,DeveloperName FROM EmailTemplate WHERE DeveloperName = 'Payment_Awareness_ACH_APR' OR DeveloperName ='Payment_Awareness_STANDARD'];
        system.debug('****emailTemplateList*****'+emailTemplateList );
        try{
            if(!undList.isEmpty()){     
            for(Underwriting_File__c unw:undList)
            {
                if(unw.Opportunity__r.ACH_APR_Process_Required__c)
                {
                    Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemplateList[0].id,null,unw.id);
                    List<string> toAddress = new List<string>();
                    toAddress.add(unw.Opportunity__r.Account.PersonEmail);
                    //toAddress.add('bindu-sri.gurram@capgemini.com'); 
                    if(!toAddress.isEmpty())
                        mail.setToAddresses(toAddress);
                        mail.setSubject(emailTemplateList[0].Subject);
                        if(emailTemplateList[0].DeveloperName == 'Payment_Awareness_ACH_APR'){
                            mail.setTemplateId(emailTemplateList[0].Id);
                        }
                        lstEmails.add(mail);
                }
                else
                {
                    Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(emailTemplateList[1].id,null,unw.id);
                    List<string> toAddress = new List<string>();
                    toAddress.add(unw.Opportunity__r.Account.PersonEmail);
                    if(!toAddress.isEmpty())
                        mail.setToAddresses(toAddress);
                        mail.setSubject(emailTemplateList[1].Subject);
                        if(emailTemplateList[1].DeveloperName == 'Payment_Awareness_STANDARD'){
                            mail.setTemplateId(emailTemplateList[1].Id);
                        }
                        lstEmails.add(mail);
                }
            }
        }
        if(!lstEmails.isEmpty()){
            Messaging.sendEmail(lstEmails,false); 
        }
    }catch(Exception err){
            ErrorLogUtility.writeLog('paymentAwarenenessDueDateBatch.execute()',err.getLineNumber(),'execute()',err.getMessage() + '***' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));       
        }
        
    }
    global void Finish(Database.BatchableContext bc){
    }  
    /**
    * Description: Schedule execute method to schedule the batch class
    */       
    global void execute(SchedulableContext sc) {
        PaymentAwarenenessDueDateBatch paymentBatch = new PaymentAwarenenessDueDateBatch();
        database.executebatch(paymentBatch);
    } 
}