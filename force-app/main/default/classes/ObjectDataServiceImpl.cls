/**
* Description: Object service related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/

public with sharing class ObjectDataServiceImpl extends RestDataServiceBase{
   
    /**
    * Description: Process any SObject related request and perform CRED Operations(i.e. insert/update/delete) and convert 
    *              response from salesforce sobject to wrapper (JSON)
    *
    * @param stsDtObj   Transfers request in IDataObject and converts request in to bean.
    *
    */
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+ObjectDataServiceImpl.class);
        IRestResponse iRestRes;
        ObjectRequestWrapper reqData = (ObjectRequestWrapper)stsDtObj;
        ObjectBean objBen = new ObjectBean();
        UnifiedBean unifBean = new UnifiedBean(); 
        system.debug('### input String: '+ reqData);
        try{
            List<Sobject> objLst = new List<SObject>();
            system.debug('reqData - '+reqData);
            
            reqData.sobjectLst = reqData.sobjectLst.removeStart('[');
            reqData.sobjectLst = reqData.sobjectLst.removeEnd(']');
            string[] partStr = reqData.sobjectLst.split('},');
            Set<String> setOfCreditIds = new Set<String>();
            for(Integer i=0;i< partStr.size();i++)
            {
                string str = '';
           
                if(partStr.size() > 1)      
                    str = partStr[i] + '}';
                else 
                    str = partStr[i];
                Sobject sObj = (Sobject)JSON.deserialize(str,Type.forName(reqData.objectType));
               
                objLst.add(sObj);
                if(reqData.serviceName == 'credit')
                    setOfCreditIds.add(sObj.id);            
            }
            system.debug('### objLst:1 '+ objLst);
            system.debug('### setOfCreditIds '+ setOfCreditIds);
            //Added As part of ORANGE-1930
            Map<Id,SLF_Credit__c> CreditObjInfoMap;
            if(setOfCreditIds != null && setOfCreditIds.size()>0){
                CreditObjInfoMap = new Map<ID,SLF_Credit__c>([Select id,Opportunity__c,Opportunity__r.Project_Category__c,Opportunity__r.Installer_Account__c,Opportunity__r.Installer_Account__r.M0_Split__c,Opportunity__r.Installer_Account__r.Permit_Split__c,Opportunity__r.Installer_Account__r.Kitting_Split__c,Opportunity__r.Installer_Account__r.M1_Split__c,Opportunity__r.Installer_Account__r.Inspection_Split__c,Opportunity__r.Installer_Account__r.M2_Split__c,Opportunity__r.Installer_Account__r.M1_Approval_Days__c, Opportunity__r.Installer_Account__r.M2_Approval_Days__c from SLF_Credit__c where id IN:setOfCreditIds]);
            }
            
            if(reqData.serviceName == 'credit')
            {
            
            
                 for(Sobject sObjiter : objLst){
                    SLF_Credit__c credRec = (SLF_Credit__c) sObjiter;
                    DateTime myDateTime = System.now();
                    String myDateform = myDateTime.format('yyyy-MM-dd HH:mm:ss', 'CST');
                    Date myDate = Date.valueOf(myDateform);
                    credRec.Credit_Submission_Date__c = myDate;
                     credRec.Credit_Submission_Date_Time__c = myDateTime; //Added by Suresh as per 2751
                    //Added As part of ORANGE-1930 R3.2
                    if(CreditObjInfoMap !=null && CreditObjInfoMap.size()>0){
                        system.debug('@@@Credit: '+CreditObjInfoMap.get(credRec.id)+'@@@@'+CreditObjInfoMap.get(credRec.id).Opportunity__r.Project_Category__c);
                        if(CreditObjInfoMap.get(credRec.id)!= null && CreditObjInfoMap.get(credRec.id).Opportunity__r.Project_Category__c =='Solar'){
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M0_Split__c != null)
                                credRec.M0_Split__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M0_Split__c;
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.Permit_Split__c!= null)
                                credRec.Permit_Split__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.Permit_Split__c;
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.Kitting_Split__c!= null)
                                credRec.Kitting_Split__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.Kitting_Split__c;
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M1_Split__c!= null)
                                credRec.M1_Split__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M1_Split__c;
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.Inspection_Split__c!= null)
                                credRec.Inspection_Split__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.Inspection_Split__c;
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M2_Split__c!= null)
                                credRec.M2_Split__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M2_Split__c;
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M1_Approval_Days__c!= null)
                                credRec.M1_Approval_Days__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M1_Approval_Days__c;
                            If(CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M2_Approval_Days__c!= null)
                                credRec.M2_Approval_Days__c = CreditObjInfoMap.get(credRec.id).Opportunity__r.Installer_Account__r.M2_Approval_Days__c;
                        }
                    }
                    
                    system.debug('### myDate '+ myDate);
                }
            }
            system.debug('### objLst:2 '+ objLst);
            if(!objLst.IsEmpty())  
            {
                if(reqData.action =='insert'){
                   insert objLst;
                }else if(reqData.action =='update'){
                    update objLst;
                }else if(reqData.action =='delete'){
                    delete objLst;
                }
            }
            
            
            if(reqData.serviceName == 'credit')
            {
                //Update multiple credit's and send responce in unified JSON format.
                Id opportunityId ;
                               
                
                for(Sobject sObjIterate : objLst){
                    SLF_Credit__c slfRec = (SLF_Credit__c) sObjIterate; 
                    opportunityId = slfRec.Opportunity__c;
                    system.debug('### opportunityId in For'+opportunityId);
                    if(null != opportunityId)
                        break;
                }
                system.debug('### opportunityId'+opportunityId);
                if(null != opportunityId)
                {
                    List<Quote> quoteList = [Select id,ToBeDesynced__c,OpportunityId,Opportunity.ownerId,Opportunity.owner.name,Opportunity.owner.email  from Quote where OpportunityId =:opportunityId and ToBeDesynced__c=true];
                    List<Opportunity> oppLst = new List<Opportunity>();
                    List<Quote> quoteUpdateLst = new List<Quote>();
                    if(!quoteList.isEmpty()){
                        for(Quote quotes :quoteList)
                        {
                            Quote quoteRec = new Quote(id=quotes.id);
                            quoteRec.ToBeDesynced__c = false;
                            quoteUpdateLst.add(quoteRec);
                            system.debug('--------opp owner details----'+quotes.opportunity.ownerId+ quotes.opportunity.owner.name +quotes.opportunity.owner.email);
                            
                            Opportunity opp = new Opportunity();
                            opp.id = quotes.OpportunityId;
                            opp.ach__c = false;
                            opp.Final_Monthly_Payment__c  = null;
                            opp.initial_payment__c = null;
                            opp.install_cost_cash__c = null;
                            opp.Monthly_Payment_Escalated_Single_Loan__c = null;
                            opp.monthly_payment__c = null;
                            opp.sighten_product_uuid__c = null;
                            opp.SyncedQuoteId = null;
                            oppLst.add(opp);
                            
                        }
                        if(!quoteUpdateLst.isEmpty()) {
                            update quoteUpdateLst;
                        }
                        if(!oppLst.isEmpty()) {
                            update oppLst;
                        }
                    }
                    
                    unifBean = SLFUtility.getUnifiedBean(new set<Id>{opportunityId},'Orange',false);
                    system.debug('### unifBean'+unifBean);
                    unifBean.returnCode = '200';
                    iRestRes = (IRestResponse)unifBean;
                }else{
                    //Updated by Adithya as part of 4380
                    unifBean.returnCode = '200';
                    iRestRes = (IRestResponse)unifBean;
                }               
                
            }else{
                objBen.result = objLst;
                iRestRes = (IRestResponse)objBen;
            }
        }catch(Exception ex){
            String errMsg = ErrorLogUtility.getErrorCodes('400',null);
            list<UnifiedBean.ErrorWrapper> lstErrors = new list<UnifiedBean.ErrorWrapper>();
            UnifiedBean.ErrorWrapper errorWrap = new UnifiedBean.ErrorWrapper();
            errorWrap.errorMessage = errMsg;
            lstErrors.add(errorWrap);
            unifBean.error = lstErrors;

            unifBean.returnCode = '400';
            iRestRes = (IRestResponse)unifBean;

            system.debug('### ObjectDataServiceImpl.transformOutput():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());
            ErrorLogUtility.writeLog('ObjectDataServiceImpl.transformOutput()',ex.getLineNumber(),'transformOutput()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null)); 
        }  
        system.debug('### Exit from transformOutput() of '+ObjectDataServiceImpl.class);
        system.debug('### iRestRes'+iRestRes);
        return iRestRes;
    }
    
    /**
    * Description: Process error messages to JSON
    * @param errMsg   error message
    */
    public IRestResponse errorMessage(string errMsg){
        ErrorMessageWrapper stserrMsg = new ErrorMessageWrapper();
        stsErrMsg.error= errMsg;
        return stsErrMsg; 
    }
 
}