public class jsonSystemResponse {

	public class Messages {
		public List<Info> info;
		public List<Critical> critical;
		public List<Critical> error;
		public List<Critical> warning;
	}

	public class Owned_by_organization {
		public String link;
		public String uuid;
		public String natural_id;
	}

	public Integer status_code;
	public Integer count;
	public Data data;
	public String db_table;
	public Messages messages;

	public class Utility_tier_offset {
		public Double one;
		public Double two;
		public Integer three;
	}

	public class Utility_tier_usage {
		public Double one;
		public Double two;
	}

	public class Monthly_generation {
		public Integer one;
		public Integer two;
		public Integer three;
		public Integer four;
		public Integer five;
		public Integer six;
		public Integer seven;
		public Integer eight;
		public Integer nine;
		public Integer one0;
		public Integer oneone;
		public Integer onetwo;
	}

	public class Data {
		public String date_updated;
		public Double consumption_offset;
		public Double capacity;
		public String primary_module_mfr;
		public Owned_by_organization owned_by_organization;
		public Owned_by_organization modified_by;
		public String uuid;
		public String primary_inverter_mfr;
		public Integer n_modules;
		public Owned_by_organization site;
		public Integer generation;
		public Owned_by_organization created_by;
		public String date_created;
		public Double productivity;
		public String name;
		public Monthly_generation monthly_generation;
		public Owned_by_organization owned_by_user;
		public Arrays arrays;
		public Utility_tier_offset utility_tier_offset;
		public String natural_id;
		public Utility_tier_usage utility_tier_usage;
	}

	public class Critical {
	}

	public class Arrays {
		public String link;
	}

	public class Info {
		public String msg_code;
		public String timestamp;
		public String message;
	}

	
	public static jsonSystemResponse parse(String json) {
		return (jsonSystemResponse) System.JSON.deserialize(json, jsonSystemResponse.class);
	}
}