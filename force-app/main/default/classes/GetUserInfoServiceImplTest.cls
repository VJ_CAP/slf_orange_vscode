@isTest
public class GetUserInfoServiceImplTest {
    @testsetup private static void testInsertAccount(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgAccflagDis = new TriggerFlags__c();
        trgAccflagDis.Name ='Disclosures__c';
        trgAccflagDis.isActive__c =true;
        trgLst.add(trgAccflagDis);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        
        //For CreateApplicantDataServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Installer_Legal_Name__c='Bright planet Solar';
        acc.Solar_Enabled__c = true;
        acc.Credit_Run__c = true;
        acc.Default_FNI_Loan_Amount__c = 25000;
        acc.Risk_Based_Pricing__c = true;
        acc.Credit_Waterfall__c = true;
        acc.Eligible_for_Last_Four_SSN__c = true;
        acc.Phone = '9874563210';
        acc.Push_Endpoint__c = 'www.google.com';
        acc.Subscribe_to_Push_Updates__c = true;
        insert acc;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright planet Solar';
        personAcc.Solar_Enabled__c = true;
        //personAcc.OwnerId = portalUser.Id;
        insert personAcc;
        
        Contact con = new Contact(FirstName = 'Sunlight',LastName ='Financial',AccountId = acc.Id);
        insert con;  
        
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc', Email='testc@mail.com', IsActive = true,Hash_Id__c='123',
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass@mail.com',ContactId = con.Id);
        insert portalUser;
        
        //Insert SLF Product Record
        List<Product__c> prodList = new List<Product__c>();
        Product__c prod = new Product__c();
        prod.Installer_Account__c =acc.id;
        prod.Long_Term_Facility_Lookup__c =acc.id;
        prod.Name='testprod';
        prod.FNI_Min_Response_Code__c=9;
        prod.Product_Display_Name__c='test';
        prod.Qualification_Message__c ='testmsg';
        prod.Product_Loan_Type__c='Solar';
        prod.State_Code__c ='CA';
        prod.ST_APR__c = 2.99;
        prod.Long_Term_Facility__c = 'TCU';
        prod.APR__c = 2.99;
        prod.ACH__c = true;
        prod.Internal_Use_Only__c = false;
        prod.Product_Tier__c ='1';
        //prod.Long_Term_Facility__c='TCU';
        prod.LT_Max_Loan_Amount__c =1000;
        prod.Term_mo__c = 120;
        // prod.Is_Active__c= true;
        prod.Product_Tier__c = '0';
        //insert prod;
        prodList.add(prod);
        
        insert prodList;
        
        //Insert opportunity record
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity personOpp = new Opportunity();
        personOpp.name = 'personOpp';
        personOpp.CloseDate = system.today();
        personOpp.StageName = 'Qualified';
        personOpp.AccountId = acc.id;
        personOpp.Installer_Account__c = acc.id;
        personOpp.Install_State_Code__c = 'CA';
        personOpp.Install_Street__c ='Street';
        personOpp.Install_City__c ='InCity';
        personOpp.Combined_Loan_Amount__c =10000;
        personOpp.Partner_Foreign_Key__c = 'TCU||abc123';
        //opp.Approved_LT_Facility__c ='TCU';
        personOpp.SLF_Product__c = prod.id;            
        //insert personOpp;
        oppList.add(personOpp);
        
        //Insert opportunity record
        Opportunity opp = new Opportunity();
        opp.name = 'OpName';
        opp.CloseDate = system.today();
        opp.StageName = 'Qualified';
        opp.AccountId = personAcc.id;
        opp.Installer_Account__c = acc.id;
        opp.Install_State_Code__c = 'CA';
        opp.Install_Street__c ='Street';
        opp.Install_City__c ='InCity';
        opp.Combined_Loan_Amount__c = 10000;
        opp.Partner_Foreign_Key__c = 'TCU||123';
        //opp.Approved_LT_Facility__c = 'TCU';
        opp.SLF_Product__c = prod.id;
        opp.Opportunity__c = personOpp.Id; 
        opp.Block_Draws__c = false;
        opp.Has_Open_Stip__c = 'False';
        opp.All_HI_Required_Documents__c=true;
        opp.Max_Draw_Count__c=3;
        opp.ProductTier__c = '0';         
        opp.Type_of_Residence__c  = 'Own';
        opp.language__c = 'Spanish';
        opp.Project_Category__c = 'Solar';
        //insert opp;
        oppList.add(opp);
        
        insert oppList;
        
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = opp.id;
        undStpbt.Approved_for_Payments__c = true;
        insert undStpbt;
        
        Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'VOI', Email_Text__c = 'Test data', POS_Message__c = 'test', Review_Classification__c = 'SLS II');
        insert sd;
        
        Stipulation__c stp = new Stipulation__c();
        stp.Status__c ='New';
        stp.Stipulation_Data__c = sd.id;
        stp.Underwriting__c =undStpbt.id;
        insert stp;
        
        //Insert Quote Record
        List<Quote> quoteList = new List<Quote>();
        Quote quoteRec = new Quote();
        quoteRec.Name ='Test quote';
        quoteRec.Opportunityid = opp.Id;
        quoteRec.SLF_Product__c = prod.Id ;
        //quoteRec.System_Design__c= sysDesignRec.id;
        //quoteRec.Accountid =acc.id;
        //insert quoteRec; 
        quoteList.add(quoteRec); 
        
        insert quoteList;
        
        //Insert Credit Record
        List<SLF_Credit__c> creditList = new List<SLF_Credit__c>();
        SLF_Credit__c credit = new SLF_Credit__c();
        credit.Opportunity__c = opp.id;
        //credit.Primary_Applicant__c = personAcc.id;
        //credit.Co_Applicant__c = acc.id;
        credit.SLF_Product__c = prod.Id ;
        credit.Total_Loan_Amount__c = 100;
        credit.Status__c = 'New';
        credit.LT_FNI_Reference_Number__c = '123';
        credit.Application_ID__c = '123';
        //insert credit;
        creditList.add(credit);
        insert creditList;
        
    }
    
    private testMethod static void GetUserInfoDataServiceTest(){
        
        List<user> loggedInUsr = [select Id,username,Email,Hash_Id__c,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc@mail.com' limit 1];
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Hash_Id__c,Name,Partner_Foreign_Key__c,SLF_Product__c from opportunity where AccountId =: acc.Id LIMIT 1]; 
        Product__c prod = [select Id,Name from Product__c where Name =: 'testprod' LIMIT 1];
        //  SLF_Credit__c cred = [SELECT Id, Opportunity__c from SLF_Credit__c where Opportunity__c =: opp.id LIMIT 1]; 
        
        System.runAs(loggedInUsr[0])
        { 
            Test.startTest();
            string getUserInfoDataJsonReq;
            Map<String, String> getUserInfoMap = new Map<String, String>(); 
            
            getUserInfoDataJsonReq = '{ "projects": [ { "id":"'+opp.Id+'" } ], "users": [{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"1234"} ] }';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');            
            
            getUserInfoDataJsonReq = '{ "projects": [ { "id":"","externalId":"'+opp.id+'","hashId": ""  } ], "users": [{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"1234"} ] }';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');            
            
            getUserInfoDataJsonReq = '{ "projects": [ { "id":"'+opp.Id+'","externalId":"'+opp.id+'","hashId": "'+opp.Hash_Id__c+'" } ], "users": [{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"1234"} ] }';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');            
            
            getUserInfoDataJsonReq = '{ "projects": [ { "id":"","externalId":"","hashId": "'+opp.Hash_Id__c+'" } ], "users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"1234"}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');           
            
            getUserInfoDataJsonReq = '{ "projects": [ { "id":"","externalId":"","hashId": "" } ], "users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"1234"}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');           
            
            getUserInfoDataJsonReq = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":""}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo'); 
            
            getUserInfoDataJsonReq = '{"users":[{"id":"1wwwww","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":""}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');
            
            getUserInfoDataJsonReq = '{"users":[{"id":"'+loggedInUsr[0].id+'","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"1234"}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo'); 
            
            getUserInfoDataJsonReq = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"'+loggedInUsr[0].Hash_Id__c+'"}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo'); 
            
            getUserInfoDataJsonReq = '{"users":[{"id":"","firstName":"Test","lastName":"User","email":"testc@mail.com","phone":"9263693291","isActive":true,"title":"","role":"","reportsTo":"","HashId":"2535"}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');
            
            getUserInfoDataJsonReq = '{"users":[{"firstName":"","lastName":"","email":"","phone":"","isActive":false,"title":"executive","role":"Top Role","reportsTo":""}]}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');
            
            getUserInfoDataJsonReq = '{"users":[{"id":"","firstName":"","lastName":"","email":"","phone":"","isActive":true,"title":"","role":"","reportsTo":""},"werwer":,"","ggf":],"data":"wette"}';
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');
            
            getUserInfoDataJsonReq = null;
            MapWebServiceURI(getUserInfoMap,getUserInfoDataJsonReq,'getuserinfo');
            
            // To cover exception block
            GetUserInfoServiceImpl  getUserInfoTest = new GetUserInfoServiceImpl();
            getUserInfoTest.fetchData(null);            
            Test.stopTest();            
        }
        
    }
    /**
* Description: used for Map Webservice URI for any Object
*/
    Public Static void MapWebServiceURI(Map<String, String> pricingMap,string jsonString,string uriPeram){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            for(String str : pricingMap.Keyset()){
                gen.writeStringField(str,pricingMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
        //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
    
}