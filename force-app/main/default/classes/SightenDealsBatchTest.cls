@isTest
public class SightenDealsBatchTest {
    @testsetup static void createtestdata(){
        SLFUtilityDataSetup.initData();        
    }
    private testMethod static void testStipulationStatusBatch(){       
        Set<id> oppIdList = new Set<id>();        
        List<Opportunity> oppList = [select Id,Name from opportunity];
        for(Opportunity oppObj : oppList){
            oppObj.Hash_Id__c = null;
            oppIdList.add(oppObj.id);
        }   
        update oppList;
        Test.startTest();
            SightenDealsBatch st1 = new SightenDealsBatch(oppIdList);
            DataBase.executeBatch(st1);
        Test.stopTest();
    }
}