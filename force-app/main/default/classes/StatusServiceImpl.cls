/**
* Description: Status API related integration logic.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           05/30/2017          Created
******************************************************************************************/
public with sharing class StatusServiceImpl extends RESTServiceBase{  
    private String msg = '';
    
    /**
    * Description: fetchData method is uesd to deserialize request in to wrapper object to process business related logic.
    *          
    * @param rw    Get all POST request data.
    *
    * return res   Send response back in bean(JSON) format.
    */
    public override IRestResponse fetchData(RequestWrapper rw){ 
        system.debug('### Entered into fetchData() of ' + StatusServiceImpl.class);
        
        ResponseWrapper res = new ResponseWrapper();
        UnifiedBean stsRespBean = new UnifiedBean();
        StatusDataServiceImpl statusDataSrvImpl = new StatusDataServiceImpl(); 
        List<IRestResponse> iabList = new List<IRestResponse>();
        List<Opportunity> oppResults = new List<Opportunity>();
        
        StatusDataObject stsDtObj = new StatusDataObject();
        Map<string,string> reqIdMap = new Map<string,string>();
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
        try{
            boolean isMaxRecExceded = false;
            System.debug('### Request: '+rw.reqDataStr);
            UnifiedBean reqData = (UnifiedBean)JSON.deserialize(rw.reqDataStr, UnifiedBean.class);
            boolean hasValue = false;
            String conditions = ' Account.IsPersonAccount = true AND '; 
            conditions +='(';
            if((String.isNotBlank(reqData.projectIds) && String.isNotBlank(reqData.externalIds) && String.isNotBlank(reqData.hashIds))
                ||(String.isNotBlank(reqData.projectIds) && (String.isNotBlank(reqData.externalIds) || String.isNotBlank(reqData.hashIds)))
                ||(String.isNotBlank(reqData.externalIds) && (String.isNotBlank(reqData.projectIds) || String.isNotBlank(reqData.hashIds)))
                ||(String.isNotBlank(reqData.hashIds) && (String.isNotBlank(reqData.projectIds) || String.isNotBlank(reqData.externalIds))))
            {
                hasValue = false;
                msg = ErrorLogUtility.getErrorCodes('208',null);
                stsRespBean.returnCode = '208';
            }else if(String.isNotBlank(reqData.projectIds))
            {
                string[] accIdSplit = reqData.projectIds.split(',');
                set<string> idset = new  set<string>();
                for(string str : accIdSplit)
                {
                    if(str.length()>15)
                        str = str.subString(0,15);
                        
                    reqIdMap.put(str.trim(),str.trim());
                    idset.add(str.trim());
                }
                if(idset.size() > 25)
                {
                    isMaxRecExceded = true;
                }else{
                    conditions += ' OR Id IN ' + ':idset'  ;
                    hasValue = true;
                }
            }else if(String.isNotBlank(reqData.externalIds))
            {
                String externalId;
                List<user> loggedInUsr = [select Id,contact.AccountId,contact.Account.FNI_Domain_Code__c from User where Id =: userinfo.getUserId()];
                string[] extIdSplit = reqData.externalIds.split(',');
                set<string> extidset = new  set<string>();
                for(string str : extIdSplit)
                {
                    if(!loggedInUsr.isEmpty())
                    {
                        if(loggedInUsr[0].contact.Account.FNI_Domain_Code__c != null){
                            externalId = loggedInUsr[0].contact.Account.FNI_Domain_Code__c+'||'+str;
                        }else{
                            stsRespBean.returnCode = '400';
                            msg = ErrorLogUtility.getErrorCodes('400',null);
                        }
                        
                    }
                    reqIdMap.put(externalId.trim(),externalId.trim());
                    extidset.add(externalId.trim());
                }
                if(extidset.size() > 25)
                {
                    isMaxRecExceded = true;
                }else{
                    conditions += ' OR Partner_Foreign_Key__c IN ' + ':extidset'   ;
                    hasValue = true;
                }
                
                
            }else if(String.isNotBlank(reqData.hashIds))
            {
                string[] hashIdSplit = reqData.hashIds.split(',');
                set<string> hashidset = new  set<string>();
                for(string str : hashIdSplit)
                {
                    reqIdMap.put(str.trim(),str.trim());
                    hashidset.add(str.trim());
                }
                system.debug('### hashidset '+hashidset );
                if(hashidset.size() > 25)
                {
                    isMaxRecExceded = true;
                }else{
                    conditions += ' OR Hash_Id__c IN ' + ':hashidset'   ;
                    hasValue = true;
                }
                
            }
            
            if(isMaxRecExceded)
            {
                msg = ErrorLogUtility.getErrorCodes('393',null);
                stsRespBean.returnCode = '393';
            }
            
            stsDtObj.reqIdMap = reqIdMap; 
            conditions+=')';
            if(hasValue && isMaxRecExceded == false)
            {
                Map<string,Opportunity> OppMap = new Map<string,Opportunity>();
                Map<string,List<Underwriting_File__c>> UnderwritingMap = new Map<string,List<Underwriting_File__c>>();
                Map<Id,List<dsfs__DocuSign_Status__c>> DocuSignMap = new Map<Id,List<dsfs__DocuSign_Status__c>>();
                String queryStr = 'select id,Name,Long_Term_APR__c,Credit_Decision__c,ACH_APR__c,NonACH_APR__c,Partner_Foreign_Key__c,Hash_Id__c,isACH__c,ACH_APR_Process_Required__c,SLF_Product__r.APR__c,SLF_Product__r.LT_OID__c,SLF_Product__r.Promo_APR__c,SLF_Product__r.LT_Facility_Compensation_Rate__c,SLF_Product__r.NonACH_APR__c,SLF_Product__r.NonACH_Facility_Fee__c,SLF_Product__r.NonACH_OID__c,SLF_Product__r.NonACH_Promo_APR__c, SLF_Product__r.ACH__c from Opportunity where '+conditions;
              
                queryStr= queryStr.replaceFirst(' OR ', ' ');
                system.debug('### queryStr '+queryStr );
                oppResults = Database.query(queryStr);
                system.debug('### oppResults '+oppResults );
                if(!oppResults.isEmpty())
                {
                    for(Opportunity oppIterate : oppResults)
                    {
                        OppMap.put(string.ValueOf(oppIterate.Id).subString(0,15),oppIterate);
                        OppMap.put(oppIterate.Partner_Foreign_Key__c,oppIterate);
                        OppMap.put(oppIterate.Hash_Id__c,oppIterate);
                        
                        //Added by Deepika as part of Orange - 6761 & 6785 - Start
                        //if(oppIterate.Long_Term_APR__c == 0 || oppIterate.Long_Term_APR__c == null){  Commented by Adithya as part of 6813                          
                        if((oppIterate.ACH_APR__c == 0.00 || null == oppIterate.ACH_APR__c) && (oppIterate.NonACH_APR__c == 0.00 || null == oppIterate.NonACH_APR__c)){
                            oppIterate.isACH__c = oppIterate.SLF_Product__r.ACH__c;
                            oppIterate.ACH_APR__c = oppIterate.SLF_Product__r.APR__c;
                            oppIterate.ACH_OID__c = oppIterate.SLF_Product__r.LT_OID__c;
                            oppIterate.ACH_Promo_APR__c = oppIterate.SLF_Product__r.Promo_APR__c;
                            oppIterate.ACH_Facility_Fee__c = oppIterate.SLF_Product__r.LT_Facility_Compensation_Rate__c;
                            oppIterate.NonACH_APR__c = oppIterate.SLF_Product__r.NonACH_APR__c;
                            oppIterate.NonACH_Facility_Fee__c = oppIterate.SLF_Product__r.NonACH_Facility_Fee__c;
                            oppIterate.NonACH_OID__c = oppIterate.SLF_Product__r.NonACH_OID__c;
                            oppIterate.NonACH_Promo_APR__c = oppIterate.SLF_Product__r.NonACH_Promo_APR__c;                         
                        }
                        //Added by Deepika as part of Orange - 6761 & 6785  - End
                    }
                    update oppResults;
                    // Updated Query as part of 1930 by Venkat
                    List<Underwriting_File__c> UnderwritingLst = [select Id,Name,Project_Status__c ,Opportunity__c,Funding_status__c
                                            ,LT_Loan_Agreement_Review_Complete__c, ST_Loan_Agreement_Review_Complete__c, 
                                            Final_Design_Review_Complete__c,Project_Status_Detail__c,Confirm_ACH_Information__c, Installation_Photos_Review_Complete__c, 
                                            Confirmation_Call_Initiated__c, Welcome_Call_Initiated__c, PTO_Review_Complete__c, Install_Contract_Review_Complete__c, 
                                            Primary_Borrower_ID_Type__c, Utility_Bill_Review_Complete__c,(select id,Name,Status__c, Description__c,Stipulation_Data_Name__c, 
                                            Completed_Date__c, ETC_Notes__c, Stipulation_Data__r.Name,Stipulation_Data__r.Installer_Only_Email__c from Stipulations__r) from Underwriting_File__c where 
                                            Opportunity__c IN: OppMap.keyset() ORDER BY CreatedDate ASC ];
                                            
                    if(!UnderwritingLst.isEmpty())   
                    {                     
                        for(Underwriting_File__c UnderwritingIterate : UnderwritingLst){
                            
                            if(UnderwritingMap.containsKey(UnderwritingIterate.Opportunity__c))
                            {
                                UnderwritingMap.get(UnderwritingIterate.Opportunity__c).add(UnderwritingIterate);
                            }else{
                                UnderwritingMap.put(UnderwritingIterate.Opportunity__c,new List<Underwriting_File__c>());
                                UnderwritingMap.get(UnderwritingIterate.Opportunity__c).add(UnderwritingIterate);
                            }                           
                        }
                    }                   
                    stsDtObj.oppMap = OppMap; 
                    stsDtObj.UnderwritingMap = UnderwritingMap;
                    //stsDtObj.creditMap = creditMap;
                    //stsDtObj.DocuSignMap = stsDtObj;
                    res.response = statusDataSrvImpl.transformOutput(stsDtObj);
                }else{
                    msg = ErrorLogUtility.getErrorCodes('204',null);
                    stsRespBean.returnCode = '204';
                }       
            }
        }catch(Exception err){
            msg = ErrorLogUtility.getErrorCodes('204',null);
            stsRespBean.returnCode = '204';
            
            system.debug('### StatusServiceImpl.fetchData():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog(' StatusServiceImpl.fetchData()',err.getLineNumber(),'fetchData()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));  
        }
        //Error message and status code
        if(String.isNotBlank(msg)){
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,msg);
            stsRespBean.error = new List<UnifiedBean.errorWrapper>();
            stsRespBean.error.add(errorMsg);
            res.response = (IRestResponse)stsRespBean;
        }
        system.debug('### Exit from fetchData() of ' + StatusServiceImpl.class);
        return res.response ;
    }
    
    
}