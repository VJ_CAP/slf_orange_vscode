global class StipulationStatusBatch implements Database.Batchable<sObject> {
    global Set<Id> uwIdSet = new Set<Id>();
    global String query;
    global set<string> stipDataSet = new set<string>();
    
    global StipulationStatusBatch(Set<id> idSet){ 
        stipDataSet.add('ACH');
        stipDataSet.add('APR');
        stipDataSet.add('INV');
    
        query = 'select Id,Underwriting__c,Underwriting__r.Opportunity__c,Underwriting__r.Opportunity__r.Has_Open_Stip__c from Stipulation__c where Stipulation_Data__r.name IN: stipDataSet AND Underwriting__r.Opportunity__c != null AND Status__c!= \'Completed\'';
         
        if(idSet != null && !idSet.isEmpty()){
            uwIdSet.addAll(idSet);
            query += ' and id in :uwIdSet';
        }  
        System.debug('uwIdSet:::'+uwIdSet); 
        System.debug('query:::'+query);         
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        Set<Id> uwIds = new Set<Id>();
        List<Stipulation__c> lstStips =  database.query(query);
        for(Stipulation__c objS : lstStips){
            uwIds.add(objS.Underwriting__c);
        }
        String uwQuery = 'select Id,Opportunity__r.Has_Open_Stip__c,Opportunity__c,(select Id,Stipulation_Data__r.name,Status__c,Stipulation_Data__r.Suppress_In_Portal__c from Stipulations__r) from Underwriting_File__c where  id IN: uwIds';        
      return Database.getQueryLocator(uwQuery);
    }

   global void execute(Database.BatchableContext BC, List<Underwriting_File__c> uwList){    
        system.debug('### Entered into execute() of '+ StipulationStatusBatch.class);  
        Map<Id,Opportunity> oppUpdateMap = new Map<Id,Opportunity>();
        for(Underwriting_File__c UWIterate : uwList){
            integer OpenStipCount = 0;
            Opportunity objOpp = new Opportunity();
            if(UWIterate.Stipulations__r != null && !UWIterate.Stipulations__r.isEmpty()){
                for(Stipulation__c objStip : UWIterate.Stipulations__r){
                    if( objStip.Status__c != 'Completed' && !objStip.Stipulation_Data__r.Suppress_In_Portal__c ){
                        OpenStipCount += 1;
                    }
                }
            }
            if(OpenStipCount > 0){
                objOpp = new Opportunity(Id = UWIterate.Opportunity__c); 
                objOpp.Has_Open_Stip__c = 'true';
                oppUpdateMap.put(objOpp.Id,objOpp);
            }else if(OpenStipCount == 0 ) {
                objOpp = new Opportunity(Id = UWIterate.Opportunity__c); 
                objOpp.Has_Open_Stip__c = 'false';
                oppUpdateMap.put(objOpp.Id,objOpp);
            }
        }
        if(!oppUpdateMap.isEmpty()){
           System.debug('###oppUpdateMap:'+oppUpdateMap.values());
           Database.update(oppUpdateMap.values(),false);
        }
       system.debug('### Exit into execute() of '+ StipulationStatusBatch.class);
    }

   global void finish(Database.BatchableContext BC){
   }
}