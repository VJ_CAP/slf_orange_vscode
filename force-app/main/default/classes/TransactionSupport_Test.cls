@isTest
private class TransactionSupport_Test 
{
	
	@isTest static void test () 
	{
		System.assertEquals (false,TransactionSupport.isProcessed('test1',1));
		System.assertEquals (true,TransactionSupport.isProcessed('test1',1));

		System.assertEquals (false,TransactionSupport.isProcessed('test2',2));
		System.assertEquals (false,TransactionSupport.isProcessed('test2',2));
		System.assertEquals (true,TransactionSupport.isProcessed('test2',2));

		System.assertEquals (false,TransactionSupport.isProcessed('test3',3));
		System.assertEquals (false,TransactionSupport.isProcessed('test3',3));
		System.assertEquals (false,TransactionSupport.isProcessed('test3',3));
		System.assertEquals (true,TransactionSupport.isProcessed('test3',3));

		System.assertEquals (false,TransactionSupport.isProcessed('test4'));
		System.assertEquals (false,TransactionSupport.isNotProcessed('test4'));

		TransactionSupport.block('Test Block 1');
		System.assert(TransactionSupport.isBlocked('Test Block 1'));

		TransactionSupport.unBlock('Test Block 1');
		System.assert(TransactionSupport.isUnBlocked('Test Block 1'));
	}	
}