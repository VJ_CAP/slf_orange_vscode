/**
*
* Description: This method is used to cover InstallerinfoServiceImpl and InstallerinfoDataServiceImpl 
*
*/
    @isTest
    public class InstallerInfoServiceImplTest{    
        @testsetup static void createtestdata(){

            SLFUtilityDataSetup.initData();
        }
    
    Public Static void MapWebServiceURI(Map<String, String> reqMap,string jsonString,string uriPeram){
        system.debug('*******reqMapstart****'+reqMap);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/api/'+uriPeram;
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        system.debug('*******reqMap****'+reqMap);
        
        if(string.isNotBlank(jsonString))
        {
            
            req.requestBody = Blob.valueof(jsonString); // Add JSON Message as a POST   
            system.debug('#########req.requestBody###############'+jsonString);
        }else{
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();

            for(String str : reqMap.Keyset()){
                gen.writeStringField(str,reqMap.get(str));
            }
            gen.writeEndObject();
            req.requestBody = Blob.valueof(gen.getAsString()); // Add JSON Message as a POST    
            system.debug('#########req.requestBody###############'+gen.getAsString());
        }
        IRestResponse resWrap = SLFRestDispatch.doPost();  
        
      //  ResponseWrapper resW = SLFRestDispatch.doGet(); 
    }
    
    private testMethod static void installerinfoTest(){
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
         Account acc1 = new Account();
        acc1.name = 'Test_Acc1';
        acc1.Installer_Email__c = 'testemail_a@gmail.com';
        acc1.Solar_Enabled__c=true;
        acc1.M2_Required__c = 'Communications';
        acc1.M0_Required__c = 'Communications';
        acc1.M1_Required__c = 'Communications';
        insert acc1;
        Contact con = new Contact();
        con.lastname = 'Test Contact1';
        con.accountid = acc1.id;
        insert con;
        User portalUser = new User(alias = 'testc', Email='testc_2@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass_1@mail.com',ContactId = con.Id);
        insert portalUser;
    
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc_2@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1]; 
        
        System.runAs(loggedInUsr[0])
        {
            Map<String, String> apprmilMap = new Map<String, String>();
            Test.startTest();
            try{
            Map<String, String> installinfoMap = new Map<String, String>();
            string installinfoString1 = ' {"installerInfo": { "milestoneFilter" :  "m3RequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString1,'getinstallerinfo');
            
            //Used to cover getinstallerinfo service API
            installinfoMap = new Map<String, String>();
            String installinfoString = '{"installerInfo": { "milestoneFilter" :  "m0RequiredDocuments"} }';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = '{"installerInfo": { "milestoneFilter" :  "m1RequiredDocuments"} }';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "m2RequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "KittingRequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "PermitRequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "InspectionRequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            //Wrong Request Format
            installinfoString = ' {"installerInfo1": { "milestoneFilter1" :  "m1RequiredDocuments","milestoneFilter":"m2RequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": {} } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            //Empty request
            installinfoString = '';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
                                   
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            RequestWrapper rw = new RequestWrapper();
            rw.reqDataStr = '';
            InstallerInfoServiceImpl objInstallerInfoServiceImpl = new InstallerInfoServiceImpl();
            objInstallerInfoServiceImpl.fetchData(rw);
            objInstallerInfoServiceImpl.documentResponseString(null);
            }catch(Exception err){}
            Test.stopTest();
        }
        String installinfoString = ' {"installerInfo": { "milestoneFilter" :  "" } } ';
        MapWebServiceURI(new Map<String, String>(),installinfoString,'getinstallerinfo');
    }
    
    
    
    
    private testMethod static void installerinfoTest2(){
        map<String, Id> roleMap1 = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap1.put(role.DeveloperName, role.Id);
        }
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
         Account acc2 = new Account();
        acc2.name = 'Test_acc2';
        acc2.Installer_Email__c = 'testemail_a2@gmail.com';
        acc2.Solar_Enabled__c=true;
        acc2.M2_Required__c = null;
        acc2.M0_Required__c = null;
        acc2.M1_Required__c = null;
        acc2.Permit_Ever_Enabled__c = true; 
        acc2.Kitting_Ever_Enabled__c = true;
        acc2.Inspection_Ever_Enabled__c = true;
        insert acc2;
        Contact con1 = new Contact();
        con1.lastname = 'Test Contact2';
        con1.accountid = acc2.id;
        insert con1;
        User portalUser = new User(alias = 'testc', Email='testc_1@mail.com', IsActive = true,Hash_Id__c='123',
        EmailEncodingKey='UTF-8', LastName='test', 
        LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, UserRoleId = roleMap1.get('Top Role'),
        //UserRoleId = r.id,
        timezonesidkey='America/Los_Angeles', 
        username='testclass_1@mail.com',ContactId = con1.Id);
        insert portalUser;
    
        List<user> loggedInUsr = [select Id,Email,contact.AccountId,contact.Account.Default_FNI_Loan_Amount__c from User where Email =: 'testc_1@mail.com'];
        
        Account acc = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1]; 
        Opportunity opp = [select Id,Name from opportunity where AccountId =: acc.Id LIMIT 1]; 
        
        System.runAs(loggedInUsr[0])
        {
            Map<String, String> apprmilMap = new Map<String, String>();
            Test.startTest();
            try{
            Map<String, String> installinfoMap = new Map<String, String>();
            string installinfoString1 = ' {"installerInfo": { "milestoneFilter" :  "m3RequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString1,'getinstallerinfo');
            
            //Used to cover getinstallerinfo service API
            installinfoMap = new Map<String, String>();
            String installinfoString = '{"installerInfo": { "milestoneFilter" :  "m0RequiredDocuments"} }';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = '{"installerInfo": { "milestoneFilter" :  "m1RequiredDocuments"} }';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "m2RequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "KittingRequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "PermitRequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "InspectionRequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            //Wrong Request Format
            installinfoString = ' {"installerInfo1": { "milestoneFilter1" :  "m1RequiredDocuments","milestoneFilter":"m2RequiredDocuments" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            installinfoString = ' {"installerInfo": {} } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            //Empty request
            installinfoString = '';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
                                   
            installinfoString = ' {"installerInfo": { "milestoneFilter" :  "" } } ';
            MapWebServiceURI(installinfoMap,installinfoString,'getinstallerinfo');
            
            RequestWrapper rw = new RequestWrapper();
            rw.reqDataStr = '';
            InstallerInfoServiceImpl objInstallerInfoServiceImpl = new InstallerInfoServiceImpl();
            objInstallerInfoServiceImpl.fetchData(rw);
            objInstallerInfoServiceImpl.documentResponseString(null);
            
            }catch(Exception ex){}
            
            Test.stopTest();
        }
        String installinfoString = ' {"installerInfo": { "milestoneFilter" :  "" } } ';
        MapWebServiceURI(new Map<String, String>(),installinfoString,'getinstallerinfo');
    }
    
    
    
}