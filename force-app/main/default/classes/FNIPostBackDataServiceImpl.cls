/**
* Description: FNI PostBack related integration logic .
*
*   Modification Log :
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma           11/03/2017          Created
******************************************************************************************/

public class FNIPostBackDataServiceImpl extends RestDataServiceBase{
    /**
* Description: Convert out response from salesforce sobject to wrapper (JSON)
*
* @param sObj           Convert the sobject record to bean.
* @param status         Status of the record like Success/Fail while create/update/delete/upsert operation.
* @param msg            Each record specific message, if success/fail.
* return null           IRestResponse returns object specific bean based on the consumer.
*/
    public override IRestResponse transformOutput(IDataObject stsDtObj){
        system.debug('### Entered into transformOutput() of '+FNIPostBackDataServiceImpl.class);
        
        UnifiedBean reqData = (UnifiedBean)stsDtObj;
        system.debug('### '+reqData);
        
        UnifiedBean FNIPostBackRespBean = new UnifiedBean(); 
        FNIPostBackRespBean.projects = new List<UnifiedBean.OpportunityWrapper>();
        //FNIPostBackben = reqData ;
        FNIPostBackRespBean.error = new list<UnifiedBean.errorWrapper>();
        IRestResponse iRestRes;
        
        string errMsg = '';
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            try
        {   
            if(null != reqData.projects)
            {
                UnifiedBean.OpportunityWrapper projectWrap = new UnifiedBean.OpportunityWrapper();
                projectWrap.credits = new List<UnifiedBean.CreditsWrapper>();
                if(null != reqData.projects[0].credits)
                {
                    List<UnifiedBean.CreditsWrapper> reqCredits = reqData.projects[0].credits;
                    List<UnifiedBean.ApplicantWrapper> reqApplicants = reqData.projects[0].applicants;
                    
                    if(reqCredits!=null && reqCredits.size()>0){
                        //Set<SLF_Credit__c> setUpdateCredits = new Set<SLF_Credit__c>();
                        List<SLF_Credit__c> lstUpdateCredits = new List<SLF_Credit__c>();
                        Map<id,SLF_Credit__c> mapUpdateCredits = new Map<id,SLF_Credit__c>();
                        
                        for(UnifiedBean.CreditsWrapper reqCredit : reqCredits){
                            
                            if(
                                (reqCredit.creditDecisionDate==null || String.isBlank(reqCredit.creditDecisionDate))||
                                (reqCredit.ltFNIDecision==null || String.isBlank(reqCredit.ltFNIDecision)))
                            {
                                errMsg = ErrorLogUtility.getErrorCodes('330', null);
                                getErrorMsg(FNIPostBackRespBean,errMsg,'330',iolist);
                                iRestRes = (IRestResponse)FNIPostBackRespBean;
                                return iRestRes;
                            }
                            //modifed the below query and error message to handle SP-711 defect
                            List<SLF_Credit__c> lstCredit = [select id,FNI_Reference_Number__c,LT_FNI_Reference_Number__c from SLF_Credit__c where  LT_FNI_Reference_Number__c=:reqCredit.fniReferenceNumber];
                            if(lstCredit!=null && lstCredit.size()>0){
                                for(SLF_Credit__c creditUpdate :lstCredit){
                                    creditUpdate.Credit_Decision_Date__c = DateTime.valueOf(reqCredit.creditDecisionDate);
                                    //creditUpdate.Credit_Decision_Date__c = DateTime.valueOfGmt(reqCredit.creditDecisionDate);
                                    
                                    if(reqCredit.maxApprovedAmount==null ){
                                        creditUpdate.Long_Term_Amount_Approved__c = 0;
                                    }else{
                                        creditUpdate.Long_Term_Amount_Approved__c = reqCredit.maxApprovedAmount;
                                    }
                                    creditUpdate.LT_StipRsn1__c = reqCredit.ltStipRsn1;
                                    creditUpdate.LT_StipRsn2__c = reqCredit.ltStipRsn2;
                                    creditUpdate.LT_StipRsn3__c = reqCredit.ltStipRsn3;
                                    creditUpdate.LT_StipRsn4__c = reqCredit.ltStipRsn4;
                                    creditUpdate.LT_FNI_Decision__c = reqCredit.ltFNIDecision;
                                    creditUpdate.FNI_Post_Back_Request__c = reqCredit.fniPostBackRequest;
                                    
                                    //RBP fields changes
                                    creditUpdate.CL_Primary_Applicant_First_Name__c = reqCredit.clPrimaryApplicantFirstName;
                                    creditUpdate.CL_Primary_Applicant_Last_Name__c = reqCredit.clPrimaryApplicantLastName;
                                    creditUpdate.CL_Primary_Applicant_Middle_Name__c = reqCredit.clPrimaryApplicantMiddleName;
                                    creditUpdate.Bureau_Primary_DoB__c = reqCredit.clPrimaryApplicantDOB;
                                    creditUpdate.Bureau_Primary_SSN__c = reqCredit.clPrimaryApplicantSSN!=null ? reqCredit.clPrimaryApplicantSSN : null;
                                    
                                    creditUpdate.CL_Co_Applicant_First_Name__c = reqCredit.clCoApplicantFirstName;
                                    creditUpdate.CL_Co_Applicant_Full_Last__c = reqCredit.clCoApplicantLastName;
                                    creditUpdate.CL_Co_Applicant_Middle_Name__c = reqCredit.clCoApplicantMiddleName;
                                    creditUpdate.Bureau_Co_Applicant_DoB__c = reqCredit.clCoApplicantDOB;
                                    creditUpdate.Bureau_Co_Applicant_SSN__c = reqCredit.clCoApplicantSSN!=null ? reqCredit.clCoApplicantSSN : null;
                                    
                                    creditUpdate.CL_Owner_of_Record__c = reqCredit.clOwnerOfRecord;
                                    creditUpdate.CL_Street_Address__c = reqCredit.clStreetAddress;
                                    creditUpdate.CL_City__c = reqCredit.clCity;
                                    creditUpdate.CL_State_Code__c = reqCredit.clStateCode;
                                    creditUpdate.CL_ZipCode__c = reqCredit.clZipCode;
                                    //End RBP Changes
                                    
                                    if(String.isNotBlank(reqCredit.creditExpirationDate))
                                    {
                                        system.debug('### creditexpirationdate: '+ reqCredit.creditExpirationDate);
                                        creditUpdate.FNI_Credit_Expiration__c = date.parse(reqCredit.creditExpirationDate);
                                    }
                                    
                                    if(!mapUpdateCredits.containsKey(creditUpdate.id)){
                                        mapUpdateCredits.put(creditUpdate.id,creditUpdate);
                                    }
                                    //setUpdateCredits.add(creditUpdate);
                                    
                                    projectWrap.credits.add(reqCredit);
                                }
                            }
                            else{
                                errMsg = ErrorLogUtility.getErrorCodes('331', null);
                                getErrorMsg(FNIPostBackRespBean,errMsg,'331',iolist);
                                iRestRes = (IRestResponse)FNIPostBackRespBean;
                                return iRestRes;
                            }
                        }
                        
                        try{
                            
                            if(mapUpdateCredits.values().size()>0){
                                lstUpdateCredits.addAll(mapUpdateCredits.values());
                                update lstUpdateCredits;
                                FNIPostBackRespBean.message = ErrorLogUtility.getErrorCodes('200', null);
                                FNIPostBackRespBean.returnCode = '200';
                                FNIPostBackRespBean.projects.add(projectWrap); 
                            }
                        }catch(Exception ex){
                            errMsg = ErrorLogUtility.getErrorCodes('330', null);
                            getErrorMsg(FNIPostBackRespBean,errMsg,'330',iolist);
                            iRestRes = (IRestResponse)FNIPostBackRespBean;
                            ErrorLogUtility.writeLog('FNIPostBackDataServiceImpl.transformOutput()',ex.getLineNumber(),'transformOutput()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));  
                            return iRestRes;
                            
                        }
                        
                    }
                }else{
                    errMsg = ErrorLogUtility.getErrorCodes('396', null);
                    getErrorMsg(FNIPostBackRespBean,errMsg,'396',iolist);
                    iRestRes = (IRestResponse)FNIPostBackRespBean;
                    return iRestRes;
                }            
                
            }
            
            iRestRes = (IRestResponse)FNIPostBackRespBean;
        }catch(Exception err){
            iolist = new list<DataValidator.InputObject>{};
                errMsg = ErrorLogUtility.getErrorCodes('400', null);
            
            UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
            //FNIPostBackben.error  = errorMsg;
            FNIPostBackRespBean.error.add(errorMsg);
            FNIPostBackRespBean.returnCode = '400';
            iRestRes = (IRestResponse)FNIPostBackRespBean;
            system.debug('### FNIPostBackDataServiceImpl.transformOutput():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            ErrorLogUtility.writeLog('FNIPostBackDataServiceImpl.transformOutput()',err.getLineNumber(),'transformOutput()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));   
        }    
        system.debug('### Exit from transformOutput() of '+FNIPostBackDataServiceImpl.class);
        system.debug('### iRestRes: '+iRestRes);
        return iRestRes;
    }
    
    public void getErrorMsg(UnifiedBean objUnifiedBean, String sErrorMsg, String sReturnCode, list<DataValidator.InputObject> iolist){
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,sErrorMsg);
        objUnifiedBean.error.add(errorMsg);
        objUnifiedBean.returnCode = sReturnCode;
    }
}