Public Class DisclosureTriggerHandler{
    public static void OnBeforeInsert(List<Disclosures__c> newDisclosureList){
        system.debug('### Entered into OnBeforeInsert() of '+ DisclosureTriggerHandler.class);
        dupCheckDisclsrRec(newDisclosureList, null, true, false);  
        system.debug('### Exit from OnBeforeInsert() of '+ DisclosureTriggerHandler.class);
    }
    
    public static void OnBeforeUpdate(List<Disclosures__c> newDisclosureList, Map<Id,Disclosures__c> oldDisclosureMap){
        system.debug('### Entered into OnBeforeUpdate() of '+ DisclosureTriggerHandler.class);
        dupCheckDisclsrRec(newDisclosureList, oldDisclosureMap, false, true);  
        system.debug('### Exit from OnBeforeUpdate() of '+ DisclosureTriggerHandler.class);
    }
    
    public static void dupCheckDisclsrRec(List<Disclosures__c> newDisclosureList, Map<Id,Disclosures__c> oldDisclosureMap, Boolean isInsert, Boolean isUpdate){
        system.debug('### Entered into dupCheckDisclsrRec() of '+ DisclosureTriggerHandler.class);  
        try{      
            Map<Id, Disclosures__c> existingDisclrMap = new Map<Id, Disclosures__c>([Select id, Active__c, Type__c, DupCheckText__c from Disclosures__c]);
            if(!existingDisclrMap.isEmpty()){
                Set<String> existingDupTextSet = new Set<String>();
                for(Disclosures__c disclsrObj : existingDisclrMap.values()){
                    existingDupTextSet.add(disclsrObj.DupCheckText__c);
                }
                system.debug('### existingDupTextSet '+ existingDupTextSet);
                for(Disclosures__c disclsrObj : newDisclosureList){
                    String str = '';
                    str = str + (disclsrObj.Active__c?'True':'False');
                    str = str + disclsrObj.Type__c;
                    system.debug('### str '+ str);
                    if(isInsert){
                        if(disclsrObj.Active__c && existingDupTextSet.contains(str)){
                            system.debug('### disclsrObj '+ disclsrObj);
                            disclsrObj.addError('Duplicate record found with same type, please deacitvate old record and try again.');
                        }   
                    }  
                    else if(isUpdate){
                        if(disclsrObj.Active__c && (oldDisclosureMap.get(disclsrObj.Id).Active__c != disclsrObj.Active__c || oldDisclosureMap.get(disclsrObj.Id).Type__c != disclsrObj.Type__c) && existingDupTextSet.contains(str)){
                            system.debug('### disclsrObj '+ disclsrObj);
                            disclsrObj.addError('Duplicate record found with same type, please deacitvate old record and try again.');
                        }  
                    }  
                }
            }    
        }
        catch(Exception ex){
            system.debug('### DisclosureTriggerHandler.dupCheckDisclsrRec():'+ex.getLineNumber()+':'+ ex.getMessage() + '###' +ex.getStackTraceString());            
            ErrorLogUtility.writeLog('DisclosureTriggerHandler.dupCheckDisclsrRec()',ex.getLineNumber(),'dupCheckDisclsrRec()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));   
        }
        system.debug('### Entered into dupCheckDisclsrRec() of '+ DisclosureTriggerHandler.class);
    }
}