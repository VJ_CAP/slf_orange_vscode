global class Batch_UpdateMilestones implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts, System.Schedulable {

    Map<Id, Boolean> successOppMap = new Map<Id, Boolean>();
    Boolean errorEncountered;
    
    /**
     * This Batch updates comments on milestones in Sighten
     * using milestones uuids stored in a quote
     * 
     **/
    public Batch_UpdateMilestones()
    {
        errorEncountered = false;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {

        // *** return all Quotes belonging to Opportunities where Upload_Comment__c != null
        String query = 'Select Id,Sighten_Milestone_UUID__c, Opportunity.Upload_Comment_Text__c, Opportunity.Account.External_Comments__c, OpportunityId from quote where OpportunityId in (SELECT Id FROM Opportunity where Upload_Comment__c = true)';
                
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext sc)
    {
        Batch_UpdateMilestones bf = new Batch_UpdateMilestones();
        Database.executeBatch(bf,10);
    }
    
    global void execute(Database.BatchableContext BC,List<Quote> ql) 
    {
        try {
        List<Quote> updateQuoteList = new List<Quote>();
        for (Quote q : ql) {
            if (sightenUtil.updateMilestoneComments(q,q.Sighten_Milestone_UUID__c,q.Opportunity.Upload_Comment_Text__c)) 
            {
                successOppMap.put(q.OpportunityId, true);
            }
            updateQuoteList.add(q);            
        }
       
        update updateQuoteList;
        } catch (Exception e) {
            errorEncountered = true;
            throw new CustException(e);
        }
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        if ( !errorEncountered) {
            // *** here we uncheck Upload_Comment__c on the opportunities that were successfully processed
            //Srikanth - 08/21 - Add an AND condition to clear the flag only for Solar City
            List<Opportunity> ol = new List<Opportunity>([Select Id from Opportunity where Upload_Comment__c = true AND Installer_Account__r.Name = 'Solar City']);
            for (Opportunity o : ol) 
            {
                if (successOppMap.containsKey(o.Id))
                {
                    o.Upload_Comment__c = false;
                    o.Upload_Comment_Text__c = '';
                }
            }
            update ol;
        }

        checkRejectedComments();
    }

    public static void checkRejectedComments ()
    {
        //  *** now count how many Opportunities still have Upload_Comment__c = true, and if > 100,
        //  *** send an email to Gary Pattison

        SFSettings__c settings = SFSettings__c.getInstance();

        list<String> toAddresses = new list<String>{'gary.pattison@restyn.com'};
        Integer threshold = 100;

        if (settings != null && settings.Comments_Alert_Emails__c != null && settings.Comments_Alert_Threshold__c != null)
        {
            toAddresses = settings.Comments_Alert_Emails__c.split(',');
            threshold = Integer.valueOf(settings.Comments_Alert_Threshold__c);
        }

        if ([SELECT COUNT() FROM Opportunity WHERE Upload_Comment__c = TRUE] > threshold)
        {
            list<Messaging.SingleEmailMessage> messages = new list<Messaging.SingleEmailMessage>{}; 
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

            message.setToAddresses(toAddresses);
            message.setSubject('WARNING: More than ' + threshold + ' Opportunity comments were rejected by Sighten');
            message.setPlainTextBody('WARNING: More than ' + threshold + ' Opportunity comments were rejected by Sighten');

            messages.add (message);

            list<Messaging.Sendemailresult> sendEmailResults = Messaging.sendEmail (messages);           
        }
    }
}