/**
* Description: Base class which provides implementation of IRestService interface, 
*              so that actual Service implementation providing classes can override 
*              the virtual method what they want to provide implementation instead 
*              of providing implementation for all methods of interface.
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma         05/30/2017             Created
******************************************************************************************/

public virtual class RestServiceBase implements IRestService{
    
    /**
    * Description: Read records from salesforce, gets invoked when action is read or HTTP operation is POST
    *
    * @param rw             RequestWrapper class contains all post body messages like request action parameters, request POST data.
    * return null           ResponseWrapper class with response status message, status code and response collection.
    */
    public virtual IRestResponse fetchData(RequestWrapper rw){
        return null;
    }
    
     /**
    * Description: Read records from salesforce, gets invoked when action is read or HTTP operation is POST
    *
    * @param rw             RequestWrapper class contains all delete body messages like request action parameters, request delete data.
    * return null           ResponseWrapper class with response status message, status code and response collection.
    */
    public virtual IRestResponse deleteData(RequestWrapper rw){
        return null;
    }
}