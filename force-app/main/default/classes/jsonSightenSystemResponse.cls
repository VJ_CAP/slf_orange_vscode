public class jsonSightenSystemResponse {

    public class Messages {
        public List<Error> error;
        public List<Error> warning;
        public List<Error> critical;
        public List<Error> info;
    }

    public class Owned_by_organization {
        public String uuid;
        public String link;
        public String natural_id;
    }

    public Data data;
    public Integer status_code;
    public Messages messages;

    public class Error {
    }

    
    public class Monthly_generation {
        public Integer one;
        public Integer two;
        public Integer three;
        public Integer four;
        public Integer five;
        public Integer six;
        public Integer seven;
        public Integer eight;
        public Integer nine;
        public Integer ten;
        public Integer eleven;
        public Integer twelve;
    }

    public class Data {
        public Double consumption_offset;
        public String primary_inverter_mfr;
        public Owned_by_organization owned_by_organization;
        public String uuid;
        public Owned_by_organization created_by;
        public Monthly_generation monthly_generation;
        public String primary_module_mfr;
        public Integer n_modules;
        public String natural_id;
        public Double generation;
        public Owned_by_organization site;
        public String name;
        public Arrays arrays;
        public Owned_by_organization owned_by_user;
        public Double capacity;
        public Double productivity;
        public Owned_by_organization modified_by;
    }

    public class Arrays {
        public String link;
    }

    
    public static jsonSightenSystemResponse parse(String json) {
        return (jsonSightenSystemResponse) System.JSON.deserialize(json, jsonSightenSystemResponse.class);
    }
}