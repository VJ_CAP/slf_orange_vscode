/**
* Description: Trigger Business logic associated with State Allocation is handled here. 
*
*   Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Adithya Sarma K         07/10/2019          Created 
******************************************************************************************/

public with sharing Class StateAllocationTriggerHandler
{
    public static boolean isTrgExecuting = true;
    
    /**
    * Constructor to initialize
    *
    * @param isExecuting    Has value of which context it is executing.
    */
    public StateAllocationTriggerHandler(boolean isExecuting){
        //   isTrgExecuting = isExecuting;
    }
    /**
    * Description: 
    *
    * @param new Credit records.
    */
    public void OnBeforeInsert(State_Allocation__c[] newStateAllocation){
        try
        {
            system.debug('### Entered into OnBeforeInsert() of '+ StateAllocationTriggerHandler.class);
            for(State_Allocation__c stAllocIterate : newStateAllocation){
                string errorMessage = 'Allocation must sum up to 100%. Please adjust allocation across the Facilities for @X Year.';
                boolean isErrorExist = false;
                if(null != stAllocIterate.X5_Year_Allocation_Total__c && 0.00 != stAllocIterate.X5_Year_Allocation_Total__c && 100 != stAllocIterate.X5_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '5');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X10_Year_Allocation_Total__c && 0.00 != stAllocIterate.X10_Year_Allocation_Total__c && 100 != stAllocIterate.X10_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '10');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X12_Year_Allocation_Total__c && 0.00 != stAllocIterate.X12_Year_Allocation_Total__c && 100 != stAllocIterate.X12_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '12');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X15_Year_Allocation_Total__c && 0.00 != stAllocIterate.X15_Year_Allocation_Total__c && 100 != stAllocIterate.X15_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '15');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X20_Year_Allocation_Total__c && 0.00 != stAllocIterate.X20_Year_Allocation_Total__c && 100 != stAllocIterate.X20_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '20');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X25_Year_Allocation_Total__c && 0.00 != stAllocIterate.X25_Year_Allocation_Total__c && 100 != stAllocIterate.X25_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '25');
                    isErrorExist = true;
                }
                if(isErrorExist)
                    stAllocIterate.addError(errorMessage);
            }            
            system.debug('### Exit from OnBeforeInsert() of '+ StateAllocationTriggerHandler.class);
        }catch(Exception ex)
        {
            ErrorLogUtility.writeLog('StateAllocationTriggerHandler.OnBeforeInsert()',ex.getLineNumber(),'OnBeforeInsert()',ex.getMessage() + '###' +ex.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(ex,null,null,null,null));
        }
    }
    
    
    /**
    * Description: OnBeforeUpdate
    * 
    * @param new Credit records.
    */
    public  void OnBeforeUpdate(State_Allocation__c[] newStateAllocation,Map<Id,State_Allocation__c> oldStateAllocationMap){
        try{
            system.debug('### Entered into OnBeforeUpdate() of '+ StateAllocationTriggerHandler.class);
            for(State_Allocation__c stAllocIterate : newStateAllocation){
                string errorMessage = 'Allocation must sum up to 100%. Please adjust allocation across the Facilities for @X Year.';
                boolean isErrorExist = false;
                if(null != stAllocIterate.X5_Year_Allocation_Total__c && 0.00 != stAllocIterate.X5_Year_Allocation_Total__c && 100 != stAllocIterate.X5_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '5');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X10_Year_Allocation_Total__c && 0.00 != stAllocIterate.X10_Year_Allocation_Total__c && 100 != stAllocIterate.X10_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '10');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X12_Year_Allocation_Total__c && 0.00 != stAllocIterate.X12_Year_Allocation_Total__c && 100 != stAllocIterate.X12_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '12');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X15_Year_Allocation_Total__c && 0.00 != stAllocIterate.X15_Year_Allocation_Total__c && 100 != stAllocIterate.X15_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '15');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X20_Year_Allocation_Total__c && 0.00 != stAllocIterate.X20_Year_Allocation_Total__c && 100 != stAllocIterate.X20_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '20');
                    isErrorExist = true;
                }
                if(null != stAllocIterate.X25_Year_Allocation_Total__c && 0.00 != stAllocIterate.X25_Year_Allocation_Total__c && 100 != stAllocIterate.X25_Year_Allocation_Total__c)
                {
                    errorMessage = errorMessage.replace('@X', '25');
                    isErrorExist = true;
                }
                if(isErrorExist)
                    stAllocIterate.addError(errorMessage);
            }  
            system.debug('### Exit from OnBeforeUpdate() of '+ StateAllocationTriggerHandler.class);
        }catch(Exception err){
            
            system.debug('### StateAllocationTriggerHandler.OnBeforeUpdate():'+err.getLineNumber()+':'+ err.getMessage() + '###' +err.getStackTraceString());
            
            ErrorLogUtility.writeLog('StateAllocationTriggerHandler.OnBeforeUpdate()',err.getLineNumber(),'OnBeforeUpdate()',err.getMessage() + '###' +err.getStackTraceString(),'Error',ErrorLogUtility.getErrorRecIds(err,null,null,null,null));           
        }
    }
}