/******************************************************************************************
* Description: The Class CreateTaskForUnderWrittingCTRL to do unin test 
*
* Modification Log : 
---------------------------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------------------------
Veereandranath Jalla   11/27/2018          Created
******************************************************************************************/
@isTest
public class UpdateTaskForUnderWrittingCTRLTest {
     @testsetup static void createtestdata(){
        //SLFUtilityDataSetup.initData();
        myUnitTest();
    }
    public static testMethod void myUnitTest(){
                
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgAccflagTsk = new TriggerFlags__c();
        trgAccflagTsk.Name ='Task';
        trgAccflagTsk.isActive__c =true;
        trgLst.add(trgAccflagTsk);
        
        TriggerFlags__c trgAccflagFunding = new TriggerFlags__c();
        trgAccflagFunding.Name ='Funding_Data__c';
        trgAccflagFunding.isActive__c =true;
        trgLst.add(trgAccflagFunding);
        
        insert trgLst;
        Test.startTest();
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();   
        
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.Inspection_Enabled__c = true;
        acc.Kitting_Enabled__c = true;
        acc.Permit_Enabled__c = true;
        acc.Kitting_Split__c = 10;
        acc.Permit_Split__c = 10;
        acc.Inspection_Split__c =10 ;
        acc.M0_Split__c=20;
        acc.M1_Split__c=20;
        acc.M2_Split__c=30;
        insert acc;
                 
        Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.Product_Loan_Type__c='Solar';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = false;
            // prod.Product_Tier__c ='1';
            //prod.Long_Term_Facility__c='TCU';
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            // prod.Is_Active__c= true;
            prod.Product_Tier__c = '0';
            insert prod;
         
         Opportunity opp = new Opportunity();
            opp.name = 'OpName';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA';
            opp.Install_Street__c ='Street';
            opp.Install_City__c ='InCity';
            opp.SLF_Product__c = prod.id;
            opp.isACH__c = true;
            insert opp;
        
        Underwriting_File__c undStpbt = new Underwriting_File__c();           
        undStpbt.Opportunity__c = opp.id;
        undStpbt.Approved_for_Payments__c = true;
        undStpbt.M0_Approval_Date__c = system.today();
        undStpbt.Project_Status__c = 'M0';
        undStpbt.Confirm_ACH_Information__c = 'ACH data Not Confirmed';
        insert undStpbt;
        
      //  undStpbt.M0_Approval_Date__c = system.now();
      //  undStpbt.Project_Status__c = 'M1';
      //  update undStpbt;
        
        system.debug('***Underwriting Record****'+undStpbt);
        
        Task objTask = new Task();
        objTask.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Underwriting Task').getRecordTypeId();
        objTask.WhatId = undStpbt.id;//strUnWrId;
        objTask.OwnerId = UserInfo.getUserId();
        objTask.Status = 'Open';
        objTask.Subject = undStpbt.Project_Status__c + 'Review' ;
        objTask.ActivityDate = system.today();
        objTask.Start_Date_Time__c = system.now();
        objTask.IsReminderSet = true;
        insert objTask;
        
        Stipulation_Data__c  sd = new Stipulation_Data__c(Name = 'APR', Email_Text__c = 'Test data', POS_Message__c = 'test', Review_Classification__c = 'SLS II');
        insert sd;
        
        Stipulation__c stp = new Stipulation__c();
        stp.Status__c ='New';
        stp.Stipulation_Data__c = sd.id;
        stp.Underwriting__c =undStpbt.id;
        insert stp;
        system.debug('***Stipulation Record****'+stp);
        
        List<UpdateTaskForUnderWrittingCTRL.Input> lstInput = new List<UpdateTaskForUnderWrittingCTRL.Input>(); 
        UpdateTaskForUnderWrittingCTRL.Input globalInput1 = new UpdateTaskForUnderWrittingCTRL.Input();
        lstInput.add(globalInput1);

        UpdateTaskForUnderWrittingCTRL.Input globalInput = new UpdateTaskForUnderWrittingCTRL.Input(undStpbt.Id);
        globalInput.strUnderWrittingId = undStpbt.Id;  
        lstInput.add(globalInput);
        
        List<UpdateTaskForUnderWrittingCTRL.Output> lstOutput = new List<UpdateTaskForUnderWrittingCTRL.Output>();
        UpdateTaskForUnderWrittingCTRL.Output ouput = new UpdateTaskForUnderWrittingCTRL.Output();
        lstOutput = UpdateTaskForUnderWrittingCTRL.UpdateTaskStatus(lstInput);
      //  for(UpdateTaskForUnderWrittingCTRL.Output objouput:lstOutput){
      //  system.assertequals(null,objouput.message);
     //   }
        test.stopTest();
    }
}