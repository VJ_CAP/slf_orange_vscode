/**
* Description: RequestActionParam class has all request parameters like application id, sobject, action, operations
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma         05/30/2017             Created
******************************************************************************************/

public class RequestActionParam{

    //sObject API name like Account, Opportunity, etc
    public String serviceName{get; set;}
    
    //HTTP actions like POST, GET, PUT, DELETE, etc
    public String action {get; set;}
    
    //SFDC Operations like Create, Update, Read and Delete
    public String operation {get; set;}
    
    public Id sObjId {get; set;} 
}