@isTest
public class SLFUtilityTest{
    @TestSetup
    static void initData()
    {
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgFundingPackg = new TriggerFlags__c();
        trgFundingPackg.Name ='Funding_Data__c';
        trgFundingPackg.isActive__c =true;
        trgLst.add(trgFundingPackg);
        
        insert trgLst;
        
        //Insert Custom Endpoints Custom Setting Record
        List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
        SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
        endpoint1.Name = 'Pricing';
        endpoint1.Endpoint_URL__c =  'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
        endpoint.add(endpoint1);
        
        SLF_Boomi_API_Details__c endpoint2 = new SLF_Boomi_API_Details__c();
        endpoint2.Name = 'BoomiToAWSPricing';
        endpoint2.Endpoint_URL__c =  'https://test.connect.boomi.com/ws/rest/v1/dev/pricing/callaws/';
        endpoint.add(endpoint2);
        
        
        SLF_Boomi_API_Details__c apidetails = new SLF_Boomi_API_Details__c();
        apidetails.Name ='Boomi QA';
        apidetails.Username__c ='sunlightfinancial-WSHA3Z.N29SCE';
        apidetails.Password__c ='2458a5de-d689-4708-a4fb-d3594c067610';
        apidetails.Endpoint_URL__c ='https://test.connect.boomi.com/ws/soap12/prequal';
        apidetails.viaBoomi__c =true;
        endpoint.add(apidetails);
        
        SLF_Boomi_API_Details__c apidetails1 = new SLF_Boomi_API_Details__c();
        apidetails1.Name ='Application Integration'; 
        apidetails1.Username__c ='EXa2Ns4nkI3/eO28jtW6Fdj/24lyCKRICcIWFvpDpAU';
        apidetails1.Password__c ='0ZyMD44G4iGqjtZ6kGUOOBHBbJY3P4VYd5A8iRI6gv8';
        // https://sfws.sunlightfinancial.com/ws/ApplicationIntegration.svc/json/
        apidetails1.Endpoint_URL__c ='https://test.connect.boomi.com/ws/ApplicationIntegration.svc/json/';
        
        apidetails1.viaBoomi__c =true; 
        apidetails1.Timeout__c = 60000;
        endpoint.add(apidetails1);
        
        
        insert endpoint;
        
        List<Build_Unified_JSON__c> buildUnifiedJSONLst = new List<Build_Unified_JSON__c>();
        Build_Unified_JSON__c buildUnifiedJsonId = new Build_Unified_JSON__c();
        buildUnifiedJsonId.Name = '01';
        buildUnifiedJsonId.Attribute_Name__c = 'id';
        buildUnifiedJsonId.Field_API_Name__c = 'id';
        buildUnifiedJsonId.Object_API_Name__c = 'SLF_Credit__c';
        buildUnifiedJSONLst.add(buildUnifiedJsonId);
        
        Build_Unified_JSON__c buildUnifiedJsonStatus = new Build_Unified_JSON__c();
        buildUnifiedJsonStatus.Name = '02';
        buildUnifiedJsonStatus.Attribute_Name__c = 'Status__c';
        buildUnifiedJsonStatus.Field_API_Name__c = 'Status__c';
        buildUnifiedJsonStatus.Object_API_Name__c = 'SLF_Credit__c';
        buildUnifiedJSONLst.add(buildUnifiedJsonStatus);
        
        insert buildUnifiedJSONLst;
        
        //get the account record type
        Schema.DescribeSObjectResult sobjResult = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recTypeInfoMap = sobjResult.getRecordTypeInfosByName();      
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community User for Salesforce1' limit 1];
        User u = [Select id, name, email from User where ProfileId = :p.Id limit 1];
        
        TriggerFlags__c trgflag1 = new TriggerFlags__c();
        trgflag1.Name ='Account';
        trgflag1.isActive__c =true;
        insert trgflag1;
        
        //For getInstallerinfoServiceImpl Inserting Portal Test User 
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.Installer_Email__c = 'test@gmail.com';
        acc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        acc.FNI_Domain_Code__c = 'TCU';
        acc.Subscribe_to_Event_Updates__c = true;
        acc.Installer_Legal_Name__c='Bright Solar Planet';
        acc.Solar_Enabled__c = true;
        insert acc;
        
        List<Draw_Allocation__c> lstDrawAllocInst = new List<Draw_Allocation__c>();
        Draw_Allocation__c drawAlloc = new Draw_Allocation__c();
        drawAlloc.Account__c = acc.id;
        drawAlloc.Level__c = '1';
        drawAlloc.Draw_Allocation__c = 50;
        lstDrawAllocInst.add(drawAlloc);
        
        Draw_Allocation__c drawAlloc1 = new Draw_Allocation__c();
        drawAlloc1.Account__c = acc.id;
        drawAlloc1.Level__c = '2';
        drawAlloc1.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc1);
        
        Draw_Allocation__c drawAlloc2 = new Draw_Allocation__c();
        drawAlloc2.Account__c = acc.id;
        drawAlloc2.Level__c = '3';
        drawAlloc2.Draw_Allocation__c = 25;
        lstDrawAllocInst.add(drawAlloc2);
        
        insert lstDrawAllocInst;
        //End
        
        Contact con = new Contact(LastName ='testCon12',AccountId = acc.Id);
        insert con;  
        
        //User thisUser = [SELECT Id,userrole.id FROM User WHERE Id = :UserInfo.getUserId()];
        map<String, Id> roleMap = new map<String, Id>();
        for(UserRole role:[select Id, DeveloperName from UserRole]){
            roleMap.put(role.DeveloperName, role.Id);
        }
        User portalUser = new User(alias = 'testc1', Email='testc@mail.com', IsActive = true,
                                   EmailEncodingKey='UTF-8', LastName='test', 
                                   LanguageLocaleKey='en_US', 
                                   LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = roleMap.get('Top Role'),
                                   //UserRoleId = r.id,
                                   timezonesidkey='America/Los_Angeles', 
                                   username='testclass12@mail.com',ContactId = con.Id);
        insert portalUser;
        
        acc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        
        acc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        acc.BillingStateCode = 'CA';
        update acc;
        
        
        AccountShare accShare = new AccountShare(); //a new empty AccountShare object
        accShare.userorgroupid = portalUser.Id;
        accShare.accountid = con.accountid;
        accShare.accountaccesslevel = 'Edit';
        accShare.OpportunityAccessLevel = 'None';
        accShare.CaseAccessLevel = 'None';
        insert accShare;
        
        //Insert Account record
        Account personAcc = new Account();
        personAcc.Name = 'SLFTest12';
        personAcc.RecordTypeId = recTypeInfoMap.get('Business Account').getRecordTypeId();
        personAcc.M0_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M1_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.M2_Required__c ='Communications;Loan Agreements;Government IDs;Final Designs;Install Contracts;Installation Photos;Approval Letters;Utility Bills;Payment Election Form';
        personAcc.BillingStateCode = 'CA';
        personAcc.Installer_Legal_Name__c='Bright Solar Planet';
        //personAcc.OwnerId = portalUser.Id;
        personAcc.Solar_Enabled__c = true;
        insert personAcc;
        
        Contact personcon = new Contact(LastName ='testCon',AccountId = personAcc.Id);
        insert personcon; 
        
        personAcc = [select Id from Account where Id=: personAcc.Id];
        
        personAcc.recordTypeId = recTypeInfoMap.get('Person Account').getRecordTypeId();
        update personAcc;
        
        
        AccountShare personAccShare = new AccountShare(); //a new empty AccountShare object
        personAccShare.userorgroupid = portalUser.Id;
        personAccShare.accountid = personAcc.Id;
        personAccShare.accountaccesslevel = 'Edit';
        personAccShare.OpportunityAccessLevel = 'None';
        personAccShare.CaseAccessLevel = 'None';
        insert personAccShare;
        
        System.runAs (portalUser)
        { 
            //Insert SLF Product Record
            Product__c prod = new Product__c();
            prod.Installer_Account__c =acc.id;
            prod.Long_Term_Facility_Lookup__c =acc.id;
            prod.Name='testprod';
            prod.FNI_Min_Response_Code__c=9;
            prod.Product_Display_Name__c='test';
            prod.Qualification_Message__c ='testmsg';
            prod.State_Code__c ='CA';
            prod.ST_APR__c = 2.99;
            prod.Long_Term_Facility__c = 'TCU';
            prod.APR__c = 2.99;
            prod.ACH__c = true;
            prod.Internal_Use_Only__c = true;
            //prod.Expiration_Date__c = date.today()+10;
            prod.LT_Max_Loan_Amount__c =1000;
            prod.Term_mo__c = 120;
            prod.Product_Tier__c = '0';
            insert prod;
            
            //Inserting duplicate SLF Product Records
            Product__c prod1 = new Product__c();
            prod1.Installer_Account__c =acc.id;
            prod1.Long_Term_Facility_Lookup__c =acc.id;
            prod1.Name='testprod';
            prod1.FNI_Min_Response_Code__c=9;
            prod1.Product_Display_Name__c='test';
            prod1.Qualification_Message__c ='testmsg';
            prod1.State_Code__c ='CA';
            prod1.ST_APR__c = 2.99;
            prod1.Long_Term_Facility__c = 'TCU';
            prod1.APR__c = 2.99;
            prod1.ACH__c = true;
            prod1.Internal_Use_Only__c = true;
            
            prod1.LT_Max_Loan_Amount__c =1000;
            prod1.Term_mo__c = 144;
            prod1.Product_Tier__c = '0';
            insert prod1;
            
            //Inserting duplicate SLF Product Records
            Product__c prod2 = new Product__c();
            prod2.Installer_Account__c =acc.id;
            prod2.Long_Term_Facility_Lookup__c =acc.id;
            prod2.Name='testprod';
            prod2.FNI_Min_Response_Code__c=9;
            prod2.Product_Display_Name__c='test';
            prod2.Qualification_Message__c ='testmsg';
            prod2.State_Code__c ='CA';
            prod2.ST_APR__c = 2.99;
            prod2.Long_Term_Facility__c = 'TCU';
            prod2.APR__c = 2.99;
            prod2.ACH__c = true;
            prod2.Internal_Use_Only__c = true;
            
            prod2.LT_Max_Loan_Amount__c =1000;
            prod2.Term_mo__c = 144;
            prod2.Product_Tier__c = '0';
            insert prod2;
            
            //Insert SLF Product Record
            Product_Proxy__c prodProxy = new Product_Proxy__c();
            prodProxy.ACH__c = true;
            prodProxy.APR__c = 2.99;
            prodProxy.Installer__c =acc.id;
            prodProxy.Internal_Use_Only__c = false;
            prodProxy.Is_Active__c = true;
            prodProxy.SLF_ProductID__c = prod.Id;
            prodProxy.Name = 'testprod';
            prodProxy.State_Code__c ='CA';
            prodProxy.Term_mo__c = 120;
            insert prodProxy;
            
            Product_Proxy__c prodProxy1 = new Product_Proxy__c();
            prodProxy1.ACH__c = true;
            prodProxy1.APR__c = 2.99;
            prodProxy1.Installer__c =acc.id;
            prodProxy1.Internal_Use_Only__c = false;
            prodProxy1.Is_Active__c = true;
            prodProxy1.SLF_ProductID__c = prod1.Id;
            prodProxy1.Name = 'testprod';
            prodProxy1.State_Code__c ='CA';
            prodProxy1.Term_mo__c = 144;
            insert prodProxy1;
            
            Product_Proxy__c prodProxy2 = new Product_Proxy__c();
            prodProxy2.ACH__c = true;
            prodProxy2.APR__c = 2.99;
            prodProxy2.Installer__c =acc.id;
            prodProxy2.Internal_Use_Only__c = false;
            prodProxy2.Is_Active__c = true;
            prodProxy2.SLF_ProductID__c = prod2.Id;
            prodProxy2.Name = 'testprod';
            prodProxy2.State_Code__c ='CA';
            prodProxy2.Term_mo__c = 144;
            insert prodProxy2;
            
            test.starttest();
            //Insert opportunity record
            Opportunity personOpp = new Opportunity();
            personOpp.name = 'perOpputility1';
            personOpp.CloseDate = system.today();
            personOpp.StageName = 'Qualified';
            personOpp.AccountId = personAcc.id;
            personOpp.Installer_Account__c = acc.id;
            personOpp.Install_State_Code__c = 'CA'; 
            personOpp.Combined_Loan_Amount__c =10000;
            personOpp.Partner_Foreign_Key__c = 'TCU||12345';
            personOpp.Hash_Id__c = 'hashId1';
            //opp.Approved_LT_Facility__c ='TCU';
            personOpp.SLF_Product__c = prod.id;
            
            insert personOpp;
            
            //Insert opportunity record
            Opportunity opp = new Opportunity();
            opp.name = 'Opprtest';
            opp.CloseDate = system.today();
            opp.StageName = 'Qualified';
            opp.AccountId = acc.id;
            opp.Co_Applicant__c = acc.id;
            opp.Installer_Account__c = acc.id;
            opp.Install_State_Code__c = 'CA'; 
            opp.Combined_Loan_Amount__c = 10000;
            opp.Partner_Foreign_Key__c = 'TCU||123';
            //opp.Approved_LT_Facility__c = 'TCU';
            opp.SLF_Product__c = prod.id;
            opp.Opportunity__c = personOpp.Id;
            opp.Hash_Id__c = 'hashId';
            opp.isACH__c = true;
            insert opp;
            
            
            personOpp.Opportunity__c = opp.Id;
            update personOpp;
            
            //Insert Credit Record
            SLF_Credit__c credit = new SLF_Credit__c();
            credit.Opportunity__c = opp.id;
            credit.Primary_Applicant__c = acc.id;
            credit.Co_Applicant__c = acc.id;
            credit.SLF_Product__c = prod.Id ;
            credit.Total_Loan_Amount__c = 100;
            credit.Status__c = 'New';
            credit.LT_FNI_Reference_Number__c = '123';
            credit.Application_ID__c = '123';
            insert credit;
            
            SLF_Credit__c credit1 = new SLF_Credit__c();
            credit1.Opportunity__c = opp.id;
            credit1.Primary_Applicant__c = acc.id;
            credit1.Co_Applicant__c = acc.id;
            credit1.SLF_Product__c = prod.Id ;
            credit1.Total_Loan_Amount__c = 100;
            credit1.Status__c = 'New';
            credit1.LT_FNI_Reference_Number__c = '123';
            credit1.Application_ID__c = '123';
            credit1.IsSyncing__c = false;
            insert credit1;
                                  
            opp.Synced_Credit_Id__c = credit.Id;
            update opp;
            
            
            //Insert System Design Record
            System_Design__c sysDesignRec = new System_Design__c();
            sysDesignRec.System_Cost__c = 11;
            sysDesignRec.Opportunity__c = personOpp.Id;
            sysDesignRec.Est_Annual_Production_kWh__c = 10;
            sysDesignRec.Inverter_Count__c = 11;
            sysDesignRec.Inverter_Model__c = '12';
            sysDesignRec.Module_Count__c = 10;
            sysDesignRec.Module_Model__c = '11';
            insert sysDesignRec;
            
            //Insert Quote Record
            Quote quoteRec = new Quote();
            quoteRec.Name ='Test quote';
            quoteRec.Opportunityid = opp.id;
            quoteRec.SLF_Product__c = prod.Id ;
            quoteRec.System_Design__c= sysDesignRec.id;
            quoteRec.isACH__c = true;
            insert quoteRec;    
            
            Credit_Opinion__c credOp = new Credit_Opinion__c();
            credOp.Credit__c = credit.id;
            credOp.Quote__c = quoteRec.id;
            credOp.Status__c = 'New';
            insert credOp;
            
            Underwriting_File__c underWrFilRec = new Underwriting_File__c();
            underWrFilRec.name = 'Underwriting File1';
            underWrFilRec.ACT_Review__c ='Yes';
            underWrFilRec.Opportunity__c = opp.Id;
            underWrFilRec.All_Installers_M2_Queue__c = 'Yes';
            underWrFilRec.Install_Contract_Review_Complete__c = true;
            underWrFilRec.Utility_Bill_Review_Complete__c = true;
            underWrFilRec.LT_Loan_Agreement_Review_Complete__c = true;
            underWrFilRec.Installation_Photos_Review_Complete__c = true;
            underWrFilRec.Final_Design_Review_Complete__c = true;   
            underWrFilRec.Funding_status__c = 'Under Review by Sunlight Financial';
            underWrFilRec.Title_Review_Complete_Date__c = system.today();
            insert underWrFilRec;
            
            Box_Fields__c boxFlds = new Box_Fields__c(PTO_Received__c = system.today(),
                                                      Underwriting__c = underWrFilRec.Id,
                                                      Box_Approval_Letters_TS__c  =  system.Today(),
                                                      Box_Loan_Agreements_TS__c =  system.Today(),
                                                      Box_Communications_TS__c =  system.Today(),
                                                      Box_Government_IDs_TS__c =  system.Today(),
                                                      Box_Final_Designs_TS__c =  system.Today(),
                                                      Box_Install_Contracts_TS__c  =  system.Today(),
                                                      Box_Installation_Photos_TS__c  = system.Today(),
                                                      Box_Utility_Bills_TS__c  =  system.Today(),
                                                      Box_Payment_Election_Form_TS__c  =  system.Today(),
                                                      Final_Design_Document_Received__c = system.today()
                                                     );
            insert boxFlds;
            
            Stipulation_Data__c sData  = new Stipulation_Data__c();
            sData.Name = 'SD Test';
            sData.CL_Code__c='CL1';
            sData.POS_Message__c='test data';
            sData.Review_Classification__c = 'SLS II';
            insert sData;
            
            Stipulation__c stipTest  = new Stipulation__c();
            stipTest.Status__c = 'New';
            stipTest.Completed_Date__c = system.today();
            stipTest.Stipulation_Data__c  = sData.id;
            stipTest.Underwriting__c = underWrFilRec.id;
            stipTest.ETC_Notes__c = 'test';
            stipTest.Credit_Record__c = credit.id;
            insert stipTest;
            
            dsfs__DocuSign_Status__c docusignVoided = new dsfs__DocuSign_Status__c();
            docusignVoided.dsfs__Opportunity__c = opp.Id;
            docusignVoided.dsfs__Sent_Date_Time__c = system.Now();  
            docusignVoided.dsfs__Subject__c = 'Test';
            docusignVoided.dsfs__Completed_Date_Time__c = system.Now(); 
            docusignVoided.dsfs__Envelope_Status__c = 'voided';
            insert docusignVoided;
            
            dsfs__DocuSign_Status__c docusignDeclined = new dsfs__DocuSign_Status__c();
            docusignDeclined.dsfs__Opportunity__c = opp.Id;
            docusignDeclined.dsfs__Sent_Date_Time__c = system.Now();    
            docusignDeclined.dsfs__Subject__c = 'Test';
            docusignDeclined.dsfs__Completed_Date_Time__c = system.Now();   
            docusignDeclined.dsfs__Envelope_Status__c = 'declined';
            insert docusignDeclined;
            
            
            test.stoptest();
        }   
    }
    /**
* Description: Thais method is used to create Custom Settings Data
*/
    Public Static void createCustomSettings(){
        List<TriggerFlags__c> trgLst = new List<TriggerFlags__c>();
        
        TriggerFlags__c trgOppflag = new TriggerFlags__c();
        trgOppflag.Name ='Opportunity';
        trgOppflag.isActive__c =true;
        trgLst.add(trgOppflag);
        
        TriggerFlags__c trgCreditflag = new TriggerFlags__c();
        trgCreditflag.Name ='SLF_Credit__c';
        trgCreditflag.isActive__c =true;
        trgLst.add(trgCreditflag);
        
        TriggerFlags__c trgAccflag = new TriggerFlags__c();
        trgAccflag.Name ='Account';
        trgAccflag.isActive__c =true;
        trgLst.add(trgAccflag);
        
        TriggerFlags__c trgQotflag = new TriggerFlags__c();
        trgQotflag.Name ='Quote';
        trgQotflag.isActive__c =true;
        trgLst.add(trgQotflag);
        
        TriggerFlags__c trgConflag = new TriggerFlags__c();
        trgConflag.Name ='Contact';
        trgConflag.isActive__c =true;
        trgLst.add(trgConflag);
        
        TriggerFlags__c trgUserflag = new TriggerFlags__c();
        trgUserflag.Name ='User';
        trgUserflag.isActive__c =true;
        trgLst.add(trgUserflag);
        
        
        TriggerFlags__c trgStipflag = new TriggerFlags__c();
        trgStipflag.Name ='Stipulation__c';
        trgStipflag.isActive__c =true;
        trgLst.add(trgStipflag);
        
        TriggerFlags__c trgUnderwrtflag = new TriggerFlags__c();
        trgUnderwrtflag.Name ='Underwriting_File__c';
        trgUnderwrtflag.isActive__c =true;
        trgLst.add(trgUnderwrtflag);
        
        TriggerFlags__c trgSystemDesnflag = new TriggerFlags__c();
        trgSystemDesnflag.Name ='System_Design__c';
        trgSystemDesnflag.isActive__c =true;
        trgLst.add(trgSystemDesnflag);
        
        TriggerFlags__c trgDocuSinflag = new TriggerFlags__c();
        trgDocuSinflag.Name ='dsfs__DocuSign_Status__c';
        trgDocuSinflag.isActive__c =true;
        trgLst.add(trgDocuSinflag);
        
        TriggerFlags__c trgFundingPackg = new TriggerFlags__c();
        trgFundingPackg.Name ='Funding_Data__c';
        trgFundingPackg.isActive__c =true;
        trgLst.add(trgFundingPackg);
        
        insert trgLst;
        
        SFSettings__c SFSettings = new SFSettings__c();
        SFSettings.Encrypt_Private_Key__c ='SLFOrange';
        insert SFSettings;
        
        //Insert Custom Endpoints Custom Setting Record
        List<SLF_Boomi_API_Details__c> endpoint = new List<SLF_Boomi_API_Details__c>();
        SLF_Boomi_API_Details__c endpoint1 = new SLF_Boomi_API_Details__c();
        endpoint1.Name = 'Pricing';
        endpoint1.Endpoint_URL__c =  'http://mockbin.org/bin/240ef9db-522e-4656-acdb-1b2f88db4525';
        endpoint.add(endpoint1);
        
        SLF_Boomi_API_Details__c endpoint2 = new SLF_Boomi_API_Details__c();
        endpoint2.Name = 'BoomiToAWSPricing';
        endpoint2.Endpoint_URL__c =  'https://test.connect.boomi.com/ws/rest/v1/dev/pricing/callaws/';
        endpoint.add(endpoint2);
        
        
        SLF_Boomi_API_Details__c apidetails = new SLF_Boomi_API_Details__c();
        apidetails.Name ='Boomi QA';
        apidetails.Username__c ='sunlightfinancial-WSHA3Z.N29SCE';
        apidetails.Password__c ='2458a5de-d689-4708-a4fb-d3594c067610';
        apidetails.Endpoint_URL__c ='https://test.connect.boomi.com/ws/soap12/prequal';
        apidetails.viaBoomi__c =true;
        endpoint.add(apidetails);
        
        insert endpoint;
        
        SF_Category_Map__c sfmap = new SF_Category_Map__c();
        sfmap.Name = 'Loan Agreement';
        sfmap.Folder_Name__c = 'Loan Agreements';
        sfmap.Folder_Id_Field_on_Underwriting__c = 'boxId_Loan_Agreements__c';
        sfmap.TS_Field_On_Underwriting__c = 'LT_Loan_Agreement_Received__c';
        sfmap.Last_Run_TS_Field_on_Underwriting_File__c =  'LT_Loan_Agreement_Received__c';
        insert sfmap;
        
        SF_Category_Map__c cm1 = new SF_Category_Map__c (Name = 'Test Folder Name', Folder_Name__c = 'Test Folder Name',
                                                         Folder_Id_Field_on_Underwriting__c = 'boxId_Communications__c',
                                                         TS_Field_On_Underwriting__c = 'Box_Communications_TS__c',Last_Run_TS_Field_on_Underwriting_File__c =  'Latest_Communications_Date_Time__c');
        insert cm1;
        
    }
    
    private testMethod static void testWriteLogExp()
    {  
        ErrorLogUtility.writeLog('funtion', 1, 'classMethod', 'Description', 'Database.insert','');       
        ErrorLogUtility.writeFutureLog('funtion', 1, 'classMethod', 'Description', 'Database.insert');
    }
    
    /**
* Description : This method is used to cover insertData method on SLFUtility.
*/
    private testMethod static void SLFInsertDataTest()
    {
        List<Account> insertList = [select id from Account limit 1];
        List<Account> accTest = insertList.clone();
        SLFUtility.insertData(accTest);  
        
        //To Cover try-Catch
        SLFUtility.insertData(null); 
    }
    
    /**
* Description : This method is used to cover updateData method on SLFUtility.
*/
    private testMethod static void SLFupdateDataTest()
    {
        List<Account> updateList = [select id from Account limit 1];
        SLFUtility.updateData(updateList);  
        
        //To Cover try-Catch
        SLFUtility.updateData(null); 
    }
    
    /**
* Description : This method is used to cover UpsertData method on SLFUtility.
*/
    private testMethod static void SLFUpsertDataTest()
    {
        List<Account> upsertList = [select id from Account limit 1];
        SLFUtility.upsertData(upsertList);  
        
        //To Cover try-Catch
        SLFUtility.upsertData(null); 
    }
    
    /**
* Description : This method is used to cover DeleteData and unDeleteData method's on SLFUtility.
*/
    private testMethod static void SLFDeleteDataTest()
    {
        List<Account> deleteList = [select id from Account limit 1];
        SLFUtility.deleteData(deleteList); 
        
        SLFUtility.unDeleteData(deleteList);    
        
        //To Cover try-Catch
        SLFUtility.deleteData(null); 
        SLFUtility.unDeleteData(null);
    }
    
    
    /**
* Description : This method is used to cover parseDataValidator method on SLFUtility.
*/
    private testMethod static void ParseDataValidatorTest()
    {
        List<Account> accRec = [select id from Account limit 1];
        list<DataValidator.InputObject> iolist = new list<DataValidator.InputObject>{};
            string errMsg = 'Test Class.';
        UnifiedBean.errorWrapper errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
        errMsg = '';
        accRec[0].PersonOtherPhone = '12345632456';
        iolist.add (new DataValidator.InputObject (accRec[0],'FNI Credit')); 
        errorMsg = SLFUtility.parseDataValidator(iolist,errMsg);
    }
    
    
    /**
* Description : This method is used to cover deSyncCreditRecord method on SLFUtility.
*/
    private testMethod static void deSyncCreditRecordTest(){
        
        List<SLF_Credit__c> creditLst = [select Id,Opportunity__c from SLF_Credit__c limit 1];
        
        set<Id> credtIdSet = new set<Id>();
        set<Id> oppIdSet = new set<Id>();
        
        List<SLF_Credit__c> updateCredit = new List<SLF_Credit__c>();
        for(SLF_Credit__c slfCrdtIterate : creditLst)
        {
            //slfCrdtIterate.IsSyncing__c = true; 
            updateCredit.add(slfCrdtIterate);
        }
        update updateCredit;
        
        for(SLF_Credit__c slfCrdtIterate : creditLst)
        {
            credtIdSet.add(slfCrdtIterate.Id);
            oppIdSet.add(slfCrdtIterate.Opportunity__c);
        }
        
        
        SyncDesyncUtilities.deSyncCreditRecord(new set<Id>(),credtIdSet);
        
        
        SyncDesyncUtilities.deSyncCreditRecord(oppIdSet,credtIdSet);
        
        //To cover try Catch
        SyncDesyncUtilities.deSyncCreditRecord(null,null);
        
        
    }
    
    /**
* Description : This method is used to cover buildUnifiedJSON method on CreateUnifiedJSON.
*/
    private testMethod static void buildUnifiedJSONTest()
    {
        List<SLF_Credit__c> creditLst = [select Id,Opportunity__c from SLF_Credit__c limit 1];
        
        
        
        set<Id> credtIdSet = new set<Id>();
        set<Id> oppIdSet = new set<Id>();
        Map<Id,SLF_Credit__c> creditMap = new Map<Id,SLF_Credit__c>();
        for(SLF_Credit__c slfCrdtIterate : creditLst)
        {
            credtIdSet.add(slfCrdtIterate.Id);
            oppIdSet.add(slfCrdtIterate.Opportunity__c);
            creditMap.put(slfCrdtIterate.Id,slfCrdtIterate);
        }
        CreateUnifiedJSON.buildUnifiedJSON('SLF_Credit__c','credits',oppIdSet,creditLst,null,null);
        
        //Insert StatusAudit record
        Status_Audit__c stsAdt = new Status_Audit__c();
        stsAdt.JSON_Body__c = '{"projects":[{"stips":[{"stipStatus":"New","stipNotes":"Test","stipName":null,"stipId":"S-3","stipEmail":false,"stipDescription":"Verification of mortgage","stipCompletedDate":null},{"stipStatus":"In Progress","stipNotes":"Testing Stipulation","stipName":null,"stipId":"S-4","stipEmail":false,"stipDescription":"Loan agreement not customers legal name","stipCompletedDate":"2017-06-20"},{"stipStatus":"Completed","stipNotes":"Testing Stipulation","stipName":null,"stipId":"S-5","stipEmail":false,"stipDescription":"Loan agreement not customers legal name","stipCompletedDate":"2017-06-20"}],"projectStatus":null,"projectId":"006V000000713cz","fundingStatus":null,"errorMsg":null,"documents":[],"credits":[{"id" : "a1pV0000000s8ICIAY","maxApprovedAmount" : 1000.00}]}]}';
        stsAdt.isProcessed__c = false;
        stsAdt.Opportunity__c = creditLst[0].Opportunity__c;
        insert stsAdt;
        
        //Insert StatusAudit record
        Status_Audit__c stsAdtOne = new Status_Audit__c();
        string JsonString = '{"projects":[{"stips":[{"stipStatus":"New","stipNotes":"Test","stipName":null,"stipId":"S-3","stipEmail":false,"stipDescription":"Verification of mortgage","stipCompletedDate":null},{"stipStatus":"In Progress","stipNotes":"Testing Stipulation","stipName":null,"stipId":"S-4","stipEmail":false,"stipDescription":"Loan agreement not customers legal name","stipCompletedDate":"2017-06-20"},{"stipStatus":"Completed","stipNotes":"Testing Stipulation","stipName":null,"stipId":"S-5","stipEmail":false,"stipDescription":"Loan agreement not customers legal name","stipCompletedDate":"2017-06-20"}],"projectStatus":null,"projectId":"'+creditLst[0].Opportunity__c+'","fundingStatus":null,"errorMsg":null,"documents":[],"credits":[{"id" : "'+creditLst[0].Id+'","maxApprovedAmount" : 1000.00}]}]}';
        
        JsonString = JsonString.replaceAll('\n', '').replaceAll('\r','');
        
        stsAdtOne.JSON_Body__c = JsonString;
        
        stsAdtOne.isProcessed__c = false;
        stsAdtOne.Opportunity__c = creditLst[0].Opportunity__c;
        insert stsAdtOne;
        //List<opportunity> oppRecLst = [select Id,Name from opportunity where ID IN:oppIdSet];
        CreateUnifiedJSON.buildUnifiedJSON('SLF_Credit__c','credits',oppIdSet,creditLst,creditMap,null); 
        
    }
    
    /**
* Description : This method is used to cover getUnifiedBean method on SLFUtility.
*/
    private testMethod static void getUnifiedBeanTest()
    {
        List<opportunity> oppRecLst = [select Id,Name from opportunity];
        set<Id> oppIdSet = new set<Id>();
        for(Opportunity OppIterate : oppRecLst){
            oppIdSet.add(OppIterate.Id);
        }
        
        SLFUtility.getUnifiedBean(oppIdSet,'Orange',false);
        //To Cover Try Catch block
        SLFUtility.getUnifiedBean(null,'Orange',false);
        
    }
    
    
    /**
* Description : This method is used to cover guidIdGenerator method on SLFUtility.
*/
    private testMethod static void guidIdGeneratorTest()
    {
        SLFUtility.guidIdGenerator();
    }
    
    /**
* Description : This method is used to cover getStateCode method on SLFUtility.
*/
    private testMethod static void getStateCodeTest()
    {
        SLFUtility.getStateCode('AK');
        
        //To Cover Try Catch block
        SLFUtility.getStateCode(null);
        SLFUtility.getStateName(null);
        SLFUtility.getCharAtIndex(null,null);
        CreateUnifiedJSON.retriveJSONBean(null);
    }
    
}