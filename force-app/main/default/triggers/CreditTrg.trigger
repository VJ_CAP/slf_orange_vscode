/**
* Description: Credit Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/27/2017          Created
******************************************************************************************/
trigger CreditTrg on SLF_Credit__c (before insert,before update, after insert, after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('SLF_Credit__c');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        CreditTriggerHandler crdTriggerHandler = new CreditTriggerHandler(Trigger.isExecuting);
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                crdTriggerHandler.OnBeforeInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                system.debug('### Executing OnBeforeUpdate');               
                crdTriggerHandler.OnBeforeUpdate(trigger.new,trigger.oldMap);
            }
        }
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                crdTriggerHandler.OnAfterInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                crdTriggerHandler.OnAfterUpdate(trigger.new,trigger.oldMap);
            }
        }
    }
}