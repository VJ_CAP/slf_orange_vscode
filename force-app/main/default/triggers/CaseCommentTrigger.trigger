trigger CaseCommentTrigger on CaseComment (before update) {
    for(CaseComment objC:trigger.new){
        if(objC.CommentBody != Trigger.oldmap.get(objC.id).CommentBody){
            objC.addError('You are not allowed to modify Case Comments');
        }
    }

}