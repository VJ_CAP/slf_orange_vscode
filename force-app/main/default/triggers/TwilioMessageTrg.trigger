/**
* Description: TwilioSF__Message__c related Trigger logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           01/09/2019          Created
*****************************************************************************/ 
trigger TwilioMessageTrg on TwilioSF__Message__c (after insert) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('TwilioSF__Message__c');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        TwilioMessageTriggerHandler twlTriggerHandler = new TwilioMessageTriggerHandler(Trigger.isExecuting);
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                TwilioMessageTriggerHandler.OnAfterInsert(trigger.new);
            }
            
        }
    }
}