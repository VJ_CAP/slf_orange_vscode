trigger SoftPullCreditTrg on Credit__c (before insert, after insert, before update, after update) {

     if(Trigger.isBefore){
        if(Trigger.isInsert){
            SoftPullCreditTriggerHandler.OnBeforeInsert(trigger.new);     
        }
        else if(Trigger.isUpdate){
            SoftPullCreditTriggerHandler.OnBeforeUpdate(trigger.newMap,trigger.oldMap);
        }
    }
    else if(Trigger.isAfter){
        if(Trigger.isInsert){  
            SoftPullCreditTriggerHandler.OnAfterInsert(trigger.newMap);   
        }
        else if(Trigger.isUpdate){
            SoftPullCreditTriggerHandler.OnAfterUpdate(trigger.newMap,trigger.oldMap);
        }
    }
}