trigger BoxUpload_Trigger on Box_Upload__c (after update, after delete) {

    SFSettings__c settings = SFSettings__c.getOrgDefaults();
    if (!settings.Disable_Box_Upload_Trigger__c ) {
        if (Trigger.isUpdate) 
        {

                Map<Id, Boolean> errorMap = new Map<Id, Boolean>();
            
                for (Box_Upload__c u : Trigger.New)
                {
                    if (u.Opportunity__c != null) {
                        errorMap.put(u.Opportunity__c, true);
                    }
                }
                
                // *** now query the Opportunities and add these counts to the error counts on the Opportunity
                List<Opportunity> ol = new List<Opportunity>([Select Id from Opportunity where Id in :errorMap.keySet()]);
                for (Opportunity o : ol) {
                    o.Has_File_Download_Errors__c = true;
                }
            
                update ol;    
        }
        

    }
}