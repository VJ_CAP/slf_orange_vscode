trigger SLFProductTrg on Product__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerHandlerManager.createAndExecuteHandler(ProductTriggerManager.class);
    SLFProductTiggerHandler handler = new SLFProductTiggerHandler();
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            handler.beforeInsert(Trigger.new);
        } 
        else if(Trigger.isUpdate){
            handler.beforeUpdate(Trigger.newMap,Trigger.oldMap);
        }   
    }
}