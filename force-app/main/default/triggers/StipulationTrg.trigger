/**
* Description: Stipulation Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/27/2017          Created
******************************************************************************************/
trigger StipulationTrg on Stipulation__c(before insert,before update,after insert, after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Stipulation__c');
    system.debug('### checkFlag - '+checkFlag);
    system.debug('### checkFlagisActive__c - '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){       
        StipulationTriggerHandler StiplatnTriggerHandler = new StipulationTriggerHandler(Trigger.isExecuting);       
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                StiplatnTriggerHandler.OnAfterInsert(trigger.newMap);
            }else if(Trigger.isUpdate){
                StiplatnTriggerHandler.onAfterUpdate(trigger.newMap, trigger.oldMap);
            } 
        } 
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                StiplatnTriggerHandler.OnBeforeInsert(trigger.new);
            }else if(Trigger.isUpdate){
                StiplatnTriggerHandler.OnBeforeUpdate(trigger.new, trigger.oldMap);
            } 
        }           
    }
}