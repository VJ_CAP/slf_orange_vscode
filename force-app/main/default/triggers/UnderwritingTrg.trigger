/**
* Description: Underwriting File Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         06/27/2017          Created
******************************************************************************************/
trigger UnderwritingTrg on Underwriting_File__c (before insert,before update,after insert, after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Underwriting_File__c');
    if(checkFlag.isActive__c){
        UnderwritingFileTriggerHandler UnderwrTriggerHandler = new UnderwritingFileTriggerHandler(Trigger.isExecuting);
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                system.debug('### Executing before insert');
                UnderwrTriggerHandler.OnBeforeInsert(trigger.new);
            }else if(Trigger.isUpdate){
                system.debug('### Executing before update');
                UnderwrTriggerHandler.OnBeforeUpdate(trigger.new,trigger.oldMap);
            }
        }else if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                system.debug('### Executing after insert');
                UnderwrTriggerHandler.OnAfterInsert(trigger.new);
            }else if(Trigger.isUpdate){
                system.debug('### Executing after update');
                UnderwrTriggerHandler.onAfterUpdate(trigger.newMap, trigger.oldMap);
            } 
        }
    }
}