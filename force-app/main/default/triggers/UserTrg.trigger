/**
* Description: User Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh                  01/03/2018          Created
******************************************************************************************/
trigger UserTrg on User (after insert,before insert,before update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('User');
    system.debug('### checkFlag '+checkFlag);
    
    if(checkFlag.isActive__c){
        UserTriggerHandler usrTriggerHandler = new UserTriggerHandler(Trigger.isExecuting);
        
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                system.debug('### Executing After Insert ');
                usrTriggerHandler.OnAfterInsert(Trigger.new);
            }
        }
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                system.debug('### Executing After Insert ');
                usrTriggerHandler.OnBeforeInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                system.debug('### Executing Before Update');
                usrTriggerHandler.OnBeforeUpdate(Trigger.new);
            }
        }
    }
}