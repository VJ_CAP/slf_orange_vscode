/**
* Description: DocuSign Status Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh R             06/27/2017          Created
******************************************************************************************/
trigger DocusignTrg on dsfs__DocuSign_Status__c (after update, after insert) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('dsfs__DocuSign_Status__c');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        DocusignStatusTriggerHandler docusignTriggerHandler = new DocusignStatusTriggerHandler(Trigger.isExecuting);
       
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                docusignTriggerHandler.OnAfterInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                docusignTriggerHandler.OnAfterUpdate(trigger.newMap,trigger.oldMap);
            }
        }
        /*
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
            system.debug('### Executing OnBeforeInsert ');
                docusignTriggerHandler.OnBeforeInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                system.debug('### Executing OnBeforeUpdate ');
                docusignTriggerHandler.OnBeforeUpdate(trigger.new,trigger.oldMap);
            }
        }*/
    }
}