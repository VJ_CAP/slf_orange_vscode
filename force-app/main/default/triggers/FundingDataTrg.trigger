/**
* Description: Funding Data related Trigger logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           04/05/2019          Created
******************************************************************************************/ 

trigger FundingDataTrg on Funding_Data__c (before insert,before update,after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Funding_Data__c');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        FundingDataTriggerHandler fndDataTriggerHandler = new FundingDataTriggerHandler(Trigger.isExecuting);
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                fndDataTriggerHandler.OnBeforeInsert(Trigger.new); 
            }
            if(Trigger.isUpdate){
                fndDataTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap); 
            }
        }
        if(Trigger.isAfter)
        {
            if(Trigger.isUpdate){
                fndDataTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap); 
            }
        }
        
    }
}