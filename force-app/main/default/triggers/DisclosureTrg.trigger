trigger DisclosureTrg on Disclosures__c (before insert, before update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Disclosures__c');
    system.debug('### checkFlag - '+checkFlag);
    system.debug('### checkFlagisActive__c - '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){             
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                DisclosureTriggerHandler.OnBeforeInsert(trigger.new);
            }else if(Trigger.isUpdate){
                DisclosureTriggerHandler.OnBeforeUpdate(trigger.new, trigger.oldMap);
            } 
        }           
    }
}