/**
* Description: Account related Trigger logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma           08/04/2017          Created
******************************************************************************************/ 

trigger AccountTrg on Account (before update,after update,after insert,before insert) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Account');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        AccountTriggerHandler ActTriggerHandler = new AccountTriggerHandler(Trigger.isExecuting);
        RecordsSharingToParentAccount RecordShareHandler  = new RecordsSharingToParentAccount();
        if(Trigger.isUpdate && Trigger.isAfter ){
            ActTriggerHandler.OnAfterUpdate(Trigger.new, Trigger.oldMap); 
            if(!Test.isRunningTest()){
                RecordShareHandler.AccountShareToParentAccount(Trigger.newMap,Trigger.oldMap);//Added By Brahmeswar
                //System.enqueuejob(new AccountShareToParentAccount(Trigger.newMap,Trigger.oldMap));
            }
        }
        if(Trigger.isInsert && Trigger.isAfter){//Added By Brahmeswar
          if(!Test.isRunningTest()){
                RecordShareHandler.AccountShareToParentAccount(Trigger.newMap,new Map<id,Account>());
                //System.enqueuejob(new AccountShareToParentAccount(Trigger.newMap,new Map<id,Account>()));
            }
                ActTriggerHandler.accountSharetoServicePartner(Trigger.new);
        }
        if(Trigger.isInsert && Trigger.isBefore){//Added By Deepika
            ActTriggerHandler.OnBeforeInsert(Trigger.new); 
        }
        if(Trigger.isUpdate && Trigger.isBefore){//
            RecordShareHandler.keepManualSharedRecordsOnOwnerChangeOfAccount(Trigger.newMap,Trigger.oldMap);
            ActTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap); //Srikanth - 07/15 - Orange-990
        }
        
    }
}