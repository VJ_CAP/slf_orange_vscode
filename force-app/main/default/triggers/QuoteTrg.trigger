/**
* Description: Quote Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         09/06/2017          Created
******************************************************************************************/
trigger QuoteTrg on Quote (before insert,after insert,after update, before update){
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Quote');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        QuoteTriggerHandler quoteTriggerHandler = new QuoteTriggerHandler(Trigger.isExecuting);
        if(Trigger.isAfter)
        {
            //Updated By Adithya as part of ORANGE-1628
            //Start
            //Orange - 7744 Udaya Kiran Start
           //QuotePaymentSchedulesGenerator.run();
            //QuoteMaxPaymentSchedulesGenerator.run();
            //
            if(Trigger.isInsert){                
                QuotePaymentSchedulesGenerator.run();
                QuoteMaxPaymentSchedulesGenerator.run();
                
                //Added by Adithya for testing                
                QuotePaymentSchedulesIOGenerator.run();
                quoteTriggerHandler.onAfterInsert(trigger.new);
            }else if(Trigger.isUpdate){
                for(Quote objQ:Trigger.new){
                    System.debug('@@@@quoteID'+objQ.Id);
                    //if(objQ.SLF_Product__c != Trigger.OldMap.get(objQ.Id).SLF_Product__c){
                    if(objQ.IsSyncing){
                        QuotePaymentSchedulesGenerator.run();
                        QuoteMaxPaymentSchedulesGenerator.run();
                        
                        //Added by Adithya for testing                        
                        QuotePaymentSchedulesIOGenerator.run();
                    }
                }
                quoteTriggerHandler.onAfterUpdate(trigger.new,trigger.oldMap);
                
            }//Orange - 7744 Udaya Kiran End
        }
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                quoteTriggerHandler.QuoteOwnerChangeBasedOnOpportunityOwner(trigger.new);
            }else if(Trigger.isUpdate){  // Added as part of ORANGE-2040
                quoteTriggerHandler.onBeforeUpdate(trigger.new); 
                system.debug('---Quote trigger before update completed.---');
            }
        }
        
        
    }
}