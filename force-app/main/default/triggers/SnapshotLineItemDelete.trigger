trigger SnapshotLineItemDelete on Funding_Snapshot_Line_Item__c (before delete) 
{
	delete [SELECT Id FROM Underwriting_Transaction__c WHERE Funding_Snapshot_Line_Item__c IN :trigger.oldmap.keySet()];
}