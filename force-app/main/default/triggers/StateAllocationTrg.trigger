/**
* Description: State Allocation Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         07/10/2019          Created
******************************************************************************************/
trigger StateAllocationTrg on State_Allocation__c (before insert,before update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('State_Allocation__c');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        StateAllocationTriggerHandler stAlocTriggerHandler = new StateAllocationTriggerHandler(Trigger.isExecuting);
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                stAlocTriggerHandler.OnBeforeInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                system.debug('### Executing OnBeforeUpdate');               
                stAlocTriggerHandler.OnBeforeUpdate(trigger.new,trigger.oldMap);
            }
        }
    }
}