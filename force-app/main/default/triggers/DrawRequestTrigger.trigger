/**
* Description: Draw Requests related Trigger logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Suresh           01/04/2019          Created
******************************************************************************************/ 
trigger DrawRequestTrigger on Draw_Requests__c (before update,after insert,after Update) {
TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Draw_Requests__c');
    if(checkFlag.isActive__c){
        DrawRequestTriggerHandler drawReqTriggerHandler = new DrawRequestTriggerHandler(Trigger.isExecuting);
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                system.debug('### Executing after insert');
                drawReqTriggerHandler.OnAfterInsert(trigger.new);
            } 
            if(Trigger.isUpdate){
                system.debug('### Executing after insert');
                drawReqTriggerHandler.OnAfterUpdate(trigger.new,trigger.oldMap);
            }
        }
        if(Trigger.isBefore)
        {
            if(Trigger.isUpdate){
                system.debug('### Executing before update');
                drawReqTriggerHandler.onBeforeUpdate(trigger.new,trigger.oldMap);
            }
        }
    }
}