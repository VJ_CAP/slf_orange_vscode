trigger OpportunityTrg on Opportunity (after insert, after update,before insert,before update) {
   TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Opportunity');
    
    if(checkFlag.isActive__c){
        OpportunityTriggerHandler optyTriggerHandler = new OpportunityTriggerHandler(Trigger.isExecuting);
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                optyTriggerHandler.OnAfterInsert(trigger.newMap);                
            }
            else if(Trigger.isUpdate){
                optyTriggerHandler.OnAfterUpdate(trigger.newMap,trigger.oldMap);              
            }
        }
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                optyTriggerHandler.OnBeforeInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                optyTriggerHandler.OnBeforeUpdate(trigger.newMap,trigger.oldMap);
            }
        }
    }
}