/**
* Description: Box_Fields__c Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         01/31/2019          Created
******************************************************************************************/
trigger BoxFieldsTrg on Box_Fields__c (after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Box_Fields__c');
    if(checkFlag.isActive__c){
        BoxFieldsTriggerHandler boxFieldsTriggerHandler = new BoxFieldsTriggerHandler(Trigger.isExecuting);
        if(Trigger.isAfter)
        {
            if(Trigger.isUpdate){
                system.debug('### Executing after update');
                boxFieldsTriggerHandler.onAfterUpdate(trigger.newMap, trigger.oldMap);
            } 
        }
    }
}