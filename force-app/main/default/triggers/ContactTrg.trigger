/**
* Description: Contact Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Sreenivas             23/05/2018               Created
******************************************************************************************/
trigger ContactTrg on Contact (before insert,before update,after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Contact');
    system.debug('### checkFlag '+checkFlag);
                                                  
    if(checkFlag.isActive__c){
        ContactTriggerHandler triggerHandler = new ContactTriggerHandler(Trigger.isExecuting);
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                triggerHandler.OnBeforeInsert(trigger.new);
            }
            else if(Trigger.isUpdate){
                triggerHandler.OnBeforeUpdate(trigger.new,trigger.oldMap);
            }
        }
        else if(Trigger.isAfter)
        {
            if(Trigger.isUpdate){
                ContactTriggerHandler.OnAfterUpdate(trigger.newMap,trigger.oldMap);
            }
        }
    }
}