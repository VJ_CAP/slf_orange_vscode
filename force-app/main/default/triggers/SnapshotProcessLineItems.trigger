trigger SnapshotProcessLineItems on Funding_Snapshot__c (before update, after update, before delete) 
{
	if (trigger.isBefore && trigger.isUpdate)
	{
		for (Funding_Snapshot__c fs : trigger.new)
		{
			if (trigger.oldMap.get (fs.Id).Processed__c != null && fs.Processed__c == null)
			{
				//  fs.addError ('You cannot unset the Processed Date of a Snapshot');
			}
		}
	} 

	set<Id> facilitySnapshotIds = new set<Id>{};
	set<Id> partnerSnapshotIds = new set<Id>{};
	set<Id> undoSnapshotIds = new set<Id>{};

	set<Id> accountIds = new set<Id>{};  //  SP-192

//  *********************************************************************

	if (trigger.isAfter && trigger.isUpdate)
	{
		for (Funding_Snapshot__c fs : trigger.new)
		{
			if (trigger.oldMap.get (fs.Id).Processed__c == null && fs.Processed__c != null)
			{
				if (fs.Account_Type__c == 'Facility') facilitySnapshotIds.add (fs.Id);
				if (fs.Account_Type__c == 'Partner') partnerSnapshotIds.add (fs.Id);

				accountIds.add (fs.Account__c);  //  SP-192
			}

			if (trigger.oldMap.get (fs.Id).Processed__c != null && fs.Processed__c == null)  // undo
			{
				undoSnapshotIds.add (fs.Id);
			}
		}

		if (facilitySnapshotIds.size() > 0) FundingSnapshotSupport.processLineItemsForFacility (facilitySnapshotIds);
		if (partnerSnapshotIds.size() > 0) FundingSnapshotSupport.processLineItemsForPartner (partnerSnapshotIds);
		if (undoSnapshotIds.size() > 0) FundingSnapshotSupport.deleteTransactionsForSnapshot (undoSnapshotIds);

		//  delete old unprocessed Snapshots for the Accounts that have just been processed  SP-192

		delete [SELECT Id FROM Funding_Snapshot__c WHERE Account__c IN :accountIds AND Processed__c = null];
	}

//  *********************************************************************

	//  if Snapshot is deleted, explicitly delete it's line items, so that the 
	//  line item trigger will fire to delete associated transactions, thus rolling back the Snapshot

	if (trigger.isDelete)
	{
		FundingSnapshotSupport.deleteLineItemsForSnapshot (trigger.oldMap.keyset());
	}
}