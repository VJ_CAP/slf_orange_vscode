/**
* Description: Task related Trigger logic .
*
*   Modification Log :
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Rajesh                      01/03/2019          Created
******************************************************************************************/ 

trigger TaskTrg on Task (after insert,after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('Task');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        if(Trigger.isInsert && Trigger.isAfter){
            TaskTriggerHandler TskTriggerHandler = new TaskTriggerHandler();
            TskTriggerHandler.OnAfterInsert(Trigger.new);
        }
        if(Trigger.isUpdate && Trigger.isAfter){
            TaskTriggerHandler TskTriggerHandler = new TaskTriggerHandler();
            TskTriggerHandler.OnAfterUpdate(Trigger.new,Trigger.oldMap);
        }
    }
}