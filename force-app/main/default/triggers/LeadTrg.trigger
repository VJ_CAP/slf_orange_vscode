trigger LeadTrg on Lead (before insert, after insert, before update, after update) {     
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            LeadTriggerHandler.OnBeforeInsert(trigger.new);     
        }
        else if(Trigger.isUpdate){
            LeadTriggerHandler.OnBeforeUpdate(trigger.newMap,trigger.oldMap);
        }
    }
    else if(Trigger.isAfter){
        if(Trigger.isInsert){  
            LeadTriggerHandler.OnAfterInsert(trigger.newMap);   
        }
        else if(Trigger.isUpdate){
            LeadTriggerHandler.OnAfterUpdate(trigger.newMap,trigger.oldMap);
        }
    }
}