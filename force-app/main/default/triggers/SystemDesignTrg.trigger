/**
* Description: System Design Trigger. 
*
*   Modification Log : 
---------------------------------------------------------------------------
    Developer               Date                Description
---------------------------------------------------------------------------
    Adithya Sarma K         10/18/2017          Created
******************************************************************************************/
trigger SystemDesignTrg on System_Design__c (before insert,after insert, after update) {
    TriggerFlags__c checkFlag = TriggerFlags__c.getInstance('System_Design__c');
    system.debug('### checkFlag '+checkFlag);
    system.debug('### checkFlagisActive__c '+checkFlag.isActive__c);
    if(checkFlag.isActive__c){
        SystemDesignTriggerHandler sysdesignTriggerHandler = new SystemDesignTriggerHandler(Trigger.isExecuting);
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert){
                sysdesignTriggerHandler.OnAfterInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                sysdesignTriggerHandler.OnAfterUpdate(trigger.new,trigger.oldMap);
            }
        }
        
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert){
                sysdesignTriggerHandler.SystemDesignOwnerChangeBasedOnOpportunityOwner(trigger.new);                
            }
        }
    }
}